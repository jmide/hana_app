$.import("timp.core.server", "util");
var util = $.timp.core.server.util;
$.import("timp.core.server.orm", "table");
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models", "schema");
var schema = $.timp.core.server.models.schema;

this.components = new table_lib.Table({
	component: 'CORE',
	name: schema.default+'."CORE::COMPONENT"',
	fields: {
		id: new table_lib.AutoField({
			name: "ID",
			auto: schema.default+'."CORE::COMPONENT::ID".nextval',
			type: $.db.types.INTEGER,
			pk: true
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		version: new table_lib.Field({
			name: 'VERSION',
			type: $.db.types.NVARCHAR,
			dimension: 10
		}),
		description: new table_lib.Field({
			name: 'LABEL_PTBR',
			type: $.db.types.NVARCHAR,
			dimension: 255
		})
	}
});

this.components.getList = function(whereOptions) {
	return this.READ();
};

this.getList = function(whereOptions, fields) {
	if (typeof whereOptions === "object") {
		return this.components.READ({
			where: whereOptions,
			fields: fields !== undefined ? fields : []
		});
	}
	return this.components.READ();
};

this.apps = new table_lib.Table({
	component: 'CORE',
	name: schema.default+'."CORE::Apps"',
	fields: {
		id: new table_lib.AutoField({
			name: "ID",
			auto: schema.default+'."CORE::Apps::ID".nextval',
			type: $.db.types.INTEGER,
			pk: true
		}),
		creationDate: new table_lib.AutoField({
			name: 'CREATION.DATE',
			auto: 'NOW()',
			update: false,
			type: $.db.types.TIMESTAMP
		}),
		creationUser: new table_lib.AutoField({
			name: 'CREATION.USER',
			auto: 'SESSION_USER',
			update: false,
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		modificationDate: new table_lib.AutoField({
			name: 'MODIFICATION.DATE',
			auto: 'NOW()',
			update: true,
			type: $.db.types.TIMESTAMP
		}),
		modificationUser: new table_lib.AutoField({
			name: 'MODIFICATION.USER',
			auto: 'SESSION_USER',
			update: true,
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		url: new table_lib.Field({
			name: 'URL',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		icon: new table_lib.Field({
			name: 'ICON',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		background: new table_lib.Field({
			name: 'BACKGROUND',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		iconfont: new table_lib.Field({
			name: 'ICONFONT',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		component: new table_lib.Field({
			name: 'COMPONENT',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		component_id: new table_lib.Field({
			name: 'COMPONENT_ID',
			type: $.db.types.INTEGER
		}),
		description: new table_lib.Field({
			name: 'DESCRIPTION',
			type: $.db.types.TEXT
		}),
		usLabel: new table_lib.Field({
			name: 'US_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		brLabel: new table_lib.Field({
			name: 'BR_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 128
		})
	}
});

this.privileges = new table_lib.Table({
	component: 'CORE',
	name: schema.default+'."CORE::Privileges"',
	fields: {
		id: new table_lib.AutoField({
			name: "ID",
			auto: schema.default+'."CORE::Privileges::ID".nextval',
			type: $.db.types.INTEGER,
			pk: true
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 64
		}),
		id_app: new table_lib.Field({
			name: 'ID_APP',
			type: $.db.types.INTEGER
		}),
		description: new table_lib.Field({
			name: 'DESCRIPTION',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		descriptionPtBr: new table_lib.Field({
			name: 'DESCRIPTION_PTBR',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		descriptionEnUs: new table_lib.Field({
			name: 'DESCRIPTION_ENUS',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		role: new table_lib.Field({
			name: 'ROLE',
			type: $.db.types.NVARCHAR,
			dimension: 255
		})
	}
});

/*
Get privileges
Usage:
    this.getPrivileges({
        component_name (optional): "BRB"
    });
Returns:
    [<privilege>, ..., <privilege>]
*/

// this.apps.getPrivilegesAppsByComponents = function(component) {
// 	var options = {
// 		fields: ['id', 'name', 'component'],
// 		join: [{
// 			fields: ['id', 'name'],
// 			alias: 'privileges',
// 			table: $.timp.core.server.models.components.privileges,
// 			on: [{
// 				left: 'id',
// 				right: 'id_app'
// 			}]
//      }]
// 	};

// 	if (component) {
// 		options.where = [
// 			{
// 				"field": "component",
// 				"value": component,
// 				"oper": "="
// 			}
//             ];
// 	}

// 	return this.READ(options);
// };