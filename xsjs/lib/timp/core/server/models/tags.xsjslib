$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

/*
CREATE COLUMN TABLE "TIMP"."CORE::Tags" (ID INTEGER, OBJTYPE NVARCHAR(500), OBJID INTEGER, EMPRESA NVARCHAR(5000), ESTADO NVARCHAR(5000), FILIAL NVARCHAR(5000), IMPOSTO NVARCHAR(5000));
CREATE SEQUENCE "TIMP"."CORE::Tags::ID";
*/
var tags_table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Tags"',
	fields: {
		'id': new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Tags::ID".nextval',
			type: $.db.types.INTEGER,
			pk : true
		}),
		'objtype': new table_lib.Field({
			name: 'OBJTYPE',
			type: $.db.types.NVARCHAR,
			dimension: 500
		}),
		'objid': new table_lib.Field({
			name: 'OBJID',
			type: $.db.types.INTEGER
		}),
		'empresa': new table_lib.Field({
			name: 'EMPRESA',
			type: $.db.types.NVARCHAR,
			dimension: 5000
		}),
		'estado': new table_lib.Field({
			name: 'ESTADO',
			type: $.db.types.NVARCHAR,
			dimension: 5000
		}),
		'filial': new table_lib.Field({
			name: 'FILIAL',
			type: $.db.types.NVARCHAR,
			dimension: 5000
		}),
		'imposto': new table_lib.Field({
			name: 'IMPOSTO',
			type: $.db.types.NVARCHAR,
			dimension: 5000
		})
	}
});
this.tags_table = tags_table;
this.table = tags_table;

/*
	STATIC
*/
tags_table.categories = ['empresa', 'estado', 'filial', 'imposto'];

/*
Creates the join with Tags for selection.
Example:
	tags_table.getJoin({
		table: '"TIMP_TEST"."BRB::Report"',
		empresa (optional): ['A', 'B', 'C'],
		estado (optional):  ['SP', 'RJ', 'MG'],
		filial (optional):  ['001', '002', '003'],
		imposto (optional): ['ICMS', 'IPI', 'PIS/COFINS']
	}, values)
Returns:
	INNER JOIN 
		"TIMP_TEST"."CORE::Tags" 
	ON 
		"TIMP_TEST"."CORE::Tags"."OBJTYPE" = ? AND 
		"TIMP_TEST"."CORE::Tags"."OBJID" = "TIMP_TEST"."BRB::Report"."ID" AND 
		(   "TIMP_TEST"."CORE::Tags"."EMPRESA" = ? OR 
			"TIMP_TEST"."CORE::Tags"."EMPRESA" LIKE ? OR 
			"TIMP_TEST"."CORE::Tags"."EMPRESA" LIKE ? OR 
			"TIMP_TEST"."CORE::Tags"."EMPRESA" LIKE ?
		) AND (
			"TIMP_TEST"."CORE::Tags"."IMPOSTO" = ? OR 
			"TIMP_TEST"."CORE::Tags"."IMPOSTO" LIKE ? OR 
			"TIMP_TEST"."CORE::Tags"."IMPOSTO" LIKE ? OR 
			"TIMP_TEST"."CORE::Tags"."IMPOSTO" LIKE ?
		) AND ...
And populates the values array:
	[ { value: '"TIMP_TEST"."BRB::Report"', type: 11 },
	  { value: 'ANY', type: 51 },
	  { value: '%;A;%', type: 51 },
	  { value: '%;B;%', type: 51 },
	  { value: '%;C;%', type: 51 } ]
*/
tags_table.getJoin = function (options, values) {
    util.debug('tags_table.getJoin', options, values);
    
	if(!(options.hasOwnProperty('table') && util.isString(options.table)))
	    return "";
	var where = [
		{field: 'objtype', oper: '=', value: options.table},
		{field: 'objid', oper: '=', value: options.table+'."ID"', literal: true}
	];

	var categories = ['empresa', 'estado', 'filial', 'imposto'];

	for (var x = 0; x < categories.length; x++) {
		var category = categories[x];
		if(options.hasOwnProperty(category) && util.isArray(options[category])) {
			var sub = [{field: category, oper: '=', value: 'ANY'}];
			for (var i = 0; i < options[category].length; i++) {
			    if (util.isString(options[category][i]))
				    sub.push({field: category, oper: 'LIKE', value: '%;' + options[category][i] + ';%'});
			}
			where.push(sub);
		}
	}
	
	var join = 'INNER JOIN ' + this.name + ' ON ' + this.__parseWhere__(where, values);
	return join;
}

/*
Creates the join with Tags for selection.
Example:
	tags_table.createTags({
		table: '"TIMP_TEST"."BRB::Report"',
		id: 180,
		empresa (optional): ['A', 'B', 'C'],
		estado (optional):  ['SP', 'RJ', 'MG'],
		filial (optional):  ['001', '002', '003'],
		imposto (optional): ['ICMS', 'IPI', 'PIS/COFINS']
	})
Returns:
	true
*/
tags_table.createTags = function (options) {
	var categories = this.categories;

	var values = {
		objtype: options.table,
		objid: options.id
	};
	for (var i = 0; i < categories.length; i++) {
		var category = categories[i];
		if(options.hasOwnProperty(category)) {
			if (util.isArray(options[category])) {
				values[category] = ';' + options[category].join(';') + ';';
			} else {
				values[category] = 'ANY';
			}
		} else {
			values[category] = ';;';
		}
	}

    this.tags = false;
	return this.CREATE(values);
}

/*
Creates the join with Tags for selection.
Example:
	tags_table.updateTags({
		table: '"TIMP_TEST"."BRB::Report"',
		id: 180,
		empresa (optional): ['A', 'B', 'C'],
		estado (optional):  ['SP', 'RJ', 'MG'],
		filial (optional):  ['001', '002', '003'],
		imposto (optional): ['ICMS', 'IPI', 'PIS/COFINS']
	})
Returns:
	true
*/
tags_table.updateTags = function (options) {
	var categories = this.categories;

	var values = {};
	for (var i = 0; i < categories.length; i++) {
		var category = categories[i];
		if(options.hasOwnProperty(category) && util.isArray(options[category])) {
			values[category] = ';' + options[category].join(';') + ';';
		} else {
			values[category] = 'ANY';
		}
	}

	return this.UPDATEWHERE(values, [{field: 'objtype', oper: '=', value: options.table}, {field: 'objid', oper: '=', value: options.id}])
}

/*
Creates the join with Tags for selection.
Example:
	tags_table.updateTags({
		table: '"TIMP_TEST"."BRB::Report"',
		id: 180
	})
Returns:
	true
*/
tags_table.deleteTags = function (options) {
	return this.DELETEWHERE([{field: 'objtype', oper: '=', value: options.table}, {field: 'objid', oper: '=', value: options.id}]);
}

/*
	Usage:
		tags_table.list({ table:'"TIMP"."BRB::Report"', field: 'empresa' });
	Returns:
		['TRAP','trap','Trap Ltda', 'Pirelli', 'Goodyear', 'Goldyear', ..]
		
	Usage:
		tags_table.list({ table:'"TIMP"."BRB::Report"', field: 'estado' });
	Returns:
		['SP','MG','São Paulo', 'RJ', 'rj', 'Rio de Janeiro', ..]
	
	Usage:
		tags_table.list({ table:'"TIMP"."BRB::Report"', field: 'filial' });
	Returns:
		['001','123456/123', ..]
	
	Usage:
		tags_table.list({ table:'"TIMP"."BRB::Report"', field: 'imposto' });
	Returns:
		['ICMS','PIS/CONFINS','IPI', ..]
*/
tags_table.list = function (options) {
	if(tags_table.list.hasOwnProperty('_validate')) tags_table.list._validate(arguments);
	
	var records = this.READ({
		fields: [options.field],
		where: [{'field': 'objtype', 'oper': '=', 'value': options.table}]	
	});
	var values = "";
	for (var i = 0; i < records.length; i++) {
		var item = records[i][options.field];
		if (!(item == "ANY" || item == ";;")) {
			values += item.slice(1);
		}
	}
	values = values.slice(0,-1).split(';');
	var response = [];
	for (var i = 0; i < values.length; i++) {
		if(response.indexOf(values[i]) == -1 && values[i])
			response.push(values[i])
	}
	return response;
}
tags_table.list = util.Declare({
	options: {
		table: util.isString,
		field: util.isString
	}
}, tags_table.list);