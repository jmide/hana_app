$.import("timp.core.server","util");
var util = $.timp.core.server.util;

$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;

var table = new table_lib.Table({
    component: 'CORE',
	name: '"_SYS_BIC"."timp.mkt.modeling/CV_USER_PRIVILEGES"',
	tags: false,
	fields: {
        id: new table_lib.Field({
			name: 'PrivilegeID',
			type: $.db.types.INTEGER
		}),
		hanaUser : new table_lib.Field({
			name: 'HANA_USER',
			type: $.db.types.NVARCHAR
		}),
		privilegeID : new table_lib.Field({
			name: 'PrivilegeID',
			type: $.db.types.INTEGER
		}),
		privilegeName : new table_lib.Field({
			name: 'PrivilegeName',
			type: $.db.types.NVARCHAR
		}),
		appID : new table_lib.Field({
			name: 'AppID',
			type: $.db.types.INTEGER
		}),
		appName : new table_lib.Field({
			name: 'AppName',
			type: $.db.types.NVARCHAR
		}),
		component : new table_lib.Field({
			name: 'COMPONENT',
			type: $.db.types.NVARCHAR
		})
	}
});

this.table = table;

this.table.listViewUsersPrivileges = function(component, user){
    
    var options = {
        where: [
                {"field": "hanaUser", "value": user, "oper": "="}
            ]    
    };
    
    if(component){
        options.where.push( 
                {"field": "component", "value": component, "oper": "="}
            );
    }
    return this.READ(options);
}