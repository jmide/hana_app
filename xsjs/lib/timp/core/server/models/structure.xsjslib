// $.import('timp.core.server.api','api');
// var core_api = $.timp.core.server.api.api;
// var user = core_api.users;

$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

$.import('timp.core.server.orm', 'table');
var def_table = $.timp.core.server.orm.table;

$.import('timp.core.server.orm', 'view');
var def_view = $.timp.core.server.orm.view;

$.import('timp.core.server.models', 'users');
var user = $.timp.core.server.models.users.users;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

$.import('timp.core.server.models', 'schema');
var schema = $.timp.core.server.models.schema;

// Get labels and description in current language
var lang = $.request.cookies.get("Content-Language") === "enus" ? "EN" : "PT";

//Model for the ATR Structure
var table = new def_table.Table({
    component: 'ATR',
	name: schema.default + '."ATR::Structure"',
	default_fields: 'common', 
	fields: {
		id: new def_table.AutoField({
			name: 'ID',
			auto: schema.default + '."ATR::Structure::ID".nextval',
			type: $.db.types.INTEGER,
			pk: true
		}),
		isDeleted: new def_table.Field({
			name: 'isDeleted',
			type: $.db.types.TINYINT,
			translate: {1:true, 0:false}
		}),
		title: new def_table.Field({
			name: 'title',
			type: $.db.types.NVARCHAR,
			dimension : 100
		}),
		structure: new def_table.Field({
			name: 'JSON',
			type: $.db.types.NCLOB
		})
	}
});
this.table = table;
/*
Structure
	public createStructure(json_structure)
		Number:id
	public listStructures(json_options)
		Object[]:json_report
	public getStructure(id)
		Object:json_structure
	public deleteStructure(id)
		boolean:isSuccess
	public updateStructure(json_structure) #with id
		boolean:isSuccess
*/
table._translateFields = function(fields){
    for (var i = 0; i < fields.length; i++){
        fields[i].label = fields[i]["label"+lang];
    }
    return fields;
}
	
table._translateLevels = function(levels){
    if(levels.length > 0){
        for (var j = 0; j < levels.length; j++) {
            levels[j].name = levels[j]["name"+lang];
            levels[j].description = levels[j]["description"+lang];
            if(levels[j].levels.length > 0)
                levels[j].levels = this._translateLevels(levels[j].levels);
        }
    }
    return levels;
}

table.createStructure = function (json_structure) {
	var options = {
		isDeleted: json_structure.isDeleted,
		creationDate: json_structure.creationDate,
		creationUser: json_structure.creationUser,
		modificationDate: json_structure.modificationDate,
		modificationUser: json_structure.modificationUser,
		title: json_structure.title,
		JSON: JSON.stringify(json_structure.JSON)
	};
	return this.CREATE(options);
};

table.__process__ = function (obj) {
	var response = [];
	var i;
	for (i = 0; i < obj.length; i++) {
		if(util.isString(obj[i].structure)) {
			obj[i].structure = JSON.parse(obj[i].structure);
		}
		//var j;
		//for(j = 0; j < obj[i].length; j++){
			//var x = obj[i];
		for(var x in obj[i]) {
			if(obj[i].hasOwnProperty(x) && x !== 'structure') {
				obj[i].structure[x] = obj[i][x];
			}
		}
		if(obj[i].structure.hasOwnProperty('levels')){
            obj[i].structure.levels = this._translateLevels(obj[i].structure.levels);
		}
		if(obj[i].structure.hasOwnProperty('fields')){
            
            for(var j = 0; j < obj[i].structure.fields.length; j++){
                obj[i].structure.fields[j].label = obj[i].structure.fields[j]["label" + lang];
            }

            obj[i].structure.fields = this._translateFields(obj[i].structure.fields);
		}
		obj[i].structure.description = obj[i].structure["description"+lang];
		response.push(obj[i].structure);
	}
	return response;
};

table.listStructures = function (json_options) {
	var options = {
        join: [{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'modificationUserInfo',
            table: user.table,
            on: [{left: 'modificationUser', right: 'id'}],
            outer: 'left'
        },{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'creationUserInfo',
            table: user.table,
            rename: 'B',
            on: [{left: 'creationUser', right: 'id'}],
            outer: 'left'
        }]
	};
// 	throw json_options.title;
	if(json_options.title) {
		options.where = [{
			"field": "title",
			"value": json_options.title,
			"oper": " LIKE "
		}];
	}
	if(json_options.ids){
        if(!options.where){
            options.where = [];
        }
        var idsWhere = [];
        for(var i=0; i<json_options.ids.length;i++){
            idsWhere.push({
                field: 'id',
                oper: '=',
                value: json_options.ids[i]
            })
        }
        options.where.push(idsWhere);
	}
// 	throw options;
	var obj = this.READ(options);
	
	return this.__process__(obj);
};

table.listStructuresById = function (json_options) {
	var options = {
        join: [{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'modificationUserInfo',
            table: user.table,
            on: [{left: 'modificationUser', right: 'id'}],
            outer: 'left'
        },{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'creationUserInfo',
            table: user.table,
            rename: 'B',
            on: [{left: 'creationUser', right: 'id'}],
            outer: 'left'
        }]
	};
	if(json_options.title) {
		options.where = [{
			"field": "title",
			"value": json_options.title,
			"oper": " LIKE "
		}];
	}
	if(json_options.ids){
        if(!options.where){
            options.where = [];
        }
        var idsWhere = [];
        for(var i=0; i<json_options.ids.length;i++){
            idsWhere.push({
                field: 'id',
                oper: '=',
                value: json_options.ids[i]
            })
        }
        options.where.push(idsWhere);
	}
	var obj = this.READ(options);
	return this.__process__(obj);
};

table.getStructure = function (id) {
	var options = {
		"where": [{"field": "id", "value": id, "oper": "="}],
		"join": [{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'modificationUserInfo',
            table: user.table,
            on: [{left: 'modificationUser', right: 'id'}],
            outer: 'left'
        },{
            fields: ['name', 'last_name', 'hana_user'],
            alias: 'creationUserInfo',
            table: user.table,
            rename: 'B',
            on: [{left: 'creationUser', right: 'id'}],
            outer: 'left'
        }]
	};
	var obj = this.READ(options);
	
	obj = this.__process__(obj)[0];
	
	return obj;
};

/*model.getStructureField = function(structureId, fieldId){
	var structure = getStructure(structureId);
	var i;
	for(i = 0; i < structure.fields.length; i++){
		var f = structure.fields[i];
		//for (f in structure.fields){
		 if(f.id === fieldId){
			 return f;
		 }
	 }
	 return null;
};*/

function getCurrentDate (){
    
    var currentDate = new Date();
    var t = currentDate.toJSON();

    t = t.replace("T", " ");
    t = t.replace("Z", "");
    
    return t;

}

function getCurrentUserId () {
    var currentUsername = $.session.getUsername();
    var options = {
        fields: ['id'],
        where: [{"field": "name", "value": currentUsername, "oper": "="}]
    };
    var object = user.READ(options);
    
    if (object.length === 0) {
        return -1;
    } else {
        return object[0].id;
    }
}

table.updateStructure = function (json_structure) {
	var options = {
		id: json_structure.id,
		title: json_structure.title,
		structure: json_structure.structure
	}
	return this.UPDATE(options);
};

table.deleteStructure = function (id) {
	return this.UPDATE({
		id: id,
		isDeleted: 0
	});
};

/*
Get values for a field of a structure. May be used by multiple components.
Usage:
    this.getFieldValues({
        structure: <structure_id>,
        field: <field_id>
    })
*/
table.getFieldValues = function (options) {
    if (!options.hasOwnProperty('structure') || isNaN(options.structure)) {
        throw "Received invalid structure ID for getFieldValues: " + JSON.stringify(options);
    }
    if (!options.hasOwnProperty('field') || isNaN(options.field)) {
        throw "Received invalid field ID for getFieldValues: " + JSON.stringify(options);
    }
    var struct = this.getStructure(options.structure);
    if (!util.isValid(struct)) {
        throw 'No structure was found for the ID: ' + options.structure;
    }
    
    var viewname = '"_SYS_BIC"."' + struct.hanaPackage + '/' + struct.hanaName + '"';
    
    for (var i = 0; i < struct.fields.length; i++) {
        if (struct.fields[i].ID == options.field) {
            var fieldname = struct.fields[i].hanaName;
        }
    }
    
    if(!util.isString(fieldname)) {
        throw 'No field was found on "' + struct.title + '" with the ID: ' + options.id; 
    }
    
    var query = "SELECT DISTINCT " + fieldname + " FROM " + viewname;
    try {
        var response = SQL.SELECT({
            query: query,
            values: []
        });
        
        if(response.length) {
            for (var i = 0; i < response.length; i++) {
                response[i] = response[i][0];
            }
        }
        
        return response;
    } catch (e) {
        throw 'Failed to execute statement: ' + query;
    }
}

/*
    Returns an array of COLUMN_NAMEs.
    @type Array
    
    returns:
    [
        "column_name1",
        "column_name2",
        "column_name3"
    ]
*/
table.getViewColumns = function(obj) {
    var options = JSON.parse(obj);
    var struct = this.getStructure(options.id);
    
    var query = "SELECT \"COLUMN_NAME\" FROM _SYS_BI.BIMC_PROPERTIES WHERE CATALOG_NAME = '" + struct.hanaPackage + "' AND CUBE_NAME = '" + struct.hanaName + "' AND LEVEL_UNIQUE_NAME is not null AND PROPERTY_NAME <> 'LEVEL_ATTRIBUTE'";
    try {
        var response = SQL.SELECT({
            query: query,
            values: []
        });
        
        if(response.length) {
            for (var i = 0; i < response.length; i++) {
                response[i] = response[i][0];
            }
        }
        return response;
    }catch(e){
        throw "Failed to execute getViewColumns: " + util.parseError(e);
    }
}

/*
Get ATR View
    table.getStructureView(1);
*/
table.getStructureView = function (id) {
    var struct = this.READ({
    	where: [{field: 'id', oper: '=', value: Number(id)}]
    })[0];
    
    if (!struct) {
        return false;
    }
    
    struct = JSON.parse(struct.structure);
    struct.fields = struct.fields;
    
    var options = {
        name: '"_SYS_BIC"."' + struct.hanaPackage + '/' + struct.hanaName + '"',
        fields: {}
    }
    
    var fields = [];
    for (var i = 0; i < struct.fields.length; i++) {
        var field = struct.fields[i];
        fields.push(field.hanaName);
        options.fields[field.hanaName] = new def_table.Field({
    		name: field.hanaName,
    		type: $.db.types[field.type],
    		dimension: field.dimension,
    		precision: field.precision
    	});
    }
    
    var view = new def_view.View(options);
    
    return view;
}
