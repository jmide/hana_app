$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

var table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Labels"',
	tags: false,
	fields: {
		id : new table_lib.AutoField({
            pk : true,
			name: "ID",
			auto: schema.default + '."CORE::Labels::ID".nextval',
			type: $.db.types.INTEGER
		}),
		objType: new table_lib.Field({
			name: 'OBJTYPE',
			type: $.db.types.NVARCHAR,
            dimension: 50
		}),
		key: new table_lib.Field({
			name: 'KEY',
			type: $.db.types.NVARCHAR,
            dimension: 50
        }),
		brLabel: new table_lib.Field({
			name: 'BR_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		usLabel: new table_lib.Field({
			name: 'US_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		bond: new table_lib.Field({
			name: 'BOND',
			type: $.db.types.NVARCHAR,
            dimension: 50
		})
	}
});
this.table = table;

//Old Function
//this.table.getLabelsByObjType = function(objType) {
//	var options = {
        //where: [{"field": "objType", "value": objType.toUpperCase(), "oper": "="}]
	//}
    
//     return this.READ(options);
// }

this.table.getLabelsByObjTypeAndKey = function(objType, keys){
    
    try{
    	var options = {
            where: [{
                    "field": "objType", 
                    "value": objType.toUpperCase(),
                    "oper": "="
                },{
                    field: "key",
                    oper: "=",
                    value: keys
                }]
	    };
        return this.table.READ(options);
    }catch(e){
        //let ex = e;
    }
    return null;
    
};

this.table.getLabelsByObjType = function(objType, bond, orderByName, key) {
	var options = {};
	
    var lang = $.request.cookies.get("Content-Language");
    lang = (lang === "enus") ? "us" : "br";
	
    if(typeof bond === "string" && bond.length && bond !== '""' && util.isValid(bond)) {
        options.where = [
            {"field": "objType", "value": objType.toUpperCase(), "oper": "="},
            {"field": "bond", "value": bond, "oper": "="}
        ];
    } else {
        options.where = [{"field": "objType", "value": objType.toUpperCase(), "oper": "="}];
	}
	if(typeof key === "string" && key.length && key !== '""' && util.isValid(key)) {
        options.where.push({"field": "key", "value": key, "oper": "="});
	}else if(typeof key === "object" && key.length){
	    options.where.push({"field": "key", "value": key, "oper": "="});
	}
    var response = this.READ(options);
    
    for(var i = 0; i < response.length; i++){
        response[i].label = response[i][lang+"Label"];
        // delete response[i].usLabel;
        // delete response[i].brLabel;
    }
    
    var _sortLabel = function(a, b) {
        var _a = a.label.toLowerCase(),
            _b = b.label.toLowerCase();
        if (_a < _b) return -1;
        return _a > _b;
    };
    
    var _sortKey = function(a, b) {
        var _a = a.key.toLowerCase(),
            _b = b.key.toLowerCase();
        if (_a < _b) return -1;
        return _a > _b;
    };
    
    if(typeof orderByName != "undefined" && orderByName == true){
        response.sort(_sortLabel);
    } else {
        response.sort(_sortKey);        
    }
    
    return response;
};