$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

var table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::DataTypeConversion"',
	tags: false,
	fields: {
		id : new table_lib.AutoField({
            pk : true,
			name: "ID",
			auto: schema.default + '."CORE::DataTypeConversion::ID".nextval',
			type: $.db.types.INTEGER
		}),
		
		target: new table_lib.Field({
			name: 'TARGET',
			type: $.db.types.VARCHAR,
            dimension: 16
		}),
		targetTypeId: new table_lib.Field({
			name: 'TARGET_TYPE_ID',
			type: $.db.types.SMALLINT
        }),
		targetSqlDataType: new table_lib.Field({
			name: 'TARGET_SQL_DATA_TYPE',
			type: $.db.types.SMALLINT
		}),
		source: new table_lib.Field({
			name: 'SOURCE',
			type: $.db.types.VARCHAR,
			dimension: 12
		}),
		sourceTypeId: new table_lib.Field({
			name: 'SOURCE_TYPE_ID',
			type: $.db.types.INTEGER
		}),
		sourceSqlDataType: new table_lib.Field({
			name: 'SOURCE_SQL_DATA_TYPE',
			type: $.db.types.INTEGER
		})
	}
});
this.table = table;


this.table.insertDataTypes = function() {
    try {
        var clearDataTypeConversion = SQL.DELETE({
            query: "DELETE FROM " + schema.default + ".\"CORE::DataTypeConversion\" "
        });
        
        var insertSmallDecimal = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'SMALLDECIMAL' AS \"SOURCE\", " +
                	" 3000 AS \"SOURCE_TYPE_ID\", " +
                	" 3000 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('DECIMAL','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
    	var insertBinText = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'BINTEXT' AS \"SOURCE\", " +
                	" -10 AS \"SOURCE_TYPE_ID\", " +
                	" -10 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        var insertText = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'TEXT' AS \"SOURCE\", " +
                	" -10 AS \"SOURCE_TYPE_ID\", " +
                	" -10 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        var insertAlphanum = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'ALPHANUM' AS \"SOURCE\", " +
                	" -9 AS \"SOURCE_TYPE_ID\", " +
                	" -9 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertNVarchar = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'NVARCHAR' AS \"SOURCE\", " +
                	" -9 AS \"SOURCE_TYPE_ID\", " +
                	" -9 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARBINARY','ALPHANUM','VARCHAR') " +
                ") )"
        });
        
        var insertShortText = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'SHORTTEXT' AS \"SOURCE\", " +
                	" -9 AS \"SOURCE_TYPE_ID\", " +
                	" -9 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARBINARY','ALPHANUM','VARCHAR') " +
                ") )"
        });
        
        var insertNChar = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'NCHAR' AS \"SOURCE\", " +
                	" -8 AS \"SOURCE_TYPE_ID\", " +
                	" -8 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARBINARY','ALPHANUM','VARCHAR') " +
                ") )"
        });
        
        var insertTinyInt = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'TINYINT' AS \"SOURCE\", " +
                	" -6 AS \"SOURCE_TYPE_ID\", " +
                	" -6 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('SMALLINT','INTEGER','BIGINT','DECIMAL','SMALLDECIMAL','REAL','DOUBLE','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertBigInt = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'BIGINT' AS \"SOURCE\", " +
                	" -5 AS \"SOURCE_TYPE_ID\", " +
                	" -5 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('DECIMAL','DOUBLE','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertBlob = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'BLOB' AS \"SOURCE\", " +
                	" -4 AS \"SOURCE_TYPE_ID\", " +
                	" -4 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        // // var insertStGeometry = SQL.EXECUTE({
        // // 	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
        // //         	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
        // //         	"( SELECT DISTINCT " + 
        // //         	" TYPE_NAME AS \"TARGET\", " +
        // //         	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
        // //         	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
        // //         	" 'ST_GEOMETRY' AS \"SOURCE\", " +
        // //         	" -4 AS \"SOURCE_TYPE_ID\", " +
        // //         	" -4 AS \"SOURCE_SQL_DATA_TYPE\" " +
        // //         	" FROM DATA_TYPES " +
        // //         ") )"
        // // });
        
        // // var insertStPoint = SQL.EXECUTE({
        // // 	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
        // //         	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
        // //         	"( SELECT DISTINCT " + 
        // //         	" TYPE_NAME AS \"TARGET\", " +
        // //         	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
        // //         	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
        // //         	" 'ST_POINT' AS \"SOURCE\", " +
        // //         	" -4 AS \"SOURCE_TYPE_ID\", " +
        // //         	" -4 AS \"SOURCE_SQL_DATA_TYPE\" " +
        // //         	" FROM DATA_TYPES " +
        // //         ") )"
        // // });
        
        // // var insertStPointZ = SQL.EXECUTE({
        // // 	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
        // //         	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
        // //         	"( SELECT DISTINCT " + 
        // //         	" TYPE_NAME AS \"TARGET\", " +
        // //         	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
        // //         	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
        // //         	" 'ST_POINTZ' AS \"SOURCE\", " +
        // //         	" -4 AS \"SOURCE_TYPE_ID\", " +
        // //         	" -4 AS \"SOURCE_SQL_DATA_TYPE\" " +
        // //         	" FROM DATA_TYPES " +
        // //         ") )"
        // // });
        
        // // var insertVarbinary = SQL.EXECUTE({
        // // 	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
        // //         	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
        // //         	"( SELECT DISTINCT " + 
        // //         	" TYPE_NAME AS \"TARGET\", " +
        // //         	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
        // //         	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
        // //         	" 'VARBINARY' AS \"SOURCE\", " +
        // //         	" -3 AS \"SOURCE_TYPE_ID\", " +
        // //         	" -3 AS \"SOURCE_SQL_DATA_TYPE\" " +
        // //         	" FROM DATA_TYPES " +
        // //         ") )"
        // // });
        
        // var insertBinary = SQL.EXECUTE({
        // 	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
        //         	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
        //         	"( SELECT DISTINCT " + 
        //         	" TYPE_NAME AS \"TARGET\", " +
        //         	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
        //         	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
        //         	" 'BINARY' AS \"SOURCE\", " +
        //         	" -2 AS \"SOURCE_TYPE_ID\", " +
        //         	" -2 AS \"SOURCE_SQL_DATA_TYPE\" " +
        //         	" FROM DATA_TYPES " +
        //         ") )"
        // });
        
        var insertClob = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'CLOB' AS \"SOURCE\", " +
                	" -1 AS \"SOURCE_TYPE_ID\", " +
                	" -1 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        var insertChar = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'CHAR' AS \"SOURCE\", " +
                	" 1 AS \"SOURCE_TYPE_ID\", " +
                	" 1 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        var insertDecimal = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'DECIMAL' AS \"SOURCE\", " +
                	" 3 AS \"SOURCE_TYPE_ID\", " +
                	" 3 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertInteger = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'INTEGER' AS \"SOURCE\", " +
                	" 4 AS \"SOURCE_TYPE_ID\", " +
                	" 4 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('BIGINT','DECIMAL','SMALLDECIMAL','REAL','DOUBLE','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertSmallInt = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'SMALLINT' AS \"SOURCE\", " +
                	" 5 AS \"SOURCE_TYPE_ID\", " +
                	" 5 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('INTEGER','BIGINT','DECIMAL','SMALLDECIMAL','REAL','DOUBLE','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertReal = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'REAL' AS \"SOURCE\", " +
                	" 7 AS \"SOURCE_TYPE_ID\", " +
                	" 7 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('DOUBLE','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertDouble = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'DOUBLE' AS \"SOURCE\", " +
                	" 8 AS \"SOURCE_TYPE_ID\", " +
                	" 8 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertVarchar = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'VARCHAR' AS \"SOURCE\", " +
                	" 12 AS \"SOURCE_TYPE_ID\", " +
                	" 12 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('NVARCHAR','VARBINARY','ALPHANUM') " +
                ") )"
        });
        
        var insertDate = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'DATE' AS \"SOURCE\", " +
                	" 91 AS \"SOURCE_TYPE_ID\", " +
                	" 91 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('SECONDDATE','TIMESTAMP','VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertTime = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'TIME' AS \"SOURCE\", " +
                	" 92 AS \"SOURCE_TYPE_ID\", " +
                	" 92 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertSecondDate = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'SECONDDATE' AS \"SOURCE\", " +
                	" 93 AS \"SOURCE_TYPE_ID\", " +
                	" 93 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertTimestamp = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'TIMESTAMP' AS \"SOURCE\", " +
                	" 93 AS \"SOURCE_TYPE_ID\", " +
                	" 93 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                	" WHERE TYPE_NAME IN ('VARCHAR','NVARCHAR') " +
                ") )"
        });
        
        var insertNClob = SQL.EXECUTE({
        	query: "INSERT INTO " + schema.default + ".\"CORE::DataTypeConversion\" ( " +
                	" SELECT " + schema.default + ".\"CORE::DataTypeConversion::ID\".nextval AS \"ID\", * FROM " + 
                	"( SELECT DISTINCT " + 
                	" TYPE_NAME AS \"TARGET\", " +
                	" TYPE_ID AS \"TARGET_TYPE_ID\", " +
                	" SQL_DATA_TYPE AS \"TARGET_SQL_DATA_TYPE\", " +
                	" 'NCLOB' AS \"SOURCE\", " +
                	" -10 AS \"SOURCE_TYPE_ID\", " +
                	" -10 AS \"SOURCE_SQL_DATA_TYPE\" " +
                	" FROM DATA_TYPES " +
                ") )"
        });
        
        var response = {
            insertSmallDecimal: insertSmallDecimal,
            insertBinText: insertBinText,
            insertText: insertText,
            insertAlphanum: insertAlphanum,
            insertNVarchar: insertNVarchar,
            insertShortText: insertShortText,
            insertNChar: insertNChar,
            insertTinyInt: insertTinyInt,
            insertBigInt: insertBigInt,
            insertBlob: insertBlob,
            // insertStGeometry: insertStGeometry,
            // insertStPoint: insertStPoint,
            // insertStPointZ: insertStPointZ,
            // insertVarbinary: insertVarbinary,
            // insertBinary: insertBinary,
            insertClob: insertClob,
            insertChar: insertChar,
            insertDecimal: insertDecimal,
            insertInteger: insertInteger,
            insertSmallInt: insertSmallInt,
            insertReal: insertReal,
            insertDouble: insertDouble,
            insertVarchar: insertVarchar,
            insertDate: insertDate,
            insertTime: insertTime,
            insertSecondDate: insertSecondDate,
            insertTimestamp: insertTimestamp,
            insertNClob: insertNClob
        };
        return response;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.getColumnsTypeFromTable = function(table, isBackup) {
    try {
        var tableFullPath = table.split(".");
        var schema = tableFullPath[0].slice(1,-1);
        var tableName = tableFullPath[1].slice(1,-1);
        
        var tableNameBackup = "BKP_" + tableFullPath[1].slice(1,-1);
        
        if(isBackup) {
            tableName = tableNameBackup;
        }
        
        var columns = SQL.SELECT({
        	query: " SELECT " + 
                	" COLUMN_NAME, " +
                	" DATA_TYPE_NAME, " +
                	" CS_DATA_TYPE_ID, " +
                	" CS_DATA_TYPE_NAME " +
                	" FROM TABLE_COLUMNS " +
                	" WHERE SCHEMA_NAME = '" + schema + "'" +
                	" AND TABLE_NAME = '" + tableName + "'" +
                	" ORDER BY POSITION "
        });
     
        return columns;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.getDataTypeConversion = function(source, target) {
    try {
        var options = {
            where: [
                {"field": "source", "value": source, "oper": "="},
                {"field": "target", "value": target, "oper": "="}
            ]
        };
	
        var response = this.READ(options);
        return response;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.createBackupTable = function(table) {
    try {
        var tableFullPath = table.split(".");
        var schema = tableFullPath[0];
        var tableName = tableFullPath[1];
        
        var tableNameBackup = "\"BKP_" + tableFullPath[1].slice(1,-1) + "\"";
        
        var createBackupTable = SQL.EXECUTE({
        	query: " CREATE COLUMN TABLE " + schema + "." + tableNameBackup + " AS " +
                	" (SELECT * FROM " + schema + "." + tableName + ")"
        });
        
        // $.trace.error(createBackupTable);
        return createBackupTable;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.rollbackTable = function(table) {
    try {
        var tableFullPath = table.split(".");
        var schema = tableFullPath[0];
        var tableName = tableFullPath[1];
        
        var tableNameBackup = "\"BKP_" + tableFullPath[1].slice(1,-1) + "\"";
        
        var createTable = SQL.EXECUTE({
        	query: " CREATE COLUMN TABLE " + schema + "." + tableName + " AS " +
                	" (SELECT * FROM " + schema + "." + tableNameBackup + ")"
        });
        
        // $.trace.error(createTable);
        return createTable;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.dropTable = function(table, isBackup) {
    try {
        var tableFullPath = table.split(".");
        var schema = tableFullPath[0];
        var tableName = tableFullPath[1];
        
        var tableNameBackup = "\"BKP_" + tableFullPath[1].slice(1,-1) + "\"";
        
        var dropTable;
        if(isBackup) {
            dropTable = SQL.EXECUTE({
            	query: " DROP TABLE " + schema + "." + tableNameBackup
            });
        } else {
            dropTable = SQL.EXECUTE({
            	query: " DROP TABLE " + schema + "." + tableName
            });
        }
        // $.trace.error(dropTable);
        return dropTable;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};

this.table.insertDataFromBackup = function(table, differentTypeColumns, columnsTypeFromTable) {
    try {
        var tableFullPath = table.split(".");
        var schema = tableFullPath[0];
        var tableName = tableFullPath[1];
        
        var tableNameBackup = "\"BKP_" + tableFullPath[1].slice(1,-1) + "\"";
        
        var newColumns = [];
        var columnNames = [];
        for(var i = 0; i < columnsTypeFromTable.length; i++) {
            for(var name in differentTypeColumns) {
                var diffColumn = differentTypeColumns[name];
                // If the table columns from database is not into the diffentTypeColumns
                if(columnsTypeFromTable[i][0] !== diffColumn.name) {
                    // Validation to avoid repeated columns
                    if(columnNames.indexOf(columnsTypeFromTable[i][0]) === -1) {
                        // Add into newColumns array the columns from database
                        newColumns.push({
                            name: columnsTypeFromTable[i][0],
                            type: columnsTypeFromTable[i][1],
                            conversion: false
                        });
                        columnNames.push(columnsTypeFromTable[i][0]);
                    }
                } else {
                    // Validation to avoid repeated columns
                    if(columnNames.indexOf(diffColumn.name) === -1) {
                        // Add into the newColumns array the columns that need conversion
                        newColumns.push({
                            name: diffColumn.name,
                            type: diffColumn.type,
                            conversion: true
                        });
                        columnNames.push(diffColumn.name);
                    }
                }
            }
        }
        
        var selectQuery = " INSERT INTO " + schema + "." + tableName;
        selectQuery += " SELECT ";
		var first = true;
        for(var x = 0; x < newColumns.length; x++) {
	        if (first) {
               first = false;
            } else {
               selectQuery += ", ";
            }
            if(newColumns[x].conversion === true) {
                selectQuery += " CAST(\"" + newColumns[x].name + "\" AS " + newColumns[x].type + ")";
            } else {
            	selectQuery += "\"" + newColumns[x].name + "\"";
            }
        }
        selectQuery += " FROM " + schema + "." + tableNameBackup;
        
        var insertData = SQL.EXECUTE({
        	query: selectQuery
        });
        // $.trace.error(insertData);
        return insertData;
    } catch(e) {
        $.trace.error(e);
        return null;
    }
};