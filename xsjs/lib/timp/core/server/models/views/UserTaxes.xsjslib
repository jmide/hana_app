$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server.orm', 'table');
var tableLib = $.timp.core.server.orm.table;

let useComponentAttribute = false;
let userTaxes = new BaseModel({
	name: 'timp.core.modeling.users/USER_TAXES',
	schema: '_SYS_BIC',
	identity: 'timp.core.modeling.users/USER_TAXES',
	type: 'view',
	inputParameters: [
		{
		    hanaName: 'USER_ID',
			columnName: '$$USER_ID$$',
			type: 'integer',
			defaultValue: $.getUserID()
		},
		{
		    hanaName: 'HANA_NAME',
			columnName: '$$HANA_NAME$$',
			type: 'string',
			defaultValue: $.session.getUsername()
		}
	],
	fields: {
		user: {
			columnName: 'USER',
			type: 'integer',
			primaryKey: true
		},
		tax: {
			columnName: 'TAX',
			type: 'string',
			primaryKey: true
		},
		taxName: {
			columnName: 'TAX_DESCRIPTION',
			type: 'string'
		}
	}
}, useComponentAttribute);

this.READ = function(object) {
	return userTaxes.find(object);
};

this.getTaxesByUser = function(nameRequired) {
	const result = this.READ({
		where: [{
			field: "user",
			value: $.getUserId(),
			operator: '$eq'
        }]
	}).results;
	var taxesArray = result.map((tax) => nameRequired ? tax.taxName : tax.tax);
	if (!taxesArray.length) {
		taxesArray.push('-1');
	}
	return taxesArray;
};

this.getTaxesByUserToLike = function(fieldName) {
	const result = this.READ({
		where: [{
			field: "user",
			value: $.getUserId(),
			operator: '$eq'
        }]
	}).results;
	if (result.length > 0) {
		var taxesArray = result.map((e) => ({
			field: fieldName,
			oper: "LIKE",
			value: '%' + e.tax + '%'
		}));
		if (result.length > 1) {
			return taxesArray;
		} else {
			return taxesArray[0];
		}
	} else {
		var taxesObj = {
			field: fieldName,
			oper: "LIKE",
			value: '%' + null + '%'
		};
		return taxesObj;
	}
};

let userTaxes_orm1 = new tableLib.Table({
	component: 'CORE',
	name: '"_SYS_BIC"."timp.core.modeling.users/USER_TAXES"',
	fields: {
		user: new tableLib.AutoField({
			name: 'USER',
			type: $.db.types.INTEGER
		}),
		tax: new tableLib.Field({
			name: 'TAX',
			type: $.db.types.NVARCHAR,
			dimension: 10
		})
	}
});
this.userTaxes_orm1 = userTaxes_orm1;
this.getIdentity = function() {
	return userTaxes.getIdentity();
};

this.getOrm1Table = function() {
	return userTaxes_orm1;
};

this.getJoin = function(fieldName, alias) {
	return {
	    map:'taxJoin',
		alias: 'taxJoin',
		type: 'inner',
		on: [{
			alias: 'taxJoin',
			field: 'tax',
			operator: '$eq',
			value: {
				alias: alias,
				field: fieldName
			}
		},
		{
			alias: 'taxJoin',
			field: 'user',
			operator: '$eq',
			value: $.getUserID()
		}]
	}
};

this.getAlias = function() {
	return {
		collection: userTaxes.getIdentity(),
		name: 'taxJoin'
	}
};

this.validateTax = function(tax){
    var taxe = tax;
    return this.getTaxesByUser().indexOf(tax) !== -1;
};