$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

var userPrivilegeView = new BaseModel({
	name: 'timp.core.modeling.users/CV_USER_PRIVILEGES',
	schema: '_SYS_BIC',
	identity: 'timp.core.modeling.users/CV_USER_PRIVILEGES',
	type: 'view',
	fields: {
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		},
		privilegeId: {
		    columnName: 'PRIVILEGE_ID',
			type: 'integer',
			primaryKey: true
		},
		hanaUser: {
			columnName: 'HANA_USER',
			type: 'string',
			size: 32
		},
		privilegeName: {
			columnName: 'PRIVILEGE_NAME',
			type: 'string',
			size: 50
		},
		descriptionEnus: {
			columnName: 'DESCRIPTION_ENUS',
			type: 'string',
			size: 255
		},
		descriptionPtbr: {
			columnName: 'DESCRIPTION_PTBR',
			type: 'string',
			size: 255
		},
		role: {
			columnName: 'ROLE',
			type: 'string',
			size: 255
		}, 
		componentId: {
		    columnName: 'COMPONENT_ID',
		    type: 'integer'
		},
		componentName: {
		    columnName: 'COMPONENT_NAME',
		    type: 'string',
		    size: 128
		}
	}
}, false);

this.userPrivilegeView = userPrivilegeView;

var orgPrivilegesView = new BaseModel({
	name: 'timp.core.modeling.users/CV_USER_ORG_PRIVILEGES',
	schema: '_SYS_BIC',
	identity: 'timp.core.modeling.users/CV_USER_ORG_PRIVILEGES',
	"inputParameters": { // Input parameters mapping for the calculation views
        "IP_MANDANTE": {
            "columnName": "$$IP_MANDANTE$$",
            "isMandatory": true,
            "type": "string"
        }
    },
	type: 'view',
	fields: {
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		},
		hanaUser: {
			columnName: 'HANA_USER',
			type: 'string',
			size: 32
		},
		companyId: {
			columnName: 'COMPANY_ID',
			type: 'string',
			size: 4
		},
		stateId: {
			columnName: 'STATE_ID',
			type: 'string',
			size: 2,
			primaryKey: true
		},
		branchId: {
			columnName: 'BRANCH_ID',
			type: 'string',
			size: 4
		}
	}
}, false);

this.orgPrivilegesView = orgPrivilegesView;