$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

const fileSystemOptions = {
    "name": "timp.core.modeling.fileSystem/CV_FILE",
    "schema": "_SYS_BIC",
    "identity": "timp.core.modeling.fileSystem/CV_FILE",
    "type": "view",
    "inputParameters":
    {
        "$$USER_ID$$":
        {
            "columnName": "$$USER_ID$$",
            "type": "string",
            "isMandatory": true,
            "defaultValue": ""
        },
        "$$language$$":
        {
            "columnName": "$$language$$",
            "type": "string",
            "isMandatory": false,
            "defaultValue": ""
        },
        "$$client$$":
        {
            "columnName": "$$client$$",
            "type": "string",
            "isMandatory": false,
            "defaultValue": ""
        }
    },
    "fields":
    {
        "bcbBuilderconfigurationId":
        {
            "type": "integer",
            "columnName": "BCB_BUILDERCONFIGURATION_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "bcbBuilderexecutionversionId":
        {
            "type": "integer",
            "columnName": "BCB_BUILDEREXECUTIONVERSION_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "bcbHierarchyconfigurationId":
        {
            "type": "integer",
            "columnName": "BCB_HIERARCHYCONFIGURATION_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "bfbFormId":
        {
            "type": "integer",
            "columnName": "BFB_FORM_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "bfbLayoutId":
        {
            "type": "integer",
            "columnName": "BFB_LAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "bfbSettingId":
        {
            "type": "integer",
            "columnName": "BFB_SETTING_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "creationDate":
        {
            "type": "datetime",
            "columnName": "CREATION_DATE",
            "default": null,
            "required": false
        },
        "creationUser":
        {
            "type": "integer",
            "columnName": "CREATION_USER",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "creationUserLastName":
        {
            "type": "string",
            "columnName": "CREATION_USER_LAST_NAME",
            "size": 50,
            "precision": null,
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false
        },
        "creationUserName":
        {
            "type": "string",
            "columnName": "CREATION_USER_NAME",
            "size": 50,
            "precision": null,
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false
        },
        "dfgAn3Id":
        {
            "type": "integer",
            "columnName": "DFG_AN3_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "dfgAn4Id":
        {
            "type": "integer",
            "columnName": "DFG_AN4_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "dfgDigitalfileId":
        {
            "type": "integer",
            "columnName": "DFG_DIGITALFILE_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "dfgLayoutId":
        {
            "type": "integer",
            "columnName": "DFG_LAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "dfgSettingId":
        {
            "type": "integer",
            "columnName": "DFG_SETTING_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "folderId":
        {
            "type": "integer",
            "columnName": "FOLDER_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "id":
        {
            "type": "integer",
            "columnName": "ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "isFavorite":
        {
            "type": "tinyint",
            "columnName": "IS_FAVORITE",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "isShared":
        {
            "type": "tinyint",
            "columnName": "IS_SHARED",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "modificationDate":
        {
            "type": "datetime",
            "columnName": "MODIFICATION_DATE",
            "default": null,
            "required": false
        },
        "modificationUser":
        {
            "type": "integer",
            "columnName": "MODIFICATION_USER",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "modificationUserLastName":
        {
            "type": "string",
            "columnName": "MODIFICATION_USER_LAST_NAME",
            "size": 50,
            "precision": null,
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false
        },
        "modificationUserName":
        {
            "type": "string",
            "columnName": "MODIFICATION_USER_NAME",
            "size": 50,
            "precision": null,
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false
        },
        "status":
        {
            "type": "tinyint",
            "columnName": "STATUS",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tdkDashboardLayoutId":
        {
            "type": "integer",
            "columnName": "TDK_DASHBOARDLAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tdkExecutionId":
        {
            "type": "integer",
            "columnName": "TDK_EXECUTION_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tdkKpilayoutId":
        {
            "type": "integer",
            "columnName": "TDK_KPILAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tdkLayoutId":
        {
            "type": "integer",
            "columnName": "TDK_LAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tfbBookId":
        {
            "type": "integer",
            "columnName": "TFB_BOOK_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tfbLayoutId":
        {
            "type": "integer",
            "columnName": "TFB_LAYOUT_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        },
        "tfbSettingId":
        {
            "type": "integer",
            "columnName": "TFB_SETTING_ID",
            "default": null,
            "required": false,
            "primaryKey": false,
            "unique": false,
            "autoIncrement": false
        }
    }
};

this.fileSystemCV = new BaseModel(fileSystemOptions,false);