$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'table');
var tableLib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

this.table = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::AdditionalFunction"',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::AdditionalFunction::ID".nextval',
            type: $.db.types.INTEGER,
            pk: true
        }),
        name: new tableLib.Field({
            name: 'NAME',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        component: new tableLib.Field({
            name: 'COMPONENT',
            type: $.db.types.NVARCHAR,
            dimension: 5
        })
    }
});
this.create = function(object){
    var response = {};
    try{
        var requiredProperties = ['name','component'];
        var missingProperties = requiredProperties.filter(function(r){return Object.keys(object).indexOf(r) === -1;});
        if(missingProperties.length > 0){
            $.messageCodes.push({code:'CORE009072',type:'E',errorInfo: "Missing properties: " + missingProperties.join(',')});
            return response;
        }
        var objToCreate = {
            name: object.name,
            component: object.component
        };
        response = this.table.CREATE(objToCreate);
    }catch(e){
        $.messageCodes.push({code:'CORE009071',type:'E',errorInfo: util.parseError(e)});
    }
    return response;
};
this.read = function(object){
    var response = null;
    try{
        var requiredProperties = ['name','component'];
        var missingProperties = requiredProperties.filter(function(r){return Object.keys(object).indexOf(r) === -1;});
        if(missingProperties.length > 0){
            $.messageCodes.push({code:'CORE009072',type:'E',errorInfo: "Missing properties: " + missingProperties.join(',')});
            return response;
        }
        response = this.table.READ({
            where:[
                {field:'name',oper:'=',value:object.name},
                {field:'component',oper:'=',value:object.component}
            ]
        })[0];
    }catch(e){
        $.messageCodes.push({code:'CORE009072',type:'E',errorInfo: util.parseError(e)});
    }
    return response;
};