$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;
$.import('timp.core.server.orm', 'table');
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.messageCodes = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::MessageCode"',
	default_fields: 'common',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::MessageCode::ID".nextval', 
			type: $.db.types.INTEGER
		}),
		code: new table_lib.Field({
			name: 'CODE',
			type: $.db.types.NVARCHAR,
			dimension: 20,
			pk: true
		}),
		id_component: new table_lib.Field({
			name: 'ID_COMPONENT',
			type: $.db.types.INTEGER
		}),
		dev_type: new table_lib.Field({
			name: 'DEV_TYPE',
			type: $.db.types.NVARCHAR,
			dimension: 1
		}),
		area: new table_lib.Field({
			name: 'AREA',
			type: $.db.types.NVARCHAR,
			dimension: 2
		}),
		code_seq: new table_lib.Field({
			name: 'CODE_SEQ',
			type: $.db.types.NVARCHAR,
			dimension: 3
		})
	}
})

this.messageCodeTexts = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::MessageCodeText"',
	default_fields: 'common',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::MessageCodeText::ID".nextval', 
			type: $.db.types.INTEGER
		}),
		code: new table_lib.Field({
			name: 'CODE',
			type: $.db.types.NVARCHAR,
			dimension: 20,
			pk: true
		}),
		lang: new table_lib.Field({
			name: 'LANG',
			type: $.db.types.NVARCHAR,
			dimension: 10,
			pk: true
		}),
		text: new table_lib.Field({
			name: 'TEXT',
			type: $.db.types.TEXT
		})
	}
})