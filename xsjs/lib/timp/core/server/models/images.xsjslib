$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'table');
var table = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.images_table = new table.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Images"',
	default_fields: "common",
	fields: {
		id: new table.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Images::ID".nextval',
			type: $.db.types.INTEGER,
			pk:true
		}),
		name: new table.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		description: new table.Field({
			name: 'DESCRIPTION',
			type: $.db.types.NVARCHAR,
			dimension: 500
		}),
		image: new table.Field({
			name: 'IMAGE',
			type: $.db.types.BLOB
		})
	}
});
this.table = this.images_table;
