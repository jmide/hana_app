$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'table');
var table = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.attachment = new table.Table({
    component: 'CORE',
    name: schema.default + '."CORE::Attachments"',
    default_fields: "common",
    fields: {
        id: new table.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::Attachments::ID".nextval',
            type: $.db.types.INTEGER,
            pk: true
        }),
        name: new table.Field({
            name: 'NAME',
            type: $.db.types.NVARCHAR,
            dimension: 50
        }),
        description: new table.Field({
            name: 'DESCRIPTION',
            type: $.db.types.NVARCHAR,
            dimension: 500
        }),
        type: new table.Field({
            name: 'MIME_TYPE',
            type: $.db.types.NVARCHAR,
            dimension: 150
        }),
        idComponent: new table.Field({
            name: 'ID_COMPONENT',
            type: $.db.types.INTEGER
        }),
        file: new table.Field({
            name: 'FILE',
            type: $.db.types.BLOB
        })
    }
});
this.table = this.attachment;