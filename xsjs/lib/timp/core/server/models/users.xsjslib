$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import("timp.core.server.orm","table");
var tableLib = $.timp.core.server.orm.table;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

this.users = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::USER"',
	fields: {
		id : new tableLib.AutoField({
			name: "ID",
			auto: schema.default + '."CORE::USER::ID".nextval',
			type: $.db.types.INTEGER
		}),
		creationDate: new tableLib.AutoField({
			name: 'CREATION_DATE',
			auto: 'NOW()',
			update: false,
			type: $.db.types.TIMESTAMP
		}),
		creationUser: new tableLib.AutoField({
			name: 'CREATION_USER',
			auto: 'SESSION_USER',
			update: false,
			type: $.db.types.INTEGER
		}),
		modificationDate: new tableLib.AutoField({
			name: 'MODIFICATION_DATE',
			auto: 'NOW()',
			update: true,
			type: $.db.types.TIMESTAMP
		}),
		modificationUser: new tableLib.AutoField({
			name: 'MODIFICATION_USER',
			auto: 'SESSION_USER',
			update: true,
			type: $.db.types.INTEGER
		}),
		name : new tableLib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 50
		}),
		email : new tableLib.Field({
			name: 'EMAIL',
			type: $.db.types.NVARCHAR,
			dimension: 100
		}),
		hana_user : new tableLib.Field({
			name: 'HANA_USER',
			type: $.db.types.NVARCHAR,
			dimension: 32,
			pk: true
		}),
		last_name : new tableLib.Field({
			name: 'LAST_NAME',
			type: $.db.types.NVARCHAR,
			dimension: 50
		}),
		cargo : new tableLib.Field({
			name: 'POSITION',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		status : new tableLib.Field({
			name: 'IS_ACTIVE',
			type: $.db.types.TINYINT
		}),
		admin : new tableLib.Field({
			name: 'IS_ADMIN',
			type: $.db.types.TINYINT
		})
	}
});
this.table = this.users;

/*
Usage:
	this.setUserStatus({
		status: 4,
		users: [1,2,3,4,5]
	})
*/
this.users.setUserStatus = util.Declare({
    'options': {
        'status': util.isNumber,
        'users': [util.isNumber]
    }
}, function (options) {
    try {
        if (typeof this._validate == 'function') this._validate(arguments);
        
        var where = [],
            i,
            old_users,
            old_statuses = {};
        for (i = 0; i < options.users.length; i++) {
            where.push({field: 'id', oper: '=', value: options.users[i]});
        }
        
        old_users = this.users.READ({
            fields: ['id', 'status'],
            where: [where]
        });
        
        for (i = 0; i < old_users.length; i++) {
            old_statuses[old_users[i].id] = old_users[i].status;
        }
        
        var result = [];
        for (i = 0; i < options.users.length; i++) {
            if(old_statuses.hasOwnProperty(options.users[i])) {
                if (old_statuses[options.users[i]] !== options.status) {
                    var output = this.users.UPDATE({
                        status: options.status,
                        id: options.users[i]
                    });
                    
                    result.push(output);
                } else {
                    result.push('no change');
                }
            } else {
                result.push('there is no user ' + options.users[i]);
            }
        }
        return result;
    } catch (e) {
        var error = 'Impossible to execute this.setStatus: ' + util.parseError(e);
        if (typeof this._doc == 'string') error += '\n' + this._doc;
        throw error;
    }
});

this.group = new tableLib.Table({
    component: "CORE",
    name: schema.default + '."CORE::GROUP"',
    fields:{
        id: new tableLib.AutoField({
            name: "ID",
			auto: schema.default + '."CORE::GROUP::ID".nextval',
			type: $.db.types.INTEGER
        }),
        name: new tableLib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 50
		}),
		description: new tableLib.Field({
			name: 'DESCRIPTION',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		id_leader: new tableLib.Field({
			name: 'LEADER_ID',
			type: $.db.types.INTEGER
		}),
		isActive: new tableLib.Field({
			name: 'IS_ACTIVE',
			type: $.db.types.TINYINT
		}),
        creationDate: new tableLib.AutoField({
			name: 'CREATION_DATE',
			auto: 'NOW()',
			update: false,
			type: $.db.types.TIMESTAMP
		}),
		creationUser: new tableLib.AutoField({
			name: 'CREATION_USER',
			auto: 'SESSION_USER',
			update: false,
			type: $.db.types.NVARCHAR,
			dimension: 100
		}),
		modificationDate: new tableLib.AutoField({
			name: 'MODIFICATION_DATE',
			auto: 'NOW()',
			type: $.db.types.TIMESTAMP
		}),
		modificationUser: new tableLib.AutoField({
			name: 'MODIFICATION_USER',
			auto: 'SESSION_USER',
			type: $.db.types.NVARCHAR,
			dimension: 100
		})
    }
});
this.groupUser = new tableLib.Table({
    component: "CORE",
    name: schema.default + '."CORE::GROUP_USER"',
    fields:{
        id: new tableLib.AutoField({
            name: "GROUP_ID",
			type: $.db.types.INTEGER
        }),
        id_group: new tableLib.Field({
			name: 'GROUP_ID',
			type: $.db.types.INTEGER
		}),
		id_user: new tableLib.Field({
			name: 'USER_ID',
			type: $.db.types.INTEGER
		})
    }
});
/*
Usage:
    resetPassword('TWAKA')
OR
    resetPassword()
*/
this.users.resetPassword = function (hana_username) {
    return SQL.__runUpdate__({
        query: "ALTER USER " + (hana_username || $.session.getUsername()) + " RESET CONNECT ATTEMPTS"
    });
};

/*
Usage:
    listHanaUsers()
Returns:
    [['USERONE'],['USERTWO']]
*/
this.users.listHanaUsers = function () {
    return SQL.SELECT({
        query: "SELECT USER_NAME FROM USERS WHERE USER_NAME NOT LIKE 'SYS%' AND USER_NAME NOT LIKE '_SYS%' ORDER BY USER_NAME"
    });
};

/*
Usage:
    getUser(id)
Returns:
    {name: ... }
    or
    undefined
*/
this.users.getUser = function (id) {
    var user = this.READ({
        where: [{field: 'id', value: Number(id), oper: '='}]
    });
    if(user && user.length > 0){
        return user[0];
    }else{
        return null;
    }
    
};
this.listUsers = function(optionsSub) {
	try {
		var options = {};
		if (!options.hasOwnProperty('where') || !util.isArray(options['where'])) {
			options.where = [];
		}
		if(optionsSub && optionsSub.whereOptions){
		    options.where = optionsSub.whereOptions;
		}
		if(optionsSub && optionsSub.where && optionsSub.where.length){ 
		    options.where.push(optionsSub.where)
		};
        
        if (optionsSub && optionsSub.page) {
            options.paginate = {
		        number: optionsSub.page || 1,
		        size: 15,
		        count: true
		    };
        }
        
		var allUsers = this.users.READ({
			order_by: ['id']
		});

		var indexed = {};
		for (var i = 0; i < allUsers.length; i++) {
			indexed[allUsers[i].id] = allUsers[i];
		};
		options.where.push({
			"field": "status",
			"value": 4,
			"oper": "!="
		});

		options.order_by = optionsSub && optionsSub.order ? optionsSub.order : ['id'];

		allUsers = this.users.READ(options);
        // for (var i = 0; i < allUsers.length; i++) {
		//     if (removeRoot.id == allUsers[i].id) {
		//         allUsers.splice(i, 1);
		//     }
		// } //Please, DO NOT do this, it throws an error when you're logged with TIMP user and try to list the users on MKT.

		for (var i = 0; i < allUsers.length; i++) {
			if (allUsers[i].hasOwnProperty('id_substituto') && indexed.hasOwnProperty(allUsers[i].id_substituto)) {
				var substituto = indexed[allUsers[i].id_substituto];
				allUsers[i].substituto = {
					'id': substituto.id,
					'hana_user': substituto.hana_user,
					'name': substituto.name,
					'email': substituto.email,
					'middle_name': substituto.middle_name,
					'last_name': substituto.last_name
				}
			}
			if (allUsers[i].hasOwnProperty('id_manager') && indexed.hasOwnProperty(allUsers[i].id_manager)) {
				var manager = indexed[allUsers[i].id_manager];
				allUsers[i].manager = {
					'id': manager.id,
					'hana_user': manager.hana_user,
					'name': manager.name,
					'email': manager.email,
					'middle_name': manager.middle_name,
					'last_name': manager.last_name
				}
			}
		}
        if (optionsSub && optionsSub.page) {
            return {
                data: allUsers,
                pages: allUsers.pageCount
            };
        }
		return allUsers;
	} catch (e) {
		$.messageCodes.push({
			"code": "MKT201102",
			"type": 'E',
			"errorInfo": util.parseError(e)
		});
		return null;
	}
};
