$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;
$.import('timp.core.server.orm', 'table');
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.objects = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Object"',
	default_fields: 'common',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Object::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		description: new table_lib.Field({
			name: 'DESCRIPTION',
			type: $.db.types.NVARCHAR,
			dimension: 500
		}),
		status: new table_lib.Field({
			name: 'STATUS',
			type: $.db.types.TINYINT,
			'default': 1,
			translate: {1:'Active', 2: 'Trash', 3: 'Deleted'}
		}),
		idObject: new table_lib.Field({
			name: 'ID_OBJECT',
			type: $.db.types.INTEGER
		}),
		idObjectType: new table_lib.Field({
			name: 'ID_OBJECTTYPE',
			type: $.db.types.INTEGER
		}),
		idComponent: new table_lib.Field({
			name: 'ID_COMPONENT',
			type: $.db.types.INTEGER
		}),
		idFolder: new table_lib.Field({
			name: 'ID_FOLDER',
			type: $.db.types.INTEGER
		}),
		shared:  new table_lib.Field({
            name: 'SHARED',
            type: $.db.types.TINYINT,
            'default': 0,
            translate:{0:'Private',1:'Shared',2:'Public'}
		})
	}
})

/*
SELECT 
    "TIMP"."CORE::Apps"."ID", "TIMP"."CORE::Components" ."ID",  
    "TIMP"."CORE::Apps"."NAME", "TIMP"."CORE::Components" ."NAME"
FROM 
    "TIMP"."CORE::Apps" 
INNER JOIN
    "TIMP"."CORE::Components" 
ON 
    "TIMP"."CORE::Apps"."COMPONENT_ID" = "TIMP"."CORE::Components"."ID";

INSERT INTO "TIMP"."CORE::ObjectType" VALUES (3, 'DFG Layout', 33);
*/
this.objectTypes = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::ObjectType"',
	default_fields: 'common',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::ObjectType::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		/*id_app: new table_lib.Field({
			name: 'ID_APP',
			type: $.db.types.INTEGER
		}),*/
// 		idComponent: new table_lib.Field({
// 			name: 'ID_COMPONENT',
// 			type: $.db.types.INTEGER
// 		})
        id_component: new table_lib.Field({
			name: 'ID_COMPONENT',
			type: $.db.types.INTEGER
		})
	}
})

// this.folders = new table_lib.Table({
//     component: 'CORE',
// 	name: schema.default + '."CORE::Folder"',
// 	default_fields: 'common',
// 	fields: {
// 		id: new table_lib.AutoField({
// 			name: 'ID',
// 			auto: schema.default + '."CORE::Folder::ID".nextval', 
// 			type: $.db.types.INTEGER,
// 			pk: true
// 		}),
// 		name: new table_lib.Field({
// 			name: 'NAME',
// 			type: $.db.types.NVARCHAR,
// 			dimension: 255
// 		}),
// 		description: new table_lib.Field({
// 			name: 'DESCRIPTION',
// 			type: $.db.types.NVARCHAR,
// 			dimension: 500
// 		}),
// 		status: new table_lib.Field({
// 			name: 'STATUS',
// 			type: $.db.types.TINYINT,
// 			'default': 1,
// 			translate: {1:'Active', 2: 'Trash', 3: 'Deleted'}
// 		}),
// 		idParent: new table_lib.Field({
// 			name: 'ID_PARENT',
// 			type: $.db.types.INTEGER
// 		}),
// 		type: new table_lib.Field({
// 			name: 'TYPE',
// 			type: $.db.types.TINYINT,
// 			'default': 0,
// 			translate:{0:'Private',1:'Shared',2:'System',3:'Standard'}
// 		})
// 	}
// })

this.objectsUserData  = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::ObjectUserData"',
	default_fields: 'common',
	fields: {
		id:new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::ObjectUserData::ID".nextval', 
			type: $.db.types.INTEGER
		}),
		idObject: new table_lib.Field({
			name: 'ID_OBJECT',
			type: $.db.types.INTEGER,
			pk: true
		}),

		idUser: new table_lib.Field({
			name: 'ID_USER',
			type: $.db.types.INTEGER,
			pk: true
		}),
		isFavorite: new table_lib.Field({
			name: 'IS_FAVORITE',
			type: $.db.types.TINYINT,
			translate:{0:false,1:true}
		})
	}	
})

this.objectShare = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::ObjectShare"',
	default_fields: 'common',
	fields: {
        id:new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::ObjectShare::ID".nextval', 
			type: $.db.types.INTEGER
		}),
		idObject: new table_lib.Field({
			name: 'ID_OBJECT',
			type: $.db.types.INTEGER,
			pk: true
		}),
		idUser: new table_lib.Field({
            name: 'ID_USER',
            type: $.db.types.INTEGER,
            pk: true,
            'default': -1
		}),
		idGroup: new table_lib.Field({
            name: 'ID_GROUP',
            type: $.db.types.INTEGER,
            pk: true,
            'default':-1
		}),
		isFolder: new table_lib.Field({
            name: 'IS_FOLDER',
            type: $.db.types.TINYINT,
            pk: true,
            'default': 0,
			translate:{0:false,1:true}
		})
	}
})

this.objects.listObject = function(){
    
}
try{
    $.import('timp.core.server.models.tables', 'object');
    this.objectTypeBaseModel = $.timp.core.server.models.tables.object.objectType;
}catch(e){
    this.objectTypeBaseModel = {};
}