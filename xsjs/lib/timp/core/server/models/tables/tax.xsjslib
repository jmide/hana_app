$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.taxModel = new BaseModel({
    name: 'CORE::TAX',
	schema: schema.application.schema.slice(1,-1),
	identity: 'TAX',
	component: 'CORE',
	type: 'table',
	fields: {
		code: {
		    columnName: 'CODE',
			type: 'string',
			size: 128,
			primaryKey: true
		},
		description: {
		    columnName: 'DESCRIPTION',
			type: 'string',
			size: 255
		}
	}
});

this.taxTypeModel = new BaseModel({
    name: 'CORE::TAX_TYPE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'TAX_TYPE',
	component: 'CORE',
	type: 'table',
	fields: {
		code: {
		    columnName: 'CODE',
			type: 'string',
			size: 128,
			primaryKey: true
		},
		description: {
		    columnName: 'DESCRIPTION',
			type: 'string',
			size: 255
		},
		coverage: {
		    columnName: 'COVERAGE',
			type: 'string',
			size: 128
		},
		operationMode: {
		    columnName: 'OPERATION_MODE',
			type: 'string',
			size: 128
		}
	}
});

this.taxMapModel = new BaseModel({
    name: 'CORE::TAX_MAP',
	schema: schema.application.schema.slice(1,-1),
	identity: 'TAX_MAP',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        taxOrigin: {
		    columnName: 'TAX_ORIGIN',
			type: 'string',
			size: 128
		},
        taxTypeCode: {
		    columnName: 'TAX_TYPE_CODE',
			type: 'string',
			size: 128,
			index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		},
        taxCodeERP: {
		    columnName: 'TAX_CODE_ERP',
			type: 'string',
			size: 128
		},
        taxDescription: {
		    columnName: 'TAX_DESCRIPTION',
			type: 'string',
			size: 255
		},
		taxTypeDescription: {
		    columnName: 'TAX_TYPE_DESCRIPTION',
			type: 'string',
			size: 128
		}
	}
});