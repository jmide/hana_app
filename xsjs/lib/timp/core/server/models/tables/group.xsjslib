$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

var groupModel = new BaseModel({
	'name': 'CORE::GROUP',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'GROUP',
	'component': 'CORE',
	'defaultFields': 'complete',
	'type': 'table',
	'fields': {
		name: {
			columnName: 'NAME',
			type: 'string',
			size: 50,
			required: true
		},
		description: {
			columnName: 'DESCRIPTION',
			type: 'string',
			size: 255
		},
		leaderId: { // Foreign key
			columnName: 'LEADER_ID',
			type: 'integer'
		},
		isActive: {
			columnName: 'IS_ACTIVE',
			type: 'tinyint',
			required: true,
			default: 1,
			translate: {
				'1': true,
				'0': false
			}
		},
		grcRole: {
		    columnName: 'GRC_ROLE',
			type: 'string',
			size: 100
		}
	}
});

this.groupModel = groupModel;

var groupUserModel = new BaseModel({
	'name': 'CORE::GROUP_USER',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'GROUP_USER',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		groupId: {
			columnName: 'GROUP_ID',
			type: 'integer',
			primaryKey: true
		},
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.groupUserModel = groupUserModel;

var groupPrivilegeModel = new BaseModel({
	'name': 'CORE::GROUP_PRIVILEGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'GROUP_PRIVILEGE',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		groupId: {
			columnName: 'GROUP_ID',
			type: 'integer',
			primaryKey: true
		},
		privilegeId: {
			columnName: 'PRIVILEGE_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.groupPrivilegeModel = groupPrivilegeModel;

var groupPackageModel = new BaseModel({
	'name': 'CORE::GROUP_PACKAGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'GROUP_PACKAGE',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		groupId: {
			columnName: 'GROUP_ID',
			type: 'integer',
			primaryKey: true
		},
		packageId: {
			columnName: 'PACKAGE_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.groupPackageModel = groupPackageModel;

var groupOrgPrivilegeModel = new BaseModel({
	'name': 'CORE::GROUP_ORG_PRIVILEGES',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'GROUP_ORG_PRIVILEGES',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		groupId: {
			columnName: 'GROUP_ID',
			type: 'integer',
			primaryKey: true
		},
		companyId: {
			columnName: 'COMPANY_ID',
			type: 'string',
			size: 4,
			primaryKey: true
		},
		branchId: {
			columnName: 'BRANCH_ID',
			type: 'string',
			size: 4,
			primaryKey: true
		}
	}
});

this.groupOrgPrivilegeModel = groupOrgPrivilegeModel;

this.getGroupsList = function () {
	let groupList =  groupModel.find({
	    where: [{ 
            "field": 'isActive',
            "operator": '$eq',
            "value": 1
        }],
	    xselect: [{
			field: 'creationUser'
		}, {
			field: 'modificationUser'
		}, {
			field: 'creationDate'
		}, {
			field: 'modificationDate'
		}]
	}).results;
	return groupList;
};