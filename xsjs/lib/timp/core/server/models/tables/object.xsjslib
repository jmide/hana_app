$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.objectType = new BaseModel({
    name: 'CORE::ObjectType',
    schema: $.schema.slice(1,-1),
    identity: 'ObjectType',
    component: 'CORE',
    type: 'table',
    fields: {
        ID: {
            name: 'ID',
            autoIncrement: true,
            type: 'integer',
            pk: true
        },
        NAME: {
            name: 'NAME',
            type: 'string',
            size: 255
        },
        ID_COMPONENT: {
            name: 'ID_COMPONENT',
            type: 'integer'
        },
        'CREATION.ID_USER': {
            name: 'CREATION.ID_USER',
            type: 'integer',
            "default": 'CURRENT_USER'
        },
        'CREATION.DATE': {
            name: 'CREATION.DATE',
            type: 'datetime',
            "default": "CURRENT_TIMESTAMP"
        },
        'MODIFICATION.ID_USER': {
            name: 'MODIFICATION.ID_USER',
            type: 'integer',
            "default": 'CURRENT_USER',
            onUpdate: 'CURRENT_USER'
        },
        'MODIFICATION.DATE': {
            name: 'MODIFICATION.DATE',
            type: 'datetime',
            "default": "CURRENT_TIMESTAMP",
            onUpdate: "CURRENT_TIMESTAMP"
        }
    }
});