$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;


this.configurationModel = new BaseModel({
    name: 'CORE::CONFIGURATION',
    schema: schema.application.schema.slice(1, -1),
    identity: 'CONFIGURATION',
    component: 'CORE',
    type: 'table',
    fields: {
        key: {
            columnName: 'KEY',
            type: 'string',
            size: 128,
            primaryKey: true
        },
        componentId: {
            columnName: 'COMPONENT_ID',
            type: 'integer',
            primaryKey: true
        },
        value: {
            columnName: 'VALUE',
            type: 'string',
            size: 255
        }
    }
});

this.configurationModel.schema = {
    properties: {
        componentName: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        key: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        value: {
            required: true,
            type: 'any'
        }
    }
};

this.alertDeleteLogModel = new BaseModel({
    name: 'CORE::ALERT_DELETE_LOG',
    schema: schema.application.schema.slice(1, -1),
    identity: 'ALERT_DELETE_LOG',
    component: 'CORE',
    type: 'table',
    fields: {
        id: {
            columnName: 'ID',
            type: 'integer',
            primaryKey: true
        }
    }
});

this.alertDeleteFilesModel = new BaseModel({
    name: 'CORE::ALERT_DELETE_FILES',
    schema: schema.application.schema.slice(1, -1),
    identity: 'ALERT_DELETE_FILES',
    component: 'CORE',
    type: 'table',
    fields: {
        id: {
            columnName: 'ID',
            type: 'integer',
            primaryKey: true
        }
    }
});

this.licenseModel = new BaseModel({
    name: 'CORE::LICENSE',
    schema: schema.application.schema.slice(1, -1),
    identity: 'LICENSE',
    component: 'CORE',
    type: 'table',
    fields: {
        id: {
            columnName: 'ID',
            type: 'integer',
            primaryKey: true,
            autoIncrement: true
        },
        license: {
            columnName: 'LICENSE',
            type: 'string',
            required: true,
            unique: true,
            size: 512
        },
        availableUsers: {
            columnName: 'AVAILABLE_USERS',
            type: 'integer',
            required: true
        },
        expiration: {
            columnName: 'EXPIRATION',
            type: 'string',
            required: true,
            size: 64
        },
        licenseType: {
            columnName: 'LICENSE_TYPE',
            type: 'tinyint',
            required: true,
            translate: {
                '1': '01', //Tribute activation customer
                '2': '02'  //Total activation customer
            },
            default: 2
        },
        isActive: {
            columnName: 'IS_ACTIVE',
            type: 'tinyint',
            required: true,
            translate: {
                '1': true,
                '0': false
            },
            default: 1
        }
    }
});

this.licenseModel.schema = {
    properties: {
        id: {
            type: 'integer'
        },
        license: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        userQuantity: {
            required: true,
            type: 'integer'
        },
        expiration: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        licenseType: {
            required: true,
            type: 'string'
        },
        isActive: {
            required: true,
            type: 'boolean'
        },
        taxLicenses: {
            required: false,
            type: 'array'
        }
    }
};

this.fullDatesModel = new BaseModel({
    name: 'CORE::FULLDATES',
    schema: schema.application.schema.slice(1, -1),
    identity: 'FULLDATES',
    component: 'CORE',
    type: 'table',
    fields: {
        fullDate: {
            columnName: 'FULLDATE',
            type: 'string',
            size: 8,
            required: true
        },
        year: {
            columnName: 'YEAR',
            type: 'string',
            size: 4,
            required: true
        }
    }
});

this.taxLicenseModel = new BaseModel({
    name: 'CORE::TAX_LICENSE',
    schema: schema.application.schema.slice(1, -1),
    identity: 'TAX_LICENSE',
    component: 'CORE',
    type: 'table',
    fields: {
        id: {
            columnName: 'ID',
            type: 'integer',
            primaryKey: true,
            autoIncrement: true,
            comment: 'ID'
        },
        idLicense: {
            columnName: 'ID_LICENSE',
            type: 'integer',
            required: true,
            comment: 'ID license'
        },
        idTax: {
            columnName: 'ID_TAX',
            type: 'integer',
            required: true,
            comment: 'ID tributo'
        },
        license: {
            columnName: 'LICENSE',
            type: 'string',
            required: true,
            unique: true,
            size: 512,
            comment: 'Licença'
        },
        isActive: {
            columnName: 'IS_ACTIVE',
            type: 'tinyint',
            required: true,
            translate: {
                '0': false,
                '1': true
            },
            default: 1
        }
    }
});

this.taxLicenseModel.schema = {
    properties: {
        id: {
            type: 'integer'
        },
        idTax: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        license: {
            required: true,
            type: 'string',
            allowEmpty: false
        },
        isActive: {
            required: true,
            type: 'boolean'
        }
    }
};