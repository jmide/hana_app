$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.table = new BaseModel({
    name: 'CORE::COMPONENT_MANUAL',
	schema: schema.application.schema.slice(1,-1),
	identity: 'COMPONENT_MANUAL',
	component: 'CORE',
	type: 'table',
	defaultFields: ["complete"],
	fields: {
		componentName: {
		    columnName: 'COMPONENT_NAME',
			type: 'string',
			size: 128
		},
		attachmentId: {
		    columnName: 'ATTACHMENT_ID',
		    type: 'integer'
		},
		language: {
		    columnName: 'LANGUAGE',
			type: 'string',
			size: 6
		},
		effectiveDateFrom: {
		    columnName: 'EFFECTIVE_DATE_FROM',
		    type: 'date'
		},
		effectiveDateTo: {
		    columnName: 'EFFECTIVE_DATE_TO',
		    type: 'date'
		},
		isDeleted: {
		    columnName: 'IS_DELETED',
		    type: 'integer',
		    "default": 0
		}
	}
});