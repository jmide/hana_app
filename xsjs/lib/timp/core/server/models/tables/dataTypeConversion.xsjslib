$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.dataTypeConversionModel = new util.BaseModel({
	name: 'CORE::DATA_TYPE_CONVERSION',
	schema: schema.application.schema.slice(1,-1),
	identity: 'DATA_TYPE_CONVERSION',
	component: 'CORE',
	type: 'table',
	fields: {
		'id': {
			'type': 'integer',
			'primaryKey': true,
			'autoIncrement': true,
			'columnName': 'ID'
		},
		'target': {
			'type': 'string',
			'size': 16,
			'columnName': 'TARGET',
			'required': true
		},
		'targetTypeId': {
			'type': 'integer',
			'columnName': 'TARGET_TYPE_ID',
			'required': true
		},
		'targetSQLDataType': {
			'type': 'integer',
			'columnName': 'TARGET_SQL_DATA_TYPE',
			'required': true
		},
        'source': {
			'type': 'string',
			'size': 16,
			'columnName': 'SOURCE',
			'required': true
		},
		'sourceTypeId': {
			'type': 'integer',
			'columnName': 'SOURCE_TYPE_ID',
			'required': true
		},
		'sourceSQLDataType': {
			'type': 'integer',
			'columnName': 'SOURCE_SQL_DATA_TYPE',
			'required': true
		}
	}
});