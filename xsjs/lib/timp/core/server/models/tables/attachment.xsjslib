$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.imageModel = new BaseModel({
    name: 'CORE::IMAGE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'IMAGE',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		name: {
		    columnName: 'NAME',
			type: 'string',
			size: 128
		},
		description: {
		    columnName: 'DESCRIPTION',
			type: 'string',
			size: 255
		},
		image: {
			columnName: 'IMAGE',
			type: 'file'
		}
	}
});

this.attachmentModel = new BaseModel({
    name: 'CORE::ATTACHMENT',
	schema: schema.application.schema.slice(1,-1),
	identity: 'ATTACHMENT',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		name: {
		    columnName: 'NAME',
			type: 'string',
			size: 128
		},
		description: {
		    columnName: 'DESCRIPTION',
			type: 'string',
			size: 255
		},
		mimeType: {
		    columnName: 'MIME_TYPE',
			type: 'string',
			size: 128
		},
		componentId: {
		    columnName: 'COMPONENT_ID',
			type: 'integer'
		},
		file: {
			columnName: 'FILE',
			type: 'file'
		}
	}
});