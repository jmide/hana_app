$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

var userModel = new BaseModel({
	'name': 'CORE::USER',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'USER',
	'component': 'CORE',
	'defaultFields': 'complete',
	'type': 'table',
	'fields': {
		name: {
			columnName: 'NAME',
			type: 'string',
			size: 50,
			required: true
		}, 
		lastName: {
			columnName: 'LAST_NAME',
			type: 'string',
			size: 50, 
			required: true
		},
		hanaUser: {
			columnName: 'HANA_USER',
			type: 'string',
			size: 32,
			required: true,
			unique: true
		}, 
		email: {
			columnName: 'EMAIL',
			type: 'string',
			size: 100
		},
		position: {
			columnName: 'POSITION',
			type: 'string',
			size: 255
		}, 
		isActive: {
			columnName: 'IS_ACTIVE',
			type: 'tinyint',
			required: true,
			default: 1,
			translate: {
				'1': true,
				'0': false
			}
		},
		appPositions: {
			columnName: 'APP_POSITIONS',
			type: 'json'
		},
		isAdmin: {
			columnName: 'IS_ADMIN',
			type: 'tinyint',
			required: true,
			default: 0,
			translate: {
				'1': true,
				'0': false
			}
		}
	}
});

this.userModel = userModel;

var userPrivilegeModel = new BaseModel({
	'name': 'CORE::USER_PRIVILEGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'USER_PRIVILEGE',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		},
		privilegeId: {
			columnName: 'PRIVILEGE_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.userPrivilegeModel = userPrivilegeModel;

var userOrgPrivilegeModel = new BaseModel({
	'name': 'CORE::USER_ORG_PRIVILEGES',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'USER_ORG_PRIVILEGES',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		},
		companyId: {
			columnName: 'COMPANY_ID',
			type: 'string',
			size: 4,
			primaryKey: true
		},
		branchId: {
			columnName: 'BRANCH_ID',
			type: 'string',
			size: 4,
			primaryKey: true
		}
	}
});

this.userOrgPrivilegeModel = userOrgPrivilegeModel;

var userPackageModel = new BaseModel({
	'name': 'CORE::USER_PACKAGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'USER_PACKAGE',
	'component': 'CORE',
	'type': 'table',
	'fields': {
		userId: {
			columnName: 'USER_ID',
			type: 'integer',
			primaryKey: true
		},
		packageId: {
			columnName: 'PACKAGE_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.userPackageModel = userPackageModel;

this.getUsersList = function () {
    let results = userModel.find({
		'select': [{
			field: 'id'
	            }, {
			field: 'name'
	            }, {
			field: 'lastName'
            	}, {
			field: 'hanaUser'
        }]
	}).results;
	return results;
}