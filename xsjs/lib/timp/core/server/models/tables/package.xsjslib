$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

var packageModel = new BaseModel({
    'name': 'CORE::PACKAGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'PACKAGE',
	'component': 'CORE',
	'defaultFields': 'complete',
	'type': 'table',
	'fields': {
	    name: {
			columnName: 'NAME',
			type: 'string',
			size: 50,
			required: true
		}, 
		description: {
			columnName: 'DESCRIPTION',
			type: 'string',
			size: 100
		}
	} 
});

this.packageModel = packageModel;

var packagePrivilegeModel = new BaseModel({
	'name': 'CORE::PACKAGE_PRIVILEGE',
	'schema': schema.application.schema.slice(1,-1),
	'identity': 'PACKAGE_PRIVILEGE',
	'component': 'CORE',
	'type': 'table',
	'fields':{
		privilegeId: {
			columnName: 'PRIVILEGE_ID',
			type: 'integer',
			primaryKey: true
		},
		packageId: {
			columnName: 'PACKAGE_ID',
			type: 'integer',
			primaryKey: true
		}
	}
});

this.packagePrivilegeModel = packagePrivilegeModel;

this.getPackagesList = function () {
	let packageList = packageModel.find({
		'xselect': [{
			field: 'creationUser'
		}, {
			field: 'modificationUser'
		}, {
			field: 'creationDate'
		}, {
			field: 'modificationDate'
		}]
	}).results;
	return packageList;
};