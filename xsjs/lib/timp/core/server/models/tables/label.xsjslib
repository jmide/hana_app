$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.labelModel = new BaseModel({
    name: 'CORE::LABEL',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LABEL',
	component: 'CORE',
	type: 'table',
	fields: {
		key: {
		    columnName: 'KEY',
			type: 'string',
			size: 50,
			primaryKey: true
		},
		componentId: {
		    columnName: 'COMPONENT_ID',
			type: 'integer',
			primaryKey: true
		},
		lang: {
		    columnName: 'LANG',
			type: 'string',
			size: 8,
			primaryKey: true
		},
		value: {
		    columnName: 'VALUE',
			type: 'string',
			size: 50,
			primaryKey: true
		},
		text: {
		    columnName: 'TEXT',
			type: 'string',
			size: 255
		}
	}
});