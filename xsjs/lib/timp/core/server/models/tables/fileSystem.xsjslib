$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.objectTypeModel = new BaseModel({
	name: 'CORE::OBJECT_TYPE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'OBJECT_TYPE',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
			type: 'integer',
			columnName: 'ID',
			primaryKey: true,
			autoIncrement: true
		},
		componentId: {
			type: 'integer',
			columnName: 'COMPONENT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			} 
		},
		name: {
			type: 'string',
			size: 128,
			columnName: 'NAME',
			required: true,
			unique: true
		}
	}
});

this.folderModel = new BaseModel({
	name: 'CORE::FOLDER',
	schema: schema.application.schema.slice(1,-1),
	identity: 'FOLDER',
	component: 'CORE',
	defaultFields: 'complete',
	type: 'table',
	fields: {
		parentId: {
			type: 'integer',
			columnName: 'PARENT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		componentId: {
		    type: "integer",
		    columnName: "COMPONENT_ID",
		    index: {
		        type: "default",
		        globalIndexOrder: "ASC"
		    }
		},
		tabId: {
			type: 'integer',
			columnName: 'TAB_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		name: {
			type: 'string',
			size: 128,
			columnName: 'NAME'
		},
		description: {
			type: 'string',
			size: 255,
			columnName: 'DESCRIPTION'
		},
		status: {
			type: 'tinyint',
			columnName: 'STATUS',
			translate: {
				1: 'Active',
				2: 'Trash',
				3: 'Deleted'
			},
			default: 1
		}
	}
});

this.folderShareModel = new BaseModel({
	name: 'CORE::FOLDER_SHARE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'FOLDER_SHARE',
	component: 'CORE',
	type: 'table',
	fields: {
		folderId: {
			type: 'integer',
			columnName: 'FOLDER_ID',
			primaryKey: true
		},
		userId: {
			type: 'integer',
			columnName: 'USER_ID',
			primaryKey: true
		},
		groupId: {
			type: 'integer',
			columnName: 'GROUP_ID',
			primaryKey: true
		}
	}
});

this.fileModel = new BaseModel({
	name: 'CORE::FILE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'FILE',
	component: 'CORE',
	defaultFields: 'complete',
	type: 'table',
	fields: {
		folderId: {
			type: 'integer',
			columnName: 'FOLDER_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bcbHierarchyConfigurationId: {
			type: 'integer',
			columnName: 'BCB_HIERARCHYCONFIGURATION_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bcbBuilderConfigurationId: {
			type: 'integer',
			columnName: 'BCB_BUILDERCONFIGURATION_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bcbBuilderExecutionVersionId: {
			type: 'integer',
			columnName: 'BCB_BUILDEREXECUTIONVERSION_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bfbLayoutId: {
			type: 'integer',
			columnName: 'BFB_LAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bfbFormId: {
			type: 'integer',
			columnName: 'BFB_FORM_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bfbSettingId: {
			type: 'integer',
			columnName: 'BFB_SETTING_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		brbReportId: {
		    type: 'integer',
		    columnName: 'BRB_REPORT_ID',
		    index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		brbVariantId: {
		    type: 'integer',
		    columnName: 'BRB_VARIANT_ID',
		    index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		dfgLayoutId: {
			type: 'integer',
			columnName: 'DFG_LAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		dfgDigitalFileId: {
			type: 'integer',
			columnName: 'DFG_DIGITALFILE_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		dfgSettingId: {
			type: 'integer',
			columnName: 'DFG_SETTING_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		dfgAn3Id: {
			type: 'integer',
			columnName: 'DFG_AN3_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		dfgAn4Id: {
			type: 'integer',
			columnName: 'DFG_AN4_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tdkLayoutId: {
			type: 'integer',
			columnName: 'TDK_LAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tdkDashboardLayoutId: {
			type: 'integer',
			columnName: 'TDK_DASHBOARDLAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tdkExecutionId: {
			type: 'integer',
			columnName: 'TDK_EXECUTION_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tdkKpiLayoutId: {
			type: 'integer',
			columnName: 'TDK_KPILAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tfbLayoutId: {
			type: 'integer',
			columnName: 'TFB_LAYOUT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tfbSettingId: {
			type: 'integer',
			columnName: 'TFB_SETTING_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		tfbBookId: {
			type: 'integer',
			columnName: 'TFB_BOOK_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		status: {
			type: 'tinyint',
			columnName: 'STATUS',
			translate: {
				0: 'Standard',
				1: 'Active',
				2: 'Trash',
				3: 'Deleted',
				4: 'Public'
			},
			default: 1
		}
	}
});

this.fileShareModel = new BaseModel({
	name: 'CORE::FILE_SHARE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'FILE_SHARE',
	component: 'CORE',
	type: 'table',
	fields: {
		fileId: {
			type: 'integer',
			columnName: 'FILE_ID',
			primaryKey: true
		},
		userId: {
			type: 'integer',
			columnName: 'USER_ID',
			primaryKey: true
		},
		groupId: {
			type: 'integer',
			columnName: 'GROUP_ID',
			primaryKey: true
		}
	}
});

this.fileFavoriteModel = new BaseModel({
	name: 'CORE::FILE_FAVORITE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'FILE_FAVORITE',
	component: 'CORE',
	type: 'table',
	fields: {
		fileId: {
			type: 'integer',
			columnName: 'FILE_ID',
			primaryKey: true
		},
		userId: {
			type: 'integer',
			columnName: 'USER_ID',
			primaryKey: true
		}
	}
});