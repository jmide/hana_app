$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.componentModel = new BaseModel({
    name: 'CORE::COMPONENT',
	schema: schema.application.schema.slice(1,-1),
	identity: 'COMPONENT',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		name: {
		    columnName: 'NAME',
			type: 'string',
			size: 128
		},
		labelEnus: {
		    columnName: 'LABEL_ENUS',
			type: 'string',
			size: 255
		},
		labelPtbr: {
		    columnName: 'LABEL_PTBR',
			type: 'string',
			size: 255
		},
		version: {
		    columnName: 'VERSION',
			type: 'string',
			size: 10
		}
	}
});

this.internationalizationModel = new BaseModel({
    name: 'CORE::INTERNATIONALIZATION',
	schema: schema.application.schema.slice(1,-1),
	identity: 'INTERNATIONALIZATION',
	component: 'CORE',
	type: 'table',
	fields: {
		code: {
		    columnName: 'CODE',
			type: 'string',
			size: 32,
		    primaryKey: true
		},
		lang: {
		    columnName: 'LANG',
			type: 'string',
			size: 8,
			primaryKey: true
		},
		text: {
		    columnName: 'TEXT',
			type: 'string',
			size: 255,
			required: true
		},
		componentId: {
			type: 'integer',
		    columnName: 'COMPONENT_ID',
            index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		}
	}
});

this.privilegeModel = new BaseModel({
    name: 'CORE::PRIVILEGE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'PRIVILEGE',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		componentId: {
		    columnName: 'COMPONENT_ID',
		    type: 'integer',
		    index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		},
		name: {
		    columnName: 'NAME',
		    type: 'string',
		    required: true,
		    size: 64
		},
		descriptionEnus: {
		    columnName: 'DESCRIPTION_ENUS',
		    type: 'string', 
		    required: true,
		    size: 255
		},
		descriptionPtbr: {
		    columnName: 'DESCRIPTION_PTBR',
		    type: 'string',
		    required: true,
		    size: 255
		},
		role: { 
		    columnName: 'ROLE',
		    type: 'string',
		    required: true,
		    unique: true,
		    size: 255
		}
	}
});