$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.logModel = new BaseModel({
    name: 'CORE::LOG',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		componentId: {
		    columnName: 'COMPONENT_ID',
			type: 'integer',
			index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		},
		userId: {
		    columnName: 'USER_ID',
			type: 'integer',
			index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		},
		internationalizationCode: {
		    columnName: 'INTERNATIONALIZATION_CODE',
			type: 'string',
			size: 32
		},
		date: {
		    columnName: 'DATE',
		    type: 'datetime',
		    "default": "CURRENT_TIMESTAMP"
		},
		objectId: {
		    columnName: 'OBJECT_ID',
			type: 'integer'
		},
		type: {
		    columnName: 'TYPE',
		    type: 'tinyint',
		    translate: {
		        '0': 'S',
		        '1': 'E'
		    }
		},
		trace: {
		    columnName: 'TRACE',
		    type: 'text'
		},
		encryptedTrace: {
		    columnName: 'ENCRYPTED_TRACE',
		    type: 'text'
		}
	}
});


this.logDetails = new BaseModel({
    name: 'CORE::LOG_DETAILS',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_DETAILS',
	component: 'CORE',
	type: 'table',
	fields: {
		idDetail: {
		    columnName: 'ID_DETAILID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		idHeader: {
		    columnName: 'ID_HEADER',
			type: 'integer'
			
		},
		codLog: {
		    columnName: 'INTERNATIONALIZATION_CODE',
			type: 'string',
			size: 32
		},
		
		oldValue: {
		    columnName: 'OLD_VALUE',
			type: 'string',
			size: 200
		},
		newValue: {
		    columnName: 'NEW_VALUE',
			type: 'string',
			size: 200
		},
		field: {
		    columnName: 'FIELD',
		    type: 'string',
		    size: 120
		}
	}
});

this.logTCMDetails = new BaseModel({
    name: 'CORE::LOG_TCM_IMPORT_EXPORT_DETAILS',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TCM_IMPORT_EXPORT_DETAILS',
	component: 'CORE',
	type: 'table',
	fields: {
		idDetail: {
		    columnName: 'ID_DETAILID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		idHeader: {
		    columnName: 'ID_HEADER',
			type: 'integer'
		},
		codLog: {
		    columnName: 'INTERNATIONALIZATION_CODE',
			type: 'string',
			size: 32
		},
		component:{
		    columnName: 'COMPONENT',
			type: 'string',
			size: 10
		},
		model:{
		    columnName: 'MODEL',
			type: 'string',
			size: 80
		},
		globalUUID:{
		    columnName: 'GLOBAL_UUID',
			type: 'string',
			size: 32
		},
		originObjectId:{
		    columnName: 'ORIGIN_OBJECT',
			type: 'integer'
		}
	}
});

// Componente, modelo, globalUUID, objetoorigen
