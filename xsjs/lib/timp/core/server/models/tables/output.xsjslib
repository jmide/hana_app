$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.outputModel = new BaseModel({
    name: 'CORE::OUTPUT',
	schema: schema.application.schema.slice(1,-1),
	identity: 'OUTPUT',
	component: 'CORE',
	defaultFields: 'complete',
	type: 'table',
	fields: {
	    subPeriodId: {
			type: 'integer',
		    columnName: 'SUB_PERIOD_ID',
            index: {
                type: 'default',
                globalIndexOrder: 'ASC'
            }
		},
		name: {
			type: 'string',
			size: 128,
		    columnName: 'NAME',
		    required: true
		},
		description: {
			type: 'string',
			size: 255,
		    columnName: 'DESCRIPTION'
		},
		value: {
			type: 'text',
		    columnName: 'VALUE'
		},
		json: {
			type: 'json',
		    columnName: 'JSON'
		},
		brbReportId: {
			type: 'integer',
			columnName: 'BRB_REPORT_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bcbConfigurationId: {
			type: 'integer',
			columnName: 'BCB_CONFIGURATION_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		bfbFormId: {
			type: 'integer',
			columnName: 'BFB_FORM_ID',
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		originalId: {
		    type: 'integer',
			columnName: 'ORIGINAL_ID'
		}
	}
});

this.outputModel.schema = {
	properties: {
		name: {
			required: true,
			type: 'string',
			allowEmpty: false,
			maxLength: 128
		},
		description: {
			type: 'string',
			allowEmpty: true,
			maxLength: 255
		}, 
		value: {
			types: ['integer', 'string', 'array', 'null', 'object']
		},
		json: {
			type: ['null', 'object']
		},
		subPeriodId: {
			type: 'integer',
			minimum: 1
		},
		componentId: {
		    type: 'integer',
		    minimum: 1
		}
	}
};