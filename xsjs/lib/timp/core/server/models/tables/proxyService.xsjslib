$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.adapterModel = new util.BaseModel({
	name: 'CORE::ADAPTER',
	schema: schema.application.schema.slice(1,-1),
	identity: 'ADAPTER',
	component: 'CORE',
	type: 'table',
	defaultFields: 'complete',
	fields: {
		type: {
			type: 'tinyint',
			columnName: 'TYPE',
			translate: {
				1: 'ECC',
				2: 'TDF'
			},
			default: 2,
			required: true
		},
		name: {
			type: 'string',
			columnName: 'NAME',
			required: true,
			size: 50
		},
		hostName: {
			type: 'string',
			columnName: 'HOST_NAME',
			required: true,
			size: 64
		},
		port: {
			type: 'integer',
			columnName: 'PORT',
			required: true
		},
		userName: {
			type: 'string',
			columnName: 'USER_NAME',
			size: 32,
			required: true
		},
		password: {
			type: 'string',
			columnName: 'PASSWORD',
			required: true,
			size: 128
		},
		client: {
			type: 'integer',
			columnName: 'CLIENT',
			required: true
		},
		isActive: {
			type: 'tinyint',
			columnName: 'IS_ACTIVE',
			required: true,
			default: 0,
			translate: {
				'1': true,
				'0': false
			}
		},
		isDefault: {
			type: 'tinyint',
			columnName: 'IS_DEFAULT',
			default: 0,
			translate: {
				'1': true,
				'0': false
			}
		}
	}
});

this.serviceModel = new util.BaseModel({
	name: 'CORE::SERVICE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'SERVICE',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
			columnName: 'ID',
			type: 'integer',
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			columnName: 'NAME',
			type: 'string',
			size: 100
		},
		descriptionPtbr: {
			columnName: 'DESCRIPTION_PTBR',
			type: 'string',
			size: 500
		},
		descriptionEnus: {
			columnName: 'DESCRIPTION_ENUS',
			type: 'string',
			size: 500
		},
		type: {
			type: 'tinyint',
			columnName: 'TYPE',
			translate: {
				1: 'ECC',
				2: 'TDF'
			},
			default: 2,
			required: true
		},
		modificationUser: {
			type: 'integer',
			columnName: 'MODIFICATION_USER',
			default: 'CURRENT_USER',
			onUpdate: 'CURRENT_USER'
		},
		modificationDate: {
			type: 'datetime',
			columnName: 'MODIFICATION_DATE',
			default: 'CURRENT_TIMESTAMP',
			onUpdate: 'CURRENT_TIMESTAMP'
		}
	}
});

this.serviceDestinationModel = new util.BaseModel({
	name: 'CORE::SERVICE_DESTINATION',
	schema: schema.application.schema.slice(1,-1),
	identity: 'SERVICE_DESTINATION',
	component: 'CORE',
	type: 'table',
	fields: {
		id: {
			columnName: 'ID',
			type: 'integer',
			autoIncrement: true,
			unique: true
		},
		serviceId: {
			columnName: 'SERVICE_ID',
			type: 'integer',
			primaryKey: true,
			index: {
				type: 'default',
				globalIndexOrder: 'ASC'
			}
		},
		destinationPath: { //erase?
			columnName: 'DESTINATION_PATH',
			type: 'string',
			size: 512
		},
		destination: {
			columnName: 'DESTINATION',
			type: 'string',
			primaryKey: true,
			size: 512
		},
		adapterId: {
			columnName: 'ADAPTER_ID',
			type: 'integer',
			primaryKey: true
		},
		isHttpsEnabled: {
			type: 'tinyint',
			columnName: 'IS_HTTPS_ENABLED',
			default: 0,
			translate: {
				'1': true,
				'0': false
			}
		},
		isDefault: {
			type: 'tinyint',
			columnName: 'IS_DEFAULT',
			default: 0,
			translate: {
				'1': true,
				'0': false
			}
		}
	}
});