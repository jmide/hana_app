$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

const table = new BaseModel({
	'schema': $.getSchema(),
	'name': 'CORE::USER_TAXES',
	'identity': 'USER_TAXES',
	'component': 'CORE',
	'defaultFields': 'log',
	'type': 'table',
	'fields': {
		user: {
			columnName: 'USER',
			type: 'integer',
			size: 25,
			required: true
		},
		tax: {
			columnName: 'TAX',
			type: 'string',
			size: 10
		}
	}
});

this.getTableModel = function () {
    return table;  
};

this.getUserTaxesById = function (object) {
    let {id} = object;
    let response = table.find({
        select: [
            {
                field: 'user'
            }, 
            {
                field: 'tax'
            }
        ],
        where: [
            {
                field: 'user',
                operator: '$eq',
                value: id
            }
        ]
    });
    return response.results;
};

this.delete = function (option) {
    let {isValid, errors} = validate.delete(option);
    if(!isValid){
        return {
            results: {
                deleted: false
            },
            errors
        };
    }
    let result = table.delete(option);
    return result;
};

this.batchCreate = function (options) { 
    let {isValid, errors} = validate.createOptions(options);
    if(!isValid){
        return {
            results: {
                created: false
            },
            errors
        };
    }
    let result = table.batchCreate(options);
    return result;
};

const validate = {
    delete: function (option) {
        let {valid, errors} = $.validator.validate(option, schema.delete);
        let isValid = valid && option.where.reduce( (isValid, option) => {
            let {
                valid, 
                errors: _errors
            } = $.validator.validate(option, schema.deleteWhereOption);
            errors.concat(_errors);
            return isValid && valid;
        }, true);
        if(errors.length){
            $.logError('Invalid delete Options', errors, 'W');
        }
        return {
            isValid,
            errors
        };
    },
    createOptions: function (options) {
        let isValid = Array.isArray(options);
        let errors = [];
        if(!isValid){
            errors.push('options must be and array of objects to create');
        }
        isValid = isValid && options.reduce( (isValid, option) => {
            let {
                isValid: _isValid, 
                errors: _errors
            } = this.createOption(option);
            errors.concat(_errors);
            return isValid && _isValid;
        });
        return {
            isValid,
            errors
        };
    },
    createOption: function (option) {
        let {valid, errors} = $.validator.validate(option, schema.createOption);
        return {
            isValid: valid,
            errors
        };
    }
};

const schema = {
    example: {
        type: 'string',
        required: true,
        allowEmpty: false
    },
    delete: {
        properties: {
            where: {
                type: 'array',
                required: true,
                allowEmpty: false
            }
        }
    },
    deleteWhereOption: {
        properties: {
            field: {
                type: 'string',
                required: true,
                allowEmpty: false
            },
            operator: {
                type: 'string',
                required: true,
                allowEmpty: false
            },
            value: {
                type: ['string', 'integer'],
                required: true,
                allowEmpty: false
            }
        }
    },
    createOption: {
        properties: {
            user: {
                type: 'integer',
                required: true,
                allowEmpty: false
            },
            tax: {
                type: 'string',
                required: true,
                allowEmpty: false
            }
        }
    }
};