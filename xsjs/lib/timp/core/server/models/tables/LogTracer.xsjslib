$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

this.LogTracerTable = new BaseModel({
	name: 'CORE::LOG_TRACER',
	schema: $.schema.slice(1, -1),
	identity: 'LOG_TRACER',
	component: 'CORE',
	type: 'table',
	fields: {
		Query:{
            columnName: 'QUERY',
			type: 'json'
		}, 
		ExecutionTime: {
			columnName: 'EXECUTION_TIME',
            type: 'string',
            size: 40
		},
		ServerFunction: {
			columnName: 'SERVER_FUNCTION',
			type: 'string',
            size: 50
		},
		UserID:  {
            columnName: 'USER_ID',
            type: 'integer'
        },
        UserName: {
            columnName: 'USER_NAME',
			type: 'string',
            size: 50
        },
        SessionID:  {
            columnName: 'SESSION_ID',
            type: 'string',
            size: 40
        },
        Component:  {
            columnName: 'COMPONENT',
            type: 'string',
            size: 10
        },
        RegistersNumber:  {
            columnName: 'REGISTERS_NUMBER',
            type: 'integer'
        },
        Observation:  {
            columnName: 'OBSERVATION',
            type: 'string',
            size: 50
        },
        IdConfiguration:{
            columnName: 'ID_CONFIGURATION',
            type: 'integer'
        },
        ConfigurationName:{
            columnName: 'CONFIGURATION_NAME',
            type: 'string',
            size: 30
        }
	}
});

//BCB inicio e FIM
//CORE ORM V2


this.createLogTrace = function(object){
    //to do : fazer conexão direita e utilizar o bind paramiter
    try{
        const query = 'INSERT INTO "' + $.schema.slice(1, -1) + '"."CORE::LOG_TRACER" VALUES(?,?,?,?,?,?,?,?,?,?,?)';
        $.import('timp.core.server.orm', 'sql');
        const SQL = $.timp.core.server.orm.sql;
        SQL.executeTraceBindParamiters({
            query: query,
            values: object
        });
        return;
    }
    catch(e){
    }
};

// this.deleteLogTrace = function(){ 

// };

// this.readLogTrace = function(){

// };