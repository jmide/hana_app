$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config;

this.logATRModel = new BaseModel({
    name: 'CORE::LOG_ATR',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_ATR',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

this.logBCBModel = new BaseModel({
    name: 'CORE::LOG_BCB',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_BCB',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        field:          { columnName: 'FIELD',      type: 'string', size: 128 },
        value:          { columnName: 'VALUE',      type: 'string', size: 128 },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        service:        { columnName: 'SERVICE',    type: 'string', size: 128 },
        sentXML:        { columnName: 'SENTXML',    type: 'string', size: 3000 },
        obtainedXML:    { columnName: 'OBTAINEDXML',      type: 'string', size: 3000 },
        parameters:     { columnName: 'PARAMETERS', type: 'string', size: 512 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
	}
});

this.logBFBModel = new BaseModel({
    name: 'CORE::LOG_BFB',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_BFB',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
	}
});

this.logBRBModel = new BaseModel({
    name: 'CORE::LOG_BRB',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_BRB',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        name:           { columnName: 'NAME',    type: 'string', size: 512 },
        oldName:        { columnName: 'OLDNAME',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        ruleids:       { columnName: 'RULEIDS',   type: 'string', size: 512 },
        values:       { columnName: 'VALUES',   type: 'string', size: 3000 },

        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
	}
});

this.logBREModel = new BaseModel({
    name: 'CORE::LOG_BRE',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_BRE',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        code:           { columnName: 'CODE',       type: 'string', size: 128 },
        description:    { columnName: 'DESCRIPTION',type: 'string', size: 512 },
        base:           { columnName: 'BASE',       type: 'string', size: 128 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'text', size: 10000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'text', size: 10000 },
        action:         { columnName: 'ACTION',     type: 'string', size: 128 },
        name:           { columnName: 'NAME',       type: 'string', size: 128 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
	}
});

this.logBSCModel = new BaseModel({
    name: 'CORE::LOG_BSC',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_BSC',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        service:        { columnName: 'SERVICE',    type: 'string', size: 128 },
        sentXML:        { columnName: 'SENTXML',    type: 'text', size: 10000 },
        obtainedXML:    { columnName: 'OBTAXML',    type: 'text', size: 10000 },
        fieldName:      { columnName: 'FIELDNAME',  type: 'string', size: 128 },
        reason:         { columnName: 'REASON',     type: 'string', size: 512 },
        justification:  { columnName: 'JUSTIFICATION',    type: 'string', size: 512 },
        objId:          { columnName: 'OBJID',      type: 'integer'},
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 },
        docnum:       { columnName: 'DOCNUM',   type: 'string', size: 12 },
        itemnum:       { columnName: 'ITEMNUM',   type: 'string', size: 12 }
    }
});

this.logDFGModel = new BaseModel({
    name: 'CORE::LOG_DFG',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_DFG',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        block:          { columnName: 'BLOCK',      type: 'string', size: 128 },
        record:         { columnName: 'RECORD',     type: 'string', size: 128 },
        field:          { columnName: 'FIELD',      type: 'string', size: 128 },
        blockId:        { columnName: 'BLOCKID',    type: 'string', size: 6 },
        recordId:       { columnName: 'RECORDID',    type: 'string', size: 6 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
    }
});

this.logMDRModel = new BaseModel({
    name: 'CORE::LOG_MDR',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_MDR',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:         { columnName: 'OLDVALUE',     type: 'string', size: 3000 },
        newValue:         { columnName: 'NEWVALUE',     type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

this.logTAAModel = new BaseModel({
    name: 'CORE::LOG_TAA',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TAA',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        service:        { columnName: 'SERVICE',    type: 'string', size: 128 },
        sentXML:        { columnName: 'SENTXML',    type: 'text', size: 10000 },
        obtainedXML:    { columnName: 'OBTAXML',    type: 'text', size: 10000 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

this.logTBDModel = new BaseModel({
    name: 'CORE::LOG_TBD',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TBD',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

this.logTCCModel = new BaseModel({
    name: 'CORE::LOG_TCC',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TCC',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        logId:          { columnName: 'LOGID',      type: 'integer' },
        field:          { columnName: 'FIELD',      type: 'string', size: 128 },
        value:          { columnName: 'VALUE',      type: 'string', size: 512 },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

this.logTDKModel = new BaseModel({
    name: 'CORE::LOG_TDK',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TDK',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        widget:         { columnName: 'WIDGET',    type: 'string', size: 512 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
	}
});

this.logTFBModel = new BaseModel({
    name: 'CORE::LOG_TFB',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TFB',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
	}
});

this.logTFPModel = new BaseModel({
    name: 'CORE::LOG_TFP',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TFP',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
        logId:          { columnName: 'LOGID',      type: 'integer' },
        objId:          { columnName: 'OBJID',      type: 'integer'},
        idCompany:      { columnName: 'IDCOMPANY',  type: 'string', size: 4 },
        uf:             { columnName: 'UF',         type: 'string', size: 2 },
        idBranch:       { columnName: 'IDBRANCH',   type: 'string', size: 4 },
        idTax:          { columnName: 'IDTAX',      type: 'string', size: 128 },
        year:           { columnName: 'YEAR',       type: 'string', size: 4 },
        month:          { columnName: 'MONTH',      type: 'string', size: 2 },
        subPeriod:      { columnName: 'SUBPERIOD',  type: 'string', size: 4 },
        startDate:      { columnName: 'STARTDATE',  type: 'string', size: 128 },
        endDate:        { columnName: 'ENDDATE',    type: 'string', size: 128 },
        status:         { columnName: 'STATUS',     type: 'string', size: 4 },
        periodicity:    { columnName: 'PERIODICITY', type: 'string', size: 128 },
        startDateTypeDate:      { columnName: 'STARTDATETYPE',  type: 'string', size: 128 },
        endDateTypeDate:        { columnName: 'ENDDATETYPE',    type: 'string', size: 128 },
        tributo:        { columnName: 'TRIBUTO',    type: 'string', size: 128 },
        oldStatus:      { columnName: 'OLDSTATUS',  type: 'string', size: 4 },
        validFrom:      { columnName: 'VALIDFROM',  type: 'string', size: 128 },
        validTo:        { columnName: 'VALIDTO',    type: 'string', size: 128 },
        statusTdf:      { columnName: 'STATUSTDF',  type: 'string', size: 4 },
        openType:       { columnName: 'OPENTYPE',   type: 'integer'},
        dayType:        { columnName: 'DAYTYPE',    type: 'integer'},
        day:            { columnName: 'DAY',        type: 'string', size: 4 },
        creationDate:   { columnName: 'CREATIONDATE',  type: 'string', size: 128 },
        creationUser:   { columnName: 'CREATIONUSER',    type: 'integer'},
        modificationDate:   { columnName: 'MODIFDATE',  type: 'string', size: 128 },
        modificationUser:   { columnName: 'MODIFUSER',    type: 'integer'},
        company:        { columnName: 'COMPANY',  type: 'string', size: 4 },
        branch:         { columnName: 'BRANCH',   type: 'string', size: 4 },
        type:           { columnName: 'TYPE',      type: 'integer'},
        service:        { columnName: 'SERVICE',    type: 'string', size: 128 },
        sentXML:        { columnName: 'SENTXML',    type: 'text', size: 10000 },
        obtainedXML:    { columnName: 'OBTAXML',    type: 'text', size: 10000 },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        idObjectType:   { columnName: 'IDOBJECTTYPE',    type: 'string', size: 128 },
        others:         { columnName: 'OTHERS',      type: 'string', size: 3000 }
    }
});

this.logTPCModel = new BaseModel({
    name: 'CORE::LOG_TPC',
	schema: schema.application.schema.slice(1,-1),
	identity: 'LOG_TPC',
	component: 'CORE',
	type: 'table',
	fields: {
	    id: {
		    columnName: 'ID',
			type: 'integer',
		    primaryKey: true,
		    autoIncrement: true
		},
		logId:          { columnName: 'LOGID',      type: 'integer' },
        message:        { columnName: 'MESSAGE',    type: 'string', size: 512 },
        service:        { columnName: 'SERVICE',    type: 'string', size: 128 },
        sentXML:        { columnName: 'SENTXML',    type: 'text', size: 10000 },
        obtainedXML:    { columnName: 'OBTAXML',    type: 'text', size: 10000 },
        oldValue:       { columnName: 'OLDVALUE',   type: 'string', size: 3000 },
        newValue:       { columnName: 'NEWVALUE',   type: 'string', size: 3000 },
        others:         { columnName: 'OTHERS',     type: 'string', size: 3000 }
    }
});

