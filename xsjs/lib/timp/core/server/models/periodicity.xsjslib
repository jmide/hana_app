$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

var table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Periodicity"',
	tags: false,
	fields: {
		id : new table_lib.AutoField({
            pk : true,
			name: "ID",
			auto: schema.default + '."CORE::Periodicity::ID".nextval',
			type: $.db.types.INTEGER
		}),
		brLabel: new table_lib.Field({
			name: 'BR_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 255
		}),
		usLabel: new table_lib.Field({
			name: 'US_LABEL',
			type: $.db.types.NVARCHAR,
			dimension: 255
		})
	}
});
this.table = table;
