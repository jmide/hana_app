//general configurations
$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'table');
var table = $.timp.core.server.orm.table;

$.import("timp.core.server.models", "schema");
var schema = $.timp.core.server.models.schema;

var model = this.model = new table.Table({
	component: 'CORE',
	name: schema.default+'."CORE::CONFIGURATION"',
	fields: {
// 		'id': new table.AutoField({
// 			name: 'ID',
// 			auto: schema.default+'."CORE::CONFIGURATION::ID".nextval',
// 			type: $.db.types.INTEGER,
// 			pk: true
// 		}),
		'component': new table.Field({
			name: 'COMPONENT_ID',
			type: $.db.types.INTEGER
		}),
		'parameter': new table.Field({
			name: 'KEY',
			type: $.db.types.NVARCHAR,
			dimension: 128
		}),
		'value': new table.Field({
			name: 'VALUE',
			type: $.db.types.NVARCHAR,
			dimension: 255
		})
	}
});
this.table = this.model;


this.getParameter = function(component, parameter) {
	try {
		var componentId = util.getComponentInformation(component).results[0].id;
		var value = model.READ({
			fields: ["value"],
			where: [{
					field: 'component',
					oper: '=',
					value: componentId
				},
				{
					field: 'parameter',
					oper: '=',
					value: parameter
				}]
		})[0].value;
	} catch (e) {
		return null;
	}
	return value;
};

this.setParameter = function(component, parameter, value) {
	try {
		var componentId = util.getComponentInformation(component).results[0].id;
		var entry = model.READ({
			fields: ["value"],
			where: [{
					field: 'component',
					oper: '=',
					value: componentId
				},
				{
					field: 'parameter',
					oper: '=',
					value: parameter
				}]
		});
		if (entry.length) {
			entry[0].value = value;
			return model.UPDATE(entry[0]);
		} else {
			return model.CREATE({
				component: componentId,
				parameter: parameter,
				value: value
			});
		}
	} catch (e) {
		return false;
	}
};