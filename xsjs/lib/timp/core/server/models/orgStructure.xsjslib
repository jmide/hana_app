$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

/*----------------------------*/
// Organizational Structure
// This are the models for
// Company (Empresa)
// Region (Estado)
// Branch (Filial)
/*----------------------------*/


// Company
this.company = new table_lib.Table({
    component: 'CORE',
	name: '"_SYS_BIC"."timp.atr.modeling.emp_fil/EMPRESA"',
	tags: false,
	fields: {
        mandt: new table_lib.Field({
			name: 'MANDT',
			type: $.db.types.NVARCHAR
		}),
		id: new table_lib.Field({
			name: 'COD_EMP',
			type: $.db.types.NVARCHAR
		}),
		name : new table_lib.Field({
			name: 'NOME_EMPRESA',
			type: $.db.types.NVARCHAR
		})
	}
});

// Branch
this.branch = new table_lib.Table({
    component: 'CORE',
	name: '"_SYS_BIC"."timp.atr.modeling.emp_fil/FILIAL"',
	tags: false,
	fields: {
        mandt: new table_lib.Field({
			name: 'MANDT',
			type: $.db.types.NVARCHAR
		}),
		companyID: new table_lib.Field({
			name: 'COD_EMP',
			type: $.db.types.NVARCHAR
		}),
		id : new table_lib.Field({
			name: 'COD_FILIAL',
			type: $.db.types.NVARCHAR
		}),
		name : new table_lib.Field({
			name: 'NOME_FILIAL',
			type: $.db.types.NVARCHAR
		}),
	/*	addressID: new table_lib.Field({
			name: 'ADRNR',
			type: $.db.types.NVARCHAR
		}),
		name1: new table_lib.Field({
			name: 'NAME1',
			type: $.db.types.NVARCHAR
		}),
		name2: new table_lib.Field({
			name: 'NAME2',
			type: $.db.types.NVARCHAR
		}),*/
		state : new table_lib.Field({
			name: 'UF_FILIAL',
			type: $.db.types.NVARCHAR
		})
	}
});

// throw this.branch
    

