var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;
$.import('timp.core.server.orm', 'table');
var tableLib = $.timp.core.server.orm.table;

$.import("timp.core.server.models", "schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;
var _self = this;

function AsIs(foo) {
    return foo;
}

//OUTPUT TYPE TEXT
var outputTypeText = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::OutputTypeText"',
    default_fields: 'common',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::OutputTypeText::ID".nextval',
            type: $.db.types.INTEGER
        }),
        idOutputType: new tableLib.Field({
            name: 'ID_OUTPUT_TYPE',
            type: $.db.types.INTEGER,
            pk: true
        }),
        name: new tableLib.Field({
            name: 'NAME',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        description: new tableLib.Field({
            name: 'DESCRIPTION',
            type: $.db.types.NVARCHAR,
            dimension: 250
        }),
        lang: new tableLib.Field({
            name: 'LANG',
            type: $.db.types.NVARCHAR,
            dimension: 10,
            pk: true
        })
    }
});
this.outputTypeText = outputTypeText;

//OUTPUT TYPE
var outputType = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::OutputType"',
    default_fields: 'common',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::OutputType::ID".nextval',
            type: $.db.types.INTEGER,
            pk: true
        }),
        iconFont: new tableLib.Field({
            name: 'ICON_FONT',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        icon: new tableLib.Field({
            name: 'ICON',
            type: $.db.types.NVARCHAR,
            dimension: 100
        })
    }
});
this.outputType = outputType;

this.outputTypeList = function () {
    try {
        var options = {
            where: [],
            join: [],
            fields: ['id', 'iconFont', 'icon']
        };

        options.where.push({
            field: "lang",
            oper: "LIKE",
            value: $.request.cookies.get("Content-Language"),
            table: outputTypeText
        });
        options.join.push({
            alias: 'outputTypeText',
            table: outputTypeText,
            on: [{
                left: 'id',
                right: 'idOutputType'
            }]
        });
        // options.simulate = true;
        return this.outputType.READ(options);
    } catch (e) {
        $.trace.error(e);
    }
};

this.outputType.list = function () {
    return _self.outputTypeList();
};

//OUTPUT VALUE
var outputValue = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::OutputValue"',
    default_fields: 'common',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::OutputValue::ID".nextval',
            type: $.db.types.INTEGER
        }),
        idOutput: new tableLib.Field({
            name: 'ID_OUTPUT',
            type: $.db.types.INTEGER
        }),
        value: new tableLib.Field({
            name: 'VALUE',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        valueDataType: new tableLib.Field({
            name: 'VALUE_DATA_TYPE',
            type: $.db.types.INTEGER,
            translate: {
                1: 'TINYINT',
                2: 'SMALLINT',
                3: 'INTEGER',
                4: 'BIGINT',
                5: 'DECIMAL',
                6: 'REAL',
                7: 'DOUBLE',
                8: 'CHAR',
                9: 'VARCHAR',
                10: 'NCHAR',
                11: 'NVARCHAR',
                12: 'BINARY',
                13: 'VARBINARY',
                14: 'DATE',
                15: 'TIME',
                16: 'TIMESTAMP',
                25: 'CLOB',
                26: 'NCLOB',
                27: 'BLOB',
                47: 'SMALLDECIMAL',
                51: 'TEXT',
                52: 'SHORTTEXT'
            }
        }),
        json: new tableLib.Field({
            name: 'JSON',
            type: $.db.types.NCLOB
        }),
        month: new tableLib.Field({
            name: 'MONTH',
            type: $.db.types.NVARCHAR,
            dimension: 2
        }),
        year: new tableLib.Field({
            name: 'YEAR',
            type: $.db.types.NVARCHAR,
            dimension: 4
        }),
        subperiod: new tableLib.Field({
            name: 'SUBPERIOD',
            type: $.db.types.NVARCHAR,
            dimension: 2
        }),
        idCompany: new tableLib.Field({
            name: 'ID_COMPANY',
            type: $.db.types.NVARCHAR,
            dimension: 4
        }),
        uf: new tableLib.Field({
            name: 'UF',
            type: $.db.types.NVARCHAR,
            dimension: 2
        }),
        idBranch: new tableLib.Field({
            name: 'ID_BRANCH',
            type: $.db.types.NVARCHAR,
            dimension: 4
        }),
        idTax: new tableLib.Field({
            name: 'ID_TAX',
            type: $.db.types.NVARCHAR,
            dimension: 3
        }),
        destinationBranch: new tableLib.Field({
            name: 'DESTINATION_BRANCH',
            type: $.db.types.NVARCHAR,
            dimension: 2
        })
    }
});
this.outputValue = outputValue;

this.outputValueCreate = function (object) {
    try {
        var options = {};
        object = JSON.parse(object);
        options = {
            idOutput: object.idOutput,
            value: object.value,
            dataType: object.dataType,
            json: JSON.stringify(object.json),
            year: object.year,
            month: object.month,
            subperiod: object.subperiod,
            idCompany: object.idCompany,
            uf: object.uf,
            idBranch: object.idBranch,
            idTax: object.idTax
        };
        if (!$.lodash.isNil(object.destinationBranch)) {
            options.destinationBranch = object.destinationBranch;
        }
        return outputValue.CREATE(options);
    } catch (e) {
        $.trace.error(e);
    }
};
this.outputValue.create = function (object) {
    return _self.outputValueCreate(object);
};

this.outputValueUpdate = function (object) {
    try {
        var options = {};
        var object = JSON.parse(object);
        options = {
            id: object.id,
            value: object.value,
            dataType: object.dataType,
            json: object.json,
            year: object.year,
            month: object.month,
            subperiod: object.subperiod,
            idCompany: object.idCompany,
            uf: object.uf,
            idBranch: object.idBranch,
            idTax: object.idTax
        };
        if (!$.lodash.isNil(object.destinationBranch)) {
            options.destinationBranch = object.destinationBranch;
        }
        return this.outputValue.UPDATE(options);
    } catch (e) {
        $.trace.error(e);
    }
};
this.outputValue.update = function (object) {
    return _self.outputValueUpdate(object);
};

this.outputValueRead = function (object) {
    try {
        var where = [];
        object = JSON.parse(object);
        Object.keys(object).forEach(function (ele) {
            if (ele !== "json") {
                where.push({
                    field: ele,
                    oper: "=",
                    value: object[ele]
                });
            }
        });
        return this.outputValue.READ({
            where: where
        });
    } catch (e) {
        $.trace.error(e);
    }
};
this.outputValue.read = function (object) {
    return _self.outputValueRead(object);
};

//OUTPUT 
var output = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::Output"',
    default_fields: 'common',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::Output::ID".nextval',
            type: $.db.types.INTEGER,
            pk: true
        }),
        idComponent: new tableLib.Field({
            name: 'ID_COMPONENT',
            type: $.db.types.INTEGER
        }),
        objectType: new tableLib.Field({
            name: 'OBJECT_TYPE',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        idObject: new tableLib.Field({
            name: 'ID_OBJECT',
            type: $.db.types.INTEGER
        }),
        idOutputType: new tableLib.Field({
            name: 'ID_OUTPUT_TYPE',
            type: $.db.types.INTEGER
        }),
        name: new tableLib.Field({
            name: 'NAME',
            type: $.db.types.NVARCHAR,
            dimension: 100
        }),
        description: new tableLib.Field({
            name: 'DESCRIPTION',
            type: $.db.types.NVARCHAR,
            dimension: 100
        })
    }
});
this.output = output;

this.outputList = function (object) {
    try {
        var options = {
            where: [],
            join: [],
            fields: ['id', 'idComponent', 'objectType', 'idObject', 'name', 'description']
        };

        var object = JSON.parse(object);
        if (object.hasOwnProperty("idOutputType")) {
            options.where.push({
                field: "idOutputType",
                oper: "=",
                value: object.idOutputType
            });
            options.join.push({
                alias: 'outputType',
                table: outputType,
                on: [{
                    left: 'idOutputType',
                    right: 'id'

                }],
                outer: "left"
            });
        }
        if (object.hasOwnProperty("objectType")) {
            options.where.push({
                field: "objectType",
                oper: "=",
                value: object.objectType
            });
        }
        if (object.hasOwnProperty("idObject")) {
            options.where.push({
                field: "idObject",
                oper: "=",
                value: object.idObject
            });
        }
        if (object.hasOwnProperty("idComponent")) {
            options.where.push({
                field: "idComponent",
                oper: "=",
                value: object.idComponent
            });
        }

        options.where.push({
            field: "lang",
            oper: "LIKE",
            value: $.request.cookies.get("Content-Language"),
            table: outputTypeText
        });
        options.join.push({
            alias: 'outputTypeText',
            table: outputTypeText,
            on: [{
                left: 'idOutputType',
                right: 'idOutputType'
            }]
        });

        if (object.hasOwnProperty("values")) {
            if (object.values) {
                // ----------< Begin Testes: Bayron >----------
                if (object.idCompany && object.uf && object.idBranch && object.idTax) {
                    options.join.push({
                        alias: 'values',
                        table: outputValue,
                        on: [{
                                left: 'id',
                                right: 'idOutput'
                            },
                            {
                                field: 'idCompany',
                                oper: "=",
                                value: object.idCompany
                            },
                            {
                                field: 'uf',
                                oper: "=",
                                value: object.uf
                            },
                            {
                                field: 'idBranch',
                                oper: "=",
                                value: object.idBranch
                            },
                            {
                                field: 'idTax',
                                oper: "=",
                                value: object.idTax
                            }
                        ],
                        outer: "left"
                    });
                    // ----------< End Testes: Bayron >----------    
                } else {
                    options.join.push({
                        alias: 'values',
                        table: outputValue,
                        on: [{
                            left: 'id',
                            right: 'idOutput'
                        }],
                        outer: "left"
                    });
                }
            }
        }
        // options.simulate = true;
        return this.output.READ(options);
    } catch (e) {
        $.trace.error(e);
    }

};
this.output.list = function (object) {
    return _self.outputList(object);
};

this.outputCreate = function (object) {
    try {
        var options = {};
        var object = JSON.parse(object);

        options = {
            idComponent: object.idComponent,
            objectType: object.objectType,
            idObject: object.idObject,
            idOutputType: object.idOutputType ? object.idOutputType : -1,
            name: object.name,
            description: object.description
        };
        return this.output.CREATE(options);
    } catch (e) {
        $.trace.error(e);
    }
};
this.output.create = function (object) {
    return _self.outputCreate(object);
};

this.outputUpdate = function (object) {
    try {
        var options = {};
        var object = JSON.parse(object);

        options = {
            id: object.id,
            idOutputType: object.idOutputType,
            name: object.name,
            description: object.description
        };

        return this.output.UPDATE(options);
    } catch (e) {
        $.trace.error(e);
    }

};
this.output.update = function (object) {
    return _self.outputUpdate(object);
};