//Default schema for all of TIMP tables
$.import('timp.schema.server.api','api');
var schema = $.timp.schema.server.api.api.schema;
this.default = schema.default; //'"TIMP"';

//Set it true and ORM will ignore using HANA Plan Cache. Must only be used for SP10.
this.IgnorePlanCache = false;
