$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;
$.import('timp.core.server.orm', 'table');
var table_lib = $.timp.core.server.orm.table;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::ActionsParameters"',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::ActionsParameters::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		type: new table_lib.Field({
			name: 'TYPE',
			type: $.db.types.TINYINT,
			default: 0,
			translate: {0: 'input', 1: 'output'}
		}),
		id_action: new table_lib.Field({
			name: 'ID_ACTION',
			type: $.db.types.INTEGER
		}),
		id_objectType: new table_lib.Field({
			name: 'ID_OBJ_TYPE',
			type: $.db.types.INTEGER
		})
	}
});