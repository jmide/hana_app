$.import('timp.core.server.orm', 'table');
var tableLib = $.timp.core.server.orm.table;

$.import('timp.core.server.models','schema');
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

$.import('timp.core.server.models', 'objects');
var objectTypeModel = $.timp.core.server.models.objects.objectTypes;

//CONSTANTS
this.statusTypes = {
	STANDARD: 0,
	ACTIVE: 1,
	TRASH: 2,
	DELETED: 3,
	PUBLIC: 4
};

this.folder = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Folder"',
	defaultFields: 'common',
	fields: {
		id: new tableLib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Folder::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		name: new tableLib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 256
		}),
		description: new tableLib.Field({
			name: 'DESCRIPTION',
			type: $.db.types.NVARCHAR,
			dimension: 512
		}),
		status: new tableLib.Field({
			name: 'STATUS',
			type: $.db.types.TINYINT,
			'default': 1,
			translate: {0:'Standard', 1:'Active', 2: 'Trash', 3: 'Deleted'}
		}),
		idParent: new tableLib.Field({
			name: 'ID_PARENT',
			type: $.db.types.INTEGER
		}),
		idObjectType: new tableLib.Field({
			name: 'ID_OBJECT_TYPE',
			type: $.db.types.INTEGER
		})
	}
});

this.file = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::File"',
	defaultFields: 'common',
	fields: {
		id: new tableLib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::File::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		status: new tableLib.Field({
			name: 'STATUS',
			type: $.db.types.TINYINT,
			'default': 1,
			translate: {0:'Standard', 1:'Active', 2: 'Trash', 3: 'Deleted',4:'Public'}
		}),
		idFolder: new tableLib.Field({
			name: 'ID_FOLDER',
			type: $.db.types.INTEGER
		}),
		idObjectType: new tableLib.Field({
			name: 'ID_OBJECT_TYPE',
			type: $.db.types.INTEGER
		}),
		idObject: new tableLib.Field({
			name: 'ID_OBJECT',
			type: $.db.types.INTEGER
		})
	}
});

this.folderShare = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::FolderShare"',
	defaultFields: 'common',
	fields: {
		id: new tableLib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::FolderShare::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		idFolder: new tableLib.Field({
			name: 'ID_FOLDER',
			type: $.db.types.INTEGER
		}),
		idUser: new tableLib.Field({
			name: 'ID_USER',
			type: $.db.types.INTEGER
		}),
		idGroup: new tableLib.Field({
			name: 'ID_GROUP',
			type: $.db.types.INTEGER
		})
	}
});

this.fileShare = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::FileShare"',
	defaultFields: 'common',
	fields: {
		id: new tableLib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::FileShare::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		idFile: new tableLib.Field({
			name: 'ID_FILE',
			type: $.db.types.INTEGER
		}),
		idUser: new tableLib.Field({
			name: 'ID_USER',
			type: $.db.types.INTEGER
		}),
		idGroup: new tableLib.Field({
			name: 'ID_GROUP',
			type: $.db.types.INTEGER
		}),
		idObjectType: new tableLib.Field({
    		name: 'ID_OBJECT_TYPE',
    		type: $.db.types.INTEGER
		}),
		idObject: new tableLib.Field({
    		name: 'ID_OBJECT',
    		type: $.db.types.INTEGER
		})
	}
});

this.fileFavs = new tableLib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::FileFavs"',
	defaultFields: 'common',
	fields: {
		id: new tableLib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::FileFavs::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		idFile: new tableLib.Field({
			name: 'ID_FILE',
			type: $.db.types.INTEGER
		}),
		idUser: new tableLib.Field({
			name: 'ID_USER',
			type: $.db.types.INTEGER
		}),
		idObjectType: new tableLib.Field({
			name: 'ID_OBJECT_TYPE',
			type: $.db.types.INTEGER
		}),
		idObject: new tableLib.Field({
    		name: 'ID_OBJECT',
    		type: $.db.types.INTEGER
		})
	}
});

this.fileLock = new tableLib.Table({
    component: 'CORE',
    name: schema.default + '."CORE::FileLock"',
    defaultFields: 'common',
    fields: {
        id: new tableLib.AutoField({
            name: 'ID',
            auto: schema.default + '."CORE::FileLock::ID".nextval', 
            type: $.db.types.INTEGER,
            pk: true
        }),
        idObjectType: new tableLib.Field({
            name: 'ID_OBJECT_TYPE',
            type: $.db.types.INTEGER
        }),
        idObject: new tableLib.Field({
            name: 'ID_OBJECT',
            type: $.db.types.NVARCHAR,
            dimension: 64
        }),
        onExecute: new tableLib.Field({
            name: "ON_EXECUTE",
            type: $.db.types.INTEGER,
            translate: { 1: true, 0: false, null: false, undefined: false }
        }),
        lockDate: new tableLib.Field({
            name: 'LOCK_DATE',
            type: $.db.types.TIMESTAMP
        }),
        lockVerification: new tableLib.Field({
            name: 'LOCK_VERIFICATION',
            type: $.db.types.TIMESTAMP
        }),
        lockConditional: new tableLib.Field({
            name: 'LOCK_CONDITIONAL',
            type: $.db.types.INTEGER,
            translate: { 1: true, 2: false }
        }),
        userRequest: new tableLib.Field({
            name: 'USER_REQUEST',
            type: $.db.types.INTEGER
        }),
        dateRequest: new tableLib.Field({
            name: 'DATE_REQUEST',
            type: $.db.types.TIMESTAMP
        })
    }
});
try{
    $.import('timp.core.server.orm', 'BaseModel');
    const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;
    $.import('timp.core.server', 'config');
    const aSchema = $.timp.core.server.config;
    this.fileBaseModel = new BaseModel({
        name: 'CORE::File',
        schema: aSchema.application.schema.slice(1,-1),
        identity: 'File',
        component: 'CORE',
        type: 'table',
        fields: {
            id: {
                columnName: 'ID',
                type: 'integer'
            },
            status: {
                columnName: 'STATUS',
                type: 'tinyint',
                translate: {0:'Standard', 1:'Active', 2: 'Trash', 3: 'Deleted',4:'Public'}
            },
            idFolder: {
                columnName: 'ID_FOLDER',
                type: 'integer'
            },
            idObjectType: {
                columnName: 'ID_OBJECT_TYPE',
                type: 'integer'
            },
            idObject: {
                columnName: 'ID_OBJECT',
                type: 'integer'
            },
            creationDate: {
                columnName: 'CREATION.DATE',
                type: 'datetime'
            },
            creationUserId: {
                columnName: 'CREATION.ID_USER',
                type: 'integer'
            },
            modificationDate: {
                columnName: 'MODIFICATION.DATE',
                type: 'datetime'
            },
            modificationUserId: {
                columnName: 'MODIFICATION.ID_USER',
                type: 'integer'
            }
        }
    });
    
    this.folderBaseModel = new BaseModel({
        name: 'CORE::Folder',
        schema: aSchema.application.schema.slice(1,-1),
        identity: 'Folder',
        component: 'CORE',
        type: 'table',
        fields: {
            id: {
                columnName: 'ID',
                type: 'integer'
            },
            name: {
                columnName: 'NAME',
                type: 'string',
                size: 256
            },
            description: {
                columnName: 'DESCRIPTION',
                type: 'string',
                size: 512
            },
            status: {
                columnName: 'STATUS',
                type: 'tinyint',
                translate: {0:'Standard', 1:'Active', 2: 'Trash', 3: 'Deleted'}
            },
            idParent: {
                columnName: 'ID_PARENT',
                type: 'integer'
            },
            idObjectType: {
                columnName: 'ID_OBJECT_TYPE',
                type: 'integer'
            },
            creationDate: {
                columnName: 'CREATION.DATE',
                type: 'datetime'
            },
            creationUserId: {
                columnName: 'CREATION.ID_USER',
                type: 'integer'
            },
            modificationDate: {
                columnName: 'MODIFICATION.DATE',
                type: 'datetime'
            },
            modificationUserId: {
                columnName: 'MODIFICATION.ID_USER',
                type: 'integer'
            }
        }
    });
    
    this.fileFavsBaseModel = new BaseModel({
        name: 'CORE::FileFavs',
        schema: aSchema.application.schema.slice(1,-1),
        identity: 'FileFavs',
        component: 'CORE',
        type: 'table',
        fields: {
            id: {
                columnName: 'ID',
                type: 'integer'
            },
            idFile: {
                columnName: 'ID_FILE',
                type: 'integer'
            },
            idUser: {
                columnName: 'ID_USER',
                type: 'integer'
            },
            idObjectType: {
                columnName: 'ID_OBJECT_TYPE',
                type: 'integer'
            },
            idObject: {
                columnName: 'ID_OBJECT',
                type: 'integer'
            },
            creationDate: {
                columnName: 'CREATION.DATE',
                type: 'datetime'
            },
            creationUserId: {
                columnName: 'CREATION.ID_USER',
                type: 'integer'
            },
            modificationDate: {
                columnName: 'MODIFICATION.DATE',
                type: 'datetime'
            },
            modificationUserId: {
                columnName: 'MODIFICATION.ID_USER',
                type: 'integer'
            }
        }
    });
    
    this.fileShareBaseModel = new BaseModel({
        name: 'CORE::FileShare',
        schema: aSchema.application.schema.slice(1,-1),
        identity: 'FileShare',
        component: 'CORE',
        type: 'table',
        fields: {
            id: {
                columnName: 'ID',
                type: 'integer'
            },
            idFile: {
                columnName: 'ID_FILE',
                type: 'integer'
            },
            idUser: {
                columnName: 'ID_USER',
                type: 'integer'
            },
            idObjectType: {
                columnName: 'ID_OBJECT_TYPE',
                type: 'integer'
            },
            idObject: {
                columnName: 'ID_OBJECT',
                type: 'integer'
            },
            idGroup: {
                columnName: 'ID_GROUP',
                type: 'integer'
            },
            creationDate: {
                columnName: 'CREATION.DATE',
                type: 'datetime'
            },
            creationUserId: {
                columnName: 'CREATION.ID_USER',
                type: 'integer'
            },
            modificationDate: {
                columnName: 'MODIFICATION.DATE',
                type: 'datetime'
            },
            modificationUserId: {
                columnName: 'MODIFICATION.ID_USER',
                type: 'integer'
            }
        }
    });
    
}catch(e){
    this.fileBaseModel = {};
    this.folderBaseModel = {};
    this.fileFavsBaseModel = {};
    this.fileShareBaseModel = {};
}

// File Functions
this.readFile = function(object) {
    if (typeof object === 'number') {
    
        return this.file.READ({
			where: [{
				field: 'id',
				oper: '=',
				value: object
			}]
		})[0];
    }
    var options = this.getOptions(object);
   
    return this.file.READ(options);
};

this.readFileShare = function(object) {
    if (typeof object === 'number') {
        return this.fileShare.READ({
			where: [{
				field: 'idFile',
				oper: '=',
				value: object
			}]
		})[0];
    }
    var options = this.getOptions(object);
    return this.fileShare.READ(options);
};

this.readFileFavs = function(object) {
    if (typeof object === 'number') {
        return this.fileFavs.READ({
			where: [{
				field: 'idFile',
				oper: '=',
				value: object
			}]
		})[0];
    }
    var options = this.getOptions(object);
    return this.fileFavs.READ(options);
};

this.createFile = function(object) {
    var options = {
		status: object.status || this.statusTypes.ACTIVE,
		idFolder: object.idFolder || null,
		idObjectType: object.idObjectType || null,
		idObject: object.idObject || null
	};
	return this.file.CREATE(options);
};

this.createFileShare = function(object) {
    var options = {
        idFile: object.idFile || null,
        idUser: object.idUser || null,
		idGroup: object.idGroup || null,
		idObjectType: object.idObjectType || null,
		idObject: object.idObject || null
	};
	return this.folderShare.CREATE(options);
};

this.createFileFavs = function(object) {
    var options = {
        idFile: object.idFile || null,
        idUser: object.idUser || null,
		idObjectType: object.idObjectType || null,
		idObject: object.idObject || null
	};
	return this.fileFavs.CREATE(options);
};

this.createShareFile = function(object, i) {
	var fileShare;
	var options = {
		where: [{
			field: 'idUser',
			oper: '=',
			value: object.idUser
		}, {
			field: 'idFile',
			oper: '=',
			value: object.file[i].id
		}, {
			field: 'idObjectType',
			oper: '=',
			value: object.file[i].idObjectType
		}, {
			field: 'idObject',
			oper: '=',
			value: object.file[i].idObject
		}]
	};
	if (object.idGroup) {
		options.where.push({
			field: 'idGroup',
			oper: '=',
			value: object.idGroup
		});
	} else {
		options.where.push({
			field: 'idGroup',
			oper: 'IS NULL'
		});
	}
	fileShare = this.fileShare.READ(options);
	if (fileShare.length === 0) {
		var share = [];
		share.idFile = object.file[i].id;
		share.idUser = object.idUser;
		share.idGroup = object.idGroup;
		share.idObjectType = object.file[i].idObjectType;
		share.idObject = object.file[i].idObject;
		this.fileShare.CREATE(share);
		return 1;
	}
	return 0;
};

this.updateFile = function(object, where) {
    var properties = ['status', 'idFolder', 'idObjectType', 'idObject'];
    var options = {
        id: object.id || object.idFile
    };
    for (var i = 0; i < properties.length; i++) {
        if (object[properties[i]]) {
            options[properties[i]] = object[properties[i]];
        }
    }
    if (where !== undefined) {
        return this.file.UPDATEWHERE(options, where);
    }
    return this.file.UPDATE(options);
};

this.deleteFile = function(object) {
    if (typeof object === 'number') {
        return this.file.DELETE(object);
    }
    return this.file.DELETEWHERE(object);
};

this.deleteFileShare = function(object) {
    if (typeof object === 'number') {
        return this.fileShare.DELETE(object);
    }
    return this.fileShare.DELETEWHERE(object);
};

this.deleteFileFavs = function(object) {
    if (typeof object === 'number') {
        return this.fileFavs.DELETE(object);
    }
    return this.fileFavs.DELETEWHERE(object);
};



// Folder Functions
this.readFolder = function(object) {
    if (typeof object === 'number') {
        return this.folder.READ({
			where: [{
				field: 'id',
				oper: '=',
				value: object
			}]
		})[0];
    }
    var options = this.getOptions(object);
    return this.folder.READ(options);
};

this.readFolderShare = function(object) {
    if (typeof object === 'number') {
        return this.folderShare.READ({
			fields: ['idUser', 'idGroup'],
			where: [{
				field: 'idFolder',
				oper: '=',
				value: object
			}]
		})[0];
    }
    var options = this.getOptions(object);
    return this.folderShare.READ(options);
};

this.createFolder = function(object) {
    var options = {
        name: object.name || '',
        description: object.description || '',
		status: object.status || this.statusTypes.ACTIVE,
		idParent: object.idParent || -1,
		idObjectType:object.idObjectType
	};
	return this.folder.CREATE(options);
};

this.createFolderShare = function(object) {
    var options = {
        idFolder: object.idFolder || null,
        idUser: object.idUser || null,
		idGroup: object.idGroup || null
	};
	return this.folderShare.CREATE(options);
};

this.createShareFolder = function(object, i) {
	var folderShare;
	var options = {
		where: [{
			field: 'idUser',
			oper: '=',
			value: object.idUser[i].id_user
		}, {
			field: 'idFolder',
			oper: '=',
			value: object.idFolder
		}]
	};
	if (object.idUser[i].id_group) {
		options.where.push({
			field: 'idGroup',
			oper: '=',
			value: object.idUser[i].id_group
		});
	} else {
		options.where.push({
			field: 'idGroup',
			oper: 'IS NULL'
		});
	}
	folderShare = this.folderShare.READ(options);
	if (folderShare.length === 0) {
		var share = {};
		share.idFolder = object.idFolder;
		share.idUser = object.idUser[i].id_user;
		share.idGroup = object.idUser[i].id_group;
		this.folderShare.CREATE(share);
		return 1;
	}
	return 0;
};

this.updateFolder = function(object, where) {
    var properties = ['name', 'description', 'status', 'idParent'];
    var options = {
        id: object.id || object.idFolder
    };
    for (var i = 0; i < properties.length; i++) {
        if (object[properties[i]]) {
            options[properties[i]] = object[properties[i]];
        }
    }
    if (where !== undefined) {
        return this.folder.UPDATEWHERE(options, where);
    }
    return this.folder.UPDATE(options);
};

this.deleteFolder = function(object) {
    if (typeof object === 'number') {
        return this.folder.DELETE(object);
    }
    return this.folder.DELETEWHERE(object);
};

this.deleteFolderShare = function(object) {
    if (typeof object === 'number') {
        return this.folderShare.DELETE(object);
    }
    return this.folderShare.DELETEWHERE(object);
};



// Other Functions
this.getOptions = function(object) {
    var options = {};
    if (object.fields) {
        options.fields = object.fields;
    }
    if (object.where) {
        options.where = object.where;
    }
    if (object.join) {
        options.join = object.join;
    }
    if (object.count) {
        options.count = object.count;
    }
    if (object.group_by) {
        options.groupBy = object.group_by;
        options.group_by = object.group_by;
    }
    if (object.distinct) {
        options.distinct = object.distinct;
    }
    if (object.simulate) {
        options.simulate = object.simulate;
    }
    if (object.properties) {
        if (options.where === undefined) {
            options.where = [];
        }
        for (var i = 0; i < object.properties.length; i++) {
            options.where.push({
                field: object.properties[i].field,
                oper: object.properties[i].oper || '=',
                value: object.properties[i].value
            });
        }
    }
    return options;
};

this.getIdObjectType = function(objectType) {
	var object = objectTypeModel.READ({
	    fields: ['id'],
		where: [{
			field: 'name',
			oper: '=',
			value: objectType
		}]
	})[0];

	if (object === undefined) {
		throw 'ObjectType Not Found';
	}
	return object.id;
};

this.getUsersFromGroup = function(object) {
    $.import('timp.mkt.server.api', 'api');
	var userGroups = $.timp.mkt.server.api.api.tables.users_groups;
	if (object.options !== undefined) {
		return userGroups.READ(object.options);
	}
	if (typeof object.idGroup === 'number') {
		object.idGroup = [object.idGroup];
	} else if (typeof object.idGroup === 'string') {
		object.idGroup = [Number(object.idGroup)];
	}
	var ids = object.idGroup.map(function(group) {
		return {
			field: 'id_group',
			oper: '=',
			value: group
		};
	});
	var users = userGroups.READ({
		fields: ['id_user', 'id_group'],
		where: [ids],
		orderBy: ['id_user']
	});
	return users;
};


// File Lock
this.readFileLock = function(object) {
    var options = {};
    if (object.fields) {
        options.fields = object.fields;
    }
    if (object.where) {
        options.where = object.where;
    }
    if (object.orderBy) {
        options.orderBy = object.orderBy;
    }
    return this.fileLock.READ(options);
};

this.saveFileLock = function(object) {
    if (object.id) {
        return this.fileLock.UPDATE(object);
    }
    return this.fileLock.CREATE(object);
};

this.deleteFileLock = function(object) {
    if (typeof object === 'object') {
        this.fileLock.DELETEWHERE(object);
    }
    return this.fileLock.DELETE(Number(object));
};