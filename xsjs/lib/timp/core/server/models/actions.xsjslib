$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;
$.import('timp.core.server.orm', 'table');
var table_lib = $.timp.core.server.orm.table;
$.import('timp.core.server.models', 'components');
var componentsModel = $.timp.core.server.models.components.components;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

$.import("timp.core.server.models","objects");
var objects = $.timp.core.server.models.objects;
$.import("timp.core.server.models","actionsParameters");
var actionsParameters = $.timp.core.server.models.actionsParameters;

this.table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Actions"',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Actions::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		id_component: new table_lib.Field({
			name: 'ID_COMPONENT',
			type: $.db.types.INTEGER
		}),
		id_app: new table_lib.Field({
			name: 'ID_APP',
			type: $.db.types.INTEGER
		}),
		id_objectType: new table_lib.Field({
			name: 'ID_OBJ_TYPE',
			type: $.db.types.INTEGER
		}),
		name: new table_lib.Field({
			name: 'ACTION',
			type: $.db.types.VARCHAR,
			dimension: 255
		}),
		url: new table_lib.Field({
			name: 'URL',
			type: $.db.types.VARCHAR,
			dimension: 255
		}),
		type: new table_lib.Field({
			name: 'ACTION_TYPE',
			type: $.db.types.INTEGER, 
			translate: {0: 'manual', 1: 'automatic', 2: 'gatewayTask'},
			default: 0
		}),
		close: new table_lib.Field({
			name: 'CLOSE_TYPE',
			type: $.db.types.TINYINT, 
			translate: {0: 'manual', 1: 'stage'},
			default: 0
		}),
		nameEnus: new table_lib.Field({
			name: 'NAME_ENUS',
			type: $.db.types.VARCHAR,
			dimension: 255
		}),
		namePtbr: new table_lib.Field({
			name: 'NAME_PTBR',
			type: $.db.types.VARCHAR,
			dimension: 255
		})
	},
	default_joins: [{
	    fields: ['type', 'id_objectType'],
	    alias: 'parameters',
	    table: actionsParameters.table,
	    on: [{left: 'id', right: 'id_action'}]
	}]
});

/*
this.getLogAction({
	component: 'BRE',
	action: 'editRule',
	input: {},
	outputs: {}
})
*/
this.getLogAction = function (options) {
    //TO BE DEFINED
    return {
        getShortDescription: function () {
            return 'TBD';
        }
    }
}

/*
this.getActionsList({
    component (optional): 35 //BRB
})
*/
this.getActionsList = function (options) {
    options = options || {};
    
    //Get the component name
    var compOptions = {
        fields: ['id', 'name']
    };
    if (options.hasOwnProperty('component') && !isNaN(options.component)) {
        compOptions.where = [{field: 'id', oper: '=', value: Number(options.component)}];
    }
    var compsRaw = componentsModel.READ(compOptions),
        compNames = {};
    for (var x = 0; x < compsRaw.length; x++) {
        compNames[compsRaw[x].id] = compsRaw[x].name;
    }
    
    //Default read options, to be extended by "options"
    var read_opts = {
        fields: ['id', 'id_component', 'name', 'type', 'close', 'namePtbr', 'nameEnus'],
        join: [{
    	   // fields: ['type', 'id_objectType'],
    	    alias: 'parameters',
    	    table: actionsParameters.table,
    	    on: [{left: 'id', right: 'id_action'}],
    	    outer: 'left'
    	},{
    	    fields: ['id','name'],
            table: objects.objectTypes,
            alias: "objectType",
            on: [{left_table: actionsParameters.table, left: 'id_objectType', right: 'id'}],
            outer: 'left'
        },{
    	    alias: 'component',
    	    table: componentsModel.table,
    	    on: [{left: 'id_component', right: 'id'}]
    	}],
    	order_by: [{table: componentsModel.table, field: 'name'}, 'name']
    }
    
    //If options specify a component
    if (options.component && util.isNumber(options.component)) {
        read_opts.where = [{field: 'id_component', oper: '=', value: options.component}];
    }
    
    //Execute READ
    var actions = this.table.READ(read_opts);
    
    //Untangle
    var components = {};
    for (var z = 0; z < actions.length; z++) {
        //Process components
        var component = actions[z].component[0];
        actions[z].actionName = compNames[component.id] + '.' + actions[z].name;
        if (!components[component.id]) {
            components[component.id] = component; //Actions belong to a single component
            components[component.id].actions = {};
        }
        
        //Process component actions
        var n_action;
        if (!components[component.id].actions[actions[z].id]) {
            n_action = {};
            for (var attr in actions[z]) {
                if (typeof actions[z][attr] !== 'object') { //Skip the arrays of relations
                    n_action[attr] = actions[z][attr];
                }
            }
            n_action.inputs = {};
            n_action.outputs = {};
            components[component.id].actions[actions[z].id] = n_action;
        } else {
            n_action = components[component.id].actions[actions[z].id]
        }
        
        //Process input types
        var param_types = {}
        for (var y = 0; y < actions[z].objectType.length; y++) {
            var obj_type = actions[z].objectType[y];
            param_types[obj_type.id] = obj_type;
        }
        
        //Process action parameters
        for (var y = 0; y < actions[z].parameters.length; y++) {
            var param = actions[z].parameters[y];
            if (!param.hasOwnProperty('id_objectType')) {
                continue;
                // throw 'Misdefined parameter ' + util.parseError(param) + ' for ' + actions[z].actionName;
            }
            if (!param_types.hasOwnProperty(param.id_objectType)) {
                continue;
                // throw 'Unknown ObjectType ID: ' + param.id_objectType + ' at ' + actions[z].actionName;
            }
            param.objectType = param_types[param.id_objectType];
            n_action[(param.type?'outputs':'inputs')][param.id] = param_types[param.id_objectType];
        }
    }
    
    //Make components and actions into arrays again
    var result = [];
    for (var id in components) {
        result.push(components[id]);
        var acts = [];
        for (var a_id in components[id].actions) {
            acts.push(components[id].actions[a_id]);
        }
        components[id].actions = acts;
    }
    
    return result;
}
