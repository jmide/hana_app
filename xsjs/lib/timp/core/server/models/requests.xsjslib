$.import("timp.core.server","util");
var util = $.timp.core.server.util;
$.import("timp.core.server.orm","table");
var table_lib = $.timp.core.server.orm.table;
$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

$.import("timp.core.server.models","schema");
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

this.table = new table_lib.Table({
    component: 'CORE',
	name: schema.default + '."CORE::Requests"',
	fields: {
		id: new table_lib.AutoField({
			name: 'ID',
			auto: schema.default + '."CORE::Requests::ID".nextval', 
			type: $.db.types.INTEGER,
			pk: true
		}),
		instancesID: new table_lib.Field({
			name: 'ID_INSTANCES',
			type: $.db.types.INTEGER
		}),
		actionID: new table_lib.Field({
			name: 'ID_ACTION',
			type: $.db.types.INTEGER
		}),
		name: new table_lib.Field({
			name: 'NAME',
			type: $.db.types.NVARCHAR,
			dimension: 100
		}),
		startDate: new table_lib.Field({
			name: 'STARTDATE',
			type: $.db.types.TIMESTAMP
		}),
		endDate: new table_lib.Field({
			name: 'ENDDATE',
			type: $.db.types.TIMESTAMP
		}),
		assignatorID: new table_lib.Field({
			name: 'ID_ASSIGNATOR',
			type: $.db.types.INTEGER
		}),
		group: new table_lib.Field({
			name: 'GROUP',
			type: $.db.types.TINYINT,
			translate: {0 : false, 1 : true}
		}),
		anticipable: new table_lib.Field({
			name: 'ANTICIPABLE',
			type: $.db.types.TINYINT,
			translate: {0 : false, 1 : true}
		}),
		postponable: new table_lib.Field({
			name: 'POSTPONABLE',
			type: $.db.types.TINYINT,
			translate: {0 : false, 1 : true}
		}),
		json: new table_lib.Field({
			name: 'JSON',
			type: $.db.types.NCLOB,
			json: true
		}),
		status: new table_lib.Field({
			name: 'STATUS',
			type: $.db.types.TINYINT
			//translate: {0: 'in queue', 1: 'open', 2: 'in progress', 3: 'delayed', 4: 'failed', 5: 'cancelled', 6: 'completed', 7: 'disabled', 8: 'delegating', 9: 'unassigned', 10: 'blocked, 11: 'delegated', 12: 'appropriateed'}
		}),
		navigation: new table_lib.Field({
			name: 'NAVIGATION',
			type: $.db.types.NVARCHAR,
			dimension: 16
		}),
		policy: new table_lib.Field({
		    name: 'POLICY',
			type: $.db.types.TINYINT,
			translate: {0: 'CALENDARDAY', 1: 'WORKINGDAY'},
			default: 0
		}),
		idOperation: new table_lib.Field({
			name: 'ID_OPERATION',
			type: $.db.types.INTEGER
		}),
		idActivity: new table_lib.Field({
			name: 'ID_ACTIVITY',
			type: $.db.types.INTEGER
		}),
		idTask: new table_lib.Field({
			name: 'ID_TASK',
			type: $.db.types.INTEGER
		}),
		idObject: new table_lib.Field({
			name: 'ID_OBJECT',
			type: $.db.types.INTEGER
		}),
		idOldUser: new table_lib.Field({
			name: 'ID_OLD_USER',
			type: $.db.types.INTEGER,
			default: 0
		}),
		delegatedBy: new table_lib.Field({
			name: 'DELEGATED_BY',
			type: $.db.types.INTEGER,
			default: 0
		}),
		delegatedTo: new table_lib.Field({
			name: 'DELEGATED_TO',
			type: $.db.types.INTEGER,
			default: 0
		}),
		supervisor: new table_lib.Field({
			name: 'SUPERVISOR',
			type: $.db.types.INTEGER,
			default: 0
		}),
		delegatedTask: new table_lib.Field({
			name: 'DELEGATED_TASK',
			type: $.db.types.INTEGER,
			default: 0
		}),
		oldStatus: new table_lib.Field({
			name: 'OLD_STATUS',
			type: $.db.types.INTEGER,
			default: 0
		})
	}
});