var util = this;

// 
//      #--------------------------------------------------- CONSTANTS ---------------------------------------------------
//

$.messageCodes = [];
$.successMessageCodesActive = false;

// X-Frame-Options SAMEORIGIN
$.response.cookies.set('X-Frame-Options', 'DENY');
$.response.headers.set('X-Frame-Options', 'DENY');

//Set default language
if ($.request.parameters.get('lang')) {
	$.response.cookies.set('Content-Language', $.request.parameters.get('lang'));
} else {
	if (!$.request.cookies.get('Content-Language')) {
		$.response.cookies.set('Content-Language', 'ptrbr');
	}
}

// 
//      #--------------------------------------------------- ADDING TO $ ---------------------------------------------------
//

/**
 * @param   { string }    code    - Code of message
 * @param   { string }    error   - Error catched
 * @param   { string }    type    - Type of error
 */
$.logError = function(code, error, type) {
	$.messageCodes.push({
		code: code,
		type: type || 'E',
		error: util.parseError(error || code)
	});
};

const getQueryViewPlaceholder = function (placeholdersMap) {
	$.import('timp.core.server.libraries.external', 'lodash');
	let _ = $.timp.core.server.libraries.external.lodash;
	if (_.isNil(placeholdersMap) || _.isEmpty(placeholdersMap)) {
		throw 'No Valid Placeholders';
	}
	let query = `
		SELECT
			DISTINCT "KEY",
			"VALUE"
		FROM
			"${ $.schema.slice(1, -1) }"."CORE::CONFIGURATION" AS CONFIGS
		INNER JOIN "${ $.schema.slice(1, -1) }"."CORE::COMPONENT" AS COMPONENTS
			ON CONFIGS."COMPONENT_ID" = COMPONENTS."ID"
		WHERE (
			KEY IN (
				${placeholdersMap.map(elem => `'${elem}'`).toString()}
			)
			AND COMPONENTS."NAME" LIKE '%CORE%'
		)`;
	return query;
};

$.getViewPlaceholder = function(placeholders, jsonFormat, inputParameters, valuesMap) {
	$.import('timp.core.server.libraries.external', 'lodash');
	let _ = $.timp.core.server.libraries.external.lodash;
	let isNoJson = _.isNil(jsonFormat) || jsonFormat === false;
	let response = isNoJson ? '' : {};
	let connection;
	let movement;
	let company;
	let filial;
	let tax;
	let dateTo;
	if (_.isArray(inputParameters)) {
		movement = _.find(inputParameters, (param) => param.hanaName === 'DIRECAO_MOVIMENTO');
		company = _.find(inputParameters, (param) => ['EMPRESA'].indexOf(param.hanaName) !== -1);
		filial = _.find(inputParameters, (param) => ['FILIAL'].indexOf(param.hanaName) !== -1);
		tax = _.find(inputParameters, (param) => param.hanaName === 'TAX');
		dateTo = _.find(inputParameters, (param) => param.hanaName === 'IP_DATE_TO');
	}
	try {
		connection = $.hdb.getConnection({
			'sqlcc': 'timp.core.server.orm::SQLConnection',
			'pool': true
		});
		if (_.isNil(placeholders) || _.isEmpty(placeholders)) {
			throw 'No Placeholders';
		}
		// let query = 'SELECT DISTINCT "KEY", "VALUE"';
		let result = {};
		let tfpClient = '';
		let map = {
			'ECC::CLIENT': 'IP_MANDANTE',
			'ECC::FULLDATE': 'IP_DATE_FROM',
			'ECC::LANG': 'IP_LANGUAGE',
			'ECC::YEAR': 'IP_YEAR_FROM',
			'ECC::YEAR_2_DIGIT': 'IP_YEAR2_FROM',
			'USER_ID': 'USER_ID',
			'HANA_NAME': 'HANA_NAME'
		};
		let reverseMap = {
			'IP_MANDANTE': 'ECC::CLIENT',
			'IP_DATE_FROM': 'ECC::FULLDATE',
			'IP_LANGUAGE': 'ECC::LANG',
			'IP_YEAR_FROM': 'ECC::YEAR',
			'IP_YEAR2_FROM': 'ECC::YEAR_2_DIGIT'
		};
		let placeholdersMap = _.reduce(placeholders, function(ret, placeholder) {
			if (reverseMap[placeholder.hanaName] || reverseMap[placeholder]) {
				ret.push(reverseMap[placeholder.hanaName] || reverseMap[placeholder]);
			}
			return ret;
		}, []);
		if (_.isNil(placeholdersMap) || _.isEmpty(placeholdersMap)) {
			throw 'No Valid Placeholders';
		}
		// query += ' FROM "' + $.schema.slice(1, -1) + '"."CORE::CONFIGURATION" WHERE KEY IN (\'';
		// query += placeholdersMap.join('\', \'');
		// query += '\');';
		let query = getQueryViewPlaceholder(placeholdersMap);
		result = connection.executeQuery(query);
		result = JSON.parse(result.toString());
		result = addDynamicLanguage(result);
		result = addDynamicPlaceHolders(result, inputParameters);
		query = 'SELECT TOP 1 CLIENT FROM "' + $.schema.slice(1, -1) + '"."CORE::ADAPTER" WHERE "IS_ACTIVE" = 1 AND "IS_DEFAULT" = 1';
		tfpClient = connection.executeQuery(query)[0];
		if (!_.isNil(tfpClient) && !_.isEmpty(tfpClient) && !_.isNil(tfpClient.CLIENT)) {
			map['TDF::IP_MANDANTE_TDF'] = 'IP_MANDANTE_TDF';
			reverseMap.IP_MANDANTE_TDF = 'TDF::IP_MANDANTE_TDF';
			result[result.length] = {
				KEY: 'TDF::IP_MANDANTE_TDF',
				VALUE: tfpClient.CLIENT.toString()
			};
		}
		connection.commit();
		connection.close();
		if (!_.isNil(result) && !_.isEmpty(result)) {
			if (isNoJson) {
				response += ' ('; 
			}
			_.forEach(result, function(item) {
				let key = map[item.KEY];
				if (key === 'IP_DATE_FROM' && !_.isEmpty(inputParameters) && _.isArray(inputParameters)) {
					let date = _.filter(inputParameters, function(param) {
						return param && (param.hanaName === 'DT_E_S' || param.hanaName === 'IP_DATE_FROM') && param.value && true;
					})[0];
					if (date && date.value) {
						item.VALUE = date.value;
					}
				}
                if (valuesMap && valuesMap[key]) {
                    item.VALUE = valuesMap[key];
                }
				if (isNoJson) {
					response += '\'PLACEHOLDER\' = (\'$$' + key + '$$\', \'' + item.VALUE + '\'), ';
				} else {
					response[key] = item.VALUE;
				}
			});
			if (isNoJson) {
				if (!$.lodash.isNil(movement) && !$.lodash.isEmpty(movement)) {
					response += '\'PLACEHOLDER\' = (\'$$DIRECAO_MOVIMENTO$$\', \'' + (movement.value || '*') + '\')';
				} else {
					response = response.substring(0, response.length - 2);
					if (!$.lodash.isNil(company) && !$.lodash.isEmpty(company)) {
						company.value = Array.isArray(company.value) ? company.value.map(function(item) {
							return '\'\'' + item + '\'\'';
						}).join(',') : '\'\'' + company.value + '\'\'';
						response += ', \'PLACEHOLDER\' = (\'$$IP_EMPRESA$$\', \'' + (company.value) + '\'), ';
					}
					if (!$.lodash.isNil(filial) && !$.lodash.isEmpty(filial)) {
						filial.value = Array.isArray(filial.value) ? filial.value.map(function(item) {
							return '\'\'' + item + '\'\'';
						}).join(',') : '\'\'' + filial.value + '\'\'';
						response += '\'PLACEHOLDER\' = (\'$$IP_FILIAL$$\', \'' + (filial.value) + '\')';
					}
					if (!$.lodash.isNil(tax) && !$.lodash.isEmpty(tax)) {
						tax.value = Array.isArray(tax.value) ? tax.value.map(function(item) {
							return '\'\'' + item + '\'\'';
						}).join(',') : '' + tax.value + '';
						response += ', \'PLACEHOLDER\' = (\'$$TAX$$\', \'' + (tax.value) + '\')';
					}
					if (!$.lodash.isNil(dateTo) && !$.lodash.isEmpty(dateTo)) {
					    if (valuesMap !== undefined && valuesMap.IP_DATE_TO !== undefined) {
					        dateTo.value = valuesMap.IP_DATE_TO || dateTo.value;
					    } else {
                            dateTo.value = dateTo.value ? dateTo.value : '';					       
					    }
					    
                        if (dateTo.value) {
                            dateTo.value = Array.isArray(dateTo.value) ? dateTo.value.map(function(item) {
                                return '\'\'' + item + '\'\'';
                            }).join(',') : '' + dateTo.value + '';
                            response += ', \'PLACEHOLDER\' = (\'$$IP_DATE_TO$$\', \'' + (dateTo.value) + '\')';
                        }
					}
				}
				response += ') ';
			}
		}
	} catch (e) {
		if (!_.isNil(connection)) {
			connection.commit();
			connection.close();
		}
		response = isNoJson ? '' : {};
	}
	return response;
};

let addDynamicLanguage = function (results) {
    let keys = Object.keys(results);
    let index = keys.findIndex( (key) => results[key] && results[key].KEY === 'ECC::LANG');
    
    if (index === -1){
        return results;
    }

    results[index].VALUE = getPlaceHolderLanguage();
    return results;
};

let addDynamicPlaceHolders = function (results, inputParameters) {
    if (!Array.isArray(inputParameters)){
        return results;
    }    
    let acceptedInputs = ['HANA_NAME', 'USER_ID'];
    let _inputs = inputParameters.filter( (parameter) => {
        let isValid = acceptedInputs.indexOf(parameter.hanaName) != -1;
        let hasValue = parameter.defaultValue != undefined || parameter.value != undefined;
        isValid = isValid && hasValue;
        return isValid;
    });
    _inputs.forEach( (parameter) => {
        results[parameter.hanaName] = {
            KEY: parameter.hanaName,
            VALUE: parameter.value || parameter.defaultValue
        };
    });
    return results;
};

let getPlaceHolderLanguage = function () {
    let lang = $.request.cookies.get('Content-Language');
    lang = lang === 'ptrbr' ? 'P' : 'E';
    return lang;
};

/**
 * Shorten way of using $.response.setBody
 * Does not give an error when receives an object, but converts it using JSON.stringify
 * @example
 *      $.res( < whatever > )
 * @param   { * }   value   - An Object to
 * @param   { * }   indent  -
 * @returns { string }   - Modified value
 */
$.res = function(value, indent) {
	if (value && typeof value === 'object' && value.constructor.name === 'ArrayBuffer') {
		$.response.setBody(value);
	} else if (indent && value && typeof value !== 'string') {
		value = JSON.stringify(value, undefined, indent);
		$.response.setBody(value);
	} else {
		value = util.str(value);
		$.response.setBody(value);
	}
	return value;
};

$.getHighPerformanceBRBEngineActivity = function() {
	let result;
	let connection;
	let highPerformanceBRBEngineActivity = false;
	try {
		connection = $.hdb.getConnection({
			'sqlcc': 'timp.core.server.orm::SQLConnection',
			'pool': true
		});
		let schema = $.schema.slice(1, -1);
		let query = 'SELECT KEY, VALUE FROM "'.concat(schema, '"."CORE::CONFIGURATION" WHERE KEY = \'TIMP::HighPerformanceBRBEngine\'');
		result = connection.executeQuery(query);
		result = JSON.parse(result.toString());
		highPerformanceBRBEngineActivity = $.lodash.filter(result, function(e) {
			return e.VALUE === 'true';
		}).length > 0;
		connection.commit();
		connection.close();
	} catch (e) {
		if (connection) {
			connection.commit();
			connection.close();
		}
	}
	return highPerformanceBRBEngineActivity;
};

$.getHighPerformanceBRBEngineRulesActivity = function() {
	let result;
	let connection;
	let highPerformanceBRBEngineRulesActivity = false;
	try {
		connection = $.hdb.getConnection({
			'sqlcc': 'timp.core.server.orm::SQLConnection',
			'pool': true
		});
		let schema = $.schema.slice(1, -1);
		let query = `SELECT KEY, VALUE FROM ${schema}."CORE::CONFIGURATION" WHERE KEY = 'TIMP::HighPerformanceBRBEngineRules'`;
		result = connection.executeQuery(query);
		
		let useThisEngine = [];
		for ( var name in result){
		    let isValid = result[name]['VALUE'];
		   if (isValid){
		       useThisEngine.push(true);
		   }
		}
		highPerformanceBRBEngineRulesActivity = useThisEngine.length > 0;
		connection.close();
	} catch (e) {
		if (connection) {
			connection.close();
		}
		$.logError('server error', e);
		highPerformanceBRBEngineRulesActivity = false;
	}
	return highPerformanceBRBEngineRulesActivity;
};

$.getBrbNewExecutorFlags = function () {
    let result;
	let connection;
	try {
		connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection','pool': true});
		let schema = $.schema.slice(1, -1);
        let query = `
            SELECT KEY, VALUE 
            FROM ${schema}."CORE::CONFIGURATION" 
            WHERE KEY in (
                'TIMP::HighPerformanceBRBEngine', 
                'TIMP::HighPerformanceBRBEngineRules',
                'TIMP::HighPerformanceBRBEngineBCB'
            )
        `;
		result = connection.executeQuery(query);
		
		let map = {
		    'TIMP::HighPerformanceBRBEngine': 'highPerformanceBRBEngine',
		    'TIMP::HighPerformanceBRBEngineRules': 'highPerformanceBRBEngineRules',
		    'TIMP::HighPerformanceBRBEngineBCB': 'highPerformanceBRBEngineBCB'
		};
		let newMap = {};
		for ( let index in result){
		    let key = map[result[index]['KEY']];
		    let isValid = getCookie(key) != undefined ? getCookie(key) : result[index]['VALUE'];
		    
		    if (isValid === 'choose'){
		        newMap[key + 'UserChoose'] = true;
		    }
		    if (key){
		        let _key = isValid === 'choose' ? key +  'UserChoose' : key;
		        newMap[_key] = isValid === 'choose' ? true : false; 
		        newMap[key] = isValid === 'false' || _key !== key ? false : Boolean(isValid); 
		    }
		}
		result = newMap;
		connection.close();
	} catch (e) {
		if (connection) {
			connection.close();
		}
		$.logError('server error', e);
		result = {};
	}
	return result;
};
// 
//      #--------------------------------------------------- EDITING PROTOTYPE ---------------------------------------------------
//

let getCookie = function (name) {
    let value = $.request.cookies.get(name);
    return  value == 0 || value == 1 ? Number(value) : undefined;
};
//Forcing the toJSON to keep the Timezone information
Date.prototype.toJSON = Date.prototype.toString;

Date.prototype.toSAPDate = function() {
	var year = String(this.getFullYear());
	var month = String(this.getMonth() + 1);
	month = month.length === 1 ? '0' + month : month;
	var date = this.getDate().toString();
	date = date.length === 1 ? '0' + date : date;
	return year + month + date;
};

Date.toTimpDate = function(_date) {
	if (!_date) {
		return '#';
	}

	if (typeof _date === 'object') {
		_date = _date.toString();
	}

	_date = new Date(_date);
	if (isNaN(_date)) {
		return '#';
	}

	var parts = _date.toString().split(' ');
	var months = {
		'Jan': '01',
		'Feb': '02',
		'Mar': '03',
		'Apr': '04',
		'May': '05',
		'Jun': '06',
		'Jul': '07',
		'Aug': '08',
		'Sep': '09',
		'Oct': '10',
		'Nov': '11',
		'Dec': '12'
	};
	return [parts[2], months[parts[1]], parts[3]].join('/');
};

//Forcing the string convertion of an Object to be it's JSON representation
Object.prototype.toString = function() {
	var seen = [];

	return JSON.stringify(this, function(key, val) {
		if (val !== null && typeof val === 'object') {
			if (seen.indexOf(val) !== -1) {
				try {
					JSON.stringify(val);
					return val;
				} catch (e) {
					return 'cyclic value!';
				}
			}
			seen.push(val);
		}
		return val;
	});
};

//Escape HTML characters
String.prototype.escapeHtml = function() {
	return String(this).replace(/<.*>/g, function(s) {
		return s.replace(/(<|>)/g, function(ele) {
			return {
				'<': '&lt;',
				'>': '&gt;'
			}[ele];
		});
	});
};

// 
//      #--------------------------------------------------- FUNCTIONS ---------------------------------------------------
//

/**
 * @example
 *  util.str('foo') // returns 'foo'
 *  util.str(foo)   // returns JSON.stringify(foo)
 * @param   { Object | string } value - A string or an object
 * @returns { string } - A string, an object in a string, type of the value parameter
 */
this.str = function(value) {
	if (typeof value === 'string') {
		return value;
	}
	var seen = [];

	return JSON.stringify(value, function(key, val) {
		if (val !== null && typeof val === 'object') {
			if (seen.indexOf(val) !== -1) {
				try {
					JSON.stringify(val);
					return val;
				} catch (e) {
					return 'cyclic value!';
				}
			}
			seen.push(val);
		}
		return val;
	}) || typeof value;
};

/**
 * This callback type is called `iterateCallback` and is displayed as a global symbol.
 * @callback iterateCallback
 * @param   { Object }  value   - Current value
 * @param   { string }  index   - Current index
 * @param   { Object | Object[] }  collection   - Collection that is being iterated
 */
/**
 * The 'each' function, iterates over an Object or Array
 * @example
 *  util.each({ foo: '12', bar: '13' }, function (v, k, a) { console.log(v, k, a, this.what); }, { what: 12 })
 *      //Prints:
 *      '12' 'foo' { foo: '12', bar: '13' } 12
 *      '13' 'bar' { foo: '12', bar: '13' } 12
 *  util.each(['foo', 'bar'], function (v, k, a) { console.log(v, k, a, this.what); }, { what: 12 })
 *      //Prints:
 *      'foo' 0 ['foo', 'bar'] 12
 *      'bar' 1 ['foo', 'bar'] 12
 * @param   { Object | Object[] }   collection  - Collection that you want to iterate
 * @param   { iterateCallback }     callback    - Function that use to modify the collection items
 * @param   { Object }              context     - Configurable scope
 * @returns { Object | Object[] }   - The modified collection
 */
this.each = function(collection, callback, context) {
	var res = [];
	var response;
	if (typeof context === 'undefined') {
		context = this;
	}
	if (typeof collection === 'undefined') {
		return res;
	} else if (typeof collection !== 'object') {
		response = callback.call(context, collection);
		if (typeof response !== 'undefined') {
			res.push(response);
		}
	} else if (collection instanceof[].constructor) {
		var len = collection.length;
		for (let index = 0; index < len; index++) {
			if (typeof index !== 'undefined' && typeof collection[index] !== 'undefined') {
				response = callback.call(context, collection[index], index, collection);
				if (typeof response !== 'undefined') {
					res.push(response);
				}
			}
		}
	} else {
		for (let prop in collection) {
			if (collection.hasOwnProperty(prop)) {
				if (typeof prop !== 'undefined' && typeof collection[prop] !== 'undefined') {
					response = callback.call(context, collection[prop], prop, collection);
					if (typeof response !== 'undefined') {
						res.push(response);
					}
				}
			}
		}
	}
	return res;
};
this.map = util.each;

/*
Your function must return an array like: [key, value]
mapObject(['foo','bar'],function (v, k, a) {
    return [v, v.length]
}) 
returns:
{
    'foo':3,
    'bar':3
}
*/
this.mapObject = function(collection, callback, context) {
	var res = {};
	var fnRes = [];
	var isValidAnswer = function(fnRe) {
		return ((typeof fnRe === 'object') && (typeof fnRe.length === 'number') && (fnRe.length === 2));
	};
	if (typeof context === 'undefined') {
		context = this;
	}
	if (typeof collection === 'undefined') {
		return res;
	} else if (typeof collection !== 'object') {
		fnRes = callback.call(context, collection);
		if (isValidAnswer(fnRes)) {
			res[fnRes[0]] = fnRes[1];
		}
	} else if (typeof collection.length === 'number') {
		var len = collection.length;
		for (let index = 0; index < len; index++) {
			if (index !== 'undefined' && collection[index] !== 'undefined') {
				fnRes = callback.call(context, collection[index], index, collection);
			}
			if (isValidAnswer(fnRes)) {
				res[fnRes[0]] = fnRes[1];
			}
		}
	} else {
		for (let prop in collection) {
			if (collection.hasOwnProperty(prop)) {
				if (prop !== 'undefined' && collection[prop] !== 'undefined') {
					fnRes = callback.call(context, collection[prop], prop, collection);
				}
				if (isValidAnswer(fnRes)) {
					res[fnRes[0]] = fnRes[1];
				}
			}
		}
	}
	return res;
};

/*
    flipDictionary({
        'foo':   12,
        'bar':   13.,
        'jimmy': 13
    })
    Returns:
        {
            12: 'foo',
            13: 'jimmy'
        }
*/
this.flipDictionary = function(oldDic) {
	var result = {};
	util.each(oldDic, function(value, key) {
		if (typeof value === 'string' && typeof key === 'string') {
			result[value] = key;
		}
	});
	return result;
};

/*
Wraps two functions together.
Was originally intended to help parse function parameters.

function Foo (options) {this.foo = options.foo;}
function ValidateFoo (options) {
    if(typeof options.foo !== 'number') {
        throw 'Invalid parameter for Foo: options.foo is ' + options.foo + ' (' + (typeof options.foo) + ')';
    }
}

Foo = FunctionWrapper(ValidateFoo, Foo);

Foo({})
    'Invalid parameter for Foo: options.foo is undefined (undefined)'

new Foo({})
    'Invalid parameter for Foo: options.foo is undefined (undefined)'
*/
this.FunctionWrapper = function(validate, callback) {
	var _callback = callback;
	callback = function() {
		validate.apply(this, arguments);
		_callback.apply(this, arguments);
	};
	return callback;
};

/*
Set of functions to parse datatypes:
*/
this.isValid = function(value) {
	return (typeof value !== 'undefined' && value !== null);
};

this.isNone = function(value) {
	return !util.isValid(value);
};

this.isNumber = function(value) {
	return (typeof value === 'number' && !isNaN(value));
};

this.isString = function(value) {
	return (typeof value === 'string');
};

this.isBoolean = function(value) {
	return (typeof value === 'boolean');
};

this.isObject = function(value) {
	return (util.isValid(value) && value instanceof Object);
};

this.isArray = function(value) {
	return (util.isValid(value) && value instanceof Array);
};

this.isDate = function(value) {
	return (util.isValid(value) && value instanceof Date);
};

this.isRegex = function(value) {
	return (util.isValid(value) && value instanceof RegExp || util.isString(value));
};

this.isFunction = function(value) {
	return (typeof value === 'function');
};

this.isDBType = function(value) {
	return (util.isValid(value) && util.flipDictionary($.db.types).hasOwnProperty(value));
};

/*
This is a recursive function that can validate objects, arrays, strings, regexes and numbers.
How to use:
    Parameter_name: Validate Function
    Parameter_name: {
        Parameter_name: Validate_Function
    }

Example:
    util.ParseAnything(isNumber, 'foo', 'page');
    exception: 'page isNumber('foo') returned false'

    util.ParseAnything([isNumber], [10, 12, 13, 'foo'], 'page');
    'page[3] isNumber('foo') returned false'

    var object_to_be_validated = {
        foo: '12',
        bar: {
            lorem: ['foo', 'bar', 'jimmy', 'cliff'],
            ipsum: {}
        }
    }
    util.ParseAnything({
        foo: isNumber,
        bar: {
            lorem: [isString],
            ipsum: isObject
        }
    }, object_to_be_validated, 'options');
    exception: 'options.foo isNumber('12') returned false'

    var object_to_be_validated = {
        foo: 12,
        bar: {
            lorem: ['foo', 'bar', 'jimmy', 'cliff'],
            dolor: [{inside: 'foo'}, {inside: 'bar'}, {inside: 'jimmy'}, {inside: 12}],
            ipsum: {}
        }
    }
    util.ParseAnything({
        foo: isNumber,
        bar: {
            lorem: [isString],
            dolor: [{inside: isString}],
            ipsum: isObject
        }
    }, object_to_be_validated, 'options');
    exception: 'options.bar.dolor[3].inside isString(12) returned false'
*/
this.ParseAnything = function(format, object, name) {
	if (!util.isValid(object)) {
		throw name + ' is undefined';
	}

	var fn;
	var property;
	if (util.isFunction(format)) {
		fn = format;
		var isValid2 = fn(object);
		if (!isValid2) {
			throw name + ' ' + fn.name + '(' + JSON.stringify(object) + ') returned false';
		}
	} else if (util.isArray(format) && format.length === 1) {
		fn = format[0];
		if (util.isArray(object)) {
			for (var i = 0; i < object.length; i++) {
				property = name + '[' + i + ']';
				util.ParseAnything(fn, object[i], property);
			}
		} else {
			throw property + ' isArray(' + JSON.stringify(object) + ') returned false';
		}
	} else if (util.isObject(format)) {
		for (var x in format) {
			property = name + '.' + x;
			if (!object.hasOwnProperty(x)) {
				throw name + ' has not attribute ' + JSON.stringify(x);
			}

			if (format.hasOwnProperty(x)) {
				util.ParseAnything(format[x], object[x], property);
			}
		}
	}
	return true;
};

/*
    JSON.stringify that won't break for circular references
*/
this.defuse = function(e, depth) {
	if (typeof depth === 'undefined') {
		depth = 100;
	}
	if (depth === 0) {
		return 'util.defuse: too deep';
	}
	depth -= 1;
	var bones = {};
	if (typeof e === 'undefined') {
		return ({}).u;
	}
	try {
		if (typeof e === 'object' && typeof e.toArray === 'function') {
			e = e.toArray();
		}
	} catch (e1) {
		//return undefined;
	}
	if (e instanceof[].constructor) {
		bones = [];
	}
	for (var x in e) {
		if (e.hasOwnProperty(x)) {
			if (typeof e[x] === 'function') {
				bones[x] = 'function';
			} else if (typeof e[x] === 'object') {
				try {
					bones[x] = util.defuse(e[x], depth);
				} catch (_e) {
					bones[x] = '__impossible__';
				}
			} else {
				bones[x] = e[x];
			}
		} else {
			try {
				if (bones[x] instanceof Date) {
					bones[x] = bones[x].toString();
				} else if (typeof e[x] === 'object') {
					bones[x] = util.defuse(e[x], depth);
				} else {
					bones[x] = e[x];
				}
			} catch (error1) {
				bones[x] = '__hidden__';
			}
		}
	}
	return bones;
};

this.parseError = function(error) {
	var seen = [];
	if (typeof error === 'object' && error !== null && typeof error.hasOwnProperty === 'function' &&
		error.hasOwnProperty('fileName') && error.hasOwnProperty('lineNumber') && error.hasOwnProperty('columnNumber')) {
		return JSON.stringify({
			'fileName': error.fileName,
			'lineNumber': error.lineNumber,
			'columnNumber': error.columnNumber,
			'Error Msg': error.toString()
		});
	}
	if (typeof error === 'object') {
		return JSON.stringify(error, function(key, value) {
			if (value !== null && typeof value === 'object') {
				if (seen.indexOf(value) !== -1) {
					try {
						JSON.stringify(value);
						return value;
					} catch (e1) {
						return 'cyclic value!';
					}
				}
				seen.push(value);
			}
			return value;
		}, 4) + '\n' + error.toString();
	} else {
		return error;
	}
};

$.parseError = this.parseError;

/*
    Will print on XSJS trace all the parameters it receives:
    debug(1,2,3,4,5,6,7,7...);
    trace: [1,2,3,4,5,6,7,7...];
*/
function debug() {
	var error = {};
	//  var error = util.parseError(
	//      util.each(arguments, function (v) {
	//          return v;
	//      })
	//  )
	//  $.trace.error(error);
	return error;
}
this.debug = debug;

/*
Declare is a function wrapper.
There is one line boilerplate code that must be inserted into the function iself:
    e.g
        function Foo () {
            Foo._validate(arguments);

            //Here goes your code
        }

var Jimmy = Declare({
    options: {
        foo: isNumber,
        bar: {
            lorem: [isString],
            dolor: [{inside: isString}],
            ipsum: isObject
        }
    },
    pages: [isNumber],
    table: isString
}, function Jimmy (options, pages, table) {
    Jimmy._validate(arguments);

    this.foo = options.foo;
    this.bar = options.bar;
    for (var i = 0; i < pages.length; i++) {
        pages[i]
    }
    this.table = table;
});

Jimmy({
    foo: 12,
    bar: {
        lorem: ['asd', 'bsd', 'fsd'],
        dolor: [{inside: 'Nanana'}],
        ipsum: {}
    }
}, [12, 12, 12], 'tablename');

var jack = new Jimmy({
    foo: 12,
    bar: {
        lorem: ['asd', 'bsd', 'fsd'],
        dolor: [{inside: 'Nanana'}],
        ipsum: {}
    }
}, [12, 12, 12], 'tablename');

console.log(Jimmy._doc);
    Usage: Jimmy(options, pages, table)
    Where: 
        options = {
            foo : (number),
            bar : {
                lorem : [(string), ..., (string)],
                dolor : [{
                    inside : (string)
                }, ..., {
                    inside : (string)
                }],
                ipsum : (object)
            }
        }
        pages = [(number), ..., (number)]
        table = (string)
*/
function Declare(args, callback) {
	var parse = [];
	var _args = [];
	for (let prop in args) {
		if (args.hasOwnProperty(prop)) {
			_args.push(prop);
			parse.push([prop, args[prop]]);
		}
	}
	var _doc = 'Usage: ' + callback.name + '(' + _args.join(', ') + ')\nWhere: \n';
	for (let i = 0, len = parse.length; i < len; i++) {
		let name = parse[i][0];
		let rule = parse[i][1];
		let partial = SelfDocument(rule, name);
		_doc += '\t' + partial.replace(/\n/g, '\n\t') + '\n';
	}

	callback._validate = function(arg) {
		try {
			for (let i = 0, len = parse.length; i < len; i++) {
				var name2 = parse[i][0];
				var rule2 = parse[i][1];
				util.ParseAnything(rule2, arg[i], name2);
			}
		} catch (error) {
			throw 'Wrong parameteres for function ' + callback.name + '\n\t' + util.parseError(error) + '\n' + _doc + '\nReceived: ' + util.parseError(
				args);
		}
	};

	callback._doc = _doc;

	return callback;
}
this.Declare = Declare;

this.encrypt = function(plainText) {
	$.import('timp.core.server', 'cryptoAES');
	const CryptoJS = $.timp.core.server.cryptoAES.CryptoJS;
	var key = CryptoJS.enc.Base64.parse('253D3FB468A0E24677C28A624BE0F939');
	var iv = CryptoJS.enc.Base64.parse('                ');
	var encrypted = CryptoJS.AES.encrypt(plainText, key, {
		iv: iv
	});
	return encrypted;
};

this.decrypt = function(encrypted) {
	$.import('timp.core.server', 'cryptoAES');
	const CryptoJS = $.timp.core.server.cryptoAES.CryptoJS;
	var key = CryptoJS.enc.Base64.parse('253D3FB468A0E24677C28A624BE0F939');
	var iv = CryptoJS.enc.Base64.parse('                ');
	var decrypted = CryptoJS.AES.decrypt(encrypted, key, {
		iv: iv
	});
	return decrypted.toString(CryptoJS.enc.Utf8);
};

/**
 * @param { string } string - String with currency (money) format E.g. '114,20' or '114.20'
 * @returns { !number } - Decimal format number
 */
this.currencyToDecimalFormat = function(string) {
	let afterComma = string.indexOf(',');
	let afterDot = string.indexOf('.');
	if (afterDot >= 0 && afterComma >= 0 && afterComma > afterDot) {
		let arrayNumber = string.split(',');
		let integer = arrayNumber[0].replace('.', '');
		let completeNumber = integer + '.' + arrayNumber[1];
		return parseFloat(completeNumber);
	} else if (afterDot >= 0 && afterComma >= 0 && afterDot > afterComma) {
		let integer2 = string.replace(',', '');
		return parseFloat(integer2);
	} else if (afterDot >= 0 && afterComma < 0) {
		return parseFloat(string);
	} else if (afterComma >= 0 && afterDot < 0) {
		let number = string.replace(',', '.');
		return parseFloat(number);
	}
	return Number(string);
};

/**
 * @example
 *  roundNumber(3.33333);       // returns 3
 *  roundNumber(3.77777);       // returns 4
 *  roundNumber(3.77777, 2);        // returns 3.78
 *  roundNumber(763.77777, -1);     // returns 760
 *
 * @param { number } number - The number to round
 * @param { number } precision - An integer to find precision after decimal point, default = 0
 * @returns { number } The number rounded to the precision decimal
 */
this.roundNumber = function(number, precision) {
	const _precision = precision || 0;
	const multiplier = Math.pow(10, _precision);
	return Math.round(number * multiplier) / multiplier;
};

/* 
Workaround for the problems TimpData caused by wrapping every POST in a 'object' argument
Usage:
    util.getParam('my_parameter');
*/
this.getParam = function getParam(name) {
	var resp = $.request.parameters.get(String(name));
	if (!util.isValid(resp)) {
		resp = $.request.parameters.get('object');
		if (util.isValid(resp)) {
			try {
				while (util.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp.hasOwnProperty(name) ? resp[name] : undefined;
			} catch (e) {
				return resp;
			}
		}
	} else {
		if (util.isValid(resp)) {
			try {
				while (util.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp;
			} catch (e) {
				return resp;
			}
		}
	}
	return resp;
};

//This function has the objective of sorting objects by desired property and order type. It returns an array.
//Obj is object you want to sort.
//propertyToSort is the property you want to sort, if not mentioned it will only convert the Object to an Array.
//orderType is the order you want to sort. By default is ascending order, but if its 'desc' it will be descending order.
//var obj = {'28':{'fieldId':28,'position':0},'30':{'fieldId':30,'position':1},'31':{'fieldId':31,'position':3},'174':{'fieldId':174,'position':2}};
//var result = sortObjectsToArray(obj, 'position', 'desc');
//It will become: [{'fieldId':31,'position':3},{'fieldId':174,'position':2},{'fieldId':30,'position':1},{'fieldId':28,'position':0}]

function sortObjectsToArray(object, propertyToSort, orderType) {
	var result = [];
	for (let prop in object) {
		if (object.hasOwnProperty(prop)) {
			result.push(object[prop]);
		}
	}
	result.sort(function(a, b) {
		if (typeof orderType !== 'undefined' && orderType.toUpperCase() === 'DESC') {
			return b[propertyToSort] - a[propertyToSort];
		} else {
			return a[propertyToSort] - b[propertyToSort];
		}
	});
	return result;
}
this.sortObjectsToArray = sortObjectsToArray;

/*
This is a recursive function that generates documentation based on the ParseAnything behaviour.
How to use:
    Parameter_name: Validate Function
    Parameter_name: {
        Parameter_name: Validate_Function
    }

Example:
    SelfDocument({
        foo: isNumber,
        bar: {
            lorem: [isString],
            dolor: [{inside: isString}],
            ipsum: isObject
        }
    }, 'options');
    Returns:
        options = {
            foo : (number),
            bar : {
                lorem : [(string), ..., (string)],
                dolor : [{
                    inside : (string)
                }, ..., {
                    inside : (string)
                }],
                ipsum : (object)
            }
        }

    SelfDocument([isNumber], 'pages');
    Returns:
        pages = [(number), ..., (number)]

    SelfDocument(isString, 'table');
    Returns:
        table = (string)
*/
function SelfDocument(format, name) {
	return name + ' = ' + SelfDocument._iter(format);
}
SelfDocument.translations = {
	'isValid': '(not undefined)',
	'isNumber': '(number)',
	'isString': '(string)',
	'isObject': '(object)',
	'isArray': '(array)',
	'isDate': '(date)',
	'isRegex': '(regex)',
	'isFunction': '(function)'
};
SelfDocument._iter = function(format) {
	var translation;
	var sub;
	if (util.isFunction(format)) {
		if (SelfDocument.translations.hasOwnProperty(format.name)) {
			translation = SelfDocument.translations[format.name];
		} else {
			translation = '(' + format.name + ')';
		}
		return translation;
	} else if (util.isArray(format) && format.length === 1) {
		sub = SelfDocument._iter(format[0]);
		translation = '[' + sub + ', ..., ' + sub + ']';
		return translation;
	} else if (util.isObject(format)) {
		sub = [];
		for (var x in format) {
			if (format.hasOwnProperty(x)) {
				var _sub = '\t' + x + ' : ' + SelfDocument._iter(format[x]).replace(/\n/g, '\n\t');
				sub.push(_sub);
			}
		}
		translation = '{\n' + sub.join(',\n') + '\n}';
		return translation;
	}
	return '';
};
this.SelfDocument = SelfDocument;

function JSTypeConverter() {
	this.types = {
		1: function(n) {
			var v = convertNumber(n);
			if (v < 256) {
				return v;
			}
			return false;
		},
		2: convertNumber,
		3: convertNumber,
		4: convertNumber,
		5: convertNumber,
		6: convertNumber,
		7: convertNumber,
		8: String,
		9: String,
		10: String,
		11: String,
		12: String,
		13: String,
		14: convertDate,
		15: convertDate,
		16: convertDate,
		25: forceStringify,
		26: String,
		27: String,
		47: convertNumber,
		51: String,
		52: String,
		55: String,
		62: convertDate
	};

	function convertNumber(n) {
		if (n === null || typeof n === 'undefined' || n === '') {
			return null;
		}
		var value = parseFloat(n);
		if (isNaN(value)) {
			value = undefined;
		}
		return value;
	}

	function convertDate(n) {
		if (typeof n !== 'string' || !n) {
			return false;
		} else {
			//return new Date(n).toISOString().split('T')[0];
			//this.debug('WHY? Date', n, typeof n);
			return new Date().toString();
		}
	}

	function forceStringify(n) {
		return typeof n === 'string' && n || JSON.stringify(n);
	}
	this.isValid = function(value, type) {
		var val = this.parseValue(value, type);
		if (val === 0) {
			return true;
		} else if (val === '') {
			return true;
		} else if (val === null) {
			return true;
		}
		return !!val;
	};
	this.parseValue = function(value, type) {
		if (typeof this.types[type] !== 'function') {
			return value;
		}
		var val = this.types[type](value);
		return val;
	};
}
this.JSTypeConverter = JSTypeConverter;

/*
PublicMethod([{ //If you want an id
    id: isNumber,
},{ //If you want an object
    object: isObject
},{ //If you want an object with an id
    object: {
        id: isNumber
    }
}], function (options, case) {
    switch (case) {
        case 1:
            return objTable.READ({
                where: [{field: 'id', oper:'=', value: options}]
            });
        case 2:
            return objTable.CREATE(options);
        case 3:
            return objTable.UPDATE(options);
        default:
            return undefined;
    }
})
*/
// function PublicMethod(args, fn) {
//     return function () {
//         function validate (arg) {
//             for (var y in arg) {
//                  if (arg.hasOwnProperty(y)) {
//                      var item = object[y];

//                  }
//                 }
//             try {
//                 util.ParseAnything(arg);
//                 return true;
//             } catch (e) {
//                 return false;
//             }
//         }
//         if (util.isArray(args)) {
//             for (var x = 0; x < args.length; x++) {

//                 validate(args[x], getParam())
//             }
//         }
//     }
// }

//Added a URL Router
/*
Parameters:
    routes: [
        {url: <Regex>, view: <function>},
        ...
    ],
    default: <function>
Notice:
    Keep the regex in the most restrictive order: '/foo/bar/' before '/foo/(\\w+)/'
Usage example:
    function Foo () {
        this.respond = function () {
            $.response.status = $.net.http.OK;
            $.response.contentType = 'text/javascript';
            $.response.setBody(JSON.stringify(arguments));
        }
        this.create = function (id) {
            this.respond('Foo.create: '+id);
        }
        this.update = function (id) {
            this.respond('Foo.update: '+id);
        }
        this.read = function (id) {
            this.respond('Foo.read: '+id);
        }
        this.remove = function (id) {
            this.respond('Foo.delete: '+id);
        }
    }
    var foo = new Foo();
    var router = new util.URLRouter({
        routes: [
            {url: '^create/$', view: foo.create},
            {url: '^read/(\\d+)/$', view: foo.read},
            {url: '^read/all/$', view: foo.read},
            {url: '^update/(\\d+)/$', view: foo.update},
            {url: '^delete/(\\d+)/$', view: foo.remove}
        ],
        default: function () {
            $.response.status = 404; //404
            $.response.contentType = 'text/plain';
            var response = 'Unknown URL\n';
            var urls = ['create/', 'read/:id/', 'read/all/', 'update/:id/', 'delete/:id/'];
            for (var i = 0; i < urls.length; i++) {
                response += urls[i]+'\n';
            }
            $.response.setBody(response);
        }
    });
    router.parseURL(foo);
*/
function URLRouter(options) {
	//URLRouter._validate(arguments);
	this.init = function(opts) {
		this.routes = opts.routes;
		this.default = opts.default;
		return this;
	};
	this.selfDocument = function(lb) {
		lb = lb || '<br>';
		var content = '<pre>======================= SELF DOCUMENTATION =======================';
		for (var i = 0; i < this.routes.length; i++) {
			var route = this.routes[i];
			content += lb + route.url.replace(/^\^/, '/').replace(/\$$/, '');
			for (var y = 0; route.hasOwnProperty('test') && y < route.test.length; y++) {
				// if ('data' in route.test[y]) {
				if (util.isArray(route.test[y]) && route.test[y].length === 2) {
					content += lb + '\treceives: ' + lb + '\t\t' + util.str(route.test[y][0], undefined, 4);
					content += lb + '\t returns: ' + lb + '\t\t' + util.str(route.test[y][1], undefined, 4);
				} else {
					content += lb + '\treceives: ' + lb + '\t\t' + util.str(route.test[y], undefined, 4);
				}

				// }
			}
		}
		return content + '</pre>';
	};
	this.init(options);
	return this;
}
URLRouter.prototype.parseURL = function(ctx, component) {
	//Default response type
	$.response.contentType = 'application/json';
	var url = $.request.queryPath;

	var response;
	for (var i = 0; i < this.routes.length; i++) {
		var route = this.routes[i];
		var result = url.match(route.url);
		if (result) {
			if (typeof route.ctx !== 'undefined') {
				ctx = route.ctx;
			}
			var parameters = result.slice(1);
			if (!route.hasOwnProperty('view') || typeof route.view !== 'function') {
				throw 'Misconfigured route: ' + i + '\nThe assigned view is not a function.';
			}
			$.response.status = 200;
			ctx.__URLROUTER = true; //Let the controller know it's being called from the UI

			var s = util.getParam('object');

			if (typeof s !== 'undefined') {
				parameters = parameters.concat([s]);
			}

			var error = false;
			parameters.forEach(function(item) {
				if (typeof item !== 'object') {
					return;
				}
				Object.keys(item).forEach(function(key) {
					if (typeof item[key] === 'string' && item[key].toLowerCase().match(/eval\s*\((.)*\)/g)) {
						error = true;
					}
					if (!(item.comp === 'TIS' && key === 'xml')) {
						item[key] = typeof item[key] === 'string' ? item[key].escapeHtml() : item[key];
					}

					if (typeof item[key] === 'object') {
						var tmp = JSON.stringify(item[key]);
						if (tmp.toLowerCase().match(/eval\s*\((.)*\)/g)) {
							error = true;
						}
						if (!(item.comp === 'TIS' && key === 'xml')) {
							item[key] = JSON.parse(tmp.escapeHtml());
						}
					}
				});
			});
			if (error) {
				$.response.status = 404;
				$.res({
					messageCodes: [{
						type: 'E',
						code: 'CORE000013',
						errorInfo: util.parseError('Expression "eval" found')
                    }]
				});
				return;
			}
			let valid;
			let privileges = ($.lodash.isArray(route.privileges) && !$.lodash.isNil(route.privileges)) ? route.privileges : [];
			if (!$.lodash.isEmpty(privileges)) {
				if (!$.lodash.isString(component)) {
					component = '';
				}
				component = $.lodash.toUpper(component);
				valid = true;
			}
			if ($.lodash.isEmpty(privileges) || valid) {
				response = route.view.apply(ctx, parameters);
			} else {
				$.response.status = $.net.http.FORBIDDEN;
				response = {
					messageCodes: $.messageCodes,
					results: {}
				};
				$.res(response);
			}

			if (typeof this.errorHandler === 'function' && $.messageCodes.length) {
				this.errorHandler(response);
			} else {
				$.res(response);
			}
			return response;
		}
	}
	response = this.default.apply(ctx || this) || '';
	response = '<pre>Invalid URL</pre>';
	$.response.status = 404;
	//response += this.selfDocument(); //Removed valid endpoints list for certification security vulnerability purposes
	$.response.contentType = 'text/html';
	$.res(response);
	return response;
};
this.URLRouter = URLRouter;

// this.URLRouter = Declare({ = {;
//  options: {
//      'routes': [{url: isRegex, view: isFunction}],
//      'default': isFunction
//  }
// }, URLRouter);

//Uses the URLRouter to add some functionality to the alltax.alltax.base.controller.BasicController
/*
Parameters:
    controller: <alltax.alltax.base.controller.BasicController>
Usage example:
    $.import('alltax.alltax','util');
    var util = $.alltax.alltax.util;
    try {
        $.import('alltax.alltax.base','controller');
        var controller = $.alltax.alltax.base.controller;
        $.import('bre.server.model','rule');
        var model = $.bre.server.model.rule.model;
        $.import('bre.server.model','datastructure');
        var datastructure = $.bre.server.model.datastructure.model;
        
        var controller = new controller.BasicController({
            model: model
        });
        
        //controller.parseRequest();
        util.RestWrapper(controller);
    } catch (e) {
        $.response.setBody(util.parseError(e));    
    }
*/
function RestWrapper(controller) {
	var router = new URLRouter({
		routes: [{
				url: '^create/$',
				view: controller.CREATE
            },
			{
				url: '^read/(\\d+)/$',
				view: controller.READ
            },
			{
				url: '^read/$',
				view: controller.READ
            },
			{
				url: '^update/(\\d+)/$',
				view: controller.UPDATE
            },
			{
				url: '^delete/(\\d+)/$',
				view: controller.DELETE
            },
			{
				url: '^/?$',
				view: controller.parseRequest
            }
        ],
		'default': function() {
			$.response.status = 404; //404
			$.response.contentType = 'text/plain';
			var resp = 'Unknown URL\n';
			var urls = ['create/', 'read/:id/', 'read/', 'update/:id/', 'delete/:id/'];
			for (var i = 0; i < urls.length; i++) {
				resp += urls[i] + '\n';
			}
			$.response.setBody(resp);
		}
	});

	var response = router.parseURL(controller);

	if (typeof response !== 'undefined') {
		if (typeof response === 'object') {
			var strRes = JSON.stringify(response);
			$.response.status = 200;
			$.response.contentType = 'application/json';
			if ($.request.parameters.get('callback')) {
				$.response.setBody($.request.parameters.get('callback') + '(' + strRes + ')');
			} else {
				$.response.setBody(strRes);
			}
		}
	}
	return undefined;
}
this.RestWrapper = RestWrapper;

// $.getTraceActivity = function () {  
//     $.import('timp.core.server.controllers.refactor','configuration');
//     const configurationController = $.timp.core.server.controllers.refactor.configuration;
//     let systemConfigurationResponse = configurationController.getSystemConfiguration({
//         keys:['TIMP::TracerActivity'],
//         componentName: "CORE"
//     });
//     var tracerActivity = $.lodash.filter(systemConfigurationResponse,function(e){
//         return e.value === "true";
//     }).length > 0;
//     return tracerActivity;
// };
$.getTraceActivity = function() {
	let result;
	let connection;
	let tracerActivity = false;
	try {
		connection = $.hdb.getConnection({
			'sqlcc': 'timp.core.server.orm::SQLConnection',
			'pool': true
		});
		let schema = $.schema.slice(1, -1);
		let query = 'SELECT KEY, VALUE FROM "'.concat(schema, '"."CORE::CONFIGURATION" WHERE KEY = \'TIMP::TracerActivity\'');
		result = connection.executeQuery(query);
		result = JSON.parse(result.toString());
		tracerActivity = $.lodash.filter(result, function(e) {
			return e.VALUE === 'true';
		}).length > 0;
		connection.commit();
		connection.close();
	} catch (e) {
		if (connection) {
			connection.commit();
			connection.close();
		}
	}
	return tracerActivity;
};

//Overwrite the $.import to solve the infinite loop condition
// if (!$.hasOwnProperty('_importList')) {
//     $._import = $.import;
//     $._importList = {};
//     $._importDebug = ['importing files'];
//     $.import = function (pack, file) {
//         var key = pack + '.' + file;
//         $._importDebug.push(key);
//         try {
//             if (!$._importList.hasOwnProperty(key)) {
//                 $._importList[key] = $._import(pack, file);
//                 if (!$._importList.hasOwnProperty(key)) {
//                     $._importList[key] = {};
//                 }
//             }
//         } catch (e) {
//             $._importDebug.push('failed', util.parseError(e));
//             throw $._importDebug.join('\n');
//         }
//         return $._importList[key];
//     }
// } 

// 
//      #--------------------------------------------------- DEPRECATED ---------------------------------------------------
//

//Just call this function and the setBody will handle the callback itself
/*
Usage example:
    $.import('alltax.alltax','util');
    var util = $.alltax.alltax.util;
    util.CallbackDecorator();

    $.response.status = 200;
    $.response.contentType = 'text/javascript';
    $.response.setBody(JSON.stringify({foo:12}));
Case mything.xsjs:
    {'foo':12}
Case mything.xsjs?callback=foo:
    foo({'foo':12})
*/
function CallbackDecorator() { //DEPRECATED
	//     $.response.callback = {
	//         setBody: $.response.setBody
	//     }
	//  $.response.setBody = function (body) {
	//      var callback = $.request.parameters.get('callback');
	//      try {
	//             if (!!callback) {
	//                 // $.response.contentType = 'application/javascript';
	//                 $.response.callback.setBody(callback+'('+body+')');
	//             } else {
	//                 // $.response.contentType = 'text/json';
	//                 $.response.callback.setBody(body);
	//             }
	//      } catch (e) {
	//          $.response.status = 500;
	//          $.response.callback.setBody('ERROR: $.response.setBody requires a string');
	//      }
	//      return body;
	//  }
}
this.CallbackDecorator = CallbackDecorator;