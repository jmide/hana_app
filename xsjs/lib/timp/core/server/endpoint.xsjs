$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

var routes = [];

$.import('timp.core.server.controllers', 'config');
var config = $.timp.core.server.controllers.config;
$.import('timp.core.server.controllers', 'privileges');
var privileges = $.timp.core.server.controllers.privileges;
$.import('timp.core.server.controllers', 'users');
var users = $.timp.core.server.controllers.users;
routes.push({ url: '^checkParametersByTask/$', view: privileges.checkParametersByTask, ctx: privileges });

try {
    $.import('timp.core.server.controllers', 'images');
    var images = $.timp.core.server.controllers.images;
    routes.push({ url: '^images/put/$', view: images.PUT, ctx: images });
    routes.push({ url: '^images/put64/$', view: images.PUT64, ctx: images });
    routes.push({ url: '^images/put/(\\d+)/$', view: images.PUT, ctx: images });
    routes.push({ url: '^images/get/(\\d+)/$', view: images.GET, ctx: images });
    routes.push({ url: '^images/delete/(\\d+)/$', view: images.DELETE, ctx: images });
    routes.push({ url: '^images/list/$', view: images.LIST, ctx: images });
    routes.push({ url: '^images/listImages/$', view: images.listImages, ctx: images });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers', 'attachments');
    var attachments = $.timp.core.server.controllers.attachments;
    routes.push({ url: '^attachments/put/$', view: attachments.PUT, ctx: attachments });
    routes.push({ url: '^attachments/upload/$', view: attachments.UPLOAD, ctx: attachments });
    routes.push({ url: '^attachments/upload/(\\d+)/$', view: attachments.UPLOAD, ctx: attachments });
    routes.push({ url: '^attachments/get/(\\d+)/$', view: attachments.GET, ctx: attachments });
    routes.push({ url: '^attachments/get/$', view: attachments.GET, ctx: attachments });
    routes.push({ url: '^attachments/delete/(\\d+)/$', view: attachments.DELETE, ctx: attachments });
    routes.push({ url: '^attachments/delete/$', view: attachments.DELETE, ctx: attachments });
    routes.push({ url: '^attachments/list/$', view: attachments.LIST, ctx: attachments });
    routes.push({ url: '^attachments/listFiles/$', view: attachments.list, ctx: attachments });
    routes.push({ url: '^getHelpFile/$', view: attachments.getHelpFile, ctx: attachments });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers', 'fileCRUDFNew');
    var fileCRUDFNew = $.timp.core.server.controllers.fileCRUDFNew;
    routes.push({ url: '^fileCRUDFNew/createFolder/$', view: fileCRUDFNew.createFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getFolder/$', view: fileCRUDFNew.getFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/updateFolder/$', view: fileCRUDFNew.updateFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/deleteFolder/$', view: fileCRUDFNew.deleteFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/copyFolder/$', view: fileCRUDFNew.copyFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/moveFolder/$', view: fileCRUDFNew.moveFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFolderTree/$', view: fileCRUDFNew.listFolderTree, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFolderTreeByObjectTypes/$', view: fileCRUDFNew.listFolderTreeByObjectTypes, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFolders/$', view: fileCRUDFNew.listFolders, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listSharedFolders/$', view: fileCRUDFNew.listSharedFolders, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/shareFolder/$', view: fileCRUDFNew.shareFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/unshareFolder/$', view: fileCRUDFNew.unshareFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getUsersFromGroup/$', view: fileCRUDFNew.getUsersFromGroup, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/addUserToGroup/$', view: fileCRUDFNew.addUsersToGroup, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/groupDeleted/$', view: fileCRUDFNew.groupDeleted, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/deleteUser/$', view: fileCRUDFNew.deleteUser, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/createFile/$', view: fileCRUDFNew.createFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getFile/$', view: fileCRUDFNew.getFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFilesByFolder/$', view: fileCRUDFNew.listFilesByFolder, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFilesByStatus/$', view: fileCRUDFNew.listFilesByStatus, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFilesByUser/$', view: fileCRUDFNew.listFilesByUser, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listFilesInTrash/$', view: fileCRUDFNew.listFilesInTrash, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/listSharedFiles/$', view: fileCRUDFNew.listSharedFiles, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/markFavorite/$', view: fileCRUDFNew.markFavorite, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/unmarkFavorite/$', view: fileCRUDFNew.unmarkFavorite, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/updateFileStatus/$', view: fileCRUDFNew.updateFileStatus, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/shareFile/$', view: fileCRUDFNew.shareFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/unshareFile/$', view: fileCRUDFNew.unshareFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getUsersGroupsFromShare/$', view: fileCRUDFNew.getUsersGroupsFromShare, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/deleteFile/$', view: fileCRUDFNew.deleteFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/restoreFile/$', view: fileCRUDFNew.restoreFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/copyFile/$', view: fileCRUDFNew.copyFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/moveFile/$', view: fileCRUDFNew.moveFile, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/updateOldFilesToNewVersion/$', view: fileCRUDFNew.updateOldFilesToNewVersion, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getMoveFolderRequiredInfo/$', view: fileCRUDFNew.getMoveFolderRequiredInfo, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/getMoveFileRequiredInfo/$', view: fileCRUDFNew.getMoveFileRequiredInfo, ctx: fileCRUDFNew });
    routes.push({ url: '^fileCRUDFNew/updateFileParent/$', view: fileCRUDFNew.updateFileParent, ctx: fileCRUDFNew });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers', 'fileLock');
    var fileLock = $.timp.core.server.controllers.fileLock;
    routes.push({ url: '^fileLock/getLock/$', view: fileLock.getLock, ctx: fileLock });
    routes.push({ url: '^fileLock/verify/$', view: fileLock.verify, ctx: fileLock });
    routes.push({ url: '^fileLock/extendTime/$', view: fileLock.extendTime, ctx: fileLock });
    routes.push({ url: '^fileLock/isLocked/$', view: fileLock.isLocked, ctx: fileLock });
    routes.push({ url: '^fileLock/removeLock/$', view: fileLock.removeLock, ctx: fileLock });
    routes.push({ url: '^fileLock/verifyConnection/$', view: fileLock.verifyConnection, ctx: fileLock });
} catch (e) {

}

try {
    $.import('timp.core.server.controllers', 'messageCodes');
    var messageCodes = $.timp.core.server.controllers.messageCodes;
    //messageCodes
    routes.push({ url: '^messageCodes/messageCodes/create/$', view: messageCodes.messageCodes.create, ctx: messageCodes.messageCodes });
    routes.push({ url: '^messageCodes/messageCodes/list/$', view: messageCodes.messageCodes.list, ctx: messageCodes.messageCodes });
    routes.push({ url: '^messageCodes/messageCodes/update/$', view: messageCodes.messageCodes.update, ctx: messageCodes.messageCodes });
    routes.push({ url: '^messageCodes/messageCodes/listByComponent/$', view: messageCodes.messageCodes.listByComponent, ctx: messageCodes.messageCodes });
    routes.push({ url: '^messageCodes/messageCodes/updateAll/$', view: messageCodes.messageCodes.updateAll, ctx: messageCodes.messageCodes });
    //messageCodeTexts
    routes.push({ url: '^messageCodes/messageCodeTexts/create/$', view: messageCodes.messageCodeTexts.create, ctx: messageCodes.messageCodeTexts });
    routes.push({ url: '^messageCodes/messageCodeTexts/list/$', view: messageCodes.messageCodeTexts.list, ctx: messageCodes.messageCodeTexts });
    routes.push({ url: '^messageCodes/messageCodeTexts/listTags/$', view: messageCodes.messageCodeTexts.listTags, ctx: messageCodes.messageCodeTexts });
    routes.push({ url: '^messageCodes/messageCodeTexts/update/$', view: messageCodes.messageCodeTexts.update, ctx: messageCodes.messageCodeTexts });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000004', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers', 'output');
    var output = $.timp.core.server.controllers.output;
    routes.push({ url: '^output/list/$', view: output.list, ctx: output });
    routes.push({ url: '^output/create/$', view: output.create, ctx: output });
    routes.push({ url: '^output/update/$', view: output.update, ctx: output });
    routes.push({ url: '^output/createValue/$', view: output.createValue, ctx: output });
    routes.push({ url: '^output/updateValue/$', view: output.updateValue, ctx: output });
    routes.push({ url: '^output/listOutputType/$', view: output.listOutputType, ctx: output });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers', 'formula');
    var formulaController = $.timp.core.server.controllers.formula;
    routes.push({ url: '^formula/getRequiredInfo/$', view: formulaController.getRequiredInfo, ctx: formulaController });
} catch (e) { 
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

try {
    $.import('timp.core.server.controllers.refactor', 'main');
    const mainCtrl = $.timp.core.server.controllers.refactor.main;
    routes.push({ url: '^main/getCitiesByStates/$', view: mainCtrl.getCitiesByStates, ctx: mainCtrl });
} catch (e) {
    // $.messageCodes.push({code: 'CORE000001', type: 'W', catchInfo: e});
}

var router = new util.URLRouter({
    routes: routes,
    default: function () {
        $.response.setBody('');
        $.response.contentType = 'text/html';
    }
});

try {
    router.parseURL(null, 'core');
} catch (e) {
    $.response.setBody('Unknown failure: ' + util.parseError(e));
}