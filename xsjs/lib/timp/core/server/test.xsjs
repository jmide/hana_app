const p = {};
let actions = {
    setP: function(name){
        let total = ( Date.now() - p[name].start ) / 1000;
        p[name] = Number(total.toFixed(2));
    },    
    initP: function(name){
        if(p[name] == undefined){
            p[name] = {};
        }
        p[name].start = Date.now();
    },
    getApi: function(name){
        $.import(`timp.${name}.server.api`, 'api');
        return $.timp[name].server.api.api;
    },
    importApi: function(name){
        this.initP(name);
        this.getApi(name);
        this.setP(name);
    }
}


let apis = JSON.parse($.request.parameters.get('object'));

apis.forEach( (name) => {
    actions.importApi(name);
})

let total  = 0;

let response = Object.keys(p).reduce( (acc, key) => {
	acc.push(`${key}: ${p[key]}`);
	total += p[key];
	return acc;
} , [])

response.push(`total: ${total.toFixed(1)}`);



$.response.contentType = "application/json";
$.response.setBody(JSON.stringify(response.join('  ->  ')));