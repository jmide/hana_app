$.import('timp.core.server.api', 'api');
var metaObjects = $.timp.core.server.api.api.meta_object;
var baseObject = $.timp.core.server.api.api.base_object;
$.import('timp.bre.server.models', 'tables');
var tableModel = $.timp.bre.server.models.tables;

this.BaseObject = baseObject.BaseObject;

this.klass = baseObject.BaseObject({
    component: 'BRE',
    name: 'Table',
    mainModel: 'table',
    models: {
        table: {
            // schema (optional): 'TIMP', //default
            name: 'Tables',
//          versioning: 'simple',
            // component (optional): 'BRE' //Takes the root component definition by default
            fields: {
                id: {
                    name: 'ID',
                    sequence: 'ID', //defaults to Table::ID,
                    type: $.db.types.INTEGER,
                    pk: true
                },
                name: {
                    name: 'NAME',
                    type: $.db.types.NVARCHAR,
                    size: 256
                },
                description: {
                    name: 'DESCRIPTION',
                    type: $.db.types.TEXT
                },
                definition: {
                    name: 'DEFINITION',
                    type: $.db.types.CLOB,
                    json: {
                        columns: {
                            '[Number]': {
                                name: String,
                                type: String,
                                '(isKey)': Boolean
                            }
                        },
                        column_order: [Number]
                    }
                },
                data: {
                    name: 'DATA',
                    type: $.db.types.CLOB,
                    json: {}
                },
                deleted: {
                    name: 'IS_DELETED',
                    type: $.db.types.INTEGER,
                    translate: {
                        0: false,
                        1: true
                    }
                }
            }
        }
    },
    relationships: {
        rule: {
            type: 'dClient', // dClient, dProvider
            klass: 'BRE.Rule',
            cardinal: 'collection', // collection, single
            on: [{
                    l: 'id',
                    r: 'rule.table_links{id}'
                }] //
        }
    },
    dialogFields: {
            'id': {
                'i18n': 'ID'
            },
           // 'creationDate': {
           //     'i18n': 'CREATIONDATE'
           // },
           // 'creationUser': {
           //     'i18n': 'CREATIONUSER'
           // },
           // 'modificationDate': {
           //     'i18n': 'MODIFICATIONDATE'
           // },
           // 'modificationUser': {
           //     'i18n': 'MODIFICATIONUSER'
           // },
            'name': {
                'i18n': 'NAME'
            },
            'description': {
                'i18n': 'DESCRIPTION'
            },
            'definition': {
                'i18n': 'DEFINITION'
            },
            'deleted': {
                'i18n': 'DELETED'
            }
        }
});

this.klass.prototype.newFunctionality = function(){
    return 'I\'m a new functionality';
};

this.Table = metaObjects.BaseObject({
    name: 'Table',
    table: tableModel.table,
    parseFields: [],
    dialogPick: {}
});