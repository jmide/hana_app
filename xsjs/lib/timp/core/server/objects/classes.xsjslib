try {
	$.import('timp.core.server.objects','request');
	this.request = $.timp.core.server.objects.request;
	$.import('timp.core.server.objects','user');
	this.user = $.timp.core.server.objects.user;
	$.import('timp.core.server.objects','action');
	this.action = $.timp.core.server.objects.action;
} catch (e) {
	this.error = e;
}