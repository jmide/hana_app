$.import('timp.core.server.api', 'api');
var baseObject = $.timp.core.server.api.api.base_object;

this.BaseObject = baseObject.BaseObject;

this.klass = baseObject.BaseObject({
	component: 'Core',
	name: 'TestObject',
	mainModel: 'table',
	models: {
		table: {
			name: 'TestObject',
			versioning: 'simple',
            fields: {
                id: {
                    name: 'ID',
                    sequence: "ID",
                    type: $.db.types.INTEGER,
                    pk: true
                },
                string_val: {
                    name: 'STRING_VALUE',
                    type: $.db.types.NVARCHAR,
                    dimension: 255
                },
                int_val: {
                    name: 'INTEGER_VALUE',
                    type: $.db.types.INTEGER
                },
                tran_val: {
                    name: 'TRANSLATE_VALUE',
                    type: $.db.types.TINYINT,
                    translate: {
                        1: 'Did not show',
                        2: 'Did Show',
                        3: 'Did Read'
                    },
                    'default': 1
                },
                json_val: {
                    name: 'JSON_VALUE',
                    type: $.db.types.NCLOB,
                    json: {}
                }
            }
		}
	},
	relationships: {
		rule: {
			type: 'dClient', // dClient, dProvider
			klass: 'BRE.Rule',
			cardinal: 'collection', // collection, single
			on: [{
					l: 'id',
					r: 'rule.table_links{id}'
				}] //
		}
	},
	dialogFields: {
    	    "id": {
    	        "i18n": "ID"
    	    },
    	   // "creationDate": {
    	   //     "i18n": "CREATIONDATE"
    	   // },
    	   // "creationUser": {
    	   //     "i18n": "CREATIONUSER"
    	   // },
    	   // "modificationDate": {
    	   //     "i18n": "MODIFICATIONDATE"
    	   // },
    	   // "modificationUser": {
    	   //     "i18n": "MODIFICATIONUSER"
    	   // },
    	    "string_val": {
    	        "i18n": "STRING VALUE"
    	    },
    	    "int_val": {
    	        "i18n": "INTEGER VALUE"
    	    },
    	    "tran_val": {
    	        "i18n": "TRANSLATE VALUE"
    	    },
    	    "json_val": {
    	        "i18n": "JSON VALUE"
    	    }
    	}
});

this.klass.prototype.newFunctionality = function(){
    return "I'm a new functionality";
};