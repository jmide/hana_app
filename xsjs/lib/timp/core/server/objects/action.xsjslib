$.import('timp.core.server.logic', 'meta_action');
var metaAction = $.timp.core.server.logic.meta_action;
$.import('timp.core.server.objects', 'meta_object');
var metaObject = $.timp.core.server.objects.meta_object;
$.import('timp.core.server.models', 'actions');
var actionsModel = $.timp.core.server.models.actions;
$.import('timp.core.server.models', 'objects');
var objects = $.timp.core.server.models.objects;
$.import('timp.core.server.models', 'actionsParameters');
var actionsParameters = $.timp.core.server.models.actionsParameters;
$.import('timp.core.server.models', 'components');
var componentsModel = $.timp.core.server.models.components.components;

this.Action = metaObject.BaseObject({
    name: 'Action',
    table: actionsModel.table,
    parseFields: [],
    dialogPick: {
        'id': {
            'i18n': 'ID'
        },
        'name': {
            'i18n': 'NAME'
        }
    }
});

this.Action.prototype.READ = function (options) {
    options.join = [{
      // fields: ['type', 'id_objectType'],
        alias: 'parameters',
        table: actionsParameters.table,
        on: [{left: 'id', right: 'id_action'}]
    },{
        fields: ['id','name'],
        table: objects.objectTypes,
        alias: 'objectType',
        on: [{ leftTable: actionsParameters.table, left: 'id_objectType', right: 'id' }]
    },{
        fields: ['id','name'],
        alias: 'component',
        table: componentsModel.table,
        on: [{left: 'id_component', right: 'id'}]
    }];
    var data = this.table.READ(options),
        actions = [];
    for (var z = 0; z < data.length; z++) {
        var action = data[z];
        
        if (!action.hasOwnProperty('component')) {
            // data[z] = action;
            continue;
        }
        
        var nA = {
                id: action.id,
                actionName: action.component[0].name + '.' + action.name,
                type: action.type,
                inputs: [],
                outputs: []
            },
            oTs = {};
            
        // for (var prop in action) {
        //     if (typeof action[prop] !== 'object') {
        //         nA[prop] = action[prop];
        //     }
        // }
        
        for (var y = 0; y < action.objectType.length; y++) {
            var oT = action.objectType[y];
            oTs[oT.id] = oT;
        }
        
        for (y = 0; y < action.parameters.length; y++) {
            var p = action.parameters[y];
            var param = {
                type: oTs[p.id_objectType].name
            };
            if (p.type == 0) { //Input
                nA.inputs.push(param);
            } else { //Output
                nA.outputs.push(param);
            }
        }
        
        try {
            nA.fn = metaAction.BaseAction.getAction({action: nA.actionName});
        } catch (e) {
            nA.fn = e;
        }
        
        actions.push(nA);
    }
    return actions;
};

