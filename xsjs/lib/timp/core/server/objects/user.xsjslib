$.import('timp.core.server.objects', 'meta_object');
var metaObjects = $.timp.core.server.objects.meta_object;
$.import('timp.core.server.models', 'users');
var userModel = $.timp.core.server.models.users;

this.User = metaObjects.BaseObject({
	name: "User",
	table: userModel.table,
	parseFields: [],
	dialogPick: {
	    "id": {
	        "i18n": "ID"
	    },
	    "creationDate": {
	        "i18n": "CREATIONDATE"
	    },
	    "creationUser": {
	        "i18n": "CREATIONUSER"
	    },
	    "modificationDate": {
	        "i18n": "MODIFICATIONDATE"
	    },
	    "modificationUser": {
	        "i18n": "MODIFICATIONUSER"
	    },
	    "name": {
	        "i18n": "NAME"
	    },
	    "email": {
	        "i18n": "EMAIL"
	    },
	    "hana_user": {
	        "i18n": "HANA_USER"
	    },
	    "id_manager": {
	        "i18n": "ID_MANAGER"
	    },
	    "photo": {
	        "i18n": "PHOTO"
	    },
	    "middle_name": {
	        "i18n": "MIDDLE_NAME"
	    },
	    "last_name": {
	        "i18n": "LAST_NAME"
	    },
	    "id_estado": {
	        "i18n": "ID_ESTADO"
	    },
	    "id_empresa": {
	        "i18n": "ID_EMPRESA"
	    },
	    "id_filial": {
	        "i18n": "ID_FILIAL"
	    },
	    "cargo": {
	        "i18n": "CARGO"
	    },
	    "id_departamento": {
	        "i18n": "ID_DEPARTAMENTO"
	    },
	    "id_substituto": {
	        "i18n": "ID_SUBSTITUTO"
	    },
	    "validity": {
	        "i18n": "VALIDITY"
	    },
	    "status": {
	        "i18n": "STATUS"
	    },
	    "admin": {
	        "i18n": "ADMIN"
	    },
	    "id_delegate": {
	        "i18n": "ID_DELEGATE"
	    }
	}
});