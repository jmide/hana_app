$.import('timp.core.server.objects', 'meta_object');
var metaObjects = $.timp.core.server.objects.meta_object;
$.import('timp.core.server.models', 'requests');
var requestModel = $.timp.core.server.models.requests;

this.Request = metaObjects.BaseObject({
	name: "Request",
	table: requestModel.table,
	parseFields: ["json"],
	dialogPick: {
	    "id": {
	        "i18n": "ID"
	    },
	    "instancesID": {
	        "i18n": "INSTANCESID"
	    },
	    "actionID": {
	        "i18n": "ACTIONID"
	    },
	    "name": {
	        "i18n": "NAME"
	    },
	    "startDate": {
	        "i18n": "STARTDATE"
	    },
	    "endDate": {
	        "i18n": "ENDDATE"
	    },
	    "assignatorID": {
	        "i18n": "ASSIGNATORID"
	    },
	    "group": {
	        "i18n": "GROUP"
	    },
	    "anticipable": {
	        "i18n": "ANTICIPABLE"
	    },
	    "postponable": {
	        "i18n": "POSTPONABLE"
	    },
	    "status": {
	        "i18n": "STATUS"
	    }
	}
});