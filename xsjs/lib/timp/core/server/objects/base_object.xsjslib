$.import('timp.core.server.orm','table');
var tableLib = $.timp.core.server.orm.table;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

$.import('timp.core.server.models', 'schema');
var schema = $.timp.core.server.models.schema;

// var tableLib = $.import('../orm/table.xsjslib');
// var schema = $.import('../models/schema.xsjslib');

function __getFile__(options){
    var objComp, objName, objParts, importPath, objFile, klass;
    objParts = options._.split('.');
    objComp = objParts[0].toLowerCase();
    objName = objParts[1].toLowerCase();
    importPath = '../../../' + objComp + '/server/objects/' + objName + '.xsjslib';
    try {
        objFile = $.import(importPath);    
    } catch(e) {
        importPath = '../../' + objComp + '/server/objects/' + objName + '.xsjslib';
        objFile = $.import(importPath);
        // throw 'Couldn't find the file ' + importPath;
    }
    
    try {
        klass = objFile.klass;    
    } catch(e) {
        throw 'The file ' + importPath + 'doesn\'t contain any .klass object';
    }
    return klass;
}

function __prepareModelData__(options, master){
    var modelData = options;
    for (var fieldKey in modelData.fields) {
        var fieldData = modelData.fields[fieldKey];
        if(fieldData.hasOwnProperty('sequence')){
            fieldData.auto = schema.default + '."' +  master.component + '::' + modelData.name + '::' + fieldData.sequence + '".nextval';
        }
    }
    modelData.name = schema.default + '."' +  master.component + '::' + modelData.name + '"';
    return options;
}

function __getModelField__(options) {
    if (options.hasOwnProperty('auto')) {
        return new tableLib.AutoField(options);
    } else {
        return new tableLib.Field(options);
    }
}

function __getModel__(options, master) {
    options = __prepareModelData__(options, master);
    for (var fieldName in options.fields) {
        var fieldOptions = options.fields[fieldName];
        options.fields[fieldName] = __getModelField__(fieldOptions);
    }
    return new tableLib.Table(options);
}

var proto = {
    __init__: function (options) {
        this.data = {};
        for (var fieldName in this.table.fields) {
            // var field = this.table.fields[fieldName];
            if (options.hasOwnProperty(fieldName)) {
                this.data[fieldName] = options[fieldName];
            }
        }
    },
    __load__: function (options) {
        for (var fieldName in options) {
            if (this.data.hasOwnProperty(fieldName)) {
                if (
                    (typeof this.data[fieldName] == 'function') ||
                    (fieldName == 'prototype')
                ){ continue; }
            }
            if(this.table.fields.hasOwnProperty(fieldName) &&
                this.table.fields[fieldName].json && typeof options[fieldName] === 'string'){
                this.data[fieldName]  = JSON.parse(options[fieldName]);
            }else{
                this.data[fieldName]  = options[fieldName];
            }
        }
    },
    serialize: function () {
        var json = {};
        for (var fieldName in this.table.fields) {
            var fieldObject = this.table.fields[fieldName];
            if(fieldObject.hasOwnProperty('json') && fieldObject.json){
                json[fieldName] = JSON.stringify(this.data[fieldName]);
            }else{
                json[fieldName] = this.data[fieldName];
            }
        }
        return json;
    },
    save: function () {
        var json = this.serialize();
        var data;
        if (json.hasOwnProperty('id') && json.id) {
            data = this.table.UPDATE(json);
            this.__init__(json);
        } else {
            data = this.table.CREATE(json);
            data.id = Number(data.id);
            this.__load__(data);    
        }
        return data;
    },
    delete: function () {
        var result = this.table.DELETE(this.data.id);
        if(result){
            delete this.data.id;
        }
        return result;
    },
    getTag: function () {
        return { type: this.constructor.prototype.objectType, id: Number(this.data.id)} ;
    },
    getVersions: function () {
        // throw this.id;
        var response = this.table.READ({
            versions: true,
            where: [{
                field: 'id',
                oper: '=',
                value: this.data.id
            }]
        });
        return response;
    },
    toJSON: function(){
        var json = {};
        if ( typeof this.table === 'undefined' ) {
            return json;
        }
        for (var fieldName in this.table.fields) {
            // var fieldObject = this.table.fields[fieldName];
            json[fieldName] = this.data[fieldName];
        }
        return json;
    } 
};

function __dialogPick__(options){
    var defFields = {},
        fields = [],
        table = this.table;
    
    options = options || {};
    options.fields = options.fields || this.dialogFields;
    
    for (var field in this.dialogFields) {
        if (table.fields.hasOwnProperty(field)) {
            defFields[field] = this.dialogFields[field];
            defFields[field].type = table.fields[field].type;
            fields.push(field);
        }
    }
    
    var dialogPick = {
        definition: defFields
    };
    
    options = options || {};
    
    options.fields = fields;
    
    dialogPick.data = this.table.READ(options);
    
    if(dialogPick.data.hasOwnProperty('pageCount')) {
        dialogPick.pageCount = dialogPick.data.pageCount;
    }
    return dialogPick;
}

function __READ__(options){
    var readOptions = {};
    options = options || {};
    var klass = options.klass || __getFile__(options.input);
    if (options.input.hasOwnProperty('table') && options.input.table) {
        return klass.dialogPick();
    } else {
        if (options.input.hasOwnProperty('id')) {
            readOptions.where = [{ 'field': 'id', 'oper': '=', 'value': options.input.id }];
        } else if (options.input.hasOwnProperty('query') && typeof options.input.query === 'object') {
            // If the dev sends a fields attribute we remove it 
            // in order to avoid any problems in the object creation
            if (options.input.query.hasOwnProperty('fields') && options.input.query.fields &&  !options.data) {
                delete options.input.query.fields;
            }
            readOptions = options.input.query;
        }
        var response = klass.READ(readOptions);
        if (!options.data) {
            response = response.map(function(datum){
                return new klass(datum);
            });
        }
        
        if (options.input.hasOwnProperty('id')) {
            if(response.length === 0){
                throw 'The instance of the object type ' + options._ + ' with the ID ' + options.id + 'does not exist';
            }else{
                return response[0];
            }
        }
        return response;    
    }
}

function BaseObject2(options){
    var definition = new Function('json', 
        'return function ' + options.name + '(options){' + 
            'if (this instanceof ' + options.name + ') {' +
            'this.__init__(options);' +
            '} else {' +
            'return new ' + options.name + '(options);' +
            '}' + 
        '}'
    )();
    
    for (var prop in proto) {
        definition.prototype[prop] = proto[prop];
    }
    definition.prototype.models = {};
    for(var modelName in options.models){
        definition.prototype.models[modelName] = __getModel__(options.models[modelName], options);
    }
    definition.table = definition.prototype.table = definition.prototype.models[options.mainModel];
    definition.READ = function(ops) {
        return this.table.READ(ops);
    };
    definition.GET = function(ops) {
        return __READ__({
            klass: this,
            input: ops || {}
        });
    };
    definition.DATA = function(ops) {
        return __READ__({
            klass: this,
            data: true,
            input: ops || {}
        });
    };
    definition.dialogPick = __dialogPick__;
    definition.dialogFields = options.dialogFields || {};
    // throw options;
    return definition;
}
BaseObject2.CLASS = __getFile__;
BaseObject2.GET = function(options){
    return __READ__({
        input: options || {}
    });
};
BaseObject2.DATA = function(options){
    return __READ__({
        data: true,
        input: options || {}
    });
};


this.BaseObject = BaseObject2;

// /*@doc
// MetaObject({
//     name: "Request",
//     table: <table object>,
//     parseFields: ['json'],
//     displayPick: ['id', 'reportName']
// })

// Extend the method displayPick to return:
//     {
//         definition: {
//             id: $.db.types.INTEGER,
//             name: $.db.types.NVARCHAR
//         },
//         data: [ ... ] 
//     }
// That should affect all object selectors for that object on the system.
// */
// function BaseObject (options) {
//     if (!options.table) {
//         // throw 'Undefined table: ' + util.parseError(options)
//         var e = '';
//     }
    

//     definition.prototype.table = options.table;
//     definition.prototype.parseFields = options.parseFields;
//     definition.prototype.dialogFields = options.dialogPick;
    
//     definition.dialogPick = (function (options) {
//         var defFields = {},
//             fields = [],
//             table = this.prototype.table;
        
//         options = options || {};
//         options.fields = options.fields || this.prototype.dialogFields;
        
//         for (var field in this.prototype.dialogFields) {
//             if (table.fields.hasOwnProperty(field)) {
//                 defFields[field] = this.prototype.dialogFields[field];
//                 defFields[field].type = table.fields[field].type;
//                 fields.push(field);
//             }
//         }
        
//         var dialogPick = {
//             definition: defFields
//         }
        
//         options = options || {};
        
//         options.fields = fields;
        
//         dialogPick['data'] = this.prototype.table.READ(options);
        
//         if(dialogPick['data'].hasOwnProperty('pageCount')) {
//             dialogPick['pageCount'] = dialogPick['data']['pageCount']
//         }
        
//         return dialogPick; 
//     }).bind(definition);
    
//     definition.READ = function (options) {
//         return this.prototype.table.READ(options);
//     }
    
//     definition.find = function (options) {
//         var items = this.prototype.table.READ(options);
//         for (var x = 0; x < items.length; x++) {
//             items[x] = this(items[x]);
//         }
//         return items;
//     }

//     return definition;
// }
// this.BaseObject = BaseObject;

// BaseObject.getObject = function (obj_tag) {
//     var klass = this.getClass(obj_tag);
//     return klass(obj_tag);
// }

// BaseObject.getClass = function (obj_tag) {
//     if (obj_tag.type.match(/Native/)) {
//         var type = obj_tag.type.split(':')[1]
//         return eval(type);
//     }
//     var sliced = obj_tag.type.split('::'),
//         component = 'timp.' + sliced[0].toLowerCase() + '.server.api',
//         component_name = sliced[1],
//         object = sliced[1][0].toLowerCase() + sliced[1].slice(1);
    
//     sliced = component.split('.');
//     sliced.push('api')
//     sliced.push('classes')
//     try {
//         $.import(component, 'api');
//         var reference = $;
//         for (var x = 0; x < sliced.length; x++) {
//             if (!reference.hasOwnProperty(sliced[x])) {
//                 throw 'Undefined reference';
//             }
//             reference = reference[sliced[x]];
//         }
        
//         try {
//             reference[object][component_name].prototype.objectType = obj_tag.type;
//             return reference[object][component_name];
//         } catch (e1) {
//             if (reference.hasOwnProperty('error')) {
//                 reference.error
//             } else {
//                 throw e1;
//             }
//         }
        
//         return reference[object][component_name];
//     } catch (e) {
//         throw 'Failed to load $.import("'+ component+ '", "api");' + JSON.stringify(sliced) + '.' + object + '["' + component_name + '"]\n' + util.parseError(e)
//     }
// }
