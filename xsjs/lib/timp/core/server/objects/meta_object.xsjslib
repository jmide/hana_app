$.import('timp.core.server','util');
var util = $.timp.core.server.util;

var proto = {
    __init__: function (options) {
        for (var fieldName in this.table.fields) {
            var field = this.table.fields[fieldName];
            if (options.hasOwnProperty(fieldName) && field.instanceOf == 'Field') {
                this[fieldName] = options[fieldName];
            }
        }
    },
    __load__: function (options) {
        for (var fieldName in options) {
            if (this.hasOwnProperty(fieldName)) {
                if (
                    (typeof this[fieldName] == 'function') ||
                    (fieldName == 'prototype')
                ){ continue; }
            }
            this[fieldName]  = options[fieldName];
        }
        // for (var fieldName in this.table.fields) {
        //     if (options.hasOwnProperty(fieldName)) {
        //         this[fieldName]  = options[fieldName];
        //     }
        // }
    },
    serialize: function () {
        var json = {};
        for (var fieldName in this.table.fields) {
            // this.table.fields[fieldName]
            json[fieldName] = this[fieldName];
        }
        return json;
    },
    save: function () {
        var json = this.serialize();
        // for (var p in this.parseFields) {
        //     json[this.parseFields[p]] = JSON.stringify(json[this.parseFields[p]]);
        // }
        var data = this.table.CREATE(json);
        this.__load__(data);
    },
    update: function (options) {
        options = options || this;
        options.id = this.id;
        this.table.UPDATE(options);
        this.__init__(options);
    },
    delete: function () {
        this.table.DELETE(this.id);
    },
    getTag: function () {
        return {type: this.constructor.prototype.objectType, id: Number(this.id)};
    },
    READ: function (options) {
        return this.table.READ(options);
    }
};

/*@doc
MetaObject({
    name: "Request",
    table: <table object>,
    parseFields: ['json'],
    displayPick: ['id', 'reportName']
})

Extend the method displayPick to return:
    {
        definition: {
            id: $.db.types.INTEGER,
            name: $.db.types.NVARCHAR
        },
        data: [ ... ] 
    }
That should affect all object selectors for that object on the system.
*/
function BaseObject (options) {
    var e;
    if (!options.table) {
        // throw 'Undefined table: ' + util.parseError(options)
        e = '';
    }
    
    var definition = function (opts) {
        if (this instanceof definition) {
            if (opts.hasOwnProperty('id') && opts.hasOwnProperty('type')) { //options instanceof ObjectTag && 
                var readOpts = {
                    where: [{field: 'id', oper: '=', value: opts.id}]
                };
                var data = this.READ(readOpts);
                if (!data.length) {
                    readOpts.simulate = true;
                    throw 'It was not possible to find the object ' + util.parseError(opts) + '\n' + this.READ(readOpts);
                }
                for (var i = 0; i < this.parseFields.length; i++) {
                    var json = this.parseFields[i];
                    try {
                        data[0][json] = JSON.parse(data[0][json]);
                    } catch (e) {
                        data[0][json] = null;
                    }
                }
                this.__load__(data[0]);
            } else {
                this.__init__(opts);
            }
        } else {
            return new definition(opts);
        }
        return this;
    };
    
    for (var prop in proto) {
        definition.prototype[prop] = proto[prop];
    }

    definition.prototype.table = options.table;
    definition.prototype.parseFields = options.parseFields;
    definition.prototype.dialogFields = options.dialogPick;
    
    definition.dialogPick = (function (opts) {
        var lang = $.request.cookies.get("Content-Language");
        lang = lang !== "enus" && lang !== "ptrbr" ? "ptrbr" : lang;
        var defFields = {},
            fields = [],
            table = this.prototype.table;
        
        opts = opts || {};
        opts.fields = opts.fields || this.prototype.dialogFields;
        opts.where = [[]];
        
        for (var field in this.prototype.dialogFields) {
            if (table.fields.hasOwnProperty(field)) {
                var dataField = this.prototype.dialogFields[field];
                defFields[field] = {
                    type: table.fields[field].type,
                    name: dataField.i18n    
                };
                
                if (dataField.hasOwnProperty("i18nPtrbr") && lang === "ptrbr") {
                    defFields[field].name = dataField.i18nPtrbr; 
                } else if (dataField.hasOwnProperty("i18nEnus") && lang === "enus") {
                    defFields[field].name = dataField.i18nEnus; 
                }
                
                fields.push(field);
                var type = table.fields[field].type === $.db.types.INTEGER ? 1 : (table.fields[field].type === $.db.types.VARCHAR || table.fields[field].type === $.db.types.NVARCHAR) ? 2 : 0; 
                if (opts.hasOwnProperty("textToSearch") && (type === 1  || type === 2)) {
                    if (type === 1 && !isNaN(opts.textToSearch)) {
                        opts.where[0].push({
                            field: field,
                            oper: "=",
                            value: opts.textToSearch
                        });
                    } else if (type === 2) {
                        opts.where[0].push({
                            field: field,
                            oper: "LIKE",
                            maskFn: 'UPPER',
                            value: "%" + (typeof(opts.textToSearch) === "string" ? opts.textToSearch.toUpperCase() : opts.textToSearch) + "%"
                        });
                    }
                }
            }
        }
        if (opts.where[0].length === 0) {
            delete opts.where;
        }
        if(opts.hasOwnProperty('remove')){
            let where = JSON.parse(opts.remove)
            let varil = where[opts.type]
            for(var x in varil){
                opts.where[0].push({
                            field: x,
                            oper: "=",
                            value: varil[x]
                        });
            }
        }
        var dialogPick = {
            definition: defFields
        };
        	
        if(opts.type === "BRB::Variant"){
            opts.where = opts.where || [];
            opts.where.push({
                field: 'isDeleted',
                oper: "=",
                value: 0
            });
            opts.where.push({
                field: 'isPublic',
                oper: "=",
                value: 1
            });
        }
        if(opts.type === "BRB::Report"){
            opts.where = opts.where || [];
            opts.where.push({
                field: 'isPublic',
                oper: "=",
                value: 1
            });
        }
        if(opts.type === "TFB::Livro"){
            opts.where = opts.where || [];
            opts.where.push({
                field: 'isPublic',
                oper: "=",
                value: 1
            });
        }
        
        
        opts = opts || {};
        
        opts.fields = fields;
        
        dialogPick.data = this.prototype.table.READ(opts);
        
        if(dialogPick.data && dialogPick.data.length && opts.type === "ATR::AjusteAssociacao"){
            const ajusteAssociacaoView = $.createBaseRuntimeModel($.viewSchema,"timp.mdr.modeling/CV_AJUSTE_ASSOCIACAO", true);
            var ids = dialogPick.data.map(function(elem){
                return elem.id;
            });
            
            var dataViewAssociation = ajusteAssociacaoView.find({
                select:[{field:"COD_AJUSTE"},{field:"COD_OFICIAL"},{field:"ID"}],
                where:[{
                    field:"ID",
                    operator:"$in",
                    value: ids
                }]
            }).results;
            
            if(dataViewAssociation && dataViewAssociation.length){
                dialogPick.data.forEach(function(elemData){
                    dataViewAssociation.forEach(function(elemView){
                        if(elemData.id === elemView.ID){
                           elemData.codAjuste = elemView.COD_AJUSTE || "";
                           elemData.codOficial = elemView.COD_OFICIAL || "";
                        }
                    });
                });
            }   
        }
        
        // if(dialogPick.data && dialogPick.data.length > 0 && opts.type === 'BRB::Report'){
        //     dialogPick.data=dialogPick.data.filter(function(elemData){
        //         return elemData.isPublic===true;
        //     });
        // }
        
        if(dialogPick.data.hasOwnProperty('pageCount')) {
            dialogPick.pageCount = dialogPick.data.pageCount;
        }
        
        return dialogPick; 
    }).bind(definition);
    
    definition.READ = function (options2) {
        return this.prototype.table.READ(options2);
    };
    
    definition.find = function (options2) {
        var items = this.prototype.table.READ(options2);
        for (var x = 0; x < items.length; x++) {
            items[x] = this(items[x]);
        }
        return items;
    };
    return definition;
}
this.BaseObject = BaseObject;

BaseObject.getObject = function(objTag) {
    var klass = this.getClass(objTag);
    return klass(objTag);
};

BaseObject.getClass = function (objTag) {
    if (objTag.type.match(/Native/)) {
        var type = objTag.type.split(':')[1];
        return eval(type);
    }
    var sliced = objTag.type.split('::'),
        component = 'timp.' + sliced[0].toLowerCase() + '.server.api',
        componentName = sliced[1],
        object = sliced[1][0].toLowerCase() + sliced[1].slice(1);
    
    sliced = component.split('.');
    sliced.push('api');
    sliced.push('classes');
    
    try {
        $.import(component, 'api');
        var reference = $;
        for (var x = 0; x < sliced.length; x++) {
            if (!reference.hasOwnProperty(sliced[x])) {
                throw 'Undefined reference';
            }
            reference = reference[sliced[x]];
        }
        // throw sliced
        try {
            reference[object][componentName].prototype.objectType = objTag.type;
            return reference[object][componentName];
        } catch (e1) {
            if (reference.hasOwnProperty('error')) {
                reference.error;
            } else {
                throw e1;
            }
        }
        // throw [reference, object, componentName]
        return reference[object][componentName];
    } catch (e) {
        throw 'Failed to load $.import("'+ component+ '", "api");' + JSON.stringify(sliced) + '.' + object + '["' + componentName + '"]\n' + util.parseError(e);
    }
};
