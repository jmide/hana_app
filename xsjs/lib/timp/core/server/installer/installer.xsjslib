let performance = {
	libraries: null,
	init: null
};

performance.libraries = new Date().getTime();

$.import('timp.core.server', 'config');
const currentVersion = $.timp.core.server.config.application.version;
const maxModelsPerInstallation = $.timp.core.server.config.application.maxModelsPerInstallation;

$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

$.import('timp.core.server.orm', 'tableServices');
const services = $.timp.core.server.orm.tableServices;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server.orm', 'table');
const Table = $.timp.core.server.orm.table.Table;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;
const i18nModel = $.timp.core.server.models.tables.component.internationalizationModel;
const privilegeModel = $.timp.core.server.models.tables.component.privilegeModel;
$.import('timp.core.server.models.tables', 'label');
const labelModel = $.timp.core.server.models.tables.label.labelModel;

$.import('timp.core.server.models.tables', 'object');
const objectTypeCoreModel = $.timp.core.server.models.tables.object.objectType;

$.import('timp.core.server.controllers.refactor', 'log');
const logInstaller = $.timp.core.server.controllers.refactor.log.logInstaller;

performance.libraries = (new Date().getTime() - performance.libraries) / 1000;

/**
 * Installs the specified models and instances.
 * @returns {Object} status - the status of the component after the installation.
 * @example
 * // {
 * // 	"errors": [],
 * // 	"results": {
 * // 		"installed": true,
 * // 		"beforeInstall": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true
 * // 			}
 * // 		},
 * // 		"installer": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true,
 * // 				"TST::A": {
 * // 					"errors": [],
 * // 					"results": {
 * // 						"altered": true,
 * // 						"status": {
 * // 							"primaryKeys": ["id"],
 * // 							"sequences": [],
 * // 							"constraints": [],
 * // 							"columns": []
 * // 						},
 * // 						"@diagnosis": {
 * // 							"primaryKeys": ["id"],
 * // 							"alterPrimaryKeyConstraint": false,
 * // 							"add": {
 * // 								"columns": [],
 * // 								"sequences": [],
 * // 								"constraints": [],
 * // 								"indexes": []
 * // 							},
 * // 							"remove": {
 * // 								"columns": [],
 * // 								"sequences": [],
 * // 								"constraints": [],
 * // 								"indexes": []
 * // 							},
 * // 							"alterColumns": []
 * // 						}
 * // 					}
 * // 				},
 * // 				"TST::B": {
 * // 					"errors": [],
 * // 					"results": {
 * // 						"altered": true,
 * // 						"status": {
 * // 							"primaryKeys": ["id2"],
 * // 							"sequences": [],
 * // 							"constraints": [],
 * // 							"columns": []
 * // 						},
 * // 						"@diagnosis": {
 * // 							"primaryKeys": ["id2"],
 * // 							"alterPrimaryKeyConstraint": false,
 * // 							"add": {
 * // 								"columns": [],
 * // 								"sequences": [],
 * // 								"constraints": [],
 * // 								"indexes": []
 * // 							},
 * // 							"remove": {
 * // 								"columns": [],
 * // 								"sequences": [],
 * // 								"constraints": [],
 * // 								"indexes": []
 * // 							},
 * // 							"alterColumns": []
 * // 						}
 * // 					}
 * // 				}
 * // 			}
 * // 		},
 * // 		"component": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true,
 * // 				"instance": {
 * // 				}
 * // 			}
 * // 		},
 * // 		"privileges": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true,
 * // 				"instances": {
 * // 					"created": [],
 * // 					"updated": []
 * // 				}
 * // 			}
 * // 		},
 * // 		"i18n": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true,
 * // 				"instances": {
 * // 					"created": [],
 * // 					"updated": []
 * // 				}
 * // 			}
 * // 		},
 * // 		"labels": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true,
 * // 				"instances": {
 * // 					"created": [],
 * // 					"updated": []
 * // 				}
 * // 			}
 * // 		},
 * // 		"seeder": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true
 * // 			}
 * // 		},
 * // 		"afterInstall": {
 * // 			"errors": [],
 * // 			"results": {
 * // 				"executed": true
 * // 			}
 * // 		}
 * // 	}
 * // }
 * @param {Object} options - Options for the installer
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       beforeInstall(function): function() {},
 * //       afterInstall(function): function() {},
 * //       seeder: [{
 * //           model(BaseModel): a,
 * //           values: {
 * //               base: [{}, {}].
 * //               new: [{}, {}],
 * //               update: [{ instance: {}, where: [] }]
 * //           }
 * //       }],
 * //       privileges: {
 * //           base: [{}, {}].
 * //           new: [{}, {}],
 * //           update: [{ instance: {}, where: [] }]
 * //       },
 * //       i18n: {
 * //           base: [{}, {}].
 * //           new: [{}, {}],
 * //           update: [{ instance: {}, where: [] }]
 * //       },
 * //       labels: {
 * //           base: [{}, {}].
 * //           new: [{}, {}],
 * //           update: [{ instance: {}, where: [] }]
 * //       },
 * //       component: {
 * //           name: 'CORE',
 * //           labelEnus: 'CORE',
 * //           labelEnus: 'CORE'
 * //       },
 * //       models: [a, b, c]
 * //   }
 * // }
 * @param {Object} config - installer config.
 * @example
 * // {
 * //   returnSeederInstances: true
 * // }
 */
function Installer(options, config) {
	performance.init = new Date().getTime();
	options = lodash.isNil(options) ? {} : options;
	config = !(!lodash.isNil(config) && lodash.isPlainObject(config) && !lodash.isEmpty(config)) ? {} : config;
	this.init(options, config);
}
Installer.prototype = {
	init: function(options, config) {
		this.totalPages = -1;
		this.sanitizeModels(options.models);
		this.sanitizeComponent(options.component);
		this.sanitizePrivileges(options.privileges);
		this.sanitizeI18n(options.i18n);
		this.sanitizeLabels(options.labels);
		this.sanitizeBeforeInstall(options.beforeInstall);
		this.sanitizeAfterInstall(options.afterInstall);
		this.sanitizeSeeder(options.seeder);
		this.sanitizeObjectActions(options.objectActions);
		this.sanitizeConfig(config);
		this.sanitizePage(options.page);
		performance.init = (new Date().getTime() - performance.init) / 1000;
	},
	sanitizePage: function(page) {
		this.page = 1;
		if (lodash.isInteger(page)) {
			this.page = page;
		}
	},
	sanitizeObjectActions: function(objectActions) {
		this.objectActions = {};
		if (lodash.isPlainObject(objectActions) && !lodash.isNil(objectActions)) {
			this.objectActions = objectActions;
		}
	},
	sanitizeComponent: function(component) {
		this.component = {};
		if (lodash.isPlainObject(component) && !lodash.isEmpty(component) && lodash.isString(component.name)) {
			this.component = component;
			this.component.version = currentVersion;
		}
	},
	sanitizePrivileges: function(privileges) {
		this.privileges = {};
		if (lodash.isPlainObject(privileges) && !lodash.isEmpty(privileges)) {
			this.privileges = privileges;
		}
	},
	sanitizeI18n: function(i18n) {
		this.i18n = {};
		if (lodash.isPlainObject(i18n) && !lodash.isEmpty(i18n)) {
			this.i18n = i18n;
		}
	},
	sanitizeLabels: function(labels) {
		this.labels = {};
		if (lodash.isPlainObject(labels) && !lodash.isEmpty(labels)) {
			this.labels = labels;
		}
	},
	sanitizeConfig: function(config) {
		if (!lodash.isBoolean(config.returnSeederInstances)) {
			this.returnSeederInstances = false;
		} else {
			this.returnSeederInstances = config.returnSeederInstances;
		}
	},
	sanitizeModels: function(models) {
		const _this = this;
		_this.models = [];
		if (lodash.isArray(models) && !lodash.isEmpty(models)) {
			_this.models = models;
		}
	},
	sanitizeBeforeInstall: function(beforeInstall) {
		this.beforeInstall = null;
		if (lodash.isFunction(beforeInstall)) {
			this.beforeInstall = beforeInstall;
		}
	},
	sanitizeAfterInstall: function(afterInstall) {
		this.afterInstall = null;
		if (lodash.isFunction(afterInstall)) {
			this.afterInstall = afterInstall;
		}
	},
	sanitizeSeeder: function(seeder) {
		this.seeder = [];
		if (lodash.isArray(seeder) && !lodash.isEmpty(seeder)) {
			this.seeder = seeder;
		}
	},
	executeBeforeInstall: function() {
		const _this = this;
		let retVal = {
			errors: [],
			results: {
				executed: true
			}
		};
		try {
			if (!lodash.isNil(_this.beforeInstall)) {
				_this.beforeInstall();
			}
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				if (lodash.isArray(e) && !lodash.isEmpty(e)) {
					retVal.errors = lodash.concat(retVal.errors, e);
				} else {
					retVal.errors.push(e);
				}
			}
			return retVal;
		}
	},
	alterInstance: function(model, instance) {
		let where = [];
		let retVal = {
			errors: [],
			results: {
				instance: null
			}
		};
		const ignoreTheseTypes = ['text', 'shorttext', 'json', 'file'];
		let fieldMap = $.lodash.isNil(BaseModel.models[model.getIdentity()]) ? model.getDefinition().fields : BaseModel.models[model.getIdentity()]
			.fields;
		lodash.forEach(instance, function(value, key) {
			if (!lodash.isNil(fieldMap[key]) && lodash.indexOf(ignoreTheseTypes, fieldMap[key].type) === -1) {
				if (lodash.isPlainObject(fieldMap[key]['$translate']) && !lodash.isNil(fieldMap[key]['$translate']) && !lodash.isEmpty(fieldMap[key][
					'$translate'])) {
					value = fieldMap[key]['$translate'][value];
					if (lodash.indexOf(['integer', 'tinyint', 'bigint', 'decimal'], fieldMap[key].type) > -1) {
						value = Number(value);
					}
				}
				where.push({
					field: key,
					operator: '$eq',
					value: value
				});
			}
		});
		let response;
		try {
			response = model.find({
				where: where
			});
		} catch (e) {
			response = e;
		}
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(retVal.errors, response.errors);
			return retVal;
		}
		if (lodash.isArray(response.results) && lodash.isEmpty(response.results)) {
			retVal.results.instance = instance;
		}
		return retVal;
	},
	executeInstallerSeeder: function(model, options) {
		let retVal = {
			errors: [],
			results: {
				executed: true,
				instances: {
					created: [],
					updated: []
				}
			}
		};
		const _this = this;
		options = (!lodash.isNil(options) && lodash.isPlainObject(options)) ? options : {};
		try {
			if (model instanceof BaseModel) {
				let response;
				if (lodash.isArray(options.base) && !lodash.isEmpty(options.base)) {
					let baseInstances = [];
					lodash.forEach(options.base, function(instance) {
						response = _this.alterInstance(model, instance);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
						} else if (!lodash.isNil(response.results.instance)) {
							baseInstances.push(response.results.instance);
						}
					});
					if (!lodash.isEmpty(baseInstances)) {
						try {
							response = model.batchCreate(baseInstances, {
								getInstances: _this.returnSeederInstances
							});
						} catch (e) {
							response = e;
						}
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
						}
						if (!lodash.isEmpty(response.results.instances)) {
							retVal.results.instances.created = lodash.concat(retVal.results.instances.created, response.results.instances);
						}
					}
				}
				if (lodash.isArray(options.new) && !lodash.isEmpty(options.new)) {
					try {
						response = model.batchCreate(options.new, {
							getInstances: _this.returnSeederInstances
						});
					} catch (e) {
						response = e;
					}
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					}
					if (!lodash.isEmpty(response.results.instances)) {
						retVal.results.instances.created = lodash.concat(retVal.results.instances.created, response.results.instances);
					}
				}
				if (lodash.isArray(options.update) && !lodash.isEmpty(options.update)) {
					lodash.forEach(options.update, function(updateOptions) {
						try {
							response = model.update(updateOptions.instance, {
								where: updateOptions.where
							}, _this.returnSeederInstances);
						} catch (e) {
							response = e;
						}
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
						}
						if (!lodash.isEmpty(response.results.instances)) {
							retVal.results.instances.updated = lodash.concat(retVal.results.instances.updated, response.results.instances);
						}
					});
				}
				return retVal;
			} else {
				retVal.errors.push(resources.generateError('Invalid Model', 'The specified model isn\'t a BaseModel instance.'));
				throw retVal;
			}
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	executeAfterInstall: function() {
		const _this = this;
		let retVal = {
			errors: [],
			results: {
				executed: true
			}
		};
		try {
			if (!lodash.isNil(_this.afterInstall)) {
				_this.afterInstall();
			}
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				if (lodash.isArray(e) && !lodash.isEmpty(e)) {
					retVal.errors = lodash.concat(retVal.errors, e);
				} else {
					retVal.errors.push(e);
				}
			}
			return retVal;
		}
	},
	executeSeeder: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true
			}
		};
		const _this = this;
		try {
			let response;
			lodash.forEach(_this.seeder, function(instance) {
				if (lodash.isString(instance.identity) && lodash.isString(instance.schema)) {
					instance.model = $.createBaseRuntimeModel(instance.schema, instance.identity);
				}
				if (instance.model instanceof BaseModel) {
					response = _this.executeInstallerSeeder(instance.model, instance.values);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					}
					retVal.results[instance.model.getName()] = response.results;
				} else {
					retVal.errors.push(resources.generateError('Invalid Model', '"' + instance.model + '" isn\'t a valid BaseModel instance.'));
				}
			});
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	calculateDifferences: function(schema, identity, dbDefinition, currentDefinition) {
		let currentToDBMap = {};
		let retVal = {
			errors: [],
			results: {
				differences: {
					primaryKeys: [],
					alterPrimaryKeyConstraint: false,
					add: {
						columns: [],
						sequences: [],
						constraints: [],
						indexes: []
					},
					remove: {
						columns: [],
						sequences: [],
						constraints: [],
						indexes: []
					},
					alterColumns: [],
					update: {
						sequences: []
					}
				}
			}
		};
		let validConversions = Installer.validConversions;
		// Remember to drop the sequences of the columns to be dropped

		try {
			lodash.forEach(currentDefinition, function(value) {
				currentToDBMap[value.columnName] = value;
			});
			let response, query, sequenceValue, tempColumn, needsAnAlter, changedType;
			lodash.forEach(currentDefinition, function(value, fieldName) {
				sequenceValue = 1;
				if (lodash.has(dbDefinition, value.columnName)) {
					tempColumn = {
						columnName: value.columnName
					};
					needsAnAlter = false;
					changedType = false;
					if (value.primaryKey) {
						value.required = true;
					}
					/* Check the pk constraints */
					if (dbDefinition[value.columnName].primaryKey && !(lodash.isBoolean(value.primaryKey) ? value.primaryKey : false)) {
						retVal.results.differences.alterPrimaryKeyConstraint = true;
					} else if (!dbDefinition[value.columnName].primaryKey && (lodash.isBoolean(value.primaryKey) ? value.primaryKey : false)) {
						retVal.results.differences.primaryKeys.push(fieldName);
						retVal.results.differences.alterPrimaryKeyConstraint = true;
					} else if (dbDefinition[value.columnName].primaryKey && (lodash.isBoolean(value.primaryKey) ? value.primaryKey : false)) {
						retVal.results.differences.primaryKeys.push(fieldName);
					}
					/* Check the unique constraints */
					if (dbDefinition[value.columnName].unique && !(lodash.isBoolean(value.unique) ? value.unique : false) && (!(lodash.isBoolean(value.primaryKey) ?
						value.primaryKey : false) && !(lodash.isBoolean(dbDefinition[value.columnName].primaryKey) ? dbDefinition[value.columnName].primaryKey :
						false))) {
						// remove the unique constraint
						query = 'SELECT "CONSTRAINT_NAME" as "name" FROM "CONSTRAINTS" WHERE "SCHEMA_NAME" = \'' + schema +
							'\' AND "IS_UNIQUE_KEY" = \'TRUE\' AND "TABLE_NAME" = \'' + identity + '\' AND "COLUMN_NAME" = \'' + value.columnName + '\'';
						response = connector.execute(query, null);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(response.errors, retVal.errors);
						}
						retVal.results.differences.remove.constraints.push({
							name: response.results[0].name
						});
					} else if (!dbDefinition[value.columnName].unique && (lodash.isBoolean(value.unique) ? value.unique : false) && (!(lodash.isBoolean(
						value.primaryKey) ? value.primaryKey : false) && !(lodash.isBoolean(dbDefinition[value.columnName].primaryKey) ? dbDefinition[
						value.columnName].primaryKey : false))) {
						// add the unique constraint
						retVal.results.differences.add.constraints.push({
							columnName: value.columnName,
							unique: true
						});
					}
					/* Check the sequences */
					if (dbDefinition[value.columnName].autoIncrement && !(lodash.isBoolean(value.autoIncrement) ? value.autoIncrement : false)) {
						// remove the sequence
						retVal.results.differences.remove.sequences.push({
							name: identity + '::' + value.columnName,
							schema: schema
						});
					} else if (!dbDefinition[value.columnName].autoIncrement && (lodash.isBoolean(value.autoIncrement) ? value.autoIncrement : false)) {
						// add the sequence
						query = 'SELECT (IFNULL(MAX("' + value.columnName + '"),0) + 1) AS "value" FROM "' + schema + '"."' + identity + '"';
						response = connector.execute(query, null);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(response.errors, retVal.errors);
						} else if (!lodash.isEmpty(response.results)) {
							sequenceValue = response.results[0].value;
						}
						retVal.results.differences.add.sequences.push({
							name: identity + '::' + value.columnName,
							schema: schema,
							value: sequenceValue
						});
					} else if (dbDefinition[value.columnName].autoIncrement && (lodash.isBoolean(value.autoIncrement) ? value.autoIncrement : false)) {
						//update the sequence
						query = 'SELECT (IFNULL(MAX("' + value.columnName + '"),0) + 1) AS "value" FROM "' + schema + '"."' + identity + '"';
						response = connector.execute(query, null);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(response.errors, retVal.errors);
						} else if (!lodash.isEmpty(response.results)) {
							sequenceValue = response.results[0].value;
						}
						retVal.results.differences.update.sequences.push({
							name: identity + '::' + value.columnName,
							schema: schema,
							value: sequenceValue
						});
					}
					/* Check the indexes */
					/* Check alters in columns */
					if (dbDefinition[value.columnName].type === value.type) {
						tempColumn.type = value.type;
					} else if (dbDefinition[value.columnName].type !== value.type && lodash.indexOf(validConversions[dbDefinition[value.columnName].type],
						value.type) > -1) {
						tempColumn.type = value.type;
						needsAnAlter = true;
						changedType = true;
					} else {
						retVal.errors.push(resources.generateError('Invalid Data Conversion Type', 'Can\'t cast column "' + value.columnName + '" from "' +
							dbDefinition[value.columnName].type + '" to "' + value.type + '"'));
						return;
					}
					if (lodash.isBoolean(changedType) && changedType) {
						tempColumn['@previousDef'] = dbDefinition[value.columnName];
					}

					// Check the comments
					if (lodash.isNil(dbDefinition[value.columnName].comment) && lodash.isString(value.comment)) {
						tempColumn.comment = value.comment;
						needsAnAlter = true;
					} else if (lodash.isString(dbDefinition[value.columnName].comment) && lodash.isNil(value.comment)) {
						tempColumn.comment = value.comment;
						needsAnAlter = true;
					} else if (lodash.isString(dbDefinition[value.columnName].comment) && lodash.isString(value.comment) && value.comment !==
						dbDefinition[value.columnName].comment) {
						tempColumn.comment = value.comment;
						needsAnAlter = true;
					}

					tempColumn.changedType = changedType;
					// Check the default values (if it needs to be removed, added or altered)
					if (!lodash.isNil(dbDefinition[value.columnName].default) && lodash.isNil(value.default)) {
						needsAnAlter = true;
						tempColumn.default = value.default;
					} else if (lodash.isNil(dbDefinition[value.columnName].default) && !lodash.isNil(value.default) && !lodash.has(resources.getSQLStatementsConstants(),
						value.default)) {
						needsAnAlter = true;
						tempColumn.default = value.default;
					} else if (!lodash.has(resources.getSQLStatementsConstants(), value.default)) {
						tempColumn.default = value.default;
					}
					// Check the required values (if it needs to be added or removed)
					if (dbDefinition[value.columnName].required && !(lodash.isBoolean(value.required) ? value.required : false)) {
						tempColumn.required = false;
						needsAnAlter = true;
					} else if (!dbDefinition[value.columnName].required && (lodash.isBoolean(value.required) ? value.required : false)) {
						tempColumn.required = true;
						needsAnAlter = true;
					} else {
						tempColumn.required = value.required;
					}
					// Check the types conversion, the precision, and size changes in decimals and strings.
					if (lodash.indexOf(['string', 'decimal'], value.type) > -1) {
						if (dbDefinition[value.columnName].size > value.size) {
							retVal.errors.push(resources.generateError('Data Loss detected', 'Can\'t change the size of column "' + value.columnName +
								'" from "' + dbDefinition[value.columnName].size + '" to "' + value.size + '"'));
							return;
						} else {
							if (value.type === 'decimal' && lodash.isNumber(dbDefinition[value.columnName].size) && dbDefinition[value.columnName].size > value
								.size) {
								retVal.errors.push(resources.generateError('Data Loss detected', 'Can\'t change the size of column "' + value.columnName +
									'" from "' + dbDefinition[value.columnName].size + '" to "' + value.size + '"'));
								return;
							}
							if (value.type === 'decimal' && lodash.isNumber(dbDefinition[value.columnName].precision) && dbDefinition[value.columnName].precision >
								value.precision) {
								retVal.errors.push(resources.generateError('Data Loss detected', 'Can\'t change the precision of column "' + value.columnName +
									'" from "' + dbDefinition[value.columnName].precision + '" to "' + value.precision + '"'));
								return;
							}

							if (lodash.isNumber(dbDefinition[value.columnName].size) && dbDefinition[value.columnName].size < value.size) {
								needsAnAlter = true;
							} else if (!lodash.isNumber(dbDefinition[value.columnName].size)) {
								needsAnAlter = true;
							}
							tempColumn.size = value.size;
							if (value.type === 'decimal') {
								if (!lodash.isNumber(dbDefinition[value.columnName].precision)) {
									needsAnAlter = true;
								}
								tempColumn.precision = value.precision;
							}
						}
					}
					if (needsAnAlter) {
						retVal.results.differences.alterColumns.push(tempColumn);
					}
				} else {
					if ((lodash.isBoolean(value.primaryKey) ? value.primaryKey : false)) {
						retVal.results.differences.primaryKeys.push(fieldName);
						retVal.results.differences.alterPrimaryKeyConstraint = true;
					}
					if (lodash.isBoolean(value.autoIncrement) && value.autoIncrement) {
						retVal.results.differences.add.sequences.push({
							name: identity + '::' + value.columnName,
							schema: schema,
							value: 1
						});
					}
					retVal.results.differences.add.columns.push(fieldName);
				}
			});

			lodash.forEach(dbDefinition, function(value, key) {
				if (!lodash.has(currentToDBMap, key)) {
					if (lodash.isBoolean(value.autoIncrement) && value.autoIncrement) {
						retVal.results.differences.remove.sequences.push({
							name: identity + '::' + value.columnName,
							schema: schema
						});
					}
					if (lodash.isBoolean(value.primaryKey) && value.primaryKey) {
						retVal.results.differences.alterPrimaryKeyConstraint = true;
					}
					retVal.results.differences.remove.columns.push({
						columnName: key
					});
				}
			});
			return retVal;
		} catch (e) {
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	createColumnQuery: function(definition, hanaMap) {
		let query = '';
		query = '"' + definition.columnName + '" ' + hanaMap[definition.type] + ' ';
		if (definition.type === 'string') {
			query += '(' + definition.size + ') ';
		} else if (definition.type === 'decimal') {
			query += '(' + definition.size + ', ' + definition.precision + ')';
		}
		if (!lodash.isNil(definition.unique) && definition.unique) {
			query += ' UNIQUE ';
		}
		if ((!lodash.isNil(definition.required) && definition.required) || (!lodash.isNil(definition.primaryKey) && definition.primaryKey)) {
			query += ' NOT NULL ';
		} else {
			query += ' NULL ';
		}
		if (!lodash.isNil(definition.default) && !(lodash.isString(definition.default) && lodash.has(resources.getSQLStatementsConstants(),
			lodash.toLower(definition.default)))) {
			if (definition.type === 'string') {
				definition.default = '\'' + definition.default+'\'';
			}
			query += ' DEFAULT ' + definition.default;
		}
		return query;
	},
	getColumnParsedValue: function(tableName, definition) {
		const castMap = {
			'string': 'TO_NVARCHAR',
			'tinyint': 'TO_TINYINT',
			'bigint': 'TO_BIGINT',
			'integer': 'TO_INT',
			'decimal': 'TO_DECIMAL',
			'date': 'TO_DATE',
			'datetime': 'TO_TIMESTAMP',
			'binary': 'TO_BINARY',
			'json': 'TO_NCLOB',
			'time': 'TO_TIME',
			'file': 'TO_BLOB(TO_BINARY('
		};
		let query = '';
		if (definition.type === 'file') {
			query = castMap[definition.type] + tableName + '."' + definition.columnName + '"))';
		} else if (definition.type === 'decimal') {
			query = castMap[definition.type] + '(' + tableName + '."' + definition.columnName + '", ' + definition.size + ',' + definition.precision +
				')';
		} else {
			query = castMap[definition.type] + '(' + tableName + '."' + definition.columnName + '")';
		}
		return query;
	},
	alterColumnType: function(tableName, definition, hanaMap) {
		let retVal = {
			errors: [],
			results: {
				altered: true
			}
		};
		const _this = this;
		let tempDefinition;
		let executionPlan = {
			backUpWasCreated: false,
			backUpWasUpdated: false,
			tempWasCreated: false,
			tempWasUpdated: false,
			actualWasDropped: false,
			tempWasRenamed: false,
			backUpWasDropped: false
		};
		let query, response;
		try {
			// adds backup column
			tempDefinition = definition['@previousDef'];
			tempDefinition.columnName += '_BU';
			query = 'ALTER TABLE ' + tableName + ' ADD(' + _this.createColumnQuery(tempDefinition, hanaMap) + ')';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.backUpWasCreated = true;
			// update backup values
			query = 'UPDATE ' + tableName + 'SET "' + tempDefinition.columnName + '" = "' + definition.columnName + '"';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.backUpWasUpdated = true;
			// adds temp new column
			tempDefinition = lodash.cloneDeep(definition);
			tempDefinition.columnName = definition.columnName + '_TEMP';
			query = 'ALTER TABLE ' + tableName + ' ADD(' + _this.createColumnQuery(tempDefinition, hanaMap) + ')';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.tempWasCreated = true;
			// update values
			query = 'UPDATE ' + tableName + 'SET "' + tempDefinition.columnName + '" = ' + _this.getColumnParsedValue(tableName, definition);
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.tempWasUpdated = true;
			// drops old column
			query = 'ALTER TABLE ' + tableName + ' DROP("' + definition.columnName + '")';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.actualWasDropped = true;
			// rename the temp column to the actual column name
			query = 'RENAME COLUMN ' + tableName + '."' + tempDefinition.columnName + '" TO "' + definition.columnName + '"';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.tempWasRenamed = true;
			// drops backup column
			query = 'ALTER TABLE ' + tableName + ' DROP("' + definition.columnName + '_BU")';
			response = connector.execute(query, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			executionPlan.backUpWasDropped = true;
			return retVal;
		} catch (e) {
			retVal.results.altered = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			if (executionPlan.backUpWasCreated) {
				let queries = {
					dropBackUpColumn: 'ALTER TABLE ' + tableName + ' DROP("' + definition.columnName + '_BU")',
					dropTempColumn: 'ALTER TABLE ' + tableName + ' DROP("' + definition.columnName + '_TEMP")',
					renameBackUpColumn: 'RENAME COLUMN ' + tableName + '."' + definition.columnName + '_BU" TO "' + definition.columnName + '"'
				};
				if (executionPlan.backUpWasUpdated) {
					if (executionPlan.tempWasCreated) {
						if (executionPlan.tempWasUpdated) {
							if (executionPlan.actualWasDropped) {
								if (executionPlan.tempWasRenamed) {
									if (!executionPlan.backUpWasDropped) {
										response = connector.execute(queries.renameBackUpColumn, null, true);
									}
								} else {
									response = connector.execute(queries.renameBackUpColumn, null, true);
									response = connector.execute(queries.dropTempColumn, null, true);
								}
							} else {
								response = connector.execute(queries.dropBackUpColumn, null, true);
								response = connector.execute(queries.dropTempColumn, null, true);
							}
						} else {
							response = connector.execute(queries.dropBackUpColumn, null, true);
							response = connector.execute(queries.dropTempColumn, null, true);
						}
					} else {
						response = connector.execute(queries.dropBackUpColumn, null, true);
					}
				} else {
					response = connector.execute(queries.dropBackUpColumn, null, true);
				}
			}
			return retVal;
		}
	},
	alterTable: function(model, diagnosis) {
		let retVal = {
			errors: [],
			results: {
				altered: true,
				primaryKeys: [],
				sequences: [],
				constraints: [],
				columns: {
					altered: [],
					added: [],
					removed: []
				},
				alteredSequences: []
			}
		};
		retVal.results['@diagnosis'] = diagnosis;
		const hanaMap = resources.getSQLMap();
		const _this = this;
		try {
			/* Strategy:
                1) Remove Primary key constraint - Done
                2) Remove constraints - Done
                3) Remove sequences - Done
                4) Remove Columns - Done
                5) Add Columns - Done
                6) Add Sequences - Done
                7) Add Constraints - Done
                8) Alter columns - Pending
                9) Alter sequences 
                10) Add primary key constraint - Done
            */
			let response, query, definition = model.getDefinition();
			let commentQueries = [];
			/* Drops the primary key constraint, if the primary key needs to be altered */
			if (diagnosis.alterPrimaryKeyConstraint) {
				query = 'ALTER TABLE ' + definition.tableName + ' DROP PRIMARY KEY';
				response = connector.execute(query, null, true);
				if (!lodash.isEmpty(response.errors) && lodash.isPlainObject(response.errors[0]) && !lodash.isNil(response.errors[0]) && response.errors[
					0].name !== 'ERR_SQL_INV_INDEX') {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				}
			}

			/* Drops the constraints */
			if (!lodash.isEmpty(diagnosis.remove.constraints)) {
				lodash.forEach(diagnosis.remove.constraints, function(constraint) {
					response = services.dropConstraint(model.getSchema(), model.getIdentity(), constraint.name);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
					retVal.results.constraints.push(response.results);
				});
			}

			/* Drops the sequences */
			if (!lodash.isEmpty(diagnosis.remove.sequences)) {
				lodash.forEach(diagnosis.remove.sequences, function(sequence) {
					response = services.dropSequence(sequence.schema, sequence.name);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
					retVal.results.sequences.push(response.results);
				});
			}

			/* Drops the columns */
			if (!lodash.isEmpty(diagnosis.remove.columns)) {
				let deletedColumns = [],
					parsedDeletedColumns = [];
				lodash.forEach(diagnosis.remove.columns, function(column) {
					deletedColumns.push({
						name: column.columnName,
						operation: 'DROP'
					});
					parsedDeletedColumns.push(column.columnName);
				}); 
				parsedDeletedColumns = parsedDeletedColumns.map(function(column) {
                    return '"' + column + '"';
                });
				query = 'ALTER TABLE ' + definition.tableName + ' DROP(' + lodash.join(parsedDeletedColumns, ',') + ')';
				response = connector.execute(query, null, true);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				}
				retVal.results.columns.removed = deletedColumns;
			}

			/* Creates the columns */
			if (!lodash.isEmpty(diagnosis.add.columns)) {
				let addColumnsQuery = 'ALTER TABLE ' + definition.tableName + ' ADD(';
				let parsedColumns = [],
					statusColumns = [],
					tempFieldDef = {};
				lodash.forEach(diagnosis.add.columns, function(column) {
					tempFieldDef = definition.fields[column];
					statusColumns.push({
						name: tempFieldDef.columnName,
						operation: 'CREATE'
					});
					query = _this.createColumnQuery(tempFieldDef, hanaMap);
					parsedColumns.push(query);

					/* Creating the comments of the column*/
					if (lodash.isString(tempFieldDef.comment)) {
						commentQueries.push({
							schema: definition.schema,
							identity: definition.identity,
							comment: tempFieldDef.comment,
							column: tempFieldDef.columnName
						});
					}
				});
				addColumnsQuery += lodash.join(parsedColumns, ',') + ')';
				response = connector.execute(addColumnsQuery, null, true);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				}
				retVal.results.columns.added = statusColumns;
			}

			/* Creates the sequences */
			if (!lodash.isEmpty(diagnosis.add.sequences)) {
				lodash.forEach(diagnosis.add.sequences, function(sequence) {
					response = services.createSequence(sequence.schema, sequence.name, sequence.value);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
					retVal.results.sequences.push(response.results);
				});
			}

			/* Creates the constraints */
			if (!lodash.isEmpty(diagnosis.add.constraints)) {
				lodash.forEach(diagnosis.add.constraints, function(constraint) {
					if (constraint.unique) {
						response = services.addUniqueConstraint(model.getSchema(), model.getIdentity(), constraint.columnName);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(response.errors, retVal.errors);
						}
						retVal.results.constraints.push(response.results);
					}
				});
			}

			/* Alters the columns (Can't change the data type) */
			if (!lodash.isEmpty(diagnosis.alterColumns)) {
				let alterColumnsQuery = 'ALTER TABLE ' + definition.tableName + ' ALTER(';
				let parsedAlterColumns = [],
					statusAlterColumns = [];
				lodash.forEach(diagnosis.alterColumns, function(column) {
					if (lodash.isBoolean(column.changedType) && column.changedType) {
						response = _this.alterColumnType(definition.tableName, column, hanaMap);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(response.errors, retVal.errors);
						} else {
							/* Creating the comments of the column*/
							if (lodash.isString(column.comment)) {
								commentQueries.push({
									schema: definition.schema,
									identity: definition.identity,
									comment: column.comment,
									column: column.columnName
								});
							}
							statusAlterColumns.push({
								name: column.columnName,
								operation: 'ALTER'
							});
						}
					} else {
						statusAlterColumns.push({
							name: column.columnName,
							operation: 'ALTER'
						});
						query = _this.createColumnQuery(column, hanaMap);
						parsedAlterColumns.push(query);
						/* Altering the comments of the column*/
						commentQueries.push({
							schema: definition.schema,
							identity: definition.identity,
							comment: column.comment,
							column: column.columnName
						});
					}
				});
				if (!lodash.isEmpty(parsedAlterColumns)) {
					alterColumnsQuery += lodash.join(parsedAlterColumns, ',') + ')';
					response = connector.execute(alterColumnsQuery, null, true);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
				}
				retVal.results.columns.altered = statusAlterColumns;
			}
			/* Alter sequences */
			if (!lodash.isEmpty(diagnosis.update.sequences)) {
				lodash.forEach(diagnosis.update.sequences, function(sequence) {
					response = services.alterSequence(sequence.schema, sequence.name, sequence.value);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
					retVal.results.alteredSequences.push(response.results);
				});
			}
			/* Adds the primary key constraint, if the primary key needs to be altered */
			if (diagnosis.alterPrimaryKeyConstraint) {
				query = 'ALTER TABLE ' + definition.tableName + ' ADD CONSTRAINT "' + model.getIdentity() + '_PK" PRIMARY KEY (';
				let primaryKeys = [],
					newPrimaryKeys = [];
				lodash.forEach(diagnosis.primaryKeys, function(primaryKey) {
					if (lodash.has(definition.fields, primaryKey)) {
						primaryKeys.push('"' + definition.fields[primaryKey].columnName + '"');
						newPrimaryKeys.push(primaryKey);
					}
				});
				if (!lodash.isEmpty(primaryKeys)) {
					query += lodash.join(primaryKeys, ',') + ')';
					response = connector.execute(query, null, true);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					}
				}
				retVal.results.primaryKeys = newPrimaryKeys;
			} else {
				retVal.results.primaryKeys = definition.primaryKeys;
			}
			// Alter Columns
			if (!lodash.isEmpty(commentQueries)) {
				response = _this.alterComments(commentQueries);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				}
			}
			return retVal;
		} catch (e) {
			retVal.results.altered = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	alterComments: function(columns) {
		let retVal = {
			errors: [],
			results: {
				altered: true
			}
		};
		try {
			let response;
			lodash.forEach(columns, function(column) {
				if (lodash.isNil(column.comment)) {
					response = services.deleteComment(column.schema, column.identity, column.column);
				} else {
					response = services.alterComment(column.schema, column.identity, column.column, column.comment);
				}
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				}
			});
			return retVal;
		} catch (e) {
			retVal.results.altered = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	alterModelStrategy: function(model) {
		let retVal = {
			errors: [],
			results: {
				altered: true,
				columns: {
					altered: [],
					added: [],
					removed: []
				},
				table: {},
				sequences: [],
				constraints: [],
				indexes: []
			}
		};
		const _this = this;
		try {
			let response = model.describe();
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			performance.execute.installModels[model.getIdentity()].alterModelStrategy.calculateDifferences = new Date().getTime();
			response = _this.calculateDifferences(model.getSchema(), model.getIdentity(), response.results.definition.fields, model.getDefinition().fields);
			performance.execute.installModels[model.getIdentity()].alterModelStrategy.calculateDifferences = (new Date().getTime() - performance.execute
				.installModels[model.getIdentity()].alterModelStrategy.calculateDifferences) / 1000;
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			performance.execute.installModels[model.getIdentity()].alterModelStrategy.alterTable = new Date().getTime();
			response = _this.alterTable(model, response.results.differences);
			performance.execute.installModels[model.getIdentity()].alterModelStrategy.alterTable = (new Date().getTime() - performance.execute.installModels[
				model.getIdentity()].alterModelStrategy.alterTable) / 1000;
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
			}
			retVal.results = response.results;
			return retVal;
		} catch (e) {
			retVal.results.installed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	newModelStrategy: function(model) {
		let response;
		try {
			response = model.sync();
		} catch (e) {
			response = e;
		}
		return response;
	},
	installModels: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true
			}
		};
		let hasErrors = false;
		const _this = this;
		try {
			/*
				-> Check if the table exists
					* If it exists, check for table structure differences
						# if the table is the same (using the definition and describe operation)
							+ return retVal
						# If the table has differences
							+ Remove columns (Drop Columns Operation)
							+ Add columns (Add Columns operation)
							+ Alter columns (Think about a strategy)
							+ Drop Sequences (Drop Sequences operation)
							+ Add Sequences (Add Sequences operation)
							+ Remove unique constraint (removeConstraint operation)
							+ Add unique constraint (addConstraint operation)
							+ Remove Indexes (removeIndex operation)
							+ Add Indexes (addIndex operation)
					* If it doesn't exist, sync the table
    		*/
			let response;
			let exists;
			let models = [];
			if (_this.totalPages === -1) {
				_this.totalPages = Math.ceil(_this.models.length / maxModelsPerInstallation) + 1;
			}
			performance.execute.installModels.getSubset = new Date().getTime();
			let i = ((_this.page - 1) * maxModelsPerInstallation);
			let maxValue = (maxModelsPerInstallation * _this.page);
			for (; i < maxValue; ++i) {
				if (lodash.isNil(_this.models[i])) {
					break;
				} else {
					models.push(_this.models[i]);
				}
			}
			performance.execute.installModels.getSubset = (new Date().getTime() - performance.execute.installModels.getSubset) / 1000;
			lodash.forEach(models, function(baseModel) {
				if (baseModel instanceof BaseModel && baseModel.getType() !== 'table') {
					return;
				} else if (baseModel instanceof Table) {
					//get table name eliminating the "  and the schema and the component
					let tableName = baseModel.name.replace(new RegExp('"', 'g'), '').split('.')[1];
					let identity = tableName.substring(tableName.search('::') + 2);
					let identity2 = tableName;
					// 	let identity = baseModel.name.replace(new RegExp('"', 'g'), '').split('.')[1].split('::')[1];
					// 	let identity2 = baseModel.name.replace(new RegExp('"', 'g'), '').split('.')[1];
					performance.execute.installModels[identity2] = {};
					performance.execute.installModels[identity2].fromCore1ToCore2Parse = new Date().getTime();
					let definition = resources.parseCore1ModelTocoreModel(baseModel);
					performance.execute.installModels[identity2].fromCore1ToCore2Parse = (new Date().getTime() - performance.execute.installModels[
						identity2].fromCore1ToCore2Parse) / 1000;
					if (lodash.isPlainObject(definition) && (lodash.isNil(definition) || lodash.isEmpty(definition.fields))) {
						retVal.results[identity] = {
							errors: [],
							results: {
								altered: false
							}
						};
						retVal.results[identity].errors.push({
							'@collection': identity,
							name: 'Invalid Model',
							trace: 'Invalid Fields'
						});
						retVal.errors.push({
							'@collection': identity,
							name: 'Invalid Model',
							trace: 'Invalid Fields'
						});
						hasErrors = true;
						return;
					}
					try {
						performance.execute.installModels[identity2].creatingBaseModelInstance = new Date().getTime();
						baseModel = new BaseModel(definition);
						performance.execute.installModels[identity2].creatingBaseModelInstance = (new Date().getTime() - performance.execute.installModels[
							identity2].creatingBaseModelInstance) / 1000;
						if (!baseModel.isValidBaseModel()) {
							hasErrors = true;
                            retVal.errors.push({
                                '@collection': identity,
                                name: 'Invalid Model',
                                trace: 'Invalid Fields'
                            });
							return;
						}
					} catch (e) {
						retVal.results[identity] = {
							errors: [],
							results: {
								altered: false
							}
						};
						retVal.results[identity].errors = e.errors;
						retVal.errors = retVal.errors.concat(e.errors);
						hasErrors = true;
						performance.execute.installModels[identity2].creatingBaseModelInstance = (new Date().getTime() - performance.execute.installModels[
							identity2].creatingBaseModelInstance) / 1000;
						return;
					}
				} else if (!(baseModel instanceof BaseModel)) {
					return;
				}
				if (lodash.isNil(performance.execute.installModels[baseModel.getIdentity()])) {
					performance.execute.installModels[baseModel.getIdentity()] = {};
				}
				performance.execute.installModels[baseModel.getIdentity()].exists = new Date().getTime();
				try {
					response = baseModel.exists();
				} catch (e) {
					response = e;
				}
				performance.execute.installModels[baseModel.getIdentity()].exists = (new Date().getTime() - performance.execute.installModels[
					baseModel.getIdentity()].exists) / 1000;
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
					hasErrors = true;
				} else {
					exists = response.results.exists;
					if (exists) {
						performance.execute.installModels[baseModel.getIdentity()].alterModelStrategy = {
							total: new Date().getTime()
						};
						response = _this.alterModelStrategy(baseModel);
						performance.execute.installModels[baseModel.getIdentity()].alterModelStrategy.total = (new Date().getTime() - performance.execute.installModels[
							baseModel.getIdentity()].alterModelStrategy.total) / 1000;
					} else {
						performance.execute.installModels[baseModel.getIdentity()].newModelStrategy = new Date().getTime();
						response = _this.newModelStrategy(baseModel);
						performance.execute.installModels[baseModel.getIdentity()].newModelStrategy = (new Date().getTime() - performance.execute.installModels[
							baseModel.getIdentity()].newModelStrategy) / 1000;
						lodash.assign(response.results, {
							columns: {
								altered: [],
								removed: [],
								added: []
							}
						});
					}
					if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
                        retVal.errors = retVal.errors.concat(response.errors);
						hasErrors = true;
					}
					retVal.results[baseModel.getName()] = response;
				}
			});
		} catch (e) {
			retVal.results.executed = false;
			hasErrors = true;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
		}
		retVal['@hasErrors'] = hasErrors;
		return retVal;
	},
	installComponent: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true,
				instance: {}
			}
		};
		const _this = this;
		try {
			let response;
			try {
				response = componentModel.find({
					where: [{
						field: 'name',
						operator: '$eq',
						value: _this.component.name
                }]
				});
			} catch (e) {
				response = e;
			}
			if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
				throw retVal;
			}
			let options = {
				where: []
			};
			if (!lodash.isEmpty(response.results)) {
				options.where.push({
					field: 'name',
					operator: '$eq',
					value: response.results[0].name
				});
				this.component.id = response.results[0].id;
			}
			try {
				response = componentModel.upsert(this.component, options, true);
			} catch (e) {
				response = e;
			}
			if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
				throw retVal;
			} else if (lodash.isArray(response.results.instances) && !lodash.isEmpty(response.results.instances)) {
				retVal.results.instance = response.results.instances[0];
				_this.component = response.results.instances[0];
			}
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	installPrivileges: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true,
				instances: []
			}
		};
		const _this = this;
		try {
			let roleMap = {};
			let response;
			let currentRoles = [];
			lodash.forEach(_this.privileges.base, function(privilege) {
				privilege.componentId = _this.component.id;
				roleMap[privilege.role] = privilege;
				currentRoles.push(privilege.role);
			});
			if (!$.lodash.isEmpty(currentRoles)) {
				response = privilegeModel.delete({
					where: [{
						field: 'role',
						operator: '$notIn',
						value: currentRoles
			        }, {
						field: 'componentId',
						operator: '$eq',
						value: _this.component.id
    			    }]
				}, true);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
				} else {
					retVal.results.deleted = response.results.instances;
				}
			}
			let privileges = privilegeModel.find({
				select: [{
					field: 'role'
			    }],
				where: [{
					field: 'componentId',
					operator: '$eq',
					value: _this.component.id
			    }]
			});
			let updatePrivileges = [];
			let newPrivileges = [];
			if (lodash.isEmpty(privileges.results)) {
				lodash.forEach(roleMap, function(instance) {
					newPrivileges.push(instance);
				});
			} else {
				lodash.forEach(privileges.results, function(privilege) {
					if (lodash.has(roleMap, privilege.role)) {
						updatePrivileges.push(roleMap[privilege.role]);
						delete roleMap[privilege.role];
					}
				});
				lodash.forEach(roleMap, function(instance) {
					if (!lodash.isNil(instance)) {
						newPrivileges.push(instance);
					}
				});
			}
			if (!lodash.isEmpty(newPrivileges)) {
				response = privilegeModel.batchCreate(newPrivileges, {
					getInstances: true
				});
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
				}
				retVal.results.instances = lodash.concat(retVal.results.instances, response.results.instances);
			}
			if (!lodash.isEmpty(updatePrivileges)) {
				let values = [];
				let query = 'UPDATE ' + privilegeModel.getDefinition().tableName +
					' SET "NAME" = ?, "DESCRIPTION_ENUS" = ?, "DESCRIPTION_PTBR" = ? WHERE "ROLE" = ?';
				lodash.forEach(updatePrivileges, function(privilege) {
					if (lodash.isString(privilege.name) && lodash.isString(privilege.descriptionEnus) && lodash.isString(privilege.descriptionPtbr) &&
						lodash.isString(privilege.role)) {
						values.push([privilege.name, privilege.descriptionEnus, privilege.descriptionPtbr, privilege.role]);
					}
				});
				response = connector.executeUpdate(query, values);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
				}
			}
			if (!lodash.isEmpty(retVal.errors)) {
				throw retVal;
			}
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	installI18n: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true,
				instances: []
			}
		};
		const _this = this;
		try {
			_this.i18n.new = lodash.map(_this.i18n.new, function(instance) {
				instance.componentId = _this.component.id;
				return instance;
			});
			_this.i18n.base = lodash.map(_this.i18n.base, function(instance) {
				instance.componentId = _this.component.id;
				return instance;
			});
			_this.i18n.update = lodash.map(_this.i18n.update, function(instance) {
				instance.instance.componentId = _this.component.id;
				return instance;
			});
			return _this.executeInstallerSeeder(i18nModel, _this.i18n);
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	installLabels: function() {
		let retVal = {
			errors: [],
			results: {
				executed: true,
				instances: []
			}
		};
		const _this = this;
		try {
			_this.labels.new = lodash.map(_this.labels.new, function(instance) {
				instance.componentId = _this.component.id;
				return instance;
			});
			_this.labels.base = lodash.map(_this.labels.base, function(instance) {
				instance.componentId = _this.component.id;
				return instance;
			});
			_this.labels.update = lodash.map(_this.labels.update, function(instance) {
				instance.instance.componentId = _this.component.id;
				return instance;
			});
			return _this.executeInstallerSeeder(labelModel, _this.labels);
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	executeObjectActions: function() { 
		let retVal = {
			errors: [],
			results: {
				executed: true,
				objectTypes: {
					created: [],
					updated: []
				},
				actions: {
					created: [],
					updated: []
				}
			}
		};
		const _this = this;
		try {
			const objectTypeModel = objectTypeCoreModel;
			let response;
			if (lodash.isPlainObject(_this.objectActions) && !lodash.isNil(_this.objectActions) && !lodash.isEmpty(_this.objectActions)) {
				let objectTypes = _this.objectActions.objectTypesActions;
				// Update or create objectTypeActions

				if (!lodash.isNil(objectTypes)) {
					let databaseObjectTypes = objectTypeModel.find({
						select: [{
							field: 'ID'
    					}, {
							field: 'NAME'
    					}],
						where: [{
							field: 'NAME',
							operator: '$in',
							value: Object.keys(objectTypes)
    			       }]
					});

					if (!lodash.isEmpty(databaseObjectTypes.errors)) {
						retVal.errors = databaseObjectTypes.errors;
					}

					lodash.forEach(objectTypes, function(value, objectType) {
						let action = lodash.find(databaseObjectTypes.results, function(result) {
							return result.NAME === objectType;
						});

						if (lodash.isNil(action)) {
							response = objectTypeModel.create({
								NAME: objectType,
								ID_COMPONENT: _this.component.id
							});
							if (!lodash.isEmpty(response.errors)) {
								retVal.errors = lodash.concat(retVal.errors, response.errors);
							} else {
								retVal.results.objectTypes.created.push(response.results.instance);
							}
						}
					});
					if (!lodash.isEmpty(retVal.errors)) {
						throw retVal;
					}
					response = _this.installActions(objectTypeModel);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
						throw retVal;
					}
				}
			}
			return retVal;
		} catch (e) {
			retVal.results.executed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	installActions: function(objectTypeModel) {
		let retVal = {
			errors: [],
			results: {
				created: []
			}
		};
		const _this = this;
		try {
			const actionsModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::Actions');
			const actionsParametersModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::ActionsParameters');

			let response = objectTypeModel.find({
				select: [{
					field: 'ID',
					as: 'id'
		        }, {
					field: 'NAME',
					as: 'name'
		        }]
			});
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
				throw retVal;
			}
			let objectTypeMap = {};
			lodash.forEach(response.results, function(objectType) {
				objectTypeMap[objectType.name] = objectType;
			});

			let actions = {};
			// Import the component actions
			try {
				$.import('timp.' + lodash.toLower(_this.component.name) + '.server.api', 'api');
				actions = $.timp[lodash.toLower(_this.component.name)].server.api.api.componentActions;
			} catch (e) {
				actions = {};
			}

			if (lodash.isEmpty(actions)) {
				return retVal;
			}
			let params = [];
			let objectTypeId = 0;
			let tempAction;
			let actionTypeMap = {
				'manual': 0,
				'automatic': 1,
				'gatewayTask': 2,
				'0': 0,
				'1': 1,
				'2': 2
			};
			let actionCloseMap = {
				'manual': 0,
				'stage': 1,
				'0': 0,
				'1': 1
			};

			let currentActionIds = [];
			lodash.forEach(actions, function(action, name) {
				params = [];
				if (!(!lodash.isNil(action) && action.hasOwnProperty('instanceOf') && action.instanceOf === 'BaseAction')) {
					return;
				}

				// Verify the existence of the input's object types
				lodash.forEach(action.input, function(input, idx) {
					if (!lodash.has(objectTypeMap, input)) {
						// Create the objectType that doesn't exist
						response = objectTypeModel.create({
							NAME: input,
							ID_COMPONENT: _this.component.id
						});
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
							throw retVal;
						} else {
							objectTypeMap[response.results.instance.NAME] = {
								id: response.results.instance.ID,
								name: response.results.instance.NAME
							};
						}
					}
					// action's object type
					if (idx === 0 && lodash.has(objectTypeMap, input)) {
						objectTypeId = objectTypeMap[input].id;
					}

					params.push({
						TYPE: 0,
						ID_OBJ_TYPE: objectTypeMap[input].id
					});
				});

				// Verify the existence of the output's object types
				lodash.forEach(action.output, function(output) {
					if (!lodash.has(objectTypeMap, output)) {
						// Create the objectType that doesn't exist
						response = objectTypeModel.create({
							NAME: output,
							ID_COMPONENT: _this.component.id
						});
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
							throw retVal;
						} else {
							objectTypeMap[response.results.instance.NAME] = {
								id: response.results.instance.ID,
								name: response.results.instance.NAME
							};
						}
					}

					params.push({
						TYPE: 1,
						ID_OBJ_TYPE: objectTypeMap[output].id
					});
				});

				// Upsert the action
				tempAction = {
					ID_COMPONENT: _this.component.id,
					ACTION: name,
					ID_OBJ_TYPE: objectTypeId,
					URL: lodash.isNil(action.url) ? null : action.url,
					ACTION_TYPE: actionTypeMap[action.type] || 0,
					CLOSE_TYPE: actionCloseMap[action.close] || 0,
					NAME_ENUS: (lodash.isString(action.nameEnus)) ? action.nameEnus : null,
					NAME_PTBR: (lodash.isString(action.namePtbr)) ? action.namePtbr : null,
					REMOVE: (lodash.isString(action.remove)) ? action.remove : null
				};

				response = actionsModel.find({
					select: [{
						field: 'ID'
				    }],
					where: [{
						field: 'ACTION',
						operator: '$eq',
						value: tempAction.ACTION
				    }, {
						field: 'ID_COMPONENT',
						operator: '$eq',
						value: tempAction.ID_COMPONENT
				    }]
				});

				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
					throw retVal;
				} else if (!lodash.isEmpty(response.results)) {
					currentActionIds.push(response.results[0].ID);
					response = actionsModel.update(tempAction, {
						where: [{
							field: 'ID',
							operator: '$eq',
							value: response.results[0].ID
				        }]
					});
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
						throw retVal;
					}
				} else {
					response = actionsModel.create(tempAction);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
						throw retVal;
					} else {
						retVal.results.created.push(response.results.instance);
						currentActionIds.push(response.results.instance.ID);
					}
				}

				// Remove all action Parameters for this action
				response = actionsParametersModel.delete({
					where: [{
						field: 'ID_ACTION',
						operator: '$eq',
						value: currentActionIds.slice(-1)[0]
				    }]
				});

				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
					throw retVal;
				}

				// Create all action Parameters
				if (!lodash.isEmpty(params)) {
					params = lodash.map(params, function(param) {
						param.ID_ACTION = currentActionIds.slice(-1)[0];
						return param;
					});

					response = actionsParametersModel.batchCreate(params);

					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
						throw retVal;
					}
				}
			});

			// Delete deprecated actions and action prameters
			if (!lodash.isEmpty(currentActionIds)) {
				response = actionsModel.delete({
					where: [{
						field: 'ID_COMPONENT',
						operator: '$eq',
						value: _this.component.id
			        }, {
						field: 'ID',
						operator: '$notIn',
						value: currentActionIds
			        }]
				}, true);

				let deletedActionsId = [];
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
					throw retVal;
				} else {
					deletedActionsId = lodash.map(response.results.instances, function(instance) {
						return instance.ID;
					});
				}

				if (!lodash.isEmpty(deletedActionsId)) {
					response = actionsParametersModel.delete({
						where: [{
							field: 'ID_ACTION',
							operator: '$in',
							value: deletedActionsId
    			        }]
					});

					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
						throw retVal;
					}
				}
			}
			return retVal;
		} catch (e) {
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
			return retVal;
		}
	},
	execute: function() {
		performance.execute = {};
		/*
			Execution Plan:
				1) Before Install
				2) Install
				3) Install Component
				4) Install Privileges
				5) Install i18n
				6) Install labels
				7) Seeder
				8) Install ObjectActions
				9) After Install
    	*/
		const _this = this;
		let retVal = {
			errors: [],
			results: {
				"installed": false,
				"beforeInstall": {
					"errors": [],
					"results": {
						"executed": true
					}
				},
				"installer": {
					"errors": [],
					"results": {
						"executed": false,
						"results": {

						}
					}
				},
				"component": {
					"errors": [],
					"results": {
						"executed": false,
						"instance": {}
					}
				},
				"privileges": {
					"errors": [],
					"results": {
						"executed": false,
						"instances": {
							"created": [],
							"updated": []
						}
					}
				},
				"i18n": {
					"errors": [],
					"results": {
						"executed": false,
						"instances": {
							"created": [],
							"updated": []
						}
					}
				},
				"labels": {
					"errors": [],
					"results": {
						"executed": false,
						"instances": {
							"created": [],
							"updated": []
						}
					}
				},
				"seeder": {
					"errors": [],
					"results": {
						"executed": false
					}
				},
				"objectActions": {
					"errors": [],
					"results": {
						"executed": false
					}
				},
				"afterInstall": {
					"errors": [],
					"results": {
						"executed": false
					}
				}
			}
		};
		let response;
		let installed = true;
		try {
			if (_this.page === 1) {
				performance.execute.beforeInstall = new Date().getTime();
				// Execute beforeInstall function (Step 1)
				response = _this.executeBeforeInstall();
				retVal.results.beforeInstall = response;
				performance.execute.beforeInstall = (new Date().getTime() - performance.execute.beforeInstall) / 1000;
				if (!lodash.isEmpty(response.errors)) {
					throw retVal;
				}

			}
			// Prepare ValidConversion Cache
			if (lodash.isNil(Installer.validConversions) || lodash.isEmpty(Installer.validConversions)) {
				let validConversions = resources.getValidConversions();
				if (!lodash.isEmpty(validConversions.errors)) {
					retVal.errors = lodash.concat(retVal.errors, validConversions.errors);
				}
				Installer.validConversions = validConversions.results;
			}
			// Execute install function (Step 2)
			performance.execute.installModels = {
				total: new Date().getTime()
			};
			response = _this.installModels();
			installed = !response['@hasErrors'];
			delete response['@hasErrors'];
			retVal['@totalPages'] = _this.totalPages;
			retVal['@currentPage'] = _this.page;
			retVal.results.installer = response;
			retVal.errors = response.errors.slice();
			performance.execute.installModels.total = (new Date().getTime() - performance.execute.installModels.total) / 1000;
			if (!lodash.isEmpty(response.errors)) {
				throw retVal;
			}
			if (_this.page === _this.totalPages) {
				// Install Component (Step 3)
				performance.execute.installComponent = new Date().getTime();
				response = _this.installComponent();
				retVal.results.component = response;
				performance.execute.installComponent = (new Date().getTime() - performance.execute.installComponent) / 1000;
				if (!lodash.isEmpty(response.errors)) {
					throw retVal;
				}

				// Install Privileges (Step 4)
				performance.execute.installPrivileges = {
					total: new Date().getTime()
				};
				response = _this.installPrivileges();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.privileges = response;
				performance.execute.installPrivileges.total = (new Date().getTime() - performance.execute.installPrivileges.total) / 1000;

				// Install i18n (Step 5)
				performance.execute.i18n = {
					total: new Date().getTime()
				};
				response = _this.installI18n();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.i18n = response;
				performance.execute.i18n.total = (new Date().getTime() - performance.execute.i18n.total) / 1000;

				// Install Labels (Step 6)
				performance.execute.labels = {
					total: new Date().getTime()
				};
				response = _this.installLabels();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.labels = response;
				performance.execute.labels.total = (new Date().getTime() - performance.execute.labels.total) / 1000;

				// Execute seeder function (Step 7)
				performance.execute.seeder = {
					total: new Date().getTime()
				};
				response = _this.executeSeeder();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.seeder = response;
				performance.execute.seeder.total = (new Date().getTime() - performance.execute.seeder.total) / 1000;

				// Execute objectActions function (Step 8)
				performance.execute.objectActions = {
					total: new Date().getTime()
				};
				response = _this.executeObjectActions();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.objectActions = response;
				performance.execute.objectActions.total = (new Date().getTime() - performance.execute.objectActions.total) / 1000;

				// Execute afterInstall function (Step 9)
				performance.execute.afterInstall = {
					total: new Date().getTime()
				};
				response = _this.executeAfterInstall();
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					installed = false;
				}
				retVal.results.afterInstall = response;
				performance.execute.afterInstall.total = (new Date().getTime() - performance.execute.afterInstall.total) / 1000;
			}
			retVal.results.installed = installed;
		} catch (e) {
			retVal.results.installed = false;
			e = resources.parseError(e);
			if (!lodash.isEqual(retVal, e)) {
				retVal.errors.push(e);
			}
		}
		const hasErrors = (!lodash.isEmpty(retVal.errors) || !retVal.results.installed);
		let logInstance = logInstaller(retVal, hasErrors);
		if (lodash.isArray(logInstance.errors) && !lodash.isEmpty(logInstance.errors)) {
			retVal.errors = lodash.concat(retVal.errors, logInstance.errors);
		} else {
			logInstance = logInstance.results.instance;
		}
		retVal.results = {
			installed: retVal.results.installed,
			logId: logInstance.id
		};
		// 		retVal['@performance'] = performance;
		return retVal;
	},
	install: function() {
		return this.execute();
	}
};

this.Installer = Installer;