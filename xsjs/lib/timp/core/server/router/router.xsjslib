$.messageCodes = [];

// eslint-disable-next-line no-redeclare
let performance = {
	imports: new Date().getTime(),
	instance: null,
	parseURL: null
};

$.import('timp.core.server.libraries.internal', 'util');
$.import('timp.core.server.models.views', 'UserTaxes');

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

$.import('timp.core.server.controllers.refactor', 'user');
const userCtrl = $.timp.core.server.controllers.refactor.user;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;

$.import('timp.core.server.router.middlewares.logger', 'logger');
const logger = $.timp.core.server.router.middlewares.logger.logger;

$.import('timp.core.server.router.middlewares', 'errorHandler');
const errorHandler = $.timp.core.server.router.middlewares.errorHandler;


performance.imports = (new Date().getTime() - performance.imports) / 1000;

function Router(component, routes) {
	performance.instance = new Date().getTime();
	this.init(component, routes);
	performance.instance = (new Date().getTime() - performance.instance) / 1000;
}

Router.prototype = {
	init: function (component, routes) {
		this.routes = [];
		this.allowRequestsWithoutLogin = true;
		this.component = null;
		if ($.lodash.isArray(routes) && !$.lodash.isEmpty(routes)) {
			this.routes = routes;
		}
		if ($.lodash.isNil(Router.components)) {
			Router.components = {};
			let response;
			try {
				response = componentModel.find({
					select: [{
						field: 'id'
					}, {
						field: 'name'
					}]
				});
			} catch (e) {
				response = e;
			}
			if ($.lodash.isArray(response.results) && !$.lodash.isEmpty(response.results)) {
				$.lodash.forEach(response.results, function (_component) {
					Router.components[_component.name] = _component.id;
				});
			}
		}
		if ($.lodash.isString(component) && $.lodash.has(Router.components, $.lodash.toUpper(component))) {
			this.component = $.lodash.toUpper(component);
		}
		this.methodMap = {
			'GET': 'GET',
			'POST': 'POST',
			'DELETE': 'DEL',
			'PUT': 'PUT',
			'PATCH': 'PATCH'
		};
	},
	stringify: function (value) {
		if ($.lodash.isString(value)) {
			return value;
		}
		let seen = [];

		return JSON.stringify(value, function (key, val) {
			if ($.lodash.isNil(val) && $.lodash.isPlainObject(val)) {
				if (seen.indexOf(val) !== -1) {
					try {
						JSON.stringify(val);
						return val;
					} catch (e) {
						return 'cyclic value!';
					}
				}
				seen.push(val);
			}
			return val;
		}) || typeof value;
	},
	parseResponse: function (value) {
		if (value && typeof value === 'object' && value.constructor.name === 'ArrayBuffer') {
			$.response.setBody(value);
		} else {
			value = this.stringify(value);
			$.response.setBody(value);
		}
		return value;
	},
	parseParam: function (type, value) {
		let parsedParam = null;
		try {
			if (!$.lodash.isNil(value) && type === 'json' && !$.lodash.isPlainObject(value)) {
				parsedParam = JSON.parse(value);
			} else if (!$.lodash.isNil(value) && type === 'decimal') {
				parsedParam = parseFloat(value);
			} else if (!$.lodash.isNil(value) && $.lodash.indexOf(['integer', 'tinyint', 'bigint'], type) > -1) {
				parsedParam = parseInt(value, 10);
			} else if (!$.lodash.isNil(value) && type === 'boolean') {
				parsedParam = value === 'true';
			} else {
				if (value instanceof ArrayBuffer && type !== 'file') {
					parsedParam = $.util.stringify(value);
				} else {
					parsedParam = value;
				}
			}
		} catch (e) {
			parsedParam = null;
		}
		return parsedParam;
	},
	userHasPrivilegeToExecuteTheEndpoint: function (endpoint) {
		const _this = this;
		endpoint.bypass = $.lodash.isBoolean(endpoint.bypass) ? endpoint.bypass : false;
		endpoint.privileges = $.lodash.isArray(endpoint.privileges) ? endpoint.privileges : [];
		let response = userCtrl.userHasPrivileges(endpoint.privileges, _this.component);
		const valid = $.lodash.isBoolean(response.valid) ? response.valid : false;
		return endpoint.bypass || $.lodash.isEmpty(endpoint.privileges) || valid;
	},
	isValidAndCurrentRoute: function (route, url) {
		route.isRegex = $.lodash.isBoolean(route.isRegex) ? route.isRegex : false;
		if ($.lodash.isString(route.url)) {
			if (!route.isRegex && route.url[0] === '^') {
				route.url = route.url.slice(1);
			}
			if (!route.isRegex && route.url[0] === '/') {
				route.url = route.url.slice(1);
			}
			if (!route.isRegex && route.url[route.url.length - 1] === '$') {
				route.url = route.url.slice(0, -1);
			}
			if (!route.isRegex && route.url[route.url.length - 1] === '/') {
				route.url = route.url.slice(0, -1);
			}
		}
		if (!route.isRegex && url[url.length - 1] === '/') {
			url = url.slice(0, -1);
		}

		let __url__ = route.isRegex ? route.url : `^${route.url}$`;
		let routeFunction = this.getRouteFunction(route);
		return !$.lodash.isNil(route) &&
			$.lodash.isString(route.url) &&
			$.lodash.isString(route.method) &&
			$.lodash.isString(url) &&
			$.lodash.isFunction(routeFunction) &&
			url.match(new RegExp(__url__)) &&
			$.lodash.isString(route.category) &&
			!$.lodash.isEmpty(route.category) &&
			$.lodash.isString(route.object) &&
			!$.lodash.isEmpty(route.object);
	},
	getRouteContext: function (route) {
		let ctx;
		if (route && route.pathCtx && route.pathView && route.mainCtx) {
			let controller = route.mainCtx[route.pathCtx];  
			if (!controller()) {
				ctx = new (controller());
			} else {
				ctx = controller();
			}
		}else {
			ctx = route.ctx;
		}
		return ctx;
	},
	getRouteFunction: function (route) {
		let view;
		if (route && route.pathCtx && route.pathView && route.mainCtx) {
			let ctx = this.getRouteContext(route);
			view = ctx[route.pathView];
		}else {
			view = route.fn;
		}
		return view;
	},
	userIsNotLoggedIn: function () {
		return ($.lodash.isNil($.session.getUsername()) || ($.lodash.isString($.session.getUsername()) && $.lodash.isEmpty($.session.getUsername())));
	},
	addRetrocopability: function (route = {}) {
		route.ctx = this.getRouteContext(route);
		route.fn = this.getRouteFunction(route);
		return route;
	},
	getActualRoute: function (url) {
		let route;
		$.lodash.every(this.routes, (_route) => {
			if (this.isValidAndCurrentRoute(_route, url)) {
				route = _route;
				return false;
			}
			return true;
		});
		route = this.addRetrocopability(route);
		return route;
	},
	parseURL: function () {
		performance.parseURL = {
			searchingForTheRoute: new Date().getTime()
		};
		const _this = this;
		$.response.contentType = 'application/json';
		let url = $.request.queryPath;
		let response;
		if (url[0] === '/') {
			url = url.slice(1);
		}
		let endpoint = this.getActualRoute(url);
		performance.parseURL.searchingForTheRoute = (new Date().getTime() - performance.parseURL.searchingForTheRoute) / 1000;
		performance.parseURL.endpoint = {
			total: new Date().getTime(),
			isValid: true
		};
		$.requestInitialTime = new Date().getTime();
		if (!$.lodash.isNil($.request.parameters.get('timeForLog'))) {
			$.timeForLog = $.request.parameters.get('timeForLog');
		}
		if (!$.lodash.isNil(endpoint)) {
			endpoint.bypass = $.lodash.isBoolean(endpoint.bypass) ? endpoint.bypass : false;
			if ($.request.method !== $.net.http[_this.methodMap[$.lodash.toUpper(endpoint.method)]]) {
				performance.parseURL.endpoint.isValid = false;
				logger.log(Router.components[this.component], endpoint, [{
					object: '07',
					category: '00'
				}], 'E');
				$.response.status = $.net.http.NOT_YET_IMPLEMENTED;
				response = {
					messageCodes: $.messageCodes,
					results: {}
				};
			} else if (!_this.userHasPrivilegeToExecuteTheEndpoint(endpoint)) {
				performance.parseURL.endpoint.isValid = false;
				logger.log(Router.components[this.component], endpoint, [{
					object: '09',
					category: '00'
				}], 'E');
				$.response.status = $.net.http.FORBIDDEN;
				response = {
					messageCodes: $.messageCodes,
					results: {}
				};
			} else if (!endpoint.bypass && _this.userIsNotLoggedIn()) {
				performance.parseURL.endpoint.isValid = false;
				logger.log(Router.components[this.component], endpoint, [{
					object: '08',
					category: '00'
				}], 'E');
				$.response.status = $.net.http.UNAUTHORIZED;
				response = {
					messageCodes: $.messageCodes,
					results: {}
				};
			} else {
				// parse parameters and check if each param has the eval condition
				$.response.status = $.net.http.OK;
				let params = $.request.parameters;
				let parsedParameters = [];
				if (!($.lodash.isPlainObject(endpoint.params) && !$.lodash.isNil(endpoint.params) && !$.lodash.isEmpty(endpoint.params))) {
					endpoint.params = {
						object: 'json'
					};
				}
				if (endpoint.isRegex) {
					params = url.match(new RegExp(endpoint.url));
					params.shift();
					parsedParameters = params;
				} else {
					$.lodash.forEach(params, function (param) {
						// Needs to escape html and throw an error if the param has an eval.
						if ($.lodash.has(endpoint.params, param.name) && $.lodash.indexOf(resources.getTypes(), endpoint.params[param.name]) > -1) {
							parsedParameters.push(_this.parseParam(endpoint.params[param.name], param.value));
						}
					});
				}
				try {
					const context = $.lodash.isNil(endpoint.ctx) ? null : endpoint.ctx;
					response = endpoint.fn.apply(context, parsedParameters);
					let type = errorHandler.checkMessageCodes();
					if (type === 'E') {
						$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
					}
					if ($.lodash.isPlainObject(response) && !$.lodash.isNil(response)) {
						if (!$.lodash.isEmpty($.messageCodes)) {
							response = {
								results: response,
								messageCodes: $.messageCodes
							};
						}
					} else {
						if (!$.lodash.isEmpty($.messageCodes)) {
							response = {
								results: response,
								messageCodes: $.messageCodes
							};
						}
					}
				} catch (e) {
					performance.parseURL.endpoint.isValid = false;
					// Something went wrong during the execution of the endpoint
					$.logError(null, e);
					response = {
						results: endpoint['default'],
						messageCodes: $.messageCodes
					};
					$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
				}
			}
			performance.parseURL.endpoint.total = (new Date().getTime() - performance.parseURL.endpoint.total) / 1000;
			if ($.lodash.isBoolean(endpoint.simulate) && endpoint.simulate) {
				response = {
					routerResponse: response,
					performance: performance
				};
			}
			_this.parseResponse(response);
			return response;
		} else {
			logger.log(Router.components[this.component], {
				invalid: true,
				url: url
			}, [{
				object: '07',
				category: '00'
			}], 'E');
			$.response.status = $.net.http.NOT_FOUND;
			return response;
		}

	}
};

this.Router = Router;