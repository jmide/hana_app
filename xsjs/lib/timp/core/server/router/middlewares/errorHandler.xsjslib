this.checkMessageCodes = function() {
    let type = 'S';
    $.lodash.every($.messageCodes, function(message) {
        if(message.type === 'E') {
            type = message.type;
            return false;
        }
        return true;
    });
    return type;
};