$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server.models.tables', 'log');
const logModel = $.timp.core.server.models.tables.log.logModel;

const phrase = $.schema + '_' + $.viewSchema;

function cleanMessageObject(message) {
	delete message['@query'];
	delete message['@values'];
}

function mapTrace(prefix, be, object, type, category) {
	return function(message) {
		category = message.category || category;
		object = message.object || object;

		delete message.category;
		delete message.object;

		message = message instanceof Error ? $.parseError(message) : message;
		cleanMessageObject(message);
		message.internationalizationCode = prefix + be + object + type + category;

		return message;
	};
}

/*
 * Log Middleware that run after the call of an endpoint
 *
 * @param {number} componentId - Component to which the endpoint belongs
 * @param {Object} route - Route object
 * @param {string} route.method - HTTP Method (POST, GET, PUT, PATCH, DELETE)
 * @param {string} route.category - Category code of the message code (length 2)
 * @param {string} route.object - Object code from Objects dictionary (length 2)
 * @param {string} [route.sequence] - Sequence number of the message code (length 3)
 * @param {string} [route.prefix] - Prefix of the message code
 * @param {boolean} [route.invalid] - Flag sent when is an invalid route
 * @param {string} [route.url] - Route url
 * @param {Object} [logData] - Response that the endpoint will return
 * @param {string} type - Status of the response (Successful(S) or Error(E))
 * @param {Object} [params] - Parameters
 */
this.log = function(componentId, route, logData, type, params) {
	if (route.bypass) {
		return {
			results: 'Not logging'
		};
	}

	const defaultCategoryMap = {
		POST: '0000',
		GET: '1000',
		PUT: '2000',
		PATCH: '2000',
		DELETE: '3000'
	};
	let origin = 0;
	let defaultCategory = defaultCategoryMap[route.method || 'POST'];
	let prefix = route.prefix || 'CORE';
	let category = route.category || defaultCategory;
	let object = route.object || '00';
	let _type = type === 'E' ? 1 : 0;
	let defaultLogMessageCode = prefix + origin + object + _type + category;
	let logObject = {
		componentId: componentId,
		userId: $.getUserId(),
		// date: new Date(),
		type: type
	};

	if ($.lodash.isNil(logData)) {
		logData = [];
	} else {
		logData = $.lodash.isArray(logData) ? $.lodash.cloneDeep(logData) : $.lodash.cloneDeep([logData]);
	}

	let mainMessage;
	let fullDataToLog;

	if (route.invalid) {
		_type = 1;
		category = '00';
		object = '05';

		mainMessage = JSON.stringify({
			internationalizationCode: prefix + origin + object + _type + category,
			url: route.url,
			stack: [{
				internationalizationCode: prefix + origin + object + _type + category
			}]
		});
	} else {
		logObject.internationalizationCode = defaultLogMessageCode;

		if (type === 'S') {
			if ($.lodash.isEmpty(logData)) {
				logData.push({
					category: category,
					object: object
				});
			}
			mainMessage = $.lodash.cloneDeep(logData[0]);
			prefix = mainMessage.prefix || prefix;
			origin = mainMessage.origin || origin;
			_type = 0;
			object = mainMessage.object || object;
			category = mainMessage.category || category;

			mainMessage.data = mainMessage.json;
			delete mainMessage.json;
			fullDataToLog = $.lodash.cloneDeep(logData);

			mainMessage.stack = $.lodash.map(logData, mapTrace(prefix, origin, object, _type, category));
		} else if (!$.lodash.isNil(logData)) {
			mainMessage = $.lodash.isNil(logData) || $.lodash.isEmpty(logData) ? {
				stack: []
			} : $.lodash.cloneDeep(logData[0]);
			prefix = mainMessage.prefix || prefix;
			origin = mainMessage.origin || origin;
			_type = 1;
			object = mainMessage.object || object;
			category = mainMessage.category || category;

			fullDataToLog = $.lodash.cloneDeep(mainMessage);
			mainMessage.stack = $.lodash.map(mainMessage.stack, mapTrace(prefix, origin, object, _type, category));
		} else {
			prefix = 'CORE';
			_type = 1;
			category = '00';
			object = '05';
			mainMessage = {
				internationalizationCode: prefix + origin + object + _type + category
			};
			let stack = [$.lodash.cloneDeep(mainMessage)];
			mainMessage.stack = stack;
		}
		delete mainMessage.object;
		delete mainMessage.category;
		delete mainMessage.origin;
		delete mainMessage.type;
		delete mainMessage.prefix;

		mainMessage.internationalizationCode = prefix + origin + object + _type + category;
		if (!($.lodash.isNil(mainMessage.objectId) || isNaN(mainMessage.objectId))) {
			mainMessage.objectId = parseInt(mainMessage.objectId, 10);
		} else {
			mainMessage.objectId = null;
		}

		if (type === 'E' && $.lodash.isEmpty($.messageCodes)) {
			$.messageCodes.push({
				code: mainMessage.internationalizationCode,
				type: 'E',
				errorInfo: mainMessage.stack,
				sendToUI: true
			});
		}

		logObject.type = type;
		logObject.internationalizationCode = mainMessage.internationalizationCode;
		logObject.objectId = mainMessage.objectId;
		logObject.trace = mainMessage;
		logObject.trace.params = params;
		logObject.trace = JSON.stringify(logObject.trace);
		logObject.encryptedTrace = $.lodash.isNil(fullDataToLog) ? null : $.encrypt(JSON.stringify(fullDataToLog), phrase);
	}

	let isGetNewCategory = category.length === 4 && category.slice(0, -1) === '100';
	if (category === '99' || isGetNewCategory) {
		return {
			results: 'GET is not logged'
		};
	}

	if (logModel.exists()) {
		let logResponse;
		if (params && params.batchCreate) {
			let tempVal = logModel.batchCreate([logObject], {
				getInstances: true,
				select: params && params.select ? params.select : []
			});

			if (tempVal.results.created) {
				logResponse = {
					errors: tempVal.errors,
					results: {
						created: tempVal.results.created,
						instance: tempVal.results.instances[tempVal.results.instances.length - 1]
					}
				};
			}
		} else {
			logResponse = logModel.create(logObject, {
				getInstances: false
			});
		}
		return logResponse;
	} else {
		return {
			results: 'CORE::LOG doesn\'t exist'
		};
	}
};