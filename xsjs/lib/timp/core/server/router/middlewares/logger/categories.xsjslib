this.value = {
	"0000": "create",
	"1000": "read",
	"2000": "update",
	"3000": "delete",
	"4000": "customEvent",
	"5000": "internalError",
	"6000": "paramsError",
	"7000": "routeNotFound",
	"8000": "authenticationError",
	"9000": "insufficientPrivileges"
};