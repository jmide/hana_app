$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

$.import('timp.core.server.orm', 'tableServices');
const services = $.timp.core.server.orm.tableServices;

/**
 * Creates a new BaseModel.
 * @class
 * @param {Object} options - BaseModel options for the creation of the model (Check the BaseModel documentation for examples).
 * @param {boolean} useComponentAttribute - if the table is from a component, it adds the prefix COMPONENT:: to the identity
 * @example
 * // true
 */
function BaseModel(options, useComponentAttribute, dontAddCollectionToCache) {
    options = lodash.cloneDeep(options);
	this.init(options, useComponentAttribute, dontAddCollectionToCache);
}

BaseModel.prototype = {
	init: function(definition, useComponentAttribute, dontAddCollectionToCache) {
	
	    
		if (lodash.isNil(BaseModel.models)) {
			BaseModel.models = {};
		}
		
		let status = {
		    '@collection': definition.identity,
			errors: [],
			warnings: [],
			isValid: true
		};
		useComponentAttribute = lodash.isBoolean(useComponentAttribute) ? useComponentAttribute : true;
		dontAddCollectionToCache = lodash.isBoolean(dontAddCollectionToCache) ? dontAddCollectionToCache : false;
		status = resources.hasPropertyAndIsValidString(status, definition, 'name', false);
		status = resources.hasPropertyAndIsValidString(status, definition, 'identity', true);
		if (useComponentAttribute) {
			status = resources.hasPropertyAndIsValidString(status, definition, 'component', true);
		}
		status = resources.hasPropertyAndIsValidString(status, definition, 'schema', true);
		status = resources.hasPropertyAndIsValidString(status, definition, 'type', true);

		if (lodash.indexOf(['table', 'view'], definition.type) === -1) {
			status.errors.push(resources.generateError('Invalid Type', 'Not a valid option for model\'s type(table or view)'));
			status.isValid = false;
		}
		if (!lodash.has(definition, 'fields')) {
			status.errors.push(resources.generateError('No fields', 'The model needs the property fields(object)'));
			status.isValid = false;
		} else if (!(!lodash.isNil(definition.fields) && lodash.isPlainObject(definition.fields) && !lodash.isEmpty(definition.fields))) {
			status.errors.push(resources.generateError('Invalid fields', 'The model\'s fields need to be a non empty object'));
			status.isValid = false;
		} else {
			if (lodash.isString(definition.defaultFields)) {
				definition.defaultFields = [definition.defaultFields];
			} 
			
			if (lodash.isArray(definition.defaultFields) && !lodash.isEmpty(definition.defaultFields)) {
			    lodash.forEach(definition.defaultFields, function(defaultField) {
			        lodash.assign(definition.fields, resources.getDefaultFields(defaultField));
			    });
			}
			if (useComponentAttribute) {
				definition.identity = definition.component + '::' + definition.identity;
			}
			let response = resources.analyzeFields(definition, status, BaseModel.models);
			status = response.retVal;
			definition.fields = response.sanitizedFields;
			definition.hanaMap = response.hanaMap;
			definition.primaryKeys = response.primaryKeys;
			definition.autoIncrementFields = response.autoIncrementFields;
			definition.onUpdateFieldsDefault = response.onUpdateFieldsDefault;
			definition.defaultValuesWithSQLStatements = response.defaultValuesWithSQLStatements;
			definition.inputParameters = definition.type === 'view' ? response.inputParameters : {};
		}
		
		this.warnings = status.warnings;
		this.errors = status.errors;
		this.isValid = status.isValid;
		if (status.isValid) {
			definition.tableName = '"' + definition.schema + '"."' + definition.identity + '"';
			this.definition = definition;
			if (!dontAddCollectionToCache && !lodash.isNil(definition)) {
			    BaseModel.models[this.definition.identity] = lodash.cloneDeep(definition);   
			}
		} else {
			throw status;
		}
	},
	find: function(options) {
		let retVal = {
			errors: [],
			results: []
		};
		options = lodash.isNil(options) ? {} : options;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (lodash.isNil(options)) {
				retVal.errors.push(resources.generateError('Invalid options', 'options is not an object.'));
				throw retVal;
			}
			let response;
            // let _retVal = resources._find(this.getIdentity(), options, lodash.cloneDeep(BaseModel.models));
            let _retVal = resources._find(this.getIdentity(), options, JSON.parse(JSON.stringify(BaseModel.models)));
			if (lodash.isBoolean(options.simulate) && options.simulate) {
				retVal.results.push(_retVal.findQuery);
				return retVal;
			} else if (lodash.isBoolean(options.raw) && options.raw) {
			    response = connector.execute(_retVal.findQuery, _retVal.hanaMap, false, true);
    			if (!lodash.isEmpty(response.errors)) {
    				retVal.errors = response.errors;
    				throw retVal;
    			}
    			return response;
			}
			if (lodash.isEmpty(_retVal.aliasMapping)) {
			    response = connector.execute(_retVal.findQuery, _retVal.hanaMap);
    			if (!lodash.isEmpty(response.errors)) {
    				retVal.errors = response.errors;
    				throw retVal;
    			}
    			return response;
			} else {
			    return resources.parseFindData(_retVal.findQuery, _retVal.hanaMap, _retVal.aliasMapping);
			}
		} catch (e) {
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	delete: function(options, returnDeletedInstances) {
		let retVal = {
			errors: [],
			results: {
				deleted: true,
				instances: []
			}
		};
		returnDeletedInstances = lodash.isBoolean(returnDeletedInstances) ? returnDeletedInstances : false;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (this.definition.type !== 'table') {
				retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
				throw retVal;
			} else if (lodash.isNil(options)) {
				retVal.errors.push(resources.generateError('Invalid options', 'The options is not an object.'));
				throw retVal;
			} else if (!lodash.isArray(options.where)) {
				retVal.errors.push(resources.generateError('Invalid options.where', 'The options.where is not a valid array.'));
				throw retVal;
			}
			let parsedWhere = resources.parseWhere(this.getIdentity(), lodash.cloneDeep(BaseModel.models), options.where, null);
			if (lodash.isNil(parsedWhere)) {
				retVal.errors.push(resources.generateError('Invalid conditions', 'Something went wrong during the parsing of the conditions.'));
				throw retVal;
			} else {
				let deletedInstances = [];
				if (returnDeletedInstances) {
                    deletedInstances = this.find(options);
    				if (!lodash.isEmpty(deletedInstances.errors)) {
    					retVal.errors = deletedInstances.errors;
    					throw retVal;
    				}
    				deletedInstances = deletedInstances.results;
				}
				let deleteQuery = 'DELETE FROM ' + this.definition.tableName + ' WHERE ' + parsedWhere;
				let response = connector.execute(deleteQuery, null, true);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
					throw retVal;
				}
				retVal.results.instances = deletedInstances;
				return retVal;
			}
		} catch (e) {
			retVal.results.deleted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	update: function(instance, options, returnUpdatedInstances) {
		let retVal = {
			errors: [],
			results: {
				updated: true,
				instances: []
			}
		};
		returnUpdatedInstances = lodash.isBoolean(returnUpdatedInstances) ? returnUpdatedInstances : false;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (this.definition.type !== 'table') {
				retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
				throw retVal;
			} else if (lodash.isNil(options)) {
				retVal.errors.push(resources.generateError('Invalid options', 'The options is not an object.'));
				throw retVal;
			} else if (!lodash.isArray(options.where)) {
				retVal.errors.push(resources.generateError('Invalid options.where', 'The options.where is not a valid array.'));
				throw retVal;
			} else if (lodash.isNil(instance)) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not an object.'));
				throw retVal;
			} else if (lodash.isEmpty(instance)) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not a valid object.'));
				throw retVal;
			}
			let parsedWhere = resources.parseWhere(this.getIdentity(), lodash.cloneDeep(BaseModel.models), options.where, null);
			if (lodash.isNil(parsedWhere)) {
				retVal.errors.push(resources.generateError('Invalid conditions', 'Something went wrong during the parsing of the conditions.'));
				throw retVal;
			} else {
				const _this_ = this;
				let updateQuery = 'UPDATE ' + _this_.definition.tableName + ' ';
				let columns = [];
				let bufferColumns = [];
				lodash.forEach(instance, function(value, attributeName) {
					if (lodash.has(_this_.definition.fields, attributeName)) {
					    if (_this_.definition.fields[attributeName].type === 'file') {
					        columns.push(
    							'"' + _this_.definition.fields[attributeName].columnName + '" = ?'
    						);
    						bufferColumns.push(value);
					    } else {
					        if (!lodash.isNil(_this_.definition.fields[attributeName].$translate) && lodash.isPlainObject(_this_.definition.fields[attributeName].$translate)) {
    					        value = resources.getTranslateValue(_this_.definition.fields[attributeName], value);
    					    }
					        columns.push(
    							'"' + _this_.definition.fields[attributeName].columnName + '" = ' + resources.getHanaValue(_this_.definition.fields[attributeName], value)
    						);
					    }
					}
				});
				lodash.forEach(_this_.definition.onUpdateFieldsDefault, function(field) {
					if (!lodash.has(instance, field)) {
						columns.push(
							'"' + _this_.definition.fields[field].columnName + '" = ' + resources.getHanaValue(_this_.definition.fields[field].type, _this_.definition
								.fields[field].onUpdate)
						);
					}
				});
				if (!lodash.isEmpty(columns)) {
					updateQuery += ' SET ' + lodash.join(columns, ',') + ' WHERE ' + parsedWhere;

				} else {
					retVal.errors.push(resources.generateError('Invalid Columns', 'Invalid values from the instance.'));
					throw retVal;
				}
				let response = connector.executeUpdate(updateQuery, [bufferColumns]);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
					throw retVal;
				}
				let updatedInstances = [];
				if (returnUpdatedInstances) {
				    updatedInstances = _this_.find(options);
    				if (!lodash.isEmpty(updatedInstances.errors)) {
    					retVal.errors = updatedInstances.errors;
    					throw retVal;
    				}
    				updatedInstances = updatedInstances.results;
				}
				retVal.results.instances = updatedInstances;
				return retVal;
			}
		} catch (e) {
			retVal.results.updated = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	batchCreate: function(instances, options) {
		const _this_ = this;
		let retVal = {
			errors: [],
			results: {
				created: true,
				instances: []
			}
		};
		options = (lodash.isPlainObject(options) && !lodash.isNil(options)) ? options : {};
		
		options.ignoreSequenceFields = lodash.isArray(options.ignoreSequenceFields) ? options.ignoreSequenceFields : [];
		options.ignoreSQLStatementDefaults = lodash.isArray(options.ignoreSQLStatementDefaults) ? options.ignoreSQLStatementDefaults : [];
		options.getInstances = lodash.isBoolean(options.getInstances) ? options.getInstances : false;
		options.select = lodash.isArray(options.select) ? options.select : [];
		try {
			let response = resources.batchQuery(_this_.getIdentity(), BaseModel.models, instances, options, 'create');
			if (!lodash.isEmpty(response.errors)) {
			    retVal.errors = response.errors;
			    throw retVal;
			}
			let findOptions = null;
			if (options.getInstances) {
			    findOptions = response.whereFields;
			}
			response = connector.executeUpdate(response.query, response.values);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = response.errors;
				throw retVal;
			}
			if (options.getInstances) {
			    findOptions.select = options.select;
			    response = _this_.find(findOptions);
			    if (!lodash.isEmpty(response.errors)) {
    				retVal.errors = response.errors;
    				throw retVal;
    			}
    			retVal.results.instances = response.results;
			}
			return retVal;
		} catch (e) {
			retVal.results.created = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	create: function(instance, options = {}) {
		const _this_ = this;
		let retVal = {
			errors: [],
			results: {
				created: true,
				instance: null
			}
		};
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (this.definition.type !== 'table') {
				retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
				throw retVal;
			} else if (lodash.isNil(instance)) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not an object.'));
				throw retVal;
			} else if (lodash.isEmpty(instance)) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not a valid object.'));
				throw retVal;
			}
            let response = _this_.batchCreate([instance], {
                getInstances: options.getInstances != undefined ? options.getInstances : true
            });
            if (!lodash.isEmpty(response.errors)) {
				retVal.errors = response.errors;
				throw retVal;
			}
			retVal.results.instance = response.results.instances[response.results.instances.length - 1];
			return retVal;
		} catch (e) {
			retVal.results.created = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	upsert: function(instance, options, returnUpsertedInstances){
	    options = options || { where: [] };
	    let retVal = {
	        errors: [],
	        results: {
	            upserted: true,
	            instances: []
	        }
	    };
	    const _this = this;
	    returnUpsertedInstances = lodash.isBoolean(returnUpsertedInstances) ? returnUpsertedInstances : false;
	    try {
	        if (!lodash.has(BaseModel.models, _this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (this.definition.type !== 'table') {
				retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
				throw retVal;
			} else if (!(lodash.isPlainObject(instance) && !lodash.isNil(instance))) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not an object.'));
				throw retVal;
			} else if (lodash.isEmpty(instance)) {
				retVal.errors.push(resources.generateError('Invalid instance', 'The instance is not a valid object.'));
				throw retVal;
			}
			const ignoreSequences = !lodash.isEmpty(options.where);
			let sequencesColumns = [];
			if (!ignoreSequences) {
			   lodash.forEach(_this.definition.autoIncrementFields, function(attributeName) {
				if (!lodash.isNil(_this.definition.fields[attributeName].autoIncrement) && _this.definition.fields[attributeName].autoIncrement &&
					lodash.isNil(instance[attributeName])) {
					sequencesColumns.push(
						'"' + _this.definition.schema + '"."' + _this.definition.identity + '::' + _this.definition.fields[attributeName].columnName +
						'".nextVal AS "' + _this.definition.fields[attributeName].columnName + '"'
					);
				}
			}); 
			}
			let sequenceObject = {};
			let response;
			if (!ignoreSequences && !lodash.isEmpty(sequencesColumns)) {
				let sequenceQuery = 'SELECT ' + lodash.join(sequencesColumns, ',') + ' FROM DUMMY';
				response = connector.execute(sequenceQuery, null);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = response.errors;
					throw retVal;
				}
				lodash.forEach(response.results[0], function(value, hanaKey) {
					sequenceObject[_this.definition.hanaMap[hanaKey].name] = Number(value.toString());
				});
				lodash.assign(instance, sequenceObject);
			}
			let defaultValuesWithSQLStatementsObject = {};
			let defaultValuesWithSQLStatementsColumns = [];
			lodash.forEach(_this.definition.defaultValuesWithSQLStatements, function(attributeName) {
			    if (!lodash.has(instance, attributeName) && lodash.has(resources.getSQLStatementsConstants(), _this.definition.fields[attributeName].default)) {
			        defaultValuesWithSQLStatementsColumns.push('(' + resources.getSQLStatementsConstants()[_this.definition.fields[attributeName].default] + ') AS "' 
			                                                       + _this.definition.fields[attributeName].columnName + '"');
			    }
			});
			if (!lodash.isEmpty(defaultValuesWithSQLStatementsColumns)) {
			    let defaultValuesWithSQLStatementsQuery = 'SELECT ' + lodash.join(defaultValuesWithSQLStatementsColumns, ',') + ' FROM DUMMY';
				let defaultValuesWithSQLStatementsResponse = connector.execute(defaultValuesWithSQLStatementsQuery, null);
				if (!lodash.isEmpty(defaultValuesWithSQLStatementsResponse.errors)) {
					retVal.errors = response.errors;
					throw retVal;
				}
				lodash.forEach(defaultValuesWithSQLStatementsResponse.results[0], function(value, hanaKey) {
					defaultValuesWithSQLStatementsObject[_this.definition.hanaMap[hanaKey].name] = value;
				});
				lodash.assign(instance, defaultValuesWithSQLStatementsObject);
			}
			const ignoreTheseTypes = ['text', 'shorttext', 'json', 'file'];
			if (lodash.isEmpty(options.where)) {
			    lodash.forEach(instance, function(value, key) {
			        if (!lodash.isNil(_this.getDefinition().fields[key]) && lodash.indexOf(ignoreTheseTypes, _this.getDefinition().fields[key].type) === -1 && value !== null) {
			            options.where.push({
    			            field: key,
    			            operator: '$eq',
    			            value: value
    			        });   
			        }
			    });
			}
			let upsertQuery = 'UPSERT ' + _this.getDefinition().tableName + ' ';
			let parsedWhere = resources.parseWhere(this.getIdentity(), lodash.cloneDeep(BaseModel.models), lodash.cloneDeep(options.where), null);
			if (lodash.isNil(parsedWhere)) {
				retVal.errors.push(resources.generateError('Invalid conditions', 'Something went wrong during the parsing of the conditions.'));
				throw retVal;
			}
			let columns = [],
				hanaValues = [],
				bufferColumns = [];
			lodash.forEach(instance, function(value, attributeName) {
				if (lodash.has(_this.definition.fields, attributeName)) {
					columns.push('"' + _this.definition.fields[attributeName].columnName + '"');
					if (_this.definition.fields[attributeName].type === 'file') {
					    hanaValues.push('?');
					    bufferColumns.push(value);
					} else {
					    if (!lodash.isNil(_this.definition.fields[attributeName].$translate) && lodash.isPlainObject(_this.definition.fields[attributeName].$translate)) {
					        value = resources.getTranslateValue(_this.definition.fields[attributeName], value);
					    }
					    hanaValues.push(resources.getHanaValue(_this.definition.fields[attributeName].type, value));   
					}
				}
			});
			if (ignoreSequences) {
			    lodash.forEach(_this.definition.onUpdateFieldsDefault, function(field) {
					if (!lodash.has(instance, field)) {
					    columns.push('"' + _this.definition.fields[field].columnName + '"');
						hanaValues.push(
							resources.getHanaValue(_this.definition.fields[field].type, _this.definition.fields[field].onUpdate)
						);
					}
				});
			}
			upsertQuery += '(' + lodash.join(columns, ',') + ') VALUES(' + lodash.join(hanaValues, ',') + ') WHERE ' + parsedWhere;
			let upsertResponse = connector.executeUpdate(upsertQuery, [bufferColumns]);
			if (!lodash.isEmpty(upsertResponse.errors)) {
				retVal.errors = upsertResponse.errors;
				throw retVal;
			}
			upsertResponse = [];
			if (returnUpsertedInstances) {
			    upsertResponse = _this.find(options);
    		    if (!lodash.isEmpty(upsertResponse.errors)) {
    				retVal.errors = upsertResponse.errors;
    				throw retVal;
    			}
    			upsertResponse = upsertResponse.results;
			}
			retVal.results.instances = upsertResponse;
			return retVal;
	    } catch(e) {
	        retVal.results.upserted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
	    }
	},
	batchUpsert: function(instances, options) {
		const _this_ = this;
		let retVal = {
			errors: [],
			results: {
				upserted: true,
				instances: []
			}
		};
		options = (lodash.isPlainObject(options) && !lodash.isNil(options)) ? options : {};
		
		options.ignoreSequenceFields = lodash.isArray(options.ignoreSequenceFields) ? options.ignoreSequenceFields : [];
		options.ignoreSQLStatementDefaults = lodash.isArray(options.ignoreSQLStatementDefaults) ? options.ignoreSQLStatementDefaults : [];
		options.getInstances = lodash.isBoolean(options.getInstances) ? options.getInstances : false;
		try {
			let response = resources.batchQuery(_this_.getIdentity(), BaseModel.models, instances, options, 'upsert');
			if (!lodash.isEmpty(response.errors)) {
			    retVal.errors = response.errors;
			    throw retVal;
			}
			let findOptions = null;
			if (options.getInstances) {
			    findOptions = lodash.cloneDeep(response.whereFields);
			}
			response = connector.executeUpdate(response.query, response.values);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = response.errors;
				throw retVal;
			}
			if (options.getInstances) {
			    response = _this_.find(findOptions);
			    if (!lodash.isEmpty(response.errors)) {
    				retVal.errors = response.errors;
    				throw retVal;
    			}
    			retVal.results.instances = response.results;
			}
			return retVal;
		} catch (e) {
			retVal.results.upserted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	addConstraint: function(attributeName, options) {
		let retVal = {
			errors: [],
			results: {
				created: true,
				constraints: []
			}
		};
		const _this_ = this;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") must be a non-empty string.'));
				throw retVal;
			} else if (!(lodash.has(_this_.getDefinition().fields, attributeName) && lodash.isPlainObject(_this_.getDefinition().fields[
					attributeName]) &&
				!lodash.isNil(_this_.getDefinition().fields[attributeName]))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") isn\'t in the collection\'s fields.'));
				throw retVal;
			} else if (!(lodash.isPlainObject(options) && !lodash.isNil(options) && (lodash.isBoolean(options.unique) || (lodash.isPlainObject(
				options.foreignKey) && !lodash.isEmpty(options.foreignKey))))) {
				retVal.errors.push(resources.generateError('Invalid options',
					'Options must be an object with the property unique(boolean) or foreignKey(boolean)'));
				throw retVal;
			}
			const field = _this_.getDefinition().fields[attributeName];
			let isUnique = (lodash.isBoolean(field.unique) && field.unique) || false,
				isFk = ((lodash.isBoolean(field.isonetoone) && field.isonetoone) ||
					(lodash.isBoolean(field.isonetomany) && field.isonetomany)) || false;
			if ((lodash.isBoolean(options.unique) && options.unique) && !(lodash.isPlainObject(options.foreignKey) && !lodash.isNil(options.foreignKey) &&
				(lodash.isBoolean(options.foreignKey.isonetoone) && options.foreignKey.isonetoone))) {
				const uniqueResponse = services.addUniqueConstraint(_this_.getDefinition().schema, _this_.getDefinition().identity, attributeName);
				if (lodash.isArray(uniqueResponse.errors) && !lodash.isEmpty(uniqueResponse.errors)) {
					retVal.errors = lodash.cloneDeep(uniqueResponse.errors);
					throw retVal;
				} else if (lodash.isBoolean(uniqueResponse.results.created) && uniqueResponse.results.created && lodash.isPlainObject(uniqueResponse.results
					.constraint) && !lodash.isEmpty(uniqueResponse.results.constraint)) {
					isUnique = true;
					retVal.results.constraints.push(uniqueResponse.results.constraint);
				}
			}

			if (lodash.isPlainObject(options.foreignKey) && !lodash.isNil(options.foreignKey) && !lodash.isEmpty(options.foreignKey)) {
				const fkResponse = services.addForeignKeyConstraint(_this_.getDefinition().schema, _this_.getDefinition().identity, attributeName,
					options.foreignKey);
				if (lodash.isArray(fkResponse.errors) && !lodash.isEmpty(fkResponse.errors)) {
					retVal.errors = lodash.cloneDeep(fkResponse.errors);
					throw retVal;
				} else if (lodash.isBoolean(fkResponse.results.created) && fkResponse.results.created && lodash.isArray(fkResponse.results.constraints) &&
					!lodash.isEmpty(fkResponse.results.constraints)) {
					retVal.results.constraints = lodash.concat(retVal.results.constraints, fkResponse.results.constraints);
					isFk = true;
					isUnique = (isUnique || lodash.isBoolean(options.foreignKey.isonetoone) && options.foreignKey.isonetoone);
				}
			}
			if (isUnique && !isFk) {
				_this_.definition.fields[attributeName].unique = true;
			} else if (isFk) {
				if (isUnique) {
					delete _this_.definition.fields[attributeName].unique;
					delete _this_.definition.fields[attributeName].isonetomany;
					_this_.definition.fields[attributeName].isonetoone = true;
				} else {
					delete _this_.definition.fields[attributeName].isonetoone;
					_this_.definition.fields[attributeName].isonetomany = true;
				}
				if (!(lodash.isPlainObject(_this_.definition.fields[attributeName].constraints) && !lodash.isEmpty(_this_.definition.fields[
						attributeName]
					.constraints))) {
					_this_.definition.fields[attributeName].constraints = {
						onUpdate: options.foreignKey.onUpdate,
						onDelete: options.foreignKey.onDelete
					};
				}
			}
			BaseModel.models[_this_.definition.identity] = lodash.cloneDeep(_this_.definition);
			return retVal;
		} catch (e) {
			retVal.results.created = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	removeConstraint: function(attributeName, options) {
		let retVal = {
			errors: [],
			results: {
				deleted: true,
				constraints: []
			}
		};
		const _this_ = this;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") must be a non-empty string.'));
				throw retVal;
			} else if (!(lodash.has(_this_.getDefinition().fields, attributeName) && lodash.isPlainObject(_this_.getDefinition().fields[
					attributeName]) &&
				!lodash.isNil(_this_.getDefinition().fields[attributeName]))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") isn\'t in the collection\'s fields.'));
				throw retVal;
			} else if (!(lodash.isPlainObject(options) && !lodash.isNil(options) && (lodash.isBoolean(options.unique) || lodash.isBoolean(options.foreignKey)))) {
				retVal.errors.push(resources.generateError('Invalid options',
					'Options must be an object with the property unique(boolean) or foreignKey(boolean)'));
				throw retVal;
			}

			let queries = {
				constraints: []
			};
			let wasUnique = false,
				wasFk = false;
			if ((lodash.isBoolean(options.unique) && options.unique) || (lodash.isBoolean(options.foreignKey) && options.foreignKey)) {
				const uniqueQuery = 'SELECT "CONSTRAINT_NAME" AS "name" FROM "PUBLIC"."CONSTRAINTS" WHERE "SCHEMA_NAME" = \'' +
					_this_.getDefinition().schema + '\' AND "TABLE_NAME" = \'' + _this_.getIdentity() + '\' AND "COLUMN_NAME" = \'' +
					attributeName + '\' AND "IS_UNIQUE_KEY" = \'TRUE\'';
				const uniqueResponse = connector.execute(uniqueQuery, null);
				if (lodash.isArray(uniqueResponse.errors) && !lodash.isEmpty(uniqueResponse.errors)) {
					retVal.errors = lodash.cloneDeep(uniqueResponse.errors);
					throw retVal;
				} else if (lodash.isArray(uniqueResponse.results) && !lodash.isEmpty(uniqueResponse.results)) {
					wasUnique = true;
					queries.constraints = lodash.map(uniqueResponse.results, function(constraint) {
						return constraint.name;
					});
				}
			}
			if (lodash.isBoolean(options.foreignKey) && options.foreignKey) {
				const associatedModel = lodash.cloneDeep(BaseModel.models[_this_.getDefinition().fields[attributeName].type]);
				if (lodash.isNil(associatedModel)) {
					retVal.errors.push(resources.generateError('Invalid model', 'Model("' + _this_.getDefinition().fields[attributeName].type +
						'") isn\'t defined.'));
					throw retVal;
				}
				const fkQuery = 'SELECT "CONSTRAINT_NAME" AS "name" FROM "PUBLIC"."REFERENTIAL_CONSTRAINTS" WHERE "SCHEMA_NAME" = \'' +
					_this_.getDefinition().schema + '\' AND "TABLE_NAME" = \'' + _this_.getIdentity() + '\' AND "COLUMN_NAME" = \'' +
					attributeName + '\' AND "REFERENCED_SCHEMA_NAME" = \'' + associatedModel.schema + '\' AND "REFERENCED_TABLE_NAME" = \'' +
					associatedModel.identity + '\' AND "REFERENCED_COLUMN_NAME" = \'' +
					associatedModel.fields[associatedModel.primaryKeys[0]].columnName + '\'';
				const fkResponse = connector.execute(fkQuery, null);
				if (lodash.isArray(fkResponse.errors) && !lodash.isEmpty(fkResponse.errors)) {
					retVal.errors = lodash.cloneDeep(fkResponse.errors);
					throw retVal;
				} else if (lodash.isArray(fkResponse.results) && !lodash.isEmpty(fkResponse.results)) {
					wasFk = true;
					lodash.forEach(fkResponse.results, function(constraint) {
						queries.constraints.push(constraint.name);
					});
				}
			}
			if (!lodash.isEmpty(queries.constraints)) {
				let response;
				lodash.forEach(queries.constraints, function(name) {
					response = services.dropConstraint(_this_.getDefinition().schema, _this_.getIdentity(), name);
					if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.cloneDeep(response.errors);
						throw retVal;
					} else if (lodash.isPlainObject(response.results) && !lodash.isNil(response.results) && !lodash.isEmpty(response.results) &&
						response.results.deleted) {
						retVal.results.constraints.push(response.results.constraint);
					}
				});
				if (!lodash.isEmpty(retVal.errors)) {
					throw retVal;
				}
				if (wasUnique) {
					delete _this_.definition.fields[attributeName].unique;
					if (!wasFk) {
						_this_.definition.fields[attributeName].isonetomany = true;
						delete _this_.definition.fields[attributeName].isonetoone;
					}
				}
				if (wasFk) {
					if (wasUnique) {
						delete _this_.definition.fields[attributeName].isonetoone;
					} else {
						delete _this_.definition.fields[attributeName].isonetomany;
					}
					delete _this_.definition.fields[attributeName].constraints;
				}
				BaseModel.models[_this_.definition.identity] = lodash.cloneDeep(_this_.definition);
			} else {
				retVal.errors.push(resources.generateError('No constraints', 'No constraints were found for the column.'));
				throw retVal;
			}
			return retVal;

		} catch (e) {
			retVal.results.deleted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	addIndex: function(attributeName, options) {
		let retVal = {
			errors: [],
			results: {
				created: true,
				index: {}
			}
		};
		const _this_ = this;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") must be a non-empty string.'));
				throw retVal;
			} else if (!(lodash.has(_this_.getDefinition().fields, attributeName) && lodash.isPlainObject(_this_.getDefinition().fields[
					attributeName]) &&
				!lodash.isNil(_this_.getDefinition().fields[attributeName]))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") isn\'t in the collection\'s fields.'));
				throw retVal;
			}
			if (!(lodash.isPlainObject(options) && !lodash.isEmpty(options))) {
				options = {};
			}
			if (!(lodash.isString(options.type) && !lodash.isNil(resources.getValidIndexTypes()[options.type]))) {
				options.type = 'default';
			}
			if (!(lodash.isString(options.globalIndexOrder) && lodash.indexOf(['ASC', 'DESC'], lodash.toUpper(options.globalIndexOrder)))) {
				options.globalIndexOrder = 'ASC';
			}
			options.columns = [{
				name: _this_.getDefinition().fields[attributeName].columnName
			}];
			const response = services.createIndex(_this_.getSchema(), _this_.getIdentity(), options);
			if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.cloneDeep(response.errors);
				throw retVal;
			} else if (lodash.isPlainObject(response.results) && !lodash.isEmpty(response.results)) {
				retVal.results.index = lodash.cloneDeep(response.results.index);
			}
			_this_.definition.fields[attributeName].index = {
				type: options.type,
				globalIndexOrder: options.globalIndexOrder
			};
			BaseModel.models[_this_.definition.identity] = lodash.cloneDeep(_this_.definition);
			return retVal;
		} catch (e) {
			retVal.results.created = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	removeIndex: function(attributeName) {
		let retVal = {
			errors: [],
			results: {
				deleted: true,
				index: {}
			}
		};
		const _this_ = this;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") must be a non-empty string.'));
				throw retVal;
			} else if (!(lodash.has(_this_.getDefinition().fields, attributeName) && lodash.isPlainObject(_this_.getDefinition().fields[
					attributeName]) &&
				!lodash.isNil(_this_.getDefinition().fields[attributeName]))) {
				retVal.errors.push(resources.generateError('Invalid Atrribute Name', 'Attribute Name("' + attributeName +
					'") isn\'t in the collection\'s fields.'));
				throw retVal;
			}
			let indexQuery = 'SELECT "INDEX_NAME" AS "name" FROM "PUBLIC"."INDEXES" WHERE "TABLE_NAME" = \'' + _this_.getIdentity() +
				'\' AND "SCHEMA_NAME" =\'' +
				_this_.getSchema() + '\' AND "INDEX_NAME" = \'' + _this_.getIdentity() + '_' + _this_.getAttributeDatabaseName(attributeName) +
				'_INDEX\'';
			let response = connector.execute(indexQuery, null);
			if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.cloneDeep(response.errors);
				throw retVal;
			} else if (lodash.isArray(response.results) && !lodash.isEmpty(response.results)) {
				response = services.dropIndex(_this_.getSchema(), _this_.getIdentity(), _this_.getIdentity() + '_' + _this_.getAttributeDatabaseName(
					attributeName) + '_INDEX');
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.cloneDeep(response.errors);
					throw retVal;
				} else if (lodash.isPlainObject(response.results) && !lodash.isEmpty(response.results)) {
					retVal.results.index = lodash.cloneDeep(response.results.index);
					delete _this_.definition.fields[attributeName].index;
					BaseModel.models[_this_.definition.identity] = lodash.cloneDeep(_this_.definition);
					return retVal;
				}
			} else {
				retVal.errors.push(resources.generateError('Invalid index', 'Index("' + _this_.getIdentity() + '_' + _this_.getAttributeDatabaseName(
					attributeName) + '_INDEX' + '") doesn\'t exist'));
				throw retVal;
			}
		} catch (e) {
			retVal.results.deleted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	drop: function(dropTableAndSequences, dropDependencies) {
		let retVal = {
			errors: [],
			results: {
				deleted: true,
				table: {},
				sequences: [],
				constraints: [],
				indexes: []
			}
		};
		const _this_ = this;
		let identity = _this_.getIdentity();
		dropTableAndSequences = lodash.isBoolean(dropTableAndSequences) ? dropTableAndSequences : false;
		dropDependencies = lodash.isBoolean(dropDependencies) ? dropDependencies : false;
		try {
			if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			} else if (this.definition.type !== 'table') {
				retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
				throw retVal;
			} else if (!dropTableAndSequences) {
				delete BaseModel.models[identity];
				return retVal;
			} else if (lodash.has(BaseModel.models, identity)) {
				if (!(lodash.isString(identity) && !lodash.isEmpty(identity))) {
					retVal.errors.push(resources.generateError('Invalid identity.', 'Identity ("' + identity + '") must be a non empty string.'));
					throw retVal;
				}
				let sequencesQuery =
					'SELECT "SCHEMA_NAME" as "schema", "SEQUENCE_NAME" as "name" FROM "PUBLIC"."SEQUENCES" WHERE "SEQUENCE_NAME" LIKE \'' +
					_this_.definition.identity + '::%\' AND "SCHEMA_NAME" = \'' + _this_.definition.schema + '\'';
				let response = connector.execute(sequencesQuery, null);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
				} else {
					lodash.forEach(response.results, function(sequence) {
						response = services.dropSequence(sequence.schema, sequence.name);
						if (!lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
						} else if (!lodash.isEmpty(response.results.sequence)) {
							retVal.results.sequences.push(response.results.sequence);
						}
					});
				}
				response = services.dropTable(_this_.definition.schema, _this_.definition.identity, dropDependencies);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
				} else if (!lodash.isEmpty(response.results.table)) {
					retVal.results.table = response.results.table;
					retVal.results.constraints = lodash.cloneDeep(response.results.constraints);
					retVal.results.indexes = lodash.cloneDeep(response.results.indexes);
				}
				if (!lodash.isEmpty(retVal.errors)) {
					throw retVal;
				}
				delete BaseModel.models[identity];
			} else {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			}
			return retVal;
		} catch (e) {
			retVal.results.deleted = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	sync: function() {
		let retVal = {
			errors: [],
			results: {
				created: true,
				table: {},
				sequences: [],
				constraints: [],
				indexes: []
			}
		};
		const _this_ = this;
		let identity = _this_.getIdentity();
		try {
			if (lodash.has(BaseModel.models, identity)) {
				if (!(lodash.isString(identity) && !lodash.isEmpty(identity))) {
					retVal.errors.push(resources.generateError('Invalid identity.', 'Identity ("' + identity + '") must be a non empty string.'));
					throw retVal;
				} else if (this.definition.type !== 'table') {
					retVal.errors.push(resources.generateError('Invalid collection\'s type', '"' + this.getIdentity() + '" isn\'t a table.'));
					throw retVal;
				}
				let response = services.createTable(_this_.definition.schema, _this_.definition.identity, lodash.cloneDeep(_this_.definition.fields), lodash.cloneDeep(BaseModel.models));
				retVal.results = response.results;
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
					throw retVal;
				}
				if (!lodash.isEmpty(retVal.errors)) {
					throw retVal;
				}
			} else {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			}
			return retVal;
		} catch (e) {
			retVal.results.created = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
		}
	},
	exists: function() {
	    let retVal = {
	        errors: [],
	        results: {
	            exists: false
	        }
	    };
	    const _this = this;
	    try {
	        if (!lodash.has(BaseModel.models, this.getIdentity())) {
				retVal.errors.push(resources.generateError('Invalid Model', '"' + this.getIdentity() + '" doesn\'t exist.'));
				throw retVal;
			}
			let existenceQuery = '';
			if (_this.definition.type === 'table') {
			    existenceQuery = 'SELECT "SCHEMA_NAME" AS "schema","TABLE_NAME" as "name" FROM "PUBLIC"."TABLES" ';
			    existenceQuery += 'WHERE "SCHEMA_NAME" = \'' + _this.getSchema() + '\' AND "TABLE_NAME" = \'' + _this.getIdentity() + '\''; 
			} else {
			    existenceQuery = 'SELECT "SCHEMA_NAME" AS "schema","VIEW_NAME" as "name" FROM "PUBLIC"."VIEWS" ';
			    existenceQuery += 'WHERE "SCHEMA_NAME" = \'' + _this.getSchema() + '\' AND "VIEW_NAME" = \'' + _this.getIdentity() + '\'';
			}
			const response = connector.execute(existenceQuery);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
				throw retVal;
			} else if (!lodash.isEmpty(response.results)) {
			    retVal.results.exists = true;
			}
			return retVal;
	    } catch(e) {
	        retVal.results.exists = false;
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
	    }
	},
	describe: function() {
	    let retVal = {
	        errors: [],
	        results: null
	    };
	    const _this = this;
	    try {
	        let response = services.getDataBaseFieldsDefinition(_this.getSchema(), _this.getIdentity(), _this.definition.type === 'view');
			if (!lodash.isEmpty(response.errors)) {
			    retVal.errors = lodash.cloneDeep(response.errors);
			    throw retVal;
			}
			return response;
	    } catch(e) {
			let error = resources.parseError(e);
			if (!lodash.isEqual(retVal, error)) {
				retVal.errors.push(error);
			}
			return retVal;
	    }
	},
	getType: function() {
	    return lodash.cloneDeep(this.definition.type);
	},
	getDefinition: function() {
		return lodash.cloneDeep(this.definition);
	},
	getIdentity: function() {
		return lodash.cloneDeep(this.definition.identity);
	},
	getName: function() {
		return lodash.cloneDeep(this.definition.name);
	},
	getSchema: function() {
		return lodash.cloneDeep(this.definition.schema);
	},
	getAttributeDatabaseName: function(attributeName) {
		if (lodash.isString(attributeName) && !lodash.isNil(this.definition.fields[attributeName])) {
			return lodash.cloneDeep(this.definition.fields[attributeName].columnName);
		}
		return null;
	},
	getAttributeType: function(attributeName) {
		if (lodash.isString(attributeName) && !lodash.isNil(this.definition.fields[attributeName])) {
			return lodash.cloneDeep(this.definition.fields[attributeName].type);
		}
		return null;
	},
	getInitWarnings: function() {
		return this.warnings;
	},
	getInitErrors: function() {
		return this.errors;
	},
	isValidBaseModel: function() {
		return this.isValid;
	},
	getInitStatus: function() {
		return {
			errors: this.errors,
			warnings: this.warnings,
			isValid: this.isValid
		};
	}
};

this.BaseModel = BaseModel;