$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

$.import('timp.core.server.orm','table');
var table = $.timp.core.server.orm.table;

$.import('timp.core.server.orm.table','fields');
var fieldsLib = $.timp.core.server.orm.table.fields;
for (var x in fieldsLib) {
	this[x] = fieldsLib[x];
}

/*@doc
Example:
	new table.View({
		name: '"TIMP"."BRB::Report"',
		base: 
			'table' (default),
			'calculationview'
			'analyticview'
			'attributeview'
		versioning (optional): 
			'simple' //Just create a versions table to make rollback work
			'approval' //Create the versions table to make rollback, makes the inactive versions for the approval cycle
			'daterange' //Keep multiple versions active, will require a current_date when reading
		defaultFields: 'common', //CREATION.DATE CREATION.ID_USER MODIFICATION.DATA MODIFICATION.ID_USER
		object: 2,
		fields: {
			id (required): new table.AutoField({ //If you don't set it on the 
				name: 'ID',
				auto: '"TIMP"."BRB::Report::ID".nextval', //No one uses the following format anymore: '"TIMP"."timp.brb.server.install.sequences::report_id".nextval',
				type: $.db.types.INTEGER,
				pk: true
			}),
			label: new table.Field({
				name: 'isDeleted',
				type: $.db.types.TINYINT,
				translate: {1: true, 2: false}
			}),
			label: new table.Field({
				name: "FIELDNAME",      
				type: $.db.types.TIMESTAMP,
				format (optional): "DD.MM.YYYY HH:MI:SS.FF"
			})
		}
	})
*/
function View (options) {
	if(this instanceof View) {
		return this.__init__(options);
	} else {
		return new View(options);
	}
}

this.View = View;

View.prototype.__init__ = function (options) {
	this.name   = options.name;
	this.fields = {};
	this.hints = options.hints || [];
	this.table = this;
	this.instanceOf = 'View'; //Dirty fix for messy imports
	// this.component = options.component;
	this.idComponent = null;
	this.versioning = options.versioning || false;
	this.base = options.base || 'table';
	
	if(options.hasOwnProperty('tags')) {
		this.tags = options.tags;
	}
	
	if (options.hasOwnProperty('object')) {
		// this.idComponent = SQL.SELECT({
		//     query: 'SELECT ID FROM "TIMP"."CORE::Components"'
		// })[0][0];
		this.object = options.object;
		$.import('timp.core.server.api','api');
		var coreApi = $.timp.core.server.api.api;
		this.objects = coreApi.objects;
	} else {
		this.object = false;
	}

	this.sql = SQL;

	for(var i in options.fields) {
		if(options.fields.hasOwnProperty(i)) {
			var field = options.fields[i];
			
			if (util.isValid(field) && (field.instanceOf == 'Field' || field.instanceOf == 'AutoField')) {
				this.__parseField__(i, field);
			}
		}
	}
};

for (var method in table.Table.prototype) {
    if (method !== '__init__') {
        View.prototype[method] = table.Table.prototype[method];
    }
}

/*@doc
Construct model from table reference
    GENERATE_TABLE({
        schema: 'TIMP',
        table: 'FOOBAR'
    })
*/
this.GENERATE_VIEW = function (options) {
    var stm;
    if (options.schema == '_SYS_BIC') {
        stm = 'SELECT C.COLUMN_NAME, C.DATA_TYPE_NAME, C.LENGTH, C.OFFSET '
            + 'FROM "SYS"."VIEWS" AS T '
            + 'INNER JOIN "SYS"."VIEW_COLUMNS" AS C ON C.VIEW_OID = T.VIEW_OID '
            + 'WHERE T.VIEW_NAME = ? AND T.SCHEMA_NAME = ?';
    } else {
        stm = 'SELECT C.COLUMN_NAME, C.DATA_TYPE_NAME, C.LENGTH, C.OFFSET '
            + 'FROM "SYS"."TABLES" AS T '
            + 'INNER JOIN "SYS"."TABLE_COLUMNS" AS C ON C.TABLE_OID = T.TABLE_OID '
            + 'WHERE T.TABLE_NAME = ? AND T.SCHEMA_NAME = ?';
    }
    var meta = ['name', 'type', 'dimension', 'precision'];
	var response = SQL.SELECT({
		query: stm,
		values: [
			{type: $.db.types.NVARCHAR, value: options.table },
			{type: $.db.types.NVARCHAR, value: options.schema}
		]
	});
	
	var _options = {
        name: '"' + options.schema + '"."' + options.table + '"',
        fields: {}
    };
    
    var sFields = [];
    var field;
    for (var i = 0; i < response.length; i++) {
        var line = response[i];
        field = {};
        for (var u = 0; u < meta.length; u++) {
            var name = meta[u];
            field[name] = line[u];
        }
        sFields.push(field);
    }
    
    var fields = [];
    for (i = 0; i < sFields.length; i++) {
        field = sFields[i];
        fields.push(field.name);
        _options.fields[field.name] = new this.Field({
    		name: field.name,
    		type: $.db.types[field.type],
    		dimension: field.dimension,
    		precision: field.precision
    	});
    }
    
    var view = View(_options);
    
    return view;
};
