$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;


$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

const _this = this;
/**
 * Drops a sequence.
 * @returns {Object} status - the status of the sequence after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       deleted: true
 * //       sequence: {
 * //           name: '"TIMP"."CORE::User::ID"',
 * //           operation: 'DROP'
 * //       }
 * //   }
 * // }
 * @param {string} schema - sequence's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - sequence's name.
 * @example
 * // 'CORE::User::ID'
 */
_this.dropSequence = function(schema, identity) {
	let retVal = {
		errors: [],
		results: {
			deleted: true,
			sequence: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		}
		let sequenceQuery =
			'SELECT "SCHEMA_NAME" as "schemaName", "SEQUENCE_NAME" as "sequenceName" FROM "PUBLIC"."SEQUENCES" WHERE "SEQUENCE_NAME" = \'' +
			identity + '\' AND "SCHEMA_NAME" = \'' + schema + '\'';
		let response = connector.execute(sequenceQuery, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (!lodash.isNil(response.results[0]) && lodash.isPlainObject(response.results[0])) {
			sequenceQuery = 'DROP SEQUENCE "' + schema + '"."' + identity + '"';
			response = connector.execute(sequenceQuery, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			retVal.results.sequence = {
				name: '"' + schema + '"."' + identity + '"',
				operation: 'DROP'
			};
		} else {
			retVal.errors.push(resources.generateError('Invalid Sequence', 'Sequence doesn\'t exist.'));
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.deleted = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Drops a table.
 * @returns {Object} status - the status of the table after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       deleted: true,
 * //       table: {
 * //           name: '"TIMP"."CORE::User"',
 * //           operation: 'DROP'
 * //       },
 * //       constraints: [{
 * //           name: 'CORE::User_USER_NAME_UNIQUE_CONSTRAINT',
 * //           operation: 'DROP'
 * //       }]
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User'
 * @param {boolean} dropDependencies - delete all the constraints that have the this table as the referenced table.
 * @example
 * // true
 */
_this.dropTable = function(schema, identity, dropDependencies) {
	let retVal = {
		errors: [],
		results: {
			deleted: true,
			table: {},
			constraints: [],
			indexes: []
		}
	};
	dropDependencies = lodash.isBoolean(dropDependencies) ? dropDependencies : false;
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		}
		let tableQuery = 'SELECT "SCHEMA_NAME" as "schemaName", "TABLE_NAME" as "tableName" FROM "PUBLIC"."TABLES" WHERE "TABLE_NAME" = \'' +
			identity + '\' AND "SCHEMA_NAME" = \'' + schema + '\'';
		let response = connector.execute(tableQuery, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (!lodash.isNil(response.results[0]) && lodash.isPlainObject(response.results[0])) {
			if (dropDependencies) {
				let dependenciesQuery =
					'SELECT  "TABLE_NAME" as "table", "SCHEMA_NAME" as "schema", "CONSTRAINT_NAME" as "name" FROM "REFERENTIAL_CONSTRAINTS" WHERE ("REFERENCED_TABLE_NAME" = \'' +
					identity + '\' AND "REFERENCED_SCHEMA_NAME" = \'' + schema + '\')' +
					' OR ("TABLE_NAME" = \'' + identity + '\' AND "SCHEMA_NAME" = \'' + schema + '\')';
				response = connector.execute(dependenciesQuery, null);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
				} else if (lodash.isArray(response.results) && !lodash.isEmpty(response.results)) {
					lodash.forEach(response.results, function(constraint) {
						response = _this.dropConstraint(constraint.schema, constraint.table, constraint.name);
						if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
							retVal.errors = lodash.concat(retVal.errors, response.errors);
						} else if (lodash.isPlainObject(response.results) && !lodash.isEmpty(response.results) && response.results.deleted) {
							retVal.results.constraints.push(response.results.constraint);
						}
					});
				}
			}
			let indexesQuery =
				'SELECT "INDEX_NAME" AS "name", "SCHEMA_NAME" as "schema", "TABLE_NAME" as "table" FROM "PUBLIC"."INDEXES" WHERE "SCHEMA_NAME" = \'' +
				schema +
				'\' AND "TABLE_NAME" = \'' + identity + '\' AND "INDEX_NAME" NOT LIKE \'_SYS_%\'';
			response = connector.execute(indexesQuery, null);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
			} else if (lodash.isArray(response.results) && !lodash.isEmpty(response.results)) {
				lodash.forEach(response.results, function(index) {
					response = _this.dropIndex(index.schema, index.table, index.name);
					if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					} else if (lodash.isPlainObject(response.results) && !lodash.isEmpty(response.results) && response.results.deleted) {
						retVal.results.indexes.push(response.results.index);
					}
				});
			}
			tableQuery = 'DROP TABLE "' + schema + '"."' + identity + '"';
			response = connector.execute(tableQuery, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			retVal.results.table = {
				name: '"' + schema + '"."' + identity + '"',
				operation: 'DROP'
			};
		} else {
			retVal.errors.push(resources.generateError('Table doesn\'t exist.', 'Schema("' + schema + '") and identity ("' + identity +
				'") table doesn\'t exist in the database.'));
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.deleted = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Creates a sequence.
 * @returns {Object} status - the status of the sequence after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //       sequence: {
 * //           name: '"TIMP"."CORE::User::ID"',
 * //           operation: 'CREATE'
 * //       }
 * //   }
 * // }
 * @param {string} schema - sequence's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - sequence's name.
 * @example
 * // 'CORE::User::ID'
 */
_this.createSequence = function(schema, identity, value) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			sequence: {}
		}
	};
	value = (lodash.isInteger(value) && value > 0) ? value : 1;
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		}
		let sequenceQuery = 'CREATE SEQUENCE "' + schema + '"."' + identity + '" INCREMENT BY 1 START WITH ' + value + ' MINVALUE 1 no cycle';
		let response = connector.execute(sequenceQuery, null, true);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		}
		retVal.results.sequence = {
			name: '"' + schema + '"."' + identity + '"',
			operation: 'CREATE'
		};

		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};
/**
 * Alters a sequence.
 * @returns {Object} status - the status of the sequence after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       updated: true,
 * //       sequence: {
 * //           name: '"TIMP"."CORE::User::ID"',
 * //           operation: 'ALTER'
 * //       }
 * //   }
 * // }
 * @param {string} schema - sequence's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - sequence's name.
 * @example
 * // 'CORE::User::ID'
 */
_this.alterSequence = function(schema, identity, value){
    let retVal = {
		errors: [],
		results: {
			updated: true,
			sequence: {}
		}
	};
	value = (lodash.isInteger(value) && value > 0) ? value : 1;
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		}
		let sequenceQuery = 'ALTER SEQUENCE "' + schema + '"."' + identity + '" RESTART WITH ' + value;
		let response = connector.execute(sequenceQuery, null, true);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		}
		retVal.results.sequence = {
			name: '"' + schema + '"."' + identity + '"',
			operation: 'ALTER'
		};

		return retVal;
	} catch (e) {
		retVal.results.updated = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};
_this.alterComment = function(schema, identity, column, comment) {
	let retVal = {
		errors: [],
		results: {
			created: true
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(column) && !lodash.isEmpty(column))) {
			retVal.errors.push(resources.generateError('Invalid Column.', 'Column("' + column + '") must be non empty string.'));
			throw retVal;
		} else if (!(lodash.isString(comment) && !lodash.isEmpty(comment))) {
			retVal.errors.push(resources.generateError('Invalid Column.', 'Column("' + comment + '") must be non empty string.'));
			throw retVal;
		}
		comment = comment.replace(new RegExp('\'', 'g'), '\'\'');
		let query = 'COMMENT ON COLUMN "' + schema + '"."' + identity + '"."' + column + '" IS \'' + comment + '\'';
		let response = connector.execute(query, null, true);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

_this.deleteComment = function(schema, identity, column) {
	let retVal = {
		errors: [],
		results: {
			deleted: true
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(column) && !lodash.isEmpty(column))) {
			retVal.errors.push(resources.generateError('Invalid Column.', 'Column("' + column + '") must be non empty string.'));
			throw retVal;
		}
		let query = 'COMMENT ON COLUMN "' + schema + '"."' + identity + '"."' + column + '" IS NULL';
		let response = connector.execute(query, null, true);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.deleted = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Creates a table.
 * @returns {Object} status - the status of the table after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //       table: {
 * //           name: '"TIMP"."CORE::User"',
 * //           operation: 'CREATE'
 * //       },
 * //       constraints: [{
 * //           name: 'CORE::User_USER_NAME_UNIQUE_CONSTRAINT',
 * //           operation: 'CREATE'
 * //       }],
 * //       sequences: [{
 * //           name: '"TIMP"."CORE::User::ID"',
 * //           operation: 'CREATE'
 * //       }],
 * //       indexes: [{
 * //           name: '"TIMP"."CORE::User_USER_NAME_INDEX"',
 * //           operation: 'CREATE'
 * //       }]
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User'
 * @param {boolean} fields - the table's columns (Check timp's fields documentation for examples).
 * @param {Object} collections - the collections used for the foreign key constraints.
 */
_this.createTable = function(schema, identity, fields, collections) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			table: {},
			sequences: [],
			constraints: [],
			indexes: []
		}
	};
	const _this_ = this;
	collections = collections || {};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		}
		if (!(lodash.isPlainObject(fields) && !lodash.isNil(fields) && !lodash.isEmpty(fields))) {
			retVal.errors.push(resources.generateError('Invalid definition.', 'Definition isn\'t a valid object.'));
			throw retVal;
		}
		let columnQuery;
		let hanaMap = resources.getSQLMap();
		let queries = {
			sequences: [],
			columns: [],
			table: {},
			primaryKeys: [],
			constraints: [],
			indexes: [],
			comments: []
		};
		let currentModelStatus = {
			tableExists: false
		};
		let response;
		let query = 'SELECT "SCHEMA_NAME" AS "schema", "TABLE_NAME" AS "name" FROM "PUBLIC"."TABLES" ';
		query += 'WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "TABLE_NAME" = \'' + identity + '\'';
		response = connector.execute(query, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		} else if (!lodash.isEmpty(response.results)) {
			currentModelStatus.tableExists = true;
		}
		query = 'SELECT "SEQUENCE_NAME" AS "name" FROM "PUBLIC"."SEQUENCES"';
		query += 'WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "SEQUENCE_NAME" LIKE \'' + identity + '::%\'';
		response = connector.execute(query, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		} else if (!lodash.isEmpty(response.results)) {
			lodash.forEach(response.results, function(sequence) {
				currentModelStatus[sequence.name] = true;
			});
		}
		lodash.forEach(fields, function(definition, field) {
			columnQuery = '';
			if (!(lodash.isString(definition.columnName) && !lodash.isEmpty(definition.columnName))) {
				definition.columnName = field;
			}
			if (lodash.has(collections, definition.type) || definition.type === identity) {
				let collectionB;
				if (lodash.has(collections, definition.type)) {
					collectionB = collections[definition.type];
				} else if (definition.type === identity) {
					collectionB = {
						fields: lodash.cloneDeep(fields),
						schema: schema,
						identity: identity,
						primaryKeys: []
					};
					lodash.forEach(fields, function(def, _field) {
						if (lodash.isBoolean(def.primaryKey) && def.primaryKey) {
							collectionB.primaryKeys.push(_field);
						}
					});
				}

				let primaryKey = collectionB.primaryKeys[0];
				definition.type = collectionB.fields[primaryKey].type;
				if (collectionB.fields[primaryKey].type === 'string') {
					definition.size = collectionB.fields[primaryKey].size;
				}
				if (!lodash.isNil(definition.isonetoone) && definition.isonetoone) {
					definition.unique = true;
				}
				if (!lodash.isNil(definition.constraints) && !lodash.isNil(definition.constraints.onDelete) && !lodash.isNil(definition.constraints.onUpdate)) {
					// If it has fk constraints, build the alter table query. The fk's name has the following standard: TABLE1_FIELD_TABLE2_FIELD
					let alterTableQuery = 'ALTER TABLE "' + schema + '"."' + identity + '"' +
						'  ADD CONSTRAINT "' + identity + '_' + definition.columnName + '_' + collectionB.identity + '_' + collectionB.fields[primaryKey].columnName +
						'"' +
						'  FOREIGN KEY ("' + definition.columnName + '")' +
						'  REFERENCES "' + collectionB.schema + '"."' + collectionB.identity + '"("' + collectionB.fields[primaryKey].columnName + '")' +
						'  ON UPDATE ' + definition.constraints.onUpdate.replace('_', ' ') +
						'  ON DELETE ' + definition.constraints.onDelete.replace('_', ' ');

					queries.constraints.push({
						name: '"' + identity + '_' + definition.columnName + '_' + collectionB.identity + '_' + collectionB.fields[primaryKey].columnName +
							'"',
						query: alterTableQuery
					});
				}
			}
			columnQuery += '"' + definition.columnName + '" ' + hanaMap[definition.type] + ' ';
			if (definition.type === 'string') {
				columnQuery += '(' + definition.size + ') ';
			} else if (definition.type === 'decimal') {
				columnQuery += '(' + definition.size + ', ' + definition.precision + ')';
			}
			if (lodash.isPlainObject(definition.index) && !lodash.isEmpty(definition.index)) {
				queries.indexes.push({
					schema: schema,
					identity: identity,
					options: {
						type: definition.index.type,
						globalIndexOrder: lodash.isString(definition.index.globalIndexOrder) && lodash.indexOf(['ASC', 'DESC'], lodash.toUpper(definition.index
							.globalIndexOrder)) > -1 ? lodash.toUpper(definition.index.globalIndexOrder) : 'ASC',
						columns: [{
							name: definition.columnName
					    }]
					}
				});
			}
			if (!lodash.isNil(definition.autoIncrement) && definition.autoIncrement && lodash.isNil(currentModelStatus[identity + '::' + definition
				.columnName])) {
				queries.sequences.push({
					schema: schema,
					identity: identity + '::' + definition.columnName,
					columnName: definition.columnName
				});
			}
			if (!lodash.isNil(definition.unique) && definition.unique) {
				columnQuery += ' UNIQUE ';
			}
			if ((!lodash.isNil(definition.required) && definition.required) || (!lodash.isNil(definition.primaryKey) && definition.primaryKey)) {
				columnQuery += ' NOT NULL ';
			} else {
				columnQuery += ' NULL ';
			}
			if (!lodash.isNil(definition.default) && !(lodash.isString(definition.default) && lodash.has(resources.getSQLStatementsConstants(),
				lodash.toLower(definition.default)))) {
				if (definition.type === 'string') {
					definition.default = '\'' + definition.default + '\'';
				}
				columnQuery += ' DEFAULT ' + definition.default;
			}
			if (lodash.isBoolean(definition.primaryKey) && definition.primaryKey) {
				queries.primaryKeys.push('"' + definition.columnName + '"');
			}
			if (lodash.isString(definition.comment) && !lodash.isEmpty(definition.comment)) {
				queries.comments.push({
					schema: schema,
					identity: identity,
					column: definition.columnName,
					comment: definition.comment
				});
			}
			queries.columns.push(columnQuery);
		});
		let newValue;
		lodash.forEach(queries.sequences, function(sequence) {
			if (currentModelStatus.tableExists) {
				newValue = 'SELECT (IFNULL(MAX("' + sequence.columnName + '"), 0) + 1) as "value" FROM "' + schema + '"."' + identity + '"';
				response = connector.execute(newValue, null);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(retVal.errors, response.errors);
				} else if (!lodash.isEmpty(response.results)) {
					newValue = response.results[0].value;
				}
			} else {
				newValue = 1;
			}
			response = _this_.createSequence(sequence.schema, sequence.identity, newValue);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
			} else if (!lodash.isEmpty(response.results.sequence)) {
				retVal.results.sequences.push(response.results.sequence);
			}
		});
		let createTableQuery = 'CREATE COLUMN TABLE "' + schema + '"."' + identity + '"( ' + lodash.join(queries.columns, ',');
		if (!lodash.isEmpty(queries.primaryKeys)) {
			createTableQuery += ', PRIMARY KEY (' + lodash.join(queries.primaryKeys) + ')';
		}
		createTableQuery += ')';
		if (!currentModelStatus.tableExists) {
			response = connector.execute(createTableQuery, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(retVal.errors, response.errors);
			} else {
				retVal.results.table = {
					name: '"' + schema + '"."' + identity + '"',
					operation: 'CREATE'
				};
			}
			if (!lodash.isEmpty(queries.constraints)) {
				lodash.forEach(queries.constraints, function(constraint) {
					response = connector.execute(constraint.query, null, true);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					} else {
						retVal.results.constraints.push({
							name: constraint.name,
							operation: 'CREATE'
						});
					}
				});
			}

			if (!lodash.isEmpty(queries.indexes)) {
				lodash.forEach(queries.indexes, function(index) {
					response = _this.createIndex(index.schema, index.identity, index.options);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					} else {
						retVal.results.indexes.push(response.results.index);
					}
				});
			}

			if (!lodash.isEmpty(queries.comments)) {
				lodash.forEach(queries.comments, function(comment) {
					response = _this.alterComment(comment.schema, comment.identity, comment.column, comment.comment);
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(retVal.errors, response.errors);
					}
				});
			}

			if (!lodash.isEmpty(retVal.errors)) {
				throw retVal;
			}
		}
		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Alters a sequence.
 * @returns {Object} status - the status of the sequence after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       altered: true
 * //       sequence: {
 * //           name: '"TIMP"."CORE::User::ID"',
 * //           operation: 'DROP'
 * //       }
 * //   }
 * // }
 * @param {string} schema - sequence's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - sequence's name.
 * @example
 * // 'CORE::User::ID'
 * @param {string} newStartValue - sequence's new start value.
 * @example
 * // 256
 */
_this.alterSequence = function(schema, identity, newStartValue) {
	let retVal = {
		errors: [],
		results: {
			altered: true,
			sequence: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!lodash.isNumber(newStartValue) || !(lodash.isNumber(newStartValue) && newStartValue > 0)) {
			retVal.errors.push(resources.generateError('Invalid newStartValue.', 'newStartValue("' + newStartValue + '") must be a positive integer'));
			throw retVal;
		}
		let sequenceQuery =
			'SELECT "SCHEMA_NAME" as "schemaName", "SEQUENCE_NAME" as "sequenceName" FROM "PUBLIC"."SEQUENCES" WHERE "SEQUENCE_NAME" = \'' +
			identity + '\' AND "SCHEMA_NAME" = \'' + schema + '\'';
		let response = connector.execute(sequenceQuery, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (!lodash.isNil(response.results[0]) && lodash.isPlainObject(response.results[0])) {
			sequenceQuery = 'ALTER SEQUENCE "' + schema + '"."' + identity + '" RESTART WITH ' + newStartValue;
			response = connector.execute(sequenceQuery, null, true);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
				throw retVal;
			}
			retVal.results.sequence = {
				name: '"' + schema + '"."' + identity + '"',
				operation: 'ALTER'
			};
		} else {
			retVal.errors.push(resources.generateError('Invalid Sequence', 'Sequence doesn\'t exist.'));
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.altered = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Adds the unique constraint to the specified attributeName
 * @returns {Object} status - the status of the table after the operation.
 *  * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //       constraint: {
 * //           name: 'CORE::User_USER_NAME_UNIQUE_CONSTRAINT',
 * //           operation: 'CREATE'
 * //       }
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User'
 * @param {string} attributeName - table's column name to have the unique constraint
 * // 'USER_NAME'
 */
_this.addUniqueConstraint = function(schema, identity, attributeName) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			constraint: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
			retVal.errors.push(resources.generateError('Invalid attributeName.', 'Attribute Name ("' + attributeName +
				'") must be a non empty string.'));
			throw retVal;
		}
		let uniqueQuery = 'ALTER TABLE "' + schema + '"."' + identity + '"' +
			' ADD CONSTRAINT "' + identity + '_' + attributeName + '_UNIQUE_CONSTRAINT"' +
			' UNIQUE("' + attributeName + '")';
		const response = connector.execute(uniqueQuery, null, true);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		retVal.results.constraint = {
			operation: 'CREATE',
			name: identity + '_' + attributeName + '_UNIQUE_CONSTRAINT'
		};

		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Adds the foreign key constraint to the specified attributeName
 * @returns {Object} status - the status of the table after the operation.
 *  * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //       constraints: [{
 * //           name: 'CORE::User_creationUser_CORE::User_ID',
 * //           operation: 'CREATE'
 * //       }]
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User'
 * @param {string} attributeName - table's column name to have the unique constraint
 * // 'creationUser'
 * @param {Object} options - the referenced table, if it's one to one or one to many relationship, and onDelete and onUpdate values
 * @example
 * // {
 * //   isonetoone: true,
 * //   isonetomany: true
 * //   referencedTable: {
 * //       schema: 'TIMP',
 * //       identity: 'CORE::User',
 * //       columnName: 'ID'
 * //   },
 * //   onUpdate: 'CASCADE',
 * //   onDelete: 'SET_NULL'
 * //
 */
_this.addForeignKeyConstraint = function(schema, identity, attributeName, options) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			constraints: []
		}
	};
	const constraintOptions = ['CASCADE', 'SET_DEFAULT', 'SET_NULL', 'RESTRICT'];
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(attributeName) && !lodash.isEmpty(attributeName))) {
			retVal.errors.push(resources.generateError('Invalid attributeName.', 'Attribute Name ("' + attributeName +
				'") must be a non empty string.'));
			throw retVal;
		} else if (!(lodash.isPlainObject(options) && !lodash.isEmpty(options))) {
			retVal.errors.push(resources.generateError('Invalid options.', 'Options must be a non-empty object.'));
			throw retVal;
		} else if (!((lodash.isBoolean(options.isonetoone) && options.isonetoone) || (lodash.isBoolean(options.isonetomany) && options.isonetomany))) {
			retVal.errors.push(resources.generateError('Invalid options.', 'Options must have isonetoone or isonetomany (boolean).'));
			throw retVal;
		} else if (!(lodash.isPlainObject(options.referencedTable) && (lodash.isString(options.referencedTable.schema) && !lodash.isEmpty(options
				.referencedTable.schema)) && (lodash.isString(options.referencedTable.identity) && !lodash.isEmpty(options.referencedTable.identity)) &&
			(lodash.isString(options.referencedTable.columnName) && !lodash.isEmpty(options.referencedTable.columnName)))) {
			retVal.errors.push(resources.generateError('Invalid options.',
				'Options must have referencedTable property with schema(string), identity(string), and columnName(string).'));
			throw retVal;
		} else if (!(lodash.isString(options.onUpdate) && lodash.indexOf(constraintOptions, options.onUpdate) > -1 && lodash.isString(options.onDelete) &&
			lodash.indexOf(constraintOptions, options.onDelete) > -1)) {
			retVal.errors.push(resources.generateError('Invalid options.', 'Options must have the properties onUpdate(string) and onDelete(string).'));
			throw retVal;
		}
		if (options.isonetoone) {
			let uniqueResponse = _this.addUniqueConstraint(schema, identity, attributeName);
			if (lodash.isArray(uniqueResponse.errors) && !lodash.isEmpty(uniqueResponse.errors)) {
				retVal.errors = uniqueResponse.errors;
				throw retVal;
			}
			retVal.results.constraints.push(uniqueResponse.results.constraint);
		}
		let fkQuery = 'ALTER TABLE "' + schema + '"."' + identity + '"' +
			' ADD CONSTRAINT "' + identity + '_' + attributeName + '_' + options.referencedTable.identity + '_' + options.referencedTable.columnName +
			'"' +
			' FOREIGN KEY("' + attributeName + '")' +
			' REFERENCES "' + options.referencedTable.schema + '"."' + options.referencedTable.identity + '"("' + options.referencedTable.columnName +
			'")' +
			' ON DELETE ' + options.onDelete.replace('_', ' ') +
			' ON UPDATE ' + options.onUpdate.replace('_', ' ');

		const response = connector.execute(fkQuery, null, true);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		retVal.results.constraints.push({
			name: identity + '_' + attributeName + '_' + options.referencedTable.identity + '_' + options.referencedTable.columnName,
			operation: 'CREATE'
		});
		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Drops a constraint.
 * @returns {Object} status - the status of the constraint after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       deleted: true,
 * //       constraint: {
 * //           name: 'CORE::User_USER_NAME_UNIQUE_CONSTRAINT',
 * //           operation: 'DROP'
 * //       }
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User::ID'
 * @param {string} constraintName - constraint's name
 * @example
 * // 'CORE::User_USER_NAME_UNIQUE_CONSTRAINT'
 */
_this.dropConstraint = function(schema, identity, constraintName) {
	let retVal = {
		errors: [],
		results: {
			deleted: true,
			constraint: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(constraintName) && !lodash.isEmpty(constraintName))) {
			retVal.errors.push(resources.generateError('Invalid attributeName.', 'Constraint Name ("' + constraintName +
				'") must be a non empty string.'));
			throw retVal;
		}
		let constraintExistenceQuery = 'SELECT "SCHEMA_NAME" AS "schema", "CONSTRAINT_NAME" AS "name"' +
			' FROM (SELECT "SCHEMA_NAME", "CONSTRAINT_NAME"' +
			' FROM "PUBLIC"."CONSTRAINTS"' +
			' WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "CONSTRAINT_NAME" = \'' + constraintName + '\' AND "TABLE_NAME" = \'' + identity + '\')' +
			' UNION (SELECT "SCHEMA_NAME", "CONSTRAINT_NAME" FROM "PUBLIC"."REFERENTIAL_CONSTRAINTS"' +
			' WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "CONSTRAINT_NAME" = \'' + constraintName + '\' AND "TABLE_NAME" = \'' + identity + '\')';
		let response = connector.execute(constraintExistenceQuery, null);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		} else if (lodash.isArray(response.results) && lodash.isEmpty(response.results)) {
			retVal.errors.push(resources.generateError('Invalid constraint', 'Constraint "' + constraintName + '" doesn\'t exist.'));
			throw retVal;
		}
		let dropConstraintQuery = 'ALTER TABLE "' + schema + '"."' + identity + '"' +
			' DROP CONSTRAINT "' + constraintName + '"';
		response = connector.execute(dropConstraintQuery, null, true);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		retVal.results.constraint = {
			operation: 'DROP',
			name: constraintName
		};
		return retVal;
	} catch (e) {
		retVal.results.deleted = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Adds the index to the specified columns
 * @returns {Object} status - the status of the table after the operation.
 *  * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //       index: {
 * //           name: 'CORE::User_creationUser_INDEX',
 * //           operation: 'CREATE'
 * //       }
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User'
 * @param {Object} options - columns and type that the index will use.
 * @example
 * // {
 * //   type: 'default', // 'cpbtree', 'unique', 'btree', 'default'
 * //   globalIndexOrder: 'ASC', // 'ASC', 'DESC'
 * //   columns: [{
 * //       order: 'ASC', // 'DESC'
 * //       name: 'ID'
 * //   }]
 * //
 */
_this.createIndex = function(schema, identity, options) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			index: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isPlainObject(options) && !lodash.isEmpty(options))) {
			retVal.errors.push(resources.generateError('Invalid options.', 'Options must be a non-empty object.'));
			throw retVal;
		} else if (!(lodash.isArray(options.columns) && !lodash.isEmpty(options.columns))) {
			retVal.errors.push(resources.generateError('Invalid options.columns.', 'Options.columns must be a non-empty array.'));
			throw retVal;
		}
		const validIndexTypes = resources.getValidIndexTypes();
		if (!lodash.isString(options.type) || lodash.isNil(validIndexTypes[options.type])) {
			options.type = 'default';
		}
		let hasGlobalIndexOrder = lodash.isString(options.globalIndexOrder) && lodash.indexOf(['ASC', 'DESC'], lodash.toUpper(options.globalIndexOrder)) >
			-1;
		let parsedColumns = [];
		let tempOrder;
		let indexName = identity + '_';
		lodash.forEach(options.columns, function(column) {
			tempOrder = '';
			if (!hasGlobalIndexOrder && lodash.isString(column.order) && lodash.indexOf(['ASC', 'DESC'], lodash.toUpper(column.order)) > -1) {
				tempOrder = lodash.toUpper(column.order);
			}
			indexName += column.name + '_';
			parsedColumns.push('"' + column.name + '" ' + tempOrder);
		});
		let createIndexQuery = 'CREATE ' + validIndexTypes[options.type] + ' INDEX "' + indexName + 'INDEX" ON ' +
			'"' + schema + '"."' + identity + '"(' + lodash.join(parsedColumns, ',') + ')';
		if (hasGlobalIndexOrder) {
			createIndexQuery += ' ' + lodash.toUpper(options.globalIndexOrder);
		}
		let response = connector.execute(createIndexQuery, null, true);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		retVal.results.index = {
			operation: 'CREATE',
			name: indexName + 'INDEX'
		};
		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Drops an index.
 * @returns {Object} status - the status of the index after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       deleted: true,
 * //       index: {
 * //           name: 'CORE::User_USER_NAME_INDEX',
 * //           operation: 'DROP'
 * //       }
 * //   }
 * // }
 * @param {string} schema - table's schema.
 * @example
 * // 'TIMP'
 * @param {string} identity - table's name.
 * @example
 * // 'CORE::User::ID'
 * @param {string} indexName - index name
 * @example
 * // 'CORE::User_USER_NAME_INDEX'
 */
_this.dropIndex = function(schema, identity, indexName) {
	let retVal = {
		errors: [],
		results: {
			deleted: true,
			index: {}
		}
	};
	try {
		if (!(lodash.isString(schema) && lodash.isString(identity) && !lodash.isEmpty(schema) && !lodash.isEmpty(identity))) {
			retVal.errors.push(resources.generateError('Invalid schema or identity.', 'Schema("' + schema + '") and identity ("' + identity +
				'") must be non empty strings.'));
			throw retVal;
		} else if (!(lodash.isString(indexName) && !lodash.isEmpty(indexName))) {
			retVal.errors.push(resources.generateError('Invalid indexName.', 'Index Name ("' + indexName +
				'") must be a non empty string.'));
			throw retVal;
		}
		let indexExistenceQuery = 'SELECT "SCHEMA_NAME" AS "schema", "INDEX_NAME" AS "name"' +
			' FROM "PUBLIC"."INDEXES" WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "TABLE_NAME" = \'' + identity +
			'\' AND "INDEX_NAME" =\'' + indexName + '\'';
		let response = connector.execute(indexExistenceQuery, null);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		} else if (lodash.isArray(response.results) && lodash.isEmpty(response.results)) {
			retVal.errors.push(resources.generateError('Invalid index', 'index "' + indexName + '" doesn\'t exist.'));
			throw retVal;
		}
		let dropIndexQuery = 'DROP INDEX "' + indexName + '"';
		response = connector.execute(dropIndexQuery, null, true);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		retVal.results.index = {
			operation: 'DROP',
			name: indexName
		};
		return retVal;
	} catch (e) {
		retVal.results.deleted = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

_this.getDataBaseFieldsDefinition = function(schema, name, isCalculationView) {
	let retVal = {
		errors: [],
		results: {
			definition: null
		}
	};

	try {
		if (!lodash.isBoolean(isCalculationView)) {
			isCalculationView = false;
		}
		if (!lodash.isString(schema) || (lodash.isString(schema) && lodash.isEmpty(schema))) {
			retVal.errors.push(resources.generateError('Invalid schema', 'Schema ("' + schema + '") must be a non-empty string.'));
			throw retVal;
		} else if (!lodash.isString(name) || (lodash.isString(name) && lodash.isEmpty(name))) {
			retVal.errors.push(resources.generateError('Invalid name', 'Name ("' + name + '") must be a non-empty string.'));
			throw retVal;
		}
		let options = {
			fields: {},
			inputParameters: {}
		};
		let query;
		if (isCalculationView) {
			query =
				'SELECT "COLUMN_NAME" AS "columnName", "COMMENTS" AS "comment", "DATA_TYPE_NAME" AS "dataType", "LENGTH" AS "size", "SCALE" AS "precision", "DEFAULT_VALUE" AS "default", "IS_NULLABLE" as "notRequired", NULL as "isPrimaryKey", NULL as "isUnique", NULL as "isAutoIncrement" FROM "PUBLIC"."';
			query += 'VIEW_COLUMNS" WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "VIEW_NAME" = \'' + name + '\'';
		} else {
			query =
				'SELECT TB."COMMENTS" AS "comment", TB."COLUMN_NAME" AS "columnName", TB."DATA_TYPE_NAME" AS "dataType", TB."LENGTH" AS "size", TB."SCALE" AS "precision",  TB."DEFAULT_VALUE" AS "default",  TB."IS_NULLABLE" as "notRequired", C."IS_PRIMARY_KEY" AS "isPrimaryKey", C."IS_UNIQUE_KEY" as "isUnique", MAP(S."SEQUENCE_NAME", NULL, \'FALSE\', \'TRUE\') as "isAutoIncrement", MAP(I."INDEX_NAME", NULL, \'FALSE\', \'TRUE\') as "hasIndex"';
			query +=
				' FROM (SELECT "COLUMN_NAME", "DATA_TYPE_NAME", "LENGTH", "SCALE", "DEFAULT_VALUE", "IS_NULLABLE", "SCHEMA_NAME", "TABLE_NAME", "COMMENTS", CONCAT(TABLE_NAME, CONCAT(\'::\', "COLUMN_NAME")) as "SEQUENCE_NAME"  FROM "PUBLIC"."TABLE_COLUMNS" ';
			query += ' WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "TABLE_NAME" = \'' + name + '\') TB';
			query += ' LEFT JOIN (SELECT "SCHEMA_NAME", "TABLE_NAME", "COLUMN_NAME", "IS_PRIMARY_KEY", "IS_UNIQUE_KEY" FROM "PUBLIC"."CONSTRAINTS") C';
			query += ' ON (C."SCHEMA_NAME" = TB."SCHEMA_NAME" AND C."TABLE_NAME" = TB."TABLE_NAME" AND C."COLUMN_NAME" = TB."COLUMN_NAME")';
			query += ' LEFT JOIN (SELECT "SEQUENCE_NAME", "SCHEMA_NAME" FROM "PUBLIC"."SEQUENCES") S';
			query += ' ON (S."SCHEMA_NAME" = TB."SCHEMA_NAME" AND S."SEQUENCE_NAME" = TB."SEQUENCE_NAME")';
			query += ' LEFT JOIN (SELECT "INDEX_NAME", "SCHEMA_NAME", "TABLE_NAME", "COLUMN_NAME" FROM "PUBLIC"."INDEX_COLUMNS") I';
			query += ' ON (I."SCHEMA_NAME" = TB."SCHEMA_NAME" AND I."TABLE_NAME" = TB."TABLE_NAME" AND I."COLUMN_NAME" = TB."COLUMN_NAME")';
		}
		let hanaToORMMap = resources.getSQLToORMMap();
		let response = connector.execute(query, null, false);
		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.cloneDeep(response.errors);
			throw retVal.errors;
		} else if (!lodash.isEmpty(response.results)) {
			lodash.forEach(response.results, function(value) {
				if (lodash.isString(hanaToORMMap[value.dataType])) {
					options.fields[value.columnName] = {
						dataType: value.dataType,
						type: hanaToORMMap[value.dataType],
						columnName: value.columnName,
						size: value.size,
						comment: value.comment,
						precision: value.precision,
						'default': value.default,
						required: !(value.notRequired === 'TRUE'),
						primaryKey: (lodash.isString(value.isPrimaryKey) && value.isPrimaryKey === 'TRUE'),
						unique: (lodash.isString(value.isUnique) && value.isUnique === 'TRUE'),
						autoIncrement: (lodash.isString(value.isAutoIncrement) && value.isAutoIncrement === 'TRUE')
					};
					if (!options.fields[value.columnName].primaryKey && lodash.isString(value.hasIndex) && value.hasIndex === 'TRUE') {
						options.fields[value.columnName].index = {
							globalIndexOrder: 'ASC',
							type: 'default'
						};
					}
				}
			});
			if (isCalculationView) {
				query =
					'SELECT "PARAMETER_NAME" AS "name", "IS_MANDATORY" AS "isMandatory", "DEFAULT_VALUE" AS "defaultValue" FROM "CS_VIEW_PARAMETERS"';
				query += ' WHERE "SCHEMA_NAME" = \'' + schema + '\' AND "OBJECT_NAME" = \'' + name + '\'';
				response = connector.execute(query, null, false);
				if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.cloneDeep(response.errors);
					throw retVal.errors;
				} else if (!lodash.isEmpty(response.results)) {
					lodash.forEach(response.results, function(inputParameter) {
						options.inputParameters[inputParameter.name] = {
							columnName: inputParameter.name,
							type: 'string',
							isMandatory: inputParameter.isMandatory === 'TRUE',
							defaultValue: inputParameter.defaultValue
						};
					});
				}
			}
		} else {
			retVal.errors.push(resources.generateError('Invalid Model', 'Model doesn\'t exist in the database.'));
			throw retVal;
		}
		retVal.results.definition = options;
		return retVal;
	} catch (e) {
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};