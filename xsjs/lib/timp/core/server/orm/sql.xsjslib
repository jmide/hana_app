$.import('timp.core.server', 'util');
const util = $.timp.core.server.util;

$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

let _self = this;

function AsIs(foo) {
	return foo;
}

function DateOnly(date) {
	if (typeof date === 'undefined' || date === null) {
		return null;
	}
	if (date instanceof Date) {
		return date.toDateString();
	}
	return date;
}

/*
CONSTANT
	Maps the datatype on the DB to the method used to set or extract static values;
	The same method used to insert the static value (e.g setInteger) will be used to get the value (e.g getInteger);
*/
var DATA_TYPES = {
	1: {
		'name': 'TINYINT',
		'js': Number,
		'get': 'Integer',
		'set': 'TinyInt'
	},
	2: {
		'name': 'SMALLINT',
		'js': Number,
		'get': 'Integer',
		'set': 'SmallInt'
	},
	3: {
		'name': 'INTEGER',
		'js': Number,
		'get': 'Integer',
		'set': 'Integer'
	},
	4: {
		'name': 'BIGINT',
		'js': Number,
		'get': 'BigInt',
		'set': 'BigInt'
	},
	5: {
		'name': 'DECIMAL',
		'js': Number,
		'get': 'Decimal',
		'set': 'Decimal',
		'dimension': true,
		'precision': true
	},
	6: {
		'name': 'REAL',
		'js': Number,
		'get': 'Real',
		'set': 'Real'
	},
	7: {
		'name': 'DOUBLE',
		'js': Number,
		'get': 'Double',
		'set': 'Double',
		'dimension': true
	},
	8: {
		'name': 'CHAR',
		'js': String,
		'get': 'NString',
		'set': 'NString',
		'dimension': true
	},
	9: {
		'name': 'VARCHAR',
		'js': String,
		'get': 'NString',
		'set': 'NString',
		'dimension': true
	},
	10: {
		'name': 'NCHAR',
		'js': String,
		'get': 'NString',
		'set': 'NString',
		'dimension': true
	},
	11: {
		'name': 'NVARCHAR',
		'js': String,
		'get': 'NString',
		'set': 'NString',
		'dimension': true
	},
	12: {
		'name': 'BINARY',
		'js': String,
		'get': 'Blob',
		'set': 'Blob'
	}, //Deprecated
	13: {
		'name': 'VARBINARY',
		'js': String,
		'get': 'Blob',
		'set': 'Blob'
	},
	14: {
		'name': 'DATE',
		'js': AsIs,
		'get': 'Date',
		'set': 'Date'
	},
	15: {
		'name': 'TIME',
		'js': AsIs,
		'get': 'Time',
		'set': 'Time'
	},
	16: {
		'name': 'TIMESTAMP',
		'js': AsIs,
		'get': 'Timestamp',
		'set': 'Timestamp'
	},
	25: {
		'name': 'CLOB',
		'js': String,
		'get': 'NClob',
		'set': 'NString'
	},
	26: {
		'name': 'NCLOB',
		'js': String,
		'get': 'NClob',
		'set': 'NClob'
	},
	27: {
		'name': 'BLOB',
		'js': AsIs,
		'get': 'Blob',
		'set': 'Blob'
	}, //Deprecated
	47: {
		'name': 'SMALLDECIMAL',
		'js': String,
		'get': 'Decimal',
		'set': 'Decimal'
	},
	51: {
		'name': 'TEXT',
		'js': String,
		'get': 'Text',
		'set': 'Text'
	},
	52: {
		'name': 'SHORTTEXT',
		'js': String,
		'get': 'Text',
		'set': 'Text'
	}
};
this.DATA_TYPES = DATA_TYPES;

/*
Prints the query received, replacing the values at ?
__translate_stmt__({
	query: pstmt ($.db.PreparedStatement),
	values: [
		{type: $.db.types.TINYINT,  	value: 128},
		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
		...
	]
})
*/
this.__translate_stmt__ = function(obj) {
	var query = 'Undefined SQL Query';
	if (util.isObject(obj) && obj.hasOwnProperty('values') && util.isArray(obj.values) && obj.values.length) {
		var sql = obj.query.split('?');
		query = sql[0];
		for (var i = 1; i < sql.length; i++) {
			if (!obj.values.hasOwnProperty(i - 1) || !obj.values[i - 1].hasOwnProperty('type')) {
				continue;
			}
			if (obj.values[i - 1].type === 16 && typeof obj.values[i - 1].value === 'number') {
				var date = new Date(obj.values[i - 1].value);
				date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' +
					date.getMilliseconds();
				obj.values[i - 1].value = date;
			}
			var value = DATA_TYPES[obj.values[i - 1].type].js(obj.values[i - 1].value);
			if (DATA_TYPES[obj.values[i - 1].type].js === String) {
				value = '\'' + value + '\'';
			} else if (value instanceof Date) {
				value = '\'' + value.toISOString() + '\'';
			}
			query += value + sql[i];
		}
	} else if (util.isObject(obj) && obj.hasOwnProperty('query') && util.isString(obj.query)) {
		query = obj.query;
	}
	return query;
};

/*
Applies all the static values to a prepared statement;
__charge_pstmt__({
	query: pstmt ($.db.PreparedStatement),
	values: [
		{type: $.db.types.TINYINT,  	value: 128},
		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
		...
	]UP
})
*/
function __charge_pstmt__(values) { //(pstmt, values) {
	let response = [];
	for (var i = 0; i < values.length; i++) {
		var value = values[i];
		var type = DATA_TYPES[value.type];
		var fn = 'set' + type.set;
		var pos = i + 1;
		try {
			// If is a numeric column 
			// And is not yet a NaN ( value.value === value.value)
			// And is a NaN ( Strings mainly )
			if (value.value != null && value.type < 8 && value.value === value.value && isNaN(value.value)) {
				throw {
					name: 'Invalid type',
					description: 'Type on column ' + pos + ' should be a number'
				};
			}

			// If is a timestamp column 
			// And it has a numerica value, here we create a date from the numeric value
			if (value.type === 16 && typeof value.value === 'number') {
				var date = new Date(value.value);
				date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' +
					date.getMilliseconds();
				value.value = date;
			}
			// The number and NaN check is for cleaning "null" NaN
			if ((typeof value.value === 'number' && isNaN(value.value)) || value.value === null) {
				// pstmt.setNull(pos);
				response.push(null);
			} else {
				if (value.format && ['TIMESTAMP', 'DATE'].indexOf(type.name) !== -1 && (value.value instanceof Date || (typeof value.value === 'string' && !lodash.isEmpty(value.value)))) {
					value.value =  $.moment.utc(value.value, value.format).toISOString();  //new Date(value.value).toISOString();
				} else {
					value.value = type.js(value.value);
				}
				response.push(value.value);
				// if (value.format) {
				// 	pstmt[fn](pos, value.value, value.format);
				// } else {
				// 	pstmt[fn](pos, value.value);
				// }
			}
        } catch (e) {
            let error = {};
            if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
                let connection;
                // if ($.session.getUsername() === 'JMIDE') {
                //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
                // } else {
                    connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
                // }
				connection.setAutoCommit(1);
				let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
					')';
				let response = connection.executeQuery(query);
				connection.close();
				error = {
					name: response[0].name,
					description: response[0].description,
					'@message': e.message
				};
			} else {
				error = {
					name: 'Failed PrepareStatement',
					description: 'Failed to set value ' + value.value + ' (' + (typeof value.value) +
						') (' + (value.value ? value.value.constructor.name : value.name) + ') using $.db.PreparedStatement.' + fn + '\n' + util.parseError(e)
				};
			}
			throw error;
		}
	}
	return response;
}
this.__charge_pstmt__ = __charge_pstmt__;
/*
Runs whatever sql statement, needs types for every static value on the query;
Returns boolean true/false or throws error;
__runIUD__({
	query: "INSERT/UPDATE/DELETE ... (?, ?, ?)/FIELD = ?, FIELD = ?, FIELD = ?",
	values: [
		{type: $.db.types.TINYINT,  	value: 128},
		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
		...
	]
})
*/
this.__runIUD__ = function(options) {
	let values = [];
	let conn;
	try {
        // if ($.session.getUsername() === 'JMIDE') {
        //     conn = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
        // } else {
            conn = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        // }
		conn.setAutoCommit(1);
		// var pstmt = conn.prepareStatement(options.query);
		if (options.hasOwnProperty('values') && util.isArray(options.values)) {
			// pstmt = __charge_pstmt__(pstmt, options.values);
			values = __charge_pstmt__(options.values);
		}
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0].name,
				description: response[0].description,
				'@query': options.query,
				'@values': options.values
			};
		} else {
			error = {
				name: 'Invalid Query',
				description: util.parseError(options) + '\n' + util.parseError(e),
				'@query': options.query,
				'@values': options.values
			};
		}
		throw error;
	}
	try {
		// pstmt.execute(); //The pstmt returns a false even for successful commits.
		// pstmt.close();
		if (lodash.isNil(values) || !lodash.isArray(values)) {
			values = [];
		}
		values.unshift(options.query);
		conn.executeUpdate.apply(conn, values);
		conn.close();
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0].name,
				description: response[0].description,
				'@query': options.query,
				'@values': options.values
			};
		} else {
			error = {
				name: 'Error on __runIUD__ Query',
				description: util.parseError(e) + '\n' + _self.__translate_stmt__(options),
				'@query': options.query,
				'@values': options.values
			};
		}
		throw error;
	}
	return true;
};
/*
Runs whatever sql statement, needs types for every static value on the query;
Returns boolean true/false or throws error;
    __batchRunIUD__({
    	query: "INSERT/UPDATE/DELETE ... (?, ?, ?)/FIELD = ?, FIELD = ?, FIELD = ?",
    	fields:     [
    		{name: "FIELD", type: $.db.types.TINYINT},
    		{name: "FIELD", type: $.db.types.TIMESTAMP,	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{name: "FIELD", type: $.db.types.DATE, 		format: "dd/mm/yyyy"},
    		{name: "FIELD", type: $.db.types.TIME, 		format: "HH:MI:SS.FF AM"},
    		...
    	],
    	values: 	[
    		[128, "01.02.2003 01:02:03.123", "25/12/2009", "09:57:57.99 PM"],
    		...,
    		[128, "01.02.2003 01:02:03.123", "25/12/2009", "09:57:57.99 PM"]
    	]
    })
*/
this.__batchRunIUD__ = function(options) {
	let values = [];
	let conn;
	let response = [];
	options.values = options.values || [];
    try {
        // if ($.session.getUsername() === 'JMIDE') {
        //     conn = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
        // } else {
            conn = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        // }
		conn.setAutoCommit(1);
		// var pstmt = conn.prepareStatement(options.query);
		if (options.hasOwnProperty('values') && util.isArray(options.values) && options.hasOwnProperty('fields') && util.isArray(options.fields)) {
			// pstmt.setBatchSize(options.values.length);
			for (var x = 0; x < options.values.length; x++) {
				let actualValues = [];
				for (var y = 0; y < options.fields.length; y++) {
					options.fields[y].value = options.values[x][y];
				}
				// pstmt = __charge_pstmt__(pstmt, options.fields);
				actualValues = __charge_pstmt__(options.fields);
				values.push(actualValues);
				// try { //To workaround the unknown error: Error: PreparedStatement.addBatch: bulk insert not enabled
				// 	pstmt.addBatch();
				// } catch (e) {
				// 	break;
				// }
			}
		}
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0].name,
				description: response[0].description,
				'@query': options.query,
				'@values': options.values
			};
		} else {
			error = {
				name: 'Invalid query',
				description: util.parseError(options) + '\n' + util.parseError(e),
				'@query': options.query,
				'@values': options.values
			};
		}
		throw error;
	}
	try {
		// pstmt.executeBatch(); //The pstmt returns a false even for successful commits.
		// pstmt.close();
		// conn.commit();
		if (lodash.isNil(values) || !lodash.isArray(values)) {
			values = [];
		} else {
			values = [values];
		}
        if (values.length > 0 && values[0].length) {
            values.unshift(options.query);
            response = conn.executeUpdate.apply(conn, values);
        }
		conn.close();
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0].name,
				description: response[0].description,
				'@query': options.query,
				'@values': options.values
			};
		} else {
			error = {
				name: 'Error on __batchRunIUD__',
				description: util.parseError(e) + '\n' + _self.__translate_stmt__(options) + '\nValues: ' + options.values,
				'@query': options.query,
				'@values': options.values
			};
		}
		throw error;
	}
	return options.getResultQuery ? response : true;
};
/*
Runs whatever DDL statement, needs types for every static value on the query;
Returns boolean true/false or throws error;
__runIUD__({
	query: "ALTER TABLE/CREATE TABLE/DROP TABLE ... WHERE \"TABLE\".\"FOO\" = ?",
	values: [
		{type: $.db.types.TINYINT,  	value: 128},
		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
		...
	]
})
*/
function __runUpdate__(options) {
	try {
        let values = [];
        var conn;
        // if ($.session.getUsername() === 'JMIDE') {
        //     conn = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
        // } else {
            conn = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        // }
		conn.setAutoCommit(1);
		// var pstmt = conn.prepareStatement(options.query);
		if (options.hasOwnProperty('values') && util.isArray(options.values)) {
			// pstmt = __charge_pstmt__(pstmt, options.values);
			values = __charge_pstmt__(options.values);
		}
		// pstmt.executeUpdate(); //The pstmt returns a false even for successful commits.
		// pstmt.close();
		if (lodash.isNil(values) || !lodash.isArray(values)) {
			values = [];
		}
		values.unshift(options.query);
		conn.executeUpdate.apply(conn, values);
		conn.close();
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0].name,
				description: response[0].description,
				'@query': options.query,
				'@values': options.values
			};
		} else {
			error = {
				name: 'Invalid query',
				description: util.parseError(options) + '\n' + util.parseError(e),
				'@query': options.query,
				'@values': options.values
			};
		}
		throw error;
	}
	return true;
}
this.__runUpdate__ = __runUpdate__;
/*
Runs whatever SELECT statement, needs types for every static value on the query;
Returns a list of arrays or throws error;
__runIUD__({
	query: "SELECT ... FROM SCHEMA.TABLE [JOIN ...] WHERE FIELD = ? AND FIELD = ?",
	values: [
		{type: $.db.types.TINYINT,  	value: 128},
		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
		...
	]
})
*/

function __runSelect__(options) {
	let values = [];
	let resultSet = [];
	let conn;
	let timing = {};
	let initTime = (new Date()).getTime();
	let lastTime = (new Date()).getTime();
	util.debug('__runSelect__', options);
	options.values = options.values || [];
    try {
        // if ($.session.getUsername() === 'JMIDE') {
        //     conn = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
        // } else {
            conn = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            timing['1. getConnetion'] = (new Date()).getTime() - lastTime;
            lastTime = (new Date()).getTime();
        // }
        conn.setAutoCommit(1);
        timing['2. autocommit'] = (new Date()).getTime() - lastTime;
        lastTime = (new Date()).getTime();
		// var pstmt;
		// pstmt = conn.prepareStatement(options.query);
		if (options.hasOwnProperty('values') && util.isArray(options.values)) {
			// pstmt = __charge_pstmt__(pstmt, options.values);
			values = __charge_pstmt__(options.values);
		}
		timing['3. PrepareValues'] = (new Date()).getTime() - lastTime;
		lastTime = (new Date()).getTime();
		// resultSet = pstmt.executeQuery();
		if (lodash.isNil(values) || !lodash.isArray(values)) {
			values = [];
		}
		if($.getTraceActivity()){
            const optionsTrace = {
                query: 'INSERT INTO "' + $.schema.slice(1, -1) + '"."CORE::LOG_TRACER" VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
                values:[
                    options.query,
                    $.moment().format('MMMM Do YYYY, h:mm:ss a'),
                    'sql.__runSelect__',
                    $.getUserId(),
                    $.session.getUsername(),
                    $.session.getSecurityToken(),
                    'CORE ORM1',
                    0,
                    'Chamando ao Hana',
                    0,
                    '',
                    null
                ]
            };
            _self.executeTraceBindParamiters(optionsTrace);
        }
		values.unshift(options.query);
		resultSet = conn.executeQuery.apply(conn, values);
		timing['4. Execute Query'] = (new Date()).getTime() - lastTime;
		lastTime = (new Date()).getTime();
		conn.close();
        if($.getTraceActivity()){
            const optionsTrace = {
                query: 'INSERT INTO "' + $.schema.slice(1, -1) + '"."CORE::LOG_TRACER" VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
                values:[
                    options.query,
                    $.moment().format('MMMM Do YYYY, h:mm:ss a'),
                    'sql.__runSelect__',
                    $.getUserId(),
                    $.session.getUsername(),
                    $.session.getSecurityToken(),
                    'CORE ORM1',
                    0,
                    'Recibi resposta do Hana',
                    0,
                    '',
                    null
                ]
            };
            _self.executeTraceBindParamiters(optionsTrace);
        }
    } catch (e) {
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
			connection.setAutoCommit(1);
			let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
				')';
			let response = connection.executeQuery(query);
			connection.close();
			error = {
				name: response[0] ? response[0].name : e.code,
				description: response[0] ? response[0].description : 'No Description',
				'@query': options.query
			};
		} else {
			error = {
				name: 'Failed __runSelect__',
				description: [util.parseError(e), util.parseError(options), _self.__translate_stmt__(options)].join('\n\t')
			};
		}
		throw error;
	}
	// var meta = resultSet.getMetaData();
	// var ncols = meta.getColumnCount();
	var meta = resultSet.metadata;
	var cols = meta.columns;
	var p = 0;
	var info = [];

    for (p = 0; p < cols.length; p++) {
        let label = cols[p].label;
        if (label && lodash.isFunction(label.indexOf) && label.indexOf('TMP_FIELD_') !== -1 && lodash.isFunction(label.split)) {
            label = label.split('_');
            label = label.slice(2, label.length - 1);
            label = label.join('_');
        }
        info.push({
            type: cols[p].type,
            name: label
        });
    }
    timing['5. Prepare Columns'] = (new Date()).getTime() - lastTime;
    lastTime = (new Date()).getTime();
    var data = [];
    let value;
    try {
        let iterator = resultSet.getIterator();
        while (iterator.next()) {
            let instance = iterator.value();
            let line = [];
            // let j = -1;
            for (var j = 0; j < instance.length; j++) {
            // lodash.forEach(instance, function(val, key) {
                // j++;
                let val = instance[j];
                let colInfo = info[j];
                value = val;
                if (val instanceof ArrayBuffer) {
                    try {
                        value = $.util.stringify(val);
                    } catch (er) {
                        value = val;
                    }
                }
                if (colInfo.type === 4) {
                    if (val) {
                        value = val.toString();
                    } else {
                        value = null;
                    }
                } else if (colInfo.type === 14) {
                    value = DateOnly(val);
                } else if (!lodash.isNil(val) && ((colInfo.type < 8 && colInfo.type > 0) || colInfo.type === 47)) {
                    value = Number(val);
                }
                if (value instanceof Date) {
                    if (isNaN(value.getTime()) || value.getTime() < 0) {
                        value = null;
                    }
                }
                line.push(value);
            }
            // });
            data.push(line);
        }
        timing['6. Process Data'] = (new Date()).getTime() - lastTime;
        lastTime = (new Date()).getTime();
        if($.getTraceActivity()){
            const optionsTrace = {
                query: 'INSERT INTO "' + $.schema.slice(1, -1) + '"."CORE::LOG_TRACER" VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
                values:[
                    options.query,
                    $.moment().format('MMMM Do YYYY, h:mm:ss a'),
                    'sql.__runSelect__',
                    $.getUserId(),
                    $.session.getUsername(),
                    $.session.getSecurityToken(),
                    'CORE ORM1',
                    data.length,
                    'Convertí ao JSON',
                    0,
                    '',
                    null
                ]
            };
            _self.executeTraceBindParamiters(optionsTrace);
        }
    } catch (e) {
        let error;
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            let connection;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            connection.setAutoCommit(1);
            let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code +
                ')';
            let response = connection.executeQuery(query);
            connection.close();
            error = {
                name: response[0] ? response[0].name : e.code,
                description: response[0] ? response[0].description : 'NO DESCRIPTION',
                '@query': options.query,
                '@values': options.values
            };
        } else {
            error = {
                name: 'Invalid PreparedStatement',
                description: 'PreparedStatement: Failed to get value ' + value + ' using $.db.PreparedStatement' +
                    '\n' + util.parseError(e),
                '@query': options.query,
                '@values': options.values
            };
        }
        throw error;
    }
    data.Metadata = info;
    // pstmt.close();
    // conn.commit();
    // conn.close();
    timing['7. Return Data'] = (new Date()).getTime() - lastTime;
    timing['8. Total Data'] = (new Date()).getTime() - initTime;
    data.timing = timing;
    data.initTime = initTime;
    return data;
}
this.__runSelect__ = __runSelect__;

/*@doc
Functionality:
    SELECT 
    	SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD, SUM(SCHEMA.TABLE.FIELD)
    FROM 
    	SCHEMA.TABLE JOIN SCHEMA.TABLE ON SCHEMA.TABLE.FIELD = SCHEMA.TABLE.FIELD
    WHERE
    	(SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL
    GROUP BY
    	SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD
    ORDER BY
    	SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD
Usage:
    SELECT({
    	query:		"SELECT SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD, SUM(SCHEMA.TABLE.FIELD) FROM SCHEMA.TABLE JOIN SCHEMA.TABLE ON SCHEMA.TABLE.FIELD = SCHEMA.TABLE.FIELD WHERE (SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL GROUP BY	SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD ORDER BY	SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD",
    	values:		[
    		{type: $.db.types.TINYINT,  	value: 128},
    		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
    		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
    		...
    	]
    })
*/
function SELECT(options) {
	if (!util.isObject(options)) {
		throw {
			name: 'Error on ORM.SELECT',
			description: 'Undefined options\nReceived: ' + options
		};
	}
	util.debug('SELECT', options);
	try {
		return __runSelect__({
			query: options.query,
			values: options.values || []
		});
	} catch (e) {
		throw e;
	}
}
this.SELECT = SELECT;

/*@doc
Functionality:
    DELETE FROM
    	SCHEMA.TABLE
    WHERE
    	(SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL
Usage:
    DELETE({
    	query:		"DELETE FROM SCHEMA.TABLE WHERE (SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL",
    	values:		[
    		{type: $.db.types.TINYINT,  	value: 128},
    		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
    		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
    		...
    	]
    })
*/
function DELETE(options) {
	util.debug('DELETE', options);
	//try {
	return this.__runIUD__({
		query: options.query,
		values: options.values
	});
	// } catch (e) {
	// 	throw "Failed to execute DELETE statement.\n\t"+e;
	// }
}
this.DELETE = DELETE;

/*@doc
Functionality:
    UPDATE
    	SCHEMA.TABLE
    SET
    	SCHEMA.TABLE.FIELD = ?,
    	SCHEMA.TABLE.FIELD = ?,
    	SCHEMA.TABLE.FIELD = ?
    WHERE
    	(SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL
Usage:
    UPDATE({
    	query:		"UPDATE SCHEMA.TABLE SET SCHEMA.TABLE.FIELD = ?, SCHEMA.TABLE.FIELD = ?, SCHEMA.TABLE.FIELD = ? WHERE (SCHEMA.TABLE.FIELD = ? OR SCHEMA.TABLE.FIELD = ?) AND SCHEMA.TABLE.FIELD IS NOT NULL",
    	values:		[
    		{type: $.db.types.TINYINT,  	value: 128},
    		{type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
    		{type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
    		...
    	],
    })
*/
function UPDATE(options) {
	util.debug('UPDATE', options);
	//try {
	return this.__runIUD__({
		query: options.query,
		values: options.values
	});
	// } catch (e) {
	// 	throw "Failed to execute UPDATE statement.\n\t"+e;
	// }
}
this.UPDATE = UPDATE;

/*@doc
Functionality:
    INSERT INTO
    	SCHEMA.TABLE
    	(FIELD, FIELD, FIELD)
    VALUES
    	(?, ?, ?)
Usage:
    INSERT({
    	table:		"SCHEMA.TABLE",
    	fields: 	[
    		{name: "FIELD", type: $.db.types.TINYINT,  	value: 128},
    		{name: "FIELD", type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{name: "FIELD", type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
    		{name: "FIELD", type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
    		...
    	]
    })
*/
function INSERT(options) {
	util.debug('INSERT', options);

	var query = 'INSERT INTO ';
	query += options.table;
	var placeholders = [];
	var fields = [];
	for (var i = 0; i < options.fields.length; i++) {
		fields.push(options.fields[i].name);
		placeholders.push('?');
	}
	query += ' (' + fields.join(', ') + ') ';
	query += 'VALUES';
	query += ' (' + placeholders.join(', ') + ') ';

	return this.__runIUD__({
		query: query,
		values: options.fields
	});
}
this.INSERT = INSERT;

/*@doc
Functionality:
    UPSERT
    	SCHEMA.TABLE
    	(FIELD, FIELD, FIELD)
    VALUES
    	(?, ?, ?)
Usage:
    UPSERT({
    	table:		"SCHEMA.TABLE",
    	usePK: true | false //this is for the WITH PRIMARY CLAUSE
    	fields: 	[
    		{name: "FIELD", type: $.db.types.TINYINT,  	value: 128},
    		{name: "FIELD", type: $.db.types.TIMESTAMP,	value: "01.02.2003 01:02:03.123",	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{name: "FIELD", type: $.db.types.DATE, 		value: "25/12/2009",				format: "dd/mm/yyyy"},
    		{name: "FIELD", type: $.db.types.TIME, 		value: "09:57:57.99 PM",			format: "HH:MI:SS.FF AM"},
    		...
    	]
    })
*/
function UPSERT(options) {
	util.debug('UPSERT', options);

	var query = 'UPSERT ';
	query += options.table;
	var placeholders = [];
	var fields = [];
	for (var i = 0; i < options.fields.length; i++) {
		fields.push(options.fields[i].name);
		placeholders.push('?');
	}
	query += ' (' + fields.join(', ') + ') ';
	query += 'VALUES';
	query += ' (' + placeholders.join(', ') + ') ';
	if (options.hasOwnProperty('usePK') && options.usePK) {
		query += 'WITH PRIMARY KEY ';
	}

	//try {
	return this.__runIUD__({
		query: query,
		values: options.fields
	});
	// } catch (e) {
	// 	throw "Failed to execute INSERT statement.\n\t"+e;
	// }
}
this.UPSERT = UPSERT;

/*@doc
Functionality:
    INSERT INTO
    	SCHEMA.TABLE
    	(FIELD, FIELD, FIELD)
    VALUES
    	(?, ?, ?)
Usage:
    BATCH_INSERT({
    	table:		"SCHEMA.TABLE",
    	fields:     [
    	    {name: "FIELD", type: $.db.types.TINYINT, value: 'SEQUENCE.nextval', literal: true}, //Will not be counted for values, will be concatenated on the statement
    		{name: "FIELD", type: $.db.types.TINYINT},
    		{name: "FIELD", type: $.db.types.TIMESTAMP,	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{name: "FIELD", type: $.db.types.DATE, 		format: "dd/mm/yyyy"},
    		{name: "FIELD", type: $.db.types.TIME, 		format: "HH:MI:SS.FF AM"},
    		...
    	],
    	values: 	[
    		[128, "01.02.2003 01:02:03.123", "25/12/2009", "09:57:57.99 PM"],
    		...
    	]
    })
*/
function BATCH_INSERT(options) {
	util.debug('INSERT', options);
	var fields;
	if (!util.isArray(options.fields)) {
		fields = [];
		lodash.forEach(options.fields, function(item) {
			fields.push(item);
		});
		options.fields = fields;
	}

	if (!options.fields.length) {
		throw {
			name: 'Empty Fields',
			description: 'options.fields is an empty array'
		};
	}

	var query = 'INSERT INTO ';
	query += options.table;
	var placeholders = [];
	fields = [];
	var vFields = []; //Fields for which we have values;
	for (var i = 0; i < options.fields.length; i++) {
		var field = options.fields[i];
		if (field.literal && field.value) {
			placeholders.push(field.value);
		} else {
			placeholders.push('?');
			vFields.push(field);
		}
		fields.push(field.name);
	}
	query += ' (' + fields.join(', ') + ') ';
	query += 'VALUES';
	query += ' (' + placeholders.join(', ') + ') ';

	//try {
	return this.__batchRunIUD__({
		query: query,
		fields: vFields,
		values: options.values,
		getResultQuery: options.getResultQuery || false
	});
	// } catch (e) {
	// 	throw "Failed to execute INSERT statement.\n\t"+e;
	// }
}
this.BATCH_INSERT = BATCH_INSERT;

/*@doc
Functionality:
    INSERT INTO
    	SCHEMA.TABLE
    	(FIELD, FIELD, FIELD)
    VALUES
    	(?, ?, ?)
Usage:
    BATCH_UPSERT({
    	table:		"SCHEMA.TABLE",
    	fields:     [
    	    {name: "FIELD", type: $.db.types.TINYINT, value: 'SEQUENCE.nextval', literal: true}, //Will not be counted for values, will be concatenated on the statement
    		{name: "FIELD", type: $.db.types.TINYINT},
    		{name: "FIELD", type: $.db.types.TIMESTAMP,	format: "DD.MM.YYYY HH:MI:SS.FF"},
    		{name: "FIELD", type: $.db.types.DATE, 		format: "dd/mm/yyyy"},
    		{name: "FIELD", type: $.db.types.TIME, 		format: "HH:MI:SS.FF AM"},
    		...
    	],
    	values: 	[
    		[128, "01.02.2003 01:02:03.123", "25/12/2009", "09:57:57.99 PM"],
    		...
    	]
    })
*/
function BATCH_UPSERT(options) {
	util.debug('UPSERT', options);
	var fields;
	if (!util.isArray(options.fields)) {
		fields = [];
		lodash.forEach(options.fields, function(item) {
			fields.push(item);
		});
		options.fields = fields;
	}

	if (!options.fields.length) {
		throw {
			name: 'Empty Fields',
			description: 'options.fields is an empty array'
		};
	}

	var query = 'UPSERT ';
	query += options.table;
	var placeholders = [];
	fields = [];
	var vFields = []; //Fields for which we have values;
	for (var i = 0; i < options.fields.length; i++) {
		var field = options.fields[i];
		if (field.literal && field.value) {
			placeholders.push(field.value);
		} else {
			placeholders.push('?');
			vFields.push(field);
		}
		fields.push(field.name);
	}
	query += ' (' + fields.join(', ') + ') ';
	query += 'VALUES';
	query += ' (' + placeholders.join(', ') + ') ';
	query += 'WITH PRIMARY KEY';

	//try {
	return this.__batchRunIUD__({
		query: query,
		fields: vFields,
		values: options.values
	});
	// } catch (e) {
	// 	throw "Failed to execute INSERT statement.\n\t"+e;
	// }
}
this.BATCH_UPSERT = BATCH_UPSERT;

/*@doc
Functionality:
    SELECT
    	SOMETHING
    FROM
    	DUMMY;
Usage:    
    DUMMY({
    	select: '"SCHEMA"."MY_SEQ".nextval'
    });
    
    DUMMY({
    	select: 'NOW()'
    });
    
WARNING: NOT PROTECTED AGAINST SQL INJECTION, DON'T USE IT DIRECTLY WITH PARAMETERS FROM CLIENT SIDE
*/
function DUMMY(options) {
	if (!util.isString(options.select)) {
		return false;
	}
	var query = 'SELECT ' + options.select + ' FROM DUMMY';
	try {
		return __runSelect__({
			query: query
		});
	} catch (e) {
		throw e;
	}
}
this.DUMMY = DUMMY;

/*@doc
Functionality:
    EXECUTE({
    	query: "ANY QUERY THAT RETURNS A BOOLEAN AND DOES NOT REQUIRE STATIC PARAMETERS"
    })
*/
function EXECUTE(options) {
	if (util.isString(options.query)) {
		return this.__runIUD__({
			query: options.query
		});
	}
	return false;
}
this.EXECUTE = EXECUTE;

this.executeTraceBindParamiters = function(options){
    try{
        const connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        connection.executeQuery(options.query, options.values[0], options.values[1], options.values[2], options.values[3], options.values[4], options.values[5], options.values[6], options.values[7], options.values[8], options.values[9], options.values[10],options.values[11]);
        connection.commit();
        connection.close();
    } catch(e){
        const a = e;
    }
};