$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm', 'sql');
var SQL = $.timp.core.server.orm.sql;

$.import('timp.core.server.orm.table', 'fields');
var fieldsLib = $.timp.core.server.orm.table.fields;

$.import('timp.core.server.models', 'schema');
var schema = $.timp.core.server.models.schema;

var _ = {};
let fieldsLibKeys = Object.keys(fieldsLib);
for (let i = 0; i < fieldsLibKeys.length; i++) {
    const key = fieldsLibKeys[i];
    _[key] = this[key] = fieldsLib[key];
}


/*@doc
Example:
    new table.Table({
        name: '"TIMP"."BRB::Report"',
        base:
            'table' (default),
            'calculationview'
            'analyticview'
            'attributeview'
        versioning (optional):
            'simple' //Just create a versions table to make rollback work
            'approval' //Create the versions table to make rollback, makes the inactive versions for the approval cycle
            'daterange' //Keep multiple versions active, will require a current_date when reading
        defaultFields: 'common', //CREATION.DATE CREATION.ID_USER MODIFICATION.DATA MODIFICATION.ID_USER
        object: 2,
        fields: {
            id (required): new table.AutoField({ //If you don't set it on the
                name: 'ID',
                auto: '"TIMP"."BRB::Report::ID".nextval', //No one uses the following format anymore: '"TIMP"."timp.brb.server.install.sequences::report_id".nextval',
                type: $.db.types.INTEGER,
                pk: true
            }),
            label: new table.Field({
                name: 'isDeleted',
                type: $.db.types.TINYINT,
                translate: {1: true, 2: false}
            }),
            label: new table.Field({
                name: 'FIELDNAME',
                type: $.db.types.TIMESTAMP,
                format (optional): 'DD.MM.YYYY HH:MI:SS.FF'
            }),
            label: new table.Field({
                name: 'FIELDNAME',
                type: $.db.types.NCLOB,
                json: true //By adding the property "json" and setting it's value to "true" the ORM will parse automatically the JSON when using Table.READ
            })
        }
    })
*/
function Table(options) {
    if (util.isFunction(Table._validate)) {
        Table._validate(arguments);
    }
    if (this instanceof Table) {
        return this.__init__(options);
    } else {
        return new Table(options);
    }
}


Table.prototype = {
    __init__: function (options) {
        var field;
        this.name = options.name;
        this.fields = {};
        this.hints = options.hints || [];
        this.table = this;
        this.instanceOf = 'Table'; //Dirty fix for messy imports
        // this.component = options.component;
        this.idComponent = null;
        this.versioning = options.versioning || false;
        this.base = options.base || 'table';
        this.object = false;
        this.sql = SQL;
        if (options.hasOwnProperty('tags')) {
            this.tags = options.tags;
        }
        if (options.hasOwnProperty('object')) {
            // this.idComponent = SQL.SELECT({
            //     query: 'SELECT ID FROM "TIMP"."CORE::Components"'
            // })[0][0];
            this.object = options.object;
            $.import('timp.core.server.api', 'api');
            var coreApi = $.timp.core.server.api.api;
            this.objects = coreApi.objects;
        }
        if ((options.hasOwnProperty('default_fields') && options.default_fields === 'common') || (options.hasOwnProperty('defaultFields') && options.defaultFields === 'common')) {
            this.defaultFields = 'common';
            //Default
            options.fields.creationDate = new _.AutoField({
                name: 'CREATION.DATE',
                auto: 'NOW()',
                update: false,
                type: $.db.types.TIMESTAMP
            });
            options.fields.creationUser = new _.AutoField({
                name: 'CREATION.ID_USER',
                auto: '(SELECT "ID" FROM ' + schema.default + '."CORE::USER" WHERE "HANA_USER" = SESSION_USER)',
                type: $.db.types.INTEGER,
                update: false
            });
            options.fields.modificationDate = new _.AutoField({
                name: 'MODIFICATION.DATE',
                auto: 'NOW()',
                update: true,
                type: $.db.types.TIMESTAMP
            });
            options.fields.modificationUser = new _.AutoField({
                name: 'MODIFICATION.ID_USER',
                auto: '(SELECT "ID" FROM ' + schema.default + '."CORE::USER" WHERE "HANA_USER" = SESSION_USER)',
                update: true,
                type: $.db.types.INTEGER
            });
        }
        if (options.hasOwnProperty('versioning') && options.versioning === 'simple') {
            var newFields = {};
            newFields.id = options.fields.id;
            if (options.versioning === 'simple') {
                newFields.obj_version = new _.Field({
                    name: 'OBJ_VERSION',
                    type: $.db.types.INTEGER,
                    pk: true
                });
                newFields.obj_status = new _.Field({
                    name: 'OBJ_STATUS',
                    type: $.db.types.TINYINT,
                    translate: {
                        1: 'Active',
                        2: 'Obsolete',
                        3: 'Locked',
                        4: 'Trashed',
                        5: 'Deleted'
                    }
                });
                // }else if(options.versioning === 'approval'){
            }
            this.fieldKeys = Object.keys(options.fields);
            for (let i = 0; i < this.fieldKeys.length; i++) {
                const fieldKey = this.fieldKeys[i];
                field = options.fields[fieldKey];
                if (!newFields.hasOwnProperty(fieldKey)) {
                    newFields[fieldKey] = field;
                }
            }
            // for (var fieldKey in options.fields) {
            //     field = options.fields[fieldKey];
            //     if (!newFields.hasOwnProperty(fieldKey)) {
            //         newFields[fieldKey] = field;
            //     }
            // }
            options.fields = newFields;
        }
        // if (options.hasOwnProperty('versioning') && (['approval', 'simple'].indexOf(options.versioning) !== -1)) {
        //     //Process name
        //         //Get the rootname
        //     var name = options.name.match(/::(\w+)/);
        //     if (!name || !name.length) {
        //         throw 'The table name for versioning depends on the format ' + schema.default +  '."COMPONENT::TABLENAME"'
        //     } else {
        //         name = name[1];
        //     }
        //     //Add all original fields
        //     var hist_fields = {};
        //     for (var y in options.fields) {
        //         if (options.fields.hasOwnProperty(y)) {
        //             var def = options.fields[y].definition;
        //             if (options.fields[y].instanceOf == 'Field') {
        //                 hist_fields[y] = new _.Field(def)
        //             } else if (options.fields[y].instanceOf == 'AutoField') {
        //                 def.auto = (def.auto + '')
        //                 if (y == 'id') {
        //                     def.auto = def.auto.replace(name, name + '::Versions') //Replace the name of the ID sequence
        //                 }
        //                 hist_fields[y] = new _.AutoField(def);
        //             }
        //             // var field = options.fields[y];
        //             // hist_fields[y] = field;
        //         }
        //     }
        //     //Add the reference field
        //     hist_fields['id_original_obj'] = new _.Field({
        //         name: 'ID_ORIGINAL_OBJ',
        //         type: $.db.types.INTEGER,
        //         pk: true
        //     })
        //     //Add the version count
        //     hist_fields['curr_version'] = new _.Field({
        //         name: 'CURRENT_VERSION',
        //         type: $.db.types.INTEGER
        //     })
        //     if (this.versioning == 'approval') {
        //         //Keep record of the action intended CREATE UPDATE DELETE
        //         hist_fields['versioning_action'] = new _.Field({
        //             name: 'VERSIONING_ACTION',
        //             translate: {0:'DONE', 1:'CREATE', 2:'UPDATE', 3:'DELETE', 4: 'SKIP CREATE', 5: 'SKIP UPDATE', 6: 'SKIP DELETE', 7: 'REJECT CREATE', 8: 'REJECT UPDATE', 9: 'REJECT DELETE'},
        //             default: 0,
        //             type: $.db.types.TINYINT
        //         });
        //     }
        //     //Generate table
        //     this.historical = new Table({
        //         name: options.name.replace(name, name + "::Versions"),
        //         fields: hist_fields
        //     });
        // } else {
        //     this.historical = false;
        // }
        let fieldKeys = Object.keys(options.fields);
        for (let i = 0; i < fieldKeys.length; i++) {
            const key = fieldKeys[i];
            // if (options.fields.hasOwnProperty(key)) {
            field = options.fields[key];
            if (util.isValid(field) && (field.instanceOf == 'Field' || field.instanceOf == 'AutoField')) {
                this.__parseField__(key, field);
            }
            // }
        }
        this.fieldKeys = fieldKeys || [];
        // for (var key in options.fields) {
        //     if (options.fields.hasOwnProperty(key)) {
        //         field = options.fields[key];
        //         if (util.isValid(field) && (field.instanceOf == 'Field' || field.instanceOf == 'AutoField')) {
        //             this.__parseField__(key, field);
        //         }
        //     }
        // }
    },
    __parseField__: function (label, field) {
        this.fields[label] = field;
        field.table = this;
        if (field.type === $.db.types.CLOB) {
            this.clob = true;
        }
    }
};


this.Table = util.Declare({
    options: {
        name: util.isString //,
        // component: util.isString,
        // fields: {
        //     id: util.isObject
        // }
    }
}, Table);


/*@doc
Usage:
    Table.CREATE({
        label: value,
        ...
        label: value,
        label: value
    });
Returns: 
    true  for success
    false for failure
May throw an error
*/
Table.prototype.CREATE = function (options) { //, config) {
    // if($.session.getUsername() === 'JMIDE')
    //     throw 'options';
    var filled = this.__parseCreate__(options);
    var values = filled.values;
    var createFields = filled.create_fields;
    // response = filled.response,
    var status;
    if (this.historical && this.versioning === 'approval') {
        //Does not create the object
        //Does not insert on the main table
        values.id_original_obj = values.id;
        values.curr_version = 0;
        values.versioning_action = 'CREATE';
        this.__skipUnactivatedAction__(values.id);
        status = this.historical.CREATE(values);
    } else {
        status = this.sql.INSERT({
            table: this.name,
            fields: createFields
        });
        if (this.historical) {
            values.id_original_obj = values.id;
            values.curr_version = 0;
            this.historical.CREATE(values);
        }
        if (this.object) {
            this.objects.CREATE({
                name: values.name,
                idComponent: this.idComponent,
                id_object: values.id,
                id_objectType: 2
                //id_folder: null
            });
        }
    }
    if (status) {
        return values;
    } else {
        return status;
    }
};


/*@doc
Behaviour:
    If you don't define 'fields' it will use all table fields.
        It silently skips any field you specified but does not exist on the table.
        It returns false if no field of the specified was found on the table.
    If you don't define 'where' it will bring all existing records.
        If you define 'where' mind this: 
            Level 0: AND
            Level 1: OR
            Level 3: AND
            Level 4: OR
            ...
        What does it mean?
            [A, B, C] => A AND B AND C
            [[A, B, C]] => (A OR B OR C)
            [[A, B], C] => (A OR B) AND C
            [[[A, B], C]] => ((A AND B) OR C)
    If you specify 'orderBy' but not 'descending' it will default to ascending.
        If you want it to be descending, put a minus sign before the fieldname
    If you specify 'group_by' it will overwrite 'fields'.
Usage:
    Table.READ({
        top (optional): 10,
        count (optional): true, //Returns only the row count (if you use the group by it may have more than one result)
        distinct (optional) : true|false (default false),
        fields (optional): ['label', 'label', 'label', 'label'],
        where (optional): [
            {field: 'label', oper: '=', value: 12},
            {field: 'label', oper: '=', value: 12, not: true}, //Same as !=
            ...
            {field: 'label', oper: 'LIKE', value: '%foo%'}
            {field: 'label', oper: 'IS NULL'},
            {field: 'label', oper: 'IS NULL', not: true}, //Same as NOT FIELD IS NULL 
            {field: 'label', oper: '<', value: 'NOW()', 'literal':true},
            {field: 'label', oper: 'LIKE', value: '%foo%', maskFn: 'UPPER'}
            'FOO > 12',
            {field: label3, oper: 'IS NULL', table: <foo>}
        ],
        group_by (optional): ['label', 'label', 'label'],
        order_by (optional): ['label', '-label', 'label'],
        descending (optional): true,
        tags (optional): {
            empresa (optional): 'ANY',
            estado (optional):  ['SP', 'RJ', 'MG'],
            filial (optional):  ['001', '002', '003'],
            imposto (optional): ['ICMS', 'IPI', 'PIS/COFINS']
        },
        join (optional): [{
            table: structures,
            alias: 'structures',
            fields (optional): ['id', 'name'],
            on: [{left_table (optional): linkTable , left: 'idStructure', right: 'id', oper (optional, default is '='): '='}, {field: 'label', oper: '=', value: 12, table (optional, default is the table specified on the join): <table>}], //This accepts the same as the where
            outer (optional): 'right' //3 options: right, both, left
        },{
            table: linkTable,
            alias: 'linkTable',
            fields: [], //An empty array tells that you need that table on the join but not on the select, it may be the case for link tables on N to N relationships
            on: [{left: 'id', right: 'idTable'}],
        },{
            table: structures,
            rename: 'A', //If you need to join the same table twice, you will need to rename it
            alias: 'structures',
            on: [{left_table (optional): linkTable , left: 'idStructure', right: 'idExtended'}],
        }],
        idField (optional with join): 'fieldName' //if you want to index by something else than the id
        indexBy (optional with join): 'tableName/alias' //if you want to index by a table from the join statement
        mode (optional): 
            'raw' //Skips the aggregation fase but names the fields
            'indexed' //Normal,
            'flat' //Like indexed, but only one level
        paginate (optional): { //Returns 'pageCount' with the total of pages
            size: 100, //What is the page size?
            number: 1, //What is the page number?
            count: false //Do you want a total of pages?
        },
        inactive (optional): 
            'include', //If you set 'versioning':'approval' on the table definiton, this will show the user it's inactive versions together with the active ones
            'replace', //NOT IMPLEMENTED - If you set 'versioning':'approval' on the table definiton, this will show the user it's inactive versions instead of the actives
        simulate (optional): false, //Returns the query string, processed with the values
        safe: true //Prevents from using literals on where conditions
    });

Returns: 
    [ 
        {label: value, label: value, label: value},
        ...,
        {label: value, label: value, label: value}
    ]
*/
Table.prototype.READ = function (options) {
    var query = 'SELECT ';
    var temp;
    var i;
    var field;
    var label;
    var countQuery;
    var parseFields;
    var joins = '';
    var aliases = {};
    // var joinTables = [];
    var tblsFields = {};
    var values = [];
    var fieldsFormat = [];
    var fieldsTmp = {};
    var safe;
    var where;
    if (!util.isObject(options)) {
        options = {};
    }
    if (!options.hasOwnProperty('inactive')) {
        options.inactive = false;
    }
    if (!options.hasOwnProperty('simulate')) {
        options.simulate = false;
    }
    if (!options.hasOwnProperty('mode')) {
        options.mode = 'indexed';
    }
    if (options.hasOwnProperty('top') && util.isNumber(options.top)) {
        query += ' TOP ' + options.top;
    }
    if (options.hasOwnProperty('distinct') && util.isBoolean(options.distinct)) {
        if (options.distinct) {
            query += ' DISTINCT ';
        }
    }
    options.where = options.where || [];
    options.orderBy = options.order_by || options.orderBy || [];
    //If a grouping is specified, use the grouping fields on the select
    if ((options.hasOwnProperty('group_by') && util.isArray(options.group_by)) || (options.hasOwnProperty('groupBy') && util.isArray(options.groupBy))) {
        options.fields = options.group_by || options.groupBy;
    }
    const hasObjectInsideArray = function (element) {
        return typeof element === 'object' && element.as;
    };
    //Use either the fields specified, all the fields or fields with 'as'
    if (options.fields && options.fields.length > 0 && options.fields.filter(hasObjectInsideArray).length > 0) {
        parseFields = this.__parseFieldsReadWithAs__(options.fields);
    } else {
        parseFields = this.__parseFieldsRead__(options.fields || false);
    }
    var fields = parseFields.fields;
    var mapColumns = parseFields.map_columns || parseFields.mapColumns;
    var tableFields = parseFields.table_fields || parseFields.tableFields;
    if (this.versioning === 'approval' && options.inactive === 'include') {
        fields.push('"VERSIONING_STATUS"');
        var versioningField = new _.Field({
            name: 'VERSIONING_STATUS',
            type: $.db.types.NVARCHAR
        });
        versioningField.table = this;
        tableFields.push(versioningField);
        mapColumns.push('versioning_status');
    }
    // var pre_query = query + fields.join(', ') + ' FROM ';
    //PREPARE: JOIN OTHERTABLE ON TABLENAME.FIELD = OTHERTABLE.FIELD
    if (options.hasOwnProperty('join') && util.isObject(options.join)) {
        var joinsOutput = this.__joinStatement__({
            fields: fields,
            mapColumns: mapColumns,
            tableFields: tableFields,
            values: values
        }, options);
        joins = joinsOutput.joins;
        aliases = joinsOutput.aliases;
        // joinTables = joinsOutput.join_tables;
        tblsFields = joinsOutput.tbls_fields;
    } else {
        options.join = false;
    }
    //SELECT SCHEMA.TABLENAME.FIELD, SCHEMA.TABLENAME.FIELD, SCHEMA.TABLENAME.FIELD
    for (var y = 0; y < tableFields.length; y++) {
        const elem = tableFields[y];
        if (elem.format) {
            fieldsFormat.push('TO_CHAR(' + fields[y] + ', \'' + elem.format + '\') AS ' + elem.name);
        } else {
            let nameField = elem.name.replace(/('|")+/g, '');
            if (!$.lodash.isNil(fieldsTmp[nameField])) {
                fieldsFormat.push(fields[y] + (' AS "TMP_FIELD_' + (nameField) + '_' + y + '"'));
            } else {
                fieldsFormat.push(fields[y]);
                fieldsTmp[nameField] = true;
            }
        }
    }
    if (fields.length) {
        query += fieldsFormat.join(', ');
    }
    //FROM TABLENAME
    query += ' FROM ';
    var thisName;
    query += this.name;
    if (this.name.indexOf($.viewSchema) !== -1) {
        let inputParameters = this.inputParameters || [];
        query += ' ' + $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], false, inputParameters) + ' ';
    }
    //ADD: JOIN OTHERTABLE ON TABLENAME.FIELD = OTHERTABLE.FIELD
    if (joins) {
        if (this.versioning === 'approval' && options.inactive === 'include') {
            joins = joins.replace(thisName, 'VERSIONS_UNION');
        }
        query += joins;
    }
    //Default safe to false if it was not passed.
    if (options.hasOwnProperty('safe')) {
        safe = options.safe;
    } else {
        safe = false;
    }
    //WHERE (SCHEMA.TABLENAME.FIELD1 = ?) AND (SCHEMA.TABLENAME.FIELD2 LIKE ?) AND (SCHEMA.TABLENAME.FIELD3 IS NULL) OR (SCHEMA.TABLENAME.FIELD4 = ? AND SCHEMA.TABLENAME.FIELD5 LIKE ? AND SCHEMA.TABLENAME.FIELD6 IS NULL)
    if (options.hasOwnProperty('where') && util.isArray(options.where)) {
        if (this.versioning && this.versioning === 'simple') {
            // throw this.versioning;
            if (options.versions) {
                options.where.push({
                    field: 'obj_status',
                    oper: '!=',
                    value: 'Deleted'
                });
            } else {
                options.where.push({
                    field: 'obj_status',
                    oper: '=',
                    value: 'Active'
                });
            }
        }
        if (options.join) {
            where = this.__parseWhere__({
                'where': options.where,
                'values': values,
                'join': options.join
            }, safe);
        } else {
            where = this.__parseWhere__({
                'where': options.where,
                'values': values
            }, safe);
        }
        if (util.isString(where) && where.length) {
            if (this.versioning === 'approval' && options.inactive === 'include') {
                where = where.replace(thisName, 'VERSIONS_UNION');
            }
            query += ' WHERE ' + where;
        }
    }
    //GROUP BY SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD
    if ((options.hasOwnProperty('group_by') && util.isArray(options.group_by)) || (options.hasOwnProperty('groupBy') && util.isArray(options.groupBy))) {
        temp = options.group_by || options.groupBy;
        var gFields = this.__parseFieldArray__(temp);
        if (gFields.length) {
            var groupByFields = gFields.join(', ');
            if (this.versioning === 'approval' && options.inactive === 'include') {
                groupByFields = groupByFields.replace(thisName, 'VERSIONS_UNION');
            }
            query += ' GROUP BY ' + groupByFields;
        }
    }
    if (options.count) {
        if ((options.hasOwnProperty('group_by') && options.group_by.length) || (options.hasOwnProperty('groupBy') && options.groupBy.length)) {
            var cutPos = query.indexOf(' FROM');
            temp = options.group_by || options.groupBy;
            parseFields = this.__parseFieldsRead__(temp);
            var begin = 'SELECT ' + parseFields.fields.join(',');
            // query = query.slice(0, cutPos) + ', COUNT(*)' + query.slice(cutPos);
            query = begin + ', COUNT(*)' + query.slice(cutPos);
            if (options.simulate) {
                return this.sql.__translate_stmt__({
                    query: query,
                    values: values
                });
            }
            return this.sql.SELECT({
                query: query,
                values: values
            });
        } else {
            return this.__countQuery__({
                query: query,
                values: values,
                distinct: options.distinct
            });
        }
    }
    //ORDER BY SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD, SCHEMA.TABLE.FIELD
    if (options.hasOwnProperty('orderBy') && util.isArray(options.orderBy)) {
        var oFields = [];
        var tableName = this.name;
        const hasIdField = options.fields && options.fields.filter(function (elem) { return elem === 'id'; }).length ? true : false ;
        if (options.orderBy.length === 0) { 
            if (this.fields.hasOwnProperty('id') && this.fields['id']) {
                if (hasIdField) {
                    options.orderBy = ['id'];
                } else if (options.fields === null || options.fields === undefined) {
                    options.orderBy = ['id'];
                }
            }
        }
        for (i = 0; i < options.orderBy.length; i++) {
            var asc = true;
            if (typeof options.orderBy[i] === 'string') {
                tableName = this.name;
                label = options.orderBy[i];
            } else if (util.isObject(options.orderBy[i]) && options.orderBy[i].hasOwnProperty('table') && options.orderBy[i].hasOwnProperty('field')) {
                tableName = options.orderBy[i].table.name; //This will cause a problem if used with rename
                label = options.orderBy[i].field;
            } else {
                continue;
            }
            if (label.match(/^-(.*)/)) {
                label = label.slice(1);
                asc = false;
            }
            if (util.isObject(options.orderBy[i]) && options.orderBy[i].hasOwnProperty('table') && options.orderBy[i].hasOwnProperty('field')) {
                label = tableName + '.' + options.orderBy[i].table.fields[label].name;
                oFields.push(label + (asc ? '' : ' DESC'));
            } else if (this.fields.hasOwnProperty(label)) {
                //Add to the map_values (preparedStatement, the '?' thing)
                if (util.isArray(mapColumns)) {
                    mapColumns.push(label);
                }
                //Take the field name
                field = this.fields[label];
                if (SQL.DATA_TYPES[field.type].js == String) { //If it's a string, order by lower
                    oFields.push('LOWER(' + tableName + '.' + field.name + ') ' + (asc ? '' : ' DESC'));
                } else {
                    oFields.push(tableName + '.' + field.name + (asc ? '' : ' DESC'));
                }
            } else {
                var keys = label.match(/-?(\w+)\.(\w+)/);
                if (keys && tblsFields.hasOwnProperty(keys[1]) && tblsFields[keys[1]].hasOwnProperty(keys[2])) {
                    oFields.push(tblsFields[keys[1]][keys[2]] + (asc ? '' : ' DESC'));
                }
            }
        }
        var orderByFields = oFields.join(', ');
        if (this.versioning === 'approval' && options.inactive === 'include') {
            orderByFields = orderByFields.replace(thisName, 'VERSIONS_UNION');
        }
        if (oFields.length) {
            query += ' ORDER BY ' + orderByFields;
        }
    }
    if (options.hasOwnProperty('paginate') && options.paginate.hasOwnProperty('size') && options.paginate.hasOwnProperty('number') &&
        util.isNumber(options.paginate.size) && util.isNumber(options.paginate.number) && options.paginate.number > 0) {
        countQuery = query;
        query += ' LIMIT ' + options.paginate.size + ' OFFSET ' + options.paginate.size * (options.paginate.number - 1);
    } else {
        options.paginate = false;
    }
    if (this.table && this.table.hints && this.table.hints.length > 0) {
        var hints = this.table.hints.join(', ');
        query += ' WITH HINT(' + hints + ')';
    }
    if (options.hasOwnProperty('simulate') && options.simulate) {
        return this.sql.__translate_stmt__({
            query: query,
            values: values
        });
    }
    var results = this.sql.SELECT({
        query: query,
        values: $.lodash.cloneDeep(values)
    });
    var actualTime = (new Date()).getTime();
    // var resultsRawGroupBy = results;
    if (!results) {
        return results;
    }
    var meta = (util.isArray(results.Metadata)) ? results.Metadata : [];
    if (options.mode === 'raw') {
        if (util.isObject(options.paginate) && 'count' in options.paginate) {
            var counts = this.__countQuery__({
                query: countQuery,
                values: values,
                distinct: options.distinct
            });
            if (results && results.initTime && results.timing) {
                results.timing['9. Count'] = (new Date()).getTime() - actualTime;
            }
            // Begin - Fix added to support pagination for group by and distinct
            // if((options.group_by || options.groupBy) || (options.hasOwnProperty('distinct') && options.distinct === true) ) {
            //     results = [resultsRawGroupBy.length];
            // }
            // End
            if (results) {
                results.pageCount = Math.ceil(counts[0] / options.paginate.size);
                results.totalRowAmount = counts[0];
            } else {
                results.pageCount = -1;
                results.totalRowAmount = 0;
            }
        }
        if (results && results.initTime && results.timing) {
            results.timing['10. Total Time'] = (new Date()).getTime() - results.initTime;
        }
        return results;
    }
    var jsonResult;
    if (options.join && options.mode === 'indexed' || options.mode === 'object') {
        jsonResult = this.__joinIndexing__({
            mapColumns: mapColumns,
            results: results,
            tableFields: tableFields,
            fields: fields,
            aliases: aliases
        }, options);
    } else {
        jsonResult = [];
        for (i = 0; i < results.length; i++) {
            var line = results[i];
            var jsonLine = {};
            for (y = 0; y < line.length; y++) {
                field = tableFields[y];
                label = mapColumns[y];
                if (options.hasOwnProperty('join')) {
                    var fieldName = fields[y];
                    if (aliases.hasOwnProperty(fieldName)) {
                        label = aliases[fieldName] + '.' + label;
                    }
                }
                if (field.hasOwnProperty('translate')) {
                    jsonLine[label] = field.translate[line[y]];
                } else if (field.hasOwnProperty('json') && field.json) {
                    jsonLine[label] = JSON.parse(line[y]);
                } else {
                    jsonLine[label] = line[y];
                }
            }
            jsonResult.push(jsonLine);
        }
        jsonResult.Metadata = meta;
    }
    if (results && results.initTime && results.timing) {
        results.timing['9. Prepare JSON'] = (new Date()).getTime() - actualTime;
        actualTime = (new Date()).getTime();
    }
    if (util.isObject(options.paginate) && 'count' in options.paginate) {
        var actualTiming;
        if (results && results.initTime && results.timing) {
            actualTiming = {
                timing: results.timing,
                initTime: results.initTime
            };
        }
        results = this.__countQuery__({
            query: countQuery,
            values: values,
            distinct: options.distinct
        });
        if (actualTiming) {
            results.timing = actualTiming.timing;
            results.initTime = actualTiming.initTime;
            if (results && results.initTime && results.timing) {
                results.timing['10. Count'] = (new Date()).getTime() - actualTime;
                actualTime = (new Date()).getTime();
            }
        }
        // Begin - Fix added to support pagination for group by and distinct
        // if((options.group_by || options.groupBy) || (options.hasOwnProperty('distinct') && options.distinct === true) ) {
        //     results = [resultsRawGroupBy.length];
        // }
        // End
        if (results) {
            jsonResult.pageCount = Math.ceil(results[0] / options.paginate.size);
            jsonResult.totalRowAmount = results[0];
        } else {
            jsonResult.pageCount = -1;
            jsonResult.totalRowAmount = 0;
        }
    }
    if (results && results.initTime && results.timing) {
        results.timing['11. Total Time'] = (new Date()).getTime() - results.initTime;
    }
    jsonResult.timing = results.timing;
    return jsonResult;
};


/*@doc
Usage:
    Table.UPDATE({
        id: 10,
        ...
    }, tags (optional) {
        empresa: 'ANY',
        estado: ['SP', 'RJ'],
        filial: 'ANY',
        imposto: ['ICMS']
    });
*/
Table.prototype.UPDATE = function (options) {
    var createData, filled, readEntry;
    if (this.hasOwnProperty('versioning') && this.versioning === 'simple') {
        //Read the active version
        if (options.hasOwnProperty('id')) {
            var readResult = this.READ({
                where: [{
                    field: 'id',
                    oper: '=',
                    value: options.id
                }, {
                    field: 'obj_status',
                    oper: '=',
                    value: 'Active'
                }]
            });
            if (!readResult.length) {
                throw 'There is no active version for an object with ID: ' + options.id;
            } else {
                //clean  update fields
                readEntry = readResult[0];
                var objVersion = readEntry.obj_version;
                var systemFields = ['obj_status', 'obj_version'];
                if ((this.hasOwnProperty('default_fields') && this.default_fields === 'common') || (this.hasOwnProperty('defaultFields') && this.defaultFields === 'common')) {
                    systemFields = systemFields.concat(['creationUser', 'creationDate', 'modificationUser', 'modificationDate']);
                }
                this.fieldKeys = Object.keys(options);
                for (let i = 0; i < this.fieldKeys.length; i++) {
                    const fieldKey = this.fieldKeys[i];
                    if (systemFields.indexOf(fieldKey) !== -1) {
                        delete options[fieldKey];
                        delete readEntry[fieldKey];
                    } else {
                        readEntry[fieldKey] = options[fieldKey];
                    }
                }
                // for (var fieldKey in options) {
                //     if (systemFields.indexOf(fieldKey) !== -1) {
                //         delete options[fieldKey];
                //         delete readEntry[fieldKey];
                //     } else {
                //         readEntry[fieldKey] = options[fieldKey];
                //     }
                // }
                filled = this.__baseUpdate__({
                    id: readEntry.id,
                    obj_version: objVersion,
                    obj_status: 'Obsolete'
                });
                readEntry.obj_version = objVersion + 1;
                createData = readEntry;
                // throw readEntry;

            }
        }
        //Update its fields
        //Insert the new row
    } else {
        filled = this.__baseUpdate__(options);
    }
    var query = filled.query;
    var values = filled.values;
    var response;
    if (options.simulate) {
        return this.sql.__translate_stmt__({
            query: query,
            values: values
        });
    }
    response = this.sql.UPDATE({
        query: query,
        values: values
    });
    if (createData) {
        this._CREATE(readEntry);
    }
    return response;
};


Table.prototype.UPDATE = util.Declare({
    options: {
        id: util.isNumber
    }
}, Table.prototype.UPDATE);


/*@doc
Internal use, update records that can't be referenced by ID
Usage:
    Table.UPDATEWHERE({
        field: value,
        field: value,
        field: value
    }, [
        {field: 'label', oper: '=', value: 12},
        ...
        {field: 'label', oper: 'LIKE', value: '%foo%'}
        {field: 'label', oper: 'IS NULL'}
    ])
*/
Table.prototype.UPDATEWHERE = function (options, where) {
    if (!util.isObject(options)) {
        return false;
    }
    var query = 'UPDATE ' + this.name;
    var fields = [];
    var values = [];
    this.fieldKeys = Object.keys(this.fields);
    for (let i = 0; i < this.fieldKeys.length; i++) {
        const key = this.fieldKeys[i];
        var valO;
        if (options.hasOwnProperty(key)) {
            if (key === 'id' || key === 'obj_version') {
                continue;
            } else if (this.fields[key].instanceOf === 'AutoField') {
                if (!(this.fields[key].hasOwnProperty('update') && this.fields[key].update !== false)) {
                    fields.push(key);
                    valO = this.fields[key].toValue();
                    values.push(valO);
                }
            } else if (this.fields.hasOwnProperty(key)) {
                fields.push(key);
                valO = this.fields[key].toValue(options[key]);
                if (valO.type === $.db.types.DECIMAL && typeof valO.value === 'string' && valO.value.match(/(\d+\.)*\d+,\d+/g)) {
                    valO.value = valO.value.replace(/\./g, '').replace(/,/g, '.');
                }
                values.push(valO);
            }
        } else if (this.fields.hasOwnProperty(key) && this.fields[key].instanceOf === 'AutoField' && !(this.fields[key].hasOwnProperty('update') && this.fields[key].update === false)) {
            fields.push(key);
            valO = this.fields[key].toValue();
            values.push(valO);
        }

    }
    // for (var y in this.fields) {
    //     var valO;
    //     if (options.hasOwnProperty(y)) {
    //         if (y === 'id' || y === 'obj_version') {
    //             continue;
    //         } else if (this.fields[y].instanceOf === 'AutoField') {
    //             if (!(this.fields[y].hasOwnProperty('update') && this.fields[y].update !== false)) {
    //                 fields.push(y);
    //                 valO = this.fields[y].toValue();
    //                 values.push(valO);
    //             }
    //         } else if (this.fields.hasOwnProperty(y)) {
    //             fields.push(y);
    //             valO = this.fields[y].toValue(options[y]);
    //             if (valO.type === $.db.types.DECIMAL && typeof valO.value === 'string' && valO.value.match(/(\d+\.)*\d+,\d+/g)) {
    //                 valO.value = valO.value.replace(/\./g, '').replace(/,/g, '.');
    //             }
    //             values.push(valO);
    //         }
    //     } else if (this.fields.hasOwnProperty(y) && this.fields[y].instanceOf === 'AutoField' && !(this.fields[y].hasOwnProperty('update') && this.fields[y].update === false)) {
    //         fields.push(y);
    //         valO = this.fields[y].toValue();
    //         values.push(valO);
    //     }
    // }
    // if(options){
    //     for (var y in options) {
    //         if (options.hasOwnProperty(y) && this.fields.hasOwnProperty(y)) {
    //             if (y !== 'id') {
    //                 //fields.push(y);
    //                 values.push(this.fields[y].toValue(options[y]));
    //             }
    //         } else {
    //             throw 'The field: ' + y + 'does not exist on the table';
    //         }
    //     }    
    // } 
    fields = this.__parseFieldArray__(fields);
    var _fields = [];
    for (var i = 0; i < fields.length; i++) {
        _fields.push(fields[i] + ' = ?');
    }
    let fieldsFromTableDefinition = Object.keys(this.fields);
    for (let i = 0; i < fieldsFromTableDefinition.length; i++) {
        for (let j = 0; j < where.length; j++) {
            if (where[j].field === fieldsFromTableDefinition[i] && this.fields[fieldsFromTableDefinition[i]].type === $.db.types.DECIMAL && typeof where[j].value === 'string' && where[j].value.match(/(\d+\.)*\d+,\d+/g)) {
                where[j].value = where[j].value.replace(/\./g, '').replace(/,/g, '.');
            }
        }
    }
    query += ' SET ' + _fields.join(', ') + ' WHERE ' + this.__parseWhere__({
        'where': where,
        'values': values
    });
    return this.sql.UPDATE({
        query: query,
        values: values
    });
};


/*@doc
Usage:
    Table.DELETE(10);
Reponse:
    true
*/
Table.prototype.DELETE = function (id) {
    // throw this.versioning;
    if (this.versioning && this.versioning === 'simple') {
        return this._UPDATE({
            id: id,
            obj_status: 'Deleted'
        });
    } else {
        return this._DELETE(id);
    }
    // if (this.historical && this.versioning == 'approval') {
    //     var filled = this.__parseDelete__(Number(id));
    //     var where = filled.where,
    //         values = filled.value;
    //     var current = this.historical.READ({
    //         where: [
    //             {field: 'id_original_obj', oper: '=', value: id}
    //         ],
    //         orderBy: ['-curr_version']
    //     })[0];
    //     current['versioning_action'] = 'DELETE';
    //     current['curr_version'] = ++current.curr_version;
    //     this.__skipUnactivatedAction__(id);
    //     this.historical.CREATE(current);
    //     return true;
    // }
};


/*@doc
Internal use, delete records that can't be referenced by ID
Usage:
    Table.DELETEWHERE([
        {field: 'label', oper: '=', value: 12},
        ...
        {field: 'label', oper: 'LIKE', value: '%foo%'}
        {field: 'label', oper: 'IS NULL'}
    ], {simulate:false})
*/
Table.prototype.DELETEWHERE = function (where, options) {
    var values = [];
    var query = 'DELETE FROM ' + this.name + ' WHERE ' + this.__parseWhere__({
        'where': where,
        'values': values
    });
    if (util.isObject(options) && options.hasOwnProperty('simulate') && options.simulate) {
        return this.sql.__translate_stmt__({
            query: query,
            values: values
        });
    }
    var status = this.sql.DELETE({
        query: query,
        values: values
    });
    return status;
};


/*@doc
Usage:
    Table.getTableField('id')
Returns: 
    "SCHEMA"."TABLE"."ID"
*/
Table.prototype.getTableField = function (name) {
    if (this.fields.hasOwnProperty(name)) {
        // var field = this.fields[name];
        // if (field.format) {
        //     return 'TO_CHAR(' + this.name + '.' + field.name + ', \'' + field.format + '\')';
        // }
        return this.name + '.' + this.fields[name].name;
    } else {
        return false;
    }
};


/*
    STATIC WHERE_OPERS
    Usage:
        Table.WHERE_OPERS.hasOwnProperty('LIKE') //To say if it is a valid oper
            true
        Table.WHERE_OPERS['LIKE']({field: label5, oper: "LIKE", value: '%foo%'}) //To get its representation
            label5 LIKE ?
*/
Table.prototype.WHERE_OPERS = {
    '=': function (stmt) {
        return stmt.field + ' = ?';
    },
    '!=': function (stmt) {
        return 'NOT (' + stmt.field + ' = ?)';
    },
    '>=': function (stmt) {
        return stmt.field + ' >= ?';
    },
    '<=': function (stmt) {
        return stmt.field + ' <= ?';
    },
    '>': function (stmt) {
        return stmt.field + ' > ?';
    },
    '<': function (stmt) {
        return stmt.field + ' < ?';
    },
    'LIKE': function (stmt, toChar) {
        if (stmt.hasOwnProperty('maskFn')) {
            return (toChar ? 'TO_CHAR(' : '') + stmt.maskFn + '(' + stmt.field + (toChar ? ')' : '') + ') LIKE ' + stmt.maskFn + '(?)';
        }
        return (toChar ? 'TO_CHAR(' : '') + stmt.field + (toChar ? ')' : '') + ' LIKE ?';
    },
    'LIKE_REGEXPR': function (stmt) {
        if (stmt.hasOwnProperty('maskFn')) {
            return stmt.maskFn + '(' + stmt.field + ') LIKE_REGEXPR ' + stmt.maskFn + '(?)';
        }
        return stmt.field + ' LIKE_REGEXPR ?';
    },
    'IS NULL': function (stmt) {
        return stmt.field + ' IS NULL';
    },
    'IS NOT NULL': function (stmt) {
        return 'NOT (' + stmt.field + ' IS NULL)';
    }
};


/*
INTERNAL USE ONLY
Usage:
    Table.__parseCreate__({
        id:     value
        label:  value,
        ...
        label:  value,
        label:  value
    },{ //options
        overwrite_auto (optional): true //INTERNAL, if not used properly will cause data inconsistency
    });
Returns:
    {
        values: {id: 10, foo: 'bar'}, //All values, gaps filled
        create_fields: [{field: 'ID', type: $.db.types.INTEGER, value: 10}, ...], //Content needed by SQL.INSERT
        response: {id: 10} //Only auto fields
    }
*/
Table.prototype.__parseCreate__ = function (options, config) {
    if (!config || !util.isObject(config)) {
        config = {};
    }
    var createFields = [];
    var response = {};
    var values = {};
    if (this.versioning === 'simple') {
        options.obj_status = options.obj_status || 'Active';
        options.obj_version = options.obj_version || 1;
    }
    // if($.session.getUsername() === 'MMARA')
    //     throw options;
    this.fieldKeys = Object.keys(this.fields);
    for (let i = 0; i < this.fieldKeys.length; i++) {
        const key = this.fieldKeys[i];
        //The options contains a field the table knows
        // if (this.fields.hasOwnProperty(key)) {
        var field = this.fields[key];
        var valO;
        if (field.instanceOf == 'AutoField') {
            if (config.overwrite_auto && options.hasOwnProperty(key)) {
                valO = field.toValue(options[key]);
            } else {
                valO = field.toValue();
            }
            valO.name = field.name;
            createFields.push(valO);
            response[key] = valO.value;
        } else if (options.hasOwnProperty(key)) {
            valO = field.toValue(options[key]);
            valO.name = field.name;
            createFields.push(valO);
        } else if (field.default) {
            valO = field.toValue(field.default);
            valO.name = field.name;
            createFields.push(valO);
            response[key] = valO.value;
        }
        values[key] = valO.value;
        // }
    }

    // for (var key in this.fields) {
    //     //The options contains a field the table knows
    //     if (this.fields.hasOwnProperty(key)) {
    //         var field = this.fields[key];
    //         var valO;
    //         if (field.instanceOf == 'AutoField') {
    //             if (config.overwrite_auto && options.hasOwnProperty(key)) {
    //                 valO = field.toValue(options[key]);
    //             } else {
    //                 valO = field.toValue();
    //             }
    //             valO.name = field.name;
    //             createFields.push(valO);
    //             response[key] = valO.value;
    //         } else if (options.hasOwnProperty(key)) {
    //             valO = field.toValue(options[key]);
    //             valO.name = field.name;
    //             createFields.push(valO);
    //         } else if (field.default) {
    //             valO = field.toValue(field.default);
    //             valO.name = field.name;
    //             createFields.push(valO);
    //             response[key] = valO.value;
    //         }
    //         values[key] = valO.value;
    //     }
    // }
    return {
        values: values,
        create_fields: createFields,
        response: response
    };
};


/*@doc
INTERNAL USE, misuse will cause data inconsistency
Usage:
    Table.__skipUnactivatedAction__();
Return:
    true
*/
Table.prototype.__skipUnactivatedAction__ = function (id) {
    if (!util.isNumber(Number(id))) {
        throw 'Expected numeric ID for Table.__skipUnactivatedAction__, got ' + util.parseError(id);
    }
    if (this.versioning === 'approval') {
        return SQL.UPDATE({
            query: 'UPDATE ' + this.historical.name + ' SET VERSIONING_ACTION = VERSIONING_ACTION + 3 WHERE ID_ORIGINAL_OBJ = ? AND VERSIONING_ACTION IN (1,2,3)',
            values: [{
                type: $.db.types.INTEGER,
                value: Number(id)
            }]
        });
    }
};


/*@doc
INTERNAL USE ONLY, misuse will cause data inconsistency
Usage:
    Table._CREATE({
        id:     value
        label:  value,
        ...
        label:  value,
        label:  value
    });
*/
Table.prototype._CREATE = function (options) {
    var filled = this.__parseCreate__(options, {
        overwrite_auto: true
    });
    var status = this.sql.INSERT({
        table: this.name,
        fields: filled.create_fields
    });
    if (status) {
        return filled.response;
    } else {
        return status;
    }
};


/*
Private method
Receives:
    ['label', 'label', 'label', 'label']
Returns:
    ['"SCHEMA"."TABLENAME"."FIELD"', '"SCHEMA"."TABLENAME"."FIELD"', '"SCHEMA"."TABLENAME"."FIELD"', '"SCHEMA"."TABLENAME"."FIELD"']
*/
Table.prototype.__parseFieldArray__ = function (fieldArray, mapColumns) {
    var fields = [];
    for (var i = 0; i < fieldArray.length; i++) {
        if (this.fields.hasOwnProperty(fieldArray[i])) {
            if (util.isArray(mapColumns)) {
                mapColumns.push(fieldArray[i]);
            }
            var field = this.fields[fieldArray[i]];
            fields.push(this.name + '.' + field.name);
        }
    }
    return fields;
};
Table.prototype.__parse_field_array__ = Table.prototype.__parseFieldArray__;


/*
Private method
Receives:
    ['label', '-label', 'label', 'label']
Returns:
    ['"SCHEMA"."TABLENAME"."FIELD" ASC', '"SCHEMA"."TABLENAME"."FIELD" DESC', '"SCHEMA"."TABLENAME"."FIELD" ASC', '"SCHEMA"."TABLENAME"."FIELD" ASC']
*/
Table.prototype.__parseOrderByFieldArray__ = function (arr, mapColumns) {
    var fields = [];
    var asc = true;
    for (var i = 0; i < arr.length && typeof arr[i] === 'string'; i++) {
        var label = arr[i];
        if (label.match(/^-(.*)/)) {
            label = label.slice(1);
            asc = false;
        }
        if (this.fields.hasOwnProperty(label)) {
            if (util.isArray(mapColumns)) {
                mapColumns.push(label);
            }
            var field = this.fields[label];
            fields.push(this.name + '.' + field.name + (asc ? '' : ' DESC'));
        }
    }
    return fields;
};
Table.prototype.__parse_orderby_field_array__ = Table.prototype.__parseOrderByFieldArray__;


/*
Private method for iterating an array of arrays and strings and returning a logical assembly, as:
Usage:
    Table.__whereIteration__([
        'FOO = ?',
        'BAR = LOREM',
        [
            ['FOO = 12', 'IPSUM < 10'],
            ['FOO = 13', 'IPSUM >= 10']
        ]
    ])
Response:
    FOO = ? AND BAR = LOREM AND ((FOO = 12 AND IPSUM < 10) OR (FOO = 13 AND IPSUM >= 10))
*/
Table.prototype.__whereIteration__ = function (where, join) {
    join = !!join;
    var strJoin = (join ? ' OR ' : ' AND '); //true=OR false=AND (default is false)
    var response = '';
    var y;
    if (util.isArray(where) && where.length) {
        for (y = 0; y < where.length - 1; y++) {
            response += this.__whereIteration__(where[y], !join) + strJoin;
        }
        response += this.__whereIteration__(where[y], !join);
        response = '(' + response + ')';
    } else {
        return where;
    }
    return response;
};


/*
Private function for parsing logical statements
Usage:
    Table.__logicalStatement__({field: label2, oper: 'LIKE', value: '%foo%'}, values, safe);
    Table.__logicalStatement__('FOO = BAR', values, safe);
Returns:
    'FIELD LIKE ?' (values.push({type: $.db.types.NVARCHAR, value: '%foo%'}))
    'FOO = BAR' (does nothing to values)
*/
Table.prototype.__logicalStatement__ = function (statement, values, safe) {
    if (util.isObject(statement) && statement.hasOwnProperty('oper') && this.WHERE_OPERS.hasOwnProperty(statement.oper) && statement.hasOwnProperty('field')) {
        var field;
        var pls;
        var i;
        var value;
        if (statement.hasOwnProperty('table')) { //If the statement received has a property table use that table's name
            if ((statement.table.instanceOf === 'Table' || statement.table.instanceOf === 'View') && statement.table.fields.hasOwnProperty(statement.field)) {
                field = statement.table.fields[statement.field];
                var tableName = statement.table.rename || statement.table.name;
                statement.field = tableName + '.' + statement.table.fields[statement.field].name;
            } else {
                throw 'Did not find field ' + statement.field + ' on ' + statement.table;
            }
        } else if (this.fields.hasOwnProperty(statement.field)) { //If this table has a field with this label, use it
            field = this.fields[statement.field];
            statement.field = this.name + '.' + this.fields[statement.field].name;
        }
        if (field) { //if you found a field
            if (this.sql.DATA_TYPES[field.type].name.match('/LOB/g') || this.sql.DATA_TYPES[field.type].name === 'TEXT') {
                return false; //These fields cannot make part of where clauses
            }
            if (!statement.hasOwnProperty('value')) {
                value = this.WHERE_OPERS[statement.oper](statement);
            } else if (statement.hasOwnProperty('literal') && statement.literal) {
                if (safe) {
                    throw 'You cannot use literals in safe mode.';
                }
                if (statement.oper === '=' && util.isArray(statement.value) && !$.lodash.isEmpty(statement.value)) {
                    pls = [];
                    for (i = 0; i < statement.value.length; i++) {
                        pls.push(statement.value[i]);
                    }
                    value = statement.field + ' IN [' + pls.join(', ') + ']';
                } else {
                    value = this.WHERE_OPERS[statement.oper](statement).replace('?', statement.value);
                }
            } else {
                if (statement.oper === '=' && util.isArray(statement.value) && !$.lodash.isEmpty(statement.value)) {
                    pls = [];
                    for (i = 0; i < statement.value.length; i++) {
                        values.push(field.toValue(statement.value[i]));
                        pls.push('?');
                    }
                    value = statement.field + ' IN (' + pls.join(', ') + ')';
                } else {
                    values.push(field.toValue(statement.value));
                    if (['CLOB', 'NCLOB', 'BLOB'].indexOf(this.sql.DATA_TYPES[field.type].name) !== -1) {
                        value = this.WHERE_OPERS[statement.oper](statement, true);
                    } else {
                        value = this.WHERE_OPERS[statement.oper](statement);
                    }
                }
            }
            if (statement.hasOwnProperty('not') && statement.not) {
                return ' (NOT ' + value + ') ';
            } else {
                return value;
            }
        }
    }
    return false;
};


/*
Private method
Usage:
    Table.__assembleWhere__([
        {field: label1, oper: '=', value: 12},
        ...
        {field: label2, oper: 'LIKE', value: '%foo%'},
        {field: label2, oper: 'LIKE', value: '%foo%', maskFn: 'UPPER'}
        {field: label3, oper: 'IS NULL'},
        {field: label3, oper: 'IS NULL', table: },
        [ //OR
            {field: label4, oper: '=', value: 12},
            ...
            {field: label5, oper: 'LIKE', value: '%foo%'}
            {field: label6, oper: 'IS NULL'},
        ]
    ])
Returns:
    ['FOO = BAR','LOREM < ?', ['FIELD = ?']] //To be used by __whereIteration__
*/
Table.prototype.__assembleWhere__ = function (where, values, safe) {
    if (util.isArray(where)) {
        var stmt = [];
        for (var i = 0; i < where.length; i++) {
            var res = this.__assembleWhere__(where[i], values, safe);
            if (res) {
                stmt.push(res); //Ignore errors
            }
        }
        return stmt;
    } else if (util.isObject(where)) {
        return this.__logicalStatement__(where, values, safe);
    } else if (util.isString(where)) {
        return where;
    }
    return '';
};


/*
Private method for parsing WHERE clauses
Receives:
{
    where: [
        {field: label1, oper: '=', value: 12},
        ...
        {field: label2, oper: 'LIKE', value: '%foo%'},
        {field: label2, oper: 'LIKE', value: '%foo%', maskFn: 'UPPER'}
        {field: label3, oper: 'IS NULL'},
        {field: label3, oper: 'IS NULL', table: },
        [ //OR
            {field: label4, oper: '=', value: 12},
            ...
            {field: label5, oper: 'LIKE', value: '%foo%'}
            {field: label6, oper: 'IS NULL'},
        ]
    ],
    values: [
        {type: $.db.types.TIMESTAMP,    value: '01.02.2003 01:02:03.123',   format: 'DD.MM.YYYY HH:MI:SS.FF'}
    ]
}
Returns:
    query: 'FIELD1 = ? AND FIELD2 LIKE ? AND FIELD3 IS NULL OR (FIELD4 = ? AND FIELD5 LIKE ? AND FIELD6 IS NULL)',
Silently updates:
        values: [
            {type: $.db.types.TIMESTAMP,    value: '01.02.2003 01:02:03.123',   format: 'DD.MM.YYYY HH:MI:SS.FF'},
            {type: $.db.types.DATE,         value: '25/12/2009',                format: 'dd/mm/yyyy'},
            {type: $.db.types.TIME,         value: '09:57:57.99 PM',            format: 'HH:MI:SS.FF AM'},
            {type: $.db.types.TIME,         value: '09:57:57.99 PM',            format: 'HH:MI:SS.FF AM'},
            ...
        ]
    }
*/
Table.prototype.__parseWhere__ = function (options, safe) {
    if (!util.isObject(options)) {
        throw 'Invalid call to Table.__parseWhere__: options not an object';
    } else if (!(options.hasOwnProperty('where') && util.isObject(options.where))) {
        throw 'Invalid call to Table.__parseWhere__: options.where not an object';
    } else if (!(options.hasOwnProperty('values') && util.isArray(options.values))) {
        throw 'Invalid call to Table.__parseWhere__: options.values not an array';
    }
    var where = this.__assembleWhere__(options.where, options.values, safe);
    var logicalStmt = this.__whereIteration__(where);
    return logicalStmt;
};


/*
Private
Usage:
    Table.__iterateOn__({
        join_stmt: {<the join statement>},
        join_table: {
            name: 'ALIAS'
        },
        simulate: false/true,
        values: [<values array]
    })
Returns:
    ['FOO = BAR','LOREM < ?', ['FIELD = ?']] //To be used by __whereIteration__
Silently updates the values array
*/
Table.prototype.__iterateOn__ = function (options) {
    // if($.session.getUsername() === 'JMIDE')
    // throw options
    var conditions = [],
        joinStmt = options.join_stmt,
        joinTable = options.join_table,
        values = options.values,
        onStmt = options.target || joinStmt.on,
        simulate = options.simulate || false;
    if (util.isArray(onStmt)) {
        for (var y = 0; y < onStmt.length; y++) {
            options.target = onStmt[y];
            conditions.push(this.__iterateOn__(options));
        }
    } else if (util.isObject(onStmt)) {
        var leftT = onStmt.left_table || onStmt.leftTable;
        onStmt.leftTable = (leftT ? leftT : this);
        var l = onStmt.hasOwnProperty('left'),
            r = onStmt.hasOwnProperty('right'),
            lf = l && onStmt.leftTable.fields.hasOwnProperty(onStmt.left),
            rf = r && joinStmt.table.fields.hasOwnProperty(onStmt.right);
        if (lf && rf) {
            var fkFieldName = joinTable.name + '.' + joinStmt.table.fields[onStmt.right].name;
            var oper = ' = ';
            if (onStmt.hasOwnProperty('oper')) {
                oper = onStmt.oper;
            }
            return onStmt.leftTable.name + '.' + onStmt.leftTable.fields[onStmt.left].name + oper + fkFieldName;
        } else if (l && r && simulate) {
            var response = '\n-- wrong field information on onStmt --\n';
            if (!lf) {
                response += '\n--' + onStmt.leftTable.name + ' has no field ' + onStmt.left + '--\n';
            }
            if (!rf) {
                response += '\n--' + joinTable.name + ' has no field ' + onStmt.right + '--\n';
            }
            return response;
        } else {
            if (!onStmt.table) {
                onStmt.table = joinStmt.table;
                if (joinTable.name) {
                    onStmt.table.rename = joinTable.name;
                }
            } else if (onStmt.table.name === joinStmt.table.name) {
                onStmt.table.rename = joinTable.name;
            }
            var whereStmt = this.__logicalStatement__(onStmt, values);
            if (whereStmt === true || whereStmt) {
                return whereStmt;
            }
        }
    }
    return conditions;
};


/*
Usage:
    Table.__joinStatement__({
        data: {
            fields: [<field instance>, ..]
            mapColumns: ['label','label'..]
            tableFields: [<field instance>, ..]
        },
        options: <table read options>
    })
*/
Table.prototype.__joinStatement__ = function (data, options) {
    var joins = '';
    var aliases = {};
    var joinTables = [];
    var tblsFields = {};
    var fields = data.fields; //(fields is options.fields processed)
    var mapColumns = data.map_columns || data.mapColumns;
    var tableFields = data.table_fields || data.tableFields;
    var values = data.values;
    var i = 0;
    if (options.hasOwnProperty('indexBy') && options.indexBy !== this) { //If the index table is not this table
        for (i = 0; i < fields.length; i++) { //For each this table's fields
            aliases[fields[i]] = options.hasOwnProperty('alias') && options.alias || this.name; //Link this table's field with the alias
        }
    }
    for (i = 0; i < options.join.length; i++) { //For each join statement
        var joinStmt = options.join[i]; // joinStmt is the current join statement
        if (joinStmt.hasOwnProperty('table') && typeof joinStmt.table === 'object' && (joinStmt.table.instanceOf === 'Table' || joinStmt.table.instanceOf === 'View') && util.isArray(joinStmt.on) && joinStmt.on.length && joinStmt.hasOwnProperty('alias')) {
            var joinTableName;
            var prefix;
            var y;
            let inputParameters = joinStmt.table.inputParameters || [];
            if (joinStmt.hasOwnProperty('rename')) {
                joinTableName = joinStmt.table.name + (joinStmt.table.name.search('_SYS_BIC') !== -1 ? ' ' + $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], false, inputParameters) : '') + ' AS ' + joinStmt.rename;
                prefix = joinStmt.rename;
            } else {
                prefix = joinStmt.table.name;
                joinTableName = joinStmt.table.name + (joinStmt.table.name.search('_SYS_BIC') !== -1 ? ' ' + $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], false, inputParameters) : '');
            }
            joinTables.push(joinStmt.table);
            var fkFields;
            //Get specified fields or all fields
            if (joinStmt.hasOwnProperty('fields') && util.isArray(joinStmt.fields)) {
                if (options.hasOwnProperty('indexBy') && joinStmt.table == options.indexBy && joinStmt.fields.indexOf('id') === -1) {
                    joinStmt.fields.push('id');
                }
                fkFields = joinStmt.fields;
            } else {
                fkFields = [];
                let joinStmtFields = Object.keys(joinStmt.table.fields);
                for (let i = 0; i < joinStmtFields.length; i++) {
                    const key = joinStmtFields[i];
                    fkFields.push(key);
                }
                // for (y in joinStmt.table.fields) {
                //     if (joinStmt.table.fields.hasOwnProperty(y)) {
                //         fkFields.push(y);
                //     }
                // }
            }
            //Add the fields that do exist to the query
            for (y = 0; y < fkFields.length; y++) {
                if (joinStmt.table.fields.hasOwnProperty(fkFields[y])) {
                    var fkFieldName;
                    if (prefix) {
                        fkFieldName = prefix + '.' + joinStmt.table.fields[fkFields[y]].name;
                    } else {
                        fkFieldName = joinStmt.table.name + '.' + joinStmt.table.fields[fkFields[y]].name;
                    }
                    fields.push(fkFieldName);
                    mapColumns.push(fkFields[y]);
                    aliases[fkFieldName] = joinStmt.alias;
                    tableFields.push(joinStmt.table.fields[fkFields[y]]);
                    if (!tblsFields.hasOwnProperty(joinStmt.alias)) {
                        tblsFields[joinStmt.alias] = {};
                    }
                    tblsFields[joinStmt.alias][fkFields[y]] = fkFieldName;
                }
            }
            //Make the join
            if (joinStmt.hasOwnProperty('outer')) {
                joins += (joinStmt.outer === 'left') && ' LEFT OUTER JOIN ' ||
                    (joinStmt.outer === 'right') && ' RIGHT OUTER JOIN ' ||
                    (joinStmt.outer === 'both') && ' FULL OUTER JOIN ' ||
                    ' INNER JOIN ';
                joins += joinTableName + ' ON ';
            } else {
                joins += ' INNER JOIN ' + joinTableName + ' ON ';
            }
            if (prefix) {
                joinTableName = prefix;
            }
            //Solve the ON expression
            var conditions = this.__iterateOn__({
                join_stmt: joinStmt,
                join_table: {
                    name: joinTableName
                },
                simulate: options.simulate,
                values: values
            });
            joins += this.__whereIteration__(conditions);
        } else {
            throw util.parseError(['Invalid join statement: ', joinStmt]);
        }
    }
    return {
        joins: joins,
        aliases: aliases,
        join_tables: joinTables,
        tbls_fields: tblsFields
    };
};


/*@docs
INTERNAL, made external for it was too extense to manage inside Table.READ
Usage:
    Table.__joinIndexing__({
        mapColumns: ['label','label'..],
        results: [[1, 'ASD'],[2, 'FOO']..],
        tableFields: [<field instance>, ..],
        fields: ['"TABLE"."FIELD"', ..],
        aliases: {'"TABLE"."FIELD"': 'child', ..}
    }, )
Returns:
    [{
        id: 1,
        value: 'asd',
        child: [{id: 1, ..} ..]
    }, ..]
*/
Table.prototype.__joinIndexing__ = function (data, options) {
    //Set the environment
    var mapColumns = data.map_columns || data.mapColumns;
    var results = data.results;
    var tableFields = data.table_fields || data.tableFields;
    var fields = data.fields;
    var aliases = data.aliases;
    var mapIds = [];
    //Declare variables
    var jsonResult = {}; //This will build a big object indexed by the indexing values
    var indexer = this;
    var idField = options.idField || 'id'; // If there is not an explicid replacement, the indexing field will be 'id' 
    var idPos = mapColumns.indexOf(idField); // The idPos will be the first match on the mapColumns, e.g. ['id', 'label' ..] = 0
    var uniquePacking = {};
    var y;
    var line;
    if (options.hasOwnProperty('indexBy') && (options.indexBy.instanceOf === 'Table' || options.indexBy.instanceOf === 'View')) { //If the invoker specified a valid indexBy (a table object)
        indexer = options.indexBy;
        while (idPos !== -1 && tableFields[idPos].table !== options.indexBy) { // While the indexing you have is not from the indexing table
            var idFound = mapColumns.slice(idPos + 1).indexOf(idField); // Get a field that matched the label of the indexing field
            if (idFound === -1) { // If you reached the end of the list of labels without success
                break;
            }
            idPos += idFound + 1; //Set the proper position
        }
    } else { //If no one specified a indexing Table, the current table should be the indexer and always it's fields are always the first ones:
        idPos = mapColumns.indexOf(idField); //Take the first occurence of the indexing label
    }
    for (var i = 0; i < results.length; i++) { //For each line
        var objectPacking = {}; //Reference the line
        var jsonLine = {};
        line = results[i];
        if (!line.hasOwnProperty(idPos)) { //If the line has not that position, throw an error
            throw 'Invalid JOIN: ' + JSON.stringify(line) + ' has no attr ' + idPos;
        }
        var uniqueId = line[idPos]; //Get the indexing value on this line
        if (this.versioning === 'approval' && options.inactive === 'include') {
            uniqueId += line[mapColumns.indexOf('versioning_status')]; //Add the versioning status, when this table is versioned
        }
        if (jsonResult.hasOwnProperty(uniqueId)) { //If we passed by this ID already, take the value we have
            jsonLine = jsonResult[uniqueId];
        }
        for (y = 0; y < line.length; y++) { //For each row of each line
            var field = tableFields[y]; //Get the 
            if (field.table == indexer) { //if this field belongs to the indexing table
                var label = mapColumns[y]; // Get the field 'label'
                if (field.hasOwnProperty('translate')) { // If this field has a property translate, like {1: 'Blue', 2: 'Yellow'}
                    jsonLine[label] = field.translate[line[y]]; //Then get the translation of this fields value, like 'Blue'
                } else if (field.hasOwnProperty('json') && field.json) {
                    jsonLine[label] = JSON.parse(line[y]); //If this field has a property json, like json: true it will parse the line
                } else {
                    jsonLine[label] = line[y]; //If this field does not have a translation, simply get the value from the row
                }
            } else { //If this field belongs to an indexed table
                var fieldName = fields[y]; //Get the "Table"."Field" name
                if (!aliases.hasOwnProperty(fieldName)) { //If the field in the current row does not match any indexed table, ignore it
                    continue;
                    // throw fieldName + ' is not contained on ' + JSON.stringify(aliases);
                }
                if (aliases.hasOwnProperty(fieldName) && !jsonLine.hasOwnProperty(aliases[fieldName])) {
                    jsonLine[aliases[fieldName]] = []; //Create the array, if its the first time you encounter this alias
                }
                if (line[y] !== null) { //If there is something on this row (left/right outers return a bunch of nulls, this is in order to skip it)
                    if (!objectPacking.hasOwnProperty(aliases[fieldName])) {
                        objectPacking[aliases[fieldName]] = {};
                    }
                    //If I can't find the label for this value, will use the "TABLENAME"."FIELD" -> "FIELD" name
                    objectPacking[aliases[fieldName]][mapColumns.hasOwnProperty(y) && mapColumns[y] || field.name] = line[y];
                }
            }
        }
        if (!uniquePacking.hasOwnProperty(uniqueId)) { //If it's the first time we encounter this indexing value
            uniquePacking[uniqueId] = [];
        }
        var unique = uniquePacking[uniqueId];
        let objectPackingKeys = Object.keys(objectPacking);
        for (let i = 0; i < objectPackingKeys.length; i++) {
            const key = objectPackingKeys[i];
            // if (objectPacking.hasOwnProperty(key)) {
            if (!unique.hasOwnProperty(key)) {
                unique[key] = [];
            }
            var object = objectPacking[key];
            if (object.hasOwnProperty('id')) {
                if (unique[key].indexOf(object.id) == -1 && jsonLine.hasOwnProperty(key) && util.isArray(jsonLine[key])) {
                    unique[key].push(object.id);
                    jsonLine[key].push(object);
                }
                // else ADDED 12.11.2015 ATERR because cases of joining with views without 'id' property was skipping the result
            } else {
                jsonLine[key] = objectPacking[key];
            }
            // }
        }
        // for (y in objectPacking) {
        //     if (objectPacking.hasOwnProperty(y)) {
        //         if (!unique.hasOwnProperty(y)) {
        //             unique[y] = [];
        //         }
        //         var object = objectPacking[y];
        //         if (object.hasOwnProperty('id')) {
        //             if (unique[y].indexOf(object.id) == -1 && jsonLine.hasOwnProperty(y) && util.isArray(jsonLine[y])) {
        //                 unique[y].push(object.id);
        //                 jsonLine[y].push(object);
        //             }
        //             // else ADDED 12.11.2015 ATERR because cases of joining with views without 'id' property was skipping the result
        //         } else {
        //             jsonLine[y] = objectPacking[y];
        //         }
        //     }
        // }
        jsonResult[uniqueId] = jsonLine;
        if (mapIds.indexOf(uniqueId) === -1) {
            mapIds.push(uniqueId);
        }
    }
    //Transform the object back into an array
    /*
        {indexingField: {label: value, alias: {label:value}, .. }, ..}
        to
        [{label: value, alias: {label: value}, ..}, ..]
    */
    if (options.mode === 'object') {
        return jsonResult;
    }
    var lineResult = [];
    // for (y in mapIds) {
    //     if (jsonResult.hasOwnProperty(y)) {
    //         line = jsonResult[y];
    //         lineResult.push(line);
    //     }
    // }
    for (let i = 0; i < mapIds.length; i++) {
        const index = mapIds[i];
        if (jsonResult.hasOwnProperty(index)) {
            line = jsonResult[index];
            lineResult.push(line);
        }
    }
    // mapIds.forEach(function (index) {
    //     if (jsonResult.hasOwnProperty(index)) {
    //         line = jsonResult[index];
    //         lineResult.push(line);
    //     }
    // });
    return lineResult;
};


Table.prototype.__joinIndexing2__ = function (data, options) {
    //Set the environment
    var mapColumns = data.map_columns || data.mapColumns,
        results = data.results,
        tableFields = data.table_fields || data.tableFields,
        fields = data.fields,
        aliases = data.aliases;
    //Declare variables
    var jsonResult = {}, //This will build a big object indexed by the indexing values
        indexer = this,
        idField = options.idField || 'id', //If there is not an explicid replacement, the indexing field will be 'id' 
        idPos = mapColumns.indexOf(idField); //The idPos will be the first match on the mapColumns, e.g. ['id', 'label' ..] = 0
    if (options.hasOwnProperty('indexBy') && (options.indexBy.instanceOf === 'Table' || options.indexBy.instanceOf === 'View')) { //If the invoker specified a valid indexBy (a table object)
        indexer = options.indexBy;
        while (idPos !== -1 && tableFields[idPos].table !== options.indexBy) { //While the indexing you have is not from the indexing table
            var idFound = mapColumns.slice(idPos + 1).indexOf(idField); //Get a field that matched the label of the indexing field
            if (idFound === -1) { //If you reached the end of the list of labels without success
                break;
            }
            idPos += idFound + 1; //Set the proper position
        }
    } else { //If no one specified a indexing Table, the current table should be the indexer and always it's fields are always the first ones:
        idPos = mapColumns.indexOf(idField); //Take the first occurence of the indexing label
    }
    var uniquePacking = {},
        y, line;
    for (var i = 0; i < results.length; i++) { //For each line
        var objectPacking = {}, //Reference the line
            jsonLine = {};
        line = results[i];
        if (!line.hasOwnProperty(idPos)) { //If the line has not that position, throw an error
            throw 'Invalid JOIN: ' + JSON.stringify(line) + ' has no attr ' + idPos;
        }
        var uniqueId = line[idPos]; //Get the indexing value on this line
        if (this.versioning === 'approval' && options.inactive === 'include') {
            uniqueId += line[mapColumns.indexOf('versioning_status')]; //Add the versioning status, when this table is versioned
        }
        if (jsonResult.hasOwnProperty(uniqueId)) { //If we passed by this ID already, take the value we have
            jsonLine = jsonResult[uniqueId];
        }
        for (y = 0; y < line.length; y++) { //For each row of each line
            var field = tableFields[y]; //Get the 
            if (field.table == indexer) { //if this field belongs to the indexing table
                var label = mapColumns[y]; // Get the field 'label'
                if (field.hasOwnProperty('translate')) { // If this field has a property translate, like {1: 'Blue', 2: 'Yellow'}
                    jsonLine[label] = field.translate[line[y]]; //Then get the translation of this fields value, like 'Blue'
                } else if (field.hasOwnProperty('json') && field.json) {
                    jsonLine[label] = JSON.parse(line[y]); //If this field has a property json, like json: true it will parse the line
                } else {
                    jsonLine[label] = line[y]; //If this field does not have a translation, simply get the value from the row
                }
            } else { //If this field belongs to an indexed table
                var fieldName = fields[y]; //Get the "Table"."Field" name
                if (!aliases.hasOwnProperty(fieldName)) { //If the field in the current row does not match any indexed table, ignore it
                    continue;
                    // throw fieldName + ' is not contained on ' + JSON.stringify(aliases);
                }
                if (aliases.hasOwnProperty(fieldName) && !jsonLine.hasOwnProperty(aliases[fieldName])) {
                    jsonLine[aliases[fieldName]] = []; //Create the array, if its the first time you encounter this alias
                }
                if (line[y] !== null) { //If there is something on this row (left/right outers return a bunch of nulls, this is in order to skip it)
                    if (!objectPacking.hasOwnProperty(aliases[fieldName])) {
                        objectPacking[aliases[fieldName]] = {};
                    }
                    //If I can't find the label for this value, will use the "TABLENAME"."FIELD" -> "FIELD" name
                    objectPacking[aliases[fieldName]][mapColumns.hasOwnProperty(y) && mapColumns[y] || field.name] = line[y];
                }
            }
        }
        if (!uniquePacking.hasOwnProperty(uniqueId)) { //If it's the first time we encounter this indexing value
            uniquePacking[uniqueId] = [];
        }
        var unique = uniquePacking[uniqueId];
        for (y in objectPacking) {
            if (objectPacking.hasOwnProperty(y)) {
                if (!unique.hasOwnProperty(y)) {
                    unique[y] = [];
                }
                var object = objectPacking[y];
                if (object.hasOwnProperty('id')) {
                    if (unique[y].indexOf(object.id) == -1 && jsonLine.hasOwnProperty(y) && util.isArray(jsonLine[y])) {
                        unique[y].push(object.id);
                        jsonLine[y].push(object);
                    }
                    // else ADDED 12.11.2015 ATERR because cases of joining with views without 'id' property was skipping the result
                } else {
                    jsonLine[y] = objectPacking[y];
                }
            }
        }
        jsonResult[uniqueId] = jsonLine;
    }
    //Transform the object back into an array
    /*
        {indexingField: {label: value, alias: {label:value}, .. }, ..}
        to
        [{label: value, alias: {label: value}, ..}, ..]
    */
    if (options.mode === 'object') {
        return jsonResult;
    }
    var lineResult = [];
    for (y in jsonResult) {
        if (jsonResult.hasOwnProperty(y)) {
            line = jsonResult[y];
            lineResult.push(line);
        }
    }
    return lineResult;
};


/*
Parses the fields for reading.
Usage:
    Table.__parseFieldsRead__(['label', 'label', 'label', 'label'])
    Table.__parseFieldsRead__()
Returns:
    {
        fields: ['"TABLE"."FIELD"', ..., '"TABLE"."FIELD"'],
        mapColumns: ['label', 'label', ..., 'label'],
        tableFields: [{name:'FIELD', type: $.db.types.INTEGER, ..}, ..., <field definition/object>]
    }
*/
Table.prototype.__parseFieldsRead__ = function (fields) {
    var qFields = [];
    var mapColumns = [];
    var tableFields = [];
    //Use either the fields specified or all the fields
    if (fields && util.isArray(fields) && fields.length) {
        //Specified
        for (var i = 0; i < fields.length; i++) {
            var fieldLabel = fields[i];
            if (!this.fields.hasOwnProperty(fieldLabel)) {
                throw 'Requested nonexistent field ' + fieldLabel + ' from Table object';
            }
            mapColumns.push(fieldLabel);
            tableFields.push(this.fields[fieldLabel]);
            qFields.push(this.getTableField(fieldLabel));
        }
    } else {
        //All
        this.fieldKeys = Object.keys(this.fields);
        for (let i = 0; i < this.fieldKeys.length; i++) {
            const key = this.fieldKeys[i];
            mapColumns.push(key);
            var field = this.fields[key];
            tableFields.push(field);
            qFields.push(this.getTableField(key));
        }
        // for (var y in this.fields) {
        //     if (this.fields.hasOwnProperty(y)) {
        //         mapColumns.push(y);
        //         var field = this.fields[y];
        //         tableFields.push(field);
        //         qFields.push(this.getTableField(y));
        //     }
        // }
    }
    return {
        fields: qFields,
        mapColumns: mapColumns,
        tableFields: tableFields
    };
};


/*
Parses the fields with 'as' for reading.
Usage:
    Table.__parseFieldsReadWithAs__([{field: 'label', as: 'newLabel'}, 'label', 'label', 'label'])
Returns:
    {
        fields: ['"TABLE"."newLabel"', ..., '"TABLE"."FIELD"'],
        mapColumns: ['label', 'label', ..., 'label'],
        tableFields: [{name:'FIELD', type: $.db.types.INTEGER, ..}, ..., <field definition/object>]
    }
*/
Table.prototype.__parseFieldsReadWithAs__ = function (fields) {
    var _self = this;
    var qFields = [];
    var mapColumns = [];
    var tableFields = [];
    for (let index = 0; index < fields.length; index++) {
        const field = fields[index];
        let fieldLabel;
        let fieldWithTableAndSchema;
        let as;
        if (typeof field === 'object') {
            if (!field.field || !field.as) {
                throw '"field" and "as" are mandatory attributes in field when it is object';
            }
            fieldLabel = field.field;
            fieldWithTableAndSchema = _self.getTableField(fieldLabel) + ' as "' + field.as + '"';
            as = field.as;
        } else {
            fieldLabel = field;
            fieldWithTableAndSchema = _self.getTableField(fieldLabel);
        }
        if (!_self.fields.hasOwnProperty(fieldLabel)) {
            throw 'Requested nonexistent field "' + fieldLabel + '" from Table object';
        }
        mapColumns.push(as || fieldLabel);
        tableFields.push(_self.fields[fieldLabel]);
        qFields.push(fieldWithTableAndSchema);

    }
    // fields.forEach(function (field) {
    //     let fieldLabel;
    //     let fieldWithTableAndSchema;
    //     let as;
    //     if (typeof field === 'object') {
    //         if (!field.field || !field.as) {
    //             throw '"field" and "as" are mandatory attributes in field when it is object';
    //         }
    //         fieldLabel = field.field;
    //         fieldWithTableAndSchema = _self.getTableField(fieldLabel) + ' as "' + field.as + '"';
    //         as = field.as;
    //     } else {
    //         fieldLabel = field;
    //         fieldWithTableAndSchema = _self.getTableField(fieldLabel);
    //     }
    //     if (!_self.fields.hasOwnProperty(fieldLabel)) {
    //         throw 'Requested nonexistent field "' + fieldLabel + '" from Table object';
    //     }
    //     mapColumns.push(as || fieldLabel);
    //     tableFields.push(_self.fields[fieldLabel]);
    //     qFields.push(fieldWithTableAndSchema);
    // });
    return {
        fields: qFields,
        mapColumns: mapColumns,
        tableFields: tableFields
    };
};


/*
Usage:
    Table.__countQuery__({
        query: 'SELECT FIELD, FIELD FROM TABLE WHERE FIELD = ? GROUP BY FIELD ORDER BY FIELD',
        values: [{type: $.db.type.FIELDTYPE, value: 12}]
    })
Executes:
    'SELECT COUNT(*) FROM TABLE WHERE FIELD = 12 GROUP BY FIELD'
Returns:
    [123]
*/
Table.prototype.__countQuery__ = function (options) {
    var query = options.distinct ? options.query : options.query.replace(/.*FROM\s+/, 'SELECT COUNT(1) FROM').replace(/ORDER BY.*/, '');
    var results = this.sql.SELECT({
        query: query,
        values: options.values
    });
    if (results.length) {
        if (options.distinct) {
            results[0] = results.length;
        } else {
            for (var y = 0; y < results.length; y++) {
                results[y] = results[y][0];
            }
        }
    }
    return results;
};


/*@doc
INTERNAL USE ONLY
Usage:
    Table.__baseUpdate__({
        id:     value
        label:  value,
        ...
        label:  value,
        label:  value
    });
Returns
    {
        query: 'UPDATE table SET ...',
        values: [{type: $.db.types.INTEGER, value: 10}, ...],
        response: {id: 10, foo: 'bar'}
    }
*/
Table.prototype.__baseUpdate__ = function (options) {
    if (!util.isObject(options)) {
        return false;
    }
    if (!options.hasOwnProperty('id') || isNaN(options.id)) {
        throw 'It\'s not possible to perform an UPDATE without an id value: ' + JSON.stringify(options);
    } else {
        if (options.id.toString().substring(0, 1) !== '0') { //ID should be a string if it begins with 0
            options.id = Number(options.id); //ID should be a number in other cases
        }
    }
    var query = 'UPDATE ' + this.name;
    var fields = [];
    var values = [];
    var where = [];
    var response = {};
    this.fieldKeys = Object.keys(this.fields);
    for (let i = 0; i < this.fieldKeys.length; i++) {
        const key = this.fieldKeys[i];
        var valO;
        if (options.hasOwnProperty(key)) {
            if (key === 'id' || key === 'obj_version') {
                where.push({
                    field: key,
                    oper: '=',
                    value: options[key]
                });
            } else if (this.fields[key].instanceOf === 'AutoField') {
                if (!(this.fields[key].hasOwnProperty('update') && this.fields[key].update !== false)) {
                    fields.push(key);
                    valO = this.fields[key].toValue();
                    values.push(valO);
                    response[key] = valO.value;
                }
            } else if (this.fields.hasOwnProperty(key)) {
                fields.push(key);
                valO = this.fields[key].toValue(options[key]);
                values.push(valO);
                response[key] = valO.value;
            }
        } else if (this.fields.hasOwnProperty(key) && this.fields[key].instanceOf === 'AutoField' && !(this.fields[key].hasOwnProperty('update') && this.fields[key].update === false)) {
            fields.push(key);
            valO = this.fields[key].toValue();
            values.push(valO);
            response[key] = valO.value;
        }

    }
    // for (var key in this.fields) {
    //     var valO;
    //     if (options.hasOwnProperty(key)) {
    //         if (key === 'id' || key === 'obj_version') {
    //             where.push({
    //                 field: key,
    //                 oper: '=',
    //                 value: options[key]
    //             });
    //         } else if (this.fields[key].instanceOf === 'AutoField') {
    //             if (!(this.fields[key].hasOwnProperty('update') && this.fields[key].update !== false)) {
    //                 fields.push(key);
    //                 valO = this.fields[key].toValue();
    //                 values.push(valO);
    //                 response[key] = valO.value;
    //             }
    //         } else if (this.fields.hasOwnProperty(key)) {
    //             fields.push(key);
    //             valO = this.fields[key].toValue(options[key]);
    //             values.push(valO);
    //             response[key] = valO.value;
    //         }
    //     } else if (this.fields.hasOwnProperty(key) && this.fields[key].instanceOf === 'AutoField' && !(this.fields[key].hasOwnProperty('update') && this.fields[key].update === false)) {
    //         fields.push(key);
    //         valO = this.fields[key].toValue();
    //         values.push(valO);
    //         response[key] = valO.value;
    //     }
    // }
    fields = this.__parseFieldArray__(fields);
    var _fields = [];
    for (var i = 0; i < fields.length; i++) {
        _fields.push(fields[i] + ' = ?');
    }
    query += ' SET ' + _fields.join(', ') + ' WHERE ' + this.__parseWhere__({
        'where': where,
        'values': values
    });
    return {
        query: query,
        values: values,
        response: response
    };
};


/*@doc
INTERNAL USE ONLY, misuse will cause data inconsistency
Usage:
    Table._UPDATE({
        id:     value
        label:  value,
        ...
        label:  value,
        label:  value
    });
*/
Table.prototype._UPDATE = function (options) {
    var filled = this.__baseUpdate__(options);
    var response = this.sql.UPDATE({
        query: filled.query,
        values: filled.values
    });
    return response;
};


Table.prototype.TRASH = function (id) {
    if (!util.isNumber(id)) {
        throw 'Trash requires an ID, none received';
    }
    return this._UPDATE({
        id: id,
        obj_status: 'Trashed'
    });
};


Table.prototype.LOCK = function (id) {
    if (!util.isNumber(id)) {
        throw 'Lock requires an ID, none received';
    }
    if (this.hasOwnProperty('versioning') && this.versioning === 'simple') {
        var readResult = this.READ({
            where: [{
                field: 'id',
                oper: '=',
                value: id
            }, {
                field: 'obj_status',
                oper: '=',
                value: 'Active'
            }]
        });
        if (!readResult.length) {
            throw 'There is no active version to lock for an object with ID: ' + id;
        } else {
            //clean  update fields
            var readEntry = readResult[0];
            readEntry.obj_version = readEntry.obj_version + 1;
            var systemFields = [];
            if ((this.hasOwnProperty('default_fields') && this.default_fields === 'common') || (this.hasOwnProperty('defaultFields') && this.defaultFields === 'common')) {
                systemFields = systemFields.concat(['creationUser', 'creationDate', 'modificationUser', 'modificationDate']);
            }
            for (var fieldKey in readEntry) {
                if (systemFields.indexOf(fieldKey) !== -1) {
                    delete readEntry[fieldKey];
                }
            }
            var response = this._UPDATE({
                id: id,
                obj_status: 'Locked'
            });
            this._CREATE(readEntry);
            return response;
        }
    }
};


/**
 * Locks edit of assigned id, if the table is lockable
 * 
 * All users can lock something if the user that locked is offline/inexistent
 * No user can lock something if the user that locked is online
 */
Table.prototype.LOCKEDIT = function (id) {
    if (!util.isNumber(id)) {
        throw 'Lock Edit requires an ID, none received';
    }
    if (this.hasOwnProperty('lockable') && this.lockable === true) {
        var editStatus = this.READ({
            fields: ['is_locked', 'usr_locked'],
            where: [{
                field: 'id',
                oper: '=',
                value: id
            }]
        });
        if (editStatus.length !== 1) {
            // We did not find an element with this id
            return false;
        } else {
            editStatus = editStatus[0];
            // if ( editStatus.is_locked ) {
            //     // Is usr_locked online ?
            //     // if online, returns message code access denied
            // } 

            // Lock this up!
            $.session.getUsername();
        }
    }
};


/**
 * Unlocks edit of assigned id, if the table is lockable
 * 
 * Only the user itself can unlock something
 */
Table.prototype.UNLOCKEDIT = function (id) {
    if (!util.isNumber(id)) {
        throw 'Unlock Edit requires an ID, none received';
    }
    if (this.hasOwnProperty('lockable') && this.lockable === true) {
        var editStatus = this.READ({
            fields: ['is_locked', 'usr_locked'],
            where: [{
                field: 'id',
                oper: '=',
                value: id
            }]
        });
        if (editStatus.length !== 1) {
            // We did not find an element with this id
            return false;
        } else {
            editStatus = editStatus[0];
            // if ( editStatus.is_locked ) {
            //     // 
            //     if ( editStatus.user_locked !== $.session.getUsername() ) {
            //         // returns bam!
            //     }
            //     // unlocks!
            // }
            return false;
        }
    }
};


/*
INTERNAL USE ONLY
Usage:
    Table.__parseDelete__(10); //Record ID
Returns
    {
        where: 'ID = ?',
        values: [{value: 10, type: $.db.types.INTEGER}]
    }
*/
Table.prototype.__parseDelete__ = function (id) {
    if (!util.isNumber(id)) {
        throw 'Delete requires an ID, none received';
    }
    var values = [];
    var where = this.__parseWhere__({
        'where': [{
            field: 'id',
            oper: '=',
            value: id
        }],
        'values': values
    });
    return {
        where: where,
        values: values
    };
};


/*
INTERNAL USE ONLY, misuse will cause data inconsistency
Usage:
    Table._DELETE(10);
Reponse:
    true
*/
Table.prototype._DELETE = function (id) {
    var filled = this.__parseDelete__(id);
    var where = filled.where;
    var values = filled.values;
    var query = 'DELETE FROM ' + this.name + ' WHERE ' + where;
    var status = this.sql.DELETE({
        query: query,
        values: values
    });
    return status;
};


/*@doc
Works only with versioning == 'approval'
Usage:
    Table.ACTIVATE()
*/
Table.prototype.ACTIVATE = function (options) {
    var id = options;
    if (isNaN(id)) {
        throw 'Expected an numeric ID, like Table.ACTIVATE(2)\nReceived: ' + JSON.stringify(options);
    }
    var lastVersion = this.historical.READ({
        where: [{
            field: 'id_original_obj',
            oper: '=',
            value: id
        },
            'VERSIONING_ACTION IN (1,2,3)'
        ],
        orderBy: ['-curr_version']
    })[0];
    if (!lastVersion) {
        return 'Nothing to activate';
    }
    var lvCopy = JSON.parse(JSON.stringify(lastVersion)),
        response;
    switch (lastVersion.versioning_action) {
        case 'DELETE':
            response = this._DELETE(Number(id));
            break;
        case 'CREATE':
        case 'UPDATE':
            lastVersion.id = id; //lastVersion.id_original_obj;
            response = this['_' + lastVersion.versioning_action](lastVersion);
            break;
        default:
            return 'Nothing to activate';
    }
    lastVersion.id = lvCopy.id;
    lastVersion.versioning_action = 'DONE';
    return this.historical.UPDATE(lastVersion) && response || false;
};


/*@doc
Removes the latest inactive version
Works only with versioning == 'approval'
Usage:
    Table.REJECT()
*/
Table.prototype.REJECT = function (options) {
    var id = options;
    if (isNaN(id)) {
        throw 'Expected an numeric ID, like Table.ACTIVATE(2)\nReceived: ' + JSON.stringify(options);
    }
    var lastVersion = this.historical.READ({
        where: [{
            field: 'id_original_obj',
            oper: '=',
            value: id
        },
            'VERSIONING_ACTION IN (1,2,3,4,5,6)'
        ],
        orderBy: ['-curr_version']
    });
    //Reject 0
    var response = true;
    if (['CREATE', 'UPDATE', 'DELETE'].indexOf(lastVersion[0].versioning_action) !== -1) { //The lastest: It's open? Reject
        lastVersion[0].versioning_action = 'REJECT ' + lastVersion[0].versioning_action;
        response = response && this.historical.UPDATE(lastVersion[0]);
    }
    if (['SKIP CREATE', 'SKIP UPDATE', 'SKIP DELETE'].indexOf(lastVersion[1].versioning_action) !== -1) { //The second latest: It's skip? Open
        lastVersion[1].versioning_action = lastVersion[1].versioning_action.slice(5);
        response = response && this.historical.UPDATE(lastVersion[1]);
    }
    return response;
};


/*
@doc
Internal use, update/create records that can't be referenced by ID
Usage:
    Table.UPSERTWHERE({ //fields
        field: value,
        field: value,
        field: value
    }, [ //where (optional)
        {field: 'label', oper: '=', value: 12},
        ...
        {field: 'label', oper: 'LIKE', value: '%foo%'}
        {field: 'label', oper: 'IS NULL'}
    ], { //options
        complete (optional with where): true //Adds the fields to the where query
    })
If you don't specify a where clause it will transform the first option into a where clause:
    {field: 'label', oper: '=', value: value},
    {field: 'label', oper: '=', value: value},
    {field: 'label', oper: '=', value: value}
*/
Table.prototype.UPSERTWHERE = function (fields, where, options) {
    if (where && options && options.hasOwnProperty('complete') || !where) {
        where = where || [];
        this.fieldKeys = Object.keys(fields);
        for (let i = 0; i < this.fieldKeys.length; i++) {
            const label = this.fieldKeys[i];
            if (this.fields.hasOwnProperty(label)) {
                var field = this.fields[label];
                if (field.type >= 25) {
                    continue; //Skip LOB fields
                }
                where.push({
                    field: label,
                    oper: '=',
                    value: fields[label]
                });
            }
        }
        // for (var lbl in fields) {
        //     if (this.fields.hasOwnProperty(lbl)) {
        //         var field = this.fields[lbl];
        //         if (field.type >= 25) {
        //             continue; //Skip LOB fields
        //         }
        //         where.push({
        //             field: lbl,
        //             value: fields[lbl],
        //             oper: '='
        //         });
        //     }
        // }
    }
    //CREATE THE PART OF THE STATEMENT THAT IS SIMILAR TO UPDATE
    var query = 'UPSERT ' + this.name;
    var qFields = [];
    var values = [];
    // var response = {};
    var placeholders = [];
    this.fieldKeys = Object.keys(this.fields);
    for (let i = 0; i < this.fieldKeys.length; i++) {
        if (this.fields.hasOwnProperty(y)) {
            const key = this.fieldKeys[i];
            var valO;
            if (this.fields[key].instanceOf === 'AutoField') {
                valO = this.fields[key].toValue();
            } else if (fields.hasOwnProperty(key)) {
                valO = this.fields[key].toValue(fields[key]);
            } else {
                continue;
            }
            qFields.push(this.fields[key].name);
            values.push(valO);
            placeholders.push('?');
        }
    }
    // for (var y in this.fields) {
    //     if (this.fields.hasOwnProperty(y)) {
    //         var valO;
    //         if (this.fields[y].instanceOf === 'AutoField') {
    //             valO = this.fields[y].toValue();
    //         } else if (fields.hasOwnProperty(y)) {
    //             valO = this.fields[y].toValue(fields[y]);
    //         } else {
    //             continue;
    //         }
    //         qFields.push(this.fields[y].name);
    //         values.push(valO);
    //         placeholders.push('?');
    //     }
    // }
    query += ' (' + qFields.join(', ') + ') ';
    query += 'VALUES';
    query += ' (' + placeholders.join(', ') + ') ';
    //CREATE THE WHERE
    query += ' WHERE ' + this.__parseWhere__({
        'where': where,
        'values': values
    });
    //EXECUTE THE QUERY WITH BOOLEAN RESPONSE
    try {
        return this.sql.UPDATE({
            query: query,
            values: values
        });
    } catch (e) {
        throw ['Upsert: Impossible to execute query:\n', this.sql.__translate_stmt__({
            query: query,
            values: values
        }), '\n', values];
    }
};


/*@doc
Returns a public table definition for UI
Usage:
    Table.DEFINITION()
Returns:
    {<table definition>}
*/
Table.prototype.DEFINITION = function () {
    var definition = {
        versioning: this.versioning || false,
        fields: {}
    };
    this.fieldKeys = this.fieldKeys || Object.keys(this.fields);
    for (let i = 0; i < this.fieldKeys.length; i++) {
        const key = this.fieldKeys[i];
        var field = this.fields[key];
        definition.fields[key] = field.definition;
    }
    // for (var fn in this.fields) {
    //     if (this.fields.hasOwnProperty(fn)) {
    //         var field = this.fields[fn];
    //         definition.fields[fn] = field.definition;
    //         // if (field.instanceOf == 'AutoField') {
    //         //     definition.fields[fn] = 'auto';
    //         // } else {
    //         //     
    //         // }
    //     }
    // }
    return definition;
};


/*@doc
Generates the create statement
*/
Table.prototype.CREATE_STATEMENT = function () {
    var stmt = 'CREATE COLUMN TABLE ' + this.name;
    var lines = [];
    var primaryKeys = [];
    var uniqueFlds = [];
    var sql = SQL;
    // this.fieldKeys = Object.keys(this.fields);
    // for (let i = 0; i < this.fieldKeys.length; i++) {
    //     const key = this.fieldKeys[i];
    //     var field = this.fields[key];
    //     var line = [field.key, sql.DATA_TYPES[field.type].key];
    //     if (field.dimension && field.hasOwnProperty('precision') && !isNaN(field.precision) && sql.DATA_TYPES[field.type].dimension && sql.DATA_TYPES[field.type].precision) {
    //         line.push('(' + field.dimension + ', ' + field.precision + ')');
    //     } else if (field.dimension && sql.DATA_TYPES[field.type].dimension) {
    //         line.push('(' + field.dimension + ')');
    //     }
    //     if (field.hasOwnProperty('pk') && field.pk === true) {
    //         primaryKeys.push(field.key);
    //     }
    //     if (field.hasOwnProperty('unique') && field.unique === true) {
    //         uniqueFlds.push(field.key);
    //     }
    //     lines.push(line.join(' '));
    // }
    for (var name in this.fields) {
        var field = this.fields[name];
        var line = [field.name, sql.DATA_TYPES[field.type].name];
        if (field.dimension && field.hasOwnProperty('precision') && !isNaN(field.precision) && sql.DATA_TYPES[field.type].dimension && sql.DATA_TYPES[field.type].precision) {
            line.push('(' + field.dimension + ', ' + field.precision + ')');
        } else if (field.dimension && sql.DATA_TYPES[field.type].dimension) {
            line.push('(' + field.dimension + ')');
        }
        if (field.hasOwnProperty('pk') && field.pk === true) {
            primaryKeys.push(field.name);
        }
        if (field.hasOwnProperty('unique') && field.unique === true) {
            uniqueFlds.push(field.name);
        }
        lines.push(line.join(' '));
    }
    stmt += '(' + lines.join(', ');
    if (uniqueFlds.length) {
        stmt += ', UNIQUE(' + uniqueFlds.join(', ') + ')';
    }
    if (primaryKeys.length) {
        stmt += ', PRIMARY KEY(' + primaryKeys.join(', ') + ')';
    }
    stmt += ')';
    return stmt;
};


/*@doc
Makes a sum with all the numeric values
Usage:
    Table.AGGREGATE({
        fields (optional): ['field', 'field', 'field'],
        aggr: 'SUM'
    })
*/
Table.prototype.AGGREGATE = function (options) {
    //Use either the fields specified or all the fields
    var parseFields = this.__parseFieldsRead__(options.fields || false);
    options.aggr = options.aggr || 'SUM';
    var fields = parseFields.fields;
    // var mapColumns = parseFields.map_columns || parseFields.mapColumns;
    var tableFields = parseFields.table_fields || parseFields.tableFields;
    var query = 'SELECT ';
    var validFields = [];
    for (var y = 0; y < tableFields.length; y++) {
        if (tableFields[y].type <= 7) {
            validFields.push(options.aggr + '(' + fields[y] + ')');
        } else {
            validFields.push('NULL');
        }
    }
    query += validFields.join(', ');
    query += ' FROM ' + this.name;
    var values = [];
    if (options.hasOwnProperty('where') && util.isArray(options.where)) {
        if (this.versioning && this.versioning === 'simple') {
            // throw this.versioning;
            if (options.versions) {
                options.where.push({
                    field: 'obj_status',
                    oper: '!=',
                    value: 'Deleted'
                });
            } else {
                options.where.push({
                    field: 'obj_status',
                    oper: '=',
                    value: 'Active'
                });
            }
        }
        var where;
        var safe = false;
        if (options.join) {
            where = this.__parseWhere__({
                'where': options.where,
                'values': values,
                'join': options.join
            }, safe);
        } else {
            where = this.__parseWhere__({
                'where': options.where,
                'values': values
            }, safe);
        }
        if (util.isString(where) && where.length) {
            if (this.versioning === 'approval' && options.inactive === 'include') {
                where = where.replace(this.name, 'VERSIONS_UNION');
            }
            query += ' WHERE ' + where;
        }
    }
    return this.sql.SELECT({
        query: query,
        values: values
    });
};


Table.prototype.DROP = function () {
    try {
        var query = 'DROP TABLE ' + this.name;
        return this.sql.EXECUTE({
            query: query,
            values: []
        });
    } catch (e) {
        // throw "Error when executing: " + query
    }
};