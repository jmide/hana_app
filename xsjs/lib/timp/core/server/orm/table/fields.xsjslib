$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm','sql');
var SQL = $.timp.core.server.orm.sql;

/*@doc
This class should be declared having in mind that the auto will be generated by:
    SELECT this.auto FROM DUMMY;
That works with SEQUENCE.nextval and NOW();
Example:
    new AutoField({
        name: 'FIELDNAME',
        auto: '"SCHEMA"."SEQUENCE".nextval',
        type: $.db.types.NVARCHAR,
        update (optional): true, //Should this update everytime the record is updated?
        dimension (depends on type): 255, //The field size, for creating/altering the table properly
        pk (optional): true //Adds the field to the PRIMARY KEYS('..') list
    })
*/
function AutoField(options) {
    this.name = JSON.stringify(options.name);
    this.auto = options.auto;
    this.type = options.type;
    this.dimension = options.dimension;
    this.pk = options.pk;
    this.json = options.json;
    this.unique = options.unique;
    this.definition = options;
    this.instanceOf = 'AutoField'; //Dirty fix for messy imports
    this.update = options.update || false;
    if (options.hasOwnProperty('update')) {
        this.update = options.update;
    }
    this.SQL  = SQL;
    this.NEW = function () {
        if (this.auto && this.auto.indexOf('SESSION_USER') !== -1) {
            this.auto = this.auto.replace(/SESSION_USER/g, '\'' + $.session.getUsername() + '\'');
        }
        var response = this.SQL.DUMMY({
            select: this.auto
        });
        while(util.isArray(response)) {
            response = response[0];
        }
        if (!response) {
            return null;
        }
        if (response.constructor.name ==  'Int64') {
            response = Number(response.toString());
        }
        return response;
    };
}
this.AutoField = util.Declare({
    options: {
        name: util.isString,
        auto: util.isString,
        type: util.isDBType,
        dimension: util.isNumber,
        precision: util.isNumber,
        scale: util.isNumber,
        pk : util.isBoolean
    }
}, AutoField);


/*@doc
Method of the AutoField that returns an object in the format:
    {type: $.db.types.TIMESTAMP,    value: '01.02.2003 01:02:03.123',   format: 'DD.MM.YYYY HH:MI:SS.FF'}
Notice 'format' does not apply to all datatypes.
*/
AutoField.prototype.toValue = function(value) {
    var valO;
    if (util.isValid(value)) {
        valO = { value: value, type: this.type };
    } else {
        valO = { value: this.NEW(), type: this.type };
    }

    if (this.hasOwnProperty('format') && util.isString(this.format)) {
        valO.format = this.format;     
    }

    return valO;
};

/*@doc
This class wraps the TABLE.FIELD functionallity.
Example:
    new Field({
        name: 'FIELDNAME',
        type: $.db.types.INTEGER
    })

    new Field({
        name: 'FIELDNAME',
        type: $.db.types.TIMESTAMP,
        format: 'DD.MM.YYYY HH:MI:SS.FF'
    })
    
    new Field({
        name: 'NVARCHAR',
        type: $.db.types.NVARCHAR,
        dimension (depends on type): 255, //The field size, for creating/altering the table properly
        pk (optional): true //Adds the field to the PRIMARY KEYS('..') list
    })
    
    new Field({
        name: 'FOOBAR',
        type: $.db.types.INTEGER,
        translate: {1:'Foo', 2:'Bar'},
        default (optional): 1 //Application level default value (it will be returned to you when you create the object)
    })
*/
function Field(options) {
    this.name = JSON.stringify(options.name);
    this.type = options.type;
    this.dimension = options.dimension;
    this.precision = options.precision;
    this.scale = options.scale;
    this.format = options.format;
    if (this.precision && !this.dimension) {
        this.dimension = 16; //ATR Structures sometimes don't have it, as a quick fix I did set the largest dimension found: 16
    }
    this.pk = options.pk;
    this.json = options.json;
    this.definition = options;
    this.instanceOf = 'Field'; //Dirty fix for messy imports
    //this.fk = (options.hasOwnProperty('fk') && util.isObject(options.fk)) && options.fk || false;
    if(options.hasOwnProperty('translate') && util.isObject(options.translate)) {
        this.translate = options.translate;
    }
    this.default = options.default;
}
this.Field = util.Declare({
    options: {
        name: util.isString,
        type: util.isDBType,
        dimension: util.isNumber,
        pk : util.isBoolean,
        ':format': util.isString
    }
}, Field);

/*@doc
Method of the Field that receives a value and returns an object in the format:
    {type: $.db.types.TIMESTAMP,    value: '01.02.2003 01:02:03.123',   format: 'DD.MM.YYYY HH:MI:SS.FF'}
Notice 'format' does not apply to all datatypes.
*/
Field.prototype.toValue = function (value) {
    if (this.hasOwnProperty('translate')) {
        var newValue;
        if (this.translate.hasOwnProperty(value)) {
            newValue = SQL.DATA_TYPES[this.type].js(value);
        } else {
            for(var x in this.translate) {
                if(this.translate.hasOwnProperty(x) && this.translate[x] == value) {
                    newValue = SQL.DATA_TYPES[this.type].js(x);
                }
            }
        }
        if (typeof newValue == 'undefined') {
            throw 'Found no match for ' + util.parseError(value) + ' on ' + JSON.stringify(this.translate);
        }
        value = newValue;
    }
    var valO = { value: value, type: this.type };
    if (this.hasOwnProperty('format') && util.isString(this.format)) {
        valO.format = this.format;
    }
    return valO;
};