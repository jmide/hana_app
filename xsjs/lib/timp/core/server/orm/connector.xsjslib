$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

/**
 * Executes a query using the hdb client.
 * @returns {Object} status - the status of the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: [{
 * //       isAdmin: true,
 * //       ability: {
 * //           name: 'Second Chance',
 * //           description: 'It allows timp to survive a single fatal blow with 1 HP, provided they have more than 1 HP left.'
 * //       },
 * //       id: 16,
 * //       firstName: 'Daniel',
 * //       lastName: 'Coello',
 * //       active: true
 * //   }]
 * // }
 * @param {string} query - sql query to be executed.
 * @example
 * // 'SELECT * FROM "TIMP"."CORE::User" LIMIT 50 OFFSET 0'
 * @param {string} modelMap - map to transform databaseColumns to the desired column names.
 * @example
 * // {
 * //   IS_ADMIN: {
 * //       name: 'isAdmin',
 * //       type: 'boolean'
 * //   },
 * //   ABILITY: {
 * //       name: 'ability',
 * //       type: 'json'
 * //   },
 * //   ID: {
 * //       name: 'id',
 * //       type: 'integer'
 * //   },
 * //   FIRST_NAME: {
 * //       name: 'firstName',
 * //       type: 'string'
 * //   },
 * //   LAST_NAME: {
 * //       name: 'lastName',
 * //       type: 'string'
 * //   },
 * //   ACTIVE: {
 * //       name: 'active',
 * //       type: 'tinyint',
 * //       translate: {
 * //           1: true,
 * //           2: false
 * //       }
 * //   }
 * // }
 * @param {boolean} isDMLOperation - if the query to be executed modify the table's structure and data or it is a simple data retrieval.
 * @example
 * // true
 */
this.execute = function(query, modelMap, isDMLOperation, raw) {
    let connection;
    raw = lodash.isBoolean(raw) ? raw : false;
    try {
        if (lodash.isString(query)) {
            isDMLOperation = isDMLOperation || false;
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
                connection.executeUpdate("SET 'APPLICATION' = 'VBR_TIMP_HDI'");
            // }
            // connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            //let response = isDMLOperation ? connection.executeUpdate(query) : connection.executeQuery(query);
            let response = null;
            if(isDMLOperation){
                response = connection.executeUpdate(query);
            }else{
                response = connection.executeQuery(query);
            }
            
            connection.close();
            let results = [];
            if (raw) {
                results = response;
            } else if (!lodash.isEmpty(response)) {
                let tempInstance;
                let iterator = response.getIterator();
                let instance;
                while (iterator.next()) {
                    instance = iterator.value();
                    tempInstance = resources.parseInstanceFromDBToORM(instance, modelMap);
                    results.push(tempInstance);
                }
            }
            return {
                '@query': query,
                errors: [],
                results: results
            };
        }
        return {
            errors: [{
                '@query': query,
                name: 'Invalid query.',
                description: 'Query(' + query + ')isn\' a string.'
            }],
            results: []
        };
    } catch (e) {
        if (connection && connection.close) {
            connection.close();
        }
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code + ')';
            let response = connection.executeQuery(query);
            connection.close();
            error = {
                name: response[0].name,
                description: response[0].description
            };
        } else {
            error = resources.parseError(e);
        }
        error['@query'] = query;
        return {
            '@query': query,
            errors: [error],
            results: []
        };
    }
};
$.execute = this.execute;
/**
 * Executes a executeUpdate using the hdb client.
 * @returns {Object} status - the status of the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       created: true,
 * //   }
 * // }
 * @param {string} query - sql query to be executed.
 * @example
 * // 'INSERT INTP "TEST"("ID", "NAME") VALUES(?,?)'
 * @param {Array} values - Array of value's array to be inserted into the database
 * @example
 * // [[1, 'a'], [2, 'b'], [3, 'c']]
 */
this.executeUpdate = function(query, values) {
    let retVal = {
        '@query': query,
        '@values': values,
        errors: [],
        results: {
            created: false
        }
    };
    let connection;
    try {
        if (lodash.isString(query) && lodash.isArray(values)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            let response = null;
            if (lodash.isArray(values) && (lodash.isEmpty(values) || (!lodash.isEmpty(values) && lodash.isEmpty(values[0])))) {
                response = connection.executeUpdate(query);
            } else {
                response = connection.executeUpdate(query, values);
            }
            connection.close();
            let errorCode = {};
            lodash.every(response, function(status) {
                if (status !== 1) {
                    errorCode.code = status;
                    retVal.results.created = false;
                    return false;
                }
                return true;
            });
            if (lodash.isInteger(errorCode.code) && errorCode.code !== 1) {
                throw errorCode;
            } else {
                retVal.results.created = true;
            }
        } else {
            let tempError = resources.generateError('Invalid Params', 'Query must be a string and values an array of arrays.');
            tempError['@query'] = query;
            tempError['@values'] = values;
            retVal.errors.push(tempError);
        }

        return retVal;
    } catch (e) {
        connection.close();
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // let connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code + ')';
            let response = connection.executeQuery(query);
            connection.close();
            error = {
                name: response[0].name,
                description: response[0].description
            };
        } else {
            error = resources.parseError(e);
        }
        error['@query'] = query;
        error['@values'] = values;
        retVal.errors.push(error);
        return retVal;
    }
};
$.executeUpdate = this.executeUpdate;
/**
 * Returns the specified proc.
 * @returns {Object} status - the status of the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       proc: function(),
 * //   }
 * // }
 * @param {string} schema - schema where the proc is located.
 * @example
 * // 'TIMP'
 * @param {string} name - name of the proc
 * @example
 * // 'UPDATE_AUDIT'
 */
this.loadProcedure = function(schema, name) {
    let retVal = {
        errors: [],
        results: {
            procedure: null,
            connection: null
        }
    };
    let connection;
    try {
        if (lodash.isString(schema) && lodash.isString(name)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            retVal.results.procedure = connection.loadProcedure(schema, name);
            retVal.results.connection = connection;
        } else {
            let tempError = resources.generateError('Invalid Params', 'Schema must be a string and name a string.');
            retVal.errors.push(tempError);
        }

        return retVal;
    } catch (e) {
        connection.close();
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // let connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = ABS(' + e.code + ')';
            let response = connection.executeQuery(query);
            connection.close();
            error = {
                name: response[0].name,
                description: response[0].description
            };
        } else {
            error = resources.parseError(e);
        }
        retVal.errors.push(error);
        return retVal;
    }
};
$.loadProcedure = this.loadProcedure;