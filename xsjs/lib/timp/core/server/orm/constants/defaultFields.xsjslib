this.getDefaultFields = function (type) {
	let defaultFields = {};
	switch (type) {
		case 'basic':
			{
				defaultFields = {
					id: {
						type: 'integer',
						primaryKey: true,
						autoIncrement: true,
						columnName: 'ID'
					},
					creationUser: {
						type: 'integer',
						columnName: 'CREATION_USER',
						'default': 'CURRENT_USER'
					},
					creationDate: {
						type: 'datetime',
						columnName: 'CREATION_DATE',
						'default': 'CURRENT_TIMESTAMP'
					}
				};
				break;
			}
		case 'complete':
			{
				defaultFields = {
					id: {
						type: 'integer',
						primaryKey: true,
						autoIncrement: true,
						columnName: 'ID'
					},
					creationUser: {
						type: 'integer',
						columnName: 'CREATION_USER',
						'default': 'CURRENT_USER'
					},
					creationDate: {
						type: 'datetime',
						columnName: 'CREATION_DATE',
						'default': 'CURRENT_TIMESTAMP'
					},
					modificationUser: {
						type: 'integer',
						columnName: 'MODIFICATION_USER',
						'default': 'CURRENT_USER',
						onUpdate: 'CURRENT_USER'
					},
					modificationDate: {
						type: 'datetime',
						columnName: 'MODIFICATION_DATE',
						'default': 'CURRENT_TIMESTAMP',
						onUpdate: 'CURRENT_TIMESTAMP'
					}
				};
				break;
			}
		case 'EEF':
			{
				defaultFields = {
					companyId: {
						type: 'string',
						columnName: 'COMPANY_ID',
						size: 4
					},
					stateId: {
						type: 'string',
						columnName: 'STATE_ID',
						size: 2
					},
					branchId: {
						type: 'string',
						columnName: 'BRANCH_ID',
						size: 4
					}
				};
				break;
			}
		case 'EEFI':
			{
				defaultFields = {
					companyId: {
						type: 'string',
						columnName: 'COMPANY_ID',
						size: 4
					},
					stateId: {
						type: 'string',
						columnName: 'STATE_ID',
						size: 2
					},
					branchId: {
						type: 'string',
						columnName: 'BRANCH_ID',
						size: 4
					},
					taxId: {
						type: 'string',
						columnName: 'TAX_ID',
						size: 2
					}
				};
				break;
			}
		case 'EEFIPrimary':
			{
				defaultFields = {
					companyId: {
						type: 'string',
						columnName: 'COMPANY_ID',
						size: 4,
						primaryKey: true
					},
					stateId: {
						type: 'string',
						columnName: 'STATE_ID',
						size: 2,
						primaryKey: true
					},
					branchId: {
						type: 'string',
						columnName: 'BRANCH_ID',
						size: 4,
						primaryKey: true
					},
					taxId: {
						type: 'string',
						columnName: 'TAX_ID',
						size: 2,
						primaryKey: true
					}
				};
				break;
			}
		case 'metadata':
			{
				defaultFields = {
					name: {
						type: 'string',
						columnName: 'NAME',
						size: 128
					},
					description: {
						type: 'string',
						columnName: 'DESCRIPTION',
						size: 255
					}
				};
				break;
			}
		case 'effectiveDate':
			{
				defaultFields = {
					effectiveDateFrom: {
						type: 'datetime',
						columnName: 'EFFECTIVE_DATE_FROM',
						required: true
					},
					effectiveDateTo: {
						type: 'datetime',
						columnName: 'EFFECTIVE_DATE_TO'
					}
				};
				break;
			}
		case 'log':
			{
				defaultFields = {
					creationUser: {
						type: 'integer',
						columnName: 'CREATION_USER',
						'default': 'CURRENT_USER'
					},
					creationDate: {
						type: 'datetime',
						columnName: 'CREATION_DATE',
						'default': 'CURRENT_TIMESTAMP'
					},
					modificationUser: {
						type: 'integer',
						columnName: 'MODIFICATION_USER',
						'default': 'CURRENT_USER',
						onUpdate: 'CURRENT_USER'
					},
					modificationDate: {
						type: 'datetime',
						columnName: 'MODIFICATION_DATE',
						'default': 'CURRENT_TIMESTAMP',
						onUpdate: 'CURRENT_TIMESTAMP'
					}
				};
				break;
			}
	}
	return defaultFields;
};