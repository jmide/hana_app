this.getAdapterFunctionsMap = function() {
	return {
		'TO_DATE': {
			'sqlName': 'TO_DATE',
			'paramsTypes': ['string', 'string'],
			'params': 2,
			'requiredParamsToExecute': 1,
			'returnType': 'date'
		},
		'TO_DATS': {
			'sqlName': 'TO_DATS',
			'paramsTypes': ['string'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'any'
		},
		'TO_TIMESTAMP': {
			'sqlName': 'TO_TIMESTAMP',
			'paramsTypes': ['string', 'string'],
			'params': 2,
			'requiredParamsToExecute': 1,
			'returnType': 'datetime'
		},
		'TO_NVARCHAR': {
			'sqlName': 'TO_NVARCHAR',
			'paramsTypes': ['any', 'string'],
			'params': 2,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'TO_INTEGER': {
		    'sqlName': 'TO_INTEGER',
			'paramsTypes': ['any'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'TO_DECIMAL': {
			'sqlName': 'TO_DECIMAL',
			'paramsTypes': ['any', 'integer', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 1,
			'returnType': 'decimal'
		},
		'UPPER': {
			'sqlName': 'UPPER',
			'paramsTypes': ['string'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'LOWER': {
			'sqlName': 'LOWER',
			'paramsTypes': ['string'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'LEFT': {
			'sqlName': 'LEFT',
			'paramsTypes': ['string', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'RIGHT': {
			'sqlName': 'RIGHT',
			'paramsTypes': ['string', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'LENGTH': {
			'sqlName': 'LENGTH',
			'paramsTypes': ['string'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'SUBSTRING': {
			'sqlName': 'SUBSTRING',
			'paramsTypes': ['string', 'integer', 'integer'],
			'params': 3,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'STRING_AGG': {
			'sqlName': 'STRING_AGG',
			'paramsTypes': ['string', 'string'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'CONCAT': {
			'sqlName': 'CONCAT',
			'paramsTypes': ['string', 'string'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'REPLACE': {
			'sqlName': 'REPLACE',
			'paramsTypes': ['string', 'string', 'string'],
			'params': 3,
			'requiredParamsToExecute': 3,
			'returnType': 'string'
		},
		'RPAD': {
			'sqlName': 'RPAD',
			'paramsTypes': ['string', 'integer', 'string'],
			'params': 3,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'LPAD': {
			'sqlName': 'LPAD',
			'paramsTypes': ['string', 'integer', 'string'],
			'params': 3,
			'requiredParamsToExecute': 2,
			'returnType': 'string'
		},
		'ROUND': {
			'sqlName': 'ROUND',
			'paramsTypes': ['decimal', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'decimal'
		},
		'CEIL': {
			'sqlName': 'CEIL',
			'paramsTypes': ['decimal'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'FLOOR': {
			'sqlName': 'FLOOR',
			'paramsTypes': ['decimal'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'IFNULL': {
			'sqlName': 'IFNULL',
			'paramsTypes': ['any', 'any'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'any'
		},
		'NULLIF': {
			'sqlName': 'NULLIF',
			'paramsTypes': ['any', 'any'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'any'
		},
		'DAYNAME': {
			'sqlName': 'DAYNAME',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'DAYOFMONTH': {
			'sqlName': 'DAYOFMONTH',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'DAYOFYEAR': {
			'sqlName': 'DAYOFYEAR',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'MONTH': {
			'sqlName': 'MONTH',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'MONTHNAME': {
			'sqlName': 'MONTHNAME',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'string'
		},
		'YEAR': {
			'sqlName': 'YEAR',
			'paramsTypes': ['date'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'integer'
		},
		'ADD_DAYS': {
			'sqlName': 'ADD_DAYS',
			'paramsTypes': ['date', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'date'
		},
		'ADD_MONTHS': {
			'sqlName': 'ADD_MONTHS',
			'paramsTypes': ['date', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'date'
		},
		'ADD_YEARS': {
			'sqlName': 'ADD_YEARS',
			'paramsTypes': ['date', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'date'
		},
		'SCORE': {
			'sqlName': 'SCORE',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'returnType': 'decimal'
		},
		'FUZZY': {
			'sqlName': 'FUZZY',
			'paramsTypes': ['decimal', 'string'],
			'params': 2,
			'requiredParamsToExecute': 1,
			'returnType': 'boolean'
		},
		'OVER': {
			'sqlName': 'OVER',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'returnType': 'decimal'
		},
		'COUNT_OVER': {
			'sqlName': 'COUNT_OVER',
			'paramsTypes': ['any'],
			'params': 1,
			'requiredParamsToExecute': 1,
			'returnType': 'decimal'
		},
		'MOD': {
			'sqlName': 'MOD',
			'paramsTypes': ['integer', 'integer'],
			'params': 2,
			'requiredParamsToExecute': 2,
			'returnType': 'integer'
		}
	};
};