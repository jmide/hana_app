this.getValidMethods = function() {
	return {
		'emptyMatchesNull': {
			'sqlName': 'emptyMatchesNull',
			'paramsTypes': ['string'],
			'params': 1,
			'validParams': ['true', 'false', 'on', 'off']
		},
		'emptyScore': {
			'sqlName': 'emptyScore',
			'paramsTypes': ['decimal'],
			'params': 1,
			'validParams': []
		},
		'textSearch': {
			'sqlName': 'textSearch',
			'paramsTypes': ['string'],
			'params': 1,
			'validParams': ['fulltext', 'compare']
		},
		'similarCalculationMode': {
		    'sqlName': 'similarCalculationMode',
			'paramsTypes': ['string'],
			'params': 1,
			'validParams': ['substringsearch', 'compare', 'symmetricsearch', 'searchCompare']
		}
	};
};