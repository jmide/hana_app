this.getAdapterHints = function() {
	return {
		'NO_USE_OLAP_PLAN': {
			'sqlName': 'NO_USE_OLAP_PLAN',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'USE_OLAP_PLAN': {
			'sqlName': 'USE_OLAP_PLAN',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'IGNORE_PLAN_CACHE': {
			'sqlName': 'IGNORE_PLAN_CACHE',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'USE_REMOTE_CACHE': {
			'sqlName': 'USE_REMOTE_CACHE',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'DATA_TRANSFER_COST': {
			'sqlName': 'DATA_TRANSFER_COST',
			'paramsTypes': ['integer'],
			'params': 1,
			'requiredParamsToExecute': 1
		},
		'MAX_CONCURRENCY': {
			'sqlName': 'MAX_CONCURRENCY',
			'paramsTypes': ['integer'],
			'params': 1,
			'requiredParamsToExecute': 1
		},
		'INDEX_SEARCH': {
			'sqlName': 'INDEX_SEARCH',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'NO_INDEX_SEARCH': {
			'sqlName': 'NO_INDEX_SEARCH',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'NO_COLUMN_VIEW_ESTIMATION': {
			'sqlName': 'NO_COLUMN_VIEW_ESTIMATION',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'COLUMN_VIEW_ESTIMATION': {
			'sqlName': 'COLUMN_VIEW_ESTIMATION',
			'paramsTypes': ['any'],
			'params': 1,
			'requiredParamsToExecute': 0
		},
		'SIMPLE': {
			'sqlName': 'SIMPLE',
			'paramsTypes': [],
			'params': 0,
			'requiredParamsToExecute': 0,
			'isConstant': true
		},
		'SUBPLAN_SHARING': {
			'sqlName': 'SUBPLAN_SHARING',
			'paramsTypes': ['model'],
			'params': 1,
			'requiredParamsToExecute': 1
		},
		'NO_SUBPLAN_SHARING': {
			'sqlName': 'NO_SUBPLAN_SHARING',
			'paramsTypes': ['model'],
			'params': 1,
			'requiredParamsToExecute': 1
		}
	};
};