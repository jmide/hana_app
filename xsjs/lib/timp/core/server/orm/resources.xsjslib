$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;
const _this = this;

$.import('timp.core.server.orm', 'table');
const Table = $.timp.core.server.orm.table.Table;


$.import('timp.core.server.orm.constants', 'adapterFunctions');
$.import('timp.core.server.orm.constants', 'adapterHints');
$.import('timp.core.server.orm.constants', 'adapterMethods');
$.import('timp.core.server.orm.constants', 'defaultFields');
$.import('timp.core.server.orm.constants', 'sqlStatementsConstants');

let importFiles = [
    $.timp.core.server.orm.constants.adapterFunctions,
    $.timp.core.server.orm.constants.adapterHints,
    $.timp.core.server.orm.constants.adapterMethods,
    $.timp.core.server.orm.constants.defaultFields,
    $.timp.core.server.orm.constants.sqlStatementsConstants
];

lodash.forEach(importFiles, function(_import) {
    lodash.forEach(lodash.keys(_import), function(value) {
        if (_import.hasOwnProperty(value) && lodash.isFunction(_import[value])) {
            _this[value] = _import[value];
        }
    });
});

this.getSQLMap = function() {
    return {
        'tinyint': 'TINYINT',
        'string': 'NVARCHAR',
        'text': 'TEXT',
        'bigint': 'BIGINT',
        'integer': 'INTEGER',
        'decimal': 'DECIMAL',
        'date': 'DATE',
        'datetime': 'TIMESTAMP',
        'boolean': 'BOOLEAN',
        'binary': 'BINARY',
        'json': 'NCLOB',
        'shorttext': 'SHORTTEXT',
        'file': 'BLOB',
        'time': 'TIME'
    };
};

this.getPlaceHolders = function(inputParameters) {
    let params = $.lodash.isArray(inputParameters) ? inputParameters : $.lodash.reduce(inputParameters, function(resp, par, key) {
        return {
            ID: resp.length + 1,
            hanaName: key,
            isMandatory: true,
            value: par.defaultValue || undefined
        };
    }, []);
    let placeholders = $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], true, params);
    return lodash.map(placeholders, function(value, name) {
        return {
            name: name,
            values: [value]
        };
    });
};

this.getStringFormatPlaceHolders = function(){
    let response = '(';
    let arrayPlaceHolders = this.getPlaceHolders();
    arrayPlaceHolders.forEach(function(placeHolder,index){
        if((arrayPlaceHolders.length - 1 ) === index){
            response += "'PLACEHOLDER' = ('$$" + placeHolder.name + "$$','" + placeHolder.values[0] + "')";
            return;
        }
        response += "'PLACEHOLDER' = ('$$" + placeHolder.name + "$$','" + placeHolder.values[0] + "'),";
    });
    return response += ')';
};

this.getSQLToORMMap = function() {
    return {
        'TIMESTAMP': 'datetime',
        'INTEGER': 'integer',
        'NVARCHAR': 'string',
        'NCLOB': 'json',
        'CLOB': 'json',
        'TINYINT': 'tinyint',
        'DATE': 'date',
        'DECIMAL': 'decimal',
        'DOUBLE': 'decimal',
        'VARCHAR': 'string',
        'TEXT': 'text',
        'BLOB': 'file',
        'BIGINT': 'bigint',
        'SMALLINT': 'tinyint',
        'CHAR': 'string',
        'VARBINARY': 'binary',
        'BOOLEAN': 'boolean',
        'BINARY': 'binary',
        'SHORTTEXT': 'shorttext',
        'TIME': 'time'
    };
};

this.getInvalidTypesForSets = function() {
    return ['json', 'file', 'text', 'shorttext'];
};

this.getValidIndexTypes = function() {
    return {
        'default': '',
        'cpbtree': 'CPBTREE',
        'unique': 'UNIQUE',
        'btree': 'BTREE'
    };
};

this.getTypes = function() {
    return [
        'string',
        'text',
        'tinyint',
        'bigint',
        'integer',
        'decimal',
        'date',
        'datetime',
        'boolean',
        'binary',
        'json',
        'shorttext',
        'file',
        'time'
    ];
};

this.getValidJoins = function() {
    return ['inner', 'full', 'right', 'left'];
};

this.getJoinMap = function() {
    return {
        'inner': 'INNER JOIN',
        'full': 'FULL OUTER JOIN',
        'right': 'RIGHT JOIN',
        'left': 'LEFT JOIN'
    };
};

this.getValidSetOperations = function() {
    return ['union', 'unionall', 'intersect', 'minus', 'except'];
};

this.getSetOperationsMap = function() {
    return {
        'union': 'UNION',
        'unionall': 'UNION ALL',
        'intersect': 'INTERSECT',
        'minus': 'MINUS',
        'except': 'EXCEPT'
    };
};

this.getAdapterAggregationMap = function() {
    return {
        'SUM': {
            'sqlName': 'SUM',
            'returnType': 'decimal'
        },
        'COUNT': {
            'sqlName': 'COUNT',
            'returnType': 'integer'
        },
        'AVG': {
            'sqlName': 'AVG',
            'returnType': 'decimal'
        },
        'MAX': {
            'sqlName': 'MAX',
            'returnType': 'any'
        },
        'MIN': {
            'sqlName': 'MIN',
            'returnType': 'any'
        }
    };
};

this.getValidOperators = function() {
    return {
        '$gt': '>',
        '$gte': '>=',
        '$lt': '<',
        '$lte': '<=',
        '$eq': '=',
        '$neq': '<>',
        '$between': 'BETWEEN',
        '$notBetween': 'NOT BETWEEN',
        '$like': 'LIKE',
        '$likeRegex': 'LIKE_REGEXPR',
        '$notLike': 'NOT LIKE',
        '$in': 'IN',
        '$notIn': 'NOT IN',
        '$eqNull': 'IS NULL',
        '$neqNull': 'IS NOT NULL',
        '$contains': 'CONTAINS',
        '$exists': 'EXISTS',
        '$notExists': 'NOT EXISTS'
    };
};

this.getWhereOperators = function() {
    return [
        '$gt',
        '$gte',
        '$lt',
        '$lte',
        '$eq',
        '$neq',
        '$between',
        '$in',
        '$like',
        '$likeRegex',
        '$notBetween',
        '$notIn',
        '$notLike',
        '$contains',
        '$exists',
        '$notExists'
    ];
};

this.getSQLConstants = function() {
    return ['current_time',
        'current_date',
        'current_timestamp',
        'current_utcdate',
        'current_utctime',
        'current_utctimestamp'
    ];
};

this.getCalculatedColumnsOperations = function() {
    return {
        '+': {
            'operator': '+',
            'paramsTypes': ['decimal', 'decimal'],
            'params': 2,
            'returnType': 'decimal'
        },
        '-': {
            'operator': '-',
            'paramsTypes': ['decimal', 'decimal'],
            'params': 2,
            'returnType': 'decimal'
        },
        '*': {
            'operator': '*',
            'paramsTypes': ['decimal', 'decimal'],
            'params': 2,
            'returnType': 'decimal'
        },
        '/': {
            'operator': '/',
            'paramsTypes': ['decimal', 'decimal'],
            'params': 2,
            'returnType': 'decimal'
        },
        '%': {
            'operator': '%',
            'paramsTypes': ['decimal', 'decimal'],
            'params': 2,
            'returnType': 'decimal'
        }
    };
};

this.getCore1TocoreConversionTypes = function() {
    return {
        1: {
            'name': 'TINYINT',
            'type': 'tinyint'
        },
        2: {
            'name': 'SMALLINT',
            'type': 'tinyint'
        },
        3: {
            'name': 'INTEGER',
            'type': 'integer'
        },
        4: {
            'name': 'BIGINT',
            'type': 'bigint'
        },
        5: {
            'name': 'DECIMAL',
            'type': 'decimal'
        },
        6: {
            'name': 'REAL',
            'type': 'decimal'
        },
        7: {
            'name': 'DOUBLE',
            'type': 'decimal'
        },
        8: {
            'name': 'CHAR',
            'type': 'string'
        },
        9: {
            'name': 'VARCHAR',
            'type': 'string'
        },
        10: {
            'name': 'NCHAR',
            'type': 'string'
        },
        11: {
            'name': 'NVARCHAR',
            'type': 'string'
        },
        12: {
            'name': 'BINARY',
            'type': 'binary'
        },
        13: {
            'name': 'VARBINARY',
            'type': 'binary'
        },
        14: {
            'name': 'DATE',
            'type': 'date'
        },
        15: {
            'name': 'TIME',
            'type': 'time'
        },
        16: {
            'name': 'TIMESTAMP',
            'type': 'datetime'
        },
        25: {
            'name': 'CLOB',
            'type': 'json'
        },
        26: {
            'name': 'NCLOB',
            'type': 'json'
        },
        27: {
            'name': 'BLOB',
            'type': 'file'
        },
        47: {
            'name': 'SMALLDECIMAL',
            'type': 'decimal'
        },
        51: {
            'name': 'TEXT',
            'type': 'text'
        },
        52: {
            'name': 'SHORTTEXT',
            'type': 'shorttext'
        }
    };
};

this.getHanaValue = function(hanaValue, value, withoutUsingConstantsAndFunctions) {
    const type = hanaValue && hanaValue.type || hanaValue;
    const columnName = hanaValue && hanaValue.columnName || '';
    if (lodash.isNil(value)) {
        return 'NULL';
    }
    /*
        [
            'binary',
            'file'
        ];
    */
    withoutUsingConstantsAndFunctions = lodash.isBoolean(withoutUsingConstantsAndFunctions) ? withoutUsingConstantsAndFunctions : false;
    /*
        return ['current_time',
        'current_date',
        'current_timestamp',
        'current_utcdate',
        'current_utctime',
        'current_utctimestamp'
    ];
    */
    const hanaConstants = _this.getSQLConstants();
    const sqlDefaultStatements = _this.getSQLStatementsConstants();
    const escape = ['string', 'text', 'json', 'shorttext'];
    if (type === 'json' && typeof value === 'object') {
        value = JSON.stringify(value);
    }
    if (lodash.isString(value) && lodash.indexOf(hanaConstants, lodash.toLower(value)) > -1) {
        if (withoutUsingConstantsAndFunctions) {
            if (lodash.toLower(value) === 'current_time' || lodash.toLower(value) === 'current_utctime') {
                value = new Date().toISOString().split('T')[1];
                value = value.substring(0, value.length - 1);
            } else if (lodash.toLower(value) === 'current_date' || lodash.toLower(value) === 'current_utcdate') {
                value = new Date().toISOString().split('T')[0];
            } else if (lodash.toLower(value) === 'current_timestamp' || lodash.toLower(value) === 'current_utctimestamp') {
                value = new Date().toISOString();
                if (!$.lodash.isNil($.timeForLog)) {
                    var timeNow = new Date().getTime();
                    var timeForLog = Number($.timeForLog);
                    var timeSinceRequestStarted = timeNow - $.requestInitialTime;
                    var currentTimestamp = new Date(timeForLog + timeSinceRequestStarted);
                    var timezoneOffset = currentTimestamp.getTimezoneOffset();
                    currentTimestamp.setTime(currentTimestamp.getTime() - (3600000 * (timezoneOffset / 60)));
                    value = currentTimestamp.toISOString();
                } else {
                    let currentTimestamp = new Date();
                    let timezoneOffset = currentTimestamp.getTimezoneOffset();
                    currentTimestamp.setTime(currentTimestamp.getTime() - (3600000 * (timezoneOffset / 60)));
                    value = currentTimestamp.toISOString();
                }
            }
        } else {
            return value;
        }
    }
    if (lodash.isString(value) && lodash.has(sqlDefaultStatements, lodash.toLower(value)) && !withoutUsingConstantsAndFunctions) {
        return '(' + sqlDefaultStatements[value] + ')';
    }
    if (escape.indexOf(type) > -1) {
        value = value && value.replace && value.replace(new RegExp('\'', 'g'), '\'\'') || value;
    } else if (type === 'date') {
        if (withoutUsingConstantsAndFunctions) {
            value = new Date(value).toISOString().split('T')[0];
        } else {
            return 'TO_DATE(\'' + new Date(value).toISOString() + '\')';
        }
    } else if (type === 'datetime') {
        value = new Date(value).toISOString();
    } else if (type === 'time') {
        if (!withoutUsingConstantsAndFunctions) {
            return 'TO_TIME(\'' + value + '\')';
        }
    } else if (lodash.indexOf(['tinyint', 'bigint', 'integer', 'decimal'], type) > -1 && !lodash.isNumber(value)) {
        if(lodash.isObjectLike(value) && value.operator === '$inc'){
            return 'IFNULL("' + columnName + '",0)' + ' + ' + value.value;
        }
        return 'NULL';
    } else if (lodash.isEqual('boolean', type) && !lodash.isBoolean(value)) {
        return 'NULL';
    } else if ('file' === type) {
        return value;
    }

    return '\'' + value + '\'';
};

this.getTranslateValue = function(definition, value) {
    let parsedValue = null;
    if (lodash.has(definition.$translate, value)) {
        parsedValue = definition.$translate[value];
        if (lodash.indexOf(['tinyint', 'integer', 'bigint', 'decimal'], definition.type) > -1) {
            parsedValue = Number(parsedValue);
        } else if (definition.type === 'boolean') {
            parsedValue = lodash.isString(parsedValue) ? (lodash.toLower(parsedValue) === 'true') : (lodash.isBoolean(parsedValue) ? parsedValue :
                false);
        }
    }
    return parsedValue;
};

this.generateError = function(name, trace) {
    return {
        name: name,
        trace: trace
    };
};

this.getValidConversions = function() {
    let retVal = {
        errors: [],
        results: {}
    };
    const hanaToORMMap = {
        'tinyint': ['TINYINT'],
        'string': ['NVARCHAR', 'VARCHAR', 'CHAR'],
        'text': ['TEXT'],
        'bigint': ['BIGINT'],
        'integer': ['INTEGER'],
        'decimal': ['DECIMAL', 'DOUBLE'],
        'date': ['DATE'],
        'datetime': ['TIMESTAMP'],
        'boolean': ['BOOLEAN'],
        'binary': ['BINARY', 'VARBINARY'],
        'json': ['NCLOB', 'CLOB'],
        'shorttext': ['SHORTTEXT'],
        'file': ['BLOB'],
        'time': ['TIME']
    };
    const hanaTypesToORM = {};
    lodash.forEach(hanaToORMMap, function(value, key) {
        lodash.forEach(value, function(type) {
            hanaTypesToORM[type] = key;
        });
    });
    let validHanaTypes = lodash.reduce(hanaToORMMap, function(status, value) {
        return lodash.union(status, value);
    }, []);

    try {
        const dataTypeModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::DATA_TYPE_CONVERSION', false, true);
        let response;
        try {
            response = dataTypeModel.find({
                ignoreCache: true,
                select: [{
                    field: 'SOURCE',
                    as: 'source'
                }, {
                    function: {
                        name: 'STRING_AGG',
                        params: [{
                            field: 'TARGET'
                        }, ',']
                    },
                    as: 'targets'
                }],
                where: [{
                    field: 'SOURCE',
                    operator: '$in',
                    value: validHanaTypes
                }, {
                    field: 'TARGET',
                    operator: '$in',
                    value: validHanaTypes
                }],
                orderBy: [{
                    field: 'SOURCE'
                }],
                groupBy: [{
                    field: 'SOURCE'
                }]
            });
        } catch (e) {
            response = e;
        }
        if (!lodash.isEmpty(response.errors)) {
            retVal.errors = lodash.concat(response.errors, retVal.errors);
            throw retVal;
        } else if (lodash.isEmpty(response.results)) {
            retVal.errors.push(_this.generateError('Empty Table', 'Data Type Conversion table is empty.'));
            throw retVal;
        }
        let temp;
        response.results = lodash.map(response.results, function(item) {
            item.targets = lodash.split(item.targets, ',');
            item.targets.push(item.source);
            temp = {
                source: hanaTypesToORM[item.source],
                targets: lodash.uniq(lodash.map(item.targets, function(target) {
                    return hanaTypesToORM[target];
                }))
            };
            return temp;
        });
        let finalMap = {};
        lodash.forEach(response.results, function(item) {
            if (lodash.has(finalMap, item.source)) {
                finalMap[item.source] = lodash.union(finalMap[item.source], item.targets);
            } else {
                finalMap[item.source] = item.targets;
            }
        });
        // We add these values because of the ORM's semantics.
        finalMap.json = ['shorttext', 'text', 'string', 'json'];
        finalMap.file = ['shorttext', 'text', 'string', 'file'];
        retVal.results = lodash.cloneDeep(finalMap);
        return retVal;
    } catch (e) {
        let error = _this.parseError(e);
        if (!lodash.isEqual(retVal, error)) {
            retVal.errors.push(error);
        }
        return retVal;
    }
};

this.parseImportValue = function(value, attributeMap, validConversions, usedAPreprocessFunction, useATranslateValue) {
    // Parse results with the preProcessFn and convert the values depending of their type 
    // (Remember to send the valid types conversion and to substring the strings depending of their dimension.)
    let retVal = {
        errors: [],
        results: {
            isValid: true,
            value: null
        }
    };
    try {
        if (lodash.isNil(value)) {
            retVal.results.value = null;
        } else if (useATranslateValue) {
            retVal.results.value = value;
        } else if (lodash.indexOf(['bigint', 'decimal', 'integer', 'tinyint'], attributeMap.targetMetadata.type) > -1 || (usedAPreprocessFunction &&
                lodash.isNumber(value))) {
            retVal.results.value = Number(value);
        } else if (lodash.indexOf(['string', 'shorttext', 'text'], attributeMap.targetMetadata.type) > -1 || (usedAPreprocessFunction &&
                lodash.isString(value))) {
            retVal.results.value = (lodash.isPlainObject(value) || lodash.isArray(value)) ? JSON.stringify(value) : String(value);
            if (retVal.results.value.length > attributeMap.targetMetadata.size) {
                retVal.results.value = retVal.results.value.substring(0, attributeMap.targetMetadata.size);
            }
        } else if (lodash.indexOf(['date', 'time', 'datetime'], attributeMap.targetMetadata.type) > -1 || (usedAPreprocessFunction && !isNaN(Date
                .parse(value)))) {
            retVal.results.value = new Date(value.toString()).toISOString();
        } else if (attributeMap.targetMetadata.type === 'json' || (usedAPreprocessFunction && (lodash.isPlainObject(value) || lodash.isArray(
                value)))) {
            retVal.results.value = (lodash.isPlainObject(value) || lodash.isArray(value)) ? lodash.cloneDeep(value) : JSON.parse(value);
        } else if (attributeMap.targetMetadata.type === 'file') {
            if (value instanceof ArrayBuffer) {
                retVal.results.value = value;
            } else {
                let stringifyValue = (lodash.isPlainObject(value) || lodash.isArray(value)) ?
                    JSON.stringify(value) : String(value);

                retVal.results.value = _this.stringToArrayBuffer(stringifyValue);
            }
        }
        return retVal;
    } catch (e) {
        retVal.results.isValid = false;
        let error = _this.parseError(e);
        if (!lodash.isEqual(retVal, error)) {
            retVal.errors.push(error);
        }
        return retVal;
    }
};

this.stringToArrayBuffer = function(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
};

this.parseImportData = function(sourceInstances, attributeMap, validConversions, fieldsWithTranslate) {
    let retVal = {
        errors: [],
        results: {
            isValid: true,
            parsedInstances: []
        }
    };
    try {
        let tempInstance;
        let tempValue;
        let usedAPreprocessFunction;
        lodash.every(sourceInstances, function(instance) {
            tempInstance = {};
            lodash.forEach(attributeMap, function(metadata, targetKey) {
                usedAPreprocessFunction = false;
                if (lodash.indexOf(validConversions[metadata.sourceMetadata.type], metadata.targetMetadata.type) > -1) {
                    if (lodash.isFunction(metadata.preProcessFn)) {
                        tempValue = metadata.preProcessFn(instance[metadata.source]);
                        usedAPreprocessFunction = true;
                    } else {
                        tempValue = instance[metadata.source];
                    }
                    tempValue = _this.parseImportValue(tempValue, metadata, validConversions, usedAPreprocessFunction, lodash.indexOf(
                        fieldsWithTranslate, targetKey) > -1);
                    if (!lodash.isEmpty(tempValue.errors)) {
                        retVal.errors = lodash.concat(tempValue.errors, retVal.errors);
                    } else if (tempValue.results.isValid) {
                        tempInstance[targetKey] = tempValue.results.value;
                    }
                } else {
                    retVal.errors.push(_this.generateError('Invalid Conversion Type', 'Can\'t cast column "' + metadata.source + '"(type=' + metadata.sourceMetadata
                        .type + ') to "' +
                        targetKey + '"(type=' + metadata.targetMetadata.type + ').'));
                }
            });
            if (lodash.isEmpty(retVal.errors) && !lodash.isEmpty(tempInstance)) {
                retVal.results.parsedInstances.push(tempInstance);
            }
            return lodash.isEmpty(retVal.errors);
        });
        if (!lodash.isEmpty(retVal.errors)) {
            throw retVal;
        }
        return retVal;
    } catch (e) {
        retVal.results.isValid = false;
        let error = _this.parseError(e);
        if (!lodash.isEqual(retVal, error)) {
            retVal.errors.push(error);
        }
        return retVal;
    }
};

this.getCollectionName = function(collections, aliases, alias, checkAliasMap, placeholderMap, beforeFrom) {
    let getCollectionNameResult = (collection) => {
        let placeholders;
        let inputParameters = collection.inputParameters;
        let params;
        if($.lodash.isArray(inputParameters)){
            params = inputParameters;
        } else{
            inputParameters = inputParameters || [];
            params =   $.lodash.reduce( inputParameters, function(resp, par, key) {
                resp.push({
                    ID: resp.length + 1,
                    hanaName: key,
                    isMandatory: true,
                    value: par.defaultValue || undefined
                });
                return resp;
            }, []);
        }
        placeholders = $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], false, params);
        let result = '"' + collection.schema + '"."' + collection.identity + '" ' + ((collection.type === 'view' && collection.schema === $.viewSchema) && !beforeFrom ? placeholders : '');
        return  result;
    };

    let result;
    if (lodash.isBoolean(checkAliasMap) && checkAliasMap) {
        let collection = collections[aliases[alias]];
        result = getCollectionNameResult(collection);
        result = result + ' AS "' + alias + '"';
    } else {
        let collection = collections[alias];
        result = getCollectionNameResult(collection); 
    }
    return result;
};

this.getColumnAlias = function(collection, collections, aliases, field) {
    let useAlias = false;
    let alias = collection;
    if (lodash.isString(field.alias) && !lodash.isEmpty(field.alias)) {
        if (lodash.has(collections, aliases[field.alias])) {
            // If the alias is in the alias map, add it to the parser.
            alias = field.alias;
            useAlias = true;
        } else if (!lodash.isEmpty(lodash.keys(aliases)) && aliases[lodash.keys(aliases)[0]] === collection) {
            // If the alias is null, and the first alias in the alias map is the collection, add the alias to the parser.
            alias = lodash.keys(aliases)[0];
            useAlias = true;
        }
    } else if (lodash.isNil(field.alias) && !lodash.isEmpty(lodash.keys(aliases)) && aliases[lodash.keys(aliases)[0]] === collection) {
        // If the alias is null, and the first alias in the alias map is the collection, add the alias to the parser.
        alias = lodash.keys(aliases)[0];
        useAlias = true;
    }
    return {
        useAlias: useAlias,
        alias: alias
    };
};

this.transformAliasToOptions = function(aliases) {
    let options = [];
    lodash.forEach(aliases, function(collection, alias) {
        options.push({
            name: alias,
            collection: collection
        });
    });
    return options;
};

this.objectIsNotNullable = function(instance) {
    let isNullable = true;
    lodash.every(instance, function(value) {
        isNullable = lodash.isNil(value);
        return isNullable;
    });
    return !isNullable;
};

this.mergeInstances = function(instanceA, instanceB, nullableInstancesMap, instanceKeyMap, key, joinsKeyMap) {
    let unitedInstance = instanceA;
    if (lodash.isNil(unitedInstance)) {
        unitedInstance = lodash.cloneDeep(instanceB);
        unitedInstance.joinsData = {};
        lodash.forEach(instanceB.joinsData, function(value, alias) {
            if (lodash.isBoolean(nullableInstancesMap[alias]) && !nullableInstancesMap[alias]) {
                unitedInstance.joinsData[alias] = value;
                instanceKeyMap[key][alias][joinsKeyMap[alias]] = true;
            } else {
                unitedInstance.joinsData[alias] = [];
            }
        });
    } else {
        lodash.forEach(instanceB.joinsData, function(value, alias) {
            if (lodash.isArray(unitedInstance.joinsData[alias])) {
                if (!nullableInstancesMap[alias] && lodash.has(instanceKeyMap[key][alias], joinsKeyMap[alias]) && !instanceKeyMap[key][alias][
                        joinsKeyMap[alias]
                    ]) {
                    unitedInstance.joinsData[alias].push(value[0]);
                    instanceKeyMap[key][alias][joinsKeyMap[alias]] = true;
                }
            } else if (!nullableInstancesMap[alias]) {
                unitedInstance.joinsData[alias] = value;
                instanceKeyMap[key][alias][joinsKeyMap[alias]] = true;
            }
        });
    }
    return unitedInstance;
};

this.simulateInitForSubQueries = function(alias, hanaMap) {
    let template = {
        identity: alias,
        name: alias,
        primaryKeys: [],
        hanaMap: hanaMap,
        fields: {}
    };
    lodash.forEach(hanaMap, function(value, key) {
        template.fields[value.name] = {
            columnName: key,
            type: value.type,
            translate: value.translate
        };
    });
    return lodash.cloneDeep(template);
};

this.parseCore1ModelTocoreModel = function(model) {
    const types = _this.getCore1TocoreConversionTypes();
    let parsedModel = {};
    if (model instanceof Table) {
        parsedModel = {
            schema: '',
            component: '',
            name: '',
            identity: '',
            type: 'table',
            fields: {}
        };
        let dbName = model.name;
        dbName = dbName.replace(/"/g, '');
        let splitted = dbName.split('.');
        parsedModel.schema = splitted[0];
        // parsedModel.schema = lodash.split(dbName, '.')[0];
        parsedModel.component = splitted[1].substring( 0,splitted[1].search('::'));
        parsedModel.identity = splitted[1].substring( splitted[1].search('::')+2);
        parsedModel.name = parsedModel.identity;
        // parsedModel.component = lodash.split(lodash.split(dbName, '.')[1], '::')[0];
        // parsedModel.identity = lodash.split(lodash.split(dbName, '.')[1], '::')[1];
        //parsedModel.name      = lodash.split(lodash.split(dbName, '.')[1], '::')[1];
        lodash.forEach(model.DEFINITION().fields, function(definition, name) {
            if (lodash.has(types, definition.type)) {
                parsedModel.fields[name] = {
                    type: types[definition.type].type,
                    columnName: definition.name
                };
                if (lodash.isBoolean(definition.pk) && definition.pk) {
                    parsedModel.fields[name].primaryKey = true;
                }
                if (lodash.isString(definition.auto) && lodash.toLower(definition.auto).indexOf('nextval') > -1) {
                    parsedModel.fields[name].autoIncrement = true;
                } else if (lodash.isString(definition.auto) && lodash.toLower(definition.auto).indexOf('session_user') > -1) {
                    parsedModel.fields[name].onUpdate = 'current_user';
                    parsedModel.fields[name].default = 'current_user';
                } else if (lodash.isString(definition.auto) && lodash.toLower(definition.auto).indexOf('now') > -1) {
                    if (parsedModel.fields[name].type === 'date') {
                        parsedModel.fields[name].default = 'current_date';
                        parsedModel.fields[name].onUpdate = 'current_date';
                    } else if (parsedModel.fields[name].type === 'datetime') {
                        parsedModel.fields[name].default = 'current_timestamp';
                        parsedModel.fields[name].onUpdate = 'current_timestamp';
                    } else if (parsedModel.fields[name].type === 'time') {
                        parsedModel.fields[name].default = 'current_time';
                        parsedModel.fields[name].onUpdate = 'current_time';
                    }
                }
                if (parsedModel.fields[name].type === 'string' && lodash.isInteger(definition.dimension)) {
                    parsedModel.fields[name].size = definition.dimension;
                }
                if (parsedModel.fields[name].type === 'decimal' && lodash.isInteger(definition.precision)) {
                    parsedModel.fields[name].size = definition.precision;
                    if (lodash.isInteger(definition.scale)) {
                        parsedModel.fields[name].precision = definition.scale;
                    }
                }
            }
        });
    }
    return parsedModel;
};

this.isValidFunction = function(functionMetadata) {
    const adapterFunctions = _this.getAdapterFunctionsMap();
    return lodash.isString(functionMetadata.name) && !lodash.isEmpty(functionMetadata.name) &&
        !lodash.isNil(adapterFunctions[lodash.toUpper(functionMetadata.name)]) && lodash.isArray(functionMetadata.params) &&
        (functionMetadata.params.length === adapterFunctions[lodash.toUpper(functionMetadata.name)].params ||
            functionMetadata.params.length === adapterFunctions[lodash.toUpper(functionMetadata.name)].requiredParamsToExecute);
};

this.isValidAggregation = function(aggregationMetadata) {
    const adapterAggregationFunctions = _this.getAdapterAggregationMap();
    return lodash.isString(aggregationMetadata.type) && !lodash.isNil(adapterAggregationFunctions[aggregationMetadata.type]) &&
        lodash.isPlainObject(aggregationMetadata.param) && !lodash.isNil(aggregationMetadata.param);
};

this.isValidJoin = function(joinMetadata, collections, aliases, isSubQuery) {
    return lodash.isString(joinMetadata.type) && lodash.indexOf(_this.getValidJoins(), lodash.toLower(joinMetadata.type)) > -1 &&
        lodash.isString(joinMetadata.alias) && !lodash.isEmpty(joinMetadata.alias) &&
        (isSubQuery || (!isSubQuery && lodash.has(collections, aliases[joinMetadata.alias]))) &&
        lodash.isArray(joinMetadata.on) && !lodash.isEmpty(joinMetadata.on);
};

this.isValidSetOperation = function(setMetadata, collections, aliases, isSubQuery) {
    return lodash.isString(setMetadata.type) && lodash.indexOf(_this.getValidSetOperations(), lodash.toLower(setMetadata.type)) > -1 &&
        lodash.isString(setMetadata.alias) && !lodash.isEmpty(setMetadata.alias) &&
        (isSubQuery || (!isSubQuery && lodash.has(collections, aliases[setMetadata.alias])));
};

this.isValidMethod = function(methodMetadata) {
    const validMethods = _this.getValidMethods();
    return lodash.isPlainObject(methodMetadata) && !lodash.isNil(methodMetadata) && lodash.isString(methodMetadata.name) && !lodash.isNil(
            validMethods[methodMetadata.name]) &&
        !lodash.isNil(methodMetadata.param) && (
            (validMethods[methodMetadata.name].paramsTypes[0] === 'string' && lodash.isString(methodMetadata.param) && lodash.indexOf(validMethods[
                methodMetadata.name].validParams, methodMetadata.param) > -1) ||
            (validMethods[methodMetadata.name].paramsTypes[0] === 'decimal' && lodash.isNumber(methodMetadata.param)));
};

this.isValidCase = function(caseMetadata) {
    return lodash.has(caseMetadata, 'value') && lodash.has(caseMetadata, 'conditions');
};

this.isValidCalculationColumn = function(calculationMetadata) {
    return lodash.isString(calculationMetadata.operator) && !lodash.isNil(_this.getCalculatedColumnsOperations()[calculationMetadata.operator]) &&
        lodash.isArray(calculationMetadata.params) && !lodash.isEmpty(calculationMetadata.params);
};

this.isValidHint = function(hintMetadata) {
    const adapterHints = _this.getAdapterHints();
    return lodash.isString(hintMetadata.name) && !lodash.isEmpty(hintMetadata.name) &&
        !lodash.isNil(adapterHints[lodash.toUpper(hintMetadata.name)]) && lodash.isArray(hintMetadata.params) &&
        (hintMetadata.params.length === adapterHints[lodash.toUpper(hintMetadata.name)].params ||
            hintMetadata.params.length === adapterHints[lodash.toUpper(hintMetadata.name)].requiredParamsToExecute);
};

this.isValidLiteral = function(literalMetadata) {
    return lodash.isPlainObject(literalMetadata) && lodash.isString(literalMetadata.type) && lodash.indexOf(_this.getTypes(),
        literalMetadata.type) > -1;
};

this.isValidMerge = function(instances, instanceB) {
    let merge = true;
    lodash.every(instances, function(instance) {
        merge = !lodash.isEqual(instance, instanceB);
        return merge;
    });
    return merge;
};

this.hasPropertyAndIsValidString = function(retVal, definition, property, nonWhitespaceString) {
    if (!lodash.has(definition, property)) {
        retVal.errors.push(_this.generateError('No ' + property, 'The model needs a ' + property + '(string).'));
        retVal.isValid = false;
    } else if (!(lodash.isString(definition[property]) && !lodash.isEmpty(definition[property]))) {
        retVal.errors.push(_this.generateError('Invalid ' + property, 'The model\'s ' + property + ' needs to be a non empty string.'));
        retVal.isValid = false;
    }
    //Commented by requeriment in External Tables BRE...please follow the Standar in table names and then this validation is no needed
    // if (nonWhitespaceString && /\s/g.test(definition[property])) {
    //     retVal.errors.push(_this.generateError('Invalid ' + property, 'The model\'s ' + property +
    //         ' needs to be a non empty string without spaces.'));
    //     retVal.isValid = false;
    // }
    return retVal;
};

this.areValidParams = function(params, functionName) {
    const adapterFunctions = _this.getAdapterFunctionsMap();
    return adapterFunctions[functionName].requiredParamsToExecute === params.length || adapterFunctions[functionName].params === params.length;
};

/* Parsers */
this.parseError = function(e) {
    let retVal = e;
    if (lodash.isString(e.fileName) && lodash.isString(e.message) && lodash.isInteger(e.lineNumber)) {
        retVal = {
            fileName: e.fileName,
            message: e.message,
            lineNumber: e.lineNumber
        };
    }
    return retVal;
};

this.parseJoin = function(collection, joinMetadata, collections, aliases, subQueryJoins, subQueryMap, placeholderMap) {
    let retVal = {
        isValid: true,
        joinQuery: ''
    };
    let parsedWhere = _this.parseWhere(collection, collections, joinMetadata.on, aliases);
    if (!lodash.isNil(parsedWhere)) {
        if (lodash.indexOf(subQueryJoins, joinMetadata.alias) > -1) {
            retVal.joinQuery += ' ' + _this.getJoinMap()[lodash.toLower(joinMetadata.type)] + ' (' + subQueryMap[joinMetadata.alias].findQuery +
                ' ) AS "' + joinMetadata.alias + '" ON (' + parsedWhere + ') ';
        } else if (lodash.has(collections, aliases[joinMetadata.alias])) {
            retVal.joinQuery += ' ' + _this.getJoinMap()[lodash.toLower(joinMetadata.type)] + ' ' + _this.getCollectionName(collections,
                aliases,
                joinMetadata.alias, true, placeholderMap) + ' ON (' + parsedWhere + ') ';
        } else {
            retVal.isValid = false;
        }
    } else {
        retVal.isValid = false;
    }
    return retVal;
};

this.parseSetOperation = function(collection, setMetadata, collections, aliases, subQueryOperations, subQueryMap, placeholderMap) {
    let retVal = {
        isValid: true,
        setQuery: ''
    };
    if (lodash.indexOf(subQueryOperations, setMetadata.alias) > -1) {
        retVal.setQuery += ' ' + _this.getSetOperationsMap()[lodash.toLower(setMetadata.type)] + ' (' + subQueryMap[setMetadata.alias].findQuery +
            ' )';
    } else if (lodash.has(collections, aliases[setMetadata.alias])) {
        retVal.setQuery += ' ' + _this.getSetOperationsMap()[lodash.toLower(setMetadata.type)] + ' ' + _this.getCollectionName(collections,
            aliases,
            setMetadata.alias, false, placeholderMap) + ' ';
    } else {
        retVal.isValid = false;
    }
    return retVal;
};

this.parseValue = function(collection, collections, aliases, conditionType, conditionValue) {
    let value = '';
    let retVal;
    let isValid = true;
    let type;
    if (lodash.isPlainObject(conditionValue) && !lodash.isNil(conditionValue)) {
        retVal = _this.parseColumn(collection, collections, aliases, conditionValue, true, 'parseValue');
        if (retVal.isValid) {
            value = retVal.hanaColumn;
            type = retVal.type;
        }
        isValid = retVal.isValid;
    } else {
        // Is a primitive type value
        value = _this.getHanaValue(conditionType, conditionValue);
    }
    return {
        isValid: isValid,
        value: value,
        type: type
    };
};

this.parseMethod = function(method) {
    const validMethods = _this.getValidMethods();
    let retVal = {
        isValid: true,
        value: null
    };
    if (_this.isValidMethod(method)) {
        retVal.value = validMethods[method.name].sqlName + '=' + method.param;
    } else {
        retVal.isValid = false;
    }
    return retVal;
};

this.parseCondition = function(collection, collections, condition, aliases) {
    let parsedCondition = null;
    let field, value = null;
    let retVal = {};
    let conditionType;
    if (lodash.indexOf(['$notExists', '$exists'], condition.operator) === -1) {
        /* Parses the condition */
        retVal = _this.parseColumn(collection, collections, aliases, condition, true, 'parseCondition');
        if (!retVal.isValid) {
            return parsedCondition;
        }
        field = retVal.hanaColumn;
        conditionType = retVal.type;
    }
    if (!lodash.isNil(condition.value) && lodash.isArray(condition.value)) {
        if (condition.value.length !== 2 && lodash.indexOf(['$between', '$notBetween'], condition.operator) > -1) {
            return parsedCondition;
        }
        let validValue = true;
        value = lodash.map(condition.value, function(_value) {
            retVal = _this.parseValue(collection, collections, aliases, conditionType, _value);
            if (!retVal.isValid) {
                validValue = false;
            }
            return retVal.value;
        });
        retVal.type = conditionType;
        if (!validValue) {
            value = '';
        }
    } else if (!lodash.isNil(condition.value) && !lodash.isArray(condition.value)) {
        if (lodash.isPlainObject(condition.value) && !lodash.isEmpty(condition.value) && lodash.indexOf(['$contains', '$likeRegex'], condition.operator) >
            -1) {
            retVal = _this.parseValue(collection, collections, aliases, conditionType, condition.value.value);
        } else {
            retVal = _this.parseValue(collection, collections, aliases, conditionType, condition.value);
        }
        if (!retVal.isValid) {
            return parsedCondition;
        }
        value = retVal.value;
    }
    /* If it is an invalid value, ignore the condition*/
    if (!lodash.isNil(value) && lodash.isEmpty(value)) {
        return parsedCondition;
    }
    /* Checks if it is a IS NULL, IS NOT NULL, BETWEEN, NOT BETWEEN, NOT IN or IN operation*/
    if ((lodash.isNil(value) && lodash.indexOf(['$neq', '$eq'], condition.operator) === -1) || (lodash.isArray(condition.value) && lodash.indexOf(
            ['$between', '$in', '$notIn', '$notBetween'], condition.operator) === -1)) {
        return parsedCondition;
    }
    const validOperators = _this.getValidOperators();
    if (lodash.indexOf(['$eq', '$neq'], condition.operator) > -1 && lodash.isNil(value)) {
        condition.operator += 'Null';
        parsedCondition = field + ' ' + validOperators[condition.operator];
    } else if (lodash.indexOf(['$notExists', '$exists'], condition.operator) > -1) {
        parsedCondition = validOperators[condition.operator] + value;
    } else if (condition.operator === '$contains') {
        parsedCondition = validOperators[condition.operator] + '(' + field + ',' + value;
        if (lodash.isPlainObject(condition.value) && !lodash.isEmpty(condition.value) && lodash.isPlainObject(condition.value.fuzzy) && !lodash.isEmpty(
                condition.value.fuzzy)) {
            // Prepare method before the parsing of the function
            let fuzzyFunction = {
                name: 'FUZZY',
                params: [condition.value.fuzzy.minimumScore]
            };
            if (lodash.isArray(condition.value.fuzzy.methods) && !lodash.isEmpty(condition.value.fuzzy.methods)) {
                let tempResponse, parsedMethods = [];
                lodash.forEach(condition.value.fuzzy.methods, function(method) {
                    tempResponse = _this.parseMethod(method);
                    if (tempResponse.isValid) {
                        parsedMethods.push(tempResponse.value);
                    }
                });
                if (!lodash.isEmpty(parsedMethods)) {
                    fuzzyFunction.params.push(lodash.join(parsedMethods, ','));
                }
            }
            parsedCondition += ',' + _this.parseFunction(collections, collection, aliases, fuzzyFunction);
        }
        parsedCondition += ')';
    } else if (condition.operator === '$likeRegex' && lodash.isPlainObject(condition.value) && !lodash.isEmpty(condition.value) && lodash.isString(
            condition.value.flag) && lodash.indexOf(['i', 'm', 's', 'x'], lodash.toLower(condition.value.flag)) > -1) {
        parsedCondition = field + ' ' + validOperators[condition.operator] + ' ' + value + ' FLAG \'' + lodash.toLower(condition.value.flag) +
            '\'';
    } else {
        if (retVal.type !== 'sub-query' && lodash.indexOf(['$between', '$notBetween'], condition.operator) > -1) {
            value = lodash.join(value, ' AND ');
        } else if (retVal.type !== 'sub-query' && lodash.indexOf(['$in', '$notIn'], condition.operator) > -1) {
            value = ' (' + lodash.join(value, ',') + ')';
        }
        parsedCondition = field + ' ' + validOperators[condition.operator] + ' ' + value;

    }

    return parsedCondition;
};

this.parseWhere = function(collection, collections, conditions, aliases, logicalOperator) {
    let parsedConditions = [];
    logicalOperator = logicalOperator || ' AND ';
    const whereOperators = _this.getWhereOperators();
    let temp;
    if (!lodash.isNil(conditions) && lodash.isArray(conditions)) {
        lodash.forEach(conditions, function(condition) {
            if (!lodash.isNil(condition) && lodash.isObjectLike(condition)) {
                if (lodash.isArray(condition)) {
                    temp = _this.parseWhere(collection, collections, condition, aliases, (logicalOperator === ' AND ' ? ' OR ' : ' AND '));
                    if (!lodash.isNil(temp)) {
                        parsedConditions.push(temp);
                    }
                } else if (!lodash.isEmpty(condition) && lodash.has(condition, 'operator') && lodash.has(condition, 'value')) {
                    if (lodash.indexOf(whereOperators, condition.operator) > -1) {
                        temp = _this.parseCondition(collection, collections, condition, aliases);
                        if (!lodash.isNil(temp)) {
                            parsedConditions.push(temp);
                        }
                    }
                }
            }
        });
    }
    if (!lodash.isEmpty(parsedConditions)) {
        return '(' + parsedConditions.join(logicalOperator) + ')';
    } else {
        return null;
    }
};

this.parseFunction = function(collections, collection, aliases, functionMetadata) {
    let params = [];
    let retVal;
    lodash.forEach(functionMetadata.params, function(param, idx) {
        if (lodash.isPlainObject(param) && !lodash.isNil(param)) {
            if (!lodash.isNil(param.function) && _this.isValidFunction(param.function)) {
                /* It's a nested function */
                params.push(_this.parseFunction(collections, collection, aliases, param.function));
            } else if (lodash.isPlainObject(param) && !lodash.isNil(param)) {
                retVal = _this.parseColumn(collection, collections, aliases, param, true, 'parseFunction');
                if (retVal.isValid) {
                    params.push(retVal.hanaColumn);
                }
            }
        } else {
            params.push(_this.getHanaValue(_this.getAdapterFunctionsMap()[functionMetadata.name].paramsTypes[idx], param));
        }
    });
    if (_this.areValidParams(params, functionMetadata.name)) {
        if (_this.getAdapterFunctionsMap()[functionMetadata.name].sqlName === 'COUNT_OVER') {
            return 'COUNT(' + lodash.join(params, ',') + ') OVER()';
        }
        return _this.getAdapterFunctionsMap()[functionMetadata.name].sqlName + '(' + lodash.join(params, ',') + ')';
    }
    return '';
};

this.parseCase = function(collection, collections, aliases, caseMetadata) {
    let cases = [];
    let value = '',
        condition;
    lodash.forEach(caseMetadata, function(_case) {
        if (_this.isValidCase(_case)) {
            if (_case.default || lodash.isEmpty(_case.conditions)) {
                value = _this.parseValue(collection, collections, aliases, 'string', _case.value);
                if (value.isValid) {
                    cases.push(' ELSE ' + value.value);
                }
            } else {
                condition = _this.parseWhere(collection, collections, _case.conditions, aliases);
                value = _this.parseValue(collection, collections, aliases, 'string', _case.value);
                if (!lodash.isNil(condition)) {
                    if (value.isValid) {
                        cases.push(' WHEN ' + condition + ' THEN  ' + value.value);
                    }
                }
            }
        }
    });
    if (!lodash.isEmpty(cases)) {
        return 'CASE ' + lodash.join(cases, ' ') + ' END';
    }
    return '';
};

this.parseCalculationColumn = function(collections, collection, aliases, calculationMetadata) {
    let params = [];
    let retVal;
    lodash.forEach(calculationMetadata.params, function(param, idx) {
        if (lodash.isPlainObject(param) && !lodash.isNil(param)) {
            if (!lodash.isNil(param.calculation) && _this.isValidCalculationColumn(param.calculation)) {
                /* It's a nested calculation */
                params.push(_this.parseCalculationColumn(collections, collection, aliases, param.calculation));
            } else if (lodash.isPlainObject(param) && !lodash.isNil(param)) {
                retVal = _this.parseColumn(collection, collections, aliases, param, true, 'parseCalculationColumn');
                if (retVal.isValid) {
                    params.push(retVal.hanaColumn);
                }
            }
        } else {
            params.push(_this.getHanaValue(_this.getCalculatedColumnsOperations()[calculationMetadata.operator].paramsTypes[idx], param));
        }
    });
    if (params.length === 2) {
        return '(' + lodash.join(params, _this.getCalculatedColumnsOperations()[calculationMetadata.operator].operator) + ')';
    }
    return '';
};

this.parseField = function(collections, aliases, field, alias, usesAlias) {
    let _collection;
    if (usesAlias) {
        _collection = aliases[alias];
    } else {
        _collection = alias;
    }
    if (lodash.has(collections, _collection) && lodash.has(collections[_collection].fields, field)) {
        if (usesAlias) {
            return '"' + alias + '"."' + collections[_collection].fields[field].columnName + '"';
        } else {
            return '"' + collections[_collection].schema + '"."' + collections[_collection].identity + '"."' + collections[_collection].fields[field]
                .columnName + '"';
        }
    }
    return '';
};

this.parseColumn = function(collection, collections, aliases, field, acceptsSubQuery, clause) {
    let alias, hanaColumn, useAlias, type;
    alias = collection;
    useAlias = false;
    hanaColumn = '';
    type = '';
    let isValid = true;
    let retVal;
    /* Checks if the column use an alias, and obtain it */
    retVal = _this.getColumnAlias(collection, collections, aliases, field);
    useAlias = retVal.useAlias;
    alias = retVal.alias;
    if (!lodash.isNil(field.function) && _this.isValidFunction(field.function)) {
        /* It's a function column */
        hanaColumn = _this.parseFunction(collections, collection, aliases, field.function);
        type = _this.getAdapterFunctionsMap()[field.function.name].returnType;
    } else if (!lodash.isNil(field.aggregation) && _this.isValidAggregation(field.aggregation)) {
        /* It's an aggregation column */
        retVal = _this.parseColumn(collection, collections, aliases, field.aggregation.param, true, clause);
        type = _this.getAdapterAggregationMap()[field.aggregation.type].returnType;
        isValid = retVal.isValid;
        if (isValid) {
            hanaColumn = _this.getAdapterAggregationMap()[field.aggregation.type].sqlName + '(' + retVal.hanaColumn + ')';
        }
    } else if (!lodash.isNil(field.case) && lodash.isArray(field.case) && !lodash.isEmpty(field.case)) {
        /* It's a case column */
        hanaColumn = _this.parseCase(collection, collections, aliases, field.case);
        type = 'any';
    } else if (acceptsSubQuery && !lodash.isNil(field.query) && !lodash.isEmpty(field.query) && lodash.isString(field.collection) && !lodash.isNil(
            collections[field.collection])) {
        /* It's a subQuery column */
        if (lodash.isArray(field.query.aliases) && !lodash.isEmpty(field.query.aliases)) {
            field.query.aliases = lodash.union(field.query.aliases, _this.transformAliasToOptions(lodash.cloneDeep(aliases)));
        } else {
            field.query.aliases = _this.transformAliasToOptions(lodash.cloneDeep(aliases));
        }
        hanaColumn = '(' + _this._find(field.collection, field.query, collections, true).findQuery + ')';
        type = 'sub-query';
    } else if (!lodash.isNil(field.calculation) && _this.isValidCalculationColumn(field.calculation)) {
        /* Is a calculated column */
        hanaColumn = _this.parseCalculationColumn(collections, collection, aliases, field.calculation);
        type = _this.getCalculatedColumnsOperations()[field.calculation.operator].returnType;
    } else if (!lodash.isNil(field.literal) && _this.isValidLiteral(field.literal)) {
        /* Is a literalColumn */
        hanaColumn = _this.getHanaValue(field.literal.type, field.literal.value);
        type = field.literal.type;
    } else if (lodash.has(collections[useAlias ? aliases[alias] : alias].fields, field.field) && !lodash.isNil(collections[useAlias ? aliases[
            alias] : alias].fields[field.field])) {
        /* It's a field from the collection */
        hanaColumn = _this.parseField(collections, aliases, field.field, alias, useAlias);
        type = collections[useAlias ? aliases[alias] : alias].fields[field.field].type;
    } else {
        isValid = false;
    }

    if (lodash.isEmpty(type) || lodash.isEmpty(hanaColumn)) {
        isValid = false;
    }
    return {
        hanaColumn: hanaColumn,
        type: type,
        isValid: isValid
    };
};

this.parseHint = function(collection, hintMetadata, collections, aliases) {
    let retVal = {
        isValid: true,
        parsedHints: []
    };
    let currentHint;
    const hints = _this.getAdapterHints();
    let parsedParams;
    let tempResponse;
    let tempValue;
    lodash.forEach(hintMetadata, function(hint) {
        if (_this.isValidHint(hint)) {
            parsedParams = [];
            currentHint = hints[lodash.toUpper(hint.name)];
            if (currentHint.isConstant || lodash.isEmpty(hint.params)) {
                retVal.parsedHints.push(currentHint.sqlName);
            } else {
                lodash.forEach(hint.params, function(param, idx) {
                    if (currentHint.paramsTypes[idx] === 'model') {
                        if (lodash.isString(param) && !lodash.isEmpty(param)) {
                            if (!lodash.isNil(aliases[param])) {
                                parsedParams.push('"' + param + '"');
                            } else if (!lodash.isNil(collections[param])) {
                                parsedParams.push(collections[param].tableName);
                            } else {
                                retVal.isValid = false;
                            }
                        }
                    } else if (!lodash.isNil(param.hint) && _this.isValidHint(param.hint)) {
                        tempResponse = _this.parseHint(collection, [param.hint], collections, aliases);
                        retVal.isValid = retVal.isValid && tempResponse.isValid;
                        if (tempResponse.isValid) {
                            parsedParams.push(lodash.join(tempResponse.parsedHints, ','));
                        }
                    } else {
                        tempValue = _this.getHanaValue(currentHint.paramsTypes[idx], param);
                        const unescapedTypes = ['tinyint',
                            'bigint',
                            'integer',
                            'decimal'
                        ];
                        if (lodash.indexOf(unescapedTypes, currentHint.paramsTypes[idx]) > -1) {
                            tempValue = Number(tempValue.replace(/'/g, ''));
                        }
                        parsedParams.push(tempValue);
                    }
                });
                if (retVal.isValid) {
                    retVal.parsedHints.push(currentHint.sqlName + '(' + lodash.join(parsedParams) + ')');
                }
            }
        }
    });
    return retVal;
};

this.parseKey = function(key) {
    return {
        alias: lodash.trimStart(lodash.split(key, '_')[0], '@'),
        property: lodash.trimStart(key, lodash.split(key, '_')[0] + '_')
    };
};

this.parseInstance = function(instance, modelMap, aliasMapping) {
    let parsedInstance = {};
    let tempInstance = {
        joinsData: {}
    };
    let tempPropertyValue;
    let retVal = {};
    let mapKey = '';
    let nullableInstancesMap = {};
    let joinsKeyMap = {};
    lodash.forEach(instance, function(value, key) {
        if (lodash.has(modelMap, key)) {
            parsedInstance = _this.parseRowValueToORMValue(parsedInstance, instance, modelMap, key, modelMap[key]);
            value = parsedInstance[modelMap[key].name];
            retVal = _this.parseKey(modelMap[key].name);
            tempPropertyValue = {};
            if (!lodash.isNil(aliasMapping[retVal.alias])) {
                tempPropertyValue[retVal.property] = value;
                if (aliasMapping[retVal.alias].isPrimary) {
                    lodash.assign(tempInstance, tempPropertyValue);
                    if (lodash.isObjectLike(value)) {
                        mapKey += JSON.stringify(value);
                    } else {
                        mapKey += lodash.toString(value);
                    }
                } else if (!lodash.isNil(tempInstance.joinsData[aliasMapping[retVal.alias].map])) {
                    nullableInstancesMap[aliasMapping[retVal.alias].map] = nullableInstancesMap[aliasMapping[retVal.alias].map] && lodash.isNil(value);
                    tempInstance.joinsData[aliasMapping[retVal.alias].map][0] = lodash.assign(tempInstance.joinsData[aliasMapping[retVal.alias].map][0],
                        tempPropertyValue);
                    if (lodash.isObjectLike(value)) {
                        joinsKeyMap[aliasMapping[retVal.alias].map] += JSON.stringify(value);
                    } else {
                        joinsKeyMap[aliasMapping[retVal.alias].map] += lodash.toString(value);
                    }
                } else {
                    if (!lodash.has(nullableInstancesMap, aliasMapping[retVal.alias].map)) {
                        nullableInstancesMap[aliasMapping[retVal.alias].map] = true;
                    }
                    if (!lodash.has(joinsKeyMap, aliasMapping[retVal.alias].map)) {
                        joinsKeyMap[aliasMapping[retVal.alias].map] = '';
                    }
                    nullableInstancesMap[aliasMapping[retVal.alias].map] = nullableInstancesMap[aliasMapping[retVal.alias].map] && lodash.isNil(value);
                    tempInstance.joinsData[aliasMapping[retVal.alias].map] = [tempPropertyValue];
                    if (lodash.isObjectLike(value)) {
                        joinsKeyMap[aliasMapping[retVal.alias].map] += JSON.stringify(value);
                    } else {
                        joinsKeyMap[aliasMapping[retVal.alias].map] += lodash.toString(value);
                    }
                }
            }
        }
    });
    return {
        mapKey: mapKey,
        data: tempInstance,
        nullableInstancesMap: nullableInstancesMap,
        instanceKeyMap: joinsKeyMap
    };
};

this.mergeInstanceKeyMap = function(instanceKeyMapA, instanceKeyMapB, nullableInstancesMap) {
    lodash.forEach(instanceKeyMapB, function(key, join) {
        if (lodash.has(instanceKeyMapA, join) && !lodash.has(instanceKeyMapA[join], key) && (lodash.isBoolean(nullableInstancesMap[join]) && !
                nullableInstancesMap[join])) {
            instanceKeyMapA[join][key] = false;
        } else if (!lodash.has(instanceKeyMapA, join) && (lodash.isBoolean(nullableInstancesMap[join]) && !nullableInstancesMap[join])) {
            instanceKeyMapA[join] = {};
            instanceKeyMapA[join][key] = false;
        }
    });
    return instanceKeyMapA;
};

this.parseFindData = function(query, modelMap, aliasMapping) {
    let retVal = {
        errors: [],
        results: []
    };
    let connection;
    let tempObject = {};
    let dataMap = {};
    let instancesKeyMap = {};
    try {
        // if ($.session.getUsername() === 'JMIDE') {
        //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
        // } else {
            connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        // }
        // connection = $.hdb.getConnection();
        connection.setAutoCommit(1);
        let response = connection.executeQuery(query);
        connection.close();
        if (!lodash.isEmpty(response)) {
            let iterator = response.getIterator();
            let instance;
            let tempInstance;
            while (iterator.next()) {
                instance = iterator.value();
                tempInstance = _this.parseInstance(instance, modelMap, aliasMapping);
                if (!lodash.isEmpty(tempInstance)) {
                    if (lodash.isNil(dataMap[tempInstance.mapKey])) {
                        instancesKeyMap[tempInstance.mapKey] = {};
                        instancesKeyMap[tempInstance.mapKey] = _this.mergeInstanceKeyMap(instancesKeyMap[tempInstance.mapKey], tempInstance.instanceKeyMap,
                            tempInstance.nullableInstancesMap);
                        tempObject = _this.mergeInstances(dataMap[tempInstance.mapKey], tempInstance.data, tempInstance.nullableInstancesMap, instancesKeyMap,
                            tempInstance.mapKey, tempInstance.instanceKeyMap);
                        dataMap[tempInstance.mapKey] = tempObject;
                    } else {
                        instancesKeyMap[tempInstance.mapKey] = _this.mergeInstanceKeyMap(instancesKeyMap[tempInstance.mapKey], tempInstance.instanceKeyMap,
                            tempInstance.nullableInstancesMap, tempInstance.mapKey, tempInstance.instanceKeyMap);
                        tempObject = _this.mergeInstances(dataMap[tempInstance.mapKey], tempInstance.data, tempInstance.nullableInstancesMap, instancesKeyMap,
                            tempInstance.mapKey, tempInstance.instanceKeyMap);
                        dataMap[tempInstance.mapKey] = tempObject;
                    }
                }
            }
        }
        let newParsedInstance;
        lodash.forEach(dataMap, function(_instance) {
            newParsedInstance = _instance;
            lodash.assign(newParsedInstance, _instance.joinsData);
            delete newParsedInstance.joinsData;
            retVal.results.push(newParsedInstance);
        });
        return retVal;
    } catch (e) {
        connection.close();
        let error = {};
        if (lodash.isPlainObject(e) && !lodash.isNil(e.code) && lodash.isNumber(e.code)) {
            // if ($.session.getUsername() === 'JMIDE') {
            //     connection = $.hdb.getConnection({"sqlcc": "timp.core.server::SQLConnection", "pool": true});
            // } else {
                connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
            // }
            // connection = $.hdb.getConnection();
            connection.setAutoCommit(1);
            let query = 'SELECT "CODE_STRING" AS "name", "DESCRIPTION" AS "description" FROM "SYS"."M_ERROR_CODES" WHERE "CODE" = \'' + e.code +
                '\'';
            let response = connection.executeQuery(query);
            connection.close();
            error = {
                name: response[0].name,
                description: response[0].description
            };
        } else {
            error = _this.parseError(e);
        }
        error['@query'] = query;
        retVal.errors = [error];
        return retVal;
    }
};

this.parseRowValueToORMValue = function(tempInstance, instance, modelMap, hanaKey, value) {
    try {
        if (!lodash.isNil(instance[hanaKey]) && value.type === 'json') {
            tempInstance[value.name] = JSON.parse(instance[hanaKey]);
        } else if (!lodash.isNil(instance[hanaKey]) && value.type === 'decimal') {
            tempInstance[value.name] = parseFloat(instance[hanaKey]);
        } else if (!lodash.isNil(instance[hanaKey]) && lodash.indexOf(['integer', 'tinyint', 'bigint'], value.type) > -1) {
            tempInstance[value.name] = parseInt(instance[hanaKey], 10);
        } else if (!lodash.isNil(instance[hanaKey]) && value.type === 'boolean') {
            tempInstance[value.name] = instance[hanaKey] === 'true';
        } else {
            if (instance[hanaKey] instanceof ArrayBuffer && value.type !== 'file') {
                tempInstance[value.name] = $.util.stringify(instance[hanaKey]);
            } else {
                tempInstance[value.name] = instance[hanaKey];
            }
        }
        if (!lodash.isNil(value.translate)) {
            tempInstance[value.name] = value.translate[instance[hanaKey]];
        }
    } catch (e) {
        tempInstance[value.name] = null;
    }
    return tempInstance;
};

this.parseInstanceFromDBToORM = function(instance, modelMap) {
    let tempInstance = {};
    if (!lodash.isNil(modelMap) && lodash.isPlainObject(modelMap)) {
        lodash.forEach(modelMap, function(value, hanaKey) {
            if (lodash.has(instance, hanaKey)) {
                tempInstance = _this.parseRowValueToORMValue(tempInstance, instance, modelMap, hanaKey, value);
            }
        });
    } else {
        let parsedValue;
        lodash.forEach(instance, function(value, key) {
            if (value instanceof ArrayBuffer) {
                parsedValue = $.util.stringify(value);
                tempInstance[key] = parsedValue;
            } else {
                tempInstance[key] = value;
            }
            tempInstance[key] = value;
        });
    }
    return tempInstance;
};

this.parsePlaceholder = function(placeholderMetadata, alias, placeholders) {
    let retVal = {
        isValid: true,
        parsedPlaceholder: ''
    };
    if (lodash.isPlainObject(placeholderMetadata) && !lodash.isEmpty(placeholderMetadata) && lodash.isString(alias) && !lodash.isEmpty(alias) &&
        lodash.isPlainObject(placeholders) && !lodash.isEmpty(placeholders) && lodash.isArray(placeholders.params)) {
        let parsedPlaceholders = [];
        let tempParsedPlaceholder;
        let tempParsedValues;
        const escape = ['string', 'text', 'json', 'shorttext'];
        lodash.forEach(placeholders.params, function(placeholder) {
            tempParsedPlaceholder = '';
            tempParsedValues = [];
            if (lodash.isString(placeholder.name) && !lodash.isEmpty(placeholder.name) && !lodash.isNil(placeholderMetadata[placeholder.name])) {
                tempParsedPlaceholder = '\'PLACEHOLDER\' = (\'' + placeholderMetadata[placeholder.name].columnName + '\'';
                lodash.forEach(placeholder.values, function(value) {
                    value = _this.getHanaValue(placeholderMetadata[placeholder.name].type, value);
                    if (value !== 'NULL' && (placeholder.values.length === 1 || (lodash.indexOf(escape, placeholderMetadata[placeholder.name].type) ===
                            -1))) {
                        value = value.substring(1, value.length - 1);
                    }
                    tempParsedValues.push(value);

                });
                tempParsedPlaceholder += (placeholder.values.length !== 1 ? ' = ' : ' , ') + _this.getHanaValue(placeholderMetadata[placeholder.name]
                    .type, lodash.join(tempParsedValues, ',')) + ')';
                parsedPlaceholders.push(tempParsedPlaceholder);
            }
        });
        retVal.parsedPlaceholder = '(' + lodash.join(parsedPlaceholders, ', ') + ')';
    } else {
        retVal.isValid = false;
    }
    return retVal;
};

/* Init BaseModel's resources*/
this.sanitizeProperty = function(retVal, def, invalidTypes, property) {
    if (!lodash.isNil(def[property]) && lodash.indexOf(invalidTypes, def.type) > -1) {
        delete def[property];
        retVal.warnings.push({
            name: 'Invalid property "' + property + '" for column "' + def.columnName + '"',
            description: 'The type "' + def.type + '" can\'t have the property "' + property + '". The property was removed.'
        });
    }
    if (!lodash.isNil(def[property]) && lodash.indexOf(['autoIncrement', 'unique', 'primaryKey', 'required'], property) > -1 && !lodash.isBoolean(
            def[property])) {
        delete def[property];
        retVal.warnings.push({
            name: 'Invalid property "' + property + '" value for column "' + def.columnName + '"',
            description: 'Invalid value for the property "' + property + '"(' + def[property] + '). Must be a boolean.'
        });
    }

    if (property === 'size' && lodash.indexOf(['decimal', 'string'], def.type) > -1 && !lodash.isNumber(def.size)) {
        delete def[property];
        retVal.errors.push({
            name: 'Invalid property "' + property + '" value for column "' + def.columnName + '"',
            description: 'Invalid value for the property "' + property + '"(' + def[property] + '). Must be an integer.'
        });
    }

    if (property === 'precision' && lodash.indexOf(['decimal'], def.type) > -1 && !lodash.isNumber(def.precision)) {
        delete def[property];
        retVal.errors.push({
            name: 'Invalid property "' + property + '" value for column "' + def.columnName + '"',
            description: 'Invalid value for the property "' + property + '"(' + def[property] + '). Must be an integer.'
        });
    }

    return {
        def: def,
        retVal: retVal
    };
};

this.sanitizeFieldDefinition = function(retVal, def, types, field, collectionToBeDefined, collections) {
    let invalidTypes = [];
    let response;
    let isCollection = lodash.has(collections, def.type) || def.type === collectionToBeDefined;
    // Check the type of the field and removes invalid properties
    if (!lodash.isNil(def.type) && (lodash.indexOf(types, def.type) !== -1 || isCollection)) {
        // Removing autoIncrement from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['bigint', 'tinyint', 'integer'], type) === -1;
        });
        // Adding the collection type to the invalidTypes
        if (isCollection) {
            invalidTypes.push(def.type);
        }
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'autoIncrement');
        def = response.def;
        retVal = response.retVal;

        // Removing unique from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['bigint', 'tinyint', 'integer', 'binary', 'string'], type) === -1;
        });
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'unique');
        def = response.def;
        retVal = response.retVal;

        // Removing primaryKey from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['string', 'tinyint', 'bigint', 'integer', 'decimal', 'date', 'datetime', 'boolean', 'binary', 'time'], type) ===
                -1;
        });
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'primaryKey');
        def = response.def;
        retVal = response.retVal;

        // Removing size from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['decimal', 'string'], type) === -1;
        });
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'size');
        def = response.def;
        retVal = response.retVal;

        // Removing precision from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['decimal'], type) === -1;
        });
        // Adding the collection type to the invalidTypes
        if (isCollection) {
            invalidTypes.push(def.type);
        }
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'precision');
        def = response.def;
        retVal = response.retVal;

        // Removing translate from invalidTypes
        invalidTypes = lodash.filter(types, function(type) {
            return lodash.indexOf(['tinyint', 'boolean'], type) === -1;
        });
        // Adding the collection type to the invalidTypes
        if (isCollection) {
            invalidTypes.push(def.type);
        }
        response = _this.sanitizeProperty(retVal, def, invalidTypes, 'translate');
        def = response.def;
        retVal = response.retVal;

        // Check if the size is greater than 0
        if (!lodash.isNil(def.size) && def.size < 1) {
            retVal.errors.push(_this.generateError('Invalid size for ' + field, 'The model\'s ' + field +
                ' size needs to be a number greater than 0.'));
            retVal.isValid = false;
        } else if (!lodash.isNil(def.size) && !lodash.isNil(def.precision)) {
            if (def.precision < 0) {
                retVal.errors.push(_this.generateError('Invalid precision for ' + field, 'The model\'s ' + field +
                    ' precision needs to be a number greater than or equal to 0.'));
                retVal.isValid = false;
            } else if (def.precision > def.size) {
                retVal.errors.push(_this.generateError('Invalid precision for ' + field, 'The model\'s ' + field +
                    ' precision needs to be a number less than or equal to size.'));
                retVal.isValid = false;
            }
        }

        // Check if the comment is a string
        if (!(lodash.isString(def.comment) && !lodash.isEmpty(def.comment))) {
            def.comment = null;
        }
    } else {
        retVal.errors.push(_this.generateError('Invalid type', '"' + field + '" has an invalid type(' + def.type + ').'));
        retVal.isValid = false;
    }
    return {
        sanitizedDefinition: def,
        retVal: retVal
    };
};

this.checkConstraints = function(retVal, def, setDefault) {
    setDefault = setDefault || false;
    const validConstraints = ['CASCADE', 'RESTRICT', 'SET_NULL', 'SET_DEFAULT'];
    if (lodash.isNil(def.constraints)) {
        retVal.warnings.push({
            name: 'No constraints',
            description: 'No constraints were found in the property definition.'
        });
        def.constraints = {};
    } else {
        if (lodash.isNil(def.constraints.onDelete)) {
            retVal.warnings.push({
                name: 'No onDelete constraint.',
                description: 'No onDelete constraint was found in the property definition.'
            });
            if (setDefault) {
                def.constraints.onDelete = 'RESTRICT';
            }
        } else {
            if (!lodash.isString(def.constraints.onDelete)) {
                retVal.warnings.push({
                    name: 'Invalid type for onDelete constraint.',
                    description: 'onDelete constraint must be a string.'
                });
                if (setDefault) {
                    def.constraints.onDelete = 'RESTRICT';
                } else {
                    delete def.constraints.onDelete;
                }
            } else if (lodash.indexOf(validConstraints, def.constraints.onDelete) === -1) {
                retVal.warnings.push({
                    name: 'Invalid value for onDelete constraint.',
                    description: 'The onDelete value(' + def.constraints.onDelete + ') isn\'t valid.'
                });
                if (setDefault) {
                    def.constraints.onDelete = 'RESTRICT';
                } else {
                    delete def.constraints.onDelete;
                }
            }
        }

        if (lodash.isNil(def.constraints.onUpdate)) {
            retVal.warnings.push({
                name: 'No onUpdate constraint.',
                description: 'No onUpdate constraint was found in the property definition.'
            });
            if (setDefault) {
                def.constraints.onUpdate = 'RESTRICT';
            }
        } else {
            if (!lodash.isString(def.constraints.onUpdate)) {
                retVal.warnings.push({
                    name: 'Invalid type for onUpdate constraint.',
                    description: 'onUpdate constraint must be a string.'
                });
                if (setDefault) {
                    def.constraints.onUpdate = 'RESTRICT';
                } else {
                    delete def.constraints.onUpdate;
                }
            } else if (lodash.indexOf(validConstraints, def.constraints.onUpdate) === -1) {
                retVal.warnings.push({
                    name: 'Invalid value for onUpdate constraint.',
                    description: 'The onUpdate value(' + def.constraints.onUpdate + ') isn\'t valid.'
                });
                if (setDefault) {
                    def.constraints.onUpdate = 'RESTRICT';
                } else {
                    delete def.constraints.onUpdate;
                }
            }
        }
    }
    return {
        retVal: retVal,
        def: def
    };
};

this.sanitizeRelationships = function(retVal, def, collections, toBeDefinedCollection) {
    let response;
    let isCollection = lodash.has(collections, def.type) || def.type === toBeDefinedCollection.identity;
    if (isCollection) {
        if (!lodash.isNil(def.isonetomany) && def.isonetomany) {
            delete def.isonetoone;
        } else if (!lodash.isNil(def.isonetoone) && def.isonetoone) {
            delete def.isonetomany;
        }
        response = this.checkConstraints(retVal, def);
        retVal = response.retVal;
        def = response.def;
        if (!lodash.isNil(def.type)) {
            if (!(lodash.has(collections, def.type) || def.type === toBeDefinedCollection.identity)) {
                retVal.errors.push(_this.generateError('Invalid type', 'A foreign key needs a defined collection as a type. Current type "' + def.type +
                    '" is not defined.'));
                retVal.isValid = false;
            }
        } else {
            retVal.errors.push(_this.generateError('Invalid type', 'A foreign key needs a collection as a type. Current type is "' + def.type + '"'));
            retVal.isValid = false;
        }
    }
    return {
        sanitizedDefinition: def,
        retVal: retVal
    };
};

this.analyzeFields = function(definition, retVal, collections) {
    let types = _this.getTypes();
    let hanaMap = {};
    let onUpdateFieldsDefault = [];
    let defaultValuesWithSQLStatements = [];
    let autoIncrementFields = [];
    let primaryKeys = [];
    let sanitizedFields = {},
        response,
        collectionFields = {},
        inputParameters = Array.isArray(definition.inputParameters) ? [] : {};
    lodash.forEach(definition.fields, function(def, field) {
        if (lodash.isNil(def.columnName)) {
            def.columnName = field;
        }
        // ADT: Comentando estas 3 lineas mejora la performance.
        //response = _this.sanitizeFieldDefinition(retVal, def, types, field, definition.identity, collections);
        //def = response.sanitizedDefinition;
        //retVal = response.retVal;
        // Populating primaryKeys property for the adapter collection.
        if (!lodash.isNil(def.primaryKey) && def.primaryKey) {
            primaryKeys.push(field);
        }
        if (!lodash.isNil(def.onUpdate)) {
            onUpdateFieldsDefault.push(field);
        }
        if (lodash.isBoolean(def.autoIncrement) && def.autoIncrement) {
            autoIncrementFields.push(field);
        }
        if (lodash.isString(def.default) && lodash.has(_this.getSQLStatementsConstants(), lodash.toLower(def.default))) {
            def.default = lodash.toLower(def.default);
            defaultValuesWithSQLStatements.push(field);
        }
        if (lodash.isString(def.default) && lodash.indexOf(_this.getSQLConstants(), lodash.toLower(def.default)) !== -1) {
            def.default = lodash.toLower(def.default);
        }

        if (lodash.isString(def.onUpdate) && lodash.has(_this.getSQLStatementsConstants(), lodash.toLower(def.onUpdate))) {
            def.onUpdate = lodash.toLower(def.onUpdate);
            defaultValuesWithSQLStatements.push(field);
        }
        if (lodash.isString(def.onUpdate) && lodash.indexOf(_this.getSQLConstants(), lodash.toLower(def.onUpdate)) !== -1) {
            def.onUpdate = lodash.toLower(def.onUpdate);
        }

        if (!lodash.isNil(def.translate) && lodash.isPlainObject(def.translate) && !lodash.isEmpty(def.translate)) {
            def.$translate = {};
            lodash.forEach(def.translate, function(value, key) {
                def.$translate[value] = key;
            });
        }
        // Creating hanaMap (_this will change the db format to collection format).
        if (!lodash.isNil(def.columnName)) {
            if (!/\s/g.test(def.columnName)) {
                hanaMap[def.columnName] = {
                    name: field,
                    type: def.type
                };
                if (!lodash.isNil(def.translate)) {
                    hanaMap[def.columnName].translate = def.translate;
                }
            } else {
                retVal.errors.push(_this.generateError('Invalid columnName', field +
                    ' has an invalid columnName. Must be a non whitespace string.'));
                retVal.isValid = false;
            }
        }
        sanitizedFields[field] = def;
        if (lodash.has(collections, def.type) || def.type === definition.identity) {
            collectionFields[field] = def;
        }
    });
    lodash.forEach(collectionFields, function(def, field) {
        response = _this.sanitizeRelationships(retVal, def, collections, definition, primaryKeys[0]);
        sanitizedFields[field] = response.sanitizedDefinition;
        retVal = response.retVal;
    });
    if (definition.schema === $.viewSchema && (lodash.isNil(definition.inputParameters) || lodash.isEmpty(definition.inputParameters))) {
        let params = $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], true);
        let inputsParams = {};
        lodash.forEach(params, function(value, key) {
            inputsParams[key] = {
                columnName: '$$' + key + '$$',
                isMandatory: false,
                defaultValue: value,
                type: 'string'
            };
        });
        definition.inputParameters = inputsParams;
    }
    lodash.forEach(definition.inputParameters, function(def, parameterName) {
        if (lodash.isNil(def.columnName)) {
            def.columnName = parameterName;
        }

        if (!(lodash.isString(def.type) && lodash.indexOf(types, def.type) > -1)) {
            retVal.errors.push(_this.generateError('Invalid type', parameterName +
                ' has an invalid type("' + def.type + '").'));
            retVal.isValid = false;
        }

        if (!(lodash.isString(def.columnName) && !/\s/g.test(def.columnName))) {
            retVal.errors.push(_this.generateError('Invalid columnName', parameterName +
                ' has an invalid columnName. Must be a non whitespace string.'));
            retVal.isValid = false;
        }

        if (!lodash.isBoolean(def.isMandatory)) {
            def.isMandatory = false;
        }

        inputParameters[parameterName] = def;
    });
    if (!lodash.isEmpty(retVal.errors)) {
        retVal.isValid = false;
    }
    return {
        retVal: retVal,
        primaryKeys: primaryKeys,
        sanitizedFields: sanitizedFields,
        hanaMap: hanaMap,
        onUpdateFieldsDefault: onUpdateFieldsDefault,
        autoIncrementFields: autoIncrementFields,
        inputParameters: inputParameters,
        defaultValuesWithSQLStatements: defaultValuesWithSQLStatements
    };
};

this.sanitizeAliases = function(aliases, aliasMapping, validAliases) {
    let sanitizedAliases = {};
    let sanitizedAliasMapping = {};
    lodash.forEach(validAliases, function(alias) {
        sanitizedAliases[alias] = lodash.cloneDeep(aliases[alias]);
        sanitizedAliasMapping[alias] = lodash.cloneDeep(aliasMapping[alias]);
    });
    return {
        sanitizedAliases: sanitizedAliases,
        sanitizedAliasMapping: sanitizedAliasMapping
    };
};

this.getSelectedColumns = function(fields, collection, collections, aliases, isSubQuery) {
    /* Obtains the SQL Format of each column that was selected. If it's empty, get all columns */
    let retVal = {
        hanaMap: {},
        columns: [],
        validColumns: []
    };
    let parsedColumn;
    let columnName;
    let prefix;
    let tempCollection;
    lodash.forEach(fields, function(field) {
        parsedColumn = _this.parseColumn(collection, collections, aliases, field, true, 'select');
        /* Checks if the column to be added is valid */
        if (parsedColumn.isValid) {
            /* Checks if the column has an alias */
            if (lodash.isString(field.as) && !lodash.isEmpty(field.as)) {
                prefix = isSubQuery ? '' : '@' + field.alias + '_';
                columnName = lodash.isString(field.alias) && lodash.has(aliases, field.alias) ? (prefix + field.as) : field.as;
                parsedColumn.hanaColumn += ' AS "' + columnName + '"';
                retVal.hanaMap[columnName] = {
                    name: columnName,
                    type: parsedColumn.type
                };
                if (lodash.isString(field.alias) && lodash.has(aliases, field.alias)) {
                    tempCollection = collections[aliases[field.alias]];
                } else {
                    tempCollection = collections[collection];
                }
                if (lodash.isString(field.field) && !lodash.isNil(tempCollection.fields[field.field]) && !lodash.isNil(tempCollection.fields[field.field].translate)) {
                    retVal.hanaMap[columnName].translate = tempCollection.fields[field.field].translate;
                    retVal.hanaMap[columnName]['$translate'] = tempCollection.fields[field.field]['$translate'];
                }
                retVal.validColumns.push(columnName);
            } else if (lodash.isString(field.alias) && lodash.has(aliases, field.alias)) {
                prefix = isSubQuery ? '' : '@' + field.alias + '_';
                columnName = prefix + collections[aliases[field.alias]].fields[field.field].columnName;
                parsedColumn.hanaColumn += ' AS "' + columnName + '"';
                retVal.validColumns.push(columnName);
            } else if (!lodash.isNil(collections[collection].fields[field.field])) {
                retVal.validColumns.push(collections[collection].fields[field.field].columnName);
            }
            retVal.columns.push(parsedColumn.hanaColumn);
        }
    });
    return retVal;
};

this._find = function(collection, options, collections, isSubQuery) {
    const createBaseRuntimeModel = $.createBaseRuntimeModel;
    let findQuery = 'SELECT ';
    let aliases = {};
    let currentCollectionAlias;
    let validAliases = [];
    let aliasMapping = {};
    let hanaMap = {};
    let subQueryJoins = [];
    let subQueryMap = {};
    let subQueryIsPrimaryTable = false;
    let isSet = false;
    let placeholderMap = {};
    let prefix;
    isSubQuery = lodash.isBoolean(isSubQuery) ? isSubQuery : false;
    let newCollection;
    /* Reload Cached Model temporarily */
    if (lodash.isBoolean(options.ignoreCache) && options.ignoreCache) {
        newCollection = createBaseRuntimeModel(collections[collection].schema, collections[collection].identity, collections[collection].type !==
            'table', true);
        if (!(lodash.isArray(newCollection.errors) && !lodash.isEmpty(newCollection.errors))) {
            collections[newCollection.getIdentity()] = newCollection.getDefinition();
        }
    }
    /* Prepares the alias map for the select query */
    if (lodash.has(options, 'aliases') && lodash.isArray(options.aliases) && !lodash.isEmpty(options.aliases) && options.aliases.length !== 1) {
        let tempHanaMap = {},
            tempCollectionMap = {};
        lodash.forEach(options.aliases, function(alias) {
            if ((lodash.has(collections, alias.collection) || !lodash.isNil(alias.query) || (lodash.isBoolean(alias.ignoreCache) && alias.ignoreCache &&
                    lodash.isString(alias.schema) && lodash.isString(alias.collection))) && lodash.isString(alias.name) && !lodash.isEmpty(alias
                    .name)) {
                tempHanaMap = {};
                if (lodash.isBoolean(alias.ignoreCache) && alias.ignoreCache) {
                    newCollection = createBaseRuntimeModel(alias.schema, alias.collection, $.viewSchema === alias.schema, true);
                    if (lodash.isArray(newCollection.errors) && !lodash.isEmpty(newCollection.errors)) {
                        return;
                    }
                    collections[newCollection.getIdentity()] = newCollection.getDefinition();
                }
                if (!lodash.isNil(alias.query) && lodash.isPlainObject(alias.query)) {
                    aliases[alias.name] = alias.collection;
                    subQueryJoins.push(alias.name);
                    subQueryMap[alias.name] = _this._find(alias.collection, alias.query, collections, true);
                    tempCollectionMap = lodash.cloneDeep(subQueryMap[alias.name].hanaMap);
                    if (lodash.isBoolean(alias.isPrimary) && alias.isPrimary) {
                        subQueryIsPrimaryTable = true;
                        currentCollectionAlias = alias.name;
                        aliasMapping[alias.name] = {
                            isPrimary: true,
                            map: null
                        };
                    }
                    collections[alias.name] = _this.simulateInitForSubQueries(alias.name, lodash.cloneDeep(subQueryMap[alias.name].hanaMap));
                    aliases[alias.name] = alias.name;
                } else if (lodash.isNil(alias.query)) {
                    if (lodash.isBoolean(alias.isPrimary) && alias.isPrimary) {
                        currentCollectionAlias = alias.name;
                        aliasMapping[alias.name] = {
                            isPrimary: true,
                            map: null
                        };
                    } else if (alias.collection === collection && lodash.isEmpty(currentCollectionAlias)) {
                        currentCollectionAlias = alias.name;
                        aliasMapping[alias.name] = {
                            isPrimary: true,
                            map: null
                        };
                    }
                    aliases[alias.name] = alias.collection;
                    tempCollectionMap = lodash.cloneDeep(collections[alias.collection].hanaMap);
                }
                lodash.forEach(tempCollectionMap, function(value, column) {
                    prefix = isSubQuery ? '' : '@' + alias.name + '_';
                    tempHanaMap[prefix + column] = {
                        type: value.type,
                        name: prefix + value.name,
                        alias: alias.name,
                        columnName: column,
                        translate: value.translate
                    };
                });
                lodash.assign(hanaMap, tempHanaMap);
            }
        });
    }
    if (lodash.isEmpty(hanaMap)) {
        hanaMap = lodash.cloneDeep(collections[collection].hanaMap);
        lodash.forEach(hanaMap, function(value, column) {
            value.alias = _this.getCollectionName(collections, {}, collection, false, placeholderMap, true);
            hanaMap[column] = value;
        });
    }
    /* Adds the from clause to the query */
    let fromQuery = ' FROM ';
    /* Adds the current collection to the select query */
    let _alias = collection;
    let _checkAlias = false;
    /* Checks if the table has an alias */
    if (lodash.has(aliases, currentCollectionAlias) && lodash.has(collections, aliases[currentCollectionAlias])) {
        _alias = currentCollectionAlias;
        _checkAlias = true;
    } else if (!lodash.isEmpty(lodash.keys(aliases)) && aliases[lodash.keys(aliases)[0]] === collection) {
        _alias = lodash.keys(aliases)[0];
        _checkAlias = true;
    }
    /* Prepares the placeholder map for the select query */
    if (collections[collection].type === 'view' && collections[collection].schema === $.viewSchema) {
        options.placeholders = [{
            alias: _alias,
            params: _this.getPlaceHolders(collections[collection].inputParameters || [])
        }];
    }


    if (collections[collection].type === 'view' && lodash.isArray(options.placeholders) && !lodash.isEmpty(options.placeholders)) {
        let placeholderResponse;
        if (!_checkAlias) {
            placeholderResponse = _this.parsePlaceholder(collections[_alias].inputParameters, _alias, options.placeholders[0]);
            if (placeholderResponse.isValid) {
                placeholderMap[_alias] = placeholderResponse.parsedPlaceholder;
            }
        } else {
            lodash.forEach(options.placeholders, function(placeholder) {
                placeholderResponse = _this.parsePlaceholder(collections[aliases[placeholder.alias]].inputParameters, placeholder.alias, placeholder);
                if (placeholderResponse.isValid) {
                    placeholderMap[placeholder.alias] = placeholderResponse.parsedPlaceholder;
                }
            });
        }
    }
    if (!subQueryIsPrimaryTable) {
        fromQuery += ' ' + _this.getCollectionName(collections, aliases, _alias, _checkAlias, placeholderMap) + ' ';
    } else {
        fromQuery += ' (' + subQueryMap[currentCollectionAlias].findQuery +
            ' ) AS "' + currentCollectionAlias + '"';
    }

    /* Adds the join clause to the query */
    if (lodash.isArray(options.join) && !lodash.isEmpty(options.join)) {
        let joins = [];
        let retVal;

        lodash.forEach(options.join, function(joinMetadata) {
            if (_this.isValidJoin(joinMetadata, collections, aliases, lodash.indexOf(subQueryJoins, joinMetadata.alias) > -1)) {
                retVal = _this.parseJoin(collection, joinMetadata, collections, aliases, subQueryJoins, subQueryMap, placeholderMap);
                if (retVal.isValid) {
                    joins.push(retVal.joinQuery);
                    validAliases.push(joinMetadata.alias);
                    if (lodash.isString(joinMetadata.map) && !lodash.isEmpty(joinMetadata.map)) {
                        aliasMapping[joinMetadata.alias] = {
                            map: joinMetadata.map,
                            isPrimary: false
                        };
                    }
                }
            }
        });
        if (!lodash.isEmpty(joins)) {
            fromQuery += ' ' + lodash.join(joins, ' ') + ' ';
        }
    }

    /* Adds the setOperations clause to the query */
    if (lodash.isArray(options.setOperations) && !lodash.isEmpty(options.setOperations)) {
        let sets = [];
        let setRetVal;
        lodash.forEach(options.setOperations, function(setMetadata) {
            if (_this.isValidSetOperation(setMetadata, collections, aliases, lodash.indexOf(subQueryJoins, setMetadata.alias) > -1)) {
                setRetVal = _this.parseSetOperation(collection, setMetadata, collections, aliases, subQueryJoins, subQueryMap, placeholderMap);
                if (setRetVal.isValid) {
                    sets.push(setRetVal.setQuery);
                    validAliases.push(setMetadata.alias);
                    if (lodash.isString(setMetadata.map) && !lodash.isEmpty(setMetadata.map)) {
                        aliasMapping[setMetadata.alias] = {
                            map: setMetadata.map,
                            isPrimary: false
                        };
                    }
                }
            }
        });
        if (!lodash.isEmpty(sets)) {
            isSet = true;
            fromQuery += ' ' + lodash.join(sets, ' ') + ' ';
        }
    }

    if (!lodash.isEmpty(validAliases)) {
        validAliases.unshift(currentCollectionAlias);
        const retValSanitizedAliases = _this.sanitizeAliases(aliases, aliasMapping, validAliases);
        aliases = lodash.cloneDeep(retValSanitizedAliases.sanitizedAliases);
        aliasMapping = lodash.cloneDeep(retValSanitizedAliases.sanitizedAliasMapping);
        let newHanaMap = {};
        let validAliasesForNewHanaMap = [];
        lodash.forEach(aliasMapping, function(value, alias) {
            if (!lodash.isNil(value)) {
                validAliasesForNewHanaMap.push(alias);
            }
        });
        let tempKey;
        const invalidTypesForSets = _this.getInvalidTypesForSets();
        lodash.forEach(validAliasesForNewHanaMap, function(validAlias) {
            tempKey = isSubQuery ? '' : '@' + validAlias + '_';
            lodash.forEach(hanaMap, function(value, key) {
                if (key.indexOf(tempKey) > -1 && ((isSet && lodash.indexOf(invalidTypesForSets, value.type)) || (!isSet))) {
                    if (isSet) {
                        delete value.alias;
                        value.name = value.name.replace(tempKey, '');
                        _checkAlias = false;
                        aliasMapping = {};
                        _alias = collection;
                        aliases = {};
                        newHanaMap[key.replace(tempKey, '')] = lodash.cloneDeep(value);
                    } else {
                        newHanaMap[key] = lodash.cloneDeep(value);
                    }
                }
            });
        });
        hanaMap = lodash.cloneDeep(newHanaMap);
    }

    let columns = [];
    /* Adds distinct to the select query */
    if (lodash.isBoolean(options.distinct) && options.distinct) {
        findQuery += ' DISTINCT ';
    }
    let retValSelect, validColumns, tempSelectHanaMap;
    /* Removes the specified columns from the query */
    if (lodash.isArray(options.xselect) && !lodash.isEmpty(options.xselect)) {
        let retValXSelect;
        lodash.forEach(options.xselect, function(field) {
            retValXSelect = _this.parseColumn(collection, collections, aliases, field, true, 'select');
            if (retValXSelect.isValid) {
                if (lodash.isString(field.alias) && !lodash.isNil(aliases[field.alias])) {
                    prefix = isSubQuery ? '' : '@' + field.alias + '_';
                    if (lodash.has(hanaMap, '"' + prefix + collections[aliases[field.alias]].fields[field.field].columnName + '"')) {
                        delete hanaMap['"' + prefix + collections[aliases[field.alias]].fields[field.field].columnName + '"'];
                    } else if (lodash.has(hanaMap, prefix + collections[aliases[field.alias]].fields[field.field].columnName)) {
                        delete hanaMap[prefix + collections[aliases[field.alias]].fields[field.field].columnName];
                    }
                } else {
                    delete hanaMap[collections[collection].fields[field.field].columnName];
                }
            }
        });
        findQuery += _this.getAllFindQueryColumns(hanaMap, validAliases, !lodash.isEmpty(validAliases) && !isSet, isSet);
    } else if (lodash.isArray(options.select) && !lodash.isEmpty(options.select)) {
        /* Obtains the SQL Format of each column that was selected. If it's empty, get all columns */
        retValSelect = _this.getSelectedColumns(options.select, collection, collections, aliases, isSubQuery);
        columns = retValSelect.columns;
        validColumns = retValSelect.validColumns;
        if (!lodash.isEmpty(columns)) {
            hanaMap = lodash.assign(hanaMap, retValSelect.hanaMap);
            findQuery += columns.join(',');
            tempSelectHanaMap = {};
            lodash.forEach(validColumns, function(column) {
                if (!lodash.isNil(hanaMap[column])) {
                    tempSelectHanaMap[column] = lodash.cloneDeep(hanaMap[column]);
                }
            });
            hanaMap = lodash.cloneDeep(tempSelectHanaMap);
        } else {
            findQuery += _this.getAllFindQueryColumns(hanaMap, validAliases, !lodash.isEmpty(validAliases) && !isSet, isSet);

        }
    } else if (lodash.isArray(options.$select) && !lodash.isEmpty(options.$select)) {
        /* Obtains the SQL Format of each column that is going to be added. */
        retValSelect = _this.getSelectedColumns(options.$select, collection, collections, aliases, isSubQuery);
        columns = retValSelect.columns;
        validColumns = retValSelect.validColumns;
        findQuery += _this.getAllFindQueryColumns(hanaMap, validAliases, !lodash.isEmpty(validAliases) && !isSet, isSet);
        if (!lodash.isEmpty(columns)) {
            hanaMap = lodash.assign(hanaMap, retValSelect.hanaMap);
            findQuery += ', ' + columns.join(',');
        }
    } else {
        findQuery += _this.getAllFindQueryColumns(hanaMap, validAliases, !lodash.isEmpty(validAliases) && !isSet, isSet);
    }

    findQuery += fromQuery;

    /* Adds the where clause to the query*/
    if (lodash.isArray(options.where) && !lodash.isEmpty(options.where)) {
        let parsedWhere = _this.parseWhere(collection, collections, options.where, aliases);
        if (!lodash.isNil(parsedWhere)) {
            findQuery += ' WHERE ' + parsedWhere + ' ';
        }
    }
    /* Adds the group by clause to the query */
    if (lodash.isArray(options.groupBy) && !lodash.isEmpty(options.groupBy)) {
        let retValGroupBy;
        columns = [];
        lodash.forEach(options.groupBy, function(field) {
            retValGroupBy = _this.parseColumn(collection, collections, aliases, field, false, 'groupBy');
            /* Checks if the column to be added is valid */
            if (retValGroupBy.isValid) {
                columns.push(retValGroupBy.hanaColumn);
            }
        });
        if (!lodash.isEmpty(columns)) {
            findQuery += ' GROUP BY ' + lodash.join(columns, ',') + ' ';
            /* Adds the having clause to the query */
            if (lodash.isArray(options.having) && !lodash.isEmpty(options.having)) {
                let parsedHaving = _this.parseWhere(collection, collections, options.having, aliases);
                if (!lodash.isNil(parsedHaving)) {
                    findQuery += ' HAVING ' + parsedHaving + ' ';
                }
            }
        }
    }

    /* Adds the order by clause to the query */
    if (lodash.isArray(options.orderBy) && !lodash.isEmpty(options.orderBy)) {
        let retValOrderBy;
        columns = [];
        lodash.forEach(options.orderBy, function(field) {
            retValOrderBy = _this.parseColumn(collection, collections, aliases, field, false, 'orderBy');
            /* Checks if the column to be added is valid */
            if (retValOrderBy.isValid) {
                /* Checks if the column has an alias */
                if (lodash.isString(field.type) && lodash.indexOf(['ASC', 'DESC'], lodash.toUpper(field.type)) > -1) {
                    retValOrderBy.hanaColumn += ' ' + lodash.toUpper(field.type) + ' ';
                } else {
                    retValOrderBy.hanaColumn += ' ASC';
                }
                columns.push(retValOrderBy.hanaColumn);
            }
        });
        if (!lodash.isEmpty(columns)) {
            findQuery += ' ORDER BY ' + lodash.join(columns, ',') + ' ';
        }
    }
    /* Changes the top to limit */
    if (lodash.isInteger(options.top) && options.top > 0) {
        options.paginate = {
            limit: options.top
        };
    }
    /* Adds the pagination clause to the query */
    if (lodash.isPlainObject(options.paginate) && !lodash.isNil(options.paginate)) {
        if (lodash.isInteger(options.paginate.limit) && options.paginate.limit > -1) {
            findQuery += ' LIMIT ' + options.paginate.limit + ' ';
            if (lodash.isInteger(options.paginate.offset) && options.paginate.offset > -1) {
                findQuery += ' OFFSET ' + options.paginate.offset + ' ';
            }
        }
    }

    /* Adds the hint clause to the query */
    if (lodash.isArray(options.hint) && !lodash.isEmpty(options.hint)) {
        const hintRetVal = _this.parseHint(collection, options.hint, collections, aliases);
        if (hintRetVal.isValid) {
            findQuery += ' WITH HINT(' + lodash.join(hintRetVal.parsedHints, ', ') + ') ';
        }
    }
    /* Add the count to the query */
    if (lodash.isBoolean(options.count) && options.count) {
        findQuery = 'SELECT COUNT(*) as "tableCount" FROM (' + findQuery + ')';
        hanaMap = {
            'tableCount': {
                'type': 'integer',
                'name': 'tableCount'
            }
        };
        aliasMapping = {};
    }

    return {
        findQuery: findQuery,
        hanaMap: hanaMap,
        aliasMapping: aliasMapping
    };
};

this.getAllFindQueryColumns = function(hanaMap, validAliases, useJoins, isSet) {
    useJoins = useJoins || false;
    let columns = [];
    const invalidTypesForSets = _this.getInvalidTypesForSets();
    lodash.forEach(hanaMap, function(value, column) {
        if ((isSet && lodash.indexOf(invalidTypesForSets, value.type) === -1) || (!isSet)) {
            if ((!useJoins || (useJoins && lodash.indexOf(validAliases, value.alias) > -1)) && lodash.isString(value.columnName)) {
                if (isSet) {
                    columns.push('"' + value.columnName + '"');
                } else {
                    columns.push('"' + value.alias + '"."' + value.columnName + '" AS "' + column + '"');
                }
            } else if (useJoins && lodash.indexOf(validAliases, value.alias) > -1) {
                columns.push(value.alias + '."' + column + '"');
            } else if (!useJoins) {
                if (isSet) {
                    columns.push('"' + column + '"');
                } else {
                    columns.push(value.alias + '."' + column + '"');
                }
            }
        }
    });
    return lodash.join(columns, ',');
};

this.batchQuery = function(collection, collections, instances, options, operation) {
    let retVal = {
        errors: [],
        query: null,
        values: [],
        whereFields: {}
    };
    let batchMap = {
        operation: {
            'create': 'INSERT INTO ',
            'upsert': 'UPSERT'
        },
        withPrimaryKey: {
            'create': '',
            'upsert': ' WITH PRIMARY KEY'
        }
    };
    const ignoreTheseTypes = ['text', 'shorttext', 'json', 'file'];
    const textTypes = ['string', 'text', 'boolean', 'binary', 'json', 'shorttext', 'file'];
    try {
        if (!lodash.has(collections, collection)) {
            retVal.errors.push(_this.generateError('Invalid Model', '"' + collections[collection].identity + '" doesn\'t exist.'));
            throw retVal;
        } else if (!lodash.isArray(instances) || (lodash.isArray(instances) && lodash.isEmpty(instances))) {
            retVal.errors.push(_this.generateError('Invalid instances', 'Instances must be an array of objects.'));
            throw retVal;
        } else if (collections[collection].type !== 'table') {
            retVal.errors.push(_this.generateError('Invalid collection\'s type', '"' + collections[collection].identity + '" isn\'t a table.'));
            throw retVal;
        }

        let sequenceMap = {};
        lodash.forEach(collections[collection].autoIncrementFields, function(attributeName) {
            if (!lodash.isNil(collections[collection].fields[attributeName].autoIncrement) && collections[collection].fields[attributeName].autoIncrement) {
                sequenceMap[attributeName] = '"' + collections[collection].schema + '"."' + collections[collection].identity + '::' + collections[
                    collection].fields[
                    attributeName].columnName + '".nextVal';
            }
        });
        let defaultSQLMap = {};
        lodash.forEach(collections[collection].defaultValuesWithSQLStatements, function(attributeName) {
            if (!lodash.isNil(collections[collection].fields[attributeName].default) && collections[collection].fields[attributeName].default) {
                defaultSQLMap[attributeName] = _this.getHanaValue(collections[collection].fields[attributeName].type, collections[collection].fields[
                    attributeName].default);
            }
        });
        retVal.query = batchMap.operation[operation] + ' "' + collections[collection].schema + '"."' + collections[collection].identity + '"';
        let valueQuery = [];
        let columnOrder = lodash.keys(collections[collection].fields);
        if(options.fields){
            columnOrder = options.fields;
        }
        let columns = [];
        lodash.forEach(columnOrder, function(column) {
            columns.push('"' + collections[collection].fields[column].columnName + '"');
            if (lodash.isString(sequenceMap[column]) && lodash.indexOf(options.ignoreSequenceFields, column) === -1) {
                valueQuery.push(sequenceMap[column]);
            } else if (lodash.isString(defaultSQLMap[column]) && lodash.indexOf(options.ignoreSQLStatementDefaults, column) === -1) {
                valueQuery.push(defaultSQLMap[column]);
            } else {
                valueQuery.push('?');
            }
        });
        let value, tempValue;
        let parseTheValueIgnoringSequenceValue, parseTheValueIgnoringDefaultSQLValue;
        lodash.forEach(instances, function(instance) {
            value = [];
            lodash.forEach(columnOrder, function(column) {
                parseTheValueIgnoringSequenceValue = (!(lodash.indexOf(collections[collection].autoIncrementFields, column) > -1 && lodash.isEmpty(
                        options.ignoreSequenceFields)) &&
                    (!lodash.isString(sequenceMap[column]) || !(lodash.isString(sequenceMap[column]) && lodash.indexOf(options.ignoreSequenceFields,
                        column) === -1)));

                parseTheValueIgnoringDefaultSQLValue = (!(lodash.indexOf(collections[collection].defaultValuesWithSQLStatements, column) > -1 &&
                        lodash.isEmpty(
                            options.ignoreSQLStatementDefaults)) &&
                    (!lodash.isString(defaultSQLMap[column]) || !(lodash.isString(defaultSQLMap[column]) && lodash.indexOf(options.ignoreSQLStatementDefaults,
                        column) === -1)));
                if (parseTheValueIgnoringSequenceValue && parseTheValueIgnoringDefaultSQLValue) {
                    if ((lodash.isNil(instance[column]) || (lodash.indexOf(textTypes, collections[collection].fields[column].type) === -1 && instance[column] === '')) && !lodash.isNil(collections[collection].fields[column].default)) {
                        tempValue = _this.getHanaValue(collections[collection].fields[column].type, collections[collection].fields[column].default, true);
                        if (tempValue === 'NULL') {
                            tempValue = null;
                        } else if (collections[collection].fields[column].type !== 'file') {
                            tempValue = tempValue.substring(1, tempValue.length - 1);
                        }
                        if (lodash.indexOf(ignoreTheseTypes, collections[collection].fields[column].type) === -1) {
                            if (!lodash.has(retVal.whereFields, column)) {
                                retVal.whereFields[column] = {
                                    values: [],
                                    hasNull: false
                                };
                            }
                            if (!lodash.isNil(collections[collection].fields[column].default)) {
                                if (lodash.indexOf(_this.getSQLConstants(), collections[collection].fields[column].default) === -1) {
                                    retVal.whereFields[column].values.push(collections[collection].fields[column].default);
                                }
                            } else {
                                retVal.whereFields[column].hasNull = true;
                            }
                        }
                    } else if (lodash.has(instance, column)) {
                        if (!lodash.isNil(collections[collection].fields[column].$translate) && lodash.isPlainObject(collections[collection].fields[column]
                                .$translate)) {
                            instance[column] = _this.getTranslateValue(collections[collection].fields[column], instance[column]);
                        }
                        if (lodash.indexOf(ignoreTheseTypes, collections[collection].fields[column].type) === -1) {
                            if (!lodash.has(retVal.whereFields, column)) {
                                retVal.whereFields[column] = {
                                    values: [],
                                    hasNull: false
                                };
                            }
                            if (!lodash.isNil(instance[column]) && (lodash.indexOf(textTypes, collections[collection].fields[column].type) !== -1 || instance[column] !== '')) {
                                retVal.whereFields[column].values.push(instance[column]);
                            } else {
                                retVal.whereFields[column].hasNull = true;
                            }
                        }
                        tempValue = _this.getHanaValue(collections[collection].fields[column].type, instance[column], true);
                        if (tempValue === 'NULL') {
                            tempValue = null;
                        } else if (collections[collection].fields[column].type !== 'file') {
                            tempValue = tempValue.substring(1, tempValue.length - 1);
                        }
                    } else {
                        tempValue = null;
                    }
                    value.push(tempValue);
                }
            });
            retVal.values.push(value);
        });
        if (options.getInstances) {
            let parsedWhereFields = [];
            let tempCondition;
            lodash.forEach(retVal.whereFields, function(whereField, column) {
                tempCondition = [];
                if (!lodash.isEmpty(whereField.values)) {
                    tempCondition.push({
                        field: column,
                        operator: '$in',
                        value: whereField.values
                    });
                }
                if (whereField.hasNull) {
                    tempCondition.push({
                        field: column,
                        operator: '$eq',
                        value: null
                    });
                }
                if (!lodash.isEmpty(tempCondition)) {
                    if (tempCondition.length === 1) {
                        parsedWhereFields = lodash.concat(parsedWhereFields, tempCondition);
                    } else if (tempCondition.length > 1) {
                        parsedWhereFields.push(tempCondition);
                    }
                }
            });
            retVal.whereFields = {
                where: parsedWhereFields
            };
        } else {
            delete retVal.whereFields;
        }
        retVal.query += '(' + lodash.join(columns, ',') + ') VALUES(' + lodash.join(valueQuery, ',') + ') ' + batchMap.withPrimaryKey[operation];
        return retVal;
    } catch (e) {
        let error = {};
        retVal.query = null;
        error = _this.parseError(e);
        if (!lodash.isEqual(retVal, error)) {
            retVal.errors.push(error);
        }
        return retVal;
    }
};