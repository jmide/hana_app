$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

$.import('timp.core.server.orm', 'tableServices');
const tableServices = $.timp.core.server.orm.tableServices;

const _this = this;

/**
 * Transfer source data to target.
 * @returns {Object} status - the status of the table after the operation.
 * @example
 * // {
 * //   errors: [],
 * //   results: {
 * //       imported: true
 * //       sourTable: null
 * //   }
 * // }
 * @param {string} sourceSchema - source's schema.
 * @example
 * // 'TIMP'
 * @param {string} sourceIdentity - source's name.
 * @example
 * // 'CORE::User'
 * @param {string} targetSchema - target's schema.
 * @example
 * // 'TIMP'
 * @param {string} targetIdentity - target's name.
 * @example
 * // 'CORE::User2'
 * @param {Object} attributeMap - source and target fields mapping.
 * @example
 * // {
 * //   id : {
 * //       source: 'ID',
 * //       preProcessFn: function(param) {
 * //           return param.trim(' ');
 * //       }
 * //   }
 * //   name : {
 * //       source: 'name'
 * //   }
 * // }
 * @param {integer} pagination - source data batch size (default 50).
 * @example
 * // 40
 * @param {boolean} dropSourceModel - if you want to drop the source model after the data migration (default=false).
 * @example
 * // true
 */
_this.importData = function(sourceSchema, sourceIdentity, targetSchema, targetIdentity, attributeMap, options) {
	let retVal = {
		errors: [],
		results: {
			imported: true,
			skipped: false,
			sourceTable: null,
			instances: []
		}
	};
	options = (!lodash.isNil(options) && lodash.isPlainObject(options)) ? options : {};
	let pagination = options.pagination;
	let dropSourceModel = options.dropSourceModel;
	let importDataOnlyIfEmpty = options.importDataOnlyIfEmpty;
	let ignoreSequenceFields = [];
	let fieldsWithTranslate = [];
	let filters = options.filters;
	let getInstances = options.getInstances;
	let sourceModel = options.sourceModel;
	let targetModel = options.targetModel;
	if (!lodash.isNumber(pagination) || (lodash.isNumber(pagination) && pagination <= 0)) {
		pagination = 200;
	}
	dropSourceModel = lodash.isBoolean(dropSourceModel) ? dropSourceModel : false;
	importDataOnlyIfEmpty = lodash.isBoolean(importDataOnlyIfEmpty) ? importDataOnlyIfEmpty : false;
	filters = lodash.isArray(filters) ? filters : [];
	getInstances = lodash.isBoolean(getInstances) ? getInstances : false;
	sourceModel = sourceModel instanceof BaseModel.BaseModel ? sourceModel : null;
	targetModel = targetModel instanceof BaseModel.BaseModel ? targetModel : null;
	let pages = -1;
	try {
		if (!(lodash.isString(sourceSchema) && !lodash.isEmpty(sourceSchema) && lodash.isString(sourceIdentity) && !lodash.isEmpty(sourceIdentity))) {
			retVal.errors.push(resources.generateError('Invalid Source', 'The sourceSchema or sourceIdentity isn\'t a valid string.'));
			throw retVal;
		} else if (!(lodash.isString(targetSchema) && !lodash.isEmpty(targetSchema) && lodash.isString(targetIdentity) && !lodash.isEmpty(
			targetIdentity))) {
			retVal.errors.push(resources.generateError('Invalid Target', 'The targetSchema or targetIdentity isn\'t a valid string.'));
			throw retVal;
		} else if (!(lodash.isPlainObject(attributeMap) && !lodash.isEmpty(attributeMap))) {
			retVal.errors.push(resources.generateError('Invalid Attribute Map', 'Attribute map must be a non empty object.'));
			throw retVal;
		}
		// Validates that the source table exist
		let query = 'SELECT "TABLE_NAME" FROM "PUBLIC"."TABLES" WHERE "SCHEMA_NAME"=\'' + sourceSchema + '\' AND "TABLE_NAME"=\'' +
			sourceIdentity + '\'';
		let response = connector.execute(query, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (lodash.isEmpty(response.results)) {
			retVal.errors.push(resources.generateError('Invalid Source', 'Source table doesn\'t exist.'));
			throw retVal;
		}
		// Validates that the target table exist
		query = 'SELECT "TABLE_NAME" FROM "PUBLIC"."TABLES" WHERE "SCHEMA_NAME"=\'' + targetSchema + '\' AND "TABLE_NAME"=\'' + targetIdentity +
			'\'';
		response = connector.execute(query, null);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (lodash.isEmpty(response.results)) {
			retVal.errors.push(resources.generateError('Invalid Target', 'Target table doesn\'t exist.'));
			throw retVal;
		}

		// Enables the BaseModel operations to the source and target table
		const sourceTable = !lodash.isNil(sourceModel) ? sourceModel : _this.createBaseRuntimeModel(sourceSchema, sourceIdentity);
		const targetTable = !lodash.isNil(targetModel) ? targetModel : _this.createBaseRuntimeModel(targetSchema, targetIdentity);

		try {
			response = targetTable.find({
				count: true
			});
		} catch (e) {
			response = e;
		}

		if (lodash.isArray(response.errors) && !lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		}
		let tableCount = (lodash.isArray(response.results) && !lodash.isEmpty(response.results) && !lodash.isNil(response.results[0]) && lodash.isPlainObject(
			response.results[0]) && lodash.isInteger(response.results[0].tableCount)) ? response.results[0].tableCount : 0;
		if (importDataOnlyIfEmpty && tableCount > 0) {
			retVal.results.skipped = true;
			return retVal;
		}
		// Gets the batch size
		try {
			response = sourceTable.find({
				select: [{
					'function': {
						name: 'CEIL',
						params: [{
							calculation: {
								operator: '/',
								params: [{
										aggregation: {
											type: 'COUNT',
											param: {
												literal: {
													type: 'string',
													value: '*'
												}
											}
										}
                            },
    						pagination]
							}
                    }]
					},
					as: 'pages'
            }],
				where: filters
			});
		} catch (e) {
			response = e;
		}

		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = lodash.concat(response.errors, retVal.errors);
			throw retVal;
		} else if (!lodash.isEmpty(response.results)) {
			pages = Number(response.results[0].pages);
		} else {
			return retVal;
		}
		// Sanitizes the attribute map
		let sanitizedAttributeMap = {};
		lodash.forEach(attributeMap, function(value, targetKey) {
			if (!lodash.isNil(targetTable.getDefinition().fields[targetKey]) && !lodash.isNil(sourceTable.getDefinition().fields[value.source])) {
				value.targetMetadata = targetTable.getDefinition().fields[targetKey];
				value.sourceMetadata = sourceTable.getDefinition().fields[value.source];
				sanitizedAttributeMap[targetKey] = value;
				if (!lodash.isFunction(sanitizedAttributeMap[targetKey].preProcessFn)) {
					delete sanitizedAttributeMap[targetKey].preProcessFn;
				}
				if (lodash.isBoolean(value.ignoreSequence) && value.ignoreSequence) {
					ignoreSequenceFields.push(targetKey);
				}

				if (lodash.isBoolean(value.useTranslate) && value.useTranslate) {
					fieldsWithTranslate.push(targetKey);
				}
			}
		});
		if (lodash.keys(sanitizedAttributeMap).length !== lodash.keys(attributeMap).length) {
			retVal.errors.push(resources.generateError('Invalid Attribute Map', 'Attribute map doesn\'t contain valid fields.'));
			throw retVal;
		}
		const sourceOrderBy = [];
		if (!lodash.isEmpty(sourceTable.getDefinition().primaryKeys)) {
			lodash.forEach(sourceTable.getDefinition().primaryKeys, function(primaryKey) {
				sourceOrderBy.push({
					field: primaryKey,
					type: 'ASC'
				});
			});
		}
		const validConversions = resources.getValidConversions();
		if (!lodash.isEmpty(validConversions.errors)) {
			retVal.errors = lodash.concat(validConversions.errors, retVal.errors);
			throw retVal;
		}
		for (let page = 0; page <= pages; ++page) {
			try {
				response = sourceTable.find({
					orderBy: sourceOrderBy,
					where: filters,
					paginate: {
						limit: pagination,
						offset: page * pagination
					}
				});
			} catch (e) {
				response = e;
			}

			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.concat(response.errors, retVal.errors);
			} else if (!lodash.isEmpty(response.results)) {
				response = resources.parseImportData(response.results, sanitizedAttributeMap, validConversions.results, fieldsWithTranslate);
				if (!lodash.isEmpty(response.errors)) {
					retVal.errors = lodash.concat(response.errors, retVal.errors);
				} else if (response.results.isValid) {
					try {
						response = targetTable.batchCreate(response.results.parsedInstances, {
							ignoreSequenceFields: ignoreSequenceFields,
							getInstances: getInstances
						});
					} catch (e) {
						response = e;
					}
					if (!lodash.isEmpty(response.errors)) {
						retVal.errors = lodash.concat(response.errors, retVal.errors);
					} else if (lodash.isBoolean(response.results.created) && !response.results.created) {
						retVal.errors.push(resources.generateError('Invalid Data', 'Something went wrong during the batchCreate.'));
					} else if (getInstances && lodash.isArray(response.results.instances) && !lodash.isEmpty(response.results.instances)) {
						retVal.results.instances = lodash.concat(retVal.results.instances, response.results.instances);
					}
				}
			}
		}
		if (!lodash.isEmpty(retVal.errors)) {
			throw retVal;
		} else if (dropSourceModel) {
			let dropResponse;
			try {
				dropResponse = sourceTable.drop(true, true);
			} catch (e) {
				dropResponse = e;
			}
			if (!lodash.isEmpty(dropResponse.errors)) {
				retVal.errors = lodash.concat(dropResponse.errors, retVal.errors);
				throw retVal;
			}
			retVal.results.sourceTable = dropResponse.results;
		}
		return retVal;
	} catch (e) {
		retVal.results.imported = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};

/**
 * Creates a BaseModel based in the provided schema and name.
 * @returns {BaseModel|Object} status - the status of the sequence after the operation.
 * @example
 * // returns new BaseModel()
 * @example
 * // {
 * //   errors: [{
 * //       name: 'Invalid Model',
 * //       description: 'Model doesn\'t exist.'
 * //   }],
 * //   results: {
 * //       created: false
 * //       baseRuntimeModel: null
 * //   }
 * // }
 * @param {string} schema - model's schema.
 * @example
 * // '_SYS_BIC'
 * @param {string} name - model's name.
 * @example
 * // 'timp.atr.modeling.tfp/BLOQUEIOS'
 * @param {boolean} isCalculationView - true if it is a calculation view, false if it is a table.
 * @example
 * // true
 */
_this.createBaseRuntimeModel = function(schema, name, isCalculationView, dontAddCollectionToCache) {
	let retVal = {
		errors: [],
		results: {
			created: true,
			baseRuntimeModel: null
		}
	};
	try {
		if (!lodash.isBoolean(isCalculationView)) {
			isCalculationView = false;
		}
		if (!lodash.isBoolean(dontAddCollectionToCache)) {
			dontAddCollectionToCache = false;
		}
		if (!lodash.isString(schema) || (lodash.isString(schema) && lodash.isEmpty(schema))) {
			retVal.errors.push(resources.generateError('Invalid schema', 'Schema ("' + schema + '") must be a non-empty string.'));
			throw retVal;
		} else if (!lodash.isString(name) || (lodash.isString(name) && lodash.isEmpty(name))) {
			retVal.errors.push(resources.generateError('Invalid name', 'Name ("' + name + '") must be a non-empty string.'));
			throw retVal;
		} else {
			let options = {
				name: name,
				schema: schema,
				identity: name,
				type: isCalculationView ? 'view' : 'table',
				inputParameters: {},
				fields: {}
			};
			let response = tableServices.getDataBaseFieldsDefinition(schema, name, isCalculationView);
			if (!lodash.isEmpty(response.errors)) {
				retVal.errors = lodash.cloneDeep(response.errors);
				throw retVal;
			}

			lodash.assign(options, response.results.definition);
			if (isCalculationView) {
				let params = $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], true);
				let inputsParams = {};
				lodash.forEach(params, function(value, key) {
					inputsParams[key] = {
						columnName: '$$' + key + '$$',
						isMandatory: false,
						defaultValue: value,
						type: 'string'
					};
				});
				options.inputParameters = inputsParams;
			}
			let baseRuntimeModel = new BaseModel.BaseModel(options, false, dontAddCollectionToCache);
			if (lodash.isNil(baseRuntimeModel)) {
				retVal.errors.push(resources.generateError('Invalid Model', 'options("' + JSON.stringify(options) + '" aren\'t valid.'));
				throw retVal;
			}
			return new BaseModel.BaseModel(options, false, dontAddCollectionToCache);
		}
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};
$.createBaseRuntimeModel = _this.createBaseRuntimeModel;

/**/
_this.createTableFromQuery = function(schema, name, query) {
	let retVal = {
		errors: [],
		results: {
			created: true
		}
	};
	try {
		if (!(lodash.isString(schema) && !lodash.isEmpty(schema))) {
			retVal.errors.push(resources.generateError('Invalid Schema', 'Schema ("' + schema + '") must be a non-empty string.'));
			throw retVal;
		} else if (!(lodash.isString(name) && !lodash.isEmpty(name))) {
			retVal.errors.push(resources.generateError('Invalid Name', 'Name ("' + name + '") must be a non-empty string.'));
			throw retVal;
		} else if (!(lodash.isPlainObject(query) && !lodash.isEmpty(query))) {
			retVal.errors.push(resources.generateError('Invalid Query', 'Query ("' + query + '") must be a non-empty object.'));
			throw retVal;
		}
		const useJoins = $.lodash.isArray(query.join) && !$.lodash.isEmpty(query.join);
		const dummyModel = _this.createBaseRuntimeModel('SYS', 'DUMMY', false);
		let model;
		if (!useJoins) {
			model = _this.createBaseRuntimeModel(query.schema, query.collection, query.isView, query.ignoreCache);
		}
		query.simulate = true;
		let response = useJoins ? dummyModel.find(query) : model.find(query);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		const ormNames = /@[A-Za-z\s0-9]+_/ig;
		let tableDefinition = response.results[0].replace(ormNames, '');
		let tableQuery = 'CREATE COLUMN TABLE "' + schema + '"."' + name + '" AS ( ' + tableDefinition + ')';
		response = connector.execute(tableQuery, null, true);
		if (!lodash.isEmpty(response.errors)) {
			retVal.errors = response.errors;
			throw retVal;
		}
		return retVal;
	} catch (e) {
		retVal.results.created = false;
		let error = resources.parseError(e);
		if (!lodash.isEqual(retVal, error)) {
			retVal.errors.push(error);
		}
		return retVal;
	}
};