this.__runSelect__ = function(object){
    let resultSet;
    let connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
    if(object.values && Array.isArray(object.values) && object.query){ //select with bind paramiters
        /*em algumas veces é necessario continuar utilizando o mesmo object values, a copia é para não alterar o objeto recibido original*/
        let arrayPlusQuery = $.lodash.cloneDeep(object);
        arrayPlusQuery.values.unshift(object.query);
        /* -------------------------------------------------------------------------------------------------------------------------------*/
        resultSet = connection.executeQuery.apply(connection, arrayPlusQuery.values);
		connection.commit();
		connection.close();
    }else if(object.query && typeof object.query === 'string'){ // pure select
		resultSet = connection.executeQuery(object.query);
		connection.commit();
		connection.close();
    }
    /***ponto de atenção...só utilizar tipo de retorno Array no ORM 3 quando eu não vou mais processar dados*/
    if(object.returnType && object.returnType === 'Array'){
        let arrayResponse = [];
        let iterator = resultSet.getIterator();
        while (iterator.next()) {
            let instance = iterator.value();
            let line = [];
            for (var j = 0; j < instance.length; j++) {
                let val = instance[j];
                line.push(val);
            }
            arrayResponse.push(line);
        }
        resultSet = arrayResponse;
    }
    if(object.returnType && object.returnType === 'String'){
        let responseString = '';
        let iterator2 = resultSet.getIterator();
        while (iterator2.next()) {
            let instance2 = iterator2.value();
            for (var k = 0; k < instance2.length; k++) {
                let val2 = instance2[k];
                responseString += val2;
            }
        }
        return responseString;
    }
    return resultSet;
};

this.__runUpdate__ = function(object){
    let resultSet;
    let connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
    if(object.values && Array.isArray(object.values) && object.query){ 
        object.values.unshift(object.query);
        resultSet = connection.executeUpdate.apply(connection, object.values);
		connection.commit();
		connection.close();
    }else if(object.query && typeof object.query === 'string'){
		resultSet = connection.executeUpdate(object.query);
		connection.commit();
		connection.close();
    }
    if(object.returnType && object.returnType === 'Array'){
        let arrayResponse = [];
        let iterator = resultSet.getIterator();
        while (iterator.next()) {
            let instance = iterator.value();
            let line = [];
            for (var j = 0; j < instance.length; j++) {
                let val = instance[j];
                line.push(val);
            }
            arrayResponse.push(line);
        }
        resultSet = arrayResponse;
    }
    return resultSet;
};

this.__runUpdateMultiple__ = function(object){
    let resultSet;
    let connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
    // if(object.values && Array.isArray(object.values) && object.query){ 
    //     for(var i = 0; i < object.values.length; i++) {
            var values = [object.values];
            values.unshift(object.query);
            resultSet = connection.executeUpdate.apply(connection, values);
    		connection.commit();    
        // }
    	connection.close();
//     }else if(object.query && typeof object.query === 'string'){
// 		resultSet = connection.executeUpdate(object.query);
// 		connection.commit();
// 		connection.close();
//     }
    if(object.returnType && object.returnType === 'Array'){
        let arrayResponse = [];
        let iterator = resultSet.getIterator();
        while (iterator.next()) {
            let instance = iterator.value();
            let line = [];
            for (var j = 0; j < instance.length; j++) {
                let val = instance[j];
                line.push(val);
            }
            arrayResponse.push(line);
        }
        resultSet = arrayResponse;
    }
    return resultSet;
};

this.__createCachetable__ = function(query,table,schema,values){
    let resultSet;
    let connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
    if(query && typeof query === 'string'){ // pure select
        let execute = 'CREATE COLUMN TABLE "' + schema + '"."' + table + '" AS (' + query + ')';
        if(values && Array.isArray(values) && values.length > 0){
            values.unshift(execute);
            resultSet = connection.executeUpdate.apply(connection,values);
            connection.commit();
            connection.close();
            return resultSet;
        }
		resultSet = connection.executeUpdate(execute);
		connection.commit();
		connection.close();
    }
    return resultSet;
};

// this.__runExportSelect__ = function(object){
//     let resultSet = '';
//     let connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
//     if(object.values && Array.isArray(object.values) && object.query){ //select with bind paramiters
//         object.values.unshift(object.query);
//         resultSet = connection.executeQuery.apply(connection, object.values);
// 		connection.commit();
// 		connection.close();
//     }else if(object.query && typeof object.query === 'string'){ // pure select
// 		resultSet = connection.executeQuery(object.query);
// 		connection.commit();
// 		connection.close();
//     }
//     if(object.returnType && object.returnType === 'String'){
//         //let arrayResponse = [];
//         let iterator = resultSet.getIterator();
//         while (iterator.next()) {
//             let instance = iterator.value();
//             for (var j = 0; j < instance.length; j++) {
//                 let val = instance[j];
//                 resultSet += val;
//             }
//         }
//     }
//     return resultSet;
// };


//grabar tabela CORE::LOG_TRACER
this.executeTraceBindParamiters = function(options){
    try{
        const connection = $.hdb.getConnection({'sqlcc': 'timp.core.server.orm::SQLConnection', 'pool': true});
        connection.executeQuery(options.query, options.values[0], options.values[1], options.values[2], options.values[3], options.values[4], options.values[5], options.values[6], options.values[7], options.values[8], options.values[9], options.values[10],options.values[11]);
        connection.commit();
        connection.close();
    } catch(e){
        const a = e;
    }
};