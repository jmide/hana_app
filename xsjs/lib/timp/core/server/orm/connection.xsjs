var result = '';

var start, end, conn, pstmt, total, parcial;

result += 'Teste da interface $.hdb\r\n';
result += '========================\r\n';
result += Date() + '\r\n\r\n';

//result += 'Query: ' + sql + '\r\n\r\n';

result += 'Executanto com .hdb\r\n';
result += '-------------------\r\n';

var pool = [false, true];
var test_count = 30;

var i, j;

for (i = 0; i < pool.length; i++) {
	for (j = 0; j < test_count; j++) {

		total = 0;

		start = new Date();
		conn = $.hdb.getConnection({
			"sqlcc": "timp.core.server.orm::test",
			"pool": pool[i]
		});
		end = new Date();

		parcial = end - start;
		total += parcial;

		result += 'Tempo $.hdb.getConnection("pool":' + pool[i] + '): ' + parcial + ' ms\r\n';

		start = new Date();
		pstmt = conn.executeQuery(
			"SELECT STATEMENT_HASH, STATEMENT_STRING, AVG_EXECUTION_TIME, EXECUTION_COUNT, ACCESSED_TABLE_NAMES FROM \"M_SQL_PLAN_CACHE\" LIMIT 500"
		);
		end = new Date();

		parcial = end - start;
		total += parcial;

		result += 'Tempo conn.executeQuery(sql): ' + parcial + ' ms\r\n';

		start = new Date();
		pstmt = conn.executeQuery(
			"SELECT STATEMENT_HASH, STATEMENT_STRING, AVG_EXECUTION_TIME, EXECUTION_COUNT, ACCESSED_TABLE_NAMES FROM \"M_SQL_PLAN_CACHE\" LIMIT 500"
		);
		end = new Date();

		parcial = end - start;
		total += parcial;

		result += 'Tempo conn.executeQuery(sql): ' + parcial + ' ms\r\n';

		result += 'Tempo total: ' + total + ' ms\r\n\r\n';
		conn.close();
	}
}

$.response.setBody(result);
$.response.contentType = 'text/plain';
$.response.status = $.net.http.OK;