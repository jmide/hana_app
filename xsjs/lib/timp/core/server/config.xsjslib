$.import('timp.core.server.models', 'schema');
const schemaModel = $.timp.core.server.models.schema;
$.schema = schemaModel.default;
$.version = "2.0.0";
this.application = {
    schema: $.schema,
    version: $.version,
    maxModelsPerInstallation: 60
};

$.viewSchema = '_SYS_BIC';

$.getSchema = function () {
    return $.schema.slice(1,-1);
}