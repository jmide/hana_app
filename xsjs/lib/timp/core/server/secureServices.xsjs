this.getSecureDestination = function(destinationFolder, destinationPath) {
	var destination = $.net.http.readDestination(destinationFolder, destinationPath);
	return destination;
};
this._getRequestMethod = function(method) {
	const requestMethod = {
		'POST': $.net.http.POST,
		'GET': $.net.http.GET,
		'PUT': $.net.http.PUT,
		'PATCH': $.net.http.PATCH,
		'DEL': $.net.http.DEL,
		'OPTIONS': $.net.http.OPTIONS,
		'TRACE': $.net.http.TRACE,
		'CONNECT': $.net.http.CONNECT
	};
	if (requestMethod[method]) {
		return requestMethod[method];
	} else {
		return $.net.http.GET;
	}
};

this._createWebRequest = function(requestMethod, requestParameters, url) {
	var request = new $.web.WebRequest(requestMethod, url);
	request.contentType = requestParameters.contentType;
	if (Object.hasOwnProperty(requestParameters, 'headers')) {
		Object.keys(requestParameters.headers).forEach(function(header) {
			if ($.lodash.isEqual(header, 'Cookie')) {
				Object.keys(request.Parameters.headers.Cookie).forEach(function(cookie) {
					request.cookies.set(cookie, request.Parameters.headers.Cookie[cookie]);
				});
			} else {
				request.headers.set(header, requestParameters.headers[header]);
			}
		});
	}
	request.setBody(requestParameters.body);
	return request;
};

try {
    
    var object = JSON.parse($.request.parameters[0].value);
	
	var parameters = object.params;
    
    var destinationPath = object.adapter.destinationPath;
    var destinationFolder = 'timp.core.server.destinies';
    var url = '';//object.adapter.destination;
    
	var destination = this.getSecureDestination(destinationFolder, destinationPath);
	var client = new $.net.http.Client();
	
	var requestMethod = this._getRequestMethod("POST");
	
    let request = this._createWebRequest(requestMethod, parameters, url);
    
    //Response
	var response = client.request(request, destination).getResponse();
	var myCookies = [],
		myHeader = [],
		myBody;
	//Cookies
	for (var c in response.cookies) {
		myCookies.push(response.cookies[c]);
	}
	//Headers
	for (var h in response.headers) {
		myHeader.push(response.headers[h]);
	}
	//Body
	if (response.body) {
		try {
			myBody = JSON.parse(response.body.asString());
		} catch (e) {
			myBody = response.body.asString();
		}
	}
	$.response.contentType = 'text/xml';
	$.response.setBody(JSON.stringify({
		"status": response.status,
		"cookies": myCookies,
		"headers": myHeader,
		"body": myBody
	}));
} catch (e) {
	$.response.setBody(e.message);
	$.response.status = $.net.http.INTERNAL_SERVER_ERROR;
}

