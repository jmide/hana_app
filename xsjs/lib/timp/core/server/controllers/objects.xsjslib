
// $.import('timp.core.server.api','api');
// var core_api = $.timp.core.server.api.api;
$.import('timp.core.server', 'util');
$.import('timp.core.server.orm', 'sql');
$.import('timp.core.server.models', 'users');
$.import('timp.core.server.models', 'objects');

var util = $.timp.core.server.util;
var sql = $.timp.core.server.orm.sql;
var objects = $.timp.core.server.models.objects.objects;
var folders = $.timp.core.server.models.objects.folders;
var objectsUserDataModel = $.timp.core.server.models.objects.objectsUserData;
var objectsShareModel = $.timp.core.server.models.objects.objectShare;

// $.import('timp.core.server.api','api');
// var api = $.timp.core.server.models.objects;
var usersModel = $.timp.core.server.models.users.users;

$.import('timp.core.server.controllers', 'users');
var user = $.timp.core.server.controllers.users;

this.folder = {
    __getParam__: function() {
        var folder = util.getParam('object');

        if (!folder) return {};

        while (util.isString(folder)) folder = JSON.parse(folder);

        return folder;
    },
    /*
        name: string
        idParent: null //For root folder, or an id for another folder
    */
    'create': function(options) {
        // var folder = this.__getParam__();
        if (!options.hasOwnProperty('name')) {
            $.messageCodes.push({
                'code': 'CORE009000',
                'type': 'E',
                'folder': options
            });
            return null;
        } else {
            var response = folders.CREATE(options);
            //$.response.setBody(JSON.stringify(response));
            return response;
        }
    },
    /*
        id: 12, //ID is required
        name: 'Reports'
        id_parent: null //For root folder, or an id for another folder
    */
    'update': function(options) {
        var folder = this.__getParam__();

        if (!folder.hasOwnProperty('id')) {
            $.messageCodes.push({
                'code': 'CORE009001',
                'type': 'E',
                'folder': folder
            });
            return false;
        } else {

            if ( !! options && !! options.status) {
                folder.status = options.status;
            }

            var response = folders.UPDATE(folder);
            return response;
            //$.response.setBody(JSON.stringify(response));
        }

    },
    /*
        id: null //For root folder, or an id for another folder
        idParent: null //For root folder, or an id for another folder
        disablePrivileges: boolean
        type: // type of folder as in folderTable.type, null means all types
        tree: boolean // true lists the tree structure for the folder
        trash:
            'include' // includes the trashed folders
            'replace' // lists just the trashed folders
    */
    'list': function(options) {
        options = options || {};
        // var folder = this.__getParam__();
        var mkt, groupModel, userGroupModel;
        try{
            $.import('timp.mkt.server.api', 'api');
            mkt = $.timp.mkt.server.api.api.mkt_api;
            groupModel = mkt.tables.groups;
            userGroupModel = mkt.tables.users_groups;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009034',
                'type': 'E',
                'object': e
            });
            return null;
        }
        var allFolders = [];
        var timpUser = user.getTimpUser();
        var where = [];
        // filter by status
        if(options.hasOwnProperty('trash') && options.trash === 'replace'){
            where.push({
                field: 'status',
                oper: '=',
                value: 'Trash'
            });
        } else if(!options.hasOwnProperty('trash') || (options.hasOwnProperty('trash') && options.trash !== 'include')){
            where.push({
                field: 'status',
                oper: '=',
                value: 'Active'
            });
        }
        // filter by type
        if(options.hasOwnProperty('type')){
            where.push({
                field: 'type',
                oper: '=',
                value: options.type
            });
        }
        // list only children folders
        if(!options.hasOwnProperty('tree') || (options.hasOwnProperty('tree') && !options.trash)){
            if(options.hasOwnProperty('id') && options.id){
                where.push({
                    field: 'idParent',
                    oper: '=',
                    value: options.id
                });
            } else if(options.hasOwnProperty('idParent') && options.idParent){
                where.push({
                    field: 'idParent',
                    oper: '=',
                    value: options.idParent
                });
            } else {
                where.push({
                    field: 'idParent',
                    oper: 'IS NULL',
                    literal: true,
                    value: ''
                });
            }
        }
        // join with users to get Creation/Modification user
        var join = [{
            table: usersModel,
            alias: 'usersModel',
            fields: ['name', 'last_name', 'hana_user', 'cargo'],
            on: [{
                left_table: folders,
                left: 'creationUser',
                right: 'id'
            }]
        }];
        // if disabledPrivileges: true, don't join with MKT nor objectShare
        try {
             // read all the folders
            allFolders = folders.READ({
                distinct: true,
                join: [, {
                    table: usersModel,
                    alias: 'usersModel',
                    rename: 'ModificationUsersModel',
                    fields: ['name', 'last_name', 'hana_user', 'cargo'],
                    on: [{left_table: folders, left: 'modificationUser', right: 'id'}]
                }, {
                    table: user_groupModel,
                    alias: 'user_groupModel',
                    fields: [],
                    on: [{field: 'id_user', oper: '=', value: timpUser.id}]
                }, {
                    table: groupModel,
                    alias: 'groupModel',
                    fields: [],
                    on: [{left_table: user_groupModel, left: 'id_group', right: 'id'}]
                }, {
                    table: objectsShareModel,
                    alias: 'objectsShareModel',
                    fields: [],
                    on: [[{field: 'idUser', oper: '=', value: timpUser.id}, {left_table: user_groupModel, left: 'id_group', right: 'idGroup'}]],
                    outer: 'left'
                }],
                where: where,
                order_by: ['+idParent']
            });
        //     //join tables to show name of creator and modifier
        //     if (!folder.hasOwnProperty('id') && !folder.hasOwnProperty('id_parent')) {
        //         response = folders.READ({
        //             where: [{
        //                 field: 'status',
        //                 oper: '=',
        //                 value: 'Active'
        //             }],
        //             join: [{
        //                 table: usersModel,
        //                 alias: 'creationUserName',
        //                 rename: 'a',
        //                 on: [{
        //                     left: 'creationUser',
        //                     right: 'id'
        //                 }],
        //                 outer: 'left'
        //             }, {
        //                 table: usersModel,
        //                 alias: 'modificationUserName',
        //                 rename: 'b',
        //                 on: [{
        //                     left: 'modificationUser',
        //                     right: 'id'
        //                 }],
        //                 outer: 'left'
        //             }]
        //         });
        //     } else if (folder.hasOwnProperty('id')) {
        //         response = folders.READ({
        //             where: [{
        //                 field: 'id',
        //                 oper: '=',
        //                 value: folder.id
        //             }, {
        //                 field: 'status',
        //                 oper: '=',
        //                 value: 'Active'
        //             }],
        //             join: [{
        //                 table: usersModel,
        //                 alias: 'creationUserName',
        //                 rename: 'a',
        //                 on: [{
        //                     left: 'creationUser',
        //                     right: 'id'
        //                 }],
        //                 outer: 'left'
        //             }, {
        //                 table: usersModel,
        //                 alias: 'modificationUserName',
        //                 rename: 'b',
        //                 on: [{
        //                     left: 'modificationUser',
        //                     right: 'id'
        //                 }],
        //                 outer: 'left'
        //             }]
        //         });
        //     } else if (folder.hasOwnProperty('id_parent')) {
        //         if (folder.id_parent) {
        //             response = folders.READ({
        //                 where: [{
        //                     field: 'id_parent',
        //                     oper: '=',
        //                     value: folder.id_parent
        //                 }, {
        //                     field: 'status',
        //                     oper: '=',
        //                     value: 'Active'
        //                 }],
        //                 join: [{
        //                     table: usersModel,
        //                     alias: 'creationUserName',
        //                     rename: 'a',
        //                     on: [{
        //                         left: 'creationUser',
        //                         right: 'id'
        //                     }],
        //                     outer: 'left'
        //                 }, {
        //                     table: usersModel,
        //                     alias: 'modificationUserName',
        //                     rename: 'b',
        //                     on: [{
        //                         left: 'modificationUser',
        //                         right: 'id'
        //                     }],
        //                     outer: 'left'
        //                 }]
        //             });
        //         } else {
        //             response = folders.READ({
        //                 where: [{
        //                     field: 'id_parent',
        //                     oper: 'IS NULL',
        //                     literal: true,
        //                     value: ''
        //                 }, {
        //                     field: 'status',
        //                     oper: '=',
        //                     value: 'Active'
        //                 }],
        //                 join: [{
        //                     table: usersModel,
        //                     alias: 'creationUserName',
        //                     rename: 'a',
        //                     on: [{
        //                         left: 'creationUser',
        //                         right: 'id'
        //                     }],
        //                     outer: 'left'
        //                 }, {
        //                     table: usersModel,
        //                     alias: 'modificationUserName',
        //                     rename: 'b',
        //                     on: [{
        //                         left: 'modificationUser',
        //                         right: 'id'
        //                     }],
        //                     outer: 'left'
        //                 }]
        //             });
        //         }
        //     }
        //     var creationUserName = '';
        //     var modificationUserName = '';
        //     //change the content of the response.creationUsers tags with the names of creator and modifier user
        //     for (var i = 0; i < response.length; i++) {
        //         creationUserName = (response[i].creationUserName[0].name) + ' ' + (response[i].creationUserName[0].last_name);
        //         modificationUserName = (response[i].modificationUserName[0].name) + ' ' + (response[i].modificationUserName[0].last_name);
        //         response[i].creationUserName = creationUserName;
        //         response[i].modificationUserName = modificationUserName;
        //     }
        //     return response;
        //   //$.response.setBody(JSON.stringify(response));
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009034',
                'type': 'E',
                'object': e
            });
            return null;
        }
    },
    /*
        id: null //For root folder, or an id for another folder
        idParent: null //For root folder, or an id for another folder
        disablePrivileges: boolean
        type: // type of folder as in folderTable.type
        trash:
            'include' // includes the trashed folders
            'replace' // lists just the trashed folders
    */
    'listTree': function(options) {
        // var folder = this.__getParam__();
        // mkt tables
        try{
            $.import('timp.mkt.server.api', 'api');
            var mkt = $.timp.mkt.server.api.api.mkt_api;
            var groupModel = mkt.tables.groups;
            var user_groupModel = mkt.tables.users_groups;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009034',
                'type': 'E',
                'object': e
            });
            return null;
        }
        // var listFolders = [];
        var allFolders = [];
        // var flagFilter = null;
        var timpUser = user.getTimpUser();
        // filter by status: Active folders
        var where = [{
            field: 'status',
            oper: '=',
            value: 'Active'
        }];
        // filter by type
        if(options && options.hasOwnProperty('type')){
            where.push({
                field: 'type',
                oper: '=',
                value: options.type
            });
        }
        try{
            // read all the folders
            allFolders = folders.READ({
                distinct: true,
                join: [{
                    table: usersModel,
                    alias: 'usersModel',
                    fields: ['name', 'last_name', 'hana_user', 'cargo'],
                    on: [{left_table: folders, left: 'creationUser', right: 'id'}]
                }, {
                    table: usersModel,
                    alias: 'usersModel',
                    rename: 'ModificationUsersModel',
                    fields: ['name', 'last_name', 'hana_user', 'cargo'],
                    on: [{left_table: folders, left: 'modificationUser', right: 'id'}]
                }, {
                    table: user_groupModel,
                    alias: 'user_groupModel',
                    fields: [],
                    on: [{field: 'id_user', oper: '=', value: timpUser.id}]
                }, {
                    table: groupModel,
                    alias: 'groupModel',
                    fields: [],
                    on: [{left_table: user_groupModel, left: 'id_group', right: 'id'}]
                }, {
                    table: objectsShareModel,
                    alias: 'objectsShareModel',
                    fields: [],
                    on: [[{field: 'idUser', oper: '=', value: timpUser.id}, {left_table: user_groupModel, left: 'id_group', right: 'idGroup'}]],
                    outer: 'left'
                }],
                where: where,
                order_by: ['+idParent']
            });
            // add all the folders to a json object, id as key
            if(options && options.hasOwnProperty('id')){
                
            }
            var response = {
                allFolders: allFolders,
                id: null,
                folders: []
            };
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009034',
                'type': 'E',
                'object': object
            });
            return null;
        }
        // if (!options.hasOwnProperty('id')) {
        //     listFolders = folders.READ({
        //         where: [{
        //             field: 'status',
        //             oper: '=',
        //             value: 'Active'
        //         }]
        //     });
        //     for (var i = 0; i < listFolders.length; i++) {
        //         if (listFolders[i].id_parent === null) {
        //             flagFilter = listFolders[i].id;
        //         }
        //     }
        // } else {
        //     allFolders = folders.READ({
        //         where: [],
        //         order_by: ['+id_parent']
        //     });
        //     //the first for get the Folder filter and second level
        //     for (var i = 0; i < allFolders.length; i++) {
        //         if (allFolders[i].id === folder.id) {
        //             listFolders.push(allFolders[i]);
        //             flagFilter = allFolders[i].id;
        //             allFolders.splice(i, 1);
        //         } else if (allFolders[i].id_parent === folder.id) {
        //             listFolders.push(allFolders[i]);
        //             allFolders.splice(i, 1);
        //         }
        //     }

        //     for (var i = 0; i < listFolders.length; i++) {
        //         for (var k = 0; k < allFolders.length; k++) {
        //             if (allFolders[k].id_parent === listFolders[i].id) {
        //                 listFolders.push(allFolders[k]);
        //             }
        //         }
        //     }
        // }
        // var tree = {};
        // var response = {
        //     allFolders: allFolders,
        //     id: null,
        //     folders: []
        // };
        // for (var i = 0; i < listFolders.length; i++) {
        //     if (listFolders[i].id === flagFilter || listFolders[i].id_parent === null) {
        //         response.folders.push(processChildren(listFolders[i], listFolders));
        //     }
        // }

        // function processChildren(currentLevel, allFolders) {
        //     var returnLevel = {
        //         id: currentLevel.id,
        //         name: currentLevel.name,
        //         folders: []
        //     };

        //     for (var i = 0; i < allFolders.length; i++) {
        //         if (allFolders[i].id_parent == returnLevel.id) {
        //             returnLevel.folders.push(processChildren(allFolders[i], allFolders));
        //         }
        //     }
        //     return returnLevel;
        // }
        // return response;
        //$.response.setBody(JSON.stringify(response));
    },
    'listTrash': function(object) {
        try {
            var folder = object;//this.__getParam__();
            var response = [];
            response = folders.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            });
            //$.response.setBody(JSON.stringify(response));
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009033',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    'countTrash': function() {
        /*var folder = this.__getParam__();
        var response = [];
            response = folders.READ({
            where: [{field: 'status', oper: '=', value: 2}]
            });*/
        try {
            var innerQuery = folders.READ({
                simulate: true,
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            })
            var queryObject = {};
            queryObject.query = 'SELECT COUNT(*) FROM ( ' + innerQuery + ' )';
            queryObject.values = [];
            var listCount = sql.__runSelect__(queryObject)[0][0];

            return listCount;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE0090032',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    'trash': function(object) {
        var folder = object;//this.__getParam__();
        var verify = [];
        var response = [];
        //var object = $.request.parameters.get('object');
        //object = __parse__(object);
        if (!folder.hasOwnProperty('ids')) {
            $.messageCodes.push({
                'code': 'CORE009002',
                'type': 'E',
                'folder': folder
            });
        } else {
            var verifyFolder = folders.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Active'
                }]
            });
            var verifyObject = objects.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Active'
                }]
            });

            for (var i = 0; i < folder.ids.length; i++) {
                verify.length = 0;
                var idFolder = folder.ids[i];
                for (var k = 0; k < verifyFolder.length; k++) {
                    if (verifyFolder[k].id_parent === folder.ids[i]) {
                        $.messageCodes.push({
                            'code': 'CORE009022',
                            'type': 'E',
                            'folder': folder
                        });
                    }
                }
                for (var k = 0; k < verifyObject.length; k++) {
                    if (verifyObject[k].id_folder === folder.ids[i]) {
                        $.messageCodes.push({
                            'code': 'CORE009022',
                            'type': 'E',
                            'folder': folder
                        });
                    }
                }

                if (idFolder > 0 && verify.length === 0) {
                    var options = {
                        id: idFolder,
                        status: 'Trash'
                    };
                    try {
                        response.push(idFolder, folders.UPDATE(options));
                    } catch (e) {
                        //$.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        $.messageCodes.push({
                            'code': 'CORE009002',
                            'type': 'E',
                            'folder': folder
                        });
                        return false;
                    }
                } else {
                    response.push(idFolder, false);
                }
            }
            return response;
            //$.response.setBody(JSON.stringify(response));
        }
        return null;
    },
    'restore': function(object) {
        var folder = object;//this.__getParam__();

        if (!folder.hasOwnProperty('ids')) {
            $.messageCodes.push({
                'code': 'CORE009003',
                'type': 'E',
                'folder': folder
            });
        } else {
            var log = [];
            var filter = [];
            var verify = folders.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            });

            if (verify.length > 0) {
                for (var i = 0; i < folder.ids.length; i++) {
                    for (var k = 0; k < verify.length; k++) {
                        if (verify[k].id === folder.ids[i]) {
                            filter.push(verify[k]);
                            verify.splice(k, 1);
                            k--;
                        }
                    }
                }
            }
            for (var i = 0; i < verify.length; i++) {
                for (var k = 0; k < filter.length; k++) {
                    if (verify[i].id === filter[k].id_parent) {
                        log.push(false, verify[i]);
                        filter.splice(k, 1);
                        k--;
                    }
                }
            }
            for (var i = 0; i < filter.length; i++) {
                var idFolder = filter[i].id;
                if (idFolder > 0) {
                    var options = {
                        id: idFolder,
                        status: 'Active'
                    };
                    try {
                        var response = folders.UPDATE(options);
                        log.push(response, filter[i]);
                    } catch (e) {
                        // $.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        $.messageCodes.push({
                            'code': 'CORE009003',
                            'type': 'E',
                            'folder': folder
                        });
                    }
                }
            }
            return log;
            //$.response.setBody(JSON.stringify(log));
        }
    return null;
    },
    'delete': function(object) {
        var folder = object;//this.__getParam__();
        if (!folder.hasOwnProperty('ids')) {
            $.messageCodes.push({
                'code': 'CORE009004',
                'type': 'E',
                'folder': folder
            });
        } else {
            var log = [];
            var filter = [];
            var verify = folders.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            });

            if (verify.length > 0) {
                for (var i = 0; i < folder.ids.length; i++) {
                    for (var k = 0; k < verify.length; k++) {
                        if (verify[k].id === folder.ids[i]) {
                            filter.push(verify[k]);
                            verify.splice(k, 1);
                            k--;
                        }
                    }
                }
            }
            for (var i = 0; i < verify.length; i++) {
                for (var k = 0; k < filter.length; k++) {
                    if (verify[i].id_parent === filter[k].id) {
                        log.push(false, verify[i]);
                        filter.splice(k, 1);
                        k--;
                    }
                }
            }
            for (var i = 0; i < filter.length; i++) {
                var idFolder = filter[i].id;
                if (idFolder > 0) {
                    var options = {
                        id: idFolder,
                        status: 'Deleted'
                    };
                    try {
                        var response = folders.UPDATE(options);
                        log.push(response, filter[i]);
                    } catch (e) {
                        // $.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        $.messageCodes.push({
                            'code': 'CORE009004',
                            'type': 'E',
                            'folder': folder
                        });
                    }
                }
            }
            return log;
            //$.response.setBody(JSON.stringify(log));
        }
        return null;
    },
    'move': function(object) {
        var folder = object;//this.__getParam__();

        if (!folder.hasOwnProperty('ids') && !folder.hasOwnProperty('id_parent')) {
            $.messageCodes.push({
                'code': 'CORE009005',
                'type': 'E',
                'folder': folder
            });
        } else {

            // if (folder.hasOwnProperty('id_parent')) {
            var id_parent = folder.id_parent;
            for (var i = 0; i < folder.ids.length; i++) {
                var id = folder.ids[i];
                if (id > 0) {
                    var options = {
                        id: id,
                        id_parent: id_parent
                    };
                    try {
                        var response = folders.UPDATE(options);
                    } catch (e) {
                        //  $.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                    }
                }
            }
            return response;
            //$.response.setBody(JSON.stringify(response));
            // } 
        }
        return null;
    }
};

this.object = {
    __getParam__: function() {
        var object = util.getParam('object');

        if (!object) return {};

        while (util.isString(object)) object = JSON.parse(object);

        return object;
    },

    'create': function(options) {
        var object = this.__getParam__();
        try {
            // if(!options.hasOwnProperty('model')){
            //     return null;
            // }
            if (!object.hasOwnProperty('name')) {
                $.messageCodes.push({
                    'code': 'CORE009006',
                    'type': 'E',
                    'object': object
                });
                return null;
            } else {
                var response = objects.CREATE(object);
                // var response = options.model.CREATE(options.json);
                return response;
                //$.response.setBody(JSON.stringify(response));
            }
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009006',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    /*
        id: 12, //ID is required
        name: 'Reports',
        id_parent: null //For root folder, or an id for another folder
    */
    'update': function(options) {
        var object = this.__getParam__();
        try {
            if (!object.hasOwnProperty('id')) {
                $.messageCodes.push({
                    'code': 'CORE009007',
                    'type': 'E',
                    'object': object
                });return null;
            } else {
                if ( !! options && !! options.status) {
                    object.status = options.status;
                }

                var response = objects.UPDATE(object);
                return response;
                //$.response.setBody(JSON.stringify(response));
            }
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009007',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },

    'list': function(object) {
        // var object = this.__getParam__();
        try {
            var response = [];
            if (!object.hasOwnProperty('id_object') && !object.hasOwnProperty('id_folder') && !object.hasOwnProperty('id_objectType')) {
                response = objects.READ({
                    where: [{
                        field: 'status',
                        oper: '=',
                        value: 'Active'
                    }, {
                        field: 'creationUser',
                        oper: '=',
                        value: user.getTimpUser().id
                    }]
                });

            } else if (object.hasOwnProperty('id_folder')) {
                if (object.id_folder) {
                    response = objects.READ({
                        where: [{
                            field: 'id_folder',
                            oper: '=',
                            value: object.id_folder
                        }, {
                            field: 'status',
                            oper: '=',
                            value: 'Active'
                        }, {
                            field: 'creationUser',
                            oper: '=',
                            value: user.getTimpUser().id
                        }] //,
                        //   {field: 'id_user',oper:'=',value: user.getTimpUser().id,table:objectsUserDataModel}],
                        // join:[{
                        //     table: objectsUserDataModel,
                        //     alias: 'isFavorite',
                        //     on: [{left: 'id_object',right:'id_object'}],
                        //   // where:[{field: 'id_user',oper:'=',value: user.getTimpUser().id}],
                        //       outer:'left'  
                        //     }]
                    });
                } else {
                    response = objects.READ({
                        where: [{
                            field: 'id_folder',
                            oper: 'IS NULL',
                            literal: true,
                            value: ''
                        }, {
                            field: 'status',
                            oper: '=',
                            value: 'Active'
                        }, {
                            field: 'creationUser',
                            oper: '=',
                            value: user.getTimpUser().id
                        }]
                        //,
                        //         {field: 'id_user',oper:'=',value: user.getTimpUser().id,table:objectsUserDataModel}],
                        // join:[{
                        //     table: objectsUserDataModel,
                        //     alias: 'isFavorite',
                        //     on: [{left: 'id_object',right:'id_object'}],
                        //     //where:[{field: 'id_user',oper:'=',value: user.getTimpUser().id}],
                        //      outer:'left'  
                        //     }]

                    });
                    
                }
            } else if (object.hasOwnProperty('id_object')) {
                response = objects.READ({
                    where: [{
                        field: 'id_object',
                        oper: '=',
                        value: object.id_object
                    }, {
                        field: 'status',
                        oper: '=',
                        value: 'Active'
                    }, {
                        field: 'creationUser',
                        oper: '=',
                        value: user.getTimpUser().id
                    }] //,
                    //         {field: 'id_user',oper:'=',value: user.getTimpUser().id,table:objectsUserDataModel}],
                    // join:[{
                    //         table: objectsUserDataModel,
                    //         alias: 'isFavorite',
                    //         on: [{left: 'id_object',right:'id_object'}],
                    //       // where:[{field: 'id_user',oper:'=',value: user.getTimpUser().id}],
                    //         outer:'left'  
                    //       }]

                });
            } else if (object.hasOwnProperty('id_objectType')) {
                response = objects.READ({
                    where: [{
                        field: 'id_objectType',
                        oper: '=',
                        value: object.id_objectType
                    }, {
                        field: 'status',
                        oper: '=',
                        value: 'Active'
                    }, {
                        field: 'creationUser',
                        oper: '=',
                        value: user.getTimpUser().id
                    }] //,
                    //         {field: 'id_user',oper:'=',value: user.getTimpUser().id,table:objectsUserDataModel}],
                    // join:[{
                    //     table: objectsUserDataModel,
                    //     alias: 'isFavorite',
                    //     on: [{left: 'id_object',right:'id_object'}],
                    //   // where:[{field: 'id_user',oper:'=',value: user.getTimpUser().id}],
                    //     outer:'left'  
                    //     }]
                });
            }
            //   for(var i =0;i<response.length;i++){
            //       if(Object.keys(response[i].isFavorite).length!==0){
            //           response[i].isFavorite =response[i].isFavorite[0].is_favorite;
            //       }
            //       else{
            //           response[i].isFavorite =0;
            //       }
            //   }
            var response2 = objectsUserDataModel.READ({});
            //return response2;
            for (var i = 0; i < response.length; i++) {
                response[i].isFavorite = 0;
                for (var j = 0; j < response2.length; j++) {
                    if (response[i].id_object == response2[j].id_object && response2[j].is_favorite == true && response2[j].id_user == user.getTimpUser().id) {
                        response[i].isFavorite = 1;
                    }
                }
            }
            //$.response.setBody(JSON.stringify(response));
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009031',
                'type': 'E',
                'object': object,
                errorInfo: e
            });
            return null;
        }
    },
    'trash': function(object) {
        // var object = this.__getParam__();
        //var object = $.request.parameters.get('object');
        //object = __parse__(object);
        try {
            if (!object.hasOwnProperty('ids')) {
                $.messageCodes.push({
                    'code': 'CORE009008',
                    'type': 'E',
                    'object': object
                });return null;
                // throw 'Cannot trash these objects without id'
                //CORE009008
            } else {
                for (var i = 0; i < object.ids.length; i++) {
                    var id = object.ids[i];
                    if (id > 0) {
                        var options = {
                            id: id,
                            status: 'Trash'
                        };
                        try {
                            var response = objects.UPDATE(options);
                        } catch (e) {
                            // $.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        }
                    }
                }
                return response;
                //$.response.setBody(JSON.stringify(response));
            }
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009008',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    'restore': function(object) {
        // var object = this.__getParam__();
        try {
            if (!object.hasOwnProperty('ids')) {
                $.messageCodes.push({
                    'code': 'CORE009009',
                    'type': 'E',
                    'object': object
                });return null;
                // throw 'Cannot restore these objects without id'
                //CORE009009
            } else {

                for (var i = 0; i < object.ids.length; i++) {
                    var id = object.ids[i];
                    if (id > 0) {
                        var options = {
                            id: id,
                            status: 'Active'
                        };
                        try {
                            var response = objects.UPDATE(options);
                        } catch (e) {
                            $.messageCodes.push({
                                'code': 'CORE009009',
                                'type': 'E',
                                'object': object
                            });
                            return null;
                        }
                    }
                }
                return response;
                //$.response.setBody(JSON.stringify(response));
            }
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009009',
                'type': 'E',
                'object': object
            });
            return null;    
        }
    },

    'delete': function(object) {
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('ids')) {
            $.messageCodes.push({
                'code': 'CORE009010',
                'type': 'E',
                'object': object
            });return null;
        } else {

            for (var i = 0; i < object.ids.length; i++) {
                var id = object.ids[i];
                if (id > 0) {
                    var options = {
                        id: id,
                        status: 'Deleted'
                    };
                    try {
                        var response = objects.UPDATE(options);
                    } catch (e) {
                        // $.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        $.messageCodes.push({
                            'code': 'CORE009021',
                            'type': 'E',
                            'object': object
                        });
                        return null;
                    }
                }
            }
            return response;
            //$.response.setBody(JSON.stringify(response));
        }
    },
    'countTrash': function() {
        try {
            var innerQuery = objects.READ({
                simulate: true,
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            })
            var queryObject = {};
            queryObject.query = 'SELECT COUNT(*) FROM ( ' + innerQuery + ' )';
            queryObject.values = [];
            var listCount = sql.__runSelect__(queryObject)[0][0];

            return listCount;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009029',
                'type': 'E',
                'object': object
            });
            return null
        }
    },
    'listTrash': function(object) {
        // var object = this.__getParam__();
        try {
            var response = [];
            response = objects.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Trash'
                }]
            });
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009030',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    'move': function(object) {
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('ids') || !object.hasOwnProperty('id_folder')) {
            $.messageCodes.push({
                'code': 'CORE009011',
                'type': 'E',
                'object': object
            });return null;
        } else {
            var id_folder = object.id_folder;
            for (var i = 0; i < object.ids.length; i++) {
                var id = object.ids[i];
                if (id > 0) {
                    var options = {
                        id: id,
                        id_folder: id_folder
                    };
                    try {
                        var response = objects.UPDATE(options);
                    } catch (e) {
                        //$.response.setBody('Problems executing UPDATE:' + util.parseError(e));
                        $.messageCodes.push({
                            'code': 'CORE009020',
                            'type': 'E',
                            'object': object
                        });
                    }
                }
            }
            return response;
            //$.response.setBody(JSON.stringify(response));
        }
    },
    //object=[{'id_object':23,'is_favorite':false}] income json
    'setFavorite': function(object) { //ready
        // var object = this.__getParam__();
        for (var i = 0; i < Object.keys(object).length; i++) {
            if (!object[i].hasOwnProperty('id_object')) {
                $.messageCodes.push({
                    'code': 'CORE009018',
                    'type': 'E',
                    'object': object
                });
            }
            
        }
        
        var response = [];
        for (var i = 0; i < object.length; i++) {
            object[i].id_user = user.getTimpUser().id;
            var readFavorite = objectsUserDataModel.READ({where:  [{
                    field: 'id_user',
                    oper: '=',
                    value: object[i].id_user
                }, {
                    field: 'id_object',
                    oper: '=',
                    value: object[i].id_object
                }]});
            if(readFavorite.length > 0){
                response = objectsUserDataModel.UPDATEWHERE({
                    'is_favorite': object[i].is_favorite
                }, [{
                    field: 'id_user',
                    oper: '=',
                    value: object[i].id_user
                }, {
                    field: 'id_object',
                    oper: '=',
                    value: object[i].id_object
                }], true);
            }else{
                response = objectsUserDataModel.CREATE(object[i]);
            }
            
            // try {
                //response.push(objectsUserDataModel.table.CREATE(object[i]));
                // var upStat = objectsUserDataModel.UPDATEWHERE({
                //     'is_favorite': object[i].is_favorite,
                //     id_object: object[i].id_object,
                //     id_user: object[i].id_user
                // }, [{
                //     field: 'id_user',
                //     oper: '=',
                //     value: object[i].id_user
                // }, {
                //     field: 'id_object',
                //     oper: '=',
                //     value: object[i].id_object
                // }]);
                // response.push(upStat);
            // } catch (e) {
                //change the front end object from true to 1 and false to 0
                // var upStat = objectsUserDataModel.UPDATEWHERE({
                //     'is_favorite': object[i].is_favorite
                // }, [{
                //     field: 'id_user',
                //     oper: '=',
                //     value: object[i].id_user
                // }, {
                //     field: 'id_object',
                //     oper: '=',
                //     value: object[i].id_object
                // }], true);
                
                
            // }
        }
        
        //$.response.setBody(JSON.stringify(response));
        return response;

    },
    'listFavorite': function(object) { //check listFavorite, ready
        try {
            $.import('timp.mkt.server.api', 'api');
            var u = $.timp.mkt.server.api.api.mkt_api;
            var user_groupModel = u.tables.users_groups;
            
            // var object = this.__getParam__();
            var response = [];
            response = objects.READ({
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Active'
                }, {
                    field: 'id_user',
                    oper: '=',
                    value: user.getTimpUser().id,
                    table: objectsUserDataModel
                }, {
                    field: 'is_favorite',
                    oper: '=',
                    value: 1,
                    table: objectsUserDataModel
                }],
                join: [{
                    table: objectsUserDataModel,
                    alias: 'isFavorite',
                    on: [{
                        left: 'id_object',
                        right: 'id_object'
                    }]
                }, {
                    table: objectsShareModel,
                    alias: 'shared',
                    on: [{
                        left_table: objectsUserDataModel,
                        left: 'id_object',
                        right: 'id_object'
                    }],
                    outer: 'left'
                }, {
                    table: user_groupModel,
                    alias: 'group',
                    rename: 'c',
                    on: [{
                        left_table: objectsShareModel,
                        left: 'id_group',
                        right: 'id_group'
                    }],
                    outer: 'left'
                }]
            });

            var favorite_response = [];
            for (var i = 0; i < response.length; i++) {
                if (Object.keys(response[i].isFavorite).length !== 0) {
                    response[i].isFavorite = response[i].isFavorite[0].is_favorite;
                }
                if (response[i].isFavorite == 1) {
                    favorite_response.push(response[i]); //push into the array if it is a favorite object
                }
            }
            //$.response.setBody(JSON.stringify(favorite_response));
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009026',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    'listShared': function() {
        //  try{
        $.import('timp.mkt.server.api', 'api');
        var u = $.timp.mkt.server.api.api.mkt_api;
        var user_groupModel = u.tables.users_groups;
        
        var groupResponse = user_groupModel.READ({
            where: [{
                field: 'id_user',
                oper: '=',
                value: user.getTimpUser().id
            }]
        })
        var groups_array = [];
        var innerQueryGroups = [];
        var innerQueryUser = [];

        for (var i = 0; i < groupResponse.length; i++) {
            groups_array.push(groupResponse[0].id_group);
        }

        for (var i = 0; i < groups_array.length; i++) {
            innerQueryGroups = objectsShareModel.READ({
                where: [{
                    field: 'id_group',
                    oper: '=',
                    value: groups_array[i]
                }],
                join: [{
                    table: objects,
                    alias: 'shared',
                    on: [{
                        left: 'id_object',
                        right: 'id_object'
                    }]
                }]
            })
        }
        //   throw innerQueryGroups
        innerQueryUser = objectsShareModel.READ({
            where: [{
                field: 'id_user',
                oper: '=',
                value: user.getTimpUser().id
            }],
            join: [{
                table: objects,
                alias: 'shared',
                on: [{
                    left: 'id_object',
                    right: 'id_object'
                }]
            }, {
                table: objectsUserDataModel,
                alias: 'isFavorite',
                rename: 'a',
                on: [{
                    left_table: objects,
                    left: 'id_object',
                    right: 'id_object'
                }],
                outer: 'left'
            }]
        })
        var response2 = objectsUserDataModel.READ({});

        for (var i = 0; i < innerQueryUser.length; i++) {
            innerQueryUser[i].isFavorite = 0;
            if (innerQueryUser[i].shared[0].shared == 0) {
                innerQueryUser[i].shared = 'Private';
            } else if (innerQueryUser[i].shared[0].shared == 1) {
                innerQueryUser[i].shared = 'Shared';
            } else if (innerQueryUser[i].shared[0].shared == 2) {
                innerQueryUser[i].shared = 'Public';
            }
            for (var j = 0; j < response2.length; j++) {

                if (innerQueryUser[i].id_object == response2[j].id_object && response2[j].is_favorite == true && response2[j].id_user == user.getTimpUser().id) {
                    innerQueryUser[i].isFavorite = 1;
                }
            }
        }

        for (var i = 0; i < innerQueryGroups.length; i++) {
            innerQueryGroups[i].isFavorite = 0;
            if (innerQueryGroups[i].shared[0].shared == 0) {
                innerQueryGroups[i].shared = 'Private';
            } else if (innerQueryGroups[i].shared[0].shared == 1) {
                innerQueryGroups[i].shared = 'Shared';
            } else if (innerQueryGroups[i].shared[0].shared == 2) {
                innerQueryGroups[i].shared = 'Public';
            }
            for (var j = 0; j < response2.length; j++) {

                if (innerQueryGroups[i].id_object == response2[j].id_object && response2[j].is_favorite == true && response2[j].id_user == user.getTimpUser().id) {
                    innerQueryGroups[i].isFavorite = 1;
                }
            }
        }
        //if any id_object is repeated it ignores the object
        var response = innerQueryUser.concat(innerQueryGroups);
        var response_array = [];
        var final_response = [];


        for (var i = 0; i < Object.keys(response).length; i++) {
            if (response_array.indexOf(response[i].id_object) < 0) {
                response_array.push(response[i].id_object)
                final_response.push(response[i])
            }

        }
        //   throw final_response;
        //$.response.setBody(JSON.stringify(final_response));
        return final_response;
        

        //   }catch(e){
        //       $.messageCodes.push({'code':'CORE009025','type':'E','object':object}); 
        //   }
    },
    'listPublic': function() {
        var response = [];
        response = objects.READ({
            where: [{
                field: 'status',
                oper: '=',
                value: 'Active'
            }, {
                field: 'shared',
                oper: '=',
                value: 'Public'
            }],
        });

        var response2 = objectsUserDataModel.READ({});
        for (var i = 0; i < response.length; i++) {
            response[i].isFavorite = 0;
            for (var j = 0; j < response2.length; j++) {
                if (response[i].id_object == response2[j].id_object && response2[j].is_favorite == true && response2[j].id_user == user.getTimpUser().id) {
                    response[i].isFavorite = 1;
                }
            }
        }
        //$.response.setBody(JSON.stringify(response));
        return response;
    },
    'countFavorite': function() {
        try {
            $.import('timp.mkt.server.api', 'api');
            var u = $.timp.mkt.server.api.api.mkt_api;
            var user_groupModel = u.tables.users_groups;
            
            var innerQuery = objects.READ({
                distinct: true,
                fields: ['id_object'],
                simulate: true,
                where: [{
                    field: 'status',
                    oper: '=',
                    value: 'Active'
                }, {
                    field: 'id_user',
                    oper: '=',
                    value: user.getTimpUser().id,
                    table: objectsUserDataModel
                }, {
                    field: 'is_favorite',
                    oper: '=',
                    value: 1,
                    table: objectsUserDataModel
                }], //call the model inside the join, just one where level
                join: [{
                    table: objectsUserDataModel,
                    fields: ['id_object'],
                    alias: 'isFavorite',
                    on: [{
                        left: 'id_object',
                        right: 'id_object'
                    }]
                }, {
                    table: objectsShareModel,
                    fields: ['id_object'],
                    alias: 'shared',
                    on: [{
                        left_table: objectsUserDataModel,
                        left: 'id_object',
                        right: 'id_object'
                    }],
                    outer: 'left'
                }, {
                    table: user_groupModel,
                    fields: ['id_object'],
                    alias: 'group',
                    rename: 'c',
                    on: [{
                        left_table: objectsShareModel,
                        left: 'id_group',
                        right: 'id_group'
                    }],
                    outer: 'left'
                }]
            });

            var queryObject = {};
            queryObject.query = 'SELECT COUNT(*) FROM (' + innerQuery + ')';
            queryObject.values = [];
            var listCount = sql.__runSelect__(queryObject)[0][0];

            return listCount;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009024',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },
    //(1)income JSON from ui = {'id_object':273,'shared':'Private','id_users':[10,2,11,9]}
    //(2)income JSON from ui= {'id_object':273,'shared':'Private','id_groups':[10,2,11,9]}
    'shareObject': function(object) {
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('id_object') && !object.hasOwnPropert('shared')) {
            $.messageCodes.push({
                'code': 'CORE009016',
                'type': 'E',
                'object': object
            });return null;
        } else {
            try {
                var creationUserResponse = objects.READ({
                    where: [{
                        field: 'id_object',
                        oper: '=',
                        value: object.id_object
                    }]
                });
                //se agrego var global params, 
                var params;
                if (creationUserResponse[0].creationUser === user.getTimpUser().id) {
                    var newObject = {};
                    newObject.id_object = object.id_object;
                    newObject.shared = object.shared;
                    //var response = objects.UPDATE(newObject);
                    var response = objects.UPDATEWHERE({
                        'shared': newObject.shared
                    }, [{
                        field: 'id_object',
                        oper: '=',
                        value: newObject.id_object
                    }]);
                    if (object.hasOwnProperty('id_users')) {
                        var id_users = object.id_users;
                        for (var i = 0; i < Object.keys(object.id_users).length; i++) {
                            params = {};
                            params.id_object = object.id_object;
                            params.id_user = object.id_users[i];
                            try {
                                response = objectsShareModel.CREATE(params);
                            } catch (e) {}
                        }
                    }
                    if (object.hasOwnProperty('id_groups')) {
                        var id_groups = object.id_groups;
                        for (var i = 0; i < Object.keys(object.id_groups).length; i++) {
                            params = {};
                            params.id_object = object.id_object;
                            params.id_group = object.id_groups[i];
                            try {
                                response = objectsShareModel.CREATE(params);
                            } catch (e) {}
                        }
                    }
                    //$.response.setBody(JSON.stringify(response));
                    return response;
                } else {
                    $.messageCodes.push({
                        'code': 'CORE009019',
                        'type': 'E',
                        'object': object
                    });
                }
            } catch (e) {
                $.messageCodes.push({
                    'code': 'CORE009019',
                    'type': 'E',
                    'object': object
                });
            }
        }
    return null;},
    'countShared': function() {
        // try{
        // var response = user_groupModel.READ({
        //               where:[{field:'id_user',oper:'=',value:user.getTimpUser().id}]
        // });
        // var count=0;
        // var group_array =[];
        // var listCount=0;
        // for(var i=0;i<response.length;i++){
        //     group_array.push(response[0].id_group);
        // }
        // for(var i=0;i<group_array.length;i++){
        // var innerQuery2 = objectsShareModel.READ({
        //     simulate:true,
        //     where:[{field:'id_group',oper:'=',value:group_array[i]}],
        // });
        //     var queryObject={};
        //     queryObject.query = 'SELECT COUNT(*) FROM ('+innerQuery2+')'; 
        //     queryObject.values=[];
        //     listCount = sql.__runSelect__(queryObject)[0][0];
        // }
        // var innerQuery = objectsShareModel.READ({
        //     simulate:true,
        //     where:[{field:'id_user',oper:'=',value:user.getTimpUser().id}],
        // });
        // var queryObject={};
        // queryObject.query = 'SELECT COUNT(*) FROM ('+innerQuery+')'; 
        // queryObject.values=[];
        // var listCount2 = sql.__runSelect__(queryObject)[0][0];

        // count =parseInt(listCount)+ parseInt(listCount2);

        // $.response.setBody(JSON.stringify(count));
        // }catch(e){
        //     $.messageCodes.push({'code':'CORE009023','type':'E','object':object}); 
        // }
        try {
            $.import('timp.mkt.server.api', 'api');
            var u = $.timp.mkt.server.api.api.mkt_api;
            var user_groupModel = u.tables.users_groups;
            
            var groupResponse = user_groupModel.READ({
                where: [{
                    field: 'id_user',
                    oper: '=',
                    value: user.getTimpUser().id
                }]
            })
            var groups_array = [];
            var innerQueryGroups = [];
            var innerQueryUser = [];

            for (var i = 0; i < groupResponse.length; i++) {
                groups_array.push(groupResponse[0].id_group);
            }

            for (var i = 0; i < groups_array.length; i++) {
                innerQueryGroups = objectsShareModel.READ({
                    where: [{
                        field: 'id_group',
                        oper: '=',
                        value: groups_array[i]
                    }]
                })
            }

            innerQueryUser = objectsShareModel.READ({
                where: [{
                    field: 'id_user',
                    oper: '=',
                    value: user.getTimpUser().id
                }]
            })
            
            var response = innerQueryUser.concat(innerQueryGroups);
            var response_array = [];

            for (var i = 0; i < Object.keys(response).length; i++) {
                if (response_array.indexOf(response[i].id_object) < 0) {
                    response_array.push(response[i].id_object)
                }
            }
            // var whereObject = [];
            // for(var i =0; i<response_array.length; i++){
            //     whereObject.push({field: 'id_object', oper: '=', value: response_array[i]});
            // }
            // var count = objects.READ({where: [whereObject]});
            
            var count = response_array.length;
            return count;

        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009023',
                'type': 'E',
                'object': object
            });
            return null;
        }
    },

    'countPublic': function() {
        var innerQuery = objects.READ({
            simulate: true,
            where: [{
                field: 'shared',
                oper: '=',
                value: 'Public'
            },
            {
                field: 'status',
                oper: '=',
                value: 'Active'
            }]
        })

        var queryObject = {};
        queryObject.query = 'SELECT COUNT(*) FROM ( ' + innerQuery + ' )';
        queryObject.values = [];
        var listCount = sql.__runSelect__(queryObject)[0][0];

        return listCount;
    }
    //temp function, deleting trash  from tables.
    // 'temp_delete':function(){
    //     //var object = this.__getParam__();
    //     objectsUserDataModel.DELETEWHERE([{field:'id_user',oper:'=',value:8206},{field:'id_object',oper:'=',value:267}])
    //}
};

this.objectUserData = {
    __getParam__: function() {
        var object = util.getParam('object');
        if (!object) return {};
        while (util.isString(object)) object = JSON.parse(object);
        return object;
    },
    'create': function(object) {
        // var object = this.__getParam__();

        if (!object.hasOwnProperty('id_object')) {
            $.messageCodes.push({
                'code': 'CORE009012',
                'type': 'E',
                'object': object
            });return null;
        } else {
            object.id_user = user.getTimpUser().id;
            var response = objectsUserDataModel.CREATE(object);

            //$.response.setBody(JSON.stringify(response));
            return response;
        }
    },
    'update': function(object) {
        // var object = this.__getParam__();

        if (!object.hasOwnProperty('id_object')) {
            $.messageCodes.push({
                'code': 'CORE009013',
                'type': 'E',
                'object': object
            });return null;
        } else {
            object.id_user = user.getTimpUser().id;
            var response = objectsUserDataModel.UPDATE(object);

            //$.response.setBody(JSON.stringify(response));
            return response;
        }
    },
    'list': function(object) {
        // var object = this.__getParam__();
        var response = [];
        try {
            response = objectsUserDataModel.READ({
                where: [{
                    field: 'id_user',
                    oper: '=',
                    value: user.getTimpUser().id
                }, {
                    field: 'is_favorite',
                    oper: '=',
                    value: '1'
                }]
            });
           // $.response.setBody(JSON.stringify(response));
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009028',
                'type': 'E',
                'object': object
            });return null;
        }
    }
};

this.objectShare = {
    __getParam__: function() {
        var object = util.getParam('object');
        if (!object) return {};
        while (util.isString(object)) object = JSON.parse(object);
        return object;
    },
    'create': function(object) {
        // var object = this.__getParam__();
        var response;
        for (var i = 0; i < Object.keys(object).length; i++) {
            if (!object[i].hasOwnProperty('id_object')) {
                $.messageCodes.push({
                    'code': 'CORE009014',
                    'type': 'E',
                    'object': object[i]
                });return null;
            } else {
                for (var i = 0; i < Object.keys(object).length; i++) {
                    response = objectsShareModel.CREATE(object[i]);
                }
                // $.response.setBody(JSON.stringify(response));
                return response;
            }
        }
    return null;
    },
    'update': function(object) {
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('id_object')) {
            $.messageCodes.push({
                'code': 'CORE009015',
                'type': 'E',
                'object': object
            });return null;
        } else {
            var response = objectsShareModel.UPDATE(object);
            // $.response.setBody(JSON.stringify(response));
            return response;
        }
    },
    'list': function(object) {
        try {
            // var object = this.__getParam__();
            var response = [];
            response = objectsShareModel.READ({});
            // $.response.setBody(JSON.stringify(response));
            return response;
        } catch (e) {
            $.messageCodes.push({
                'code': 'CORE009027',
                'type': 'E',
                'object': object
            });return null;
        }
    },
    //params {'id_object':248,'id_users':[7475,21],'id_groups':[65,60]} ex object testing
    //if is empty any of the two it will replace the
    'updateShare': function(object) { //ready
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('id_object')) {
            $.messageCodes.push({
                'code': 'CORE009017',
                'type': 'E',
                'object': object
            });
            return null;
        } else {
            var user_array;
            var group_array;
            var response_array = [];
            var final_response;
            var response = objectsShareModel.READ({
                where: [{
                    field: 'id_object',
                    oper: '=',
                    value: object.id_object
                }]
            });
            try {
                var creationUserResponse = objects.READ({
                    where: [{
                        field: 'id_object',
                        oper: '=',
                        value: object.id_object
                    }]
                });
                if (creationUserResponse[0].creationUser === user.getTimpUser().id) {
                    var updateResponse = objects.UPDATEWHERE({
                        'shared': 'Shared'
                    }, [{
                        field: 'id_object',
                        oper: '=',
                        value: object.id_object
                    }]);
                    if (object.hasOwnProperty('id_users')) {
                        user_array = object.id_users;

                        for (var i = 0; i < response.length; i++) {
                            response_array.push(response[i].id_user);
                        }

                        for (var i = 0; i < user_array.length; i++) {
                            if (response_array.indexOf(user_array[i]) < 0) {
                                var params = {};
                                params.id_user = user_array[i];
                                params.id_object = object.id_object;
                                final_response = objectsShareModel.CREATE(params);
                            }
                        } //if the receiving params are diferent from the ones on db it changes to the new one

                        for (var i = 0; i < response_array.length; i++) {
                            if (user_array.indexOf(response_array[i]) < 0) {
                                final_response = objectsShareModel.DELETEWHERE([{
                                    field: 'id_user',
                                    oper: '=',
                                    value: response_array[i]
                                }, {
                                    field: 'id_object',
                                    oper: '=',
                                    value: object.id_object
                                }, {
                                    field: 'id_group',
                                    oper: '=',
                                    value: -1
                                }]);
                            }
                        }
                    }
                    response_array = [];
                    if (object.hasOwnProperty('id_groups')) {
                        group_array = object.id_groups;
                        for (var i = 0; i < response.length; i++) {
                            response_array.push(response[i].id_group);
                        }
                        for (var i = 0; i < group_array.length; i++) {
                            if (response_array.indexOf(group_array[i]) < 0) {
                                var params = {};
                                params.id_group = group_array[i];
                                params.id_object = object.id_object;
                                final_response = objectsShareModel.CREATE(params);
                            }
                        }
                        for (var i = 0; i < response_array.length; i++) {
                            if (group_array.indexOf(response_array[i]) < 0) {
                                final_response = objectsShareModel.DELETEWHERE([{
                                    field: 'id_group',
                                    oper: '=',
                                    value: response_array[i]
                                }, {
                                    field: 'id_object',
                                    oper: '=',
                                    value: object.id_object
                                }, {
                                    field: 'id_user',
                                    oper: '=',
                                    value: -1
                                }]);
                            }
                        }
                    }
                    return final_response;
                } else {
                    $.messageCodes.push({
                        'code': 'CORE009019',
                        'type': 'E',
                        'object': object
                    });
                    return null;
                }
            } catch (e) {
                $.messageCodes.push({
                    'code': 'CORE009019',
                    'type': 'E',
                    'object': object
                });
                return null;
            }
        }
        return null;
    }, //change shared status of the object
    //income json {'id_object':19,'shared':'Public'} id of object and new shared status
    'changeSharedStatus': function(object) {
        // var object = this.__getParam__();
        if (!object.hasOwnProperty('id_object') && !object.hasOwnProperty('shared')) {
            $.messageCodes.push({
                'code': 'CORE009035',
                'type': 'E',
                'object': object
            });
            return null;
        } else {
            try {
                var creationUserResponse = objects.READ({
                    where: [{
                        field: 'id_object',
                        oper: '=',
                        value: object.id_object
                    }]
                });
                if (creationUserResponse[0].creationUser === user.getTimpUser().id) {
                    var response;
                    switch (object.shared) {
                        case 'Private':
                            response = objects.UPDATEWHERE({
                                'shared': object.shared
                            }, [{
                                field: 'id_object',
                                oper: '=',
                                value: object.id_object
                            }]);
                            response = objectsShareModel.DELETEWHERE([{
                                field: 'id_object',
                                oper: '=',
                                value: object.id_object
                            }, {
                                field: 'id_user',
                                oper: '=',
                                value: -1
                            }]);
                            response = objectsShareModel.DELETEWHERE([{
                                field: 'id_object',
                                oper: '=',
                                value: object.id_object
                            }, {
                                field: 'id_group',
                                oper: '=',
                                value: -1
                            }]);
                            break;
                        case 'Public':
                            response = objects.UPDATEWHERE({
                                'shared': object.shared
                            }, [{
                                field: 'id_object',
                                oper: '=',
                                value: object.id_object
                            }]);
                            break;
                            // case 'Shared':
                            //     response = objects.UPDATEWHERE({'shared':object.shared},[{field:'id_object',oper:'=',value:object.id_object}]);
                            //     break;
                    }

                    return response;
                } else {
                    $.messageCodes.push({
                        'code': 'CORE009036',
                        'type': 'E',
                        'object': object
                    });
                    return null;
                    //must be id of creation user
                }
            } catch (e) {
                $.messageCodes.push({
                    'code': 'CORE009037',
                    'type': 'E',
                    'object': object
                });
                return null;
            }
        }
    }, //Income JSON ex: object={'id_object':22} receives the id_object and returns the users or groups related to that object
    'listSharedUsers': function(object) {
        // var object = this.__getParam__();
        var response = objectsShareModel.READ({
            where: [{
                field: 'id_object',
                oper: '=',
                value: object.id_object
            }]
        })
        var id_users = [];
        var id_groups = [];
        var finalResponse = {};

        for (var i = 0; i < response.length; i++) {
            if (response[i].id_user !== -1) {
                id_users.push(response[i].id_user);
            }
            if (response[i].id_group !== -1) {
                id_groups.push(response[i].id_group)
            }
        }

        finalResponse.id_users = id_users;
        finalResponse.id_groups = id_groups;

        //$.response.setBody(JSON.stringify(finalResponse));
        return finalResponse;
    }
    //returns {'id_users':[1,2,3,5,6],'id_groups':[1,2,3]} or any of them empty

};

$.import('timp.core.server.objects', 'meta_object');
var meta_objects = $.timp.core.server.objects.meta_object;

this.base = {
    display: function (object) {
        // var object = $.request.parameters.get('object');
        while (typeof object == 'string') {
            try {
                object = JSON.parse(object);
            } catch (e) {
                return util.parseError(object);
            }
        }
        var type = object['type'];
        
        
        var obj_tag = {type: type},
            options = {};
        
        try {
            var my_request = meta_objects.BaseObject.getClass(obj_tag);
            
            if (object.hasOwnProperty('pageSize') && object.hasOwnProperty('page')) {
                object.paginate = { //Returns 'pageCount' with the total of pages
                    size: object.pageSize || 30, //What is the page size?
                    number: object.page || 1, //What is the page number?
                    count: true //Do you want a total of pages?
                }
            }
            
            object.safe = true;
            return my_request.dialogPick(object);
        } catch (e) {
            throw e
            $.messageCodes.push({code: 'CORE000007', type: 'E', catchInfo: util.parseError(e)});
        }
        
        return 'failed';
    },
    get: function (object) {
        // var object = $.request.parameters.get('object');
        while (typeof object == 'string') {
            try {
                object = JSON.parse(object);
            } catch (e) {
                return util.parseError(object);
            }
        }
        
        var obj_tag = object;
        
        try {
            var my_obj = meta_objects.BaseObject.getObject(obj_tag);
            
            return my_obj;
        } catch (e) {
            $.messageCodes.push({code: 'CORE000007', type: 'E', catchInfo: e});
        }
        
        return 'failed';
    }
}