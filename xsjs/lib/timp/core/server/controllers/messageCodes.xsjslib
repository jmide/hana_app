$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;

$.import('timp.core.server.models', 'messageCodes');
var messageCodesModel = $.timp.core.server.models.messageCodes.messageCodes;
var messageCodeTextsModel = $.timp.core.server.models.messageCodes.messageCodeTexts;

//MessageCode controller
this.messageCodes = {
	'create': function(object) {
		var params = object; //this.__getParam__();

		if (!params.hasOwnProperty('code') && !params.hasOwnProperty('id')) {
			// $.messageCodes.push({"code":"CORE######","type":"E","params":params});
			throw 'Cannot create a messageCode without code or id'
		}

		var response = messageCodesModel.CREATE(params);

		return (response);
	},
	'update': function(object) {
		var params = object; //this.__getParam__();

		if (!params.hasOwnProperty('id')) {
			//$.messageCodes.push({"code":"CORE######","type":"E","params":params});
			throw 'Cannot update a messageCode without code'
		}
		var response = messageCodesModel.UPDATE(params);

		return (response);
	},

	'list': function() {
		var response = [];
		response = messageCodesModel.READ({});
		return (response);
	},
	'listByComponent': function(object) {
		var params = object; //this.__getParam__();
		if (!params.hasOwnProperty('id_component') && !params.hasOwnProperty('dev_type') && !params.hasOwnProperty('area') && !params.hasOwnProperty(
				'lang')) {
			throw 'Cannot list messageCodes without code, area or lang';
		}
		var readList = messageCodesModel.READ({
			where: [{
				field: "id_component",
				oper: "=",
				value: params.id_component
			}, {
				field: "dev_type",
				oper: "=",
				value: params.dev_type
			}, {
				field: "area",
				oper: "=",
				value: params.area
			}, {
				field: "lang",
				oper: "=",
				value: params.lang,
				table: messageCodeTextsModel
			}],
			join: [{
				table: messageCodeTextsModel,
				alias: "messageCodeText",
				on: [{
					left: "code",
					right: "code"
				}]
			}]
		});

		var code = "";
		var text = "";
		var seq = "";
		var result = {
			"lang": params.lang,
			"id_component": params.id_component,
			"dev_type": params.dev_type,
			"area": params.area,
			"codes": {}
		};

		for (var i = 0; i < readList.length; i++) {
			for (var item in readList[i]) {
				code = readList[i].code;
				seq = readList[i].code_seq;
				text = readList[i].messageCodeText[0].text;

				result.codes[code] = {};
				result.codes[code].code_seq = seq;
				result.codes[code].text = text;
				result.codes[code].id = readList[i].id;
				result.codes[code].id_text = readList[i].messageCodeText[0].id;
			}
		}
		return (result);
	},
	'updateAll': function(par) {
		var params = par || $.request.parameters.get("object"); //this.__getParam__();
		if (typeof params === 'string') {
			params = JSON.parse(params);
		}
		if (!params.hasOwnProperty('lang')) {
			//$.messageCodes.push({"code":"CORE######","type":"E","params":params});
			throw 'Cannot update a messageCode without lang attributes'
		}
		var messageCode = {};
		var messageCodeText = {};
		var response;
		//object has the key of attribute "object"
		for (var object in params.codes) {
			messageCode.id = params.codes[object].id;
			messageCode.code_seq = params.codes[object].code_seq;
			messageCode.code = object;
			messageCode.id_component = params.id_component;
			messageCode.dev_type = params.dev_type;
			messageCode.area = params.area;
			messageCodeText.id = params.codes[object].id_text;
			messageCodeText.lang = params.lang;
			messageCodeText.text = params.codes[object].text;
			messageCodeText.code = object;
			try {
				var messageCodeId = messageCodesModel.READ({
					where: [{
						field: "code",
						oper: "=",
						value: object
					}]
				});
				messageCodeId = (messageCodeId.length > 0) ? messageCodeId[0].id : -1;
				if (messageCodeId !== -1) {
					messageCode.id = messageCodeId;
					response = messageCodesModel.UPDATE(messageCode);
				} else {
					response = messageCodesModel.CREATE(messageCode);
				}
				var messageCodeTextId = messageCodeTextsModel.READ({
					where: [{
						field: "code",
						oper: "=",
						value: object
					}, {
						field: "lang",
						oper: "=",
						value: params.lang
					}]
				});
				messageCodeTextId = (messageCodeTextId.length > 0) ? messageCodeTextId[0].id : -1;
				if (messageCodeTextId !== -1) {
					messageCodeText.id = messageCodeTextId;
					response = messageCodeTextsModel.UPDATE(messageCodeText);
				} else {
					response = messageCodeTextsModel.CREATE(messageCodeText);
				}
			} catch (e) {
				response = e;
			}
		}
		return (response);
	}
};

//MessageCodeTexts controller
this.messageCodeTexts = {
	'create': function(object) {
		var params = object; //this.__getParam__();

		if (!params.hasOwnProperty('code') && !params.hasOwnProperty('lang')) {
			//$.messageCodes.push({"code":"CORE######","type":"E","params":params});
			throw 'Cannot create messageCodeTexts without code or lang'
		}
		var response = messageCodeTextsModel.CREATE(params);
		return (response);
	},
	'update': function(object) {
		var params = object; //this.__getParam__();
		if (!params.hasOwnProperty('code') && !params.hasOwnProperty('id')) {
			//$.messageCodes.push({"code":"CORE######","type":"E","params":params});
			throw 'Cannot update a messageCode without code or id'
		}
		var response = messageCodeTextsModel.UPDATE(params);
		return (response);
	},
	'list': function() {
		// var params = object;//this.__getParam__();
		var response = [];
		response = messageCodeTextsModel.READ({});
		return (response);
	},
	'listTags': function(object) {
		var params = object; //this.__getParam__();
		var response = [];
		response = messageCodeTextsModel.READ({
			fields: ["code", "text"],
			where: [{
				field: "lang",
				oper: "=",
				value: params.lang
			}]
		});
		return (response);
	},
	'listTagsByComponent': function(params) {
		var response = [];
		var whereOptions = [];
		if (params.lang) {
			whereOptions.push({
				field: "lang",
				oper: "=",
				value: params.lang
			});
		}
		if (params.idComponent) {
			whereOptions.push({
				field: "id_component",
				oper: "=",
				value: params.idComponent,
				table: messageCodesModel
			});
		}
		var options = {
			fields: ["code", "text"],
			join: [{
				table: messageCodesModel,
				alias: "messageCode",
				on: [{
					left: "code",
					right: "code"
				}]
			}],
			where: whereOptions
		};
		// options.simulate = true;
		response = messageCodeTextsModel.READ(options).map(function(message) {
			return {
				code: message.code,
				text: message.text
			};
		});
		return response;
	}
};