$.import("timp.core.server","util");
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'users');
var users = $.timp.core.server.models.users.users;

$.import('timp.core.server.controllers','messageCodes');
var messageCodes = $.timp.core.server.controllers.messageCodes;

// $.import('timp.atr.server.models', 'structure');
// var ATRmodel = $.timp.atr.server.models.structure.table;
// $.import('timp.atr.server.models', 'settings');
// var settings = $.timp.atr.server.models.settings.table;
$.import('timp.core.server.models', 'orgStructure');
var companyModel = $.timp.core.server.models.orgStructure.company;
var branchModel = $.timp.core.server.models.orgStructure.branch;
var addressModel = $.timp.core.server.models.orgStructure.address;

$.import('timp.core.server.models','cvUserPrivileges');
var tablesCvUserPrivileges = $.timp.core.server.models.cvUserPrivileges.table;

$.import("timp.core.server.models","components");
var tablesCoreApps = $.timp.core.server.models.components.apps;
var components = $.timp.core.server.models.components.components;
var privileges = $.timp.core.server.models.components.privileges;
var apps = $.timp.core.server.models.components.apps;

//---------------  VIEW -----------------//
//functions for endpoints

//list all ATR Privileges
this.listATRPrivileges = function(){
    $.import("timp.atr.server.api","api");
    var atr_api = $.timp.atr.server.api.api;
    var ATRmodel = atr_api.structure.table;
    var settings = atr_api.settings.table;

    var result = atrPrivileges.listATRPrivileges(); 
    if(!result.errorMsg){
        return (result)
    }
    else{
     return (result.errorMsg);
 }
}

//list Company Privileges
this.listCompanyPrivileges = function(){
    var result = orgPrivileges.listCompanies(); 
    if(!result.errorMsg){
        return (result)
    }
    else{
     return (result.errorMsg);
 }
}

//list State Privileges
this.listStatePrivileges = function(){
    var result = orgPrivileges.listRegions(); 
    if(!result.errorMsg){
        return (result)
    }
    else{
     return (result.errorMsg);
 }
}

//list all Organizational Privileges in a tree
this.listOrgPrivileges = function(){

}

//--------------API ------------------//
//Functions for reuse in other components controllers
this.atrPrivileges = {};
this.atrPrivileges.listATRPrivileges = function(){
    var options = {fields:["id","title"]};
    var structures = {};
    try{
        structures = ATRmodel.READ(options);
    }
    catch(e){
        structures.errorMsg = util.parseError(e);
    }
    return structures ;
}


this.orgPrivileges = {};

this.orgPrivileges.listCompanies = function(){
    var options = {
        join: [
        {
         fields: [ ],
         alias: 'M',
         table: settings,
         on: [{ left: 'mandt', right: 'mandt'}]
     }
     ]
 };
 var companies = {};
 try{
    companies = companyModel.READ(options);
}
catch(e){
    companies.errorMsg = util.parseError(e);
}
return companies ;
}

this.orgPrivileges.listRegions = function(companyID){
    var regions = {};
    var options = {
        fields : [
        'state'
        ],
        distinct : true
    };
    
    if(companyID && typeof companyID == 'string'){
        options.where =  [{field: 'companyID', oper: "=", value: companyID}];
    }
    
    try{
        regions = branchModel.READ(options);
    }
    catch(e){
        regions.errorMsg = util.parseError(e);
    }
    return regions ;
}

this.orgPrivileges.listBranches = function(companyID, stateID){
    var branches = {};
    var options = {
        fields : ['state'],
        distinct : true
    };
    
    if(companyID && typeof companyID == 'string'){
        options.where =  [{field: 'companyID', oper: "=", value: companyID}];
    }
    
    try{
        branches = branchModel.READ(options);
    }
    catch(e){
        branches.errorMsg = util.parseError(e);
    }
    return branches ;
}


this.getUserPriv = function(){
    var result = this.getUserPrivileges(); 
    if(!result.errorMsg){
        return (result)
    }
    else{
     return (result.errorMsg);
 }
}

this.getUserPrivileges = function(component){
    //component = "filleddocument";
    var username = $.session.getUsername();
    var response;
    var userAppsPrivileges = {};
    var marketplace;
    try {
        var result = components.READ({ 
            where: [
                {"field": "name", "value": 'MKT', "oper": "="}
            ]   
        });
        if(result.length>0){
            marketplace = true;
        }
        else{
            marketplace = false;
        }
        
    } catch (e) {
        marketplace = false;
    }
    
    try {
        if (marketplace) {
        //   if(component == 'mkt'){  
            response = tablesCvUserPrivileges.listViewUsersPrivileges(component, username);
            for (var i = 0; i < response.length; i++) {
                //id is not needed because its equals to privilegeID and a view doesn't need a field ID, so delete it from object
                delete response[i].id; 
                // if (response[i].privilegeName.match(/create|update|delete|copy|edit/i)) {
                //     continue; //Deactivate all permissions for creating objects
                // }
                userAppsPrivileges[response[i].privilegeID] = response[i]; 
            }
            
            return userAppsPrivileges;
        } else {
            response = tablesCoreApps.getPrivilegesAppsByComponents(component);
            for (var i = 0; i < response.length; i++) {
                for (var j = 0; j < response[i].privileges.length; j++) {
                    var name = response[i].privileges[j].name;
                    // if (name.match(/create|update|delete|copy|edit/i)) {
                    //     continue; //Deactivate all permissions for creating objects
                    // }
                    userAppsPrivileges[response[i].privileges[j].id] = {};
                    userAppsPrivileges[response[i].privileges[j].id].hanaUser = $.session.getUsername(); 
                    userAppsPrivileges[response[i].privileges[j].id].privilegeID = response[i].privileges[j].id; 
                    userAppsPrivileges[response[i].privileges[j].id].privilegeName = response[i].privileges[j].name; 
                    userAppsPrivileges[response[i].privileges[j].id].component = response[i].component; 
                    userAppsPrivileges[response[i].privileges[j].id].appID = response[i].id; 
                    userAppsPrivileges[response[i].privileges[j].id].appName = response[i].name; 
                }
            }
            

            return userAppsPrivileges;
        }
    }
    catch(e){
     throw 'Error on reading privileges: ' + util.parseError(e); 
 }

}

this.getComponentPrivileges = function(component){
    var index = {};
    try{
        var username = $.session.getUsername();
        var componentID = components.READ({
            fields: ["id"],
            where: [{"field": "name", "value": component, "oper": "="}]
        })[0].id;
        var allApps = apps.READ({
            fields: ["id"],
            where: [{"field": "component_id", "value": componentID, "oper": "="}]
        });
        var allPriv = privileges.READ({
            fields: ["name"],
            where: [allApps.map(function(v){
                return {"field": "id_app", "value": v.id, "oper": "="};
            })]
        });
        
        index = allPriv.reduce(function(ans, priv){
            var parts = priv.name.split(".");
            var pointer = ans;
            parts.forEach(function(part, idx, arr){
                if(!pointer.hasOwnProperty(part)){
                    if(idx === arr.length -1){
                        pointer[part] = false;
                    }else{
                        pointer[part] = {};
                        pointer = pointer[part];
                    }
                }else{
                    pointer = pointer[part];
                }
            });
            return ans;
        }, {});
        // if (username === 'JMIDE') {
        //     return tablesCvUserPrivileges;
        // }
        
        var options = {
            fields: ["privilegeName"],
            where: [
                {"field": "component", "value": component, "oper": "="},
                {"field": "hanaUser", "value": username, "oper": "="}
            ]
        };
        if ($.getUserID() === 1366643) {
            options.simulate = true;
            return tablesCvUserPrivileges.READ(options);
        }
        var userAppsPrivileges =  tablesCvUserPrivileges.READ(options);
        
        userAppsPrivileges.forEach(function(privilege){
            var parts = privilege.privilegeName.split(".");
            var pointer = index;
            parts.forEach(function(part, idx, arr){
                if(idx === arr.length -1){
                    try{
                        pointer[part] = true;    
                    }catch(e){
                        throw username;
                    }
                }else{
                    pointer = pointer[part];
                }
            });
        });
        return index;
    } catch (e){
        return index;
    }
};

this.hasPrivilege = function(component, privilege){
    //component = "filleddocument";
    var userAppsPrivileges = {};
    var marketplace;
    try {
        var result = components.READ({ 
            where: [
                {"field": "name", "value": 'MKT', "oper": "="}
            ]   
        });
        marketplace = result.length > 0;
    } catch (e) {
        marketplace = false;
    }
    
    try {
        if (marketplace) {
        //   if(component == 'mkt'){  
            var username = $.session.getUsername();
            var options = {
                where: [
                    {"field": "component", "value": component, "oper": "="},
                    {"field": "hanaUser", "value": username, "oper": "="},
                    {"field": "privilegeName", "value": privilege, "oper": "="}
                ]       
            };
            userAppsPrivileges =  tablesCvUserPrivileges.READ(options);
            return userAppsPrivileges.length > 0;
        } else {
            return true;
        }
    }
    catch(e){
     throw 'Error on reading privileges: ' + util.parseError(e); 
 }

};

this.verifyPrivilege = function(component, privilege){
    if(this.hasPrivilege(component, privilege)){
        return true;
    }
	$.response.status = 401;
	$.logError("CORE000008", {
	    component: component,
	    privilege: privilege
	});
	return false;
};

this.checkParametersByTask = function(object) {
    try {
    	object = object || $.request.parameters.get("object");
    	if (typeof object === "string") {
    		object = JSON.parse(object);
    	}
        $.import("timp.bpma.server.api","api");
        var bpmaApi = $.timp.bpma.server.api.api;
        
        if (object.idTask) {
            return bpmaApi.engine.checkParametersByTask(object);
        } else {
    		return false;
        }
        
    } catch (e) {
        $.messageCodes.push({
			"code": "BPMA001004",
			"type": 'E',
			"errorInfo": util.parseError(e)
		});
    }
};

this.getCoreData = function(object){
    try{
        $.import("timp.core.server.controllers","config");
        var config = $.timp.core.server.controllers.config;
        
        
        $.import("timp.atr.server.api","api");
        var atr_api = $.timp.atr.server.api.api;
        var settings = atr_api.settings.table;
        var settingsController = atr_api.settingsController;
        
        var response = {
            privileges: {},
            listTags: [],
            loggedUser: {},
            onCloud: config.isOnCloud(),
            sessionTime: Number(config.getSessionTime())
        };
        if ( $.session.getUsername() ) {
            var lang = $.request.cookies.get("Content-Language");
        
            var id_component = components.READ({
                fields:["id", "version"],
                where: [{
                    field: "name",
                    oper: "=",
                    value: object.component
                }] 
            });
            response.listTags = messageCodes.messageCodeTexts.listTags({
                lang: lang === "enus" ? "enus" : "ptrbr"
            });
            
            if (id_component.length === 0) {
                id_component = apps.READ({
                    fields: ["id", "name", "component_id"],
                    where: [{
                        field: "name",
                        oper: "LIKE",
                        maskFn: 'UPPER',
                        value: object.component
                    }],
                    join: [{
                        alias: "component",
                        fields:["id", "version"],
                        table: components,
                        on: [{
                            left: "component_id",
                            right: "id"
                        }]
                    }]
                });
                if (id_component.length > 0) {
                    id_component = id_component[0].component[0];
                }
            } else {
                id_component = id_component[0];
            }
            
            if(id_component && id_component.hasOwnProperty("id")){
                response.privileges = this.getComponentPrivileges(object.component);
                /*response.listTags = messageCodes.messageCodeTexts.listTagsByComponent({
                    idComponent: id_component.id,
                    lang: lang
                });*/
                response.version = id_component.version;
            }
            response.loggedUser = this.getLoggedUser();
            
            //Begin ATR::Settings verification
	            //Checks if the ATR::Settings is updated with the current year
	            var responseCheckYear = settings.checkYearSettings();
	            if(responseCheckYear && responseCheckYear.length > 0) {
	                response.updateYearSettings = [];
	            } else {
	                //If it's not update, it will update the records.
	                //Gets the minimum year in ATR::Settings
	                var minimumYear = settingsController.returnYearRange();
	                var updateYear = null;
	                if(minimumYear && minimumYear.length > 0) {
	                	if(minimumYear[0] && minimumYear[0][0]) {
	                		//Updates ATR::Settings and ATR::Fulldates
			                updateYear = settingsController.updateYearRange(minimumYear[0][0]);
	                	}
	                	response.updateYearSettings = updateYear;
	                }
	            }
	        //Ends ATR::Settings verification
        }
        else {
            response.loggedUser = false;
        }
        return response;
    }catch(e){
        return util.parseError(e);
    }
};



this.getLoggedUser = function() {
    try {
        var hana_user = $.session.getUsername();
        var options = {
            where: [{
                field: 'hana_user',
                oper: '=',
                value: hana_user.toUpperCase()
            }]
        };

        var result = users.READ(options);
        return result[0];
    } catch (e) {
        $.messageCodes.push({
            "code": "MKT201101",
            "type": 'E',
            "errorInfo": util.parseError(e)
        });
        return null;
    }
};