$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

//IMPORTS
$.import('timp.core.server.models', 'fileSystem');
var fileSystem = $.timp.core.server.models.fileSystem;
var folderShareModel = fileSystem.folderShare;
var fileModel = fileSystem.file;
var fileFavsModel = fileSystem.fileFavs;
var fileShareModel = fileSystem.fileShare;
$.import('timp.core.server.controllers', 'users');
var user = $.timp.core.server.controllers.users;

//$.import("timp.dfg.server.api", "api");
//var dfgapi = $.timp.core.server.api.api;
//var modelLayoutVersion = dfgapi.modelLayoutVersion;

//CONSTANTS
this.statusTypes = fileSystem.statusTypes;

// Files Functions

/*
	input:
	{
		idObject: Number,
		objectType: String,
		idFolder: Number
	}
*/
this.createFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('createFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		if (!object.hasOwnProperty('idFolder') || !object.idFolder || object.idFolder === null) {
			throw errorGenerator.generateError('00', '06');
		}
		if (!object.hasOwnProperty('idObject') || !object.idObject || object.idObject === null) {
			throw errorGenerator.paramsError(['idObject']);
		}
		if (!object.hasOwnProperty('objectType') || !object.objectType || object.objectType === null) {
			throw 'objectType is necessary';
		}
		if (object.idFolder !== -1) {
			var folder = fileSystem.readFolder(object.idFolder);
			if (!folder) {
				throw 'Folder doesn\'t exist';
			}
			if (folder.status === this.statusTypes.TRASH || folder.status === this.statusTypes.DELETED) {
				throw 'The Folder was deleted';
			}
		}

		var idObjectType = this.getIdObjectType(object.objectType);
		object.idObjectType = idObjectType;
		var file = fileSystem.createFile(object);
		// 		if($.getUserID()===1325941){
		// 		    return {
		// 		        newFile: file
		// 		    }
		// 		}   

		var shared = fileSystem.readFolderShare(Number(object.idFolder));

		if (shared !== undefined) {
			for (var i = 0; i < shared.length; i++) {
				var shareOptions = {
					idFile: file.id,
					idUser: shared[i].idUser,
					idGroup: shared[i].idGroup,
					idObjectType: idObjectType,
					idObject: object.idObject
				};
				fileSystem.createFileShare(shareOptions);
			}
		}
		return file;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009046',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '72', null, e);
	}
};

/*
	intput:
	{
		idFile: Number,
		fields: Array,
		idObject: Number,
		objectType: String
	}
*/
this.getFile = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var options = {
			where: []
		};
		for (var property in object) {
			if (property !== 'lang') {
				if (property === 'fields') {
					options.fields = object[property];
				} else {
					var value;
					if (property === 'objectType') {
						value = this.getIdObjectType(object.objectType);
						property = 'idObjectType';
					} else if (property === 'idFile') {
						value = object[property];
						property = 'id';
					} else {
						value = object[property];
					}
					options.where.push({
						field: property,
						oper: '=',
						value: value
					});
				}
			}
		}
		return fileSystem.readFile(options);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	intput:
	{
		idFile: Number, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
		status: Number || String
	}
*/
this.updateFileStatus = function(object) {
	let errorGenerator = new $.ErrorGenerator('updateFileStatus');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		object.fields = ['id','creationUser'];
		var files = this.getFilesToFavorites(object);
		var result = {};
		var options = [];
		for (var i = 0; i < files.length; i++) {
			if(files[i].creationUser === $.getUserID()){
				options.push( {
					id: files[i].id,
					status: object.status
				});
			}
			else{
				throw 'NOT CREATION USER';
			}
		}
		if(options.length === files.length){
			for(let i = 0; i< options.length; i++){
			   let file = fileSystem.updateFile(options[i]);
				result[files[i].id] = file;
			}
		}else{
			throw errorGenerator.generateError('00', '73');
		}
		return result;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: e!=='NOT CREATION USER'?'CORE009048':'CORE000018',
			errorInfo: util.parseError(e)
		});
		return false;
	}
};

/*
	intput:
	{
		idFile: Number || Array, 
		idObject: Number, // Opcional
		objectType: String, // Opcional
		idUser: Number || Array, // Opcional
		idGroup: Number, // Opcional
	}
*/
this.shareFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('shareFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var format = this._objectShareFormating(object);
		object = format.object;
		object.fields = ['id', 'idObjectType', 'idObject'];

		var files = this.getFilesToFavorites(object);

		var count = 0;
		for (var i = 0; i < files.length; i++) {
			for (var j = 0; j < object.idUser.length; j++) {
				var optionsCreate = {
					idUser: object.idUser[j].id_user,
					idGroup: object.idUser[j].id_group,
					file: files
				};
				count += fileSystem.createShareFile(optionsCreate, i);
			}
		}
		return {
			files: count
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009049',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
		idUser: Number, // Opcional
		idGroup: Number, // Opcional
	}
*/
this.unshareFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('unshareFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var format = this._objectShareFormating(object);
		object = format.object;
		var fileOptions = {
			fields: ['id'],
			join: [{
				table: fileModel,
				alias: 'file',
				fields: [],
				on: [{
					left: 'idFile',
					right: 'id'
				}]
			}],
			where: [
				[]
			]
		};
		var deleteOptions = [
			[]
		];
		object.fields = ['id'];
		var files = this.getFilesToFavorites(object);
		for (var i = 0; i < object.idUser.length; i++) {
			fileOptions.where[0][i] = [{
				field: 'idUser',
				oper: '=',
				value: object.idUser[i].id_user
			}];
			if (object.idUser[i].id_group) {
				fileOptions.where[0][i].push({
					field: 'idGroup',
					oper: '=',
					value: object.idUser[i].id_group
				});
			} else {
				fileOptions.where[0][i].push({
					field: 'idGroup',
					oper: 'IS NULL'
				});
			}
		}
		if (files.length) {
			var temp = [];
			for (i = 0; i < files.length; i++) {
				temp.push({
					field: 'id',
					oper: '=',
					value: files[i].id
				});
			}
			fileOptions.join[0].on.push(temp);
		}
		files = fileSystem.readFileShare(fileOptions);
		for (i = 0; i < files.length; i++) {
			deleteOptions[0].push({
				field: 'id',
				oper: '=',
				value: files[i].id
			});
		}
		var filesCount = files.length;
		var delFiles = filesCount === 0 ? false : fileSystem.deleteFileShare(deleteOptions);
		return {
			files: delFiles ? Number(filesCount) : 0
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009050',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
	}
*/
this.getUsersGroupsFromShare = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		$.import('timp.core.server.models', 'users');
		var userModel = $.timp.core.server.models.users.table;
		$.import('timp.mkt.server.api', 'api');
		var groupModel = $.timp.mkt.server.api.api.tables.groups;
		var options = [];
		for (var i = 0; i < 2; i++) {
			options.push({
				join: [{
					table: object.idFolder ? folderShareModel : fileShareModel,
					alias: 'folder',
					fields: [],
					on: [{
						left: 'id',
						right: i === 0 ? 'idUser' : 'idGroup'
					}]
				}],
				where: [[]],
				simulate: false
			});
			if (i === 1) {
				options[1].join.push({
					alias: 'group_leader',
					fields: ['id', 'name', 'hana_user', 'middle_name', 'last_name'],
					table: userModel,
					on: [{
						left: 'id_leader',
						right: 'id'
					}],
					outer: 'left'
				});
			}
		}
		options[0].join[0].on.push({
			table: fileShareModel,
			field: 'idGroup',
			oper: 'IS NULL'
		});
		if (object.idFolder) {
			for (i = 0; i < 2; i++) {
				options[i].where[0].push({
					table: folderShareModel,
					field: 'idFolder',
					oper: '=',
					value: object.idFolder
				});
			}
		}
		if (object.idFile) {
			for (i = 0; i < 2; i++) {
				options[i].where[0].push({
					table: fileShareModel,
					field: 'idFile',
					oper: '=',
					value: object.idFile
				});
			}
		}
		if (object.idObject && object.objectType) {
			var idObjectType = this.getIdObjectType(object.objectType);
			for (i = 0; i < 2; i++) {
				options[i].where[0].push([{
					table: fileShareModel,
					field: 'idObject',
					oper: '=',
					value: object.idObject
				}, {
					table: fileShareModel,
					field: 'idObjectType',
					oper: '=',
					value: idObjectType
				}]);
			}
		}
		// options[0].simulate = true;
		// options[1].simulate = true;
		var users = userModel.READ(options[0]);
		var groups = groupModel.READ(options[1]);
		return {
			users: users,
			groups: groups
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009049',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
		trash: Boolean // true - se manda al trash : false - se elimina
	}
*/
this.deleteFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('deleteFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		object.trash = object.trash !== undefined ? object.trash : true;
		object.fields = ['id'];
		var files = this.getFilesToFavorites(object);
		var shareOptions = [];
		var favOptions = [];
		var options = [];
		for (var i = 0; i < files.length; i++) {
			shareOptions.push({
				field: 'idFile',
				oper: '=',
				value: files[i].id
			});
			favOptions.push({
				field: 'idFile',
				oper: '=',
				value: files[i].id
			});
			options.push({
				field: 'id',
				oper: '=',
				value: files[i].id
			});
		}
		fileSystem.deleteFileShare([shareOptions]);
		fileSystem.deleteFileFavs([favOptions]);
		var params = {
			status: object.trash ? this.statusTypes.TRASH : this.statusTypes.DELETED
		};
		return fileSystem.updateFile(params, [options]);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009051',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '74', null, e);
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String
	}
*/
this.restoreFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('restoreFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		object.fields = ['id', 'status', 'idFolder'];
		var files = this.getFilesToFavorites(object);
		var result = {};
		for (var i = 0; i < files.length; i++) {
			if ((typeof files[i].status === 'string' && files[i].status.toUpperCase() === 'TRASH') || (files[i].status === this.statusTypes.TRASH)) {
				var folder = fileSystem.readFolder({
					fields: ['id', 'status'],
					where: [{
						field: 'id',
						oper: '=',
						value: files[i].idFolder
					}]
				})[0];
				var options = {};
				if (folder !== undefined && ((typeof folder.status === 'string' && folder.status.toUpperCase() !== 'TRASH') ||
					(typeof folder.status === 'number' && folder.status !== this.statusTypes.TRASH))) {
					options = {
						id: files[i].id,
						status: this.statusTypes.ACTIVE
					};
				} else {
					options = {
						id: files[i].id,
						status: this.statusTypes.ACTIVE,
						idFolder: -1
					};
				}
				var file = fileSystem.updateFile(options);
				result[files[i].id] = file;
			}
		}
		return result;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009052',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
		
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
		idFolder: Number
	}
*/
this.copyFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('copyFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		object.restrict = true;
		object.join = true;
		var files = this.getFilesToFavorites(object);
		var result = {};
		for (var i = 0; i < files.length; i++) {
			var options = {
				status: files[i].status,
				idFolder: object.idFolder,
				idObject: files[i].idObject,
				idObjectType: files[i].idObjectType
			};
			var file = fileSystem.createFile(options);
			result[files[i].id] = file;
		}
		return result;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009053',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '72', null, e);
	}
};

/*
	intput:
	{
		idFile: Number || Array, // Opcional
		idObject: Number, // Opcional
		objectType: String, // Opcional
		idFolder: Number
	}
*/
this.moveFile = function(object) {
	let errorGenerator = new $.ErrorGenerator('moveFile');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var folder = fileSystem.readFolder({
			fields: ['creationUser'],
			where: [{
				field: 'id',
				oper: '=',
				value: object.idFolder
			}]
		})[0];
		if (folder === undefined && object.idFolder !== -1) {
			throw errorGenerator.generateError('00', '06');
		}
		object.restrict = true;
		var files = this.getFilesToFavorites(object);
		var result = {};
		for (var i = 0; i < files.length; i++) {
			var options = {
				id: files[i].id,
				idFolder: object.idFolder
			};
			var file = fileSystem.updateFile(options);
			result[files[i].id] = file;
		}
		return result;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009054',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '72', null, e);
	}
};

/*
	input
	{
		objectType: String, // Opcional
		idFolder: Number,
		fields: Array // Opcional
	}
*/
this.listFilesByFolder = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		if (object.idFolder === 0) {
			return [];
		}
		var whereOptions = [];
		var userID = user.getTimpUser().id;
		if (object && object.objectType) {
			var idObjectType = this.getIdObjectType(object.objectType);
			whereOptions.push({
				field: 'idObjectType',
				oper: '=',
				value: idObjectType
			});
		}
		var folder = fileSystem.readFolder({
			fields: ['creationUser'],
			where: [{
				field: 'id',
				oper: '=',
				value: object.idFolder
			}]
		})[0];
		if (folder === undefined && object.idFolder !== -1) {
			throw 'Folder doesn\'t exist.';
		}
		whereOptions.push({
			field: 'idFolder',
			oper: '=',
			value: object.idFolder
		}, {
			field: 'status',
			oper: '!=',
			value: 2
		}, {
			field: 'status',
			oper: '!=',
			value: 3
		});
		var options = {};
		if (object.fields) {
			options.fields = object.fields;
		}
		if (folder && userID !== folder.creationUser) {
			options.join = [{
				table: fileShareModel,
				fields: [],
				alias: 'share',
				on: [{
					left: 'id',
					right: 'idFile'
				}],
				outer: 'left'
			}];
			whereOptions.push([{
				field: 'creationUser',
				oper: '=',
				value: userID
			}, {
				table: fileShareModel,
				field: 'idUser',
				oper: '=',
				value: userID
			}]);
		} else if (folder === undefined) {
			whereOptions.push({
				field: 'creationUser',
				oper: '=',
				value: userID
			});
		}
		options.where = whereOptions;
		return fileSystem.readFile(options);
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input
	{
		objectType: String,
		idFolder: Number, // Opcional
		fields: Array, // Opcional
		status: Number || String
	}
*/
this.listFilesByStatus = function(options) {
	options = options || $.request.parameters.get('object');
	if (typeof options === 'string') {
		options = util.__parse__(options);
	}
	if (!options.hasOwnProperty('idUser')) {
		options.idUser = user.getTimpUser().id;
	}
	try {
		var readOptions = {
			where: []
		};
		if (options.hasOwnProperty('objectType')) {
			readOptions.where.push({
				field: 'idObjectType',
				oper: '=',
				value: this.getIdObjectType(options.objectType)
			});
		}
		if (options.hasOwnProperty('idObject')) {
			readOptions.where.push({
				field: 'idObject',
				oper: '=',
				value: options.idObject
			});
		}
		if (options.hasOwnProperty('fields')) {
			readOptions.fields = options.fields;
		}
		if (options.hasOwnProperty('idFolder')) {
			readOptions.where.push({
				field: 'idFolder',
				oper: '=',
				value: options.idFolder
			});
		}
		if (options.hasOwnProperty('status')) {
			if (typeof options.status !== 'object') {
				let status = [options.status];
				options.status = status;
			}
			var where = [];
			for (var i = 0; i < options.status.length; i++) {

				switch (options.status[i]) {
					case 'ACTIVE ONLY':
						{
							where.push({
								field: 'creationUser',
								oper: '=',
								value: options.idUser
							}, {
								field: 'status',
								oper: '=',
								value: this.statusTypes.ACTIVE
							});
							break;
						}
					case 'ACTIVE':
						{
							where.push([[{
								field: 'creationUser',
								oper: '=',
								value: options.idUser
							}, {
								field: 'status',
								oper: '=',
								value: this.statusTypes.ACTIVE
					}], {
								field: 'status',
								oper: '=',
								value: this.statusTypes.PUBLIC
							}]); 
							break;
						}
					case 'TRASH':
						{
							where.push({
								field: 'status',
								oper: '=',
								value: this.statusTypes.TRASH
							});
							where.push({
								field: 'creationUser',
								oper: '=',
								value: options.idUser
							});
							break;
						}
					case 'PUBLIC':
					case 'STANDARD':
						{
							where.push({
								field: 'status',
								oper: '=',
								value: (options.status[i] === 'PUBLIC') ? this.statusTypes.PUBLIC : this.statusTypes.STANDARD
							});
							break;
						}
				}

			}
			if (where.length > 0) {
				readOptions.where = readOptions.where.concat(where);
			}

		}
		return fileSystem.readFile(readOptions);
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			code: 'CORE009047',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input
	{
		objectType: String, // Opcional
		idFolder: Number, // Opcional
		fields: Array, // Opcional
		status: Number || String, // Opcional
		idUser: Number
	}
*/
this.listFilesByUser = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var whereOptions = [];
		if (object.objectType) {
			var idObjectType = this.getIdObjectType(object.objectType);
			whereOptions.push({
				field: 'idObjectType',
				oper: '=',
				value: idObjectType
			});
		}
		if (object.idFolder) {
			whereOptions.push({
				field: 'idFolder',
				oper: '=',
				value: object.idFolder
			});
		}
		if (object.status) {
			whereOptions.push({
				field: 'status',
				oper: (object.statusNot ? '!' : '') + '=',
				value: object.status
			});
		}
		whereOptions.push({
			field: 'creationUser',
			oper: '=',
			value: object.idUser
		});
		var options = {};
		if (object.fields) {
			options.fields = object.fields;
		}
		options.where = whereOptions;
		return fileSystem.readFile(options);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input
	{
		objectType: String, // Opcional
		idFolder: Number, // Opcional
		fields: Array, // Opcional
	}
*/
this.listFilesInTrash = function(object) {
	object = object || $.request.parameters.get('object') || {};
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		object.status = this.statusTypes.TRASH;
		return this.listFilesByStatus(object);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

this.listSharedFiles = function(object) {
	object = object || $.request.parameters.get('object') || {};
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		var whereOptions = [{
			table: fileShareModel,
			field: 'idUser',
			oper: '=',
			value: userID
		}];
		if (object.idFolder) {
			whereOptions.push({
				field: 'idFolder',
				oper: '=',
				value: object.idFolder
			});
		}
		if (object.idObject) {
			whereOptions.push({
				field: 'idObject',
				oper: '=',
				value: object.idObject
			});
		}
		if (object.objectType) {
			var idObjectType = this.getIdObjectType(object.objectType);
			whereOptions.push({
				field: 'idObjectType',
				oper: '=',
				value: idObjectType
			});
		}
		return fileSystem.readFile({
			where: whereOptions,
			join: [{
				table: fileShareModel,
				fields: [],
				alias: 'shared',
				on: [{
					left: 'id',
					right: 'idFile'
				}]
			}]
		});
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		idObject: Number, // optional
		objectType: String, // optional
		idFile: String // optional
	}
*/
this.markFavorite = function(object) {
	let errorGenerator = new $.ErrorGenerator('markFavorite');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		var files = this.getFilesToFavorites(object);
		var result = {};
		for (var i = 0; i < files.length; i++) {
			var optionsCreate = {
				idFile: files[i].id,
				idUser: userID,
				idObjectType: files[i].idObjectType,
				idObject: files[i].idObject
			};
			var fav = {};
			var read = fileSystem.readFileFavs({
				where: Object.keys(optionsCreate).map(function(property) {
					return {
						field: property,
						oper: '=',
						value: optionsCreate[property]
					};
				})
			});
			if (read.length === 0) {
				fav = fileSystem.createFileFavs(optionsCreate);
			}
			result[files[i].id] = fav.hasOwnProperty('id');
		}
		return result;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009055',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
	}
};

/*
	input:
	{
		idObject: Number, // optional
		objectType: String, // optional
		idFile: String // optional
	}
*/
this.unmarkFavorite = function(object) {
	let errorGenerator = new $.ErrorGenerator('markFavorite');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		var files = this.getFilesToFavorites(object);
		var result = {};
		for (var i = 0; i < files.length; i++) {
			var temp2 = {
				idFile: files[i].id,
				idUser: userID,
				idObjectType: files[i].idObjectType,
				idObject: files[i].idObject
			};
			var optionsDelete = Object.keys(temp2).map(function(property) {
				return {
					field: property,
					oper: '=',
					value: temp2[property]
				};
			});
			var fav = fileSystem.deleteFileFavs(optionsDelete);
			result[files[i].id] = fav;
		}
		return result;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009056',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
	}
};

this.getFilesToFavorites = function(object) {
	var userID = object.idUser || user.getTimpUser().id;
	var whereOptions = [];
	if (object.objectType) {
		var idObjectType = this.getIdObjectType(object.objectType);
		whereOptions.push({
			field: 'idObjectType',
			oper: '=',
			value: idObjectType
		});
	}
	var temp, j;
	if (object.idObject) {
		if (typeof object.idObject === 'number') {
			object.idObject = [object.idObject];
		}
		temp = [];
		for (j = 0; j < object.idObject.length; j++) {
			temp.push({
				field: 'idObject',
				oper: '=',
				value: object.idObject[j]
			});
		}
		whereOptions.push(temp);
	}
	if (object.idFile) {
		if (typeof object.idFile === 'number') {
			object.idFile = [object.idFile];
		}
		temp = [];
		for (j = 0; j < object.idFile.length; j++) {
			temp.push({
				field: 'id',
				oper: '=',
				value: object.idFile[j]
			});
		}
		whereOptions.push(temp);
	}
	var joinOptions = [];
	if (object.join) {
		joinOptions.push({
			table: fileShareModel,
			fields: ['id'],
			alias: 'shared',
			outer: 'left',
			on: [{
				left: 'id',
				right: 'idFile'
			}]
		});
		whereOptions.push([{
			table: fileShareModel,
			field: 'idUser',
			oper: '=',
			value: userID
		}, {
			field: 'creationUser',
			oper: '=',
			value: userID
		}]);
	} else if (object.restrict) {
		whereOptions.push({
			field: 'creationUser',
			oper: '=',
			value: userID
		});
	}
	return fileSystem.readFile({
		fields: object.fields,
		join: joinOptions,
		where: whereOptions
	});
};

this.getFavoriteFiles = function(object) {
	var userID = object.idUser || user.getTimpUser().id;
	var idObjectType = this.getIdObjectType(object.objectType);
	var readOptions = {};
	var whereOptions = [{
		field: 'creationUser',
		oper: '=',
		value: userID
	}, {
		field: 'idObjectType',
		oper: '=',
		value: idObjectType
	}];
	readOptions.where = whereOptions;
	if (object.fields) {
		readOptions.fields = object.fields;
	}
	return fileSystem.readFileFavs(readOptions);
};

this.getSharedFiles = function(object) {
	var userID = object.idUser || user.getTimpUser().id;
	var idObjectType = this.getIdObjectType(object.objectType);
	var readOptions = {};
	var whereOptions = [{
		field: 'idUser',
		oper: '=',
		value: userID
	}, {
		field: 'idObjectType',
		oper: '=',
		value: idObjectType
	}];
	readOptions.where = whereOptions;
	if (object.fields) {
		readOptions.fields = object.fields;
	}
	return fileSystem.readFileShare(readOptions);
};

// Folders Functions

/*
	input:
	{
		idParent: Number,
		name: String,
		description: String
	}
*/
this.createFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('createFolder');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		if (!object.hasOwnProperty('idParent') || !object.idParent || object.idParent === null) {
			throw errorGenerator.generateError('00', '06');
		}
		if (!object.hasOwnProperty('name') || !object.name || object.name === null) {
			throw errorGenerator.generateError('00', '06');
		}
		if (!object.hasOwnProperty('objectType') || !object.objectType || object.objectType === null) {
			throw errorGenerator.generateError('00', '06');
		}
		if (object.idParent !== -1) {
			var folder = fileSystem.readFolder(object.idParent);
			if (!folder) {
				throw errorGenerator.generateError('00', '99');
			}
			if (folder.status === this.statusTypes.TRASH || folder.status === this.statusTypes.DELETED) {
				throw errorGenerator.generateError('00', '90');
			}
		}
		//get id object Type
		var idObjectType = this.getIdObjectType(object.objectType);
		var folderOptions = {
			idParent: object.idParent,
			name: object.name,
			description: object.description,
			status: this.statusTypes.ACTIVE,
			idObjectType: idObjectType
		};
		return fileSystem.createFolder(folderOptions);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009057',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '90', null, e);
	}
};

this.getFolder = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		return fileSystem.readFolder(object.idFolder || object.id) || {};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009057',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		idFolder: Number,
		demás propiedades a actualizar
	}
*/
this.updateFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('updateFolder');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var folder = fileSystem.readFolder(object.idFolder || object.id);
		if (!folder) {
			throw errorGenerator.generateError('00', '91');
		}
		for (var property in object) {
			if (folder.hasOwnProperty(property) && property !== 'id') {
				folder[property] = object[property];
			}
		}
		delete folder.creationUser;
		delete folder.creationDate;
		delete folder.modificationUser;
		delete folder.modificationDate;

		var parentFolder;
		if (object.hasOwnProperty('idParent') && object.idParent !== null && object.idParent !== -1) {
			parentFolder = fileSystem.readFolder({
				fields: ['status'],
				where: [{
					field: 'id',
					oper: '=',
					value: object.idParent
				}]
			});
			if (!parentFolder) {
				throw errorGenerator.generateError('00', '91');
			}
			if (parentFolder.status === this.statusTypes.TRASH || parentFolder.status === this.statusTypes.DELETED) {
				throw errorGenerator.generateError('00', '91');
			}
		}

		if (object.hasOwnProperty('status') && object.status !== null) {
			var items = this.getAllItems({
				idFolder: object.idFolder
			});
			//return items;
			fileSystem.updateFile({
				status: object.status
			}, [items.files]);
			fileSystem.updateFolder({
				status: object.status
			}, [items.folders]);
		}
		var resultFolder = fileSystem.updateFolder(folder);
		return resultFolder;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009058',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '91', null, e);
	}
};

/*
	input:
	{
		idFolder: Number
	}
*/
this.deleteFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('deleteFolder');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var items = this.getAllItems({
			idFolder: object.idFolder,
			foldersWhere: [],
			filesWhere: []
		});
		var getFileItems = function() {
			return items.files.map(function(file) {
				return {
					field: file.field,
					oper: file.oper,
					value: file.value
				};
			});
		};
		var getFolderItems = function() {
			return items.folders.map(function(file) {
				return {
					field: file.field,
					oper: file.oper,
					value: file.value
				};
			});
		};
		var filesOpt = getFileItems();
		var foldersOpt = getFolderItems();
		fileSystem.deleteFolderShare([filesOpt]);
		filesOpt = getFileItems();
		var files = fileSystem.readFile({
			fields: ['id'],
			where: [filesOpt]
		});
		var folders = fileSystem.readFolder({
			fields: ['id'],
			where: [foldersOpt]
		});
		var whereOptions = [];
		var whereOptions2 = [];
		for (var i = 0; i < files.length; i++) {
			whereOptions.push({
				field: 'idFile',
				oper: '=',
				value: files[i].id
			});
			whereOptions2.push({
				field: 'idFile',
				oper: '=',
				value: files[i].id
			});
		}
		fileSystem.deleteFileShare([whereOptions]);
		fileSystem.deleteFileFavs([whereOptions2]);
		var update = {
			status: this.statusTypes.TRASH
		};
		var updateFile = fileSystem.updateFile(update, [items.files]);
		var updateFolder = fileSystem.updateFolder(update, [items.folders]);
		return {
			files: updateFile ? files.length : 0,
			folders: updateFolder ? folders.length : 0
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009059',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '92', null, e);
	}
};

/*
	input:
	{
		idFolder: Number,
		idParent: Number
	}
*/
this.moveFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('moveFolder');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		var read = fileSystem.readFolder({
			fields: ['id'],
			where: [
				[{
					field: 'id',
					oper: '=',
					value: object.idFolder
				}, {
					field: 'id',
					oper: '=',
					value: object.idParent
				}], {
					field: 'creationUser',
					oper: '=',
					value: userID
				}
			]
		});
		if (read.length < 2 && object.idParent !== -1) {
			return false;
		}
		return fileSystem.updateFolder({
			id: object.idFolder,
			idParent: object.idParent
		});
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009060',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '91', null, e);
	}
};

/*
	input:
	{
		idFolder: Number,
		idParent: Number
	}
*/
this.copyFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('copyFolder');
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		var read = fileSystem.readFolder({
			join: [{
				table: folderShareModel,
				alias: 'shared',
				fields: ['id'],
				outer: 'left',
				on: [{
					left: 'id',
					right: 'idFolder'
				}]
			}],
			where: [
				[{
					field: 'id',
					oper: '=',
					value: object.idFolder
				}, {
					field: 'id',
					oper: '=',
					value: object.idParent
				}],
				[{
					table: folderShareModel,
					field: 'idUser',
					oper: '=',
					value: userID
				}, {
					field: 'creationUser',
					oper: '=',
					value: userID
				}]
			]
		});
		if (read.length < 2 && object.idParent !== -1) {
			return false;
		}
		var folder = {};
		var tempRead = {};
		for (var i = 0; i < read.length; i++) {
			if (Number(read[i].id) === Number(object.idFolder)) {
				tempRead = read[i];
				var fields = {
					name: read[i].name,
					description: read[i].description,
					status: read[i].status,
					idParent: object.idParent
				};
				folder = fileSystem.createFolder(fields);
			}
		}
		var cascade = function(params) {
			var files = fileSystem.readFile({
				where: [{
					field: 'idFolder',
					oper: '=',
					value: params.idFolder
				}]
			});
			for (i = 0; i < files.length; i++) {
				fileSystem.createFile({
					status: files[i].status,
					idFolder: params.folder.id,
					idObjectType: files[i].idObjectType,
					idObject: files[i].idObject
				});
			}
			var folders = fileSystem.readFolder({
				where: [{
					field: 'idParent',
					oper: '=',
					value: params.idFolder
				}]
			});
			for (i = 0; i < folders.length; i++) {
				var temp = fileSystem.createFolder({
					name: folders[i].name,
					description: folders[i].description,
					status: folders[i].status,
					idParent: params.folder.id
				});
				cascade({
					idFolder: folders[i].id,
					folder: temp
				});
			}
		};
		if (tempRead.creationUser !== userID) {
			cascade({
				idFolder: object.idFolder,
				folder: folder
			});
		}
		return folder;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009061',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '90', null, e);
	}
};

this.getAllItems = function(params) {
	var folders = fileSystem.readFolder({
		fields: ['id'],
		where: [{
			field: 'idParent',
			oper: '=',
			value: params.idFolder
		}]
	});
	params.foldersWhere = [];
	params.filesWhere = [];
	params.foldersWhere.push({
		field: 'id',
		oper: '=',
		value: params.idFolder
	});
	params.filesWhere.push({
		field: 'idFolder',
		oper: '=',
		value: params.idFolder
	});
	for (var j = 0; j < folders.length; j++) {
		params.idFolder = folders[j].id;
		return this.getAllItems(params);
	}
	return {
		folders: params.foldersWhere,
		files: params.filesWhere
	};
};

/*
	input:
	{
		objectType: String,
		shared: Boolean
	}
*/
this.listFolderTree = function(object) {
	object = object || $.request.parameters.get('object');
	var root;
	var shared;
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var folders = this.listFolders({
			idFolder: -1,
			objectType: object.objectType
		});

		root = {
			id: -1,
			name: 'ROOT',
			files: null,
			folders: folders.length > 0 //this.getChildren({ idFolder: -1 })
		};
		if(!object.avoidRootCount){
			root.files = this.getFolderFiles({
				idFolder: -1,
				objectType: object.objectType,
				count: true,
				isRoot: true
			})
		}
		if (object.shared) {
			shared = {
				id: 0,
				name: 'SHARED',
				files: 0,
				folders: this.listSharedFolders(object).length > 0
			};
			return [root, shared];
		}
		return [root];

	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

//this function will do the same as listFolderTree but for many object types
//the object must contain a property objectTypes:ARRAY
this.listFolderTreeByObjectTypes = function(object) {
	try {
		if (object) {
			if (object.hasOwnProperty('objectTypes')) {
				var retVal = {};
				for (var i = 0; i < object.objectTypes.length; i++) {
					var objectType = object.objectTypes[i];
					var folderTree = this.listFolderTree({
						objectType: objectType,
						shared: object.shared,
						avoidRootCount: object.avoidRootCount
					});
					retVal[objectType] = folderTree;
				}
				return retVal;
			}
			return [];
		}
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		idFolder: Number,
		objectType: String
	}
*/
this.listFolders = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		if (Number(object.idFolder) === 0) {
			return this.listSharedFolders(object);
		}
		var idObjectType = this.getIdObjectType(object.objectType);
		var userID = user.getTimpUser().id;
		var join = [];
		var whereOptions = [];
		var folderCount = fileSystem.readFolder({
			count: true,
			where: [{
				field: 'status',
				oper: '=',
				value: 'Active'
			}, {
				field: 'creationUser',
				oper: '=',
				value: userID
			}],
			group_by: ['idParent'],
			simulate: false
		});
		folderCount = folderCount.reduce(function(json, folder) {
			json[folder[0]] = folder[1];
			return json;
		}, {});
		join.push({
			table: fileModel,
			fields: ['id'],
			alias: 'files',
			on: [{
					left: 'id',
					right: 'idFolder'
				}, {
					table: fileModel,
					field: 'idObjectType',
					oper: '=',
					value: idObjectType
				},
				[{
					table: fileModel,
					field: 'status',
					oper: '=',
					value: 'Active'
				}, {
					table: fileModel,
					field: 'status',
					oper: '=',
					value: 'Public'
				}],
				{
					table: fileModel,
					field: 'creationUser',
					oper: '=',
					value: userID
				}
			],
			outer: 'left'
		});
		whereOptions.push({
			field: 'idParent',
			oper: '=',
			value: object.idFolder
		}, {
			field: 'status',
			oper: '=',
			value: 'Active'
		}, {
			field: 'creationUser',
			oper: '=',
			value: userID
		}, { //filter folder by object type
			field: 'idObjectType',
			oper: '=',
			value: idObjectType
		});
		var folders = fileSystem.readFolder({
			fields: ['id', 'name'],
			where: whereOptions,
			join: join,
			simulate: false
		});
		for (var i = 0; i < folders.length; i++) {
			folders[i].files = folders[i].files.length;
			folders[i].folders = folderCount[folders[i].id] !== undefined;
		}
		return folders;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		idFolder: Number,
		idUser: Number || Array,
		idGroup: Number || Array
	}
*/
this.shareFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('shareFolder');
	object = object || $.request.parameters.get('object');
	try {
		var format = this._objectShareFormating(object);
		object = format.object;
		var files = format.files;
		var shares = 0;
		var filesShare = 0;
		for (var i = 0; i < object.idUser.length; i++) {
			shares += fileSystem.createShareFolder(object, i);
			files.idUser = object.idUser[i].id_user;
			files.idGroup = object.idUser[i].id_group;
			for (var j = 0; j < files.file.length; j++) {
				filesShare += fileSystem.createShareFile(files, j);
			}
		}
		return {
			folders: shares,
			files: filesShare
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009063',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '91', null, e);
	}
};

/*
	input:
	{
		idFolder: Number,
		idUser: Number || Array,
		idGroup: Number || Array
	}
*/
// Revisar esta función porque puede que esté mala
this.unshareFolder = function(object) {
	let errorGenerator = new $.ErrorGenerator('unshareFolder');
	object = object || $.request.parameters.get('object');
	try {
		object.unshare = true;
		var format = this._objectShareFormating(object);
		object = format.object;
		var files = format.files && format.files.file ? format.files.file : format.files;
		// 		return format;

		var folderOptions = {
			fields: ['id'],
			where: []
		};
		var fileOptions = {
			fields: ['id', 'creationUser'],
			join: [{
				table: fileModel,
				alias: 'file',
				fields: [],
				on: [{
					left: 'idFile',
					right: 'id'
				}]
			}],
			where: []
		};
		var deleteOptions = [];
		var deleteFolderOptions = [];

		for (var i = 0; i < object.idUser.length; i++) {
			if (folderOptions.where.length === 0) {
				folderOptions.where[0] = [];
			}
			if (fileOptions.where.length === 0) {
				fileOptions.where[0] = [];
			}
			folderOptions.where[0][i] = [{
				field: 'idUser',
				oper: '=',
				value: object.idUser[i].id_user
			}];
			fileOptions.where[0][i] = [{
				field: 'idUser',
				oper: '=',
				value: object.idUser[i].id_user
			}];
			if (object.idUser[i].id_group) {
				folderOptions.where[0][i].push({
					field: 'idGroup',
					oper: '=',
					value: object.idUser[i].id_group
				});
				fileOptions.where[0][i].push({
					field: 'idGroup',
					oper: '=',
					value: object.idUser[i].id_group
				});
			} else {
				folderOptions.where[0][i].push({
					field: 'idGroup',
					oper: 'IS NULL'
				});
				fileOptions.where[0][i].push({
					field: 'idGroup',
					oper: 'IS NULL'
				});
			}
			folderOptions.where[0][i].push({
				field: 'idFolder',
				oper: '=',
				value: object.idFolder
			});
		}

		var folder = fileSystem.readFolder({
			fields: ['creationUser'],
			where: [{
				field: 'id',
				oper: '=',
				value: object.idFolder
			}]
		})[0];
		var updateFilesOptions = [];

		if (files.length) {
			fileOptions.join[0].on[1] = [];
		}
		for (i = 0; i < files.length; i++) {
			fileOptions.join[0].on[1].push({
				field: 'id',
				oper: '=',
				value: files[i].id
			});
			if (files[i].creationUser !== folder.creationUser) {
				updateFilesOptions.push({
					field: 'id',
					oper: '=',
					value: files[i].id
				});
			}
		}
		files = fileOptions.where.length ? fileSystem.readFileShare(fileOptions) : [];
		for (i = 0; i < files.length; i++) {
			if (deleteOptions.length === 0) {
				deleteOptions[0] = [];
			}
			deleteOptions[0].push({
				field: 'id',
				oper: '=',
				value: files[i].id
			});
		}
		var folders = folderOptions.where.length ? fileSystem.readFolderShare(folderOptions) : [];
		for (i = 0; i < folders.length; i++) {
			if (deleteFolderOptions.length === 0) {
				deleteFolderOptions[0] = [];
			}
			deleteFolderOptions[0].push({
				field: 'id',
				oper: '=',
				value: folders[i].id
			});
		}
		var filesCount = files.length;
		var folderCount = folders.length;
		// 		return {
		// 		    filesCount: filesCount,
		// 		    folderCount: folderCount,
		// 		    delFiles: deleteOptions,
		// 		    delFolders: deleteFolderOptions
		// 		};
		var delFiles = filesCount === 0 ? false : fileSystem.deleteFileShare(deleteOptions);
		var delFolders = folderCount === 0 ? false : fileSystem.deleteFolderShare(deleteFolderOptions);
		if (updateFilesOptions.length) {
			fileSystem.updateFile({
				idFolder: -1
			}, [
				updateFilesOptions
			]);
		}
		return {
			files: delFiles ? Number(filesCount) : 0,
			folders: delFolders ? Number(folderCount) : 0
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009064',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '91', null, e);
	}
};

this._objectShareFormating = function(object) {
	if (object.idUser) {
		if (typeof object.idUser === 'number') {
			object.idUser = [{
				id_user: object.idUser,
				id_group: null
			}];
		} else if (typeof object.idUser === 'string') {
			object.idUser = [{
				id_user: Number(object.idUser),
				id_group: null
			}];
		} else if (typeof object.idUser === 'object') {
			object.idUser = object.idUser.map(function(id) {
				return {
					id_user: id,
					id_group: null
				};
			});
		}
		object.unshare = false;
	} else if (object.unshare) {
		var whereOptions = [];
		var tmp = [];
		var t = 0;
		if (object.idFolder) {
			if (typeof object.idFolder === 'number') {
				object.idFolder = [object.idFolder];
			}
			for (t = 0; t < object.idFolder.length; t++) {
				tmp.push({
					field: 'idFolder',
					oper: '=',
					value: object.idFolder[t]
				});
			}
			whereOptions.push(tmp);
		} else if (object.idFile) {
			if (typeof object.idFile === 'number') {
				object.idFile = [object.idFile];
			}
			for (t = 0; t < object.idFile.length; t++) {
				tmp.push({
					field: 'idFile',
					oper: '=',
					value: object.idFile[t]
				});
			}
			whereOptions.push(tmp);
		}
		if (object.idFolder) {
			object.idUser = fileSystem.readFolderShare({
				fields: ['idUser'],
				distinct: true,
				where: whereOptions
			}).map(function(ids) {
				return {
					id_user: ids.idUser,
					id_group: null
				};
			});
		} else {
			object.idUser = fileSystem.readFileShare({
				fields: ['idUser'],
				distinct: true,
				where: whereOptions
			}).map(function(ids) {
				return {
					id_user: ids.idUser,
					id_group: null
				};
			});
		}
	} else {
		object.idUser = [];
	}

	if (object.idGroup) {
		if (typeof object.idGroup === 'number') {
			object.idGroup = [object.idGroup];
		} else if (typeof object.idGroup === 'string') {
			object.idGroup = [Number(object.idGroup)];
		}
	} else if (object.unshare) {
		object.idGroup = fileSystem.readFolderShare({
			fields: ['idGroup'],
			distinct: true,
			where: [{
				field: 'idGroup',
				oper: 'IS NULL',
				not: true
			}, {
				field: 'idFolder',
				oper: '=',
				value: object.idFolder
			}]
		}).map(function(ids) {
			return ids.idGroup;
		});
	} else {
		object.idGroup = [];
	}
	if (object.idUser.length > 1000) {
		throw 'Request options.idUser overflow';
	}
	if (object.idGroup.length > 1000) {
		throw 'Request options.idGroup overflow';
	}

	var usersGroups = [];
	if (object.idGroup.length) {
		usersGroups = this.getUsersFromGroup({
			idGroup: object.idGroup
		});
	}
	for (var i = 0; i < usersGroups.length; i++) {
		object.idUser.push(usersGroups[i]);
	}

	var files = [];
	if (object.idFolder) {
		files = this.listFilesByFolder({
			idFolder: object.idFolder,
			fields: ['id', 'idObjectType', 'idObject', 'creationUser']
		});
	}
	files = {
		file: files
	};
	return {
		object: object,
		files: files
	};
};

/*
	input:
	{
		objectType: String
	}
*/
this.listSharedFolders = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		var userID = user.getTimpUser().id;
		var join = [{
			table: folderShareModel,
			fields: [],
			alias: 'shared',
			on: [{
				left: 'id',
				right: 'idFolder'
			}, {
				table: folderShareModel,
				field: 'idUser',
				oper: '=',
				value: userID
			}]
		}];
		var result = fileSystem.readFolder({
			fields: ['id', 'name'],
			where: [{
				field: 'creationUser',
				oper: '!=',
				value: userID
			}],
			join: join,
			groupBy: ['id', 'name']
		});
		for (var i = 0; i < result.length; i++) {
			result[i].files = this.getFolderFiles({
				idFolder: result[i].id,
				objectType: object.objectType,
				count: true
			});
			result[i].folders = false;
		}
		return result;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		id: Number
	}
*/
this.getChildren = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userID = user.getTimpUser().id;
		return fileSystem.readFolder({
			where: [{
				field: 'idParent',
				oper: '=',
				value: object.idFolder
			}, {
				field: 'status',
				oper: '=',
				value: 'Active'
			}, {
				field: 'creationUser',
				oper: '=',
				value: userID
			}]
		});
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009064',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input:
	{
		idFolder: Number,
		objectType: String
	}
*/
this.getFolderFiles = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var idObjectType = this.getIdObjectType(object.objectType);
		var userID = user.getTimpUser().id;
		var options = {
			distinct: true,
			where: [{
					field: 'idFolder',
					oper: '=',
					value: object.idFolder
				},
				[{
					field: 'status',
					oper: '=',
					value: 'Active'
				}, {
					field: 'status',
					oper: '=',
					value: 'Public'
				}],
				[
					{
						field: 'creationUser',
						oper: '=',
						value: userID
					}
				], {
					field: 'idObjectType',
					oper: '=',
					value: idObjectType
				}
			]
		};
		if (!object.isRoot) {
			options.join = [{
				table: fileShareModel,
				alias: 'shared',
				fields: ['idFile'],
				outer: 'left',
				on: [{
					left: 'id',
					right: 'idFile'
				}]
			}];
			options.where[2].push([{
				table: fileShareModel,
				field: 'idUser',
				oper: '=',
				value: userID
			}, {
				table: fileShareModel,
				field: 'idUser',
				oper: 'IS NULL',
				not: true
			}]);
		}
		var files = fileSystem.readFile(options);
		return object.count ? files.length : files;
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

this.getIdObjectType = function(objectType) {
	try {
		return fileSystem.getIdObjectType(objectType);
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009065',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	input
	{
		idGroup: Number || Array
	}
*/
this.getUsersFromGroup = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		return fileSystem.getUsersFromGroup(object);
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009045',
			errorInfo: util.parseError(e)
		});
	}
};

// Funciones para MKT

this.addUsersToGroup = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var folders = this.getFoldersShareByGroup(object.idGroup);
		var files = this.getFilesShareByGroup(object.idGroup, folders);
		var foldersShare = 0;
		var filesShare = 0;
		for (var i = 0; i < folders.length; i++) {
			var share = this.shareFolder({
				idFolder: folders[i].idFolder,
				idGroup: object.idGroup
			});
			foldersShare += share.folders;
			filesShare += share.files;
		}
		for (i = 0; i < files.length; i++) {
			var fileShare = this.shareFile({
				idGroup: object.idGroup
			});
			filesShare += fileShare.files;
		}
		return {
			files: filesShare,
			folders: foldersShare
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009066',
			errorInfo: util.parseError(e)
		});
	}
};

this.groupDeleted = function(group) {
	group = group || $.request.parameters.get('object');
	try {
		if (typeof group === 'object') {
			group = group.idGroup;
		}
		var files = fileSystem.readFileShare({
			count: true,
			where: [{
				field: 'idGroup',
				oper: '=',
				value: group
			}]
		});
		var folders = fileSystem.readFolderShare({
			count: true,
			where: [{
				field: 'idGroup',
				oper: '=',
				value: group
			}]
		});
		var deleteFiles = fileSystem.deleteFileShare([{
			field: 'idGroup',
			oper: '=',
			value: group
		}]);
		var deleteFolders = fileSystem.deleteFolderShare([{
			field: 'idGroup',
			oper: '=',
			value: group
		}]);
		return {
			files: deleteFiles ? Number(files[0]) : 0,
			folders: deleteFolders ? Number(folders[0]) : 0
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009067',
			errorInfo: util.parseError(e)
		});
	}
};

this.deleteUser = function(object) {
	object = object || $.request.parameters.get('object');
	try {
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var i;
		var options = {
			fields: ['id_group'],
			distinct: true,
			where: []
		};
		if (object.not !== undefined) {
			var temp = [
				[],
				[],
				[],
				[]
			];
			for (var x = 0; x < 4; x++) {
				temp[x].push({
					field: 'idGroup',
					oper: '=',
					value: object.idGroup
				});
			}
			for (var j = 0; j < object.not.length; j++) {
				for (x = 0; x < 4; x++) {
					temp[x].push({
						field: 'idUser',
						oper: '!=',
						value: object.not[j]
					});
				}
			}
			var filesC = fileSystem.readFileShare({
				where: temp[0],
				count: true
			});
			var foldersC = fileSystem.readFolderShare({
				where: temp[1],
				count: true
			});
			var delFiles = fileSystem.deleteFileShare(temp[2]);
			var delFolders = fileSystem.deleteFolderShare(temp[3]);
			return {
				files: delFiles ? Number(filesC[0]) : 0,
				folders: delFolders ? Number(foldersC[0]) : 0
			};
		} else {
			var keys = Object.keys(object);
			for (i = 0; i < keys.length; i++) {
				options.where.push({
					field: keys[i],
					oper: '=',
					value: object[keys[i]]
				});
			}
		}
		var groups = this.getUsersFromGroup({
			options: options
		});
		var deleteOptions = [
			[],
			[],
			[],
			[]
		];
		if (object.id_user) {
			for (i = 0; i < 4; i++) {
				deleteOptions[i].push({
					field: 'idUser',
					oper: '=',
					value: object.id_user
				});
			}
		}
		if (groups.length) {
			for (i = 0; i < 4; i++) {
				deleteOptions[i].push([]);
			}
		}
		var idx = deleteOptions.length - 1;
		for (i = 0; i < groups.length; i++) {
			for (j = 0; j < 4; j++) {
				deleteOptions[j][idx].push({
					field: 'idGroup',
					oper: '=',
					value: groups[i]
				});
			}
		}

		var files = fileSystem.readFileShare({
			count: true,
			where: deleteOptions[0]
		});
		var folders = fileSystem.readFolderShare({
			count: true,
			where: deleteOptions[1]
		});
		var deleteFiles = deleteOptions[2].length ? fileSystem.deleteFileShare(deleteOptions[2]) : false;
		var deleteFolders = deleteOptions[3].length ? fileSystem.deleteFolderShare(deleteOptions[3]) : false;
		return {
			files: deleteFiles ? Number(files[0]) : 0,
			folders: deleteFolders ? Number(folders[0]) : 0
		};
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009068',
			errorInfo: util.parseError(e)
		});
	}
};

this.getFoldersShareByGroup = function(group) {
	try {
		if (group) {
			var options = {
				fields: ['idFolder'],
				distinct: true,
				where: [{
					field: 'idGroup',
					oper: '=',
					value: group
				}]
			};
			return fileSystem.readFolderShare(options);
		}
		throw 'idGroup is Undefined';
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

// Retorna los files que están compartidos para el grupo especificado
// y si folder es enviado retorna los mismos files y que no se encuentren
// en dicho(s) folder(s)
this.getFilesShareByGroup = function(group, folder) {
	try {
		if (group) {
			var options = {
				fields: ['idFile', 'idObjectType', 'idObject'],
				distinct: true,
				where: [{
					field: 'idGroup',
					oper: '=',
					value: group
				}]
			};
			if (folder) {
				options.join = [{
					table: fileModel,
					alias: 'file',
					fields: ['id'],
					on: [{
						left: 'idFile',
						right: 'id'
					}]
				}];
				for (var i = 0; i < folder.length; i++) {
					options.join[0].on.push({
						field: 'idFolder',
						oper: '!=',
						value: folder[i].idFolder
					});
				}
			}
			return fileSystem.readFileShare(options);
		}
		throw 'idGroup is Undefined';
	} catch (e) {
		// return (util.parseError(e));
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009047',
			errorInfo: util.parseError(e)
		});
	}
};

/**
 *  @param {object} object - Endpoint parameter
 *  @param {string} object.objectType - ObjectType from which you want to list files Ex. "DFG::DigitalFile"
 *  @param {boolean | optional} object.counter - Brings counters for all types of files
 *  @param {boolean | optional} object.public - Brings counters for public files
 *  @param {boolean | optional} object.standard - Brings counters for standard files
 *  @param {boolean | optional} object.favorite - Brings counters for favorite files
 *  @param {boolean | optional} object.shared - Brings counters for shared files
 *  @param {boolean | optional} object.trash - Brings counters for trash files
 *  @return {object} response - Returns an object with the count as a property of a file status
 **/
this.getFileCounters = function(object) {
	var response = {};
	try {
		var objectType = this.getIdObjectType(object.objectType);
		var userID = (object.idUser) ? object.idUser : user.getTimpUser().id;
		if (object.counter === true || object.public) {
			response.public = fileSystem.readFile({
				count: true,
				where: [{
					field: 'idObjectType',
					oper: '=',
					value: objectType
				}, {
					field: 'status',
					oper: '=',
					value: 4
				}]
			})[0];
			response.public = parseInt(response.public, 10);
		}
		if (object.counter === true || object.standard) {
			response.standard = fileSystem.readFile({
				count: true,
				where: [{
					field: 'idObjectType',
					oper: '=',
					value: objectType
				}, {
					field: 'status',
					oper: '=',
					value: 0
				}]
			})[0];
			response.standard = parseInt(response.standard, 10);
		}
		if (object.counter === true || object.favorite) {
			response.favorite = fileFavsModel.READ({
				count: true,
				fields: ['id'],
				join: [{
					table: fileModel,
					alias: 'file',
					fields: [],
					on: [{
						left: 'idFile',
						right: 'id'
					}]
				}],
				where: [{
					field: 'idUser',
					oper: '=',
					value: userID
				}, {
					field: 'idObjectType',
					oper: '=',
					value: objectType
				}, {
					table: fileModel,
					field: 'status',
					oper: '=',
					not: true,
					value: [this.statusTypes.DELETED, this.statusTypes.TRASH]
				}]
			})[0];
			response.favorite = parseInt(response.favorite, 10);
		}
		if (object.counter === true || object.shared) {
			response.shared = fileShareModel.READ({
				count: true,
				fields: ['id'],
				join: [{
					table: fileModel,
					alias: 'file',
					fields: [],
					on: [{
						left: 'idFile',
						right: 'id'
					}]
				}],
				where: [{
					field: 'idUser',
					oper: '=',
					value: userID
				}, {
					field: 'idObjectType',
					oper: '=',
					value: objectType
				}, {
					table: fileModel,
					field: 'status',
					oper: '=',
					not: true,
					value: [this.statusTypes.DELETED, this.statusTypes.TRASH]
				}]
			})[0];
			response.shared = parseInt(response.shared, 10);
		}
		if (object.counter === true || object.trash) {
			response.trash = fileSystem.readFile({
				count: true,
				where: [{
					field: 'idObjectType',
					oper: '=',
					value: objectType
				}, {
					field: 'creationUser',
					oper: '=',
					value: userID
				}, {
					field: 'status',
					oper: '=',
					value: 2
				}]
			})[0];
			response.trash = parseInt(response.trash, 10);
		}
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009069',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.updateOldFilesToNewVersion = function() {
	try {
		//remove folders
		var deletedFolders = fileSystem.folder.DELETEWHERE([{
			field: 'idObjectType',
			oper: 'IS NULL'
		}]);
		//update files, assign idFolder = -1
		var updatedFiles = fileSystem.file.UPDATEWHERE({
			idFolder: -1
		}, [{
			field: 'idFolder',
			oper: '!=',
			value: -1
		}]);
		return {
			deletedFolders: deletedFolders,
			updatedFiles: updatedFiles
		};
	} catch (e) {
		return [];
	}
};

this.getFoldersToMove = function(object) {
	try {
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		//get folders by object type and different from currentParent
		var currentFolder = this.getFolder({
			id: object.id
		});
		var idObjectType = currentFolder.idObjectType;
		var idParent = currentFolder.idParent;
		var userId = $.getUserID();
		var whereOptions = [
			{
				field: 'id',
				oper: '!=',
				value: object.id
			},
			{
				field: 'idObjectType',
				oper: '=',
				value: idObjectType
			},
			{
				field: 'idParent',
				oper: '=',
				value: idParent
			},
			{
				field: 'status',
				oper: '=',
				value: 1
			},
			{
				field: 'creationUser',
				oper: '=',
				value: userId
			}
		];
		var foldersMoveTo = fileSystem.folder.READ({
			fields: ['id', 'name'],
			where: whereOptions
		});
		var folderMoveFrom;
		if (idParent === -1) {
			folderMoveFrom = {
				id: '-1',
				name: 'ROOT'
			};
		} else {
			folderMoveFrom = fileSystem.folder.READ({
				fields: ['id', 'name'],
				where: [
					{
						field: 'id',
						oper: '=',
						value: idParent
					},
					{
						field: 'creationUser',
						oper: '=',
						value: userId
					}
				]
			})[0];
			foldersMoveTo.push({
				id: -1,
				name: 'ROOT'
			});
		}
		var response = {
			foldersMoveTo: foldersMoveTo,
			folderMoveFrom: folderMoveFrom
		};
		return response;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

this.getMoveFolderRequiredInfo = function(object) {
	try {
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var response = {};
		var currentFolder = this.getFolder({
			id: object.id
		});
		var folders = this.getFoldersToMove({
			id: object.id
		});
		response.currentFolder = currentFolder;
		response.moveFrom = folders.folderMoveFrom;
		response.moveTo = folders.foldersMoveTo;
		return response;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

//receives and object with id and idObjectType
this.getFoldersToMoveFile = function(object) {
	try {
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		var userId = $.getUserID();
		// 		throw fileSystem.folder.DEFINITION();
		if (object.id) {
			if (object.id != -1) {
				//get folders by object type and different from currentParent
				var currentFolder = this.getFolder({
					id: object.id
				});
				var idObjectType = object.idObjectType;
				var idParent = currentFolder.idParent;
				var whereOptions = [
					{
						field: 'id',
						oper: '!=',
						value: object.id
					},
					{
						field: 'idObjectType',
						oper: '=',
						value: idObjectType
					},
					[{
						field: 'idParent',
						oper: '=',
						value: idParent
					}, {
						field: 'idParent',
						oper: '=',
						value: object.id
					}, {
						field: 'id',
						oper: '=',
						value: idParent
					}],
					{
						field: 'status',
						oper: '=',
						value: 1
					},
					{
						field: 'creationUser',
						oper: '=',
						value: userId
					}
				];
				let foldersMoveTo = fileSystem.folder.READ({
					fields: ['id', 'name'],
					where: whereOptions
					//  ,simulate:true
				});
				// 		throw foldersMoveTo;
				var folderMoveFrom;
				if (idParent === -1) {
					folderMoveFrom = {
						id: currentFolder.id,
						name: currentFolder.name
					};
					foldersMoveTo.push({
						id: -1,
						name: 'ROOT'
					});
				} else {
					folderMoveFrom = fileSystem.folder.READ({
						fields: ['id', 'name'],
						where: [
							{
								field: 'id',
								oper: '=',
								value: idParent
							}
						]
					})[0];
					//  foldersMoveTo.push({id:-1,name:'ROOT'});
				}
				var response = {
					foldersMoveTo: foldersMoveTo,
					folderMoveFrom: folderMoveFrom
				};
				return response;
			} else {
				//get folders by object type and different from currentParent
				let whereOptions = [
					{
						field: 'idObjectType',
						oper: '=',
						value: object.idObjectType
					},
					{
						field: 'idParent',
						oper: '=',
						value: object.id
					},
					{
						field: 'status',
						oper: '=',
						value: 1
					},
					{
						field: 'creationUser',
						oper: '=',
						value: userId
					}
				];
				let foldersMoveTo = fileSystem.folder.READ({
					fields: ['id', 'name'],
					where: whereOptions
				});
				let folderMoveFrom = {
					id: '-1',
					name: 'ROOT'
				};
				let response = {
					foldersMoveTo: foldersMoveTo,
					folderMoveFrom: folderMoveFrom
				};
				return response;
			}
		} else {
			var foldersMoveTo = fileSystem.folder.READ({
				field: ['id', 'name'],
				where: [
					{
						field: 'idObjectType',
						oper: '=',
						value: object.idObjectType
					},
					{
						field: 'status',
						oper: '=',
						value: 1
					},
					{
						field: 'creationUser',
						oper: '=',
						value: userId
					}
				]
			});
			foldersMoveTo.push({
				id: -1,
				name: 'ROOT'
			});
			return {
				foldersMoveTo: foldersMoveTo
			};
		}
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

this.getMoveFileRequiredInfo = function(object) {
	try {
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		//object has id and objectType
		var response = {};
		var idObjectType = this.getIdObjectType(object.objectType);
		var currentFile = fileSystem.file.READ({
			where: [
				{
					field: 'idObjectType',
					oper: '=',
					value: idObjectType
				},
				{
					field: 'idObject',
					oper: '=',
					value: object.id
				}
			]
		})[0];
		var folders = this.getFoldersToMoveFile({
			id: (currentFile ? currentFile.idFolder : undefined),
			idObjectType: idObjectType
		});
		if (!folders.folderMoveFrom) {
			folders.folderMoveFrom = {
				id: 0,
				name: ''
			};
		}
		response.currentFile = currentFile;
		response.moveFrom = folders.folderMoveFrom;
		response.moveTo = folders.foldersMoveTo;
		return response;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
	}
};

//the function needs objectType, id, idParent
this.updateFileParent = function(object) {
	let errorGenerator = new $.ErrorGenerator('updateFileParent');
	try {
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = util.__parse__(object);
		}
		//search the file exists in files
		var idObjectType = this.getIdObjectType(object.objectType);
		var currentFile = fileSystem.file.READ({
			where: [
				{
					field: 'idObject',
					oper: '=',
					value: object.id
				},
				{
					field: 'idObjectType',
					oper: '=',
					value: idObjectType
				}
			]
		})[0];
		if (!currentFile) {
			//create the file
			var created = fileSystem.file.CREATE({
				status: 1,
				idFolder: object.idParent,
				idObjectType: idObjectType,
				idObject: object.id
			});
			return created;
		}
		var id = currentFile.id;
		var options = {
			id: id,
			idFolder: object.idParent
		};
		var response = fileSystem.updateFile(options);
		return response;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			type: 'E',
			code: 'CORE009062',
			errorInfo: util.parseError(e)
		});
		throw errorGenerator.generateError('00', '73', null, e);
	}
};


this.getFileAliases = function () {
	return _filesJoin.aliases();
}

this.getFileJoin = function (object) {
	return _filesJoin.getJoins(object);
};

let _filesJoin = {
	getJoins: function (object) {
		let { folder, status, objectType, user, mainAlias } = object || {};
		mainAlias = mainAlias || 'main';
		let join = [
			this.join.file(folder, status, user, mainAlias),
			this.join.folder(folder),
			this.join.objectType(objectType),
			this.join.favorite(status, user)
		];
		join = join.filter(_join => _join != undefined);
		return join;
	},
	join: {
		file: (folderId, type, user, mainAlis) => {
			let _on = {
				creationUser: {
					alias: 'file',
					field: 'creationUserId',
					operator: '$eq',
					value: user
				},
				idFolder: {
					alias: 'file',
					field: 'idFolder',
					operator: '$eq',
					value: folderId
				},
				status: (status) => {
					let on;
					let statusTypes = core_api.fileSystem.statusTypes;
					
					if(Array.isArray(status)){
						on = {
							alias: 'file',
							field: 'status',
							operator: '$in',
							value: status.map( (_status) => statusTypes[_status] ).filter( _status => _status)
						}  
					} else if (status == 'ACTIVE' || status == 'FAVORITE') {
						on = {
							alias: 'file',
							field: 'status',
							operator: '$in',
							value: [statusTypes.ACTIVE, statusTypes.PUBLIC]
						};
					} else {
						on = {
							alias: 'file',
							field: 'status',
							operator: '$eq',
							value: statusTypes[status] || -1
						};
					}
					return on;
				}
			};

			let join = {
				alias: 'file',
				type: 'inner',
				map: 'file',
				on: [{
					alias: 'file',
					field: 'idObject',
					operator: '$eq',
					value: {
						alias: mainAlis,
						field: 'id'
					}
				}]
			};

			if (folderId == -1 || folderId > 0) {
				join.on.push(_on.idFolder);
			}

			if (['ACTIVE', 'TRASH'].indexOf(type) != -1) {
				join.on.push(_on.creationUser);
			}

			join.on.push(_on.status(type));
			return join;
		},
		folder: (folderId) => {
			if ( !(folderId > 0) ) {
				return undefined;
			}

			let join = {
				alias: 'folder',
				type: 'inner',
				on: [
					{
						alias: 'folder',
						field: 'id',
						operator: '$eq',
						value: {
							alias: 'file',
							field: 'idFolder'
						}
					}
				]
			};
			return join;
		},
		objectType: (name) => ({
			alias: 'objectType',
			type: 'inner',
			on: [
				{
					alias: 'objectType',
					field: 'ID',
					operator: '$eq',
					value: {
						alias: 'file',
						field: 'idObjectType'
					}
				},
				{
					alias: 'objectType',
					field: 'NAME',
					operator: '$eq',
					value: name
				}
			]
		}),
		favorite: (type, user) => ({
			alias: 'favorite',
			type: type === 'FAVORITE' ? 'right' : 'left',
			map: 'favorite',
			on: [
				{
					alias: 'favorite',
					field: 'idFile',
					operator: '$eq',
					value: {
						alias: 'file',
						field: 'id'
					}
				},
				{
					alias: 'favorite',
					field: 'idUser',
					operator: '$eq',
					value: user
				}
			]
		})
	},
	aliases: function () {
		return [
			{
				collection: 'CORE::File',
				name: 'file'
			},
			{
				collection: 'CORE::FileFavs',
				name: 'favorite'
			},
			{
				collection: 'CORE::ObjectType',
				name: 'objectType'
			},
			{
				collection: 'CORE::Folder',
				name: 'folder'
			},
			{
				collection: 'CORE::FileFavs',
				name: 'fileFavs'
			}
		]
	}
};