$.import('timp.core.server.models.tables', 'package');
const packageModel = $.timp.core.server.models.tables.package.packageModel;
const packagePrivilegeModel = $.timp.core.server.models.tables.package.packagePrivilegeModel;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;
const privilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

$.import('timp.core.server.models.tables', 'group');
const groupPackageModel = $.timp.core.server.models.tables.group.groupPackageModel;

$.import('timp.core.server.models.tables', 'user');
const userPackageModel = $.timp.core.server.models.tables.user.userPackageModel;

$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;

$.import('timp.core.server.controllers.refactor', 'component');
const componentCtrl = $.timp.core.server.controllers.refactor.component;
const _this = this;

/**
 *
 **/
_this.savePackage = function(object) {
	var response = {};
	try {
		if (!$.lodash.isNil(object) && !$.lodash.isNil(object.name)) {
			var pkg = {
				name: object.name,
				description: object.description ? object.description : ""
			};

			if ($.lodash.isNil(object.id)) {
				response = packageModel.create(pkg);
			} else {
				response = packageModel.update(pkg, {
					where: [{
						"field": "id",
						"operator": "$eq",
						"value": object.id
                    }]
				},true);
			}

			if ($.lodash.isArray(response.errors) && $.lodash.isEmpty(response.errors)) {
				let packageId = object.id ? object.id : response.results.instance.id;
				packagePrivilegeModel.delete({
					where: [{
						"field": "packageId",
						"operator": "$eq",
						"value": packageId
                        }]
				});

				object.privileges = object.privileges.map(function(privilege) {
					return {
					    packageId: packageId, 
					    privilegeId: privilege
					};
				});
                
                if(!$.lodash.isEmpty(object.privileges)){
				    response.results.privileges = packagePrivilegeModel.batchCreate(object.privileges).results.created;
                }

				$.messageCodes.push({
					"code": "CORE000038",
					"type": 'S'
				});
			} else {
				$.messageCodes.push({
					"code": "CORE000039",
					"type": 'E'
				});
				throw $.generateError('savePackage', { object: '03', category: '1000' });
			}
		} else {
			$.messageCodes.push({
				"code": "CORE000025", //Invalid Object
				"type": 'E'
			});
		}
	} catch (e) {
		$.messageCodes.push({
			"code": "CORE000039",
			"type": 'E',
			"errorInfo": $.parseError(e)
		});
		throw $.generateError('savePackage', { object: '00', category: '5000' }, e);
	}

	return response;
};

_this.listPackages = function(object) {
	var response = {};
	try {
		let options = {
		  //  simulate: true,
			"orderBy": [{
				"field": "name",
				"type": "asc"
            }]
		};

		let whereOptions = [];

		if (!$.lodash.isNil(object)) {
			if (!$.lodash.isNil(object.filters)) {
				if (!$.lodash.isNil(object.filters.name)) {
					whereOptions.push({
						"field": "name",
						"operator": "$contains",
						"value": {
							"value": object.filters.name,
							"fuzzy": {
								"minimumScore": 0.2
							}
						}
					});
				}

				if ($.lodash.isArray(object.filters.createdBy) && !$.lodash.isEmpty(object.filters.createdBy)) {
					whereOptions.push({
						field: "creationUser",
						operator: "$in",
						value: object.filters.createdBy
					});
				}

				if ($.lodash.isArray(object.filters.modifiedBy) && !$.lodash.isEmpty(object.filters.modifiedBy)) {
					whereOptions.push({
						field: "modificationUser",
						operator: "$in",
						value: object.filters.modifiedBy
					});
				}

				if ($.lodash.isArray(object.filters.creationDate) && !$.lodash.isEmpty(object.filters.creationDate)) {
					whereOptions.push({
						"function": {
							"name": "TO_DATE",
							"params": [{
								"field": "creationDate"
                        }]
						},
						"operator": "$gte",
						"value": {
							"function": {
								"name": "TO_DATE",
								"params": [object.filters.creationDate[0]]
							}
						}
					});

					if (!$.lodash.isEmpty(object.filters.creationDate[1])) {
						whereOptions.push({
							"function": {
								"name": "TO_DATE",
								"params": [{
									"field": "creationDate"
                            }]
							},
							"operator": "$lte",
							"value": {
								"function": {
									"name": "TO_DATE",
									"params": [object.filters.creationDate[1]]
								}
							}
						});
					}
				}

				if ($.lodash.isArray(object.filters.modificationDate) && !$.lodash.isEmpty(object.filters.modificationDate)) {
					whereOptions.push({
						"function": {
							"name": "TO_DATE",
							"params": [{
								"field": "modificationDate"
                            }]
						},
						"operator": "$gte",
						"value": {
							"function": {
								"name": "TO_DATE",
								"params": [object.filters.modificationDate[0]]
							}
						}
					});

					if (!$.lodash.isEmpty(object.filters.modificationDate[1])) {
						whereOptions.push({
							"function": {
								"name": "TO_DATE",
								"params": [{
									"field": "modificationDate"
                                }]
							},
							"operator": "$lte",
							"value": {
								"function": {
									"name": "TO_DATE",
									"params": [object.filters.modificationDate[1]]
								}
							}
						});
					}
				}
				options.where = whereOptions;
			}

			if (!$.lodash.isNil(object.orderBy)) {
				options.orderBy = [{
					"field": object.orderBy.field,
					"type": object.orderBy.type.toLowerCase()
			    }];
			}
		}
    
		options.paginate = {
			"limit": 15,
			"offset": object && object.page ? (object.page - 1) * 15 : 0
		};

		response.packages = _this.getDefaultFieldsData(packageModel.find(options).results);
		response.pageCount = Math.ceil(packageModel.find({
			count: true,
			select: [{
				field: "id"
			}]
		}).results[0].tableCount / 15);
		
	} catch (e) {
		throw $.generateError('listPackages', { object: '00', category: '5000' }, e);
	}

	return response;
};

_this.deletePackage = function(packages) {
	var response = {};
	try {

		if (!$.lodash.isNil(packages) && $.lodash.isArray(packages)) {
			packages.forEach(function(pkgId) {
				response = packageModel.delete({
					where: [{
						field: "id",
						operator: "$eq",
						value: pkgId
					}]
				});

				if (response.results.deleted) {
					packagePrivilegeModel.delete({
						where: [{
							field: "packageId",
							operator: "$eq",
							value: pkgId
						}]
					});

					userPackageModel.delete({
						where: [{
							field: "packageId",
							operator: "$eq",
							value: pkgId
						}]
					});

					groupPackageModel.delete({
						where: [{
							field: "packageId",
							operator: "$eq",
							value: pkgId
						}]
					});

					$.messageCodes.push({
						"code": "CORE000036",
						"type": 'S'
					});
				} else {
					$.messageCodes.push({
						"code": "CORE000037",
						"type": 'E',
						"errorInfo": response.error[0]
					});
					throw $.generateError('deletePackage', { object: '03', category: '3000' }, response.error);
				}
			});
		}

	} catch (e) {
		$.messageCodes.push({
			"code": "CORE000037",
			"type": 'E',
			"errorInfo": $.parseError(e)
		});
		throw $.generateError('deletePackage', { object: '00', category: '5000' }, e);
	}
	return response;
};

/**
 * @method getPackageInformation - Returns the details of an specific package 
 * @param {} packageId
 * @return {Object}
 * */
_this.getPackageInformation = function(packageId) {
	var response = {};
	try {
		var lang = $.request.cookies.get("Content-Language");
		packageId = packageId || $.getParam('packageId');
		if (!$.lodash.isNil(packageId)) {

			response.metadata = _this.getDefaultFieldsData(packageModel.find({
				where: [{
					field: "id",
					operator: "$eq",
					value: packageId
				}]
			}).results)[0];

			let privilegesOptions = {
				"aliases": [{
					"collection": privilegeModel.getIdentity(),
					"name": "Privilege"
                }, {
					"collection": packagePrivilegeModel.getIdentity(),
					"name": "PackagePrivilege"
                }, {
					"collection": componentModel.getIdentity(),
					"name": "Component",
					"isPrimary": true
                }],
				"select": [{
						field: "id",
						alias: "Privilege"
					}, {
						field: "name",
						alias: "Privilege",
						"as": "privilegeName"
					}, {
						field: "id",
						alias: "Component",
						"as": "ComponentId"
					}, {
						field: "name",
						alias: "Component",
						"as": "componentName"
					}
                ],
				"join": [{
					alias: "Privilege",
					map: "grantedPrivileges",
					type: "inner",
					on: [{
						alias: "Privilege",
						field: "componentId",
						operator: "$eq",
						value: {
							"alias": "Component",
							"field": "id"
						}
                    }]
                }, {
					alias: "PackagePrivilege",
					type: "inner",
					on: [{
						alias: "PackagePrivilege",
						field: "privilegeId",
						operator: "$eq",
						value: {
							"alias": "Privilege",
							"field": "id"
						}
                    }]
                }],
				where: [{
					field: "packageId",
					alias: "PackagePrivilege",
					operator: "$eq",
					value: packageId
				}]
			};

			if (lang === "enus") {
				privilegesOptions.select.push({
					field: "labelEnus",
					alias: "Component",
					as: "label"
				}, {
					field: "descriptionEnus",
					alias: "Privilege",
					as: "label"
				});
			} else {
				privilegesOptions.select.push({
					field: "labelPtbr",
					alias: "Component",
					as: "label"
				}, {
					field: "descriptionPtbr",
					alias: "Privilege",
					as: "label"
				});
			}

			response.metadata.privileges = componentModel.find(privilegesOptions).results;
		} else {
			$.messageCodes.push({
				"code": "CORE000025", // Invalid Package ID
				"type": 'E'
			});
			throw $.generateError('getPackageInformation', { object: '00', category: '6000' }, { id: 'Invalid field.'});
		}
	} catch (e) {
		$.messageCodes.push({
			"code": "CORE000021",
			"type": 'E',
			"errorInfo": $.parseError(e)
		});
		throw $.generateError('getPackageInformation', { object: '00', category: '5000' }, e);
	}

	return response;
};

_this.getRequiredInformation = function(action, packageId) {
	var response = {};
	try {
		var lang = $.request.cookies.get("Content-Language");
		if (!$.lodash.isNil(action) && $.lodash.isEqual(action, "visualize")) {
			if (!$.lodash.isNil(packageId)) {
				response.metadata = _this.getPackageInformation(packageId).metadata;
			}
		} else {
			response.unassignedPrivileges = [];

			var options = {
				"aliases": [{
					"collection": componentModel.getIdentity(),
					"name": "Component",
					"isPrimary": true
            }, {
					"collection": privilegeModel.getIdentity(),
					"name": "Privilege"
            }],
				"select": [{
						field: "id",
						alias: "Component",
						"as": "componentId"
				}, {
						field: "name",
						alias: "Component",
						"as": "componentName"
				}, {
						field: "id",
						alias: "Privilege",
						"as": "privilegeId"
				},
					{
						field: "name",
						alias: "Privilege",
						"as": "privilegeName"
				}
            ],
				"join": [{
					alias: "Privilege",
					map: "privileges",
					type: "inner",
					on: [{
						alias: "Privilege",
						field: "componentId",
						operator: "$eq",
						value: {
							"alias": "Component",
							"field": "id"
						}
                }]
            }]
			};

			if (lang === "enus") {
				options.select.push({
					field: "labelEnus",
					alias: "Component",
					as: "label"
				}, {
					field: "descriptionEnus",
					alias: "Privilege",
					as: "label"
				});
			} else {
				options.select.push({
					field: "labelPtbr",
					alias: "Component",
					as: "label"
				}, {
					field: "descriptionPtbr",
					alias: "Privilege",
					as: "label"
				});
			}

			if ($.lodash.isEqual(action, "edit") && !$.lodash.isNil(packageId)) {
				options.where = [{
					field: "id",
					alias: "Privilege",
					operator: "$notIn",
					value: {
						"collection": packagePrivilegeModel.getIdentity(),
						"query": {
							"select": [{
								"field": "privilegeId"
					}],
							"where": [{
								"field": "packageId",
								"operator": "$eq",
								"value": packageId
					}]
						}
					}
                }];
				response.metadata = _this.getPackageInformation(packageId).metadata;
			}
			response.unassignedPrivileges = componentModel.find(options).results;
		}
	} catch (e) {
		throw $.generateError('getRequiredInformation', { object: '00', category: '5000' }, e);
	}
	return response;
};

_this.listAdvancedFilters = function() {
	var response = {
		createdBy: [],
		modifiedBy: []
	};
	let creationUserJoin = [];
	let modificationUserJoin = [];

	let userOptions = {
		"aliases": [{
			"collection": userModel.getIdentity(),
			"name": "user",
			"isPrimary": true
        }, {
			"collection": packageModel.getIdentity(),
			"name": "package"
        }],
		"select": [{
			"field": "id",
			"alias": "user"
		}, {
			"field": "name",
			"alias": "user"
		}, {
			"field": "lastName",
			"alias": "user"
		}],
		"join": []
	};

	try {
		creationUserJoin = [{
			"alias": "package",
			"type": "inner",
			"on": [{
				"alias": "package",
				"field": "creationUser",
				"operator": "$eq",
				"value": {
					"alias": "user",
					"field": "id"
				}
			    }]
			}];

		userOptions.join = creationUserJoin;
		response.createdBy = userModel.find(userOptions).results;

		modificationUserJoin = [{
			"alias": "package",
			"type": "inner",
			"on": [{
				"alias": "package",
				"field": "modificationUser",
				"operator": "$eq",
				"value": {
					"alias": "user",
					"field": "id"
				}
			    }]
			}];

		userOptions.join = modificationUserJoin;
		response.modifiedBy = userModel.find(userOptions).results;
	} catch (e) {
		throw $.generateError('listAdvancedFilters', { object: '00', category: '5000' }, e);
	}
	return response;
};

_this.getDefaultFieldsData = function(elements) {
	try {
		let options = {
			select: [{
				"field": "name"
        	}, {
				"field": "lastName"
            }]
		};

		if ($.lodash.isArray(elements) && !$.lodash.isEmpty(elements)) {
			let users = [];
			var usersData = {};

			$.lodash.forEach(elements, function(elem) {
				if (elem.creationUser) {
					users.push(elem.creationUser);
				}
				if (elem.modificationUser) {
					users.push(elem.modificationUser);
				}
			});

			if (!$.lodash.isEmpty(users)) {
				if ($.lodash.isIntArray(users)) {
					options.select.push({
						"field": "id"
					});
					options.where = [{
						"field": "id",
						"operator": "$in",
						"value": $.lodash.uniq(users)
    	            }];

					userModel.find(options).results.map(function(elem) {
						usersData[elem.id] = elem;
					});

				} else if ($.lodash.isStringArray(users)) {
					options.select.push({
						"field": "hanaUser"
					});
					options.where = [{
						"field": "hanaUser",
						"operator": "$in",
						"value": $.lodash.uniq(users)
    	            }];

					userModel.find(options).results.map(function(elem) {
						usersData[elem.hanaUser] = elem;
					});
				}
			}

			$.lodash.forEach(elements, function(elem){
			   elem.creationUser = usersData[elem.creationUser] ? usersData[elem.creationUser] : {};
			   elem.modificationUser = usersData[elem.modificationUser] ? usersData[elem.modificationUser] : {};
			});
		}
	} catch (e) {
		throw $.generateError('getDefaultFieldsData', { object: '00', category: '5000' }, e);
	}
	return elements;
};