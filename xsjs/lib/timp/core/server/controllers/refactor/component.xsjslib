$.import('timp.core.server.libraries.internal', 'util');

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;
const privilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

/**
 * Send "component" parameter to get privileges filtered by component
 * Send "fields" to list those fields.
 * @param {String} component
 * @param {Array} fields - param's format -> [{
	"field": "name", "alias": "Privilege", "as": "privilegeName" (optional)
	},{
		"field": "name", "alias": "Component", "as": "componentName" (optional)
	}]
**/
this.getComponentPrivileges = function(component, fields) {
	let errorGenerator = new $.ErrorGenerator('getComponentPrivilege');
	var response = [];
	try {
		var lang = $.request.cookies.get('Content-Language') === 'enus' ? 'enus' : 'ptbr';
		var options = {
			'aliases': [{
				'collection': componentModel.getIdentity(),
				'name': 'Component',
				'isPrimary': true
			}, {
				'collection': privilegeModel.getIdentity(),
				'name': 'Privilege'
			}],
			'join': [{
				alias: 'Privilege',
				map: 'privileges',
				type: 'inner',
				on: [{
					alias: 'Privilege',
					field: 'componentId',
					operator: '$eq',
					value: {
						'alias': 'Component',
						'field': 'id'
					}
				}]
			}]
		};
		if (!$.lodash.isNil(fields)) {
			options.select = fields;
		} else {
			options.select = [{
					field: 'id',
					alias: 'Component',
					'as': 'componentId'
				}, {
					field: 'name',
					alias: 'Component',
					'as': 'componentName'
				},
				{
					field: 'id',
					alias: 'Privilege',
					'as': 'privilegeId'
				},
				{
					field: 'name',
					alias: 'Privilege',
					'as': 'privilegeName'
				}
			];
			if (lang === 'enus') {
				options.select.push({
					field: 'labelEnus',
					alias: 'Component',
					as: 'label'
				}, {
					field: 'descriptionEnus',
					alias: 'Privilege',
					as: 'label'
				});
			} else {
				options.select.push({
					field: 'labelPtbr',
					alias: 'Component',
					as: 'label'
				}, {
					field: 'descriptionPtbr',
					alias: 'Privilege',
					as: 'label'
				});
			}
		}
		if (!$.lodash.isNil(component)) {
			options.where = [{
				field: 'name',
				alias: 'Component',
				operator: '$eq',
				value: component
			}];
		}
		response = componentModel.find(options).results;
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E',
			'errorInfo': $.parseError(e)
		});
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.getCoreComponentId = function(component, fields) {
	let errorGenerator = new $.ErrorGenerator('getComponentPrivilege');
	let response = null;
	let result = [];
	try {
		var lang = $.request.cookies.get('Content-Language') === 'enus' ? 'enus' : 'ptbr';
		var options = {
			aliases: [{
				collection: componentModel.getIdentity(),
				name: 'Component',
				isPrimary: true
			}],
			select: [{
				alias: 'Component',
				field: 'id'
			}, {
				alias: 'Component',
				field: 'name'
			}],
			where: [{
				alias: 'Component',
				field: 'name',
				operator: '$like',
				value: '%CORE%'
			}],
			orderBy: [{
				alias: 'Component',
				field: 'name',
				type: 'asc'
			}]
		};
		if (lang === 'enus') {
			options.select.push({
				field: 'labelEnus',
				alias: 'Component',
				as: 'label'
			});
		} else {
			options.select.push({
				field: 'labelPtbr',
				alias: 'Component',
				as: 'label'
			});
		}
		result = componentModel.find(options).results;
		response = result && result.length && result[0] && result[0].id ? result[0].id : null;
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE000021',
			type: 'E',
			errorInfo: $.parseError(e)
		});
		throw errorGenerator.internalError(e);
	}
	return response;
};