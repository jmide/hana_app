$.import('timp.core.server.models.tables','componentManual');
const componentManualTable = $.timp.core.server.models.tables.componentManual.table;
const tableSchema = {
    properties: {
        id: {
            required:false,
            type: 'number'
        },
        componentName: {
            required: true,
            type: 'string'
        },
        attachmentId: {
            required: true,
            type: 'number'
        },
        language: {
            required: true,
            type: 'string'
        },
        effectiveDateFrom: {
            required: true,
            type: 'any'
        },
        effectiveDateTo: {
            required: false,
            type: 'any'
        }
    }
};
const PAGE_SIZE = 15;
/*
@param object {Object}
@example {
    id: 1 //optional
}
*/
this.getRequiredInfo = function(object){
    var response = {
        requiredInformation: {
            components: []
        }
    };
    if(object && object.id){
        response.data = this.read({id:object.id});
    }
    $.import('timp.core.server.models', 'components');
    const components = $.timp.core.server.models.components.components;
    response.requiredInformation.components = $.lodash.map(components.READ({
        fields:["name"],
        orderBy: ['name']
    }),function(c){
        return {key:c.name,name:c.name};
    });
    return response;
};
/*
@param object {Object}
@example {
    componentName: "MDR",
    attachmentId: 2,
    language: "ptrbr",
    effectiveDateFrom: "2013-01-01T06:00:00.000Z",
    effectiveDateTo: "2020-12-31T06:00:00.000Z",
}
*/
this.create = function(object){
    var response = null;
    var isValid = $.validator.validate(object,tableSchema);
    if(!isValid.valid){
        throw isValid.errors.join(',');
    }
    var alreadyExists = this.alreadyExists(object);
    if(alreadyExists){
        return {
            alreadyExists: alreadyExists
        };
    }
    response = componentManualTable.create({
        componentName: object.componentName,
        attachmentId: object.attachmentId,
        language: object.language,
        effectiveDateFrom: object.effectiveDateFrom,
        effectiveDateTo: object.effectiveDateTo || null
    }).results.instance;
    if(response){
        response.effectiveDateFrom = response.effectiveDateFrom ? $.moment(response.effectiveDateFrom).format("YYYY-MM-DD") : response.effectiveDateFrom;
        response.effectiveDateTo = response.effectiveDateTo ? $.moment(response.effectiveDateTo).format("YYYY-MM-DD") : response.effectiveDateTo;
        $.import('timp.core.server.models', 'attachments');
        const attachmentsModel = $.timp.core.server.models.attachments;
        var file = attachmentsModel.attachment.READ({
            fields: ['id','name'],
            where: [{field:'id',oper:'=',value:response.attachmentId}]
        })[0];
        if(file){
            response.fileName = file.name;
        }
    }
    return response;
};
/*
@param object {Object}
@example {
    componentName: "MDR",
    language: "ptrbr",
    effectiveDateFrom: "2014-01-01",
    effectiveDateTo: "2014-12-31",
    id:1//optional
}
*/
this.alreadyExists = function(object){
    var where = [
    [
        {alias:'main',field: 'isDeleted',operator: '$eq',value:0},{alias:'main',field: 'isDeleted',operator: '$eq',value:null}
    ],{
        alias: 'main',
        field: 'componentName',
        operator: '$eq',
        value: object.componentName
    },{
        alias: 'main',
        field: 'language',
        operator: '$eq',
        value: object.language
    },{
        alias: 'main',
        field: 'effectiveDateFrom',
        operator: '$lte',
        value: object.effectiveDateFrom
    },[{
        alias: 'main',
        field: 'effectiveDateTo',
        operator: '$gte',
        value: object.effectiveDateFrom
    },{
        alias: 'main',
        field: 'effectiveDateTo',
        operator: '$eq',
        value: null
    }]];
    if(object.id){
        where.push({alias: 'main',field: 'id',operator: '$neq',value:Number(object.id)});
    }
    var response = componentManualTable.find({
        aliases: [{
            collection: componentManualTable.getIdentity(),
            name: 'main',
            isPrimary:true
        }],
        where: where
    }).results[0];
    return response;
};
/*
@param object {Object}
@example {
    id: 1
}
*/
this.read = function(object){
    var response = null;
    response = componentManualTable.find({
        aliases: [{
            collection:componentManualTable.getIdentity(),
            name: 'main',
            isPrimary: true
        }],
        where:this.getWhere(object ? {filterBy:{id: object.id}} : {},'main')
    }).results[0];
    if(response){
        response.effectiveDateFrom = response.effectiveDateFrom ? $.moment(response.effectiveDateFrom).format("YYYY-MM-DD") : response.effectiveDateFrom;
        response.effectiveDateTo = response.effectiveDateTo ? $.moment(response.effectiveDateTo).format("YYYY-MM-DD") : response.effectiveDateTo;
        $.import('timp.core.server.models', 'attachments');
        const attachmentsModel = $.timp.core.server.models.attachments;
        var file = attachmentsModel.attachment.READ({
            fields: ['id','name'],
            where: [{field:'id',oper:'=',value:response.attachmentId}]
        })[0];
        if(file){
            response.fileName = file.name;
        }
    }
    return response;
};
/*
@param object {Object}
@example {
    id:1,
    componentName: "MDR",
    effectiveDateFrom: "2014-01-01",
    effectiveDateTo: "2014-12-31",
    creationUser: 6,
    creationDate: ["2014-01-01","2014-12-31"],
    modificationUser: 6,
    modificationDate: ["2014-01-01","2014-12-31"]
}
@param alias {string}
*/
this.getWhere = function(object,alias){
    var where = [[{alias:alias,field:'isDeleted',operator:'$eq',value:0},{alias:alias,field:'isDeleted',operator:'$eq',value:null}]];
    if(object && object.filterBy){
        if(object.filterBy.id){
            where.push({alias: alias,field:'id',operator: '$eq',value:Number(object.filterBy.id)});
        }
        if(object.filterBy.componentName){
            where.push({alias: alias,field:'componentName',operator: '$eq',value:object.filterBy.componentName});
        }
        if(object.filterBy.language){
            where.push({alias: alias,field:'language',operator: '$eq',value:object.filterBy.language});
        }
        if(object.filterBy.effectiveDateFrom){
            where.push({alias: alias,field:'effectiveDateFrom',operator: '$eq',value:object.filterBy.effectiveDateFrom});
        }
        if(object.filterBy.effectiveDateTo){
            where.push({alias: alias,field:'effectiveDateTo',operator: '$eq',value:object.filterBy.effectiveDateTo});
        }
        if(object.filterBy.creationUser){
            where.push({alias: alias,field:'creationUser',operator: '$eq',value:object.filterBy.creationUser});
        }
        if(object.filterBy.creationDate){
            where.push({alias: alias,field:'creationDate',operator: '>=',value:object.filterBy.creationDate[0]});
            where.push({alias: alias,field:'creationDate',operator: '<=',value:object.filterBy.creationDate[1]});
        }
        if(object.filterBy.modificationUser){
            where.push({alias: alias,field:'modificationUser',operator: '$eq',value:object.filterBy.modificationUser});
        }
        if(object.filterBy.modificationDate){
            where.push({alias: alias,field:'modificationDate',operator: '>=',value:object.filterBy.modificationDate[0]});
            where.push({alias: alias,field:'modificationDate',operator: '<=',value:object.filterBy.modificationDate[1]});
        }
    }
    return where;
};
/*
@param object {Object}
@example {
    id: 1,
    componentName: "MDR",
    attachmentId: 2,
    language: "ptrbr",
    effectiveDateFrom: "2013-01-01T06:00:00.000Z",
    effectiveDateTo: "2020-12-31T06:00:00.000Z",
}
*/
this.update = function(object){
    var response = null;
    var isValid = $.validator.validate(object,tableSchema,{
        additionalProperties: true
    });
    if(!isValid.valid){
        throw isValid.errors.join(',');
    }
    var alreadyExists = this.alreadyExists(object);
    if(alreadyExists){
        return {
            alreadyExists: alreadyExists
        };
    }
    response = componentManualTable.update({
        componentName: object.componentName,
        attachmentId: object.attachmentId,
        lang: object.lang,
        effectiveDateFrom: object.effectiveDateFrom,
        effectiveDateTo: object.effectiveDateTo
    },{
        where: [{field:'id',operator:'$eq',value:object.id}]
    },true).results.instances[0];
    if(response){
        response.effectiveDateFrom = response.effectiveDateFrom ? $.moment(response.effectiveDateFrom).format("YYYY-MM-DD") : response.effectiveDateFrom;
        response.effectiveDateTo = response.effectiveDateTo ? $.moment(response.effectiveDateTo).format("YYYY-MM-DD") : response.effectiveDateTo;
        $.import('timp.core.server.models', 'attachments');
        const attachmentsModel = $.timp.core.server.models.attachments;
        var file = attachmentsModel.attachment.READ({
            fields: ['id','name'],
            where: [{field:'id',oper:'=',value:response.attachmentId}]
        })[0];
        if(file){
            response.fileName = file.name;
        }
    }
    response.tracker = object.tracker;
    return response;
};
/*
@param object {Object}
@example {
    id: 1,//or [1]
}
*/
this.delete = function(object){
    var response = {
        deleted: false
    };
    var isValid = $.validator.validate(object,{
        properties:{
            id: {
                required: true,
                type: ['number','array']
            }
        }
    });
    if(!isValid.valid){
        throw isValid.errors.join(',');
    }
    var deleted = componentManualTable.update({
        isDeleted: 1
    },{
        where: [{field: 'id', operator: $.lodash.isArray(object.id) ? '$in' : '$eq', value: object.id}]
    });
    response.deleted = deleted.results.updated;
    return response;
};
/*
@param object {Object}
@example {
    page:1,
    filterBy:{
        id:1,
        componentName: "MDR",
        effectiveDateFrom: "2014-01-01",
        effectiveDateTo: "2014-12-31",
        creationUser: 6,
        creationDate: ["2014-01-01","2014-12-31"],
        modificationUser: 6,
        modificationDate: ["2014-01-01","2014-12-31"]
    }
}
*/
this.list = function(object){
    var response = {
        data: [],
        pages: 1
    };
    var page = 1;
    if(object && object.page){
        page = Number(object.page);
    }
    var countResult = componentManualTable.find({
        alias: "main",
        select: [{
            aggregation:{
                type: 'COUNT',
                param: {
                    alias:'main',
                    field:'id'
                }
            },
            as: 'count',
            alias:'main'
        }],
        where: this.getWhere(object,"main")
    });
    if(countResult.results.length > 0){
        response.pages = Math.ceil(countResult.results[0].count / PAGE_SIZE);
    }
    response.data = $.lodash.map(componentManualTable.find({
        aliases: [{
            collection: componentManualTable.getIdentity(),
            name: 'main',
            isPrimary: true
        },{
            collection: "CORE::USER",
            name: 'creationUser'
        },{
            collection: "CORE::USER",
            name: 'modificationUser'
        }],
        where: this.getWhere(object,'main'),
        select: [{
            alias: 'main',
            field: 'id'
        },{
            alias: 'main',
            field: 'componentName'
        },{
            alias: 'main',
            field: 'language'
        },{
            alias: 'main',
            field: 'effectiveDateFrom'
        },{
            alias: 'main',
            field: 'effectiveDateTo'
        },{
            alias: 'main',
            field: 'attachmentId'
        },{
            alias: 'creationUser',
            field: 'name'
        },{
            alias: 'creationUser',
            field: 'lastName'
        },{
            alias: 'main',
            field: 'creationDate'
        },{
            alias: 'modificationUser',
            field: 'name'
        },{
            alias: 'modificationUser',
            field: 'lastName'
        },{
            alias: 'main',
            field: 'modificationDate'
        }],
        join:[{
            alias: 'creationUser',
            type: 'inner',
            map: 'creationUserData',
            on: [{
                alias: 'creationUser',
                field: 'id',
                operator: '$eq',
                value: {
                    alias: 'main',
                    field: 'creationUser'
                }
            }]
        },{
            alias: 'modificationUser',
            type: 'inner',
            map: 'modificationUserData',
            on: [{
                alias: 'modificationUser',
                field: 'id',
                operator: '$eq',
                value: {
                    alias: 'main',
                    field: 'modificationUser'
                }
            }]
        }],
        paginate: {
            limit: PAGE_SIZE,
            offset: (page - 1) * PAGE_SIZE
        },
        orderBy: [{
            alias: 'main',
            field: 'id',
            type: 'asc'
        }]
    }).results,function(e){
        e.effectiveDateFrom = e.effectiveDateFrom ? $.moment(e.effectiveDateFrom).format("YYYY-MM-DD") : e.effectiveDateFrom;
        e.effectiveDateTo = e.effectiveDateTo ? $.moment(e.effectiveDateTo).format("YYYY-MM-DD") : e.effectiveDateTo;
        return e;
    });
    if(response.data){
        response.data = $.lodash.map(response.data,function(e){
            e.effectiveDateFrom = e.effectiveDateFrom ? $.moment(e.effectiveDateFrom).format("YYYY-MM-DD") : e.effectiveDateFrom;
            e.effectiveDateTo = e.effectiveDateTo ? $.moment(e.effectiveDateTo).format("YYYY-MM-DD") : e.effectiveDateTo;
            return e;
        });
    }
    return response;
};
this.listAdvancedFilters = function(){
    var response = {
        componentName: [],
        language: [],
        creationUser: [],
        modificationUser: []
    };
    let lang = $.request.cookies.get('Content-Language');
    lang = lang === "enus" ? "enus" : "ptrbr";
    response.componentName = componentManualTable.find({
        distinct: true,
        select: [{
            field: 'componentName',
            as: 'key'
        },{
            field: 'componentName',
            as: 'name'
        }],
        where: [[{field:'isDeleted',operator: '$eq',value: 0},{field:'isDeleted',operator: '$eq',value: null}]],
        orderBy: [{
            field: 'componentName',
            type: 'asc'
        }]
    }).results;
    response.language = componentManualTable.find({
        distinct: true,
        select: [{
            field: 'language',
            as: 'key'
        },{
            "case": [{
                conditions:[{
                    literal: {
                        type: 'string',
                        value: lang
                    },
                    operator: '$eq',
                    value: 'ptrbr'
                },{
                    field: 'language',
                    operator: '$eq',
                    value: 'ptrbr'
                }],
                value: "Português"
            },{
                conditions:[{
                    literal: {
                        type: 'string',
                        value: lang
                    },
                    operator: '$eq',
                    value: 'enus'
                },{
                    field: 'language',
                    operator: '$eq',
                    value: 'ptrbr'
                }],
                value: "Portuguese"
            },{
                conditions:[{
                    literal: {
                        type: 'string',
                        value: lang
                    },
                    operator: '$eq',
                    value: 'ptrbr'
                },{
                    field: 'language',
                    operator: '$eq',
                    value: 'enus'
                }],
                value: "Inglês"
            },{
                conditions:[{
                    literal: {
                        type: 'string',
                        value: lang
                    },
                    operator: '$eq',
                    value: 'enus'
                },{
                    field: 'language',
                    operator: '$eq',
                    value: 'enus'
                }],
                value: "English"
            }],
            as: 'name'
        }],
        where: [[{field:'isDeleted',operator: '$eq',value: 0},{field:'isDeleted',operator: '$eq',value: null}]],
        orderBy: [{
            field: 'language',
            type: 'asc'
        }]
    }).results;
    response.creationUser = componentManualTable.find({
        aliases:[{
            collection: componentManualTable.getIdentity(),
            name: 'main',
            isPrimary:true
        },{
            collection: 'CORE::USER',
            name: 'creationUser'
        }],
        select: [
            {alias: 'main',field:'creationUser', as:'key'},
            {
                'function':{
                    name: 'CONCAT',
                    params: [{
                        alias:'creationUser',
                        field:'name'
                    },{
                        'function': {
                            name: 'CONCAT',
                            params:[' ',{
                                alias: 'creationUser',
                                field: 'lastName'
                            }]
                        }
                    }]
                },
                alias:'main',
                as: 'name'
            }
        ],
        distinct: true,
        where:[
            [{alias: 'main',field:'isDeleted',operator:'$eq',value:0},{alias: 'main',field:'isDeleted',operator:'$eq',value:null}],
            {alias:'main',field:'creationUser',operator:'$neq',value:null}
        ],
        join: [{
            alias: 'creationUser',
            type: 'inner',
            on:[{
                alias: 'creationUser',
                field: 'id',
                operator: '$eq',
                value: {
                    alias: 'main',
                    field: 'creationUser'
                }
            }]
        }]
    }).results;
    response.modificationUser = componentManualTable.find({
            aliases:[{
                collection: componentManualTable.getIdentity(),
                name: 'main',
                isPrimary:true
            },{
                collection: 'CORE::USER',
                name: 'modificationUser'
            }],
            select: [
                {alias: 'main',field:'modificationUser', as:'key'},
                {
                    'function':{
                        name: 'CONCAT',
                        params: [{
                            alias:'modificationUser',
                            field:'name'
                        },{
                            'function': {
                                name: 'CONCAT',
                                params:[' ',{
                                    alias: 'modificationUser',
                                    field: 'lastName'
                                }]
                            }
                        }]
                    },
                    alias:'main',
                    as: 'name'
                }
            ],
            distinct: true,
            where:[
                [{alias: 'main',field:'isDeleted',operator:'$eq',value:0},{alias: 'main',field:'isDeleted',operator:'$eq',value:null}],
                {alias:'main',field:'modificationUser',operator:'$neq',value:null}
            ],
            join: [{
                alias: 'modificationUser',
                type: 'inner',
                on:[{
                    alias: 'modificationUser',
                    field: 'id',
                    operator: '$eq',
                    value: {
                        alias: 'main',
                        field: 'modificationUser'
                    }
                }]
            }]
        }).results;
    return response;
};
/*
@param object.componentName 
@param object.language
*/
this.getComponentManual = function(object){
    var response = null;
    var today = new Date();
    var componentManual = componentManualTable.find({
        where: [
            [{alias: 'main',field:'isDeleted',operator:'$eq',value:0},{alias: 'main',field:'isDeleted',operator:'$eq',value:null}
        ],{
            "function":{
                name: "UPPER",
                params:[{
                    field: 'componentName'
                }]
            },
            operator: '$eq',
            value: object.componentName.toUpperCase()
        },{
            field: 'language',
            operator: '$eq',
            value: object.language
        },{
            field: 'effectiveDateFrom',
            operator: '$lte',
            value: today.toISOString()
        },[{
            field: 'effectiveDateTo',
            operator: '$gte',
            value: today.toISOString()
        },{
            field: 'effectiveDateTo',
            operator: '$eq',
            value: null
        }]]
    }).results[0];
    if(!$.lodash.isNil(componentManual)){
        //get file from CORE::Attachments
        $.import('timp.core.server.models', 'attachments');
        const attachments = $.timp.core.server.models.attachments;
        response = attachments.attachment.READ({
            fields: ['id','name','file','type'],
            where:[{field:'id',oper:'=',value:componentManual.attachmentId}]
        })[0];
    }
    return response;
};