$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;
const userPrivilegeModel = $.timp.core.server.models.tables.user.userPrivilegeModel;
const userOrgPrivilegeModel = $.timp.core.server.models.tables.user.userOrgPrivilegeModel;
const userPackageModel = $.timp.core.server.models.tables.user.userPackageModel;

$.import('timp.core.server.models.tables', 'package');
const packageModel = $.timp.core.server.models.tables.package.packageModel;
const packageModelFile = $.timp.core.server.models.tables.package;

$.import('timp.core.server.models.tables', 'group');
const groupModelFile = $.timp.core.server.models.tables.group;
const groupModel = groupModelFile.groupModel;
const groupUserModel = groupModelFile.groupUserModel;

$.import('timp.core.server.models.views', 'userPrivilege');
const userPrivilegeViewModel = $.timp.core.server.models.views.userPrivilege.userPrivilegeView;
const orgPrivilegesViewModel = $.timp.core.server.models.views.userPrivilege.orgPrivilegesView;

$.import('timp.core.server.models.tables', 'component');
const newComponentModel = $.timp.core.server.models.tables.component.componentModel;
const newPrivilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

$.import('timp.core.server.controllers.refactor', 'license');
const licenseCtrl = $.timp.core.server.controllers.refactor.license;


$.import('timp.core.server.controllers.refactor', 'component');
const componentCtrl = $.timp.core.server.controllers.refactor.component;
const _ = $.lodash;
const _this = this;

_this.getOrgPlaceHolders = function () {
    let placeholders = $.getViewPlaceholder(['IP_MANDANTE', 'IP_DATE_FROM', 'IP_LANGUAGE', 'IP_YEAR_FROM', 'IP_YEAR2_FROM'], true);
    return _.map(placeholders, function (value, name) {
        return { name: name, values: [value] };
    });
};

const _saveUserSchema = {
    properties: {
        userId: {
            type: 'integer',
            allowEmpty: false
        },
        createHanaUser: {
            type: 'boolean',
            allowEmpty: true
        },
        name: {
            type: 'string',
            required: true,
            allowEmpty: false
        },
        lastName: {
            type: 'string',
            required: true,
            allowEmpty: false
        },
        hanaUser: {
            type: 'string',
            required: true,
            allowEmpty: false
        },
        email: {
            type: 'string',
            allowEmpty: true
        },
        position: {
            type: 'string',
            allowEmpty: true
        },
        isActive: {
            type: 'boolean',
            required: true,
            allowEmpty: false
        },
        isAdmin: {
            type: 'boolean',
            required: true,
            allowEmpty: false
        },
        componentsPrivileges: {
            type: 'array',
            required: true,
            allowEmpty: true
        },
        packages: {
            type: 'array',
            required: true,
            allowEmpty: true
        },
        orgPrivileges: {
            type: 'array',
            required: true,
            allowEmpty: true
        },
        groups: {
            type: 'array',
            required: true,
            allowEmpty: true
        },
        taxes: {
            type: 'array',
            required: true,
            allowEmpty: true
        }
    }
};

/**
    object: {
        user: "SGARC" (mandatory),
        component: "BRB"(optional),
        applyFormat: true (optional)
    }
**/
_this.getUserPrivileges = function (object) {
    var response = {};
    try {
        if ( object != undefined ) {
            var user = object.user.toUpperCase();
            var userPrivileges = [];
            var query = 'SELECT DISTINCT COMPONENT.NAME AS "componentName", PRIVILEGE.ID AS "privilegeId", PRIVILEGE.NAME AS "privilegeName" ' +
                'FROM ' + $.schema + '."CORE::PRIVILEGE" AS PRIVILEGE ' +
                'INNER JOIN "PUBLIC"."EFFECTIVE_ROLES" AS ROLES ' +
                'ON PRIVILEGE.ROLE = ROLES.ROLE_NAME ' +
                'INNER JOIN ' + $.schema + '."CORE::COMPONENT" AS COMPONENT ' +
                'ON PRIVILEGE.COMPONENT_ID = COMPONENT.ID ' +
                'WHERE USER_NAME = \'' + user + '\' ';
            var viewOptions = {
                // simulate: true,
                select: [{
                    field: 'privilegeName'
                }, {
                    field: 'privilegeId'
                }, {
                    field: 'componentName'
                }],
                where: [{
                    field: 'hanaUser',
                    operator: '$eq',
                    value: user
                }]
            };

            if (object.component != undefined ) {
                var componentId = newComponentModel.find({
                    select: [{
                        field: 'id'
                    }],
                    where: [{
                        field: 'name',
                        operator: '$eq',
                        value: object.component.toUpperCase()
                    }]
                }).results[0].id;

                var componentPrivileges = newPrivilegeModel.find({
                    select: [{
                        field: 'name'
                    }],
                    where: [{
                        field: 'componentId',
                        operator: '$eq',
                        value: componentId
                    }]
                }).results.map(function (row) {
                    return row.name;
                });

                query += 'AND COMPONENT_ID = ' + componentId;

                viewOptions.where.push({
                    field: 'componentName',
                    operator: '$eq',
                    value: object.component
                });
            }
            
            if(object.nameStartSpecificPrivilege){
                viewOptions.where.push({
                    field: 'privilegeName',
                    operator: '$like',
                    value: object.nameStartSpecificPrivilege + "%"
                });
            }
            
            // GRC vs TIMP privileges
            if ($.userHasGrcPrivileges(user)) {
                userPrivileges = $.execute(query).results;
            } else {
                userPrivileges = userPrivilegeViewModel.find(viewOptions).results;
            }

            //Format Privileges
            if (!$.lodash.isEmpty(userPrivileges) && object.applyFormat != undefined) {
                userPrivileges = userPrivileges.map(function (row) {
                    return row.privilegeName;
                });
                if (!$.lodash.isEmpty(componentPrivileges)) {
                    componentPrivileges.forEach(function (cPriv) {
                        var privName = cPriv.split('.')[0];
                        var privAction = cPriv.split('.')[1];

                        if ($.lodash.indexOf(userPrivileges, cPriv) !== -1) {
                            if ($.lodash.isNil(privAction)) {
                                response[privName] = true;
                            } else {
                                if (!$.lodash.isNil(response[privName])) {
                                    response[privName][privAction] = true;
                                } else {
                                    response[privName] = {};
                                    response[privName][privAction] = true;
                                }
                            }
                        } else {
                            if ($.lodash.isNil(privAction)) {
                                response[privName] = false;
                            } else {
                                if (!$.lodash.isNil(response[privName])) {
                                    response[privName][privAction] = false;
                                } else {
                                    response[privName] = {};
                                    response[privName][privAction] = false;
                                }
                            }
                        }
                    });
                } else {
                    $.messageCodes.push({
                        'code': 'CORE000025', // Invalid Component
                        'type': 'E'
                    });
                }
            } else {
                response = userPrivileges;
            }
        }
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000021',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }
    return response;
};

_this.getUserOrgPrivileges = function (hanaUser) {
    var response = {};
    try {
        var user = hanaUser;
        if (!$.lodash.isNil(user)) {
            var orgPrivileges = [];
            if ($.userHasGrcPrivileges(hanaUser)) {
                var orgPrivQuery = 'SELECT DISTINCT OBJECT_NAME FROM "PUBLIC"."EFFECTIVE_PRIVILEGES" WHERE USER_NAME = \'' + user +
                    '\' AND OBJECT_NAME LIKE \'%TIMP_EMPRESA%\'';

                orgPrivileges = $.execute(orgPrivQuery).results.map(function (priv) {
                    var privName = priv.OBJECT_NAME;
                    return privName.substr(privName.lastIndexOf('EMPRESA') + 8);
                });
                let validate = [];
                orgPrivileges.forEach(function (currElem) {
                    var company = currElem.split('_')[0];
                    var state = currElem.split('_')[2];
                    var branch = currElem.split('_')[4];
                    if (validate.indexOf(company + '_' + state + '_' + branch) == -1) {
                        validate.push(company + '_' + state + '_' + branch);
                        if (!$.lodash.isNil(response[company])) {
                            response[company].branches.push({
                                state: state,
                                id: branch
                            });
                        } else {
                            response[company] = {
                                branches: []
                            };
                            response[company].branches.push({
                                state: state,
                                id: branch
                            });
                        }
                    }
                });
            } else {
                orgPrivileges = orgPrivilegesViewModel.find({
                    aliases: [{
                        collection: orgPrivilegesViewModel.getIdentity(),
                        name: 'OrgPriView',
                        isPrimary: true
                    }],
                    placeholders: [{
                        alias: 'OrgPriView',
                        params: _this.getOrgPlaceHolders()
                    }],

                    select: [{
                        field: 'companyId'
                    }, {
                        field: 'stateId'
                    }, {
                        field: 'branchId'
                    }],
                    where: [{
                        field: 'hanaUser',
                        operator: '$eq',
                        value: user
                    }]
                }).results;


                orgPrivileges = orgPrivileges || [];
                // return orgPrivileges;
                orgPrivileges.forEach(function (row) {
                    if (!$.lodash.isNil(response[row.companyId])) {
                        response[row.companyId].branches.push({
                            state: row.stateId,
                            id: row.branchId
                        });
                    } else {
                        response[row.companyId] = {
                            branches: []
                        };
                        response[row.companyId].branches.push({
                            state: row.stateId,
                            id: row.branchId
                        });
                    }
                });
            }
        } else {
            $.messageCodes.push({
                'code': 'CORE000025', //Invalid User
                'type': 'E'
            });
        }
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000021',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }
    return response;
};



/**
 * object: {
    id: NN,
    hanaUser: XXXX,
    name: XXXX,
    lastName: XXXX,
    email: XXXX,
    position: XXXX,
    isActive: true | false,
    isAdmin: true | false
    componentsPrivileges: [1,2,..,73],
    packages: [1,2,3..],
    orgPrivileges: [{
        companyId: "1000",
        branchId: "0001"
    },{
        companyId: "1010",
        branchId: "0003"
    }],
    groups: [1,2,...,6],
    taxes: ['00', '01', .... , '05']
    createHanaUser:true | false
 }
**/
this.saveUser = function (object) {
    var response = {
        user: {}
    };
    try {
        var isValid = $.validator.validate(object, _saveUserSchema);
        if ($.lodash.isNil(object) || $.lodash.isNil(object.hanaUser) || !isValid.valid) {
            $.messageCodes.push({
                'code': 'CORE000024', // Invalid Object
                'type': 'E'
            });
            return response;
        } else {
            //Create Object
            let hanaUserQuery = 'SELECT USER_NAME, USER_DEACTIVATED FROM PUBLIC."USERS" WHERE USER_NAME = \'' + object.hanaUser + '\'';
            let verifyHanaUser = $.execute(hanaUserQuery).results; // Search in HANA, the hana user sent in the object

            let verifyCoreUserByHanaName = userModel.find({ // Search in CORE::User, the hana user sent in the object
                count: true,
                where: [{
                    field: 'hanaUser',
                    operator: '$eq',
                    value: object.hanaUser
                }]
            }).results[0];

            let availableLicense = licenseCtrl.checkAvailableLicenses(1); // True | False if License has available users 

            if ($.lodash.isNil(object.userId)) {
                if (object.createHanaUser) {
                    if ($.session.hasSystemPrivilege('ROLE ADMIN') && $.session.hasSystemPrivilege('USER ADMIN')) {

                        if ($.lodash.isEmpty(verifyHanaUser) && $.lodash.isEqual(verifyCoreUserByHanaName.tableCount, 0)) {
                            if (availableLicense) {
                                let createHanaUserQuery = 'CREATE USER ' + object.hanaUser + ' PASSWORD Initial1';
                                let executeCreateHanaUser = $.execute(createHanaUserQuery, null, true);

                                if ($.lodash.isArray(executeCreateHanaUser.errors) && $.lodash.isEmpty(executeCreateHanaUser.errors)) {
                                    let grantTimpRoleQuery = 'GRANT "timp.core.server::timp" TO ' + object.hanaUser;
                                    $.execute(grantTimpRoleQuery, null, true);
                                    response = userModel.create(object);

                                    if (response.results.created) {
                                        $.messageCodes.push({
                                            'code': 'CORE000026', // User successfully created
                                            'type': 'S'
                                        });
                                    } else {
                                        $.messageCodes.push({
                                            'code': 'CORE000027', // Error while trying to create the user
                                            'type': 'E'
                                        });
                                        return response;
                                    }
                                }
                            } else {
                                $.messageCodes.push({
                                    'code': 'CORE000028', // No available licenses
                                    'type': 'E'
                                });
                                return response;
                            }
                        } else {
                            $.messageCodes.push({
                                'code': 'CORE000033', // Hana name ya existe en hana o core::user
                                'type': 'E'
                            });
                            return response;
                        }
                    } else {
                        $.messageCodes.push({
                            'code': 'CORE000008', // Insufficient Privileges - ROLE ADMIN, USER ADMIN
                            'type': 'E'
                        });
                        return response;
                    }
                } else {
                    if (!$.lodash.isEmpty(verifyHanaUser)) {
                        if ($.lodash.isEqual(verifyCoreUserByHanaName.tableCount, 0)) {
                            response = userModel.create(object);

                            if (response.results.created) {
                                $.messageCodes.push({
                                    'code': 'CORE000026', // User successfully created
                                    'type': 'S'
                                });
                            } else {
                                $.messageCodes.push({
                                    'code': 'CORE000027', // Error while trying to create the user
                                    'type': 'E'
                                });
                                return response;
                            }
                        } else {
                            $.messageCodes.push({
                                'code': 'CORE000029', // user already exists in core
                                'type': 'E'
                            });
                            return response;
                        }
                    } else {
                        $.messageCodes.push({
                            'code': 'CORE000030', // HANA User is invalid or does not exist
                            'type': 'E'
                        });
                        return response;
                    }
                }
            } else {
                let verifyCoreUserById = userModel.find({ // Search in CORE::User the userId sent in the object
                    select: [{
                        field: 'isActive'
                    }],
                    where: [{
                        field: 'id',
                        operator: '$eq',
                        value: object.userId
                    }]
                }).results;

                if ($.lodash.isEqual(verifyCoreUserById.length, 1)) {
                    if (object.isActive === true && verifyCoreUserById[0].isActive === false) {
                        if (verifyHanaUser[0].USER_DEACTIVATED == 'FALSE') {
                            if (availableLicense) {
                                response = userModel.update(object, {
                                    where: [{
                                        field: 'id',
                                        operator: '$eq',
                                        value: object.userId
                                    }]
                                }, true);

                                if (response.results.updated) {
                                    $.messageCodes.push({
                                        'code': 'CORE000031', // User updated successfully
                                        'type': 'S'
                                    });
                                } else {
                                    $.messageCodes.push({
                                        'code': 'CORE000032', // Error while updating 
                                        'type': 'E'
                                    });
                                    return response;
                                }
                            } else {
                                $.messageCodes.push({
                                    'code': 'CORE000028', // No available licenses
                                    'type': 'E'
                                });
                                return response;
                            }
                        } else {
                            $.messageCodes.push({
                                'code': 'CORE000030', // Hana user is deactivated
                                'type': 'E'
                            });
                            return response;
                        }
                    } else {
                        response = userModel.update(object, {
                            where: [{
                                field: 'id',
                                operator: '$eq',
                                value: object.userId
                            }]
                        }, true);

                        if (response.results.updated) {
                            $.messageCodes.push({
                                'code': 'CORE000031', // user updated successfully
                                'type': 'S'
                            });
                        } else {
                            $.messageCodes.push({
                                'code': 'CORE000032', // Error while updating
                                'type': 'E'
                            });
                            return response;
                        }
                    }
                } else {
					$.logError('CORE000033', '', 'E');
                    return response;
                }
            }

            if (Array.isArray(response.errors) && $.lodash.isEmpty(response.errors)) {
                let userId = object.userId || response.results.instance.id;
                saveUserComplementaryData(userId, object);
            }
        }
    } catch (e) {
        $.logError('CORE000019', e, 'E');
    }
    return response;
};

let saveUserComplementaryData = function (user, object) {
    let {
        componentsPrivileges, packages,
        orgPrivileges, groups, taxes
    } = object;

    saveUserPrivileges(user, componentsPrivileges);
    saveUserPackages(user, packages);
    saveUserOrganizationalPrivileges(user, orgPrivileges);
    saveUserGroups(user, groups);
    saveUserTaxes(user, taxes);
};

let saveUserPrivileges = function (user, componentsPrivileges) {
    if (!Array.isArray(componentsPrivileges)) {
        return;
    }
    userPrivilegeModel.delete({
        where: [{
            field: 'userId',
            operator: '$eq',
            value: user
        }]
    });

    if (componentsPrivileges.length == 0) {
        $.logSuccess('CORE000034');
        return;
    }

    componentsPrivileges = componentsPrivileges.map(function (priv) {
        return {
            userId: user,
            privilegeId: priv
        };
    });

    let response = userPrivilegeModel.batchCreate(componentsPrivileges);
    let isSucces = response.results.created;
    if (isSucces) {
        $.logSuccess('CORE000034');
    } else {
        $.logError('CORE000035', response.error, 'E');
    }
};

let saveUserPackages = function (user, packages) {
    if (!Array.isArray(packages)) {
        return;
    }

    userPackageModel.delete({
        where: [{
            field: 'userId',
            operator: '$eq',
            value: user
        }]
    });

    if (packages.length == 0) {
        $.logSuccess('CORE000040');
        return;
    }

    packages = packages.map(function (pkg) {
        return {
            userId: user,
            packageId: pkg
        };
    });

    let response = userPackageModel.batchCreate(packages);
    let isSucces = response.results.created;
    if (isSucces) {
        $.logSuccess('CORE000040');
    } else {
        $.logError('CORE000041', '', 'E');
    }
};

let saveUserOrganizationalPrivileges = function (user, orgPrivileges) {
    if (!Array.isArray(orgPrivileges)) {
        return;
    }
    userOrgPrivilegeModel.delete({
        where: [{
            field: 'userId',
            operator: '$eq',
            value: user
        }]
    });

    if (orgPrivileges.length == 0) {
        $.logSuccess('CORE000042');
        return;
    }

    orgPrivileges = orgPrivileges.map(function (priv) {
        return {
            'userId': user,
            'companyId': priv.companyId,
            'branchId': priv.branchId
        };
    });

	let response = userOrgPrivilegeModel.batchCreate(orgPrivileges);
	let isSucces = response.results.created;
    if (isSucces) {
        $.logSuccess('CORE000042');
    } else {
        $.logError('CORE000043', '', 'S');
    }
};

let saveUserGroups = function (user, groups) {
    if (!Array.isArray(groups)) {
        return;
    }

    groupUserModel.delete({
        where: [{
            field: 'userId',
            operator: '$eq',
            value: user
        }]
    });


    if (groups.length == 0) {
        $.logSuccess('CORE000044');
        return;
    }

    groups = groups.map(function (group) {
        return {
            userId: user,
            groupId: group
        };
    });

	let response = groupUserModel.batchCreate(groups);
	let isSucces = response.results.created;
    if (isSucces) {
        $.logSuccess('CORE000044');
    } else {
        $.logError('CORE000045');
    }
};

let saveUserTaxes = function (user, taxes) {
    let modelUserTaxes = model.userTaxes();
    if (!Array.isArray(taxes)) {
        $.logError('CORE000203');
        return;
    }

    modelUserTaxes.delete({
        where: [{
            field: 'user',
            operator: '$eq',
            value: Number(user)
        }]
    });

    if (taxes.length == 0) {
        $.logSuccess('CORE000202');
        return;
    }

    taxes = taxes.map(function (tax) {
        return {
            user: Number(user),
            tax: tax
        };
    });

    let response = modelUserTaxes.batchCreate(taxes);
    let isSuccess = response.results.created;
    if (isSuccess) {
        $.logSuccess('CORE000202');
    } else {
        $.logError('CORE000203', response.errors);
    }
};

const model = {
    userTaxes: function () {
        $.import('timp.core.server.models.tables', 'UserTaxes');
        return $.timp.core.server.models.tables.UserTaxes;
    }
};

_this.getOrgPrivileges = function () {
    var response = {};
    try {
        let cvCompanyModel = $.createBaseRuntimeModel('_SYS_BIC', 'timp.atr.modeling.emp_fil/EMPRESA', true);
        let cvBranchModel = $.createBaseRuntimeModel('_SYS_BIC', 'timp.atr.modeling.emp_fil/FILIAL', true);
        if (Array.isArray(cvCompanyModel.errors) && cvCompanyModel.errors.length == 0) {
            var options = {
                aliases: [{
                    collection: cvCompanyModel.getIdentity(),
                    name: 'company',
                    isPrimary: true
                }, {
                    collection: cvBranchModel.getIdentity(),
                    name: 'branch'
                }],
                select: [{
                    field: 'COD_EMP',
                    alias: 'company',
                    as: 'id'
                }, {
                    field: 'NOME_EMPRESA',
                    alias: 'company',
                    as: 'name'
                }, {
                    field: 'COD_FILIAL',
                    alias: 'branch',
                    as: 'id'
                }, {
                    field: 'NOME_FILIAL',
                    alias: 'branch',
                    as: 'name'
                }, {
                    field: 'UF_FILIAL',
                    alias: 'branch',
                    as: 'state'
                }],
                join: [{
                    alias: 'branch',
                    type: 'inner',
                    map: 'branches',
                    on: [{
                        alias: 'branch',
                        field: 'COD_EMP',
                        operator: '$eq',
                        value: {
                            alias: 'company',
                            field: 'COD_EMP'
                        }
                    }]
                }]
            };

            cvCompanyModel.find(options).results.forEach(function (elem) {
                response[elem.id] = elem;
                delete response[elem.id].id;
            });
        }
    } catch (e) {
        return $.parseError(e);
    }
    return response;
};

_this.getUserPackages = function (userId) {
    var response = [];
    try {
        if (userId != undefined) {
            response = packageModel.find({
                'aliases': [{
                    'collection': packageModel.getIdentity(),
                    'name': 'Package',
                    'isPrimary': true
                }, {
                    'collection': userPackageModel.getIdentity(),
                    'name': 'UserPackage'
                }],
                select: [{
                    field: 'name',
                    alias: 'Package'
                }, {
                    field: 'description',
                    alias: 'Package'
                }, {
                    field: 'id',
                    alias: 'Package',
                    as: 'packageId'
                }],
                'join': [{
                    alias: 'UserPackage',
                    type: 'inner',
                    on: [{
                        alias: 'UserPackage',
                        field: 'packageId',
                        operator: '$eq',
                        value: {
                            'alias': 'Package',
                            'field': 'id'
                        }
                    }]
                }],
                'where': [{
                    field: 'userId',
                    alias: 'UserPackage',
                    operator: '$eq',
                    value: userId
                }]
            }).results;
        } else {
            $.messageCodes.push({
                'code': 'CORE000025', // Invalid User ID
                'type': 'E'
            });
            return response;
        }
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000021',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }
    return response;
};

_this.getUserGroups = function (userId) {
    var response = [];
    try {
        if (userId != undefined && $.lodash.isNumber) {
            response = groupModel.find({
                aliases: [{
                    collection: groupModel.getIdentity(),
                    name: 'Group',
                    isPrimary: true
                }, {
                    collection: groupUserModel.getIdentity(),
                    name: 'GroupUser'
                }],
                select: [{
                    field: 'id',
                    alias: 'Group',
                    as: 'groupId'
                }, {
                    field: 'name',
                    alias: 'Group'
                }],
                join: [{
                    alias: 'GroupUser',
                    type: 'inner',
                    on: [{
                        alias: 'GroupUser',
                        field: 'groupId',
                        operator: '$eq',
                        value: {
                            alias: 'Group',
                            field: 'id'
                        }
                    }]
                }],
                where: [{
                    field: 'userId',
                    alias: 'GroupUser',
                    operator: '$eq',
                    value: userId
                }]
            }).results;
        } else {
            $.logError.push('CORE000024');
        }
    } catch (e) {
        return $.parseError(e);
    }
    return response;
};

let getUserTaxes = function (user) {
	let modelUserTaxes = model.userTaxes();
    let userTaxes  = modelUserTaxes.getUserTaxesById({id:user});
	return userTaxes;
};

_this.getUserInformation = function (userId) {
    var response = {};
    try {
        if (userId != undefined) {
            response.metadata = _this.getDefaultFieldsData(userModel.find({
                xselect: [{
                    field: 'appPositions'
                }],
                where: [{
                    field: 'id',
                    operator: '$eq',
                    value: userId
                }]
            }).results)[0];

            if (response.metadata != undefined) {
                response.orgPrivileges = _this.getUserOrgPrivileges(response.metadata.hanaUser);
                response.userPackages = _this.getUserPackages(userId);
                response.userPrivs = _this.getUserPrivileges({'user': response.metadata.hanaUser});
				response.userGroups = _this.getUserGroups(userId);
				response.userTaxes = getUserTaxes(userId);
            }
        } else {
            $.logError('CORE000024');
            return response;
        }
    } catch (e) {
        $.logError('CORE000021', e);
    }
    return response;
};

_this.getRequiredInformation = function (action, userId) {
    var response = {
        listOrgPrivileges: {},
        listPackages: [],
        listComponents: [],
        listGroups: [],
        taxes: []
    };
    try {
        let apiAtr = $.getApi('atr');
        if  ( action === 'visualize' ) {
            if ($.lodash.isNumber(userId)) {
                var userInformation = _this.getUserInformation(userId);
                response.metadata = userInformation.metadata;
                response.listOrgPrivileges = userInformation.orgPrivileges;
                response.listPackages = userInformation.userPackages;
                response.listGroups = userInformation.userGroups;
                response.taxes = userInformation.userTaxes;
                let componentFields = [{
                    'field': 'name',
                    'alias': 'Component',
                    'as': 'componentName'
                }];
                if ($.request.cookies.get('Content-Language') === 'enus') {
                    componentFields.push({
                        field: 'labelEnus',
                        alias: 'Component',
                        as: 'label'
                    });
                } else {
                    componentFields.push({
                        field: 'labelPtbr',
                        alias: 'Component',
                        as: 'label'
                    });
                }
                response.listComponents = componentCtrl.getComponentPrivileges(null, componentFields);

                let privileges = [];
                userInformation.userPrivs.forEach(function (priv) {
                    let idx = $.lodash.findIndex(privileges, function (elem) {
                        return elem.componentName === priv.componentName;
                    });

                    if (idx !== -1) {
                        privileges[idx].privileges.push({
                            id: priv.privilegeId,
                            name: priv.privilegeName
                        });
                    } else {
                        privileges.push({
                            componentName: priv.componentName,
                            label: $.lodash.find(response.listComponents, function (elem) {
                                return elem.componentName === priv.componentName;
                            }).label,
                            privileges: [{
                                id: priv.privilegeId,
                                name: priv.privilegeName
                            }]
                        });
                    }
                });

                response.listComponents = privileges;

            } else {
                $.logError('CORE000024');
                return response;
            }
        } else {
            response.listComponents = componentCtrl.getComponentPrivileges();
            response.listOrgPrivileges = _this.getOrgPrivileges();
            response.listPackages = packageModelFile.getPackagesList();
            response.listGroups = groupModelFile.getGroupsList();
            response.taxes = $.session.hasAppPrivilege('timp::timp_all_taxes') ? apiAtr.tributo.listAllTaxes() : apiAtr.tributo.listTaxes();
			response = addEditUserData(response, userId);
        }
    } catch (e) {
		$.logError('CORE000021', e);
        return $.parseError(e);
    }
    return response;
};

let addEditUserData = function (response, userId) {
	if (userId == undefined) {
		return response;
	}
	$.import('timp.core.server.controllers.refactor', 'sharedFunctions');
	let sharedFunctions = $.timp.core.server.controllers.refactor.sharedFunctions;
	let user = _this.getUserInformation(userId);
	response.metadata = user.metadata;

	let {listOrgPrivileges, listPackages, listComponents, listGroups, taxes} = response;
	let {orgPrivileges, userPackages, userPrivs, userGroups, userTaxes } = user;
	response.listOrgPrivileges = sharedFunctions.assignOrganizationalPrivileges(orgPrivileges, listOrgPrivileges);
	response.listPackages = sharedFunctions.assignPackages(userPackages, listPackages);
	response.listPrivileges = sharedFunctions.assignComponentPrivileges(userPrivs, listComponents);
	response.listGroups = sharedFunctions.assignGroups(userGroups, listGroups);
    response.taxes = sharedFunctions.assignTaxes(userTaxes, taxes);
	return response;
};

_this.automaticCreateUser = function () {
    var response = {};
    try {
        var hanaUsersQuery = 'SELECT USER_NAME FROM PUBLIC."USERS" WHERE USER_DEACTIVATED = \'FALSE\'';
        var hanaUsers = $.execute(hanaUsersQuery).results.map(function (user) {
            return user.USER_NAME;
        });

        var usersRoleQuery = 'SELECT USER_NAME FROM PUBLIC."EFFECTIVE_ROLES" WHERE USER_NAME IN (\'' + hanaUsers.join('\',\'') +
            '\') AND ROLE_NAME = \'timp.core.server::timp\'';
        var usersRole = $.execute(usersRoleQuery).results;

        var coreUsers = userModel.find({
            'select': [{
                field: 'hanaUser'
            }, {
                field: 'isActive'
            }, {
                field: 'id'
            }]
        }).results;

        $.lodash.forEach(usersRole, function (h_user) {
            var user = $.lodash.find(coreUsers, function (c_user) {
                return c_user.hanaUser === h_user.USER_NAME;
            });

            //if User exists in HANA and does not exists in CORE::User, create user
            if ($.lodash.isNil(user) && !$.lodash.isEqual(h_user.USER_NAME, 'SYSTEM')) {
                var object = {
                    name: h_user.USER_NAME,
                    lastName: h_user.USER_NAME,
                    email: '',
                    hanaUser: h_user.USER_NAME,
                    position: '',
                    isActive: true,
                    isAdmin: false
                };
                response[h_user.USER_NAME] = userModel.create(object).results;
            } else if (!$.lodash.isNil(user) && $.lodash.isNil(user.isActive)) { // If User exists on boths sides but its status is "deactivated"(false) in CORE::User, set status as "active"(1)
                response[user.hanaUser] = userModel.update({
                    'isActive': true
                }, {
                    where: [{
                        field: 'id',
                        operator: '$eq',
                        value: user.id
                    }]
                }).results;
            }
        });

        // If User does not exist in HANA but in CORE::User, set status as "deactivated"(false)
        $.lodash.forEach(coreUsers, function (c_user) {
            let user = $.lodash.find(usersRole, function (h_user) {
                return c_user.hanaUser === h_user.USER_NAME;
            });
            if ($.lodash.isNil(user)) {
                response[c_user.hanaUser] = userModel.update({
                    'isActive': false
                }, {
                    where: [{
                        field: 'id',
                        operator: '$eq',
                        value: c_user.id
                    }]
                }).results;
            }
        });
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000020',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }

    return response;
};

_this.listUsers = function (object, paginate) {
    var response = {};
    try {
        let isAdmin = object && object.hanaUser && _this.isUserAdmin(object.hanaUser);
        var options = {
            'aliases': [{
                'collection': userModel.getIdentity(),
                'name': 'User',
                'isPrimary': true
            }],
            'xselect': [{
                field: 'appPositions'
            }],
            'where': [],
            'orderBy': [{
                'field': 'name',
                'alias': 'User',
                'type': 'asc'
            }]
        };
        if (!isAdmin && object && !$.lodash.isNil(object.hanaUser)) {
            options.where.push({
                'field': 'hanaUser',
                'operator': '$eq',
                'value': object.hanaUser
            });
        }
        if (!$.lodash.isNil(object)) {
            if (!$.lodash.isNil(object.filters)) {

                if (!$.lodash.isNil(object.filters.id)) {
                    options.where.push({
                        'field': 'id',
                        'operator': '$eq',
                        'value': object.filters.id
                    });
                }

                if ($.lodash.isArray(object.filters.status) && !$.lodash.isEmpty(object.filters.status)) {
                    let status = {
                        'active': 1,
                        'inactive': 0
                    };

                    object.filters.status = object.filters.status.map(function (s) {
                        return status[s];
                    });

                    options.where.push({
                        'field': 'isActive',
                        'operator': '$in',
                        'value': object.filters.status
                    });
                }

                if ($.lodash.isArray(object.filters.createdBy) && !$.lodash.isEmpty(object.filters.createdBy)) {
                    options.where.push({
                        'field': 'creationUser',
                        'operator': '$in',
                        'value': object.filters.createdBy
                    });
                }

                if ($.lodash.isArray(object.filters.modifiedBy) && !$.lodash.isEmpty(object.filters.modifiedBy)) {
                    options.where.push({
                        'field': 'modificationUser',
                        'operator': '$in',
                        'value': object.filters.modifiedBy
                    });
                }
                if (!$.lodash.isNil(object.filters.hanaUser) && $.lodash.isString(object.filters.hanaUser)) {
                    options.where.push({
                        'field': 'hanaUser',
                        'operator': '$contains',
                        'value': {
                            'value': object.filters.hanaUser,
                            'fuzzy': {
                                'minimumScore': 0.9
                            }
                        }
                    });
                }

                if ($.lodash.isArray(object.filters.creationDate) && !$.lodash.isEmpty(object.filters.creationDate)) {
                    options.where.push({
                        'function': {
                            'name': 'TO_DATE',
                            'params': [{
                                'field': 'creationDate'
                            }]
                        },
                        'operator': '$gte',
                        'value': {
                            'function': {
                                'name': 'TO_DATE',
                                'params': [object.filters.creationDate[0]]
                            }
                        }
                    });

                    if (!$.lodash.isEmpty(object.filters.creationDate[1])) {
                        options.where.push({
                            'function': {
                                'name': 'TO_DATE',
                                'params': [{
                                    'field': 'creationDate'
                                }]
                            },
                            'operator': '$lte',
                            'value': {
                                'function': {
                                    'name': 'TO_DATE',
                                    'params': [object.filters.creationDate[1]]
                                }
                            }
                        });
                    }
                }

                if ($.lodash.isArray(object.filters.modificationDate) && !$.lodash.isEmpty(object.filters.modificationDate)) {
                    options.where.push({
                        'function': {
                            'name': 'TO_DATE',
                            'params': [{
                                'field': 'modificationDate'
                            }]
                        },
                        'operator': '$gte',
                        'value': {
                            'function': {
                                'name': 'TO_DATE',
                                'params': [object.filters.modificationDate[0]]
                            }
                        }
                    });

                    if (!$.lodash.isEmpty(object.filters.modificationDate[1])) {
                        options.where.push({
                            'function': {
                                'name': 'TO_DATE',
                                'params': [{
                                    'field': 'modificationDate'
                                }]
                            },
                            'operator': '$lte',
                            'value': {
                                'function': {
                                    'name': 'TO_DATE',
                                    'params': [object.filters.modificationDate[1]]
                                }
                            }
                        });
                    }
                }

                if (!$.lodash.isNil(object.filters.group) && $.lodash.isNumber(object.filters.group)) {

                    options.aliases.push({
                        'collection': groupUserModel.getIdentity(),
                        'name': 'GroupUser'
                    });

                    options.join = [{
                        'alias': 'GroupUser',
                        'type': 'inner',
                        'on': [{
                            'alias': 'GroupUser',
                            'field': 'userId',
                            'operator': '$eq',
                            'value': {
                                'alias': 'User',
                                'field': 'id'
                            }
                        }]
                    }];

                    options.where.push({
                        'field': 'groupId',
                        'alias': 'GroupUser',
                        'operator': '$eq',
                        'value': object.filters.group
                    });
                }
            }

            if (!$.lodash.isNil(object.orderBy)) {
                options.orderBy = [{
                    'field': object.orderBy.field,
                    'alias': 'User',
                    'type': object.orderBy.type.toLowerCase()
                }];
            }
        }
        if (paginate) {
            options.paginate = {
                'limit': 15,
                'offset': object && object.page ? (object.page - 1) * 15 : 0
            };
        }
        response.users = userModel.find(options).results;
        if (paginate) {
            response.users = _this.getDefaultFieldsData(response.users);
        }
        response.pageCount = Math.ceil(userModel.find({
            count: true,
            select: [{
                field: 'id'
            }],
            where: options.where || []
        }).results[0].tableCount / 15);
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000021',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }
    return response;
};

_this.changeUserStatus = function (object) {
    var response = [];
    try {
        if (!$.lodash.isNil(object) && !$.lodash.isEmpty(object.users)) {
            var inactiveCoreUsers = [];

            var coreUsers = userModel.find({
                select: [{
                    'field': 'id'
                }, {
                    'field': 'isActive'
                }],
                where: [{
                    'field': 'id',
                    'operator': '$in',
                    value: object.users
                }]
            }).results;

            if ($.lodash.isEqual(coreUsers.length, object.users.length)) {
                object.users.forEach(function (user) {
                    let coreUser = $.lodash.find(coreUsers, function (elem) {
                        return elem.id === user;
                    });
                    if (object.activate && coreUser.isActive === false) {
                        inactiveCoreUsers.push(coreUser);
                    }
                });
                var updated;
                if (object.activate && !$.lodash.isEmpty(inactiveCoreUsers)) {
                    var availableLicense = licenseCtrl.checkAvailableLicenses(inactiveCoreUsers.length);
                    if (availableLicense) {
                        object.users.forEach(function (user) {
                            updated = userModel.update({
                                'isActive': object.activate
                            }, {
                                'where': [{
                                    'field': 'id',
                                    'operator': '$eq',
                                    'value': user
                                }]
                            }).results.updated;

                            if (updated) {
                                $.messageCodes.push({
                                    'code': 'CORE000062', // Updated Successfully
                                    'type': 'S'
                                });
                                response.push({
                                    userId: user,
                                    status: object.activate
                                });
                            }
                        });
                    } else {
                        $.messageCodes.push({
                            'code': 'CORE000028', // Error licencia, no todos los usuarios pueden ser activados
                            'type': 'E'
                        });
                        return response;
                    }
                } else {
                    object.users.forEach(function (user) {
                        updated = userModel.update({
                            'isActive': object.activate
                        }, {
                            'where': [{
                                'field': 'id',
                                'operator': '$eq',
                                'value': user
                            }]
                        }).results.updated;

                        if (updated) {
                            $.messageCodes.push({
                                'code': 'CORE000062', // Updated Successfully
                                'type': 'S'
                            });
                            response.push({
                                userId: user,
                                status: object.activate
                            });
                        }
                    });
                }
            } else {
                $.messageCodes.push({
                    'code': 'CORE000033', // Uno o mas de los IDs enviados no existe en CORE::User
                    'type': 'E'
                });
                return response; 
            }
        }
    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000020',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }
    return response;
};

_this.listAdvancedFilters = function () {
    var response = {
        'status': ['active', 'inactive'],
        'groups': [],
        'createdBy': [],
        'modifiedBy': []
    };

    let creationUserJoin = [];
    let modificationUserJoin = [];

    let userOptions = {
        'aliases': [{
            'collection': userModel.getIdentity(),
            'name': 'user',
            'isPrimary': true
        }, {
            'collection': userModel.getIdentity(),
            'name': 'user2'
        }],
        'distinct': true,
        'select': [{
            'field': 'id',
            'alias': 'user'
        }, {
            'field': 'name',
            'alias': 'user'
        }, {
            'field': 'lastName',
            'alias': 'user'
        }],
        'join': []
    };

    try {
        creationUserJoin = [{
            'alias': 'user2',
            'type': 'inner',
            'on': [{
                'alias': 'user2',
                'field': 'creationUser',
                'operator': '$eq',
                'value': {
                    'alias': 'user',
                    'field': 'id'
                }
            }]
        }];

        userOptions.join = creationUserJoin;
        response.createdBy = userModel.find(userOptions).results;

        modificationUserJoin = [{
            'alias': 'user2',
            'type': 'inner',
            'on': [{
                'alias': 'user2',
                'field': 'modificationUser',
                'operator': '$eq',
                'value': {
                    'alias': 'user',
                    'field': 'id'
                }
            }]
        }];

        userOptions.join = modificationUserJoin;
        response.modifiedBy = userModel.find(userOptions).results;

        response.groups = groupModel.find({
            distinct: true,
            select: [{
                'field': 'id'
            }, {
                'field': 'name'
            }]
        }).results;

    } catch (e) {
        $.messageCodes.push({
            'code': 'CORE000021',
            'type': 'E',
            'errorInfo': $.parseError(e)
        });
    }

    return response;
};

_this.getDefaultFieldsData = function (elements) {
    try {
        let options = {
            select: [{
                'field': 'name'
            }, {
                'field': 'lastName'
            }]
        };

        if ($.lodash.isArray(elements) && !$.lodash.isEmpty(elements)) {
            let users = [];
            var usersData = {};

            $.lodash.forEach(elements, function (elem) {
                if (elem.creationUser) {
                    users.push(elem.creationUser);
                }
                if (elem.modificationUser) {
                    users.push(elem.modificationUser);
                }
            });

            if (!$.lodash.isEmpty(users)) {
                if ($.lodash.isIntArray(users)) {
                    options.select.push({
                        'field': 'id'
                    });
                    options.where = [{
                        'field': 'id',
                        'operator': '$in',
                        'value': $.lodash.uniq(users)
                    }];

                    userModel.find(options).results.map(function (elem) {
                        usersData[elem.id] = elem;
                    });

                } else if ($.lodash.isStringArray(users)) {
                    options.select.push({
                        'field': 'hanaUser'
                    });
                    options.where = [{
                        'field': 'hanaUser',
                        'operator': '$in',
                        'value': $.lodash.uniq(users)
                    }];

                    userModel.find(options).results.map(function (elem) {
                        usersData[elem.hanaUser] = elem;
                    });
                }
            }

            $.lodash.forEach(elements, function (elem) {
                elem.creationUser = usersData[elem.creationUser] ? usersData[elem.creationUser] : {};
                elem.modificationUser = usersData[elem.modificationUser] ? usersData[elem.modificationUser] : {};
            });
        }
    } catch (e) {
        return $.parseError(e);
    }
    return elements;
};

/**
 *
 * @method userHasPrivileges
 * @param {Array} privileges
 * @param {string} componentName
 * @return {Object}
 * */
_this.userHasPrivileges = function (privileges, componentName) {
    var response = {
        errors: []
    };
    try {
        let userName = $.session.getUsername();
        if ($.lodash.isArray(privileges) && !$.lodash.isEmpty(privileges) && !$.lodash.isNil(componentName) && $.lodash.isString(componentName)) {
            let userPrivilegesByComponent = _this.getUserPrivileges({
                user: userName,
                component: componentName
            }).map(function (elem) {
                return elem.privilegeName;
            });
            $.lodash.forEach(privileges, function (privilege) {
                if (privilege && $.lodash.indexOf(userPrivilegesByComponent, privilege) === -1) {
                    response.errors.push({
                        privilege: privilege,
                        error: 'Missing Privilege'
                    });
                }
            });
            if ($.lodash.isEmpty(response.errors)) {
                response.valid = true;
            } else {
                response.valid = false;
            }
        }
    } catch (e) {
        return $.parseError(e);
    }
    return response;
};

_this.isUserAdmin = function (hanaUser) {
    let result = userModel.find({
        select: [{
            field: 'isAdmin'
        }],
        where: [{
            field: 'hanaUser',
            operator: '$eq',
            value: hanaUser
        }]
    });
    return result && result.results.length ? result.results[0].isAdmin : false;
}; 