$.import('timp.core.server', 'config');
const currentVersion = $.timp.core.server.config.application.version;

$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

const _this = this;

_this.getParsedSeeder = function(seeders) {
	let tempSeeder;
	let seeder = [];
	$.lodash.forEach(seeders, function(_seeder) {
		if (_seeder) {
			if (_seeder.model instanceof BaseModel || ($.lodash.isString(_seeder.identity) && $.lodash.isString(_seeder.schema))) {
				tempSeeder = {
					base: _seeder.base,
					'new': $.lodash.isArray(_seeder.new) ? _seeder.new : [],
					update: $.lodash.isArray(_seeder.update) ? _seeder.update : []
				};
				if (!$.lodash.isNil(_seeder[currentVersion])) {
				    if ($.lodash.isArray(_seeder[currentVersion].base) && !$.lodash.isEmpty(_seeder[currentVersion].base)) {
				        tempSeeder.base = $.lodash.concat(tempSeeder.base, _seeder[currentVersion].base);
				    }
					if ($.lodash.isArray(_seeder[currentVersion].new) && !$.lodash.isEmpty(_seeder[currentVersion].new)) {
				        tempSeeder.new = $.lodash.concat(tempSeeder.new, _seeder[currentVersion].new);
				    }
				    if ($.lodash.isArray(_seeder[currentVersion].update) && !$.lodash.isEmpty(_seeder[currentVersion].update)) {
				        tempSeeder.update = $.lodash.concat(tempSeeder.update, _seeder[currentVersion].update);
				    }
				}
				seeder.push({
					model: _seeder.model,
					identity: _seeder.identity,
					schema: _seeder.schema,
					values: tempSeeder
				});
			}
		}
	});
	return seeder;
};