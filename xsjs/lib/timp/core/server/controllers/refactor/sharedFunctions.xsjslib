this.assignOrganizationalPrivileges = function (orgPrivileges, listOrgPrivileges) {
	Object.keys(orgPrivileges).forEach(function (userComp) {
		if (listOrgPrivileges[userComp]) {
			listOrgPrivileges[userComp].isAssigned = true;
			$.lodash.forEach(listOrgPrivileges[userComp].branches, function (branch) {
				branch.isAssigned = orgPrivileges[userComp].branches.find(function (userBranch) {
					return $.lodash.isEqual(userBranch.id, branch.id);
				}) ? true : undefined;
			});
		}
	});
	return listOrgPrivileges;
};

this.assignPackages = function (userPackages, listPackages) {
	$.lodash.forEach(userPackages, function (userPkg) {
		let idx = $.lodash.findIndex(listPackages, function (pkg) {
			return userPkg.packageId === pkg.id;
		});
		if (idx !== -1) {
			listPackages[idx].isAssigned = true;
		}
	});
	return listPackages;
};


this.assignComponentPrivileges = function (userPrivs, listComponents) {
	$.lodash.forEach(userPrivs, function (userPriv) {
		let idx = $.lodash.findIndex(listComponents, function (component) {
			return userPriv.componentName === component.componentName;
		});
		if (idx !== -1) {
			listComponents[idx].isAssigned = true;
			$.lodash.forEach(listComponents[idx].privileges, function (priv) {
				if (userPriv.privilegeId === priv.privilegeId) {
					priv.isAssigned = true;
				}
			});
		}
	});
	return listComponents;
};

this.assignGroups = function (userGroups, listGroups) {
	$.lodash.forEach(userGroups, function (uGroup) {
		let idx = $.lodash.findIndex(listGroups, function (group) {
			return uGroup.groupId === group.id;
		});
		if (idx !== -1) {
			listGroups[idx].isAssigned = true;
		}
	});
	return listGroups;
};

this.assignUsers = function (groupUsers, listUsers) {
    groupUsers.forEach(function(gUser) {
	   listUsers.forEach(function(user) {
		   if (gUser.userId === user.id) {
			   user.isAssigned = true;
		   }
	   });
   });
   return listUsers;
};

this.assignTaxes = function (userTaxes, taxes) {
	for(let i = 0; i < userTaxes.length; i++){
		let key = userTaxes[i].tax;
		let tax = taxes.find( (_tax) => _tax.key == key);
		if(tax){
			tax.isAssigned = true;
		}
	}
	return taxes;
};
