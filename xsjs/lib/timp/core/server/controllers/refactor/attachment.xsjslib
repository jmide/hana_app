$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server.models.tables', 'attachment');
const attachmentModel = $.timp.core.server.models.tables.attachment.attachmentModel;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;

$.import('timp.core.server.models.tables', 'fileSystem');
const fileModel = $.timp.core.server.models.tables.fileSystem.fileSystem;

const _attachmentSchema = {
    properties: {
        id: {
            type: 'integer'
        },
        name: {
            type: 'string',
            required: true
        },
        description: {
            type: 'string'
        },
        mimeType: {
            type: 'string',
            required: true
        },
        component: {
            type: ['integer', 'string'],
            required: true
        },
        file: {
            type: 'string',
            pattern: /^data:([A-Za-z-+\/\.]+);base64,(.+)$/,
            required: true
        }
	}
};
this.saveAttachment = function(attachment) {
    let parameterValidation = $.validator.validate(attachment, _attachmentSchema, {
		additionalProperties: false
	});
	let errorGenerator = new $.ErrorGenerator('saveAttachment');
	if (!$.lodash.isNil(attachment) && parameterValidation.valid) {
	    let componentId = $.lodash.isString(attachment.component)
	        ? util.generalInformation.getComponentInformation(attachment.component) : attachment.component;
        
        if($.lodash.isString(attachment.component) && !$.lodash.isNil(componentId) && !$.lodash.isEmpty(componentId.results)) {
            componentId = componentId.results[0].id;
        }
	    attachment.componentId = componentId;
	    delete attachment.component;
	    let file = attachment.file.split('base64,')[1];
	    attachment.file = $.util.codec.decodeBase64(file);
	    
	    let attachmentResponse = attachmentModel.create(attachment).results;
	    if(attachmentResponse.created) {
	        let fileData = {
                status: 4,
                folderId: -1,
                coreAttachmentId: attachmentResponse.id
    		};
    		
    		let responseFile = fileModel.create(fileData).results;
    		
    		if (responseFile.created) {
    		    let result = $.lodash.assign(attachmentResponse, {
    		        status: 'Public',
    		        url: '/timp/core/server/endpoint.xsjs/attachments/get/' + attachmentResponse.id + '/',
    		        idFile: responseFile.id
    		    });
    		    
    		    return result;
    		} else {
    		    throw errorGenerator.saveError(responseFile, '39');
    		}
	    } else {
	        throw errorGenerator.saveError(attachmentResponse);
	    }
	} else {
	    throw errorGenerator.paramsError(parameterValidation.errors);
	}
};

this.getAttachment = function(id) {
    let errorGenerator = new $.ErrorGenerator('getAttachment');
    if($.lodash.isInteger(id)) {
        let response = attachmentModel.find({
            where: [{
                field: 'id',
                operator: '$eq',
                value: id
            }]
        });
        
        return response.results[0];
    } else {
        throw errorGenerator.paramsError(['id']);
    }
};