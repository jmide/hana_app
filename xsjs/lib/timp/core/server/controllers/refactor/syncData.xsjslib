$.import('timp.core.server.libraries.internal', 'util');
const lodash = $.timp.core.server.libraries.internal.util.lodash;
const validator = $.timp.core.server.libraries.internal.util.jsonValidator;

$.import('timp.core.server.models.tables', 'fileSystem');
const newObjectTypeModel = $.timp.core.server.models.tables.fileSystem.objectTypeModel;
const newFolderModel = $.timp.core.server.models.tables.fileSystem.folderModel;
const newFolderShareModel = $.timp.core.server.models.tables.fileSystem.folderShareModel;
const newFileModel = $.timp.core.server.models.tables.fileSystem.fileModel;
const newFileShareModel = $.timp.core.server.models.tables.fileSystem.fileShareModel;
const newFileFavoriteModel = $.timp.core.server.models.tables.fileSystem.fileFavoriteModel;

$.import('timp.core.server.models.tables', 'attachment');
const newImageModel = $.timp.core.server.models.tables.attachment.imageModel;

$.import('timp.core.server.models.tables', 'output');
const newOutputModel = $.timp.core.server.models.tables.output.outputModel;

$.import('timp.core.server.models.tables', 'component');
const newComponentModel = $.timp.core.server.models.tables.component.componentModel;
const newMessageCodeModel = $.timp.core.server.models.tables.component.messageCodeModel;
const newPrivilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

$.import('timp.core.server.models.tables', 'configuration');
const newConfigurationModel = $.timp.core.server.models.tables.configuration.configurationModel;
const newLicenseModel = $.timp.core.server.models.tables.configuration.licenseModel;

$.import('timp.core.server.models.tables', 'user');
const newUserModel = $.timp.core.server.models.tables.user.userModel;
const newUserPrivilegeModel = $.timp.core.server.models.tables.user.userPrivilegeModel;
const newUserOrgPrivilegesModel = $.timp.core.server.models.tables.user.userOrgPrivilegeModel;
const newUserPackageModel = $.timp.core.server.models.tables.user.userPackageModel;
const newUserAppsPositionModel = $.timp.core.server.models.tables.user.userAppPositionsModel;

$.import('timp.core.server.models.tables', 'package');
const newPackageModel = $.timp.core.server.models.tables.package.packageModel;
const newPackagePrivilegeModel = $.timp.core.server.models.tables.package.packagePrivilegeModel;

$.import('timp.core.server.models.tables', 'group');
const newGroupModel = $.timp.core.server.models.tables.group.groupModel;
const newGroupUserModel = $.timp.core.server.models.tables.group.groupUserModel;
const newGroupPrivilegeModel = $.timp.core.server.models.tables.group.groupPrivilegeModel;
const newGroupPackageModel = $.timp.core.server.models.tables.group.groupPackageModel;
const newGroupOrgPrivilegeModel = $.timp.core.server.models.tables.group.groupOrgPrivilegeModel;

const adapterModel = $.import("/timp/core/server/models/tables/proxyService.xsjslib");

const layoutModel = $.import('/timp/bfb/server/models/models/tables/layout/layout.xsjslib').layoutModel;
const formModel = $.import('/timp/bfb/server/models/models/tables/form/form.xsjslib').formModel;
const formTypeModel = $.import('/timp/bfb/server/models/models/tables/form/formType.xsjslib').formTypeModel;
const settingModel = $.import('/timp/bfb/server/models/models/tables/setting/setting.xsjslib').settingModel;
const settingEEFIModel = $.import('/timp/bfb/server/models/models/tables/setting/settingEEFI.xsjslib').settingEEFIModel;

const dfgLayoutFile = $.import('/timp/dfg/server/models/models/tables/layout/layout.xsjslib');
const dfgLayoutModel = dfgLayoutFile.layoutModel;
const dfgLayoutStructureModel = dfgLayoutFile.layoutStructureModel;
const dfgLayoutTypeModel = dfgLayoutFile.layoutTypeModel;
const dfgLayoutVersionModel = dfgLayoutFile.layoutVersionModel;
const dfgSettingFile = $.import('/timp/dfg/server/models/models/tables/setting/setting.xsjslib');
const dfgSettingModel = dfgSettingFile.settingModel;
const dfgSettingEEFIModel = dfgSettingFile.settingEEFIModel;
const dfgDigitalFileFile = $.import('/timp/dfg/server/models/models/tables/digitalFile/digitalFile.xsjslib');
const dfgDigitalFileModel = dfgDigitalFileFile.digitalFileModel;
const dfgDigitalFileSubperiodModel = dfgDigitalFileFile.digitalFileSubperiodModel;
const dfgSpedFile = $.import('/timp/dfg/server/models/models/tables/sped/sped.xsjslib');
const dfgSpedModel = dfgSpedFile.spedModel;
const dfgSpedEEFIModel = dfgSpedFile.spedEEFIModel;
const dfgSpedJobExecutionModel = dfgSpedFile.spedJobExecutionModel;
const dfgSpedJobExecutionSubperiodModel = dfgSpedFile.spedJobExecutionSubperiodModel;
const dfgSpedLabelModel = dfgSpedFile.spedLabelModel;
const dfgSpedRecordTdfTableModel = dfgSpedFile.spedRecordTdfTableModel;
const dfgSpedRunModel = dfgSpedFile.spedRunModel;
const dfgEfdIcmsIpiVariantModel = $.import('/timp/dfg/server/models/models/tables/sped/variant/efdIcmsIpiVariant.xsjslib').efdIcmsIpiVariantModel;
const dfgEfdContributionsVariantModel = $.import('/timp/dfg/server/models/models/tables/sped/variant/efdContributionsVariant.xsjslib').efdContributionsVariantModel;
const dfgAn3File = $.import('/timp/dfg/server/models/models/tables/an3/an3.xsjslib');
const dfgAn3Model = dfgAn3File.an3Model;
const dfgAn3RuleModel = dfgAn3File.an3RuleModel;
const dfgAn3ReportModel = dfgAn3File.an3ReportModel;
const dfgAn4File = $.import('/timp/dfg/server/models/models/tables/an4/an4.xsjslib');
const dfgAn4Model = dfgAn4File.an4Model;
const dfgAn4DigitalFileModel = dfgAn4File.an4DigitalFileModel;
const dfgAn4ExternalFileModel = dfgAn4File.an4ExternalFileModel;
const dfgAn4RuleModel = dfgAn4File.an4RuleModel;
const dfgAn4ReportModel = dfgAn4File.an4ReportModel;
const dfgPanelFile = $.import('/timp/dfg/server/models/models/tables/panel/panel.xsjslib');
const dfgPanelModel = dfgPanelFile.panelModel;
const dfgPanelJustifyModel = dfgPanelFile.panelJustifyModel;
const dfgPanelApproveModel = dfgPanelFile.panelApproveModel;
const dfgPanelCommentModel = dfgPanelFile.panelCommentModel;
const dfgPanelSettingModel = dfgPanelFile.panelSettingModel;
const dfgPanelStatusModel = dfgPanelFile.panelStatusModel;

const brbReportModel = $.import('/timp/brb/server/models/tables/report/report.xsjslib').reportModel;
this.syncRecentModels = function() {
	try {
		let responses = [];
		if(!brbReportModel.exists().results.exists) {
		    responses.push(brbReportModel.sync());
		}

		// if(!layoutModel.exists().results.exists) {
		//     responses.push(layoutModel.sync());
		// }

		// if(!formTypeModel.exists().results.exists) {
		//     responses.push(formTypeModel.sync());
		// }
		// if(!formModel.exists().results.exists) {
		//     responses.push(formModel.sync());
		// }
		// if(!settingModel.exists().results.exists) {
		//     responses.push(settingModel.sync());
		// }
		// if(!settingEEFIModel.exists().results.exists) {
		//     responses.push(settingEEFIModel.sync());
		// }

		// if(!newFileModel.exists().results.exists) {
		//     responses.push(newFileModel.sync());
		// }

		// if(!newFolderModel.exists().results.exists) {
		//      responses.push(newFolderModel.sync());
		// }

		// if(!newFolderShareModel.exists().results.exists) {
		//     responses.push(newFolderShareModel.sync());
		// }

		// if(!newFileShareModel.exists().results.exists) {
		//     responses.push(newFileShareModel.sync());
		// }

		// if(!newFileFavoriteModel.exists().results.exists) {
		//     responses.push(newFileFavoriteModel.sync());
		// }

		// if(!newImageModel.exists().results.exists) {
		//     responses.push(newImageModel.sync());
		// }

		// if(!newOutputModel.exists().results.exists) {
		//     responses.push(newOutputModel.sync());
		// }
		/*
		if (!dfgLayoutModel.exists().results.exists) {
			responses.push(dfgLayoutModel.sync());
		}
		if (!dfgLayoutStructureModel.exists().results.exists) {
			responses.push(dfgLayoutStructureModel.sync());
		}
		if(!dfgLayoutVersionModel.exists().results.exists){
		    responses.push(dfgLayoutVersionModel.sync());
		}
		if (!dfgLayoutTypeModel.exists().results.exists) {
			responses.push(dfgLayoutTypeModel.sync());
		}
		if (!dfgSettingModel.exists().results.exists) {
			responses.push(dfgSettingModel.sync());
		}
		if (!dfgSettingEEFIModel.exists().results.exists) {
			responses.push(dfgSettingEEFIModel.sync());
		}
		if (!dfgDigitalFileModel.exists().results.exists) {
			responses.push(dfgDigitalFileModel.sync());
		}
		if (!dfgDigitalFileSubperiodModel.exists().results.exists) {
			responses.push(dfgDigitalFileSubperiodModel.sync());
		}
		if (!dfgSpedModel.exists().results.exists) {
			responses.push(dfgSpedModel.sync());
		}
		if (!dfgSpedEEFIModel.exists().results.exists) {
			responses.push(dfgSpedEEFIModel.sync());
		}
		if (!dfgSpedJobExecutionModel.exists().results.exists) {
			responses.push(dfgSpedJobExecutionModel.sync());
		}
		if (!dfgSpedJobExecutionSubperiodModel.exists().results.exists) {
			responses.push(dfgSpedJobExecutionSubperiodModel.sync());
		}
		if (!dfgSpedLabelModel.exists().results.exists) {
			responses.push(dfgSpedLabelModel.sync());
		}
		if (!dfgSpedRecordTdfTableModel.exists().results.exists) {
			responses.push(dfgSpedRecordTdfTableModel.sync());
		}
		if (!dfgSpedRunModel.exists().results.exists) {
			responses.push(dfgSpedRunModel.sync());
		}
		if(!dfgEfdIcmsIpiVariantModel.exists().results.exists){
		    responses.push(dfgEfdIcmsIpiVariantModel.sync());
		}
		if(!dfgEfdContributionsVariantModel.exists().results.exists){
		    responses.push(dfgEfdContributionsVariantModel.sync());
		}
		if (!dfgAn3Model.exists().results.exists) {
			responses.push(dfgAn3Model.sync());
		}
		if (!dfgAn3RuleModel.exists().results.exists) {
			responses.push(dfgAn3RuleModel.sync());
		}
		if (!dfgAn3ReportModel.exists().results.exists) {
			responses.push(dfgAn3ReportModel.sync());
		}
		if (!dfgAn4Model.exists().results.exists) {
			responses.push(dfgAn4Model.sync());
		}
		if (!dfgAn4DigitalFileModel.exists().results.exists) {
			responses.push(dfgAn4DigitalFileModel.sync());
		}
		if (!dfgAn4ExternalFileModel.exists().results.exists) {
			responses.push(dfgAn4ExternalFileModel.sync());
		}
		if (!dfgAn4RuleModel.exists().results.exists) {
			responses.push(dfgAn4RuleModel.sync());
		}
		if (!dfgAn4ReportModel.exists().results.exists) {
			responses.push(dfgAn4ReportModel.sync());
		}
		if (!dfgPanelModel.exists().results.exists) {
			responses.push(dfgPanelModel.sync());
		}
		if (!dfgPanelJustifyModel.exists().results.exists) {
			responses.push(dfgPanelJustifyModel.sync());
		}
		if (!dfgPanelApproveModel.exists().results.exists) {
			responses.push(dfgPanelApproveModel.sync());
		}
		if (!dfgPanelCommentModel.exists().results.exists) {
			responses.push(dfgPanelCommentModel.sync());
		}
		if (!dfgPanelSettingModel.exists().results.exists) {
			responses.push(dfgPanelSettingModel.sync());
		}
		if (!dfgPanelStatusModel.exists().results.exists) {
			responses.push(dfgPanelStatusModel.sync());
		}
        */
		if (!newComponentModel.exists().results.exists) {
			responses.push(newComponentModel.sync());
		}

		// if(!adapterModel.exists().results.exists) {
		//     responses.push(adapterModel.sync());
		// }

		//  if(!newPrivilegeModel.exists().results.exists) {
		//     responses.push(newPrivilegeModel.sync());
		// }

		// if(!newConfigurationModel.exists().results.exists) {
		//     responses.push(newConfigurationModel.sync());
		// }

		// if(!newUserModel.exists().results.exists) {
		//     responses.push(newUserModel.sync());
		// }

		// if(!newUserPrivilegeModel.exists().results.exists) {
		//     responses.push(newUserPrivilegeModel.sync());
		// }

		// if(!newUserOrgPrivilegesModel.exists().results.exists) {
		//     responses.push(newUserOrgPrivilegesModel.sync());
		// }

		// if(!newUserPackageModel.exists().results.exists) {
		//     responses.push(newUserPackageModel.sync());
		// }

		// if(!newUserAppsPositionModel.exists().results.exists) {
		//     responses.push(newUserAppsPositionModel.sync());
		// }

		// if(!newPackageModel.exists().results.exists) {
		//     responses.push(newPackageModel.sync());
		// }

		// if(!newPackagePrivilegeModel.exists().results.exists) {
		//     responses.push(newPackagePrivilegeModel.sync());
		// }

		// if(!newGroupModel.exists().results.exists) {
		//     responses.push(newGroupModel.sync());
		// }

		// if(!newGroupUserModel.exists().results.exists) {
		//     responses.push(newGroupUserModel.sync());
		// }

		// if(!newGroupPrivilegeModel.exists().results.exists) {
		//     responses.push(newGroupPrivilegeModel.sync());
		// }

		// if(!newGroupPackageModel.exists().results.exists) {
		//     responses.push(newGroupPackageModel.sync());
		// }

		// if(!newGroupOrgPrivilegeModel.exists().results.exists) {
		//     responses.push(newGroupOrgPrivilegeModel.sync());
		// }

		// if(!newLicenseModel.exists().results.exists) {
		//     responses.push(newLicenseModel.sync());
		// }

		return responses;
	} catch (e) {
		return $.parseError(e);
	}
};