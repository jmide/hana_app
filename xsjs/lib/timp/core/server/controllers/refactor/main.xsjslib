const _specialCases = ['LOGIN', 'TKB', 'ADM'];
const _ = $.lodash;

const verifyInstallationStatus = function () {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;
	$.import('timp.core.server.models.tables', 'component');
	const componentModel = $.timp.core.server.models.tables.component.componentModel;
	let response = {
		isPresent: false
	};
	let component = componentModel.exists();
	let user = userModel.exists();
	let errors = [];
	if (component.results.exists && user.results.exists) {
		response.isPresent = true;
		return response;
	}
	response.isPresent = false;
	_.forEach(_.concat(component.errors, user.errors), function (error) {
		if (error.message && error.message.indexOf('user is forced to change password') && _.indexOf(errors, 'CORE000110') === -1) {
			errors.push('CORE000110');
		}
	});
	response.errorsFromCore = errors;
	return response;
};

const getTranslations = function () {
	$.import('timp.core.server.libraries.internal', 'util');
	const util = $.timp.core.server.libraries.internal.util;
	let messageCodeTranslationsModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::MessageCodeText');
	if ($.lodash.isEmpty(messageCodeTranslationsModel.errors)) {
		return messageCodeTranslationsModel.find({
			select: [{
				field: 'CODE',
				as: 'code'
			}, {
				field: 'TEXT',
				as: 'text'
			}],
			where: [{
				field: 'LANG',
				operator: '$eq',
				value: util.getSessionLanguage()
			}]
		}).results;
	} else {
		return [];
	}
};

const processTaskboard = function () {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;
	$.import('timp.core.server.models.views', 'userPrivilege');
	const userPrivilegeView = $.timp.core.server.models.views.userPrivilege.userPrivilegeView;
	$.import('timp.core.server.models.tables', 'component');
	const componentModel = $.timp.core.server.models.tables.component.componentModel;
	$.import('timp.core.server.controllers.refactor', 'configuration');
	const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
	let verify = verifyInstallationStatus();
	if (verify.isPresent) {
		let isSingleSignOn = configurationCtrl.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['TIMP::LogonType']
		})[0];
		if (isSingleSignOn) {
			isSingleSignOn = isSingleSignOn.value;
		}
		let userInformation = userModel.find({
			select: [{
				field: 'id'
			}, {
				field: 'hanaUser'
			}, {
				field: 'name'
			}, {
				field: 'lastName'
			}, {
				field: 'isAdmin'
			}],
			where: [{
				field: 'hanaUser',
				operator: '$eq',
				value: $.session.getUsername()
			}]
		});
		let componentsAvailable = [];
		if ($.userHasGrcPrivileges($.session.getUsername())) {
			componentsAvailable = $.execute(
				'SELECT DISTINCT COMPONENT.ID AS "id", COMPONENT.NAME AS "componentName", COMPONENT.LABEL_ENUS AS "labelEnus", COMPONENT.LABEL_PTBR AS "labelPtbr" ' +
				'FROM ' + $.schema + '."CORE::PRIVILEGE" AS PRIVILEGE ' +
				'INNER JOIN "PUBLIC"."EFFECTIVE_ROLES" AS ROLES ' +
				'ON PRIVILEGE.ROLE = ROLES.ROLE_NAME ' +
				'INNER JOIN ' + $.schema + '."CORE::COMPONENT" AS COMPONENT ' +
				'ON PRIVILEGE.COMPONENT_ID = COMPONENT.ID ' +
				'WHERE USER_NAME = \'' + $.session.getUsername() + '\' AND PRIVILEGE.NAME = \'Access\' ').results;
		} else {
			componentsAvailable = userPrivilegeView.find({
				aliases: [{
					collection: userPrivilegeView.getIdentity(),
					name: 'UserPrivilege',
					isPrimary: true
				}, {
					collection: componentModel.getIdentity(),
					name: 'Component'
				}],
				select: [{
					field: 'componentId',
					alias: 'UserPrivilege',
					as: 'id'
				}, {
					field: 'componentName',
					alias: 'UserPrivilege'
				}, {
					field: 'labelEnus',
					alias: 'Component'
				}, {
					field: 'labelPtbr',
					alias: 'Component'
				}],
				join: [{
					alias: 'Component',
					type: 'inner',
					map: 'componentMetadata',
					on: [{
						alias: 'Component',
						field: 'id',
						operator: '$eq',
						value: {
							alias: 'UserPrivilege',
							field: 'componentId'
						}
					}]
				}],
				where: [{
					field: 'privilegeName',
					alias: 'UserPrivilege',
					operator: '$eq',
					value: 'Access'
				}, {
					field: 'hanaUser',
					alias: 'UserPrivilege',
					operator: '$eq',
					value: $.session.getUsername()
				}],
				orderBy: [{
					field: 'componentName',
					alias: 'UserPrivilege',
					type: 'asc'
				}]
			});
			componentsAvailable = componentsAvailable.results.map(function (component) {
				if (!$.lodash.isEmpty(component.componentMetadata)) {
					component.labelEnus = component.componentMetadata[0].labelEnus;
					component.labelPtbr = component.componentMetadata[0].labelPtbr;
				}
				delete component.componentMetadata;
				return component;
			});
		}
		return {
			loggedUser: userInformation.results[0],
			components: componentsAvailable,
			// translations: getTranslations(),
			hasAdmAccess: $.session.hasAppPrivilege('timp::timp_adm_access'),
			logonType: isSingleSignOn
		};
	}
	return {
		hasAdmAccess: $.session.hasAppPrivilege('timp::timp_adm_access'),
		components: [],
		loggedUser: ($.lodash.isEqual($.session.getUsername(), '')) ? false : $.session.getUsername(),
		errorsFromCore: verify.errorsFromCore
	};
};

const getUserInformation = function (hanaUser) {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;
	let userInformation = {
		id: null,
		isAdmin: false
	};
	if (hanaUser) {
		let result = userModel.find({
			select: [{
				field: 'id'
			}, {
				field: 'isAdmin'
			}],
			where: [{
				field: 'hanaUser',
				operator: '$eq',
				value: hanaUser
			}]
		});
		userInformation = result && result.results.length ? result.results[0] : userInformation;
	}
	return userInformation;
};

const processAdministrationCockpit = function () {
	let username = $.session.getUsername();
	let userInformation = getUserInformation(username);
	return {
		hasAdmAccess: $.session.hasAppPrivilege('timp::timp_adm_access'),
		hasUserManagement: $.session.hasAppPrivilege('timp::timp_user_management'),
		hasServiceManagement: $.session.hasAppPrivilege('timp::timp_service_management'),
		hasInstallPrivilege: $.session.hasAppPrivilege('timp::timp_install'),
		// translations: getTranslations(),
		privileges: getUserComponentPrivileges('ATR', 'tax'),
		loggedUser: {
			hanaUser: username,
			name: username,
			lastName: username,
			id: userInformation.id,
			isAdmin: userInformation.isAdmin
		}
	};
};

const processSpecialCases = function (componentName) {
	if ($.lodash.isEqual(componentName, 'TKB')) {
		return processTaskboard();
	}
	if ($.lodash.isEqual(componentName, 'LOGIN')) {
		return {
			loggedUser: ($.lodash.isEqual($.session.getUsername(), '')) ? false : $.session.getUsername()
		};
	}
	if ($.lodash.isEqual(componentName, 'ADM')) {
		return processAdministrationCockpit();
	}
};

const processComponentData = function (componentName) {
	try {
		const taxes = listTaxes();
		const privileges = getUserComponentPrivileges(componentName);
		// const translations = getTranslations();
		const logonType = getLogonType();
		const onCloud = getIsOnCloud();
		const loggedUser = getLoggedUser();
		const organizationalPrivileges = getOrganizationalPrivileges();
		let data = {
			privileges,
			// translations,
			organizationalPrivileges,
			loggedUser,
			logonType,
			onCloud,
			taxes
		};
		if (componentName.toLocaleLowerCase() == 'bcb'){
			data.BRBExecutorEngines = $.getBrbNewExecutorFlags();
		}
		return data;
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E',
			'errorInfo': $.parseError(e)
		});
		return {};
	}
};

const getOrganizationalPrivileges = function () {
	$.import('timp.core.server.controllers.refactor', 'user');
	const userCtrl = $.timp.core.server.controllers.refactor.user;
	const api = $.getApi('atr', 'organizations');
	const companyBranches = api.model.companyBranches();
	var userOrganizationalPrivileges = userCtrl.getUserOrgPrivileges($.session.getUsername());
	var associatedCompanies = companyBranches.associatedCompaniesTable.getAssociatedCompaniesWithoutPrivileges();
	var userOP = Object.keys(userOrganizationalPrivileges);
	var newUserOP = {};
	userOP.forEach(function (key) {
		var cp = associatedCompanies.filter(function (item) {
			return item.codEmpresa === key;
		});
		if (cp && cp.length > 0) {
			newUserOP[key] = userOrganizationalPrivileges[key];
		}
	});
	if (associatedCompanies && associatedCompanies.length === 0) {
		newUserOP = userOrganizationalPrivileges;
	}
	return newUserOP;
};

const getLoggedUser = function () {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;
	var userInformation = userModel.find({
		select: [{
			field: 'id'
		}, {
			field: 'hanaUser'
		}, {
			field: 'name'
		}, {
			field: 'lastName'
		}, {
			field: 'isAdmin'
		}],
		where: [{
			field: 'hanaUser',
			operator: '$eq',
			value: $.session.getUsername()
		}]
	});
	return userInformation.results[0];
};

const getUserComponentPrivileges = function (componentName, nameStartSpecificPrivilege) {
	$.import('timp.core.server.controllers.refactor', 'user');
	const userCtrl = $.timp.core.server.controllers.refactor.user;
	var userComponentPrivileges = userCtrl.getUserPrivileges({
		user: $.session.getUsername(),
		component: componentName,
		applyFormat: true,
		nameStartSpecificPrivilege: nameStartSpecificPrivilege
	});
	return userComponentPrivileges;
};

const getIsOnCloud = function () {
	$.import('timp.core.server.controllers.refactor', 'configuration');
	const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
	var isOnCloud = configurationCtrl.getSystemConfiguration({
		componentName: 'CORE',
		keys: ['TIMP::TDFIntegration']
	})[0];
	if (isOnCloud) {
		isOnCloud = !$.lodash.isEqual($.lodash.indexOf([false, 'false'], isOnCloud.value), -1);
	} else {
		isOnCloud = false;
	}
	return isOnCloud;
};

const getLogonType = function () {
	$.import('timp.core.server.controllers.refactor', 'configuration');
	const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
	var isSingleSignOn = configurationCtrl.getSystemConfiguration({
		componentName: 'CORE',
		keys: ['TIMP::LogonType']
	})[0];
	if (isSingleSignOn) {
		isSingleSignOn = isSingleSignOn.value;
	}
	return isSingleSignOn;
};

const listTaxes = function () {
	let atrApi = $.getApi('atr', 'tax');
	let taxesModel = atrApi.model.tributo().table;
	let taxes = taxesModel.getTributos() || [];
	const response = taxes.map(function (elem) {
		return {
			key: elem.codTributo,
			name: elem.descrCodTributoLabel,
			isFederal: elem.isFederal,
			taxClassification: elem.taxClassification
		};
	});
	return response;
};

const ValidateAlertsDelete = function (componentName) {
	$.import('timp.core.server.controllers.refactor', 'configuration');
	const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
	try {
		if (componentName !== 'LOGIN') {
			$.import('timp.core.server.orm', 'sql');
			var orm = $.timp.core.server.orm.sql;
			let systemConfiguration = configurationCtrl.getSystemConfiguration({
				componentName: 'CORE',
				keys: ['TIMP::DeleteDateLog', 'TIMP::DeleteTimeLog', 'TIMP::DeleteDateAncilliaryObligations', 'TIMP::DeleteTimeAncilliaryObligations']
			});
			if (systemConfiguration[0] && systemConfiguration[1] && systemConfiguration[2] && systemConfiguration[3]) {
				let dataADM = {
					deleteTimeLog: systemConfiguration[0].value,
					deleteTimeAncilliaryObligations: systemConfiguration[1].value,
					deleteDateLog: systemConfiguration[2].value,
					deleteDateAncilliaryObligations: systemConfiguration[3].value
				};
				let sql;
				let schema = $.schema.slice(1, -1);
				sql = 'SELECT ID FROM ' + schema + '."CORE::ALERT_DELETE_LOG" WHERE ID=' + $.getUserId();
				if (orm.SELECT({
					query: sql
				}).length > 0) {
					let yearDeleteLog = $.moment(dataADM.deleteDateLog, 'YYYYMMDD').year() - dataADM.deleteTimeLog;
					sql = 'DELETE FROM ' + schema + '."CORE::ALERT_DELETE_LOG" WHERE ID=' + $.getUserId();
					orm.EXECUTE({
						query: sql
					});
					$.messageCodes.push({
						code: 'CORE000200',
						aditionalText: yearDeleteLog,
						type: 'S'
					});
				}
				sql = 'SELECT ID FROM ' + schema + '."CORE::ALERT_DELETE_FILES" WHERE ID=' + $.getUserId();
				if (orm.SELECT({
					query: sql
				}).length > 0) {
					let yearDeleteFiles = $.moment(dataADM.deleteDateAncilliaryObligations, 'YYYYMMDD').year() - dataADM.deleteTimeAncilliaryObligations;
					sql = 'DELETE FROM ' + schema + '."CORE::ALERT_DELETE_FILES" WHERE ID=' + $.getUserId();
					orm.EXECUTE({
						query: sql
					});
					$.messageCodes.push({
						code: 'CORE000201',
						aditionalText: yearDeleteFiles,
						type: 'S'
					});
				}
			}
		}
	} catch (e) {
		return e;
	}
};

this.getCoreData = function (object) {
	try {
		object = object || {};
		if ($.lodash.isString(object.componentName)) {
			ValidateAlertsDelete(object.componentName);
			let response = {};
			if ($.lodash.isEqual($.lodash.indexOf(_specialCases, object.componentName), -1)) {
				response = processComponentData(object.componentName);
			} else {
				response = processSpecialCases(object.componentName);
			}
			response.version = $.version;
			return response;
		} else {
			return false;
			//throw bad request
		}
	} catch (e) {
		return $.parseError(e);
	}
};

this.setSessionInformation = function () {
	if ($.lodash.isEmpty($.session.getUsername())) {
		return {
			loggedUser: false
		};
	} else {
		var cookieLanguage = $.request.cookies.get('Content-Language');
		if ($.lodash.isEmpty(cookieLanguage)) {
			$.response.cookies.set('Content-Language', 'ptrbr');
		}
		return {
			loggedUser: true,
			loggedUsername: $.session.getUsername()
		};
	}
};

this.getTableInformation = function () {
	var model = $.createBaseRuntimeModel('SAPABAP2', '/TMF/D_NF_IMPOST').getDefinition();
	return model;
};

/**
 * @description Recibe un array de UF's retorna todas las cuidades filtradas por las UF's recibidas, si no recibe uf retornará todas las cuidades
 * @param   {[]]}   states
 * @return  {[{}]} -
 */
this.getCitiesByStates = function (states) {
	$.import('timp.atr.server.api', 'api');
	const cityModel = $.timp.atr.server.api.api.cepCidade.table;
	var response = [];
	let where = [];
	if (Array.isArray(states) && states.length) {
		where.push({
			field: 'ufeSg',
			oper: '=',
			value: states
		});
	}
	response = cityModel.READ({
		fields: [ 'id', 'cep', 'locInSit', 'locInTipoLoc', 'locNo', 'locNoBrev', 'ufeSg'],
		where: where
	});
	response = response.map(function (elem) {
		elem.key = elem.id;
		elem.name = elem.id + ' - ' + elem.ufeSg + ' - ' + elem.locNo;
		return elem;
	});
	return response;
};