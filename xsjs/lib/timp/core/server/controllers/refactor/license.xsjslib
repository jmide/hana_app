//CORE Services
$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

//CORE Models
$.import('timp.core.server.models.tables', 'configuration');
const licenseModel = $.timp.core.server.models.tables.configuration.licenseModel;

$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;

//Util Libraries
const _ = $.lodash;

const TAX_ACTIVATION_CUSTOMER = '01';
const TOTAL_ACTIVATION_CUSTOMER = '02';


/**
 * @description Function that recieves a key and runs a handmade hash generator function that creates TIMP Licenses.
 * @param   { string } key - Key that contains system host, instance name, user count and expiration
 * @returns { string } License has based on passed key
*/
var generateLicenseHash = function (key) {
    var value = 0;
    var weight = 0;
    for (var i = 0; i < key.length; i++) {
        weight += key.charCodeAt(i);
        value += String(Math.ceil(key.charCodeAt(i) * Math.exp((Math.sin(weight) * 2), 2)));
    }
    var hash = '';
    var chars = '90qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'.split('');
    var positions = [];
    while (positions.length < 128) {
        positions.push(0);
    }
    while (value.length < 768) {
        value += value;
    }
    value = value.slice(0, 768);
    for (var k = 0; k < value.length; k++) {
        let pos = k;
        while (pos >= positions.length) {
            pos -= positions.length;
        }
        positions[pos] += Number(value[k]);
    }
    for (var p = 0; p < positions.length; p++) {
        hash += chars[positions[p]];
        chars.reverse();
    }
    return hash;
};


/**
 * @description Function to calculate if there are available licenses based on endpoint parameter and current active users
 * @param {number} amountOfNeededLicenses - Positive integer value that represent the amount of needed licenses, mandatory
 * @returns {boolean} response - True - When enough licenses are available based on active licenses, current active users, requested licenses
 * False - When not enough licenses are available
 */
var checkAvailableLicenses = function (amountOfNeededLicenses) {
    if (_.isInteger(amountOfNeededLicenses)) {
        try {
            let availableUsers = licenseModel.find({
                select: [{
                    aggregation: {
                        type: 'SUM',
                        param: {
                            field: 'availableUsers'
                        }
                    },
                    as: 'availableUsers'
                }],
                where: [{
                    field: 'isActive',
                    operator: '$eq',
                    value: 1
                }]
            }).results[0];
            let activeUsers = userModel.find({
                count: true,
                select: [{
                    field: 'id'
                }],
                where: [{
                    field: 'isActive',
                    operator: '$eq',
                    value: 1
                }]
            }).results[0];
            if (_.isArray(availableUsers.errors) && !_.isEmpty(availableUsers.errors) && _.isArray(activeUsers.errors) && !_.isEmpty(
                activeUsers.errors)) {
                return false;
            }
            return (availableUsers.availableUsers - activeUsers.tableCount - amountOfNeededLicenses > -1) ? true : false;
        } catch (e) {
            return false;
        }
    } else {
        return false;
    }
};
this.checkAvailableLicenses = checkAvailableLicenses;


/**
 * @description Function that validates the license passed
 * @param   { string }  license - License you want to check
 * @param   { integer } userQuantity - Number of users the license has
 * @param   { string }  expiration - Expiration date of the license
 * @returns { boolean } Returns true if valid, false if not
*/
var validateSystemLicense = function (license, userQuantity, expiration) {
    var serverInformation = connector.execute('SELECT DATABASE_NAME, HOST FROM "M_DATABASE"').results[0];
    if (_.isObjectLike(serverInformation) && !_.isEmpty(serverInformation)) {
        let key = serverInformation.HOST + ':' + serverInformation.DATABASE_NAME + ':' + userQuantity + ':' + expiration;
        let generatedKey = generateLicenseHash(key);
        if (_.isEqual(generatedKey, license)) {
            return true;
        }
    }
    return false;
};
this.validateSystemLicense = validateSystemLicense;


/** 
 * @description Function that validates the license passed
 * @param   { string }  license - License you want to check
 * @param   { integer } userQuantity - Number of users the license has
 * @param   { string }  expiration - Expiration date of the license
 * @param   { string }  taxLabel - Tax label of the license
 * @returns { boolean } Returns true if valid, false if not
 */
var isValidTaxLicense = function (license, userQuantity, expiration, taxLabel) {
    var serverInformation = connector.execute('SELECT DATABASE_NAME, HOST FROM "M_DATABASE"').results[0];
    if (_.isObjectLike(serverInformation) && !_.isEmpty(serverInformation)) {
        let key = serverInformation.HOST + ':' + serverInformation.DATABASE_NAME + ':' + userQuantity + ':' + expiration + ':' + taxLabel;
        license = license.replace(' ', '');
        let generatedKey = generateLicenseHash(key);
        if (_.isEqual(generatedKey, license)) {
            return true;
        }
    }
    return false;
};
this.isValidTaxLicense = isValidTaxLicense;


/**
 * @description Service that is meant to create, update and delete licenses (mark inactive);
 * @param   { Object }  license - Contains license definition
 * @param   { integer } license.id - Optional if you want to update the license
 * @param   { string }  license.license - Contains the license hash you want to activate
 * @param   { integer } license.userQuantity - Contains the amount of users for the license
 * @param   { string }  license.expiration - Contains the expiration date for the license
 * @returns { Object }  Returns an object with the created/update license or error array
 */
var saveLicense = function (license) {
    var response = {
        errors: [],
        results: []
    };
    var errorGenerator = new $.ErrorGenerator('saveLicense');
    try {
        var isValidParameter = $.validator.validate(license, licenseModel.schema);
        if (isValidParameter.valid) {
            const isLicenseValid = validateSystemLicense(license.license, license.userQuantity, license.expiration);
            if (isLicenseValid) {
                let duplicateLicenses = licenseModel.find({
                    count: true,
                    where: [{
                        field: 'license',
                        operator: '$eq',
                        value: license.license
                    }]
                }).results[0];
                const isLicenseDuplicate = !_.isEqual(duplicateLicenses.tableCount, 0);
                // CREATE LICENSE
                if (!isLicenseDuplicate && _.isNil(license.id)) {
                    let createdLicense = licenseModel.create({
                        license: license.license,
                        availableUsers: license.userQuantity,
                        expiration: license.expiration,
                        licenseType: license.licenseType
                    });
                    if (license.licenseType === TAX_ACTIVATION_CUSTOMER) {
                        if (license.taxLicenses && license.taxLicenses.length) { // save tax licenses
                            var newLicenseObj = createdLicense.results.created ? createdLicense.results.instance : null;
                            saveTaxLicenses({
                                license: newLicenseObj,
                                taxLicenses: license.taxLicenses
                            });
                        }
                    }
                    if (_.isEmpty(createdLicense.errors)) { //Log success license insert
                        response.results = createdLicense.results;
                        $.messageCodes.push({
                            code: 'CORE000048',
                            type: 'S'
                        });
                        return response;
                    } else {
                        response.errors = createdLicense.errors;
                        $.messageCodes.push({
                            code: 'CORE000049',
                            type: 'E'
                        });
                        return response;
                    }
                    // UPDATE LICENSE
                } else if (_.isInteger(license.id)) {
                    let condition = {
                        where: [{
                            field: 'id',
                            operator: '$eq',
                            value: license.id
                        }]
                    };
                    let updatedLicense = licenseModel.update({
                        license: license.license,
                        availableUsers: license.userQuantity,
                        expiration: license.expiration,
                        isActive: license.isActive,
                        licenseType: license.licenseType
                    },
                        condition,
                        true
                    );
                    if (license.licenseType === TAX_ACTIVATION_CUSTOMER) {
                        if (license.taxLicenses && license.taxLicenses.length) { // save tax licenses
                            var _newLicenseObj = updatedLicense.results.updated ? updatedLicense.results.instances[0] : null;
                            saveTaxLicenses({
                                license: _newLicenseObj,
                                taxLicenses: license.taxLicenses
                            });
                        }
                    } else if (license.licenseType === TOTAL_ACTIVATION_CUSTOMER) {
                        if (license.taxLicenses && !license.taxLicenses.length) { // activate all taxes
                            activateTaxesForLicenseType();
                        }
                    }
                    if (_.isEmpty(updatedLicense.errors)) { //Log success license insert
                        response.results = updatedLicense.results.updated ? updatedLicense.results.instances[0] : null;
                        $.messageCodes.push({
                            code: 'CORE000048',
                            type: 'S'
                        });
                        return response;
                    } else {
                        response.errors = updatedLicense.errors;
                        $.messageCodes.push({
                            code: 'CORE000049',
                            type: 'E'
                        });
                        return response;
                    }
                    // NONE
                } else {
                    $.messageCodes.push({
                        text: 'License is duplicated',
                        type: 'E'
                    });
                    response.errors = ['License is duplicated'];
                    return response;
                }
            } else {
                $.messageCodes.push({
                    text: 'License is not valid',
                    type: 'E'
                });
                response.errors = ['License is not valid'];
                return response;
            }
        } else {
            response.errors = isValidParameter.errors;
        }
    } catch (exception) {
        response.errors.push($.parseError(exception));
        $.messageCodes.push({
            code: 'CORE000049',
            type: 'E'
        });
        throw errorGenerator.internalError(exception);
    }
    return response;
};
this.saveLicense = saveLicense;


/**
 * @description List licences
 * @return { Array } - An array of license objects
 */
this.listLicenses = function () {
    var response = [];
    response = licenseModel.find({}).results;
    response.forEach(function (elem) {
        if (elem && elem.id && elem.licenseType && elem.licenseType === TAX_ACTIVATION_CUSTOMER) {
            elem.taxLicenses = listTaxLicenses({ idLicense: elem.id });
        }
    });
    return response;
};


/**
 * @description List tax licences 
 * @param   { Object } object - Object with idLicense property
 * @return  { Array } - An array of taxLicense objects
 */
var listTaxLicenses = function (object) {
    $.import('timp.core.server.models.tables', 'configuration');
    const taxLicenseModel = $.timp.core.server.models.tables.configuration.taxLicenseModel;
    const taxModelRuntime = $.createBaseRuntimeModel($.schema.slice(1, -1), 'ATR::Tributo');

    var response = [];
    const options = {
        select: [{
            field: 'id',
            alias: 'taxLicenseModel'
        }, {
            field: 'idLicense',
            alias: 'taxLicenseModel'
        }, {
            field: 'idTax',
            alias: 'taxLicenseModel'
        }, {
            field: 'license',
            alias: 'taxLicenseModel'
        }, {
            field: 'isActive',
            alias: 'taxLicenseModel'
        }, {
            field: 'COD_TRIBUTO',
            as: 'taxCode',
            alias: 'taxModel'
        }, {
            field: 'DESCR_COD_TRIBUTO_LABEL',
            as: 'taxLabel',
            alias: 'taxModel'
        }],
        aliases: [{
            collection: taxLicenseModel.getIdentity(),
            isPrimary: true,
            name: 'taxLicenseModel'
        }, {
            collection: taxModelRuntime.getIdentity(),
            name: 'taxModel'
        }],
        join: [{
            alias: 'taxModel',
            type: 'inner',
            map: 'taxModel',
            on: [{
                alias: 'taxModel',
                field: 'ID',
                operator: '$eq',
                value: {
                    alias: 'taxLicenseModel',
                    field: 'idTax'
                }
            }]
        }],
        where: [{
            field: 'idLicense',
            operator: '$eq',
            value: object.idLicense
        }],
        orderBy: [{
            field: 'idTax',
            alias: 'taxLicenseModel',
            type: 'asc'
        }]
    };
    response = taxLicenseModel.find(options).results;
    response = response.map(function (elem) {
        return {
            id: elem.id,
            idLicense: elem.idLicense,
            idTax: elem.idTax,
            isActive: elem.isActive,
            license: elem.license,
            taxCode: elem.taxModel[0].taxCode,
            taxLabel: elem.taxModel[0].taxLabel
        };
    });
    return response;
};
this.listTaxLicenses = listTaxLicenses;


/**
 * @description Save tax licences
 * @param   { Object } object - Object with taxLicenses Array property 
 * @return  { Array } - An array of taxLicense objects
 */
var saveTaxLicenses = function (object) {
    $.import('timp.core.server.models.tables', 'configuration');
    const taxLicenseModel = $.timp.core.server.models.tables.configuration.taxLicenseModel;

    $.import('timp.atr.server.api', 'api');
    const atrApi = $.timp.atr.server.api.api;
    const taxModel = atrApi.tributo.table;

    var response = [];
    try {
        if (object.taxLicenses && object.taxLicenses.length) {
            object.taxLicenses.forEach(function (elem) {
                const isValid = isValidTaxLicense(elem.license, object.license.availableUsers, object.license.expiration, elem.taxLabel);
                if (elem.id) {
                    if (!isValid) {
                        taxModel.toggleTax(elem.idTax, false);
                        return;
                    }
                    const options = {
                        idLicense: object.license.id,
                        idTax: elem.idTax,
                        license: elem.license,
                        isActive: elem.isActive
                    };
                    const where = {
                        where: [{
                            field: 'id',
                            operator: '$eq',
                            value: elem.id
                        }]
                    };
                    let instance = taxLicenseModel.update(options, where, true);
                    if (instance.results.updated) {
                        let x = taxModel.toggleTax(elem.idTax, elem.isActive);
                        response.push(instance.results.instances[0]);
                    }
                } else {
                    if (!isValid) {
                        taxModel.toggleTax(elem.idTax, false);
                        return;
                    }
                    const options = {
                        idLicense: object.license.id,
                        idTax: elem.idTax,
                        license: elem.license,
                        isActive: elem.isActive
                    };
                    let instance = taxLicenseModel.create(options);
                    if (instance.results.created) {
                        let y = taxModel.toggleTax(elem.idTax, elem.isActive);
                        response.push(instance.results.instance);
                    }
                }
            });
        }
        return response;
    } catch (err) {
        return null;
    }
};
this.saveTaxLicenses = saveTaxLicenses;


/**
 * @description This function activate taxes if there are complete licenses or partial tax licenses that includes them.
 * @returns { Boolean } - True if the process was completed succesfully, false if not.
 */
var activateTaxesForLicenseType = function () {
    $.import('timp.atr.server.api', 'api');
    const atrApi = $.timp.atr.server.api.api;
    const taxModel = atrApi.tributo.table;
    var taxes = [];
    var licenses = [];
    var taxLicenses = [];
    try {
        taxes = taxModel.getAllTaxes();
        licenses = this.listLicenses();
        let totalActivationLicenses = licenses.filter(function (elem) {
            return elem.licenseType && elem.licenseType === TOTAL_ACTIVATION_CUSTOMER && elem.isActive; // '02'
        });
        let taxActivationLicenses = licenses.filter(function (elem) {
            return elem.licenseType && elem.licenseType === TAX_ACTIVATION_CUSTOMER && elem.isActive; // '01'
        });
        const hasTotalActivation = totalActivationLicenses && totalActivationLicenses.length ? true : false;
        const hasTaxesActivation = taxActivationLicenses && taxActivationLicenses.length ? true : false;
        if (hasTotalActivation) {
            taxModel.activateAllTaxes();
        } else if (hasTaxesActivation) {
            if (hasTotalActivation) {
                taxModel.activateAllTaxes();
            } else {
                taxActivationLicenses.forEach(function (license) {
                    const hasTaxLicenses = license && license.taxLicenses && license.taxLicenses.length;
                    if (hasTaxLicenses) {
                        taxLicenses = taxLicenses.concat(license.taxLicenses);
                    }
                });
                taxes.forEach(function (tax) {
                    const idTax = tax.id;
                    let taxLicensesActive = taxLicenses.filter(function (elem) { return elem.idTax === idTax && elem.isActive; });
                    if (taxLicensesActive && taxLicensesActive.length) {
                        taxModel.toggleTax(taxLicensesActive[0].idTax, taxLicensesActive[0].isActive);
                    }
                });
            }
        } else {
            taxModel.deactivateAllTaxes();
        }
        return true;
    } catch (err) {
        return false;
    }
};
this.activateTaxesForLicenseType = activateTaxesForLicenseType;