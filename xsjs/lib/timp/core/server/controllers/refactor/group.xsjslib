const model = {
	groupTaxes: function () {
		$.import('timp.core.server.models.tables', 'GroupTaxes');
		return $.timp.core.server.models.tables.GroupTaxes;
	}
};

$.import('timp.core.server.models.tables', 'group');
const groupModel = $.timp.core.server.models.tables.group.groupModel;
const groupUserModel = $.timp.core.server.models.tables.group.groupUserModel;
const groupPrivilegeModel = $.timp.core.server.models.tables.group.groupPrivilegeModel;
const groupPackageModel = $.timp.core.server.models.tables.group.groupPackageModel;
const groupOrgPrivilegeModel = $.timp.core.server.models.tables.group.groupOrgPrivilegeModel;

$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;
const userModelFile = $.timp.core.server.models.tables.user;

$.import('timp.core.server.models.tables', 'component');
const privilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

$.import('timp.core.server.controllers.refactor', 'component');
const componentCtrl = $.timp.core.server.controllers.refactor.component;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;

$.import('timp.core.server.models.tables', 'package');
const packageModel = $.timp.core.server.models.tables.package.packageModel;
const packageModelFile = $.timp.core.server.models.tables.package;

const _this = this;
const _saveGroupSchema = {
	properties: {
		id: {
			type: 'integer',
			allowEmpty: false
		},
		name: {
			type: 'string',
			required: true,
			allowEmpty: false
		},
		description: {
			type: 'string',
			required: true,
			allowEmpty: true
		},
		leaderId: {
			type: 'integer',
			required: true,
			allowEmpty: false
		},
		isActive: {
			type: 'boolean',
			required: true,
			allowEmpty: false
		},
		grcRole: {
			type: 'string',
			allowEmpty: true
		},
		orgPrivileges: {
			type: 'array',
			required: true,
			allowEmpty: true
		},
		privileges: {
			type: 'array',
			required: true,
			allowEmpty: true
		},
		packages: {
			type: 'array',
			required: true,
			allowEmpty: true
		},
		users: {
			type: 'array',
			required: true,
			allowEmpty: true
		},
		taxes: {
			type: 'array',
			required: true,
			allowEmpty: true
		}
	}
};

this.getGroupPrivileges = function (groupId) {
	var response = [];
	let errorGenerator = new $.ErrorGenerator('getGroupPrivileges');

	try {
		let selectOption = [{
			field: 'name',
			as: 'privilegeName',
			alias: 'Privilege'
		}, {
			field: 'id',
			as: 'privilegeId',
			alias: 'Privilege'
		}, {
			field: 'name',
			as: 'componentName',
			alias: 'Component'
		}];

		if ($.request.cookies.get('Content-Language') === 'enus') {
			selectOption.push({
				field: 'labelEnus',
				alias: 'Component',
				as: 'label'
			});
		} else {
			selectOption.push({
				field: 'labelPtbr',
				alias: 'Component',
				as: 'label'
			});
		}

		response = componentModel.find({
			//simulate: true,
			aliases: [{
				collection: componentModel.getIdentity(),
				name: 'Component',
				isPrimary: true
			}, {
				collection: privilegeModel.getIdentity(),
				name: 'Privilege'
			}, {
				collection: groupPrivilegeModel.getIdentity(),
				name: 'GroupPrivilege'
			}],
			select: selectOption,
			join: [{
				alias: 'Privilege',
				type: 'inner',
				map: 'privileges',
				on: [{
					alias: 'Privilege',
					field: 'componentId',
					operator: '$eq',
					value: {
						'alias': 'Component',
						'field': 'id'
					}
				}]
			}, {
				alias: 'GroupPrivilege',
				type: 'inner',
				on: [{
					alias: 'GroupPrivilege',
					field: 'privilegeId',
					operator: '$eq',
					value: {
						'alias': 'Privilege',
						'field': 'id'
					}
				}]
			}],
			where: [{
				field: 'groupId',
				alias: 'GroupPrivilege',
				operator: '$eq',
				value: groupId
			}]
		}).results;

	} catch (e) {
		throw errorGenerator.internalError(e);
	}

	return response;
};

this.getOrgPrivileges = function () {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('getOrgPrivileges');
	try {
		let cvCompanyModel = $.createBaseRuntimeModel('_SYS_BIC', 'timp.atr.modeling.emp_fil/EMPRESA', true);
		let cvBranchModel = $.createBaseRuntimeModel('_SYS_BIC', 'timp.atr.modeling.emp_fil/FILIAL', true);
		if ($.lodash.isArray(cvCompanyModel.errors) && $.lodash.isEmpty(cvCompanyModel.errors)) {
			var options = {
				//   simulate: true,
				aliases: [{
					collection: cvCompanyModel.getIdentity(),
					name: 'company',
					isPrimary: true
				}, {
					collection: cvBranchModel.getIdentity(),
					name: 'branch'
				}],
				select: [{
					field: 'COD_EMP',
					alias: 'company',
					as: 'id'
				}, {
					field: 'NOME_EMPRESA',
					alias: 'company',
					as: 'name'
				}, {
					field: 'COD_FILIAL',
					alias: 'branch',
					as: 'id'
				}, {
					field: 'NOME_FILIAL',
					alias: 'branch',
					as: 'name'
				}, {
					field: 'UF_FILIAL',
					alias: 'branch',
					as: 'state'
				}],
				join: [{
					alias: 'branch',
					type: 'inner',
					map: 'branches',
					on: [{
						alias: 'branch',
						field: 'COD_EMP',
						operator: '$eq',
						value: {
							alias: 'company',
							field: 'COD_EMP'
						}
					}]
				}]
			};

			cvCompanyModel.find(options).results.forEach(function (elem) {
				response[elem.id] = elem;
				delete response[elem.id].id;
			});
		}
	} catch (e) {
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.saveGroup = function (object) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('saveGroup');
	try {
		if (object != undefined) {
			let isValid = $.validator.validate(object, _saveGroupSchema);
			if (isValid.valid) {
				if (object.id == null) {
					response = groupModel.create({
						name: object.name,
						description: object.description,
						leaderId: object.leaderId,
						isActive: object.isActive,
						grcRole: object.grcRole
					});

					if (Array.isArray(response.errors) && response.errors.length == 0) {
						object.id = response.results.instance.id;
						$.messageCodes.push({
							'code': 'CORE000056',
							'type': 'S'
						});
					} else {
						$.messageCodes.push({
							'code': 'CORE000057',
							'type': 'E'
						});
						return response;
					}
				} else {
					response = groupModel.update({
						name: object.name,
						description: object.description,
						leaderId: object.leaderId,
						isActive: object.isActive,
						grcRole: object.grcRole
					}, {
						where: [{
							field: 'id',
							operator: '$eq',
							value: object.id
						}]
					}, true);

					if ($.lodash.isArray(response.errors) && $.lodash.isEmpty(response.errors)) {
						$.logSuccess('CORE000056');
					} else {
						$.logError('CORE000057');
						return response;
					}
				}
				response = saveGroupComplementaryData(response, object);
			} else {
				throw errorGenerator.paramsError();
			}
		}
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000057',
			'type': 'E'
		});
		if (object.id) {
			throw errorGenerator.updateError(e, null, {
				objectId: object.id
			});
		}
		throw errorGenerator.saveError(e);
	}
	return response;
};

let saveGroupComplementaryData = function (response, object) {
	if (!(Array.isArray(response.errors) && response.errors.length == 0)) {
		return response;
	}
	let groupId = object && object.id ? object.id : response.results.instance.id;
	let { users, privileges, packages, orgPrivileges, taxes } = object;

	response.users = saveGroupUsers(users, groupId);
	response.privileges = saveGroupPrivileges(privileges, groupId);
	response.packages = saveGroupPackages(packages, groupId);
	response.orgPrivileges = saveGroupOrganizationalPrivileges(orgPrivileges, groupId);
	response.taxes = saveGroupTaxes(taxes, groupId);
	if (
		response.orgPrivileges || response.packages ||
		response.privileges || response.users || response.taxes
	) {
		$.messageCodes.push({
			'code': 'CORE000067',
			'type': 'S'
		});
	}
	return response;
};

let saveGroupUsers = function (users, groupId) {
	groupUserModel.delete({
		where: [{
			field: 'groupId',
			operator: '$eq',
			value: groupId
		}]
	});

	if (users.length == 0) {
		return true;
	}

	users = users.map(function (elem) {
		return {
			groupId: groupId,
			userId: elem
		};
	});

	users = groupUserModel.batchCreate(users).results.created;

	return users;
};

let saveGroupPrivileges = function (privileges, groupId) {
	groupPrivilegeModel.delete({
		where: [{
			field: 'groupId',
			operator: '$eq',
			value: groupId
		}]
	});
	if (privileges.length == 0) {
		return true;
	}
	privileges = privileges.map(function (elem) {
		return {
			groupId: groupId,
			privilegeId: elem
		};
	});
	privileges = groupPrivilegeModel.batchCreate(privileges).results.created;
	return privileges;
};

let saveGroupPackages = function (packages, groupId) {
	groupPackageModel.delete({
		where: [{
			field: 'groupId',
			operator: '$eq',
			value: groupId
		}]
	});

	if (packages.length == 0) {
		return true;
	}

	packages = packages.map(function (elem) {
		return {
			groupId: groupId,
			packageId: elem
		};
	});

	packages = groupPackageModel.batchCreate(packages).results.created;

	return packages;
};

let saveGroupOrganizationalPrivileges = function (orgPrivileges, groupId) {
	groupOrgPrivilegeModel.delete({
		where: [{
			field: 'groupId',
			operator: '$eq',
			value: groupId
		}]
	});

	if (orgPrivileges.length == 0) {
		return true;
	}

	orgPrivileges = orgPrivileges.map(function (elem) {
		return {
			groupId: groupId,
			companyId: elem.companyId,
			branchId: elem.branchId
		};
	});

	orgPrivileges = groupOrgPrivilegeModel.batchCreate(orgPrivileges).results.created;
	return orgPrivileges;
};

let saveGroupTaxes = function (taxes, groupId) {
	let modelGroupTaxes = model.groupTaxes();

	modelGroupTaxes.delete({
		where: [{
			field: 'group',
			operator: '$eq',
			value: Number(groupId)
		}]
	});

	if (taxes.length == 0) {
		return true;
	}

	taxes = taxes.map(function (tax) {
		return {
			group: Number(groupId),
			tax: tax
		};
	});

	let response = modelGroupTaxes.batchCreate(taxes);
	let isSuccess = response.results.created;
	return isSuccess;
};
let getGroupTaxes = function (user) {
	let modelGroupTaxes = model.groupTaxes();
	let groupTaxes = modelGroupTaxes.getGroupTaxesByUser({ user });
	return groupTaxes;
};

this.getGroupInformation = function (groupId) {
	let errorGenerator = new $.ErrorGenerator('getGroupInformation');
	var response = {
		orgPrivileges: {},
		groupPackages: [],
		groupUsers: [],
		groupPrivileges: [],
		groupTaxes: []
	};
	try {
		if (groupId != undefined) {
			response.metadata = _this.getDefaultFieldsData(groupModel.find({
				where: [{
					field: 'id',
					operator: '$eq',
					value: groupId
				}]
			}).results)[0];

			if (response.metadata != undefined) {
				groupOrgPrivilegeModel.find({
					where: [{
						field: 'groupId',
						operator: '$eq',
						value: groupId
					}]
				}).results.forEach(function (row) {
					if (!$.lodash.isNil(response.orgPrivileges[row.companyId])) {
						response.orgPrivileges[row.companyId].branches.push({
							state: row.stateId,
							id: row.branchId
						});
					} else {
						response.orgPrivileges[row.companyId] = {
							branches: []
						};
						response.orgPrivileges[row.companyId].branches.push({
							state: row.stateId,
							id: row.branchId
						});
					}
				});

				response.groupPackages = packageModel.find({
					'aliases': [{
						'collection': packageModel.getIdentity(),
						'name': 'Package',
						'isPrimary': true
					}, {
						'collection': groupPackageModel.getIdentity(),
						'name': 'GroupPackage'
					}],
					'select': [{
						field: 'name',
						alias: 'Package'
					}, {
						field: 'description',
						alias: 'Package'
					}, {
						field: 'id',
						alias: 'Package',
						as: 'packageId'
					}],
					'join': [{
						alias: 'GroupPackage',
						type: 'inner',
						on: [{
							alias: 'GroupPackage',
							field: 'packageId',
							operator: '$eq',
							value: {
								'alias': 'Package',
								'field': 'id'
							}
						}]
					}],
					'where': [{
						field: 'groupId',
						alias: 'GroupPackage',
						operator: '$eq',
						value: groupId
					}]
				}).results;

				response.groupPrivs = _this.getGroupPrivileges(groupId);

				response.groupUsers = userModel.find({
					aliases: [{
						collection: userModel.getIdentity(),
						name: 'User',
						isPrimary: true
					}, {
						collection: groupUserModel.getIdentity(),
						name: 'GroupUser'
					}],
					select: [{
						field: 'id',
						alias: 'User',
						as: 'userId'
					}, {
						field: 'name',
						alias: 'User'
					}, {
						field: 'lastName',
						alias: 'User'
					}],
					join: [{
						alias: 'GroupUser',
						type: 'inner',
						on: [{
							alias: 'GroupUser',
							field: 'userId',
							operator: '$eq',
							value: {
								alias: 'User',
								field: 'id'
							}
						}]
					}],
					where: [{
						field: 'groupId',
						alias: 'GroupUser',
						operator: '$eq',
						value: groupId
					}]
				}).results;

				response.groupTaxes = getGroupTaxes(groupId);
				response.metadata.leaderId = $.lodash.find(response.groupUsers, function (elem) {
					return elem.userId === response.metadata.leaderId;
				});

			}
		} else {
			$.logError('CORE000024');
			throw errorGenerator.paramsError(['groupId']);
		}

	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E',
			'errorInfo': $.parseError(e)
		});
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.getRequiredInformation = function (action, groupId) {
	let errorGenerator = new $.ErrorGenerator('getRequiredInformation');
	let apiAtr = $.getApi('atr');
	var response = {
		listOrgPrivileges: {},
		listPackages: [],
		listComponents: [],
		listUsers: [],
		taxes: []
	};

	try {
		if (!$.lodash.isNil(action) && action === 'visualize') {
			if (!$.lodash.isNil(groupId) && $.lodash.isNumber(groupId)) {
				var groupInformation = _this.getGroupInformation(groupId);
				response.metadata = groupInformation.metadata;
				response.listOrgPrivileges = groupInformation.orgPrivileges;
				response.listPackages = groupInformation.groupPackages;
				response.listUsers = groupInformation.groupUsers;
				response.listComponents = groupInformation.groupPrivs;
				response.taxes = groupInformation.groupTaxes;
			} else {
				$.messageCodes.push({
					'code': 'CORE000024', // Invalid Parameter - Group ID is not valid or wasn't sent
					'type': 'E'
				});
				return response;
			}
		} else {
			response.listOrgPrivileges = _this.getOrgPrivileges();
			response.listPackages = packageModelFile.getPackagesList();
			response.listComponents = componentCtrl.getComponentPrivileges();
			response.listUsers = userModelFile.getUsersList();
			response.taxes = apiAtr.tributo.listTaxes();
			response = addEditGroupData(response, groupId);
		}
	} catch (e) {
		$.logError('CORE000021', e);
		throw errorGenerator.internalError(e);
	}

	return response;
};

let addEditGroupDataOld = function (response, groupId) {
	if ($.lodash.isNumber(groupId)) { 
		var group = _this.getGroupInformation(groupId);
		response.metadata = group.metadata;
 
		Object.keys(group.orgPrivileges).forEach(function(groupComp) {
			if (response.listOrgPrivileges[groupComp]) {
				response.listOrgPrivileges[groupComp].isAssigned = true;
				$.lodash.forEach(response.listOrgPrivileges[groupComp].branches, function(branch) {
					branch.isAssigned = group.orgPrivileges[groupComp].branches.find(function(groupBranch) {
						return $.lodash.isEqual(groupBranch.id, branch.id);
					}) ? true : undefined;
				});
			}
		});
 
		group.groupPackages.forEach(function(groupPkg) {
			response.listPackages.forEach(function(pkg) {
				if (groupPkg.packageId === pkg.id) {
					pkg.isAssigned = true;
				}
			});
		});
 
		group.groupPrivs.forEach(function(gComponent) {
			var idx = $.lodash.findIndex(response.listComponents, function(elem) {
				return elem.componentName === gComponent.componentName;
			});
			if (idx !== -1) {
				response.listComponents[idx].isAssigned = true;
				$.lodash.forEach(gComponent.privileges, function(gPriv) {
					let privIdx = $.lodash.findIndex(response.listComponents[idx].privileges, function(cPriv) {
						return cPriv.privilegeId === gPriv.privilegeId;
					});
					if (privIdx !== -1) {
						response.listComponents[idx].privileges[privIdx].isAssigned = true;
					}
				});
			}
		});
 
		group.groupUsers.forEach(function(gUser) {
			response.listUsers.forEach(function(user) {
				if (gUser.userId === user.id) {
					user.isAssigned = true;
				}
			});
		});
	}
	return response;
 };
let addEditGroupData = function (response, groupId) {
	if (!$.lodash.isNumber(groupId)) {
		return response;
	}

	$.import('timp.core.server.controllers.refactor', 'sharedFunctions');
	let sharedFunctions = $.timp.core.server.controllers.refactor.sharedFunctions;
	var group = _this.getGroupInformation(groupId);
	response.metadata = group.metadata;

	let { listOrgPrivileges, listPackages, listComponents, listUsers, taxes } = response;
	let { orgPrivileges, groupPackages, groupPrivs, groupUsers, groupTaxes } = group;
	response.listOrgPrivileges = sharedFunctions.assignOrganizationalPrivileges(orgPrivileges, listOrgPrivileges);
	response.listPackages = sharedFunctions.assignPackages(groupPackages, listPackages);
	response.listPrivileges = sharedFunctions.assignComponentPrivileges(groupPrivs, listComponents);
	response.listUsers = sharedFunctions.assignUsers(groupUsers, listUsers);
	response.taxes = sharedFunctions.assignTaxes(groupTaxes, taxes);

	return response;
};

this.listGroups = function (object, paginate) {
	let errorGenerator = new $.ErrorGenerator('listGroups');
	var response = {};
	try {
		var options = {
			'where': [],
			'orderBy': [{
				'field': 'name',
				'alias': 'group',
				'type': 'asc'
			}]
		};

		if (!$.lodash.isNil(object)) {
			if (!$.lodash.isNil(object.filters)) {
				if (!$.lodash.isNil(object.filters.id)) {
					options.where.push({
						'field': 'id',
						'operator': '$eq',
						'value': object.filters.id
					});
				}
				if ($.lodash.isArray(object.filters.status) && !$.lodash.isEmpty(object.filters.status)) {
					let status = {
						'active': 1,
						'inactive': 0
					};

					object.filters.status = object.filters.status.map(function (s) {
						return status[s];
					});

					options.where.push({
						'field': 'isActive',
						'operator': '$in',
						'value': object.filters.status
					});
				}

				if ($.lodash.isArray(object.filters.createdBy) && !$.lodash.isEmpty(object.filters.createdBy)) {
					options.where.push({
						'field': 'creationUser',
						'operator': '$in',
						'value': object.filters.createdBy
					});
				}

				if ($.lodash.isArray(object.filters.modifiedBy) && !$.lodash.isEmpty(object.filters.modifiedBy)) {
					options.where.push({
						'field': 'modificationUser',
						'operator': '$in',
						'value': object.filters.modifiedBy
					});
				}
				if (!$.lodash.isNil(object.filters.name) && $.lodash.isString(object.filters.name)) {
					options.where.push({
						'field': 'name',
						'operator': '$contains',
						'value': '%' + object.filters.name + '%'
				// 		'value': {
				// 			'value': object.filters.name,
				// 			'fuzzy': {
				// 				'minimumScore': 0.2
				// 			}
				// 		}
					});
				}

				if ($.lodash.isArray(object.filters.creationDate) && !$.lodash.isEmpty(object.filters.creationDate)) {
					options.where.push({
						'function': {
							'name': 'TO_DATE',
							'params': [{
								'field': 'creationDate'
							}]
						},
						'operator': '$gte',
						'value': {
							'function': {
								'name': 'TO_DATE',
								'params': [object.filters.creationDate[0]]
							}
						}
					});

					if (!$.lodash.isEmpty(object.filters.creationDate[1])) {
						options.where.push({
							'function': {
								'name': 'TO_DATE',
								'params': [{
									'field': 'creationDate'
								}]
							},
							'operator': '$lte',
							'value': {
								'function': {
									'name': 'TO_DATE',
									'params': [object.filters.creationDate[1]]
								}
							}
						});
					}
				}

				if ($.lodash.isArray(object.filters.modificationDate) && !$.lodash.isEmpty(object.filters.modificationDate)) {
					options.where.push({
						'function': {
							'name': 'TO_DATE',
							'params': [{
								'field': 'modificationDate'
							}]
						},
						'operator': '$gte',
						'value': {
							'function': {
								'name': 'TO_DATE',
								'params': [object.filters.modificationDate[0]]
							}
						}
					});

					if (!$.lodash.isEmpty(object.filters.modificationDate[1])) {
						options.where.push({
							'function': {
								'name': 'TO_DATE',
								'params': [{
									'field': 'modificationDate'
								}]
							},
							'operator': '$lte',
							'value': {
								'function': {
									'name': 'TO_DATE',
									'params': [object.filters.modificationDate[1]]
								}
							}
						});
					}
				}
			}

			if (!$.lodash.isNil(object.orderBy)) {
				options.orderBy = [{
					'field': object.orderBy.field,
					'type': object.orderBy.type.toLowerCase()
				}];
			}
		}
		if (paginate) {
			options.paginate = {
				'limit': 15,
				'offset': object && object.page ? (object.page - 1) * 15 : 0
			};
		}
		response.groups = _this.getDefaultFieldsData(groupModel.find(options).results);
		response.pageCount = Math.ceil(groupModel.find({
			count: true,
			select: [{
				field: 'id'
			}]
		}).results[0].tableCount / 15);
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E',
			'errorInfo': $.parseError(e)
		});

		throw errorGenerator.internalError(e);
	}
	return response;
};

this.listAdvancedFilters = function () {
	let errorGenerator = new $.ErrorGenerator('listAdvancedFilters');
	var response = {
		'status': ['active', 'inactive'],
		'createdBy': [],
		'modifiedBy': []
	};
	let creationUserJoin = [];
	let modificationUserJoin = [];
	let userOptions = {
		'aliases': [{
			'collection': userModel.getIdentity(),
			'name': 'user',
			'isPrimary': true
		}, {
			'collection': groupModel.getIdentity(),
			'name': 'group'
		}],
		'select': [{
			'field': 'id',
			'alias': 'user'
		}, {
			'field': 'name',
			'alias': 'user'
		}, {
			'field': 'lastName',
			'alias': 'user'
		}],
		'join': []
	};

	try {
		creationUserJoin = [{
			'alias': 'group',
			'type': 'inner',
			'on': [{
				'alias': 'group',
				'field': 'creationUser',
				'operator': '$eq',
				'value': {
					'alias': 'user',
					'field': 'id'
				}
			}]
		}];

		userOptions.join = creationUserJoin;
		response.createdBy = userModel.find(userOptions).results;

		modificationUserJoin = [{
			'alias': 'group',
			'type': 'inner',
			'on': [{
				'alias': 'group',
				'field': 'modificationUser',
				'operator': '$eq',
				'value': {
					'alias': 'user',
					'field': 'id'
				}
			}]
		}];

		userOptions.join = modificationUserJoin;
		response.modifiedBy = userModel.find(userOptions).results;
	} catch (e) {
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.changeGroupStatus = function (object) {
	let errorGenerator = new $.ErrorGenerator('changeGroupStatus');
	var response = {};
	try {
		if (!$.lodash.isNil(object)) {
			if ($.lodash.isArray(object.groups) && !$.lodash.isEmpty(object.groups) && !$.lodash.isNil(object.status)) {

				response = groupModel.update({
					isActive: object.status
				}, {
					where: [{
						field: 'id',
						operator: '$in',
						value: object.groups
					}]
				}, true).results;

				if (response.updated) {
					$.messageCodes.push({
						'code': 'CORE000062',
						'type': 'S'
					});
				}
			}
		}
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000063',
			'type': 'E'
		});
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.getGroupsByUser = function (userId) {
	let errorGenerator = new $.ErrorGenerator('getGroupsByUser');
	var response = [];
	try {
		let options = {
			aliases: [{
				collection: groupModel.getIdentity(),
				name: 'Group',
				isPrimary: true
			}, {
				collection: groupUserModel.getIdentity(),
				name: 'GroupUser'
			}, {
				collection: userModel.getIdentity(),
				name: 'User'
			}],
			select: [{
				field: 'id',
				alias: 'Group',
				as: 'groupId'
			}, {
				field: 'name',
				alias: 'Group'
			}, {
				field: 'id',
				alias: 'User',
				as: 'userId'
			}, {
				field: 'name',
				alias: 'User',
				as: 'userName'
			}, {
				field: 'lastName',
				alias: 'User',
				as: 'userLastName'
			}],
			join: [{
				alias: 'GroupUser',
				type: 'inner',
				on: [{
					alias: 'GroupUser',
					field: 'groupId',
					operator: '$eq',
					value: {
						alias: 'Group',
						field: 'id'
					}
				}]
			}, {
				alias: 'User',
				type: 'inner',
				map: 'users',
				on: [{
					alias: 'User',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'GroupUser',
						field: 'userId'
					}
				}]
			}]
		};

		if (!$.lodash.isNil(userId) && $.lodash.isNumber(userId)) {
			let userGroups = groupUserModel.find({
				select: [{
					field: 'groupId'
				}],
				where: [{
					field: 'userId',
					operator: '$eq',
					value: userId
				}]
			}).results.map(function (elem) {
				return elem.groupId;
			});

			options.where = [{
				field: 'id',
				alias: 'Group',
				operator: '$in',
				value: $.lodash.isEmpty(userGroups) ? [0] : userGroups
			}];
		}

		response = groupModel.find(options).results;
	} catch (e) {
		throw errorGenerator.internalError(e);
	}
	return response;
};

this.automaticCreateGroup = function () {
	let errorGenerator = new $.ErrorGenerator('automaticCreateGroup');
	var response = {};
	try {
		var hanaGroupRolesQuery = 'SELECT DISTINCT ROLE_NAME FROM "PUBLIC"."ROLES" WHERE ROLE_NAME LIKE \'timp.core.server.role::GROUP_%\'';
		var hanaGroupRoles = $.execute(hanaGroupRolesQuery).results.map(function (elem) {
			return elem.ROLE_NAME;
		});

		if ($.lodash.isArray(hanaGroupRoles) && !$.lodash.isEmpty(hanaGroupRoles)) {
			var coreGroups = groupModel.find({
				select: [{
					field: 'id'
				}, {
					field: 'grcRole'
				}, {
					field: 'isActive'
				}],
				where: [{
					field: 'grcRole',
					operator: '$neq',
					value: null
				}]
			}).results;
			$.lodash.forEach(hanaGroupRoles, function (roleName) {

				let object = {
					grcRole: roleName,
					description: '',
					leaderId: $.getUserId(),
					isActive: true,
					orgPrivileges: [],
					users: [],
					privileges: [],
					packages: []
				};

				let group = $.lodash.find(coreGroups, function (elem) {
					return elem.grcRole === roleName;
				});

				object.name = roleName.substr(29).split(/_/).join(' ');
				object.id = group ? group.id : undefined;

				let roleGranteesQuery = 'SELECT DISTINCT GRANTEE FROM "PUBLIC"."EFFECTIVE_ROLE_GRANTEES" WHERE ROLE_NAME = \'' + roleName + '\'';
				var roleGrantees = $.execute(roleGranteesQuery).results.map(function (elem) {
					return elem.GRANTEE;
				});

				let rolePrivilegesQuery = 'SELECT DISTINCT ROLE_NAME FROM "PUBLIC"."GRANTED_ROLES" WHERE GRANTEE = \'' + roleName + '\'';
				var rolePrivileges = $.execute(rolePrivilegesQuery).results.map(function (elem) {
					return elem.ROLE_NAME;
				});

				let roleOrgPrivilegesQuery = 'SELECT DISTINCT OBJECT_NAME FROM "PUBLIC"."GRANTED_PRIVILEGES" WHERE GRANTEE = \'' + roleName + '\' ' +
					'AND OBJECT_NAME LIKE \'%TIMP_EMPRESA%\'';

				object.orgPrivileges = $.execute(roleOrgPrivilegesQuery).results.map(function (elem) {
					var privName = elem.OBJECT_NAME.substr(elem.OBJECT_NAME.lastIndexOf('EMPRESA') + 8);
					return {
						companyId: privName.split('_')[0],
						branchId: privName.split('_')[4]
					};
				});

				object.users = userModel.find({
					select: [{
						field: 'id'
					}],
					where: [{
						field: 'hanaUser',
						operator: '$in',
						value: roleGrantees
					}]
				}).results.map(function (elem) {
					return elem.id;
				});

				object.privileges = privilegeModel.find({
					select: [{
						field: 'id'
					}],
					where: [{
						field: 'role',
						operator: '$in',
						value: rolePrivileges
					}]
				}).results.map(function (elem) {
					return elem.id;
				});
				response[roleName] = _this.saveGroup(object).results;
			});

		}
	} catch (e) {
		throw errorGenerator.internalError(e);
	}

	return response;
};

this.checkHanaGroupStatus = function () {
	let errorGenerator = new $.ErrorGenerator('checkHanaGroupStatus');
	var response = {};
	try {
		var coreGroups = groupModel.find({
			select: [{
				field: 'id'
			}, {
				field: 'grcRole'
			}],
			where: [{
				field: 'grcRole',
				operator: '$neq',
				value: null
			}, {
				field: 'isActive',
				operator: '$eq',
				value: 1
			}]
		}).results;

		var elementsToDeactivate = [];

		$.lodash.forEach(coreGroups, function (elem) {
			var hanaGroupRoleQuery = 'SELECT DISTINCT ROLE_NAME FROM "PUBLIC"."ROLES" WHERE ROLE_NAME = \'' + elem.grcRole + '\'';
			var hanaGroupRole = $.execute(hanaGroupRoleQuery);

			if ($.lodash.isEmpty(hanaGroupRole.errors) && $.lodash.isEmpty(hanaGroupRole.results)) {
				elementsToDeactivate.push(elem.id);
			}
		});

		if (!$.lodash.isEmpty(elementsToDeactivate)) {
			var object = {
				status: false,
				groups: elementsToDeactivate
			};
			response = _this.changeGroupStatus(object);
		}

	} catch (e) {
		throw errorGenerator.internalError(e);
	}

	return response;
};

this.getDefaultFieldsData = function (elements) {
	let errorGenerator = new $.ErrorGenerator('getDefaultFieldsData');
	try {
		let options = {
			select: [{
				'field': 'name'
			}, {
				'field': 'lastName'
			}]
		};

		if (Array.isArray(elements) && elements.length > 0) {
			let users = [];
			var usersData = {};

			elements.forEach(function (elem) {
				if (elem.creationUser) {
					users.push(elem.creationUser);
				}
				if (elem.modificationUser) {
					users.push(elem.modificationUser);
				}
			});

			if (users.length > 0) {
				if ($.lodash.isIntArray(users)) {
					options.select.push({
						'field': 'id'
					});
					options.where = [{
						'field': 'id',
						'operator': '$in',
						'value': $.lodash.uniq(users)
					}];

					userModel.find(options).results.map(function (elem) {
						usersData[elem.id] = elem;
					});

				} else if ($.lodash.isStringArray(users)) {
					options.select.push({
						'field': 'hanaUser'
					});
					options.where = [{
						'field': 'hanaUser',
						'operator': '$in',
						'value': $.lodash.uniq(users)
					}];

					userModel.find(options).results.map(function (elem) {
						usersData[elem.hanaUser] = elem;
					});
				}
			}

			$.lodash.forEach(elements, function (elem) {
				elem.creationUser = usersData[elem.creationUser] ? usersData[elem.creationUser] : {};
				elem.modificationUser = usersData[elem.modificationUser] ? usersData[elem.modificationUser] : {};
			});
		}
	} catch (e) {
		throw errorGenerator.internalError(e);
	}
	return elements;
};