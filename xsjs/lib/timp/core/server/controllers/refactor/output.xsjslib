//Util Libraries
$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

//Controllers
const ormServices = util.services;
$.import('timp.core.server.orm', 'resources');
const ormResources = $.timp.core.server.orm.resources;

//Models
$.import('timp.core.server.models.tables', 'output');
const outputModel = $.timp.core.server.models.tables.output.outputModel;

$.import('timp.core.server.models.tables', 'configuration');
const configurationModel = $.timp.core.server.models.tables.configurationModel;


function parseWhere(params) {
    return $.lodash.map($.lodash.keys(params), function(key) {
        let operator = '$eq';
        let value = params[key];
        
        if($.lodash.isPlainObject(params[key]) && !$.lodash.isNil(params[key])) {
            if (!$.lodash.isNil(params[key].operator)) {
                operator =  params[key].operator;
                value = params[key].value;
            }
        }

        return {
            field: key,
            operator: operator,
            value: value
        };
    });
}

/**
 * Get outputs by id or object type.
 * 
 * @param {Object} [params] - Object to filter the outputs if it's undefined or an empty object the function return all the outputs
 * @param {integer | array} [params.id] - Id or array of id's to filter the outputs
 * @param {boolean} [params.bfbForm] - Filter to get all BFB Forms Outputs
 * @param {boolean} [params.brbReport] - Filter to get all BRB Reports Outputs
 * @param {boolean} [params.bcbConfiguration] - Filter to get all BCB Configuration Outputs
 */
const _paramsSchema = {
    properties: {
        id: {
            type: ['integer', 'array']
        },
        bfbForm: {
            type: 'boolean'
        },
		brbReport: {
			type: 'boolean'
		},
		bcbConfiguration: {
			type: 'boolean'
		}
	}
};
this.getOutputs = function(params) {
    let errorGenerator = new $.ErrorGenerator('getOutputs');
    let results = {
        errors: []
    };
    try {
        let where = [];
        let parametersValidator = $.validator.validate(params, _paramsSchema);
        if(!$.lodash.isNil(params)) {
            if (parametersValidator.valid) {
                if(!$.lodash.isNil(params.id)) {
                    where.push({
                        field: 'id',
                        operator: $.lodash.isArray(params.id) ? '$in' : '$eq',
                        value: params.id
                    });
                } else {
                    let or = [];
            	    if ($.lodash.isBoolean(params.bfbForm) && params.bfbForm) {
                        or.push({
                            field: 'bfbFormId',
                            operator: '$neq',
                            value: null
                        });
                    }
                    
                    if ($.lodash.isBoolean(params.brbReport) && params.brbReport) {
                        or.push({
                            field: 'brbReportId',
                            operator: '$neq',
                            value: null
                        });
                    }
                    
                    if ($.lodash.isBoolean(params.bcbConfiguration) && params.bcbConfiguration) {
                        or.push({
                            field: 'bcbConfigurationId',
                            operator: '$neq',
                            value: null
                        });
                    }
                    
                    where.push(or);
                }
            } else {
                throw errorGenerator.paramsError(parametersValidator.errors);
            }
        }
    	
    	results = outputModel.find({
            where: where
        });
        
        return results;
    } catch(e) {
        throw errorGenerator.internalError(e);
    }
};


this.createOutput = function(outputParams) {
    let errorGenerator = new $.ErrorGenerator('createOutput');
	var result = {
		created: false
	};
	var isValidParameter = $.validator.validate(outputParams, outputModel.schema, {
		additionalProperties: false
	});
	if (!$.lodash.isNil(outputParams) && isValidParameter.valid) {
		try {
			result = outputModel.create(outputParams);

			return result;
		} catch (e) {
		    throw errorGenerator.saveError(e);
		}
	} else {
	    throw errorGenerator.paramsError(isValidParameter.errors);
	}
};