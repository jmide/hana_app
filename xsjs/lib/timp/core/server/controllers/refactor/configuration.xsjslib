$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server.models.tables', 'configuration');
const configurationModel = $.timp.core.server.models.tables.configuration.configurationModel;

/**
 * Installation Settings
 **/
const _saveSystemConfigurationSchema = {
	properties: {
		componentName: {
			type: 'string',
			required: true,
			allowEmpty: false
		},
		keys: {
			type: 'object',
			required: true,
			allowEmpty: false
		},
		delete: {
			type: 'boolean',
			allowEmpty: false
		}
	}
};


this.getSessionConfiguration = function() {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('getSessionConfiguration');
	try {
		let result = configurationModel.find({
			select: [{
				field: 'key'
			}, {
				field: 'value'
			}],
			where: [{
				field: 'key',
				operator: '$in',
				value: ['TIMP::LogonType', 'TIMP::SessionTime', 'TIMP::FileLockTime', 'TIMP::VerifyFileLockTime']
			}]
		}).results;
		for (let index = 0; index < result.length; index++) {
			const element = result[index];
			if (element.key === 'TIMP::LogonType') {
				response.logonType = element.value || null;
			} else if (element.key === 'TIMP::SessionTime') {
				response.sessionTime = element.value || 900; // Default: 900 seconds
			} else if (element.key === 'TIMP::FileLockTime') {
				let fileLockTime = element.value && Number(element.value);
				response.fileLockTime = !isNaN(fileLockTime) && fileLockTime > 0 ? fileLockTime :  300; // Default: 300 seconds
			} else if (element.key === 'TIMP::VerifyFileLockTime') {
				let verifyFileLockTime = element.value && Number(element.value);
				response.verifyFileLockTime = !isNaN(verifyFileLockTime) && verifyFileLockTime > 0 ? verifyFileLockTime : 30; // Default: 30 seconds
			}
		}
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E'
		});
	}
	return response;
};

var setSessionTimeout = function () {
	let sessionConfiguration = this.getSessionConfiguration();
	let query  = this.getSessionTimeoutQuery(sessionConfiguration.sessionTime || 900); // Default: 900
	let result = this.executeQuery(query);
	return result;
};
this.setSessionTimeout = setSessionTimeout;

this.getSessionTimeoutQuery = function (sessionTime) {
	// https://answers.sap.com/questions/9888288/xs-engine-session-timeout.html
	// https://answers.sap.com/questions/11361972/session-timeout-in-hana-xs-engine-and-sap-webdisap.html
	// https://help.sap.com/viewer/6b94445c94ae495c83a19646e7c3fd56/2.0.02/en-US/86c9d3762a6b47039346f8ef075f8a7a.html#loio86c9d3762a6b47039346f8ef075f8a7a__section_jjc_blr_v4
	// sessiontimeout: Amount of time (in seconds) before an inactive session is closed. Unsigned integer for example, 60. Default: 900
	const sessionTimeNumber = Number(sessionTime);
	const isValidNumber = !isNaN(sessionTimeNumber) && sessionTimeNumber > 0;
	if (isValidNumber) {
		return `
		ALTER SYSTEM ALTER CONFIGURATION ('xsengine.ini', 'System')
		SET ('httpserver', 'sessiontimeout') = '${sessionTimeNumber}' WITH RECONFIGURE`;
	}
	return '';
};

this.executeQuery = function (query) {
	$.import('timp.core.server.orm', 'sql');
	const slqCtrl = $.timp.core.server.orm.sql;
	let result = false;
	try {
		if (query && (typeof query === 'string')) {
			const resultExecution = slqCtrl.EXECUTE({ query: query });
			result = resultExecution;
		}
		return result;
	} catch (err) {
		$.messageCodes.push({
			text: 'Error',
			type: 'E',
			errorInfo: util.parseError(err)
		});
		return false;
	}
};

this.userCanChooseHighPerformanceBRB = function() {
	try {
		let result = configurationModel.find({
			select: [{
				field: 'value'
			}],
			where: [{
				field: 'key',
				operator: '$eq',
				value: 'TIMP::HighPerformanceBRBEngine'
			}, {
				field: 'value',
				operator: '$eq',
				value: 'choose'
			}]
		}).results;
		return result && result.length ? true : false;
	} catch (e) {
		return null;
	}
};

this.getSystemConfiguration = function(object) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('getSystemConfiguration');
	try {
		if (!$.lodash.isNil(object) && $.lodash.isArray(object.keys) && !$.lodash.isEmpty(object.keys)) {
			if (util.getComponentInformation(object.componentName)) {
				let component = util.getComponentInformation(object.componentName).results[0].id;
				response = configurationModel.find({
					select: [{
						field: 'key'
					}, {
						field: 'value'
					}],
					where: [{
						field: 'componentId',
						operator: '$eq',
						value: component
					}, {
						field: 'key',
						operator: '$in',
						value: object.keys
					}]
				}).results;
			} else {
				response = [];
			}
		} else {
			$.messageCodes.push({
				'code': 'CORE000024',
				'type': 'E'
			});
			throw errorGenerator.paramsError();
		}
	} catch (e) {
		$.messageCodes.push({
			'code': 'CORE000021',
			'type': 'E'
		});
	}
	return response;
};

this.saveSystemConfiguration = function(parameter) {
	var response = {
		errors: [],
		results: []
	};
	let errorGenerator = new $.ErrorGenerator('saveSystemConfiguration');
	if ($.lodash.isArray(parameter)) {
		$.lodash.forEach(parameter, function(componentKeys) {
			var elementsToUpsert = [];
			var elementsToDelete = [];
			let isValid = $.validator.validate(componentKeys, _saveSystemConfigurationSchema);
			if (isValid.valid) {
				let componentInformation = util.getComponentInformation(componentKeys.componentName).results[0].id;
				if (!$.lodash.isNil(componentInformation)) {
					if ($.lodash.has(componentKeys, 'delete') && $.lodash.isBoolean(componentKeys.delete)) {
						$.lodash.forEach(componentKeys.keys, function(value, key) {
							elementsToDelete.push(key);
						});
						let deletedInstances = configurationModel.delete({
							where: [{
								field: 'componentId',
								operator: '$eq',
								value: componentInformation
							}, {
								field: 'key',
								operator: '$in',
								value: elementsToDelete
							}]
						}, true);
						if ($.lodash.isEmpty(deletedInstances.errors)) {
							response.results = $.lodash.concat(response.results, deletedInstances.results.instaces);
							elementsToDelete = [];
						} else {
							response.errors = $.lodash.concat(response.errors, deletedInstances.errors);
							throw errorGenerator.internalError(deletedInstances.errors);
						}
					} else {
						Object.keys(componentKeys.keys).forEach(function(elem) {
							elementsToUpsert.push({
								key: elem,
								value: $.lodash.isNil(componentKeys.keys[elem].value) ? '' : componentKeys.keys[elem].value.toString(),
								componentId: componentInformation
							});
						});
						let createdInstances = configurationModel.batchUpsert(elementsToUpsert, {
							getInstances: true
						});
						if ($.lodash.isEmpty(createdInstances.errors)) {
							response.results = $.lodash.concat(response.results, createdInstances.results.instances);
						} else {
							response.errors = $.lodash.concat(response.errors, createdInstances.errors);
							$.messageCodes.push({
								'code': 'CORE000055',
								'type': 'E'
							});
							throw errorGenerator.internalError(createdInstances.errors);
						}
					}
				} else {
					response.errors.push({
						error: 'Component Does not Exist',
						name: parameter.componentName
					});
					$.messageCodes.push({
						'code': 'CORE000055',
						'type': 'E'
					});
					throw errorGenerator.internalError();
				}
			} else {
				response.errors = $.lodash.concat(response.errors, isValid.errors);
				$.messageCodes.push({
					'code': 'CORE000024',
					'type': 'E'
				});
				throw errorGenerator.paramsError(isValid.errors);
			}
		});
	}
	let isUpdatedSessionTimeout = setSessionTimeout();
	if (!isUpdatedSessionTimeout) {
		throw 'The system\'s session timeout could not be setted';
	}
	return response;
};

this.createSystemConfigurationKeys = function(seeder) {
	var response = {
		errors: [],
		results: []
	};
	const hasBaseSeeder = Boolean(seeder && seeder.base && seeder.base.length);
	if (hasBaseSeeder) {
		let baseSeeder = seeder.base || [];
		const whereValues = baseSeeder.map(elem => elem.key);
		let intersection = [];
		let created = [];
		let found = configurationModel.find({
			select: [{
				field: 'key'
			}, {
				field: 'value'
			}],
			where: [{
				field: 'key',
				operator: '$in',
				value: whereValues
			}]
		}).results;
		for (let i = 0; i < found.length; i++) {
			const tableElem = found[i];
			for (let j = 0; j < baseSeeder.length; j++) {
				const seederElem = baseSeeder[j];
				if (seederElem.key === tableElem.key) {
					intersection.push(tableElem);
				}
			}
		}
		baseSeeder = baseSeeder.filter(elem => {
			if (intersection.length) {
				let isDuplicated = false;
				for (let i = 0; i < intersection.length; i++) {
					const elemToRemove = intersection[i];
					if (elem.key === elemToRemove.key) {
						isDuplicated = true;
					}
				}
				return !isDuplicated; // not return the duplicated element
			} else {
				return true;
			}
		});
		if (baseSeeder && baseSeeder.length) {
			const options = {
				getInstances: true,
				select: [ { field: 'key' }, { field: 'value' } ]
			};
			created = configurationModel.batchCreate(baseSeeder, options);
			response.results = created.results.instances;
			response.errors = created.errors;
		} else {
			response.results = [];
			response.errors = [];
		}
	}
	return response;
};