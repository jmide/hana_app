$.import('timp.core.server.models.tables', 'proxyService');
const adapterModel = $.timp.core.server.models.tables.proxyService.adapterModel;
const serviceModel = $.timp.core.server.models.tables.proxyService.serviceModel;
const serviceDestinationModel = $.timp.core.server.models.tables.proxyService.serviceDestinationModel;

$.import('timp.core.server.libraries.internal', 'proxyServer');
const proxyServer = $.timp.core.server.libraries.internal.proxyServer;

$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;

const _saveAdapterSchema = {
	properties: {
		id: {
			type: 'integer',
			allowEmpty: false
		},
		type: {
			type: 'string',
			required: true,
			allowEmpty: false
		}, 
		name: {
			type: 'string',
			required: true,
			allowEmpty: false
		},
		hostName: {
			type: 'string',
			required: true,
			allowEmpty: false
		},
		port: {
			type: 'integer',
			required: true,
			allowEmpty: false
		},
		userName: {
			type: 'string',
			allowEmpty: false
		},
		password: {
			type: 'string',
			allowEmpty: false
		},
		client: {
			type: 'integer',
			required: true,
			allowEmpty: false
		},
		isActive: {
			type: 'boolean',
			allowEmpty: true
		}
	}
};

/**
 * @method saveProxyAdapter - Saves a TDF or ECC Adapter
 * @param {Object} object - Adapter metadata
 * @return {Object} has a boolean property indicating if the adapter was created/updated, if so returns the instance in a property "instance(s)".
 * */
this.saveProxyAdapter = function(object) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('saveProxyAdapter');
	try {
		if (!$.lodash.isNil(object)) {
			let systemData = $.execute('SELECT SYSTEM_ID, HOST FROM "M_DATABASE"').results[0];
			let passphrase = systemData ? systemData.HOST + "-" + systemData.SYSTEM_ID : "Deleted59Hashed53Cords";
			var encryptedPassword;

			let isValid = $.validator.validate(object, _saveAdapterSchema);
			if (isValid.valid) {
				if ($.lodash.isNil(object.id)) { //create adapter 
					encryptedPassword = $.encrypt(object.password, passphrase);
					object.password = encryptedPassword;
					//Setting first Adapter as default
					object.isDefault = $.lodash.isEqual(adapterModel.find({
						count: true,
						select: [{
							field: "id"
						}]
					}).results[0].tableCount, 0) ? true : false;
					response = adapterModel.create(object).results;
					if (response.created) {
						$.messageCodes.push({
							"code": "CORE000050",
							"type": 'S'
						});
					}
				} else { //update adapter 
					if (!$.lodash.isNil(object.password)) {
						encryptedPassword = $.encrypt(object.password, passphrase);
						object.password = encryptedPassword;
					}
					response = adapterModel.update(object, {
						where: [{
							field: "id",
							operator: "$eq",
							value: object.id
						}]
					}, true).results;
					if (response.updated) {
						$.messageCodes.push({
							"code": "CORE000050",
							"type": 'S'
						});
					}
				}
			} else {
				throw errorGenerator.paramsError(isValid.errors);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			"code": "CORE000051",
			"type": 'E'
		});
		throw errorGenerator.generateError('00', '82', null, e);
	}
	return response;
};

const _updateServiceSchema = {
	properties: {
		id: {
			type: 'integer',
			required: true,
			allowEmpty: false
		},
		destinations: {
			type: 'array',
			required: true,
			allowEmpty: false
		}
	}
};

this.updateProxyService = function(service) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('updateProxyService');
	try {
		if (!$.lodash.isNil(service)) {
			let isValid = $.validator.validate(service, _updateServiceSchema);
			if (isValid.valid) {
				serviceDestinationModel.delete({
					where: [{
						field: "serviceId",
						operator: "$eq",
						value: service.id
					}]
				});

				$.lodash.forEach(service.destinations, function(url) {
					response.updated = serviceDestinationModel.create({
						serviceId: service.id,
						destination: url.url,
						destinationPath: url.destinationPath,
						isDefault: url.isDefault ? url.isDefault : false,
						isHttpsEnabled: url.isHttpsEnabled,
						adapterId: url.adapterId
					}).results.created;
				});
				if (response.updated) {
					$.messageCodes.push({
						"code": "CORE000052",
						"type": 'S'
					});
				}
			} else {
				throw errorGenerator.paramsError(isValid.errors);
			}
		} else {
			throw errorGenerator.paramsError();
		}
	} catch (e) {
		$.messageCodes.push({
			"code": "CORE000053",
			"type": 'E'
		});
		throw errorGenerator.generateError('00', '77', null, e);
	}
	return response;
};

/**
 * Returns a list of Services
 * @param filters (optional): {
 *      type: ["TDF", "ECC"],
 *      name: "TMFFIPostingPostSyncIn"
 *  }
 **/
this.getProxyServices = function(filters) {
	var response = {};
	var whereOptions = [];
	let errorGenerator = new $.ErrorGenerator('getProxyServices');
	try {
		let selectOptions = {
			aliases: [{
				collection: serviceModel.getIdentity(),
				name: 'Service'
            }, {
				collection: serviceDestinationModel.getIdentity(),
				name: 'Destination'
            }, {
				collection: userModel.getIdentity(),
				name: 'UserModel'
            }],
			select: [{
				field: 'id',
				alias: 'Service'
            }, {
				field: 'modificationDate',
				alias: 'Service'
            }, {
				field: 'type',
				alias: 'Service'
            }, {
				field: 'name',
				alias: 'Service',
				as: "name"
            }, {
				field: 'name',
				alias: 'UserModel'
            }, {
				field: 'lastName',
				alias: 'UserModel'
            }, {
				field: 'id',
				alias: 'UserModel'
            }, {
				field: 'id',
				alias: 'Destination'
            }, {
				field: 'destination',
				alias: 'Destination'
            }, {
				field: 'destinationPath',
				alias: 'Destination'
            }, {
				field: 'isHttpsEnabled',
				alias: 'Destination'
            }, {
				field: 'isDefault',
				alias: 'Destination'
            }, {
				field: 'adapterId',
				alias: 'Destination'
            }],
			join: [{
				alias: 'Destination',
				type: 'left',
				map: 'serviceDestinations',
				on: [{
					alias: 'Destination',
					field: 'serviceId',
					operator: '$eq',
					value: {
						alias: 'Service',
						field: 'id'
					}
                }]
            },
            {
				alias: 'UserModel',
				type: 'left',
				map: 'modificationUser',
				on: [{
					alias: 'UserModel',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'Service',
						field: 'modificationUser'
					}
                }]
            }],
			where: []
		};

		if ($.request.cookies.get("Content-Language") === "enus") {
			selectOptions.select.push({
				field: "descriptionEnus",
				alias: "Service",
				as: "description"
			});
		} else {
			selectOptions.select.push({
				field: "descriptionPtbr",
				alias: "Service",
				as: "description"
			});
		}

		if (!$.lodash.isNil(filters)) {
			if (!$.lodash.isNil(filters.id) && $.lodash.isNumber(filters.id)) {
				whereOptions.push({
					field: "id",
					operator: "$eq",
					value: filters.id
				});
			}

			if (!$.lodash.isNil(filters.name) && $.lodash.isString(filters.name)) {
				whereOptions.push({
					"function": {
						name: "UPPER",
						params: [{
							field: "name"
				        }]
					},
					operator: "$like",
					value: "%" + filters.name.toUpperCase() + "%"
				});
			}

			if ($.lodash.isArray(filters.type) && !$.lodash.isEmpty(filters.type)) {
				whereOptions.push({
					field: "type",
					operator: "$in",
					value: filters.type
				});
			}

			if ($.lodash.isArray(filters.modifiedBy) && !$.lodash.isEmpty(filters.modifiedBy)) {
				whereOptions.push({
					"field": "modificationUser",
					"operator": "$in",
					"value": filters.modifiedBy
				});
			}

			if ($.lodash.isArray(filters.modificationDate) && !$.lodash.isEmpty(filters.modificationDate)) {
				whereOptions.push({
					"function": {
						"name": "TO_DATE",
						"params": [{
							"field": "modificationDate"
                            }]
					},
					"operator": "$gte",
					"value": {
						"function": {
							"name": "TO_DATE",
							"params": [filters.modificationDate[0]]
						}
					}
				});

				if (!$.lodash.isEmpty(filters.modificationDate[1])) {
					whereOptions.push({
						"function": {
							"name": "TO_DATE",
							"params": [{
								"field": "modificationDate"
                                }]
						},
						"operator": "$lte",
						"value": {
							"function": {
								"name": "TO_DATE",
								"params": [filters.modificationDate[1]]
							}
						}
					});
				}
			}
			selectOptions.where = whereOptions;
		}

		response = serviceModel.find(selectOptions).results;
		
        //this because before there was a loop with requests to database,
        //to don't modify the answer used this to remove the array
		response.forEach(function(elem) {
        	elem.modificationUser = elem.modificationUser[0];
        });
    	
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}
	return response;
};

/**
 * Return a list of Adapters
 * @param filters (optional): {
 *      type: ["TDF", "ECC"],
 *      status: ["active", "inactive"],
 *      client: ["300"],
 *      createdBy: ["SGARC", "JDOE"],
 *      modifiedBy: ["JDOE"],
 *      creationDate: ["2017-10-01","2017-11-01" (optional)] -- Range of Dates
 *      modificationDate: ["2017-10-15","2017-10-30" (optional)] -- Range of Dates
 *  }
 **/
this.getProxyAdapter = function(object) {
	let errorGenerator = new $.ErrorGenerator('getProxyAdapter');
	var response = {};
	var adapterOption = {
		"xselect": [{
			"field": "password"
		}],
		where: []
	};
	try {
		if (!$.lodash.isNil(object)) {
			let filters = object.filter;
			if (!$.lodash.isNil(filters.id) && $.lodash.isNumber(filters.id)) {
				// delete userOption.xselect;
				adapterOption.where.push({
					field: "id",
					operator: "$eq",
					value: filters.id
				});
			}
			if (!$.lodash.isNil(filters.name) && $.lodash.isString(filters.name)) {
				adapterOption.where.push({
					"field": "name",
					"operator": "$contains",
					"value": {
						"value": filters.name,
						"fuzzy": {
							"minimumScore": 0.1
						}
					}
				});
			}

			if ($.lodash.isArray(filters.type) && !$.lodash.isEmpty(filters.type)) {
				adapterOption.where.push({
					field: "type",
					operator: "$in",
					value: filters.type
				});
			}

			if ($.lodash.isArray(filters.status) && !$.lodash.isEmpty(filters.status)) {
				let status = {
					"active": 1,
					"inactive": 0
				};

				filters.status = filters.status.map(function(s) {
					return status[s];
				});

				adapterOption.where.push({
					field: "isActive",
					operator: "$in",
					value: filters.status
				});
			}

			if ($.lodash.isArray(filters.clients) && !$.lodash.isEmpty(filters.clients)) {
				adapterOption.where.push({
					field: "client",
					operator: "$in",
					value: filters.clients
				});
			}

			if ($.lodash.isArray(filters.createdBy) && !$.lodash.isEmpty(filters.createdBy)) {
				adapterOption.where.push({
					"field": "creationUser",
					"operator": "$in",
					"value": filters.createdBy
				});
			}

			if ($.lodash.isArray(filters.modifiedBy) && !$.lodash.isEmpty(filters.modifiedBy)) {
				adapterOption.where.push({
					"field": "modificationUser",
					"operator": "$in",
					"value": filters.modifiedBy
				});
			}

			if ($.lodash.isArray(filters.creationDate) && !$.lodash.isEmpty(filters.creationDate)) {
				adapterOption.where.push({
					"function": {
						"name": "TO_DATE",
						"params": [{
							"field": "creationDate"
                        }]
					},
					"operator": "$gte",
					"value": {
						"function": {
							"name": "TO_DATE",
							"params": [filters.creationDate[0]]
						}
					}
				});

				if (!$.lodash.isEmpty(filters.creationDate[1])) {
					adapterOption.where.push({
						"function": {
							"name": "TO_DATE",
							"params": [{
								"field": "creationDate"
                            }]
						},
						"operator": "$lte",
						"value": {
							"function": {
								"name": "TO_DATE",
								"params": [filters.creationDate[1]]
							}
						}
					});
				}
			}

			if ($.lodash.isArray(filters.modificationDate) && !$.lodash.isEmpty(filters.modificationDate)) {
				adapterOption.where.push({
					"function": {
						"name": "TO_DATE",
						"params": [{
							"field": "modificationDate"
                            }]
					},
					"operator": "$gte",
					"value": {
						"function": {
							"name": "TO_DATE",
							"params": [filters.modificationDate[0]]
						}
					}
				});

				if (!$.lodash.isEmpty(filters.modificationDate[1])) {
					adapterOption.where.push({
						"function": {
							"name": "TO_DATE",
							"params": [{
								"field": "modificationDate"
                                }]
						},
						"operator": "$lte",
						"value": {
							"function": {
								"name": "TO_DATE",
								"params": [filters.modificationDate[1]]
							}
						}
					});
				}
			}
		}

		adapterOption.paginate = {
			"limit": 15,
			"offset": object && object.page ? (object.page - 1) * 15 : 0
		};

		response.adapters = adapterModel.find(adapterOption).results;

		response.adapters.forEach(function(elem) {
			elem.creationUser = userModel.find({
				"select": [{
					"field": "id",
					"alias": "user"
		}, {
					"field": "name",
					"alias": "user"
		}, {
					"field": "lastName",
					"alias": "user"
		}],
				"where": [{
					"field": "id",
					"operator": "$eq",
					"value": elem.creationUser
			}]
			}).results[0];

			elem.modificationUser = userModel.find({
				"select": [{
					"field": "id",
					"alias": "user"
		}, {
					"field": "name",
					"alias": "user"
		}, {
					"field": "lastName",
					"alias": "user"
		}],
				"where": [{
					"field": "id",
					"operator": "$eq",
					"value": elem.modificationUser
			}]
			}).results[0];

		});

		response.pageCount = Math.ceil(adapterModel.find({
			count: true,
			select: [{
				field: "id"
			}]
		}).results[0].tableCount / 15);
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}
	return response;
};

/**
 * @param (string): "adapters" | "services"
 **/
this.listAdvancedFilters = function(tab) {
	let errorGenerator = new $.ErrorGenerator('listAdvancedFilters');
	var response = {};
	let creationUserJoin = [];
	let modificationUserJoin = [];
	let userOptions = {
		"aliases": [{
			"collection": userModel.getIdentity(),
			"name": "user",
			"isPrimary": true
        }],
		"select": [{
			"field": "id",
			"alias": "user"
		}, {
			"field": "name",
			"alias": "user"
		}, {
			"field": "lastName",
			"alias": "user"
		}],
		"join": []
	};
	try {
		if (!$.lodash.isNil(tab) && $.lodash.isEqual(tab, "adapters")) {
			//Adapter tab advanced Filters
			response = {
				type: [],
				createdBy: [],
				modifiedBy: [],
				status: ["active", "inactive"],
				clients: []
			};

			// processing 
			userOptions.aliases.push({
				"collection": adapterModel.getIdentity(),
				"name": "adapter"
			});

			response.type = adapterModel.find({
				distinct: true,
				select: [{
					field: "type"
			}]
			}).results.map(function(elem) {
				return elem.type;
			});

			response.clients = adapterModel.find({
				distinct: true,
				select: [{
					field: "client"
			}]
			}).results.map(function(elem) {
				return elem.client.toString();
			});

			creationUserJoin = [{
				"alias": "adapter",
				"type": "inner",
				"on": [{
					"alias": "adapter",
					"field": "creationUser",
					"operator": "$eq",
					"value": {
						"alias": "user",
						"field": "id"
					}
			    }]
			}];

			userOptions.join = creationUserJoin;
			response.createdBy = userModel.find(userOptions).results;

			modificationUserJoin = [{
				"alias": "adapter",
				"type": "inner",
				"on": [{
					"alias": "adapter",
					"field": "modificationUser",
					"operator": "$eq",
					"value": {
						"alias": "user",
						"field": "id"
					}
			    }]
			}];

			userOptions.join = modificationUserJoin;
			response.modifiedBy = userModel.find(userOptions).results;

		} else {
			response = {
				type: [],
				modifiedBy: []
			};

			userOptions.aliases.push({
				"collection": serviceModel.getIdentity(),
				"name": "service"
			});

			response.type = serviceModel.find({
				distinct: true,
				select: [{
					field: "type"
		}]
			}).results.map(function(elem) {
				return elem.type;
			});

			modificationUserJoin = [{
				"alias": "service",
				"type": "inner",
				"on": [{
					"alias": "service",
					"field": "modificationUser",
					"operator": "$eq",
					"value": {
						"alias": "user",
						"field": "id"
					}
			    }]
			}];

			userOptions.join = modificationUserJoin;
			response.modifiedBy = userModel.find(userOptions).results;
		}
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}
	return response;
};

this.getAdapterById = function(adapterId) {
	let errorGenerator = new $.ErrorGenerator('getAdapterById');
	var response = {};
	try {
		if ($.lodash.isNumber(adapterId)) {
			response = adapterModel.find({
				where: [{
					field: "id",
					operator: "$eq",
					value: adapterId
                }]
			}).results[0];
		}
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}

	return response;
};

this.getServiceDestination = function(id) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('getServiceDestination');
	try {
		if ($.lodash.isNumber(id)) {
			response = serviceDestinationModel.find({
				where: [{
					field: "id",
					operator: "$eq",
					value: id
                }]
			}).results[0];
		}
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}
	return response;
};

this.changeAdapterStatus = function(object) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('changeAdapterStatus');
	try {
		if (!$.lodash.isNil(object)) {
			if ($.lodash.isArray(object.adapters) && !$.lodash.isEmpty(object.adapters) && !$.lodash.isNil(object.status)) {
				response = adapterModel.update({
					isActive: object.status
				}, {
					where: [{
						field: "id",
						operator: "$in",
						value: object.adapters
                    }]
				}, true).results;
			}
			if (response.updated) {
				$.messageCodes.push({
					"code": "CORE000062",
					"type": 'S'
				});
			}
		}
	} catch (e) {
		throw errorGenerator.generateError('00', '80', null, e);
	}
	return response;
};

this.getServiceInformation = function(serviceName) {
    var where = [{
			field: 'name',
			alias: 'Service',
			operator: '$eq',
			value: serviceName
        }];
    if(serviceName !== 'timp_modifica_dados_contabeis'){
        where.push({
			field: 'isActive',
			alias: 'Adapter',
			operator: '$eq',
			value: 1
        });
        where.push({
			field: 'isDefault',
			alias: 'Adapter',
			operator: '$eq',
			value: 1
        });
        where.push({
			field: 'isDefault',
			alias: 'Destination',
			operator: '$eq',
			value: 1
        });
    }
	var services = serviceModel.find({
		aliases: [{
			collection: serviceModel.getIdentity(),
			name: 'Service',
			isPrimary: true
        }, {
			collection: serviceDestinationModel.getIdentity(),
			name: 'Destination'
        }, {
			collection: adapterModel.getIdentity(),
			name: 'Adapter'
        }],
		select: [{
			field: 'id',
			alias: 'Service'
        }, {
			field: 'name',
			alias: 'Service'
        }, {
			field: 'id',
			alias: 'Destination',
			as: 'serviceDestinationId'
        }, {
			field: 'destinationPath',
			alias: 'Destination'
        }, {
			field: 'destination',
			alias: 'Destination'
        }, {
			field: 'isDefault',
			alias: 'Destination'
        }, {
			field: 'isHttpsEnabled',
			alias: 'Destination'
        }, {
			field: 'adapterId',
			alias: 'Destination'
        }, {
			field: 'id',
			as: 'adapterId',
			alias: 'Adapter'
        }, {
			field: 'isDefault',
			alias: 'Adapter'
        }, {
			field: 'hostName',
			alias: 'Adapter'
        }, {
			field: 'port',
			alias: 'Adapter'
        }, {
			field: 'client',
			alias: 'Adapter'
        }],
		join: [{
			alias: 'Destination',
			type: 'inner',
			map: 'destinations',
			on: [{
				alias: 'Destination',
				field: 'serviceId',
				operator: '$eq',
				value: {
					alias: 'Service',
					field: 'id'
				}
            }]
        }, {
			alias: 'Adapter',
			type: 'inner',
			map: 'adapters',
			on: [{
				alias: 'Adapter',
				field: 'id',
				operator: '$eq',
				value: {
					alias: 'Destination',
					field: 'adapterId'
				}
            }]
        }],
		where: where
	}).results;
	return services;
};

this.getTDFServiceLocation = function(adapterId, serviceDestinationId) {
	var adapter = this.getAdapterById(adapterId);
	var serviceDestination = this.getServiceDestination(serviceDestinationId);
	if(serviceDestination.destinationPath){
	    serviceDestination.status = 200;
	    return serviceDestination;
	}
	if (!$.lodash.isNil(serviceDestination) && !$.lodash.isNil(adapter)) {
		let destination = adapter.hostName + ":" + adapter.port + serviceDestination.destination;
		if (destination.indexOf('?sap-client') === -1) {
			destination += "?sap-client=" + adapter.client;
		}
		destination += '&sap-language=P';
		let systemData = $.execute('SELECT SYSTEM_ID, HOST FROM "M_DATABASE"').results[0];
		let passphrase = systemData ? systemData.HOST + "-" + systemData.SYSTEM_ID : "Deleted59Hashed53Cords";
		let password = $.decrypt(adapter.password, passphrase);
		let serviceOptions = {
			contentType: 'text/xml',
			headers: {
				'Content-Type': 'text/xml; charset=utf-8',
				Authorization: 'Basic ' + $.util.codec.encodeBase64(adapter.userName + ':' + password)
			}
		};
		let serviceResponse = proxyServer.externalClientRequest('POST', destination, serviceOptions);
		if (serviceResponse.body && $.lodash.isFunction(serviceResponse.body.asString) && serviceResponse.status === $.net.http.OK) {
			return {
				xml: serviceResponse.body.asString()
			};
		} else {
			return {
				status: serviceResponse ? serviceResponse.status : 500,
				serviceResponse: serviceResponse,
				bodyAsString: serviceResponse && serviceResponse.body && $.lodash.isFunction(serviceResponse.body.asString) ? serviceResponse.body.asString() : ""
			};
		}
	}
};

this.setDefaultAdapter = function(adapterId) {
	var response = {};
	let errorGenerator = new $.ErrorGenerator('setDefaultAdapter');
	if ($.lodash.isNil(adapterId)) {
		throw errorGenerator.paramsError();
	} else {
		if ($.lodash.isNumber(adapterId)) {
			adapterModel.update({
				isDefault: false
			}, {
				where: [{
					literal: {
						type: 'integer',
						value: 1
					},
					operator: '$eq',
					value: 1
		        }]
			});

			response = adapterModel.update({
				isDefault: true
			}, {
				where: [{
					field: "id",
					operator: "$eq",
					value: adapterId
				}]
			}).results.updated;

			if (response) {
				$.messageCodes.push({
					"code": "CORE000062",
					"type": 'S'
				});
			}
		}
	}
	return response;
};

this.getAdaptersByType = function(type, defaultAdapter) {
	var response = [];
	var getDestinies = false;
	if(type !== 'ECC' && type !== 'TDF'){
	    type = JSON.parse(type);
	    getDestinies = type.getDestinies;
    	if(type.type){
    	    type = type.type;
    	}
	}
	
	let errorGenerator = new $.ErrorGenerator('getAdaptersByType');
	try {
		if (!$.lodash.isNil(type)) {
		    let types = {
				'ECC': 1,
				'TDF': 2
			};
			let options = {
				select: [{
					field: "id"
				}, {
					field: "name"
				}, {
				    field: "client"
				}],
				where: [{
					field: "type",
					operator: "$eq",
					value: types[type]
                }]
			};
			if (defaultAdapter) {
				options.where.push({
					field: "isDefault",
					operator: "$eq",
					value: 1 
				});
			}
			response = adapterModel.find(options).results;
		} else {
			throw errorGenerator.paramsError();
		}
	} catch (e) {
		throw errorGenerator.generateError('00', '99', null, e);
	}
	if(getDestinies){
	    $.import('timp.core.server.destinies', 'destinies');
        var destinies = $.timp.core.server.destinies.destinies;
    	return {
    	    adapters: response,
    	    httpsAdapters: destinies.destinies
    	};
	} else {
	    return response;
	}
	
};