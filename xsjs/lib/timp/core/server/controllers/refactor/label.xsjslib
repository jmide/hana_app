$.import('timp.core.server.models.tables', 'label');
const labelModel = $.timp.core.server.models.tables.label.labelModel;

this.getLabels = function(key, value, orderByLabel) {
    let errorGenerator = new $.ErrorGenerator('getLabels');
	var response = {};
	try {
		var options = {
			select: [{
				field: "key",
				as: "objType"
			}, {
				field: "value",
				as: "key"
			}, {
				field: "text",
				as: "label"
			}],
			where: [],
			orderBy: [{
			field: "key",
			type: "asc"
        }]
		};

		var lang = $.request.cookies.get("Content-Language") === "enus" ? "enus" : "ptbr";

		options.where.push({
			field: "lang",
			operator: "$eq",
			value: lang
		});

		if (!$.lodash.isNil(key) && $.lodash.isString(key)){
			options.where.push({
				field: "key",
				operator: "$eq",
				value: key.toUpperCase()
			});
		}

		if (!$.lodash.isNil(value) && $.lodash.isString(value)) {
			options.where.push({
				field: "value",
				operator: "$eq",
				"value": value
			});
		}
		
		if(orderByLabel){
		    options.orderBy = [{
		        field: "text",
			    type: "asc"
		    }];
		}
        
		response = labelModel.find(options).results;

	} catch (e) {
		throw errorGenerator.internalError(e);
	}

	return response;
};