const _ = $.lodash;
$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;
$.import("timp.core.server.models.views", "fileSystem");
const fileSystemCV = $.timp.core.server.models.views.fileSystem.fileSystemCV;

$.import("timp.core.server.models.tables", "fileSystem");
const fileModel = $.timp.core.server.models.tables.fileSystem.fileModel;
const fileFavoriteModel = $.timp.core.server.models.tables.fileSystem.fileFavoriteModel;
const fileShareModel = $.timp.core.server.models.tables.fileSystem.fileShareModel;
const folderModel = $.timp.core.server.models.tables.fileSystem.folderModel;

$.import("timp.core.server.controllers.refactor", "component");
const component = $.timp.core.server.controllers.refactor.component;
const _this = this;
const getFolderChildren = function(id) {
    let errorGenerator = new $.ErrorGenerator('getFolderChildren');
    
	let folders = folderModel.find({
		"select": [{
			"field": "id"
       }, {
			"field": "parentId"
       }],
		"where": [{
			"field": "status",
			"operator": "$eq",
			"value": 1
       }]
	});
	id = id || -1;
	var result = {};
	result[id] = { //ROOT

	};
	if (_.isEmpty(folders.errors) && !_.isEmpty(folders.results)) {
		var mapChildParent = {};
		folders.results.map(function(folder1) {
			mapChildParent[folder1.id] = folder1.parentId;
		});
		const addNode = function(folderId, parentId) {
			if (parentId === -1 && parentId !== id) {
				return undefined;
			} else if (parentId === id) {
				if (!result[id][folderId]) {
					result[id][folderId] = {};
				}
				return result[id][folderId];
			} else {
				const parentNode = addNode(parentId, mapChildParent[parentId]);
				if (parentNode) {
					if (!parentNode[folderId]) {
						parentNode[folderId] = {};
					}
					return parentNode[folderId];
				}
			}
			return undefined;
		};
		for (var folder = 0; folder < folders.results.length; folder++) {
			if (folders.results[folder].id !== id) {
				addNode(folders.results[folder].id, folders.results[folder].parentId);
			}
		}
	} else if(!_.isEmpty(folders.errors)) {
	    throw errorGenerator.readError(folders.errors);
	}
	return result;
};

/**
 *
 * @method createFile
 * @param {json} object
 *   @{integer} folderId - optional
 *   @{integer} {{objectId}} - mandatory
 * @return {integer}
 * */
this.createFile = function(object) {
    let errorGenerator = new $.ErrorGenerator('createFile');
	if (_.isObjectLike(object) && Object.keys(object).length) {
		let objectId;
		for (var param in object) {
			if (param !== "folderId") {
				if (_.isValidInteger(object[param]) && fileModel.getDefinition().fields[param]) {
					objectId = param;
					break;
				}
			}
		}
		if (_.isString(param) && param.match(/.+Id$/g)) {
			let createOptions = {
				"folderId": object.folderId || -1
			};
			createOptions[objectId] = object[objectId];
			const response = fileModel.create(createOptions);
			if (_.isEmpty(response.errors) && _.isObjectLike(response.results) && response.results.created && _.isObjectLike(response.results.instance) &&
				_.isInteger(response.results.instance.id)) {
				return response.results.instance.id;
			}
		}
		throw errorGenerator.paramsError(['id']);
	}
	throw errorGenerator.paramsError();
};

/**
 *
 * @method updateFileStatus
 * @param {json} object
 *   @{integer} {{objectId}} - mandatory
 *   @{string} key - mandatory
 *   @{integer} userId - optional
 *   @{integer} groupId - optional
 * @return {boolean}
 * */
this.updateFileStatus = function(object) {
    let errorGenerator = new $.ErrorGenerator('updateFileStatus');
	if (_.isObjectLike(object) && _.isString(object.key) && object.key.length) {
		var objectId;
		for (var param in object) {
			if (param !== "key" && param.match(/.+Id$/g)) {
				objectId = param;
				break;
			}
		}
		if (objectId && _.isValidInteger(object[objectId])) {
			let whereFileClause = [{
				"operator": "$eq",
				"value": object[objectId]
			}];
			whereFileClause[0].field = objectId;
			let fileResponse = fileModel.find({
				"select": [{
					"field": "id",
					"alias": "file"
                }],
				"where": whereFileClause
			});
			if (_.isEmpty(fileResponse.errors) && !_.isEmpty(fileResponse.results)) {
				let fileId = fileResponse.results[0].id;
				let updateField = {};
				object.key = object.key.toUpperCase();
				switch (object.key) {
					case "STANDARD":
						updateField = {
							"field": "status",
							"value": 0
						};
						break;
					case "ACTIVE":
						updateField = {
							"field": "status",
							"value": 1
						};
						break;
					case "TRASH":
						updateField = {
							"field": "status",
							"value": 2
						};
						break;
					case "DELETED":
						updateField = {
							"field": "status",
							"value": 3
						};
						break;
					case "PUBLIC":
						updateField = {
							"field": "status",
							"value": 4
						};
						break;
				}
				if (Object.keys(updateField).length) {
					let updateStatusResponse = fileModel.update(updateField, {
						"where": [{
							"field": "id",
							"operator": "$eq",
							"value": fileId
						}]
					});
					return _.isEmpty(updateStatusResponse.errors) && updateStatusResponse.results.updated;
				}
				if (object.key === "UNFAVORITE" || object.key === "FAVORITE") {
					return _this.updateFavoriteStatus(object.key,fileId);
				}
				if (object.key === "SHARED" || object.key === "UNSHARED" && (object.userId || object.groupId)) {
                    return _this.updateSharedStatus(object.key,fileId,object.userId,object.groupId);
				}
				
				throw errorGenerator.paramsError(['key']);
			}
			throw errorGenerator.readError();
		}
		throw errorGenerator.paramsError(['id']);
	}
	throw errorGenerator.paramsError();
};
/**
 *
 * @method updateFavoriteStatus
 * @param {string} key
 * @param {integer} fileId
 * @return {boolean}
 * */
this.updateFavoriteStatus = function(key, fileId) {
    let errorGenerator = new $.ErrorGenerator('updateFavoriteStatus');
	if (_.isString(key) && key.length && _.isValidInteger(fileId)) {
		if (key === "UNFAVORITE") {
			let favoriteDelete = fileFavoriteModel.delete({
				"where": [{
					"field": "fileId",
					"operator": "$eq",
					"value": fileId
                            }, {
					"field": "userId",
					"operator": "$eq",
					"value": $.getUserID()
                            }]
			});
			return _.isEmpty(favoriteDelete.errors) && favoriteDelete.results.deleted;
		}
		if (key === "FAVORITE") {
			let favoriteExistsResult = fileFavoriteModel.find({
				"select": [{
					"field": "fileId"
                        }, {
					"field": "userId"
                        }],
				"where": [{
					"field": "userId",
					"operator": "$eq",
					"value": $.getUserID()
                        }, {
					"field": "fileId",
					"operator": "$eq",
					"value": fileId
                        }]
			});
			if (_.isEmpty(favoriteExistsResult.errors) && _.isEmpty(favoriteExistsResult.results)) {
				let createFavoriteResult = fileFavoriteModel.create({
					"userId": $.getUserID(),
					"fileId": fileId
				});
				return _.isEmpty(createFavoriteResult.errors) && createFavoriteResult.results.created;
			}
			return false;
		}
		return false;
	}
	
	throw errorGenerator.paramsError();
};
/**
 *
 * @method updateSharedStatus
 * @param {string} key
 * @param {integer} fileId
 * @param {integer} userId
 * @param {integer} groupId
 * @return {boolean}
 * */
this.updateSharedStatus = function(key, fileId, userId, groupId) {
    let errorGenerator = new $.ErrorGenerator('updateSharedStatus');
    if(_.isString(key) && _.isValidInteger(fileId) && (_.isValidInteger(userId) || _.isValidInteger(groupId))){
        throw "Please inform all mandatory params";
    }
    let whereClause = [{
        "field": "fileId",
        "operator": "$eq",
        "value": fileId
    },{
        "field": userId ? "userId" : "groupId",
        "operator": "$eq",
        "value": userId || groupId
    }];
    if(key === "SHARED" && (userId || groupId)){
        
    }
    if(key === "UNSHARED" && (userId || groupId)){
        
    }
    return false;
};
/**
*
* @method getFileSystem
* @param {object} object = {
    @{integer | mandatory} tabId,
    @{string | mandatory} componentName,
    @{boolean | mandatory} {objectID}
}
* @return {json}
* */
this.getFileSystem = function(object) {
    let errorGenerator = new $.ErrorGenerator('getFileSystem');
	const fileSystemDef = fileSystemCV.getDefinition();
	if (_.isValidInteger(object.tabId) && _.isString(object.componentName)) {
		var flag = Object.keys(object).length > 2;
		var param;
		for (param in object) {
			if (param !== "tabId" && param !== "componentName") {
				if (!fileSystemDef.fields.hasOwnProperty(param)) {
					flag = false;
				}
				break;
			} 
		}
		if (flag) {
			var componentData = util.generalInformation.getComponentInformation(object.componentName.toUpperCase()).results;
			if (!_.isEmpty(componentData)) {
				componentData = componentData[0];
				const folders = folderModel.find({
					"select": [{
						"field": "parentId"
				    }, {
						"field": "id"
				    }, {
						"field": "name"
				    }],
					"where": [{
						"field": "tabId",
						"operator": "$eq",
						"value": object.tabId
                    }, {
						"field": "componentId",
						"operator": "$eq",
						"value": componentData.id
                    }, {
						"field": "status",
						"operator": "$eq",
						"value": 1
                    }, {
						"field": "creationUser",
						"operator": "$eq",
						"value": $.getUserId()
                    }]
				});
				if (_.isEmpty(folders.errors)) {
					const folderXParent = {};
					const folderXName = {};
					const folderIds = folders.results.map(function(folder) {
						folderXParent[folder.id] = folder.parentId;
						folderXName[folder.id] = folder.name;
						return folder.id;
					}).concat([0, -1]);
					const response = fileSystemCV.find({
						"select": [{
							"field": "folderId"
	                    }, {
							"aggregation": {
								"type": "COUNT",
								"param": {
									"field": "status"
								}
							},
							"as": "count"
	                    }],
						"where": [{
							"field": param,
							"operator": "$neq",
							"value": null
	                    }, {
							"field": "folderId",
							"operator": "$in",
							"value": folderIds
	                    }, {
							"field": "creationUser",
							"operator": "$eq",
							"value": $.getUserId()
	                    }, {
							"field": "status",
							"operator": "$notIn",
							"value": [2, 3]
	                    }],
						"groupBy": [{
							"field": "folderId"
                        }],
						"placeholders": [{
							"params": [{
								"name": "$$USER_ID$$",
								"values": [$.getUserId().toString()]
                            }]
                        }]
					});
					var result = {
						"-1": {
							"amount": 0,
							"name": "ROOT",
							"children": {

							}
						},
						"0": {
							"amount": 0,
							"name": "SHARED",
							"children": {

							}
						}
					};
					if (_.isEmpty(response.errors) && !_.isEmpty(response.results)) {
						var amountXFolderId = {};
						folderIds.map(function(id1) {
							amountXFolderId[id1] = 0;
						});
						response.results.map(function(result1) {
							amountXFolderId[result1.folderId] = result1.count;
						});
						const assignAmount = function(folderId, parentId, amount) {
							if (parentId === -1 || parentId === 0) {
								result[parentId].amount += amount;
								if (!result[parentId].children[folderId]) {
									result[parentId].children[folderId] = {
										"amount": 0,
										"name": folderXName[folderId],
										"children": {}
									};
								}
								return result[parentId].children[folderId];
							} else if (folderXParent[parentId]) {
								var parentNode = assignAmount(parentId, folderXParent[parentId], amount);
								if (parentNode) {
									parentNode.amount += amount;
									if (!parentNode.children[folderId]) {
										parentNode.children[folderId] = {
											"amount": amount,
											"name": folderXName[folderId],
											"children": {}
										};
									}
									return parentNode.children[folderId];
								}
								return undefined;
							}
							return undefined;
						};
						for (var id in amountXFolderId) {
							if (amountXFolderId.hasOwnProperty(id)) {
								id = parseInt(id, 10);
								if (id === -1 || id === 0) {
									result[id].amount += amountXFolderId[id];
								} else if (folderXParent[folderXParent[id]]) {
									assignAmount(id, folderXParent[id], amountXFolderId[id]);
								}
							}
						}
					}
					return result;

				}
			} else {
			    throw errorGenerator.paramsError(['componentName']);
			}
		} else {
			throw errorGenerator.paramsError();
		}
	}
	throw errorGenerator.paramsError();
};

/**
*
* @method createFolder
* @param {object} object = {
    @{integer | mandatory} tabId,
    @{string | mandatory} componentName,
    @{string | mandatory} name,
    @{string | optional} description,
    @{integer | optional} parentId
}
* @return {boolean}
* */
this.createFolder = function(object) {
    let errorGenerator = new $.ErrorGenerator('createFolder');
	if (_.isValidInteger(object.tabId) && _.isString(object.componentName) && _.isString(object.name)) {
		const componentData = component.getComponent(object.componentName.toUpperCase());
		if (!_.isEmpty(componentData)) {
			object.componentId = componentData[0].id;
			if (!_.isValidInteger(object.parentId)) {
				object.parentId = -1;
			}
			const response = folderModel.create({
				tabId: parseInt(object.tabId, 10),
				componentId: object.componentId,
				parentId: parseInt(object.parentId, 10),
				name: object.name,
				description: object.description || ""
			});
			if (_.isEmpty(response.errors) && _.isObjectLike(response.results) && response.results.created && _.isObjectLike(response.results.instance) &&
				_.isInteger(response.results.instance.id)) {
				return response.results.created;
			}
			return false;
		} else {
		    throw errorGenerator.paramsError(['componentName']);
		}
	}
	throw errorGenerator.paramsError();
};

/**
*
* @method updateFolder
* @param {} object = {
    @{integer or integer array | mandatory} id,
    @{integer | optional} parentId,
    @{string | optional} name,
    @{string | optional} description,
    @{integer | status} status
}
* @return {boolean}
* */ 
this.updateFolder = function(object) {
	if (_.isValidInteger(object.id) || _.isIntArray(object.id)) {
		let updateOptions = {};
		if (_.isString(object.name)) {
			updateOptions.name = object.name;
		}
		if (_.isString(object.description)) {
			updateOptions.description = object.description;
		}
		if (_.isValidInteger(object.parentId)) {
			updateOptions.parentId = object.parentId;
		}
		if (_.isValidInteger(object.status)) {
			updateOptions.status = object.status;
		}
		if (Object.keys(updateOptions).length) {
			const response = folderModel.update(updateOptions, {
				"where": [{
					"field": "id",
					"operator": _.isIntArray(object.id) ? "$in" : "$eq",
					"value": object.id
                }, {
					"field": "creationUser",
					"operator": "$eq",
					"value": $.getUserId()
                }]
			});
			if (_.isEmpty(response.errors) && response.results.updated) {
				return true;
			}
		}
	}
	return false;
};

/**
*
* @method deleteFolder
* @param {} object = {
    @{integer | mandatory} id
}
* @return {boolean}
* */
this.deleteFolder = function(object) {
    let errorGenerator = new $.ErrorGenerator('deleteFolder');
	if (_.isValidInteger(object.id) && parseInt(object.id, 10) !== -1 && parseInt(object.id, 10) !== 0) {
		const folderChildren = getFolderChildren(object.id);
		var folderIds = [];
		const getFolderId = function(folderChildren) {
			for (var child in folderChildren) {
				if (folderChildren.hasOwnProperty(child)) {
					folderIds.push(parseInt(child, 10));
					if (Object.keys(folderChildren[child]).length) {
						getFolderId(folderChildren[child]);
					}
				}
			}
		};
		getFolderId(folderChildren);
		const deleteResponse = folderModel.update({
			"status": 'Deleted'
		}, {
			"where": [{
				"field": "id",
				"operator": "$in",
				"value": folderIds
            }]
		}, true);
		if (_.isEmpty(deleteResponse.errors) && deleteResponse.results.updated && !_.isEmpty(deleteResponse.results.instances)) {
			fileModel.update({
				"status": 'Deleted'
			}, {
				"where": [{
					"field": "folderId",
					"operator": "$in",
					"value": folderIds
                }]
			});

		}
		return deleteResponse.results.updated;
	}
	throw errorGenerator.paramsError(['id']);
};