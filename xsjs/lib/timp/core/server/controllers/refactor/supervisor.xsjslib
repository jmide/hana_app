$.import('timp.log.server.api', 'api');
var logApi = $.timp.log.server.api.api;

function Supervisor() {}

Supervisor.prototype.automaticCreateUsers = function(object) {
    logApi.createEvent({
       componentName:  "CORE",
       messageCode: 'LOG221004',
       json: object   
    });
};

Supervisor.prototype.automaticUpdateUsers = function(object) {
    logApi.createEvent({
       componentName:  "CORE",
       messageCode: 'LOG221005',
       json: object   
    });
};

Supervisor.prototype.automaticCreateUsersError = function(trace){
    logApi.createEvent({
       componentName:  "CORE",
       messageCode: 'LOG221006',
       json:{ error: trace}
    });
};





/*----------------------------------------Services----------------------------------------*/
Supervisor.prototype.createOutput = function() {
};

this.Supervisor = Supervisor; 