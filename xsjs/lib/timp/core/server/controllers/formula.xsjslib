$.import('timp.core.server.api', 'api');
var coreApi = $.timp.core.server.api.api;
var util = coreApi.util;

//returns outputs from BRB, BFB, BCB
this.getRequiredInfo = function(){
    var response = {};
    try{
        response.bfbOutputs = this.getBFBOutputs();
        response.brbOutputs = this.getBRBOutputs();
        response.bcbOutputs = this.getBCBOutputs();
    }catch(e){
        $.messageCodes.push({code:'',type:'E',errorInfo:util.parseError(e)});
    }
    return response;
};
this.getBFBOutputs = function(){
    try{
        $.import('timp.bfb.server.api','api');
        var bfbApi = $.timp.bfb.server.api.api;
        var bfbOutputController = bfbApi.outputController;
        var response = bfbOutputController.getOutputs();
        return response;
    }catch(e){
        $.messageCodes.push({code: 'TCC201180',type: 'E',errorInfo: util.parseError(e)});
        return [];
    }
};
this.getBRBOutputs = function(){
    try{
        $.import('timp.brb.server.api','api');
        var brbApi = $.timp.brb.server.api.api;
        var brbOutputController = brbApi.controllerOutput;
        var response = brbOutputController.listByOutput();
        if(response && response.length > 0){
            return response;
        }else{
            return [];
        }
    }catch(e){
        $.messageCodes.push({code: 'TCC201181',type: 'E',errorInfo: util.parseError(e)});
        return [];
    }
};
this.getBCBOutputs = function(){
    try{
        $.import('timp.bcb.server.api','api');
        var bcbApi = $.timp.bcb.server.api.api;
        var bcbOutputController = bcbApi.bcbOutputController;
        var response = bcbOutputController.getBCBOutputs();
		return response;
    }catch(e){
        $.messageCodes.push({code: 'TCC201181',type: 'E',errorInfo: util.parseError(e)});
        return [];
    }
};