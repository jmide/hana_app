$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;
$.import('timp.core.server.orm', 'sql');
$.import('timp.core.server.models', 'users');

//IMPORTS
$.import('timp.core.server.models', 'fileSystem');
var folderModel = $.timp.core.server.models.fileSystem.folder;
var folderShareModel = $.timp.core.server.models.fileSystem.folderShare;
var fileModel = $.timp.core.server.models.fileSystem.file;
var fileFavsModel = $.timp.core.server.models.fileSystem.fileFavs;
var fileShareModel = $.timp.core.server.models.fileSystem.fileShare;
$.import('timp.core.server.models', 'objects');
$.import('timp.core.server.models.objects', 'objectTypes');
var objectTypeModel = $.timp.core.server.models.objects.objectTypes;
$.import('timp.core.server.controllers', 'users');
var user = $.timp.core.server.controllers.users;

//CONSTANTS 
this.statusTypes = {
    STANDARD: 0,
    ACTIVE: 1,
    TRASH: 2,
    DELETED: 3,
    PUBLIC: 4
};

/* Get sons of folders
   object = {
		id : 1
   }
*/
this.getFolderSons = function(object) {
    var whereConditions = [];
    whereConditions.push({
        field: 'idParent',
        oper: '=',
        value: object.id
    }, {
        field: 'status',
        oper: '=',
        value: 'Active'
    });
    var temp = folderModel.READ({
        where: whereConditions
    });
    // for(var i = 0; i < temp.length; ++i){
    //     temp[i].folders = this.getFolderSons(temp[i]);
    // }
    if (temp.length > 0) {
        return true;
    }
    return false;
};

/* Get sons of folders
   object = {
		id : 1
   }
*/
this.getTrashSons = function(object) {
    var whereConditions = [];
    whereConditions.push({
        field: 'idParent',
        oper: '=',
        value: object.id
    }, {
        field: 'status',
        oper: '=',
        value: 'Trash'
    });
    var temp = folderModel.READ({
        where: whereConditions
    });
    for (var i = 0; i < temp.length; ++i) {
        //if(this.isNotActive(temp[i]) ){
        temp[i].folders = this.getTrashSons(temp[i]);
        //}

    }
    return temp;
};

/* Get sons of folders
   object = {
		id : 1
   }
*/
this.listFolderTree = function(object) {
    
    try {
        object = object || $.request.parameters.get('object');
        if (typeof object === 'string') {
            object = util.__parse__(object);
        }
        var retVal;
        var whereConditions = [];
        var userID = user.getTimpUser().id;
        var _sharedFolders = [];
        if( object.shared == false ){
            whereConditions = [{
                field: 'idParent',
                oper: '=',
                value: object.hasOwnProperty('id') ? object.id : -1
            }, {
                field: 'status',
                oper: '=',
                value: 'Active'
            }, {
                field: 'creationUser',
                oper: '=',
                value: userID
            }];
        }else if( object.shared === true ){
            _sharedFolders = folderShareModel.READ({
                where: [{
                    field: 'idUser',
                    oper: '=',
                    value: userID
                }]
            });
            for (var x = 0; x < _sharedFolders.length; x++) {
                whereConditions.push({
                    field: 'id',
                    oper: '=',
                    value: _sharedFolders[x].idFolder
                });
            }
        }
        if ( whereConditions.length > 0 ) {
            retVal = folderModel.READ({
                where: object.shared ? [whereConditions] : whereConditions,
                order_by: ['idParent']
            });
            
        } else {
            retVal = [];
        }

        if (retVal.length > 0) {
            var temp = [];
            
            // Avoid DoS object attack
            if ( retVal.length > 1000 ) {
                throw 'Request retVal overflow';
            }
            
            for (var i = 0; i < retVal.length; ++i) {
                if (retVal[i].idParent !== -1) {
                    break;
                } else {
                    temp.push(retVal[i]);
                }
            }

            //Obtiene los Folders Activos
            if (!object.id) {
                retVal = temp;
            }
            
            for (i = 0; i < retVal.length; ++i) {
                retVal[i].folders = object.shared ? false : this.getFolderSons(retVal[i]);
                object.idFolder = retVal[i].id;
                retVal[i].files = this.listFile(object).length;
            }
            if ( object.hasOwnProperty('id') ) {
                return retVal;
            }
        }
        var _mFolders = object.shared ? (_sharedFolders.length > 0 ? true : false) : (retVal.length > 0 ? true : false);
        if( !object.hasOwnProperty('id') ){
            if(object.shared == false ){
                object.idFolder = -1;
                retVal = [{
                    id: -1,
                    name: 'ROOT',
                    folders: _mFolders,
                    files: this.listFile(object).length
                }];
            }else{
                retVal = [{
                    id: 0,
                    name: 'SHARED',
                    folders: _mFolders,
                    files: 0
                }];
            }
        }
        return retVal;
    } catch (e) {
        return (util.parseError(e));
    }
};

this.traverse = function(object, oldBrothers) {
    var exists = false;
    if (oldBrothers === 1) {
        exists = this.searchFolder(object[0], object[oldBrothers].id);
    } else {
        for (var i = 0; i < oldBrothers; ++i) {
            if (i === 0) {
                exists = this.searchFolder(object[i], object[oldBrothers].id);
            } else if (i !== 0 && exists !== true) {
                exists = this.searchFolder(object[i], object[oldBrothers].id);
                if (exists === true) {
                    return exists;
                }
            } else {
                return exists;
            }
        }
    }
    return exists;
};

this.searchFolder = function(object, folderId) {
    var exists = false;
    if (typeof object.folders !== 'undefined') {
        for (var i = 0; i < object.folders.length; ++i) {
            if (object.folders[i].id === folderId) {
                return true;
            } else if (typeof object.folders[i].folders !== 'undefined') {
                exists = this.searchFolder(object.folders[i], folderId);
                if (exists === true) {
                    return true;
                }
            }
        }
    } else {
        return false;
    }

};

this.getFolder = function(object) {
    try {
        if (!object) {
            return null;
        } else if (object.hasOwnProperty('id')) {
            var whereConditions = [];
            whereConditions = [];
            whereConditions.push({
                field: 'id',
                oper: '=',
                value: object.id
            });
            var response = folderModel.READ({
                where: whereConditions
            });
            if (object.id != -1) {
                return response;
            }
            return [{
                id: -1,
                name: 'ROOT'
            }];
        } else {
            return null;
        }


    } catch (e) {
        return util.parseError(e);
    }
};

this.listFolder = function() {

    try {
        var retVal = folderModel.READ();

        if (retVal.length > 0) {
            return retVal;
        } else {
            return {};
        }
    } catch (e) {
        return (util.parseError(e));
    }
};

/* CREATE FOLDER
   This function creates a folder 
   by default it creates a share with the logged user
   by default status = this.statusTypes.ACTIVE
   options.folder = {
		id_parent : 8 | null   //null if your creating in root
		name: "folder 1",
		description : "A folder number one"
   }
*/

this.createFolder = function(object) {

    try {
        // var userID = user.getTimpUser().id; //getUserId();
        if (object.hasOwnProperty('folder')) {
            var folder = object.folder;
            //var folder = options.folder;
            folder.status = this.statusTypes.ACTIVE;
            var result = folderModel.CREATE(folder);
            return result;
        }
        //     if (result.id) { // it created the folder
        //         //log("Created Folder with id...blah");
        //         fileShare.share({
        //             folderId: result.id,
        //             userId: userID
        //         });
        //         return folder;
        //     }
        return folder;
    } catch (e) {
        return (util.parseError(e));
    }
};

/* UPDATE FOLDER
   This function updates a folder 
   this is used to trash, delete, rename and MOVE a folder
   if the status is modified the folder item status are also modified 
   options.folder = {
   		id : 9
		id_parent : 8 | null  //optional
		status: a constant from this.statusTypes. //optional but not null
		name: "folder 1",  //optional
		description : "A folder number one"  //optional
   }
*/
this.updateFolder = function(object) {
    try {
        if(object.folder.hasOwnProperty('idFolder')){
            object.folder.id = object.folder.idFolder;
        }
        var idFolder = object.folder.id;
        //var folder = folderModel.READ(object.folder.id);
        var folder = folderModel.READ({
            where: [{
                field: 'id',
                oper: '=',
                value: object.folder.id
            }]
        });
        if (!folder) {
            return 'errorMessage';
        }
        //modify the values of the folder
        for (var property in object) {
            if (object.hasOwnProperty(property)) {
                folder[property] = object[property];
            }
        }

        //validate/modify parent folder
        //modify id_folder
        var parentFolder;
        if (object.hasOwnProperty('id_parent') && object.id_parent !== null) {
            parentFolder = folderModel.READ( //object.folder.id_parent);
                {
                    where: [{
                        field: 'id',
                        oper: '=',
                        value: object.id_parent
                    }]
                });
            if (!parentFolder) {
                return 'errorMessage';
            }
            if (parentFolder.status === this.statusTypes.TRASH || parentFolder.status === this.statusTypes.DELETED) {
                return 'errorMessage';
            }
            //folder.id_parent = object.file.id_parent;
        }

        //validate/modify status
        //throw object
        if (object.folder.hasOwnProperty('status') && object.folder.status !== null) {
            /*if (!this.statusTypes.hasOwnProperty(object.folder.status)) {
                return "errorMessage";
            }*/
            folder.status = object.folder.status;
            //cascade the status change
            var items = this.updateFolderItems({
                folderId: object.folder.folderId,
                status: object.folder.status
            });
            // Update the Folders
            for (var x = 0; x < items.folders.length; x++) {
                var optionsFolder = {
                    id: items.folders[x],
                    status: folder.status
                };
                // var resultItemsFolder = folderModel.UPDATE(optionsFolder);
                folderModel.UPDATE(optionsFolder);
            }
            // Validate if existe File and make Update
            if (items.file.length > 0) {
                for (var k = 0; k < items.file.length; k++) {
                    var optionsFile = {
                        id: items.file[k],
                        status: folder.status
                    };
                    // var resultFile = fileModel.UPDATE(optionsFile);
                    fileModel.UPDATE(optionsFile);
                }
            }
        }

        var resultFolder = folderModel.UPDATE(object.folder);
        folderShareModel.DELETEWHERE([{field: 'idFolder', oper: '=', value: idFolder}]);
        
        return resultFolder;

        // if ((resultFolder === true && resultItemsFolder === true) || resultFile === true) {
        //     //log("Modified Folder with id...blah");
        //     return {
        //         success: true
        //     };
        // }

    } catch (e) {
        return (util.parseError(e));
    }
};

this.getFileType = function(options) {
    // var objectType = objectTypeModel.READ({
    //      order_by: ['id'],
    //     where: [{
    //         field: 'name',
    //         oper: '=',
    //         value: options.objectType
    //     }]
    // })[0];

    var files = fileModel.READ({
        where: [{
            field: 'status',
            oper: '=',
            value: options.status
        }]
    });
    return files;

};
this.getFileId = function(options){ 
    var objectType = objectTypeModel.READ({
        order_by: ['id'],
        where: [{
            field: 'name',
            oper: '=',
            value: options.objectType
        }]
    })[0].id;
   
    options.where.push({
        field: 'idObjectType',
        oper: '=',
        value: objectType
    });
    var response = fileModel.READ({
		    fields: ['id'],
		    where: options.where
		});
	return response;
};
this.listFiles = function(options) {
    
    var objectType = objectTypeModel.READ({
        order_by: ['id'],
        where: [{
            field: 'name',
            oper: '=',
            value: options.objectType
        }]
    })[0];
    var userID = user.getTimpUser().id;
    for (var i = 0; i < options.status.length; i++) {
        var where =[{
                field: 'status',
                oper: '=',
                value: options.status[i]
            }, {
                field: 'idObjectType',
                oper: '=',
                value: objectType.id
            }];
        if(options.status[i] == 'Trash' || options.hasOwnProperty('attachment')){
            where.push({field: 'creationUser',oper:'=',value:userID});
        }
        var files = fileModel.READ({
            where: where
        });
    }
    return files;
};

this.list = function(options) {
    var objectType = objectTypeModel.READ({
        order_by: ['id'],
        where: [{
            field: 'name',
            oper: '=',
            value: options.objectType
        }]
    })[0];
    var readOptions = {
        where: [{
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }]
    };
    if (options.hasOwnProperty('idObject')) {
        readOptions.where.push({
            field: 'idObject',
            oper: '=',
            value: options.idObject
        });
    }
    if (options.hasOwnProperty('fields')){
        readOptions.fields = options.fields;
    }
    var files = fileModel.READ(readOptions);
    return files;
};

this.listFilesByStatus = function(options) {
    try {
        var objectType = objectTypeModel.READ({
            order_by: ['id'],
            fields: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: options.objectType
            }]
        })[0];
        var readOptions = {
            where: [{
                field: 'idObjectType',
                oper: '=',
                value: objectType.id
            },{
                field: 'status',
                oper: '!=',
                value: this.statusTypes.DELETED
            }]
        };
        if (options.hasOwnProperty('idObject')){
            readOptions.where.push({
                field: 'idObject',
                oper: '=',
                value: options.idObject
            });
        }
        if (options.hasOwnProperty('fields')){
            readOptions.fields = options.fields;
        }
        if (options.hasOwnProperty('status')) {
            switch(options.status){
                case 'ACTIVE': {
                    readOptions.where.push({
                       field: 'status',
                       oper: '=',
                       value: this.statusTypes.ACTIVE
                    });
                    if(options.idUser){
                        readOptions.where.push({
                            field: 'creationUser',
                            oper: '=',
                            value: options.idUser
                        });
                    }
                    break;
                }
                case 'STANDARD': {
                    readOptions.where.push({
                       field: 'status',
                       oper: '=',
                       value: this.statusTypes.STANDARD
                    });
                    break;
                }
                case 'TRASH': {
                    readOptions.where.push({
                       field: 'status',
                       oper: '=',
                       value: this.statusTypes.TRASH
                    });
                    if(options.idUser){
                        readOptions.where.push({
                            field: 'creationUser',
                            oper: '=',
                            value: options.idUser
                        });
                    }
                    break;
                }
                case 'FAVORITE': {
                    readOptions.where.push({
                       field: 'status',
                       oper: '!=',
                       value: this.statusTypes.TRASH
                    });
                    if(options.idUser){
                        readOptions.where.push({
                            field: 'creationUser',
                            oper: '=',
                            value: options.idUser
                        });
                    }
                    break;
                }
                case 'PUBLIC': {
                    readOptions.where.push({
                       field: 'status',
                       oper: '=',
                       value: this.statusTypes.PUBLIC
                    });
                    break;
                }
            }
        }
        var files = fileModel.READ(readOptions);
        return files;
    } catch (e) {
        $.trace.error(e);
        $.messageCodes.push({
            code: 'CORE009047',
            type: 'E', 
            errorInfo: util.parseError(e)
        });
    }
};

this.listSharedFiles = function(options) {
    try {
        var objectType = objectTypeModel.READ({
            order_by: ['id'],
            fields: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: options.objectType
            }]
        })[0];
        var readOptions = {};
        readOptions.where = [{
            field: 'idObjectType',
            oper: '=',
            value: objectType
        }];
        readOptions.where = [{
            field: 'idUser',
            oper: '=',
            value: options.idUser
        }];
        if (options.hasOwnProperty('fields')){
            readOptions.fields = options.fields;
        }
        readOptions.join = [{
            table: fileModel,
            alias: 'file',
            fields: ['idObject'],
            on: [{
                left: 'idFile',
                right: 'id'
            }]
        }];
        return fileShareModel.READ(readOptions);
    } catch (e) {
        $.trace.error(e);
        $.messageCodes.push({
            code: 'CORE009047',
            type: 'E', 
            errorInfo: util.parseError(e)
        });
    }
};

this.listFilesByFolder = function(options) {
    try {
        var objectType = objectTypeModel.READ({
            order_by: ['id'],
            fields: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: options.objectType
            }]
        })[0];
        var readOptions = {
            where: [{
                field: 'idObjectType',
                oper: '=',
                value: objectType.id
            }, {
                field: 'idFolder',
                oper: '=',
                value: options.idFolder
            }, {
                field: 'creationUser',
                oper: '=',
                value: options.idUser
            }]
        };
        readOptions.where.push([{
            field: 'status',
            oper: '=',
            value: this.statusTypes.ACTIVE
        }, {
            field: 'status',
            oper: '=',
            value: this.statusTypes.PUBLIC
        }]);
        if (options.hasOwnProperty('idObject')){
            readOptions.where.push({
                field: 'idObject',
                oper: '=',
                value: options.idObject
            });
        }
        if (options.hasOwnProperty('fields')){
            readOptions.fields = options.fields;
        }
        var files = fileModel.READ(readOptions);
        return files;
    } catch (e) {
        $.trace.error(e);
        $.messageCodes.push({
            code: 'CORE009047',
            type: 'E', 
            errorInfo: util.parseError(e)
        });
    }
};

this.listAllFilesByFolder = function(options) {
    try {
        var objectType = objectTypeModel.READ({
            order_by: ['id'],
            fields: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: options.objectType
            }]
        })[0];
        var readOptions = {
            where: [{
                field: 'idObjectType',
                oper: '=',
                value: objectType.id
            }, {
                field: 'idFolder',
                oper: '=',
                value: options.idFolder
            }]
        };
        readOptions.where.push([{
            field: 'status',
            oper: '=',
            value: this.statusTypes.ACTIVE
        }, {
            field: 'status',
            oper: '=',
            value: this.statusTypes.PUBLIC
        }]);
        if (options.hasOwnProperty('idObject')){
            readOptions.where.push({
                field: 'idObject',
                oper: '=',
                value: options.idObject
            });
        }
        if (options.hasOwnProperty('fields')){
            readOptions.fields = options.fields;
        }
        var files = fileModel.READ(readOptions);
        return files;
    } catch (e) {
        $.trace.error(e);
        $.messageCodes.push({
            code: 'CORE009047',
            type: 'E', 
            errorInfo: util.parseError(e)
        });
    }
};

this.getCounters = function(option) {
    var objectType = objectTypeModel.READ({
        order_by: ['id'],
        where: [{
            field: 'name',
            oper: '=',
            value: option.objectType
        }]
    })[0];
    var files = fileModel.READ({
        where: [{
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }]
    });
    var userID = user.getTimpUser().id;
    var shared = fileShareModel.READ({
        fields: ['id'],
        where: [{
            field: 'idUser',
            oper: '=',
            value: userID
        }, {
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }]
    });
    var favorite = this.listFavoriteFile({
        objectType: option.objectType
    });
    var publicFiles = 0;
    var standardFiles = 0;
    var trashFiles = 0;
    for (var i = 0; i < files.length; i++) {
        if (files[i].status == 'Public') {
            publicFiles++;
        } else if (files[i].status == 'Standard') {
            standardFiles++;
        } else if (files[i].status == 'Trash') {
            if(files[i].creationUser == userID){
                trashFiles++;
            }
        }
    }
    var response = {
        public: publicFiles,
        standard: standardFiles,
        shared: shared.length,
        favorite: favorite.length,
        trash: trashFiles
    };
    return response;
};

this.getFileCounters = function(option) {
    var objectType = objectTypeModel.READ({
        fields: ['id'],
        order_by: ['id'],
        where: [{
            field: 'name',
            oper: '=',
            value: option.objectType
        }]
    })[0];
    var userID = (option.idUser) ? option.idUser : user.getTimpUser().id;
    var whereOptions = [{
        field: 'idObjectType',
        oper: '=',
        value: objectType.id
    }];
    var counts = fileModel.READ({
        count: true,
        where: whereOptions,
        group_by: ['status']
    });
    var response = {
        public: 0,
        standard: 0,
        shared: 0,
        favorite: 0,
        trash: 0
    };
    for (var index = 0; index < counts.length; index++){
        switch (counts[index][0]){
            case 0: {
                response.standard = counts[index][1];
                break;
            }
            case 4: {
                response.public = counts[index][1];
                break;   
            }
        }
    }
    response.trash = fileModel.READ({
        count: true,
        where: [{
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }, {
            field: 'creationUser',
            oper: '=',
            value: userID
        }, {
            field: 'status',
            oper: '=',
            value: 2
        }]
    })[0];
    response.favorite = fileFavsModel.READ({
        count: true,
        fields: ['id'],
        where: [{
            field: 'idUser',
            oper: '=',
            value: userID
        }, {
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }]
    })[0];
    response.shared = fileShareModel.READ({
        count: true,
        fields: ['id'],
        where: [{
            field: 'idUser',
            oper: '=',
            value: userID
        }, {
            field: 'idObjectType',
            oper: '=',
            value: objectType.id
        }]
    })[0];
    return response;
};

/* MODIFY FOLDER ITEMS
   This function cascade updates the items of a folcer

   options.folderId = 8,
   options.status = a value of this.statusTypes
*/
this.updateFolderItems = function(options) {
    try {
        var allFolders = folderModel.READ();
        var listFolders = [];
        var currentLevel = {};
        // var subFolder = folderModel.READ({
        //     where: [{
        //         field: 'idParent',
        //         oper: '=',
        //         value: options.folderId
        //     }]
        // });
        currentLevel.id = options.folderId;
        //var childFolder = this.processChildren(currentLevel, allFolders);
        allFolders = folderModel.READ({
            where: [],
            order_by: ['+id_parent']
        });
        //the first for get the Folder filter and second level
        for (var i = 0; i < allFolders.length; i++) {
            if (allFolders[i].id === options.folderId) {
                listFolders.push(allFolders[i]);
                // let flagFilter = allFolders[i].id;
                allFolders.splice(i, 1);
            } else if (allFolders[i].idParent === options.folderId) {
                listFolders.push(allFolders[i]);
                allFolders.splice(i, 1);
            }
        }
        for (let i = 0; i < listFolders.length; i++) {
            for (var k = 0; k < allFolders.length; k++) {
                if (allFolders[k].idParent === listFolders[i].id) {
                    listFolders.push(allFolders[k]);
                }
            }
        }
        //var tree = {};
        /*var response = {
            allFolders: allFolders,
            id: null,
            folders: []
        };
        for (var x = 0; x < listFolders.length; x++) {
            if (listFolders[x].id === flagFilter || listFolders[x].idParent === null) {
                response.folders.push(this.processChildren(listFolders[x], listFolders));
            }
        }*/
        var result = {
            file: [],
            folders: []
        };
        for (var x = 0; x < listFolders.length; x++) {
            result.folders.push(listFolders[x].id);
        }
        if(result.folders.length>0){
            var file = fileModel.READ({
                fields: ['id'],
                where: [{
                    field: 'idFolder',
                    oper: '=',
                    value: result.folders
                }]
            });
            for (let x = 0; x < file.length; x++) {
                result.file.push(file[x].id);
            }
        }
        
        return result;

        /* for (folder in result.folders)
            this.updateFolder({
                id: folder.id,
                status: options.status
            });

        for (file in result.file)
            this.updateFile({
                id: file.id,
                status: options.status
            });

        return {
            success: true
        };*/
    } catch (e) {
        return (util.parseError(e));
    }

};
this.processChildren = function(currentLevel, allFolders) {
    var returnLevel = {
        id: currentLevel.id,
        folders: []
    };

    for (var i = 0; i < allFolders.length; i++) {
        if (allFolders[i].idParent === returnLevel.id) {
            returnLevel.folders.push(this.processChildren(allFolders[i], allFolders));
        }
    }
    return returnLevel;
};

/*--------------------------------------------------------------------------------------*/
this.getFile = function(object) {
    try {
        var response;
        if (object && object.idObject) {
            var objectType = objectTypeModel.READ({
                order_by: ['id'],
                where: [{
                    field: 'name',
                    oper: '=',
                    value: object.objectType
                }]
            });
            response = fileModel.READ({
                where: [{
                    field: 'idObject',
                    oper: '=',
                    value: object.idObject
                }, {
                    field: 'idObjectType',
                    oper: '=',
                    value: objectType[0].id
                }]
            });
        }
        return response[0];
    } catch (e) {
        return (util.parseError(e));
    }
};

this.listFile = function(object) {

    try {
        var json = object || $.request.parameters.get('object');
        if (typeof json === 'string') {
            json = util.__parse__(json);
        }
        var userID = user.getTimpUser().id;
        var objectType = objectTypeModel.READ({
            order_by: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: json.component + '::' + json.subComponent
            }]
        })[0];
        var response = [];
        if (json.hasOwnProperty('idFolder')) {
            var where =[{
                    field: 'idObjectType',
                    oper: '=',
                    value: objectType.id
                }, {
                    field: 'idFolder',
                    oper: '=',
                    value: json.idFolder
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Trash'
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Deleted'
                }];
            if(json.idFolder  == -1){
                where.push({
                    field: 'creationUser',
                    oper: '=',
                    value:userID
                });
            }  
            response = fileModel.READ({
                where: where
            });
            
        var shareFolders =folderShareModel.READ({
            where:[{
                field: 'idFolder',
                oper: '=',
                value: json.idFolder
            },{
                field: 'idUser',
                oper:'=',
                value: userID
            }],
            join:[{
                table: fileModel,
                alias: 'file',
                on: [{
                    left: 'idFolder',
                    right: 'idFolder'
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Trash'
                },{
                    field: 'idObjectType',
                    oper: '=',
                    value: objectType.id
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Deleted'
                }]
            }]
        });
        } else {
            response = fileModel.READ({
                where: [{
                    field: 'idObjectType',
                    oper: '=',
                    value: objectType.id
                }, {
                    field: 'idFolder',
                    oper: '=',
                    value: -1
                }, {
                    field: 'creationUser',
                    oper: '=',
                    value: userID
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Trash'
                },{
                    field: 'status',
                    oper:'!=',
                    value: 'Deleted'
                }]
            });
        }
        var _ret = [];
        if (response.length > 0) {
            
            // Avoid DoS object attack
            if ( response.length > 1000 ) {
                throw 'Request response overflow';
            }
            
            for (var i = 0; i < response.length; i++) {
                _ret.push(response[i].idObject);
            }
        }
        if(shareFolders.length > 0){
            for(let i = 0; i< shareFolders.length;i++){
                for(var j=0;j<shareFolders[i].file.length;j++){
                    if(_ret.indexOf(shareFolders[i].file[j].idObject) == -1){
                            _ret.push(shareFolders[i].file[j].idObject);
                    }
                }
            }
        }
        return _ret;
        
    } catch (e) {
        return (util.parseError(e));
    }
};

this.listAllUserFiles = function() {
    var userID = user.getTimpUser().id;
    var response = fileModel.READ({
        where: [{
            field: 'creationUser',
            oper: '=',
            value: userID
        }]
    });
    return response;
};

/* CREATE FILE
   This function creates a File 
   by default it creates a share with the logged user
   by default status = this.statusTypes.ACTIVE
   options.file = {
		id_folder : 8    //valid Folder cannot be null
		id_object : 10   //valid object cannot be null
		objectType : "BFB::Form"  //valid objectType cannot be null
   }
*/

this.createFile = function(object) {
    try {
        // var userID = user.getTimpUser().id;
        var file = object.file;
        //validate id_folder
        if (!(file.hasOwnProperty('id_folder') && file.id_folder !== null)) {
            return 'errorMessage';
        }
        //validate id_object
        if (!(file.hasOwnProperty('id_object')) && file.id_object !== null) {
            return 'errorMessage';
        }
        //validate file.objectType
        if (!(file.hasOwnProperty('objectType')) && file.objectType !== null) {
            return 'errorMessage';
        }

        //validate if folder exists and is not in trash or deleted
        //var folder = folderModel.READ(file.id_folder);
        var folder = folderModel.READ({
            where: [{
                field: 'id',
                oper: '=',
                value: file.id_folder
            }]
        });
        //return folder
        if (!folder) {
            return 'errorMessage';
        }
        if (folder.status === this.statusTypes.TRASH || folder.status === this.statusTypes.DELETED) {
            return 'errorMessage';
        }

        //Validate objectType
        var objectType = objectTypeModel.READ({
            //fields: ['single id'],
             order_by: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: file.objectType
            }]
        });
        //return objectType
        if (!objectType) {
            return 'errorMessage';
        }

        file.idObjectType = objectType[0].id;
        file.idFolder = file.id_folder;
        file.status = file.status ? file.status : this.statusTypes.ACTIVE;
        file.idObject = file.id_object;
        file.id = fileModel.CREATE(file);

        // if (file.id) { //it created the file 
        //     //log("Created File with id...blah") 

        //     fileShare.share({
        //         fileId: file.id,
        //         userId: userID
        //     });
        // }
        return file;
    } catch (e) {
        return (util.parseError(e));
    }
};

/* UPDATE FILE
   This function updates a file 
   this is used to trash, delete, rename and MOVE a file
   options.file = {
   		id : 9
		id_folder : 8   //optional but not null
		status: a constant from this.statusTypes. //optional but not null
   }
*/
this.updateFile = function(object) {
    try {
        var objectTypeName = object.file.component+'::'+object.file.subComponent;
        var objectType = objectTypeModel.READ({
             order_by: ['id'],
            where: [{
                field: 'name',
                oper: '=',
                value: objectTypeName
            }]
        })[0];
        for(var i = 0;i < object.file.ids.length;i++){
            var fileResponse = fileModel.READ({
                where: [{
                    field: 'id',
                    oper: '=',
                    value: object.file.ids[i]
                }]
            });
    
            if (!fileResponse) {
                return 'errorMessage Update';
            } else {
                var file = fileResponse[0];
            }
            //modify id_folder
            var folder;
            if (object.file.hasOwnProperty('id_folder') && object.file.id_folder !== null) {
                folder = folderModel.READ({
                    where: [{
                        field: 'id',
                        oper: '=',
                        value: file.idFolder
                    }]
                });
                if (!folder) {
                    return 'errorMessage Update';
                }
                if (folder.status === this.statusTypes.TRASH || folder.status === this.statusTypes.DELETED) {
                    return 'errorMessage Update';
                }
                file.idFolder = object.file.id_folder;
            }
            //Validate if object it contains status
            if (object.file.hasOwnProperty('status') && object.file.status !== null) {
                /*if (!this.statusTypes.hasOwnProperty(object.file.status))
                    return "errorMessage Update";*/
                file.status = object.file.status;
                if(object.file.status === 'Trash' || object.file.status === 'Deleted'){
                    var favorites = fileFavsModel.READ({
                        where:[{
                            field: 'idObjectType',
                            oper: '=',
                            value: objectType.id
                        },{
                            field: 'idFile',
                            oper: '=',
                            value: file.idFile
                        }]
                    });
                    var shared = fileShareModel.READ({
                        where: [{
                            field: 'idFile',
                            oper: '=',
                            value: object.file.ids[i]
                        }, {
                            field: 'idObjectType',
                            oper: '=',
                            value: objectType.id
                        }]
                    });
                    if(favorites.length > 0){
                        for(var j =0; j< favorites.length;j++){
                            fileFavsModel.DELETE(favorites[i].id);
                        }
                    }
                    if(shared.length > 0){
                         for(let j =0; j< shared.length;j++){
                            fileShareModel.DELETE(shared[i].id);
                        }
                    }
                    
                }
            }
    
            /*//Validate if object it contains idFolder
            if (object.file.hasOwnProperty("idFolder") && object.file.idFolder !== null) {
                file.idFolder = object.file.idFolder;
            }*/
           
            // var result = fileModel.UPDATE(file);
            fileModel.UPDATE(file);
        }
        return true  ;
    } catch (e) {
        return (util.parseError(e));
    }
};

// this.getUserFavoriteFiles = function(){
//     var  userID = user.getTimpUser().id;
//     // var fileShare = fileShare
// };
/* Favorite File
   This favorties a file
   options = {
		fileId : 8    //valid Folder cannot be null
		userId : 10   //optional
   }
*/
this.favoriteFile = function(object) {
    try {
        var userID;
        if (object.hasOwnProperty('userId') && object.userId !== null) {
            userID = object.userId;
        } else {
            userID = user.getTimpUser().id;
        }

        if (!object.hasOwnProperty('component') || !object.hasOwnProperty('subComponent')) {
            return false;
        }
        if (object.fileId.length > 0) {
            if (object.isFavorite == false) {
                // var objectTypeName = object.component + '::' + object.subComponent;

                for (var x = 0; x < object.fileId.length; x++) {
                    //validate file exists
                    var file = fileModel.READ({
                        where: [{
                            field: 'id',
                            oper: '=',
                            value: object.fileId[x]
                        }]
                    });
                    if (!file || file.length === 0) {
                        return 'errorMessage: File dont Exist';
                    }
                    file = file[0];
                    //validate favorite exists
                    var favorite = fileFavsModel.READ({
                        where: [{
                            field: 'idFile',
                            oper: '=',
                            value: file.idFile
                        }, {
                            field: 'idUser',
                            oper: '=',
                            value: userID
                        }]
                    });
                    if (favorite.length > 0) {
                        return 'errorMessage: Favorite already Exist';
                    }
                    // var objectType = objectTypeModel.READ({
                    //      order_by: ['id'],
                    //     where: [{
                    //         field: 'name',
                    //         oper: '=',
                    //         value: objectTypeName
                    //     }]
                    // });
                    // create Favorite
                    fileFavsModel.CREATE({
                        idFile: file.idFile,
                        idUser: userID,
                        objectType: file.idObject
                    });
                }
            } else {
                for (let x = 0; x < object.fileId.length; x++) {
                    let file = fileModel.READ({
                        where: [{
                            field: 'id',
                            oper: '=',
                            value: object.fileId[x]
                        }]
                    });
                    if (!file || file.length === 0) {
                        return 'errorMessage: File dont Exist';
                    }
                    file = file[0];
                    let favorite = fileFavsModel.READ({
                        where: [{
                            field: 'idFile',
                            oper: '=',
                            value: file.idObject
                        }, {
                            field: 'idUser',
                            oper: '=',
                            value: userID
                        }]
                    });
                    if (favorite.length > 0) {
                        //delete favorite
                        fileFavsModel.DELETE(favorite[0].id);
                    }
                }
            }
        } else {
            return 'errorMessage: Check Params';
        }
        return true;
    } catch (e) {
        return (util.parseError(e));
    }
};

this.deleteFavoriteFile = function(object) {

    try {
        //validate file exists
        var userID;
        if (object.hasOwnProperty('userId') && object.userId !== null) {
            userID = object.userId;
        } else {
            userID = user.getTimpUser().id;
        }
        if (object.fileId.length > 0) {
            for (var x = 0; x < object.fileId.length; x++) {
                var file = fileModel.READ({
                    where: [{
                        field: 'id',
                        oper: '=',
                        value: object.fileId[x]
                    }]
                });
                if (!file || file.length === 0) {
                    throw 'errorMessage: File dont Exist';
                }
                file = file[0];
                var favorite = fileFavsModel.READ({
                    where: [{
                        field: 'idFile',
                        oper: '=',
                        value: file.idObject
                    }, {
                        field: 'idUser',
                        oper: '=',
                        value: userID
                    }]
                });
                if (favorite.length === 0) {
                    break;
                    //return "errorMessage";
                }
                fileFavsModel.DELETE(favorite[0].id);
                /*{
                        where: [{
                            field: "id",
                            oper: "=",
                            value: favorite[0].id
                        }]
                    });*/
            }
        } else {
            return 'errorMessage';
        }
        return {
            success: true
        };
    } catch (e) {
        return (util.parseError(e));
    }
};

this.listFavoriteFile = function(object) {
    try {
        let options = {
            join:[
                this.getObjectTypeJoin({name: object.objectType}),
                this.getUsersJoins({id: object.idUser})
            ],
            where:[
                {
                    field: 'idObject', 
                    oper: '>=', 
                    value: 0
                }
            ]
        }
        if(object.fields){
            options.fields = object.fields;
        }
        var favorite = fileFavsModel.READ(object);
        return favorite;
    } catch (e) {
        return (util.parseError(e));
    }
};

this.getObjectTypeJoin = function(object){
    let join = {
        table: objectTypeModel,
        alias: 'objectType',
        fields: [],
        on: [
            {
                left: object.columnName || 'idObjectType', 
                right: 'id'
            }
        ] 
    };
    if(object.name){ 
        join.on.push({
            field: 'name', 
            oper: '=', 
            value: object.name
        });
    }
    return join;
};

this.getUsersJoins = function(object){
    let join = {
        table: user.usersTable,
        alias: 'user',
        fields: [],
        on: [
            {
                left: object.columnName || 'idUser',
                right: 'id'
            }
        ]
    };
    if(object.id){
        join.on.push({
            field: 'id', 
            oper: '=', 
            value: object.id
        });
    }else{
        join.on.push({
            field: 'hana_user', 
            oper: '=', 
            value: $.session.getUsername()
        });
    }
    return join;
};

this.foldersRoute = function(object) {
    try {
        if (object) {
            if (object.hasOwnProperty('id')) {
                var whereConditions = [];
                whereConditions = [];
                whereConditions.push({
                    field: 'id',
                    oper: '=',
                    value: object.id
                });
                var currentFolder = folderModel.READ({
                    where: whereConditions
                });
                var response = '';
                if (object.id != -1) {
                    response = '/' + currentFolder[0].name;
                }

                var out = false;
                while (object.id != -1 && out === false) {
                    if (currentFolder[0].idParent === -1) {
                        out = true;
                    } else {
                        whereConditions = [];
                        whereConditions.push({
                            field: 'id',
                            oper: '=',
                            value: currentFolder[0].idParent
                        });
                        currentFolder = folderModel.READ({
                            where: whereConditions
                        });
                        response = '/' + currentFolder[0].name + response;
                    }
                }
                return {
                    path: response
                };
            } else {
                return null;
            }

        } else {
            return null;
        }
    } catch (e) {
        return (util.parseError(e));
    }

};
/*
this.foldersRoute=function(object){
    if (object!= null){
    var response="";
    var acumulate;
    var folder=this.listFolder();
        for (var i=0;i<folder.length;i++){
            if (object.id == folder[i].id){
            acumulate=folder[i].name;
            response=acumulate+"/"+ response;
                if (folder[i].idParent!= null){
                    for (var j=0;j<folder.length;j++){
                        if (folder[i].idParent==folder[j].id){
                        acumulate=folder[j].name;
                        response=acumulate+"/"+ response;  
                            if (folder[j].idParent!=null){
                            object.id=folder[j].idParent;
                            i=0;
                            }else{
                            return response;
                            } 
                        }
                    }   
                }
            }
        }
        return response;
    }else{
        throw "Object is empty"
    }
};*/
this.getFoldersTree=function(object){
    var response={};
    if (object.hasOwnProperty('component')&&object.hasOwnProperty('subComponent')&&object.hasOwnProperty('lang')){
        object.shared=false;
        response.root=this.listFolderTree(object);
        object.shared=true;
        response.shared=this.listFolderTree(object);
    }
    return response;
};
