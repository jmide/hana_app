$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'attachment');
const newImageModel = $.timp.core.server.models.tables.attachment.imageModel;
const newAttachmentModel = $.timp.core.server.models.tables.attachment.attachmentModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importImageData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::Images';
        const targetSchema = newImageModel.getSchema();
        const targetIdentity = newImageModel.getIdentity();
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'name': {
                source: 'NAME'
            },
            'description': {
                source: 'DESCRIPTION'
            },
            'image': {
                source: 'IMAGE'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                pagination: 50,
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                target: newImageModel
            });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importAttachmentData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::Attachments';
        const targetSchema = newAttachmentModel.getSchema();
        const targetIdentity = newAttachmentModel.getIdentity();
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'name': {
                source: 'NAME'
            },
            'description': {
                source: 'DESCRIPTION'
            },
            'mimeType': {
                source: 'MIME_TYPE'
            },
            'componentId': {
                source: 'ID_COMPONENT'
            },
            'file': {
                source: 'FILE'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                pagination: 50,
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newAttachmentModel
            });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};