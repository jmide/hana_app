$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

const sourceSchema = $.schema.slice(1,-1);

function increaseSequence(model, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(model.getSchema(), model.getIdentity() + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
}

this.importOutputData = function(importDataOnlyIfEmpty) {
    try {
        const sourceIdentity = 'CORE::Output';
        
        const oldOutputModel = $.createBaseRuntimeModel(sourceSchema, sourceIdentity, false, true);
        const newOutputModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::OUTPUT', false, true);
        const outputValueModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::OutputValue', false, true);
        const superiodModel = $.createBaseRuntimeModel(sourceSchema, 'TFP::FiscalSubPeriod', false, true);
        
        if (importDataOnlyIfEmpty) {
            let newLogcount = newOutputModel.find({ count: true });
            
            if(!lodash.isEmpty(newLogcount.errors)) {
                return newLogcount;
            }
            if(lodash.isEmpty(newLogcount.results)) {
                return { errors: ['No Target Log values']};
            }
            
            if(newLogcount.results[0].tableCount > 0) {
                return {
                    results: {
                        skipped: true   
                    }
                };
            }
        }
        
        let oldOutputs = oldOutputModel.find({
            aliases: [{
                name: 'OV',
                collection: outputValueModel.getIdentity()
            }, {
                name: 'SP',
                collection: superiodModel.getIdentity()
            }, {
                name: 'O',
                collection: oldOutputModel.getIdentity(),
                isPrimary: true
            }],
            join: [{
                alias: 'OV',
                type: 'left',
                map: 'outputValue',
                on: [{
                    alias: 'OV',
                    field: 'ID_OUTPUT',
                    operator: '$eq',
                    value: {
                        alias: 'O',
                        field: 'ID'
                    }
                }]
            }, {
                alias: 'SP',
                type: 'left',
                map: 'subperiod',
                on: [{
                    alias: 'SP',
                    field: 'ID_COMPANY',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'ID_COMPANY'
                    }
                }, {
                    alias: 'SP',
                    field: 'ID_BRANCH',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'ID_BRANCH'
                    }
                }, {
                    alias: 'SP',
                    field: 'ID_TAX',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'ID_TAX'
                    }
                }, {
                    alias: 'SP',
                    field: 'MONTH',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'MONTH'
                    }
                }, {
                    alias: 'SP',
                    field: 'YEAR',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'YEAR'
                    }
                }, {
                    alias: 'SP',
                    field: 'MONTH',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'MONTH'
                    }
                }, {
                    alias: 'SP',
                    field: 'SUB_PERIOD',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'SUBPERIOD'
                    }
                }, {
                    alias: 'SP',
                    field: 'UF',
                    operator: '$eq',
                    value: {
                        alias: 'OV',
                        field: 'UF'
                    }
                }]
            }],
            where: [{
                alias: 'O',
                field: 'OBJECT_TYPE',
                value: 'BFB::Form'
            }]
        });
        
        let outputValues = lodash.reduce(oldOutputs.results, function(prev, row) {
            let result = [];
            if (lodash.isEmpty(row.subperiod)) {
                result.push({
                    CREATION_USER: row['CREATION.ID_USER'],
                    MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                    CREATION_DATE: row['CREATION.DATE'],
                    MODIFICATION_DATE: row['MODIFICATION.DATE'],
                    NAME: row.NAME,
                    DESCRIPTION: row.DESCRIPTION,
                    BFB_FORM_ID: row.ID_OBJECT,
                    VALUE: row.outputValue && row.outputValue[0] ? row.outputValue[0].VALUE : null,
                    JSON: row.outputValue && row.outputValue[0] ? row.outputValue[0].JSON : null,
                    ORIGINAL_ID: row.ID
                });
            } else {
                lodash.forEach(row.subperiod, function(subperiod) {
                    result.push({
                        CREATION_USER: row['CREATION.ID_USER'],
                        MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                        CREATION_DATE: row['CREATION.DATE'],
                        MODIFICATION_DATE: row['MODIFICATION.DATE'],
                        NAME: row.NAME,
                        DESCRIPTION: row.DESCRIPTION,
                        BFB_FORM_ID: row.ID_OBJECT,
                        VALUE: row.outputValue && row.outputValue[0] ? row.outputValue[0].VALUE : null,
                        JSON: row.outputValue && row.outputValue[0] ? row.outputValue[0].JSON : null,
                        ORIGINAL_ID: row.ID,
                        SUB_PERIOD_ID: subperiod ? subperiod.ID : null
                    });
                });
            }
            
            return lodash.concat(prev, result);
        }, []);
        
        let response = newOutputModel.batchCreate(outputValues);

        return increaseSequence(newOutputModel, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importBRBOutputData = function(importDataOnlyIfEmpty) {
    try {
        if(importDataOnlyIfEmpty) {
            return {
                results: {
                    skipped: true
                }   
            }; 
        }
        
        const newOutputModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::OUTPUT');
        const sourceIdentity = 'BRB::Outputs';
        const targetIdentity = newOutputModel.getIdentity();
        const targetSchema = newOutputModel.getSchema();
        
        const mapping = {
            'originalId': {
                source: 'ID'
            },
            'creationUser': {
                source: 'CREATION_USER'
            },
            'modificationUser': {
                source: 'MODIFICATION_USER'
            },
            'creationDate': {
                source: 'CREATION_DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION_DATE'
            },
            'name': {
                source: 'NAME'
            },
            'description': {
                source: 'DESCRIPTION'
            },
            'json': {
                source: 'JSON'
            },
            'brbReportId': {
                source: 'SOURCE_ID'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newOutputModel
            });

        return response;
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importBCBOutputData = function(importDataOnlyIfEmpty) {
    try {
        if(importDataOnlyIfEmpty) {
            return {
                results: {
                    skipped: true
                }   
            }; 
        }

        const sourceIdentity = 'BCB::Output';
        
        const newOutputModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::OUTPUT');
        const oldOutputModel = services.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        const outputValueModel = services.createBaseRuntimeModel(sourceSchema, 'BCB::OutputValue');
        const superiodModel = services.createBaseRuntimeModel(sourceSchema, 'TFP::FiscalSubPeriod');
        
        let count = oldOutputModel.find({ count: true });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return { errors: ['No Output values']};
        }
        
        count = count.results[0].tableCount;
        let size = 5;
        let pages = Math.ceil(count / size);
        let temp;
        let bcbOutputs = [];

        for(let i = 0; i <= pages; i++) {
            temp = oldOutputModel.find({
                aliases: [{
                    name: 'O',
                    collection: oldOutputModel.getIdentity(),
                    isPrimary: true
                }, {
                    name: 'OV',
                    collection: outputValueModel.getIdentity()
                }, {
                    name: 'SP',
                    collection: superiodModel.getIdentity()
                }, {
                    name: 'SO',
                    collection: oldOutputModel.getIdentity(),
                    query: {
                        select: [{
                            field: 'ID'
                        }],
                        paginate: {
                            limit: size,
                            offset: i * size
                        }
                    }
                }],
                join: [{
                    type: 'inner',
                    alias: 'SO',
                    on: [{
                        alias: 'SO',
                        field: 'ID',
                        operator: '$eq',
                        value: {
                            alias: 'O',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'OV',
                    type: 'left',
                    map: 'outputValue',
                    on: [{
                        alias: 'OV',
                        field: 'ID_OUTPUT',
                        operator: '$eq',
                        value: {
                            alias: 'O',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'SP',
                    type: 'left',
                    map: 'subperiod',
                    on: [{
                        alias: 'SP',
                        field: 'ID_COMPANY',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'ID_COMPANY'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_BRANCH',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'ID_BRANCH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_TAX',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'TAX_CODE'
                        }
                    }, {
                        alias: 'SP',
                        field: 'MONTH',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'MONTH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'YEAR',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'YEAR'
                        }
                    }, {
                        alias: 'SP',
                        field: 'SUB_PERIOD',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'SUBPERIOD'
                        }
                    }, {
                        alias: 'SP',
                        field: 'UF',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'UF'
                        }
                    }]
                }]
            });
            
            if (!lodash.isEmpty(temp.errors)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                let results = [];
                if (lodash.isEmpty(row.subperiod)) {
                    results.push({
                        ORIGINAL_ID: row.ID,
                        CREATION_USER: row['CREATION.ID_USER'],
                        MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                        CREATION_DATE: row['CREATION.DATE'],
                        MODIFICATION_DATE: row['MODIFICATION.DATE'],
                        NAME: row.NAME,
                        DESCRIPTION: row.DESCRIPTION,
                        BCB_CONFIGURATION_ID: row.OBJECT_ID,
                        VALUE: row.KEY
                    });
                } else {
                    lodash.forEach(row.subperiod, function(subperiod) {
                        results.push({
                            ORIGINAL_ID: row.ID,
                            CREATION_USER: row['CREATION.ID_USER'],
                            MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                            CREATION_DATE: row['CREATION.DATE'],
                            MODIFICATION_DATE: row['MODIFICATION.DATE'],
                            NAME: row.NAME,
                            DESCRIPTION: row.DESCRIPTION,
                            BCB_CONFIGURATION_ID: row.OBJECT_ID,
                            VALUE: row.KEY,
                            SUB_PERIOD_ID: subperiod ? subperiod.ID : null
                        });
                    });    
                }
                
                return lodash.concat(prev, results);
            }, []);
            
            bcbOutputs = lodash.concat(bcbOutputs, temp);
        }

        let response = newOutputModel.batchCreate(bcbOutputs);

        return increaseSequence(newOutputModel, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importBFBOutputData = function(importDataOnlyIfEmpty) {
    try {
        if(importDataOnlyIfEmpty) {
            return {
                results: {
                    skipped: true
                }   
            }; 
        }

        const sourceIdentity = 'BFB::tableOutputs';
        
        const newOutputModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::OUTPUT');
        const oldOutputModel = services.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        const outputValueModel = services.createBaseRuntimeModel(sourceSchema, 'BFB::tableOutputValues');
        const superiodModel = services.createBaseRuntimeModel(sourceSchema, 'TFP::FiscalSubPeriod');
        
        let count = oldOutputModel.find({ count: true });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return { errors: ['No Output values']};
        }
        
        count = count.results[0].tableCount;
        let size = 5;
        let pages = Math.ceil(count / size);
        let temp;
        let bfbOutputs = [];

        for(let i = 0; i <= pages; i++) {
            temp = oldOutputModel.find({
                aliases: [{
                    name: 'O',
                    collection: oldOutputModel.getIdentity(),
                    isPrimary: true
                }, {
                    name: 'OV',
                    collection: outputValueModel.getIdentity()
                }, {
                    name: 'SP',
                    collection: superiodModel.getIdentity()
                }, {
                    name: 'SO',
                    collection: oldOutputModel.getIdentity(),
                    query: {
                        select: [{
                            field: 'ID'
                        }],
                        paginate: {
                            limit: size,
                            offset: i * size
                        }
                    }
                }],
                join: [{
                    type: 'inner',
                    alias: 'SO',
                    on: [{
                        alias: 'SO',
                        field: 'ID',
                        operator: '$eq',
                        value: {
                            alias: 'O',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'OV',
                    type: 'left',
                    map: 'outputValue',
                    on: [{
                        alias: 'OV',
                        field: 'ID_TABLE_OUTPUT',
                        operator: '$eq',
                        value: {
                            alias: 'O',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'SP',
                    type: 'left',
                    map: 'subperiod',
                    on: [{
                        alias: 'SP',
                        field: 'ID_COMPANY',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'ID_COMPANY'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_BRANCH',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'ID_BRANCH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_TAX',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'ID_TAX'
                        }
                    }, {
                        alias: 'SP',
                        field: 'MONTH',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'MONTH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'YEAR',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'YEAR'
                        }
                    }, {
                        alias: 'SP',
                        field: 'SUB_PERIOD',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'SUBPERIOD'
                        }
                    }, {
                        alias: 'SP',
                        field: 'UF',
                        operator: '$eq',
                        value: {
                            alias: 'OV',
                            field: 'UF'
                        }
                    }]
                }]
            });
            
            if (!lodash.isEmpty(temp.errors)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                let results = [];
                let outputValue;
                let value;
                if(!$.lodash.isNil(row.outputValue) && !$.lodash.isEmpty(row.outputValue)) {
                    outputValue = row.outputValue[0];
                    value = {
                        totalValue: outputValue.TOTAL_VALUE,
                        groupsValue: outputValue.GROUPS_VALUE
                    };
                }
                if (lodash.isEmpty(row.subperiod)) {
                    results.push({
                        ORIGINAL_ID: row.ID,
                        CREATION_USER: row['CREATION.ID_USER'],
                        MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                        CREATION_DATE: row['CREATION.DATE'],
                        MODIFICATION_DATE: row['MODIFICATION.DATE'],
                        NAME: row.NAME,
                        DESCRIPTION: row.DESCRIPTION,
                        BFB_FORM_ID: !$.lodash.isNil(outputValue) ? outputValue.ID_FORM : null,
                        VALUE: JSON.stringify(value)
                    });
                } else {
                    lodash.forEach(row.subperiod, function(subperiod) {
                        results.push({
                            ORIGINAL_ID: row.ID,
                            CREATION_USER: row['CREATION.ID_USER'],
                            MODIFICATION_USER: row['MODIFICATION.ID_USER'],
                            CREATION_DATE: row['CREATION.DATE'],
                            MODIFICATION_DATE: row['MODIFICATION.DATE'],
                            NAME: row.NAME,
                            DESCRIPTION: row.DESCRIPTION,
                            BFB_FORM_ID: !$.lodash.isNil(outputValue) ? outputValue.ID_FORM : null,
                            VALUE: JSON.stringify(value),
                            SUB_PERIOD_ID: subperiod.ID
                        });
                    });    
                }
                
                return lodash.concat(prev, results);
            }, []);
            
            bfbOutputs = lodash.concat(bfbOutputs, temp);
        }
        
        let response = newOutputModel.batchCreate(bfbOutputs);

        return increaseSequence(newOutputModel, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};
