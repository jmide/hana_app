$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'user');
let newUserModel = $.timp.core.server.models.tables.user.userModel;
const newUserPrivilegeModel = $.timp.core.server.models.tables.user.userPrivilegeModel;
const newUserOrgPrivilegeModel = $.timp.core.server.models.tables.user.userOrgPrivilegeModel;
const newUserPackageModel = $.timp.core.server.models.tables.user.userPackageModel;
const newUserAppPositionsModel = $.timp.core.server.models.tables.user.userAppPositionsModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response, updateTimpUser) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;
    let updateResponse;
    if(updateTimpUser) {
        updateResponse = model.update({
            id: newSequence 
        }, {
            where: [{
                field: 'HANA_NAME',
                operator: '$eq',
                value: sourceSchema
            }]
        });
        newSequence++;
    }
    
    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    response.errors = $.lodash.concat(alterSequenceResponse.errors, response.errors);
    
    if (!$.lodash.isNil(updateResponse)) {
        response.errors = $.lodash.concat(updateResponse.errors, response.errors);   
    }
    
    return response;
};

this.importUserData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;

        const sourceIdentity = 'CORE::Users';
        const targetSchema = newUserModel.getSchema();
        const targetIdentity = newUserModel.getIdentity();

        const oldUserModel = services.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        const userAppPositionsModel = services.createBaseRuntimeModel(sourceSchema, 'MKT::Orders_Apps');

        let users = oldUserModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'HANA_USER'
            }, {
                field: 'NAME'
            }, {
                field: 'LAST_NAME'
            }]
        }).results;
        
        let firstUser;
        let idHanaNameMap = lodash.reduce(users, function(prev, value) {
            if (value.ID === 1) {
                firstUser = value.HANA_USER;
            }
            prev[value.HANA_USER] = {
                id: value.ID,
                name: value.NAME,
                lastName: value.LAST_NAME
            };

            return prev;
        }, {});
        
        let errors = [];
        let timpUserId;
        
        if(!$.lodash.isNil(idHanaNameMap[sourceSchema])) {
            timpUserId = idHanaNameMap[sourceSchema].id;
        } else if($.lodash.String(firstUser) && firstUser !== sourceSchema) {
            timpUserId = -1;
        }
        
        if(!$.lodash.isNil(timpUserId)) {
            let updateResponse = newUserModel.update({
                id: timpUserId 
            }, {
                where: [{
                    field: 'hanaUser',
                    operator: '$eq',
                    value: sourceSchema
                }]
            });    
            errors = updateResponse.errors;
        }
        
        let userApps = userAppPositionsModel.find({
            select: [{
                field: 'ID_USER'
            }, {
                field: 'POSITION'
            }]
        }).results;
        
        let userAppsMap = $.lodash.reduce(userApps, function(prev, row) {
            prev[row.ID_USER] = row.POSITION;
            return prev;
        }, {});
        const activeTranslate = newUserModel.getDefinition().fields.isActive.translate;
        const adminTranslate = newUserModel.getDefinition().fields.isAdmin.translate;
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param] ? idHanaNameMap[param].id : idHanaNameMap[sourceSchema].id;
                }
            },
            'modificationUser': {
                source: 'MODIFICATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param] ? idHanaNameMap[param].id : idHanaNameMap[sourceSchema].id;
                }
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'name': {
                source: 'HANA_USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param] ? idHanaNameMap[param].name || param : '';
                }
            },
            'lastName': {
                source: 'HANA_USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param] ? idHanaNameMap[param].lastName || param : '';
                }
            },
            'hanaUser': {
                source: 'HANA_USER'
            },
            'email': {
                source: 'EMAIL',
                preProcessFn: function(param) {
                    return param || '';
                }
            },
            'position': {
                source: 'CARGO',
                preProcessFn: function(param) {
                    return param || '';
                }
            },
            'isActive': {
                source: 'STATUS',
                useTranslate: true,
                preProcessFn: function(param) {
                    return activeTranslate[param || 0];
                }
            },
            'appPositions': {
                source: 'HANA_USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param] ? userAppsMap[idHanaNameMap[param].id] || null : null;
                }
            },
            'isAdmin': {
                source: 'ADMIN',
                useTranslate: true,
                preProcessFn: function(param) {
                    return adminTranslate[param || 0];
                }
            }
        };
        let filters = [{
           field: 'HANA_USER',
           operator: '$notIn',
           value: {
               collection: 'CORE::USER',
               query: {
                   select: [{
                       field: 'hanaUser'
                   }]
               }
           }
        }];
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, { filters: filters, targetModel: newUserModel });
        response.errors = $.lodash.concat(response.errors, errors);
        
        return increaseSequence(targetSchema, targetIdentity, 'ID', response, timpUserId === -1);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importUserPrivilegeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Privileges_Users';
        const targetSchema = newUserPrivilegeModel.getSchema();
        const targetIdentity = newUserPrivilegeModel.getIdentity();
        
        let mapping = {
            userId: {
                source: 'ID_USER'
            },
            privilegeId: {
                source: 'ID_PRIVILEGE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newUserPrivilegeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importUserOrgPrivilegeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Org_Privis_Users';
        const targetSchema = newUserOrgPrivilegeModel.getSchema();
        const targetIdentity = newUserOrgPrivilegeModel.getIdentity();

        let mapping = {
            userId: {
                source: 'ID_USER'
            },
            companyId: {
                source: 'ID_COMPANY'
            },
            branchId: {
                source: 'ID_BRANCH'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newUserOrgPrivilegeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importUserPackageData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Packages_Users';
        const targetSchema = newUserPackageModel.getSchema();
        const targetIdentity = newUserPackageModel.getIdentity();

        let mapping = {
            userId: {
                source: 'ID_USER'
            },
            packageId: {
                source: 'ID_PACKAGE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newUserPackageModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importUserAppPositionData = function(importDataOnlyIfEmpty) {
  try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Orders_Apps';
        const targetSchema = newUserAppPositionsModel.getSchema();
        const targetIdentity = newUserAppPositionsModel.getIdentity();

        let mapping = {
            'userId': {
                source: 'ID_USER'
            },
            'positions': {
                source: 'POSITION'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newUserAppPositionsModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};