$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'group');
const newGroupModel = $.timp.core.server.models.tables.group.groupModel;
const newGroupUserModel = $.timp.core.server.models.tables.group.groupUserModel;
const newGroupPrivilegeModel = $.timp.core.server.models.tables.group.groupPrivilegeModel;
const newGroupPackageModel = $.timp.core.server.models.tables.group.groupPackageModel;
const newGroupOrgPrivilegeModel = $.timp.core.server.models.tables.group.groupOrgPrivilegeModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);

    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importGroupData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;

        const sourceIdentity = 'MKT::Groups';
        const targetSchema = newGroupModel.getSchema();
        const targetIdentity = newGroupModel.getIdentity();
        
        const oldUserModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::Users');

        let users = oldUserModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'HANA_USER'
            }]
        }).results;
    
        let idHanaNameMap = lodash.reduce(users, function(prev, value) {
            prev[value.HANA_USER] = value.ID;
            return prev;
        }, {});
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param];
                }
            },
            'modificationUser': {
                source: 'MODIFICATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param];
                }
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'name': {
                source: 'NAME'
            },
            'description': {
                source: 'DESCRIPTION'
            },
            'leaderId': {
                source: 'LEADER'
            },
            'isActive': {
                source: 'STATUS',
                useTranslate: true,
                preProcessFn: function(param) {
                    return param === 1;
                }
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newGroupModel
            });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importGroupUserData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Users_Groups';
        const targetSchema = newGroupUserModel.getSchema();
        const targetIdentity = newGroupUserModel.getIdentity();
        
        let mapping = {
            groupId: {
                source: 'ID_GROUP'
            },
            userId: {
                source: 'ID_USER'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newGroupUserModel
            });

        return response;
    } catch(e) {
	    return {
            errors: $.parseError(e)
        };
    }
};

this.importGroupPrivilegeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Privileges_Groups';
        const targetSchema = newGroupPrivilegeModel.getSchema();
        const targetIdentity = newGroupPrivilegeModel.getIdentity();
        
        let mapping = {
            groupId: {
                source: 'ID_GROUP'
            },
            privilegeId: {
                source: 'ID_PRIVILEGE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newGroupPrivilegeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importGroupPackageData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Packages_Groups';
        const targetSchema = newGroupPackageModel.getSchema();
        const targetIdentity = newGroupPackageModel.getIdentity();
        
        let mapping = {
            groupId: {
                source: 'ID_GROUP'
            },
            packageId: {
                source: 'ID_PACKAGE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newGroupPackageModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importGroupOrgPrivilegeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Org_Privis_Groups';
        const targetSchema = newGroupOrgPrivilegeModel.getSchema();
        const targetIdentity = newGroupOrgPrivilegeModel.getIdentity();

        let mapping = {
            groupId: {
                source: 'ID_GROUP'
            },
            companyId: {
                source: 'ID_COMPANY'
            },
            branchId: {
                source: 'ID_BRANCH'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newGroupOrgPrivilegeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};
