$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'tax');
const newTaxModel = $.timp.core.server.models.tables.tax.taxModel;
const newTaxTypeModel = $.timp.core.server.models.tables.tax.taxTypeModel;
const newTaxMapModel = $.timp.core.server.models.tables.tax.taxMapModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importTaxData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'ATR::Tributo';
        const targetSchema = newTaxModel.getSchema();
        const targetIdentity = newTaxModel.getIdentity();

        const mapping = {
            'code': {
                source: 'COD_TRIBUTO'
            },
            'description': {
                source: 'DESCR_COD_TRIBUTO_LABEL'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newTaxModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importTaxTypeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'ATR::TributoTipo';
        const targetSchema = newTaxTypeModel.getSchema();
        const targetIdentity = newTaxTypeModel.getIdentity();

        const mapping = {
            'code': {
                source: 'COD_TRIBUTO_TIPO'
            },
            'description': {
                source: 'DESCR_COD_TRIBUTO_TIPO_LABEL'
            },
            'coverage': {
                source: 'ABRANGENCIA'
            },
            'operationMode': {
                source: 'MODO_OPERACAO'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newTaxTypeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importTaxMapData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'ATR::TributoMapa';
        const targetSchema = newTaxMapModel.getSchema();
        const targetIdentity = newTaxMapModel.getIdentity();

        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'taxOrigin': {
                source: 'ORIGEM_TRIBUTO'
            },
            'taxTypeCode': {
                source: 'COD_TRIBUTO'
            },
            'taxCodeERP': {
                source: 'COD_TRIBUTO_ERP'
            },
            'taxDescription': {
                source: 'DESCRICAO_CODIGO_ERP'
            },
            'taxTypeDescription': {
                source: 'TIPO_TRIBUTO_ALLTAX'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newTaxMapModel
            });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};