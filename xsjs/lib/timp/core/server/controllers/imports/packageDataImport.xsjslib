$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'package');
const newPackageModel = $.timp.core.server.models.tables.group.packageModel;
const newPackagePrivilegeModel = $.timp.core.server.models.tables.group.packagePrivilegeModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importPackageData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Packages';
        const targetSchema = newPackageModel.getSchema();
        const targetIdentity = newPackageModel.getIdentity();
        
        const oldUserModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::Users', false, true);

        let users = oldUserModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'HANA_USER'
            }]
        }).results;
    
        let idHanaNameMap = lodash.reduce(users, function(prev, value) {
            prev[value.HANA_USER] = value.ID;
            return prev;
        }, {});
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param];
                }
            },
            'modificationUser': {
                source: 'MODIFICATION.USER',
                preProcessFn: function(param) {
                    return idHanaNameMap[param];
                }
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'name': {
                source: 'NAME'
            },
            'description': {
                source: 'DESCRIPTION'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newPackageModel
            });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importPackagePrivilegeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'MKT::Privileges_Packages';
        const targetSchema = newPackagePrivilegeModel.getSchema();
        const targetIdentity = newPackagePrivilegeModel.getIdentity();
        
        let mapping = {
            packageId: {
                source: 'ID_PACKAGE'
            },
            privilegeId: {
                source: 'ID_PRIVILEGE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newPackagePrivilegeModel
            });

        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};