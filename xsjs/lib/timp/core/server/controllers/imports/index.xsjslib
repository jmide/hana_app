$.import('timp.core.server.controllers.imports', 'fileSystemDataImport');
$.import('timp.core.server.controllers.imports', 'userDataImport');
$.import('timp.core.server.controllers.imports', 'groupDataImport');
$.import('timp.core.server.controllers.imports', 'packageDataImport');
$.import('timp.core.server.controllers.imports', 'attachmentDataImport');
$.import('timp.core.server.controllers.imports', 'configurationDataImport');
$.import('timp.core.server.controllers.imports', 'logDataImport');
$.import('timp.core.server.controllers.imports', 'outputDataImport');
$.import('timp.core.server.controllers.imports', 'componentDataImport');
$.import('timp.core.server.controllers.imports', 'taxDataImport');
$.import('timp.core.server.controllers.imports', 'proxyServiceDataImport');
$.import('timp.core.server.controllers.imports', 'labelDataImport');

let imports = [
    $.timp.core.server.controllers.imports.fileSystemDataImport,
    $.timp.core.server.controllers.imports.userDataImport,
    $.timp.core.server.controllers.imports.groupDataImport,
    $.timp.core.server.controllers.imports.packageDataImport,
    $.timp.core.server.controllers.imports.attachmentDataImport,
    $.timp.core.server.controllers.imports.configurationDataImport,
    $.timp.core.server.controllers.imports.logDataImport,
    $.timp.core.server.controllers.imports.outputDataImport,
    $.timp.core.server.controllers.imports.componentDataImport,
    $.timp.core.server.controllers.imports.taxDataImport,
    $.timp.core.server.controllers.imports.proxyServiceDataImport,
    $.timp.core.server.controllers.imports.labelDataImport
];

let _this = this;
$.lodash.forEach(imports, function(_import) {
    for (let methodName in _import) {
    	if (_import.hasOwnProperty(methodName) && $.lodash.isFunction(_import[methodName])) {
    		_this[methodName] = _import[methodName];
    	}
    }
});