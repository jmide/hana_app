$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.orm', 'connector');
const connector = $.timp.core.server.orm.connector;

$.import('timp.core.server.orm', 'resources');
const resources = $.timp.core.server.orm.resources;

$.import('timp.core.server.libraries.external','cryptoJS');
const CryptoJS = $.timp.core.server.libraries.external.cryptoJS.CryptoJS;
                    
$.import('timp.core.server.models.tables', 'proxyService');
const newAdapterModel = $.timp.core.server.models.tables.proxyService.adapterModel;
const newServiceModel = $.timp.core.server.models.tables.proxyService.serviceModel;
const newServiceDestinationModel = $.timp.core.server.models.tables.proxyService.serviceDestinationModel;

$.import('timp.core.server.libraries.internal', 'util');
const util = $.timp.core.server.libraries.internal.util;

const schema = $.schema.slice(1,-1);
   
const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!$.lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = $.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!$.lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!$.lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importAdapterData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        let systemData = $.execute('SELECT SYSTEM_ID, HOST FROM "M_DATABASE"').results[0];
        let phrase = systemData ? systemData.HOST + '-' + systemData.SYSTEM_ID : 'Deleted59Hashed53Cords';

        const sourceIdentity = 'TIS::AdapterConfig';
        const targetIdentity = newAdapterModel.getIdentity();
        const targetSchema = newAdapterModel.getSchema();
        
        const oldAdapterModel = $.createBaseRuntimeModel(schema, sourceIdentity);
        
        let responseTranslate = newAdapterModel.getDefinition().fields.type.translate;
        let typeTranslate = [];
        $.lodash.forEach(responseTranslate, function(value, key) {
            typeTranslate.push(value);
        });
        
        let adapters = oldAdapterModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'STATE'
            }]
        }).results;
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.ID_USER'
            },
            'modificationUser': {
                source: 'MODIFICATION.ID_USER'
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'type': {
                useTranslate: true,
                source: 'ADAPTER_NAME',
                preProcessFn: function(param) {
                    return typeTranslate.indexOf(param) !== -1 ? param : 'TDF';
                }
            },
            'name': {
                source: 'ADAPTER_NAME'
            },
            'hostName': {
                source: 'ADAPTER_URL'
            },
            'port': {
                source: 'ADAPTER_PORT'
            },
            'userName': {
                source: 'USER'
            },
            'password': {
                source: 'PASSWORD',
                preProcessFn: function(param) {
                    let decriptedPassword = $.decrypt(param, null, null, {
                        key: CryptoJS.enc.Base64.parse("253D3FB468A0E24677C28A624BE0F939"),
                        iv: CryptoJS.enc.Base64.parse("                ")
                    });

                    return $.encrypt(decriptedPassword, phrase);
                }
            },
            'client': {
                source: 'SAP_CLIENT'
            },
            'isActive': {
                useTranslate: true,
                source: 'SAP_CLIENT',
                preProcessFn: function(param) {
                    return true;
                }
            }
        };
        
        let response = services
            .importData(schema, sourceIdentity, targetSchema, targetIdentity, mapping, { importDataOnlyIfEmpty: importDataOnlyIfEmpty, targetModel: newAdapterModel });
        
        let updateResponse = [];

        $.lodash.forEach(adapters, function(row) {
            updateResponse.push(newAdapterModel.update({
              isActive: row.STATE === 1
            }, {
                where: [{
                    field: 'id',
                    operator: '$eq',
                    value: row.ID
                }]
            }));
        });

        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importServiceData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = newServiceModel.getIdentity();
        const targetIdentity = newServiceDestinationModel.getIdentity();
        const oldServiceModel = $.createBaseRuntimeModel(schema, 'TIS::ServiceConfig');
        
        let oldServicesMap = oldServiceModel.find({
            select: [{
                field: 'SERVICE_NAME'
            }, {
                field: 'URL'
            }]
        });
        
        if(!$.lodash.isEmpty(oldServicesMap.errors)) {
            return oldServicesMap;
        }
        
        oldServicesMap = $.lodash.reduce(oldServicesMap.results, function(prev, row) {
            prev[row.SERVICE_NAME] = row;
            return prev;
        }, {});
        
        let typeMap = newAdapterModel.getDefinition().fields.type.translate;
        let adapters = newAdapterModel.find({
            select: [{
                field: 'id'
            }, {
                field: 'type'
            }, {
                field: 'isActive'
            }],
            where: [{
                field: 'isActive',
                operator: '$eq',
                value: 1
            }],
            orderBy: [{
                field: 'id'
            }]
        });
        
        if(!$.lodash.isEmpty(adapters.errors)) {
            return adapters;
        }
        
        adapters = adapters.results;
        
        let mapping = {
            'serviceId': {
                source: 'ID'
            },
            'destination': {
                source: 'NAME',
                preProcessFn: function(param) {
                    let service = oldServicesMap[param];
                    return !$.lodash.isNil(service) ? service.URL || '' : '';
                }
            },
            'adapterId': {
                source: 'TYPE',
                preProcessFn: function(param) {
                    let type = typeMap[param];
                    let adapter = $.lodash.find(adapters, function(item) {
                        return item.type === type;
                    });
                    return $.lodash.isNil(adapter) ? adapters[0] : adapter.id;
                }
            }
        };
        
        let filters = [{
           field: 'NAME',
           operator: '$in',
           value: $.lodash.keys(oldServicesMap)
        }];
        
        let response = services
            .importData(schema, sourceIdentity, schema, targetIdentity, mapping, {
                filters: filters,
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newServiceDestinationModel
            });
        
        if($.lodash.isEmpty(response.errors) && response.results.imported && !response.results.skipped) {
            let serviceIds = newServiceDestinationModel.find({
                select: [{
                    field: 'serviceId'
                }, {
                    aggregation: {
                        type: 'COUNT',
                        param: {
                            field: 'serviceId'
                        }
                    }
                }],
                groupBy: [{
                    field: 'serviceId'
                }],
                having: [{
                    aggregation: {
                        type: 'COUNT',
                        param: {
                            field: 'serviceId'
                        }
                    },
                    operator: '$eq',
                    value: 1
                }]
            });
            
            if (!$.lodash.isEmpty(serviceIds.errors)) {
                response.errors = $.lodash.concat(serviceIds.errors);
            } else {
                serviceIds = $.lodash.map(serviceIds.results, function(row) {
                    return row.serviceId;
                });
                let updateResponse = newServiceDestinationModel.update({
                    isDefault: true
                }, {
                    where: [{
                        field: 'serviceId',
                        operator: '$in',
                        value: serviceIds
                    }]
                });
                
                if (!$.lodash.isEmpty(updateResponse.errors)) {
                    response.errors = $.lodash.concat(updateResponse.errors);
                }
            }
        }
        
        return response;
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};