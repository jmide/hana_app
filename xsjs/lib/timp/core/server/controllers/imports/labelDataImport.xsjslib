$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'label');
const newLabelModel = $.timp.core.server.models.tables.label.labelModel;

const sourceSchema = $.schema.slice(1,-1);

this.importLabelsData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        if (importDataOnlyIfEmpty) {
            let newLabelcount = newLabelModel.find({ count: true });   
            
            if(!lodash.isEmpty(newLabelcount.errors)) {
                return newLabelcount;
            }
            if(lodash.isEmpty(newLabelcount.results)) {
                return { errors: ['No Target Label values']};
            }
            
            if(newLabelcount.results[0].tableCount > 0) {
                return {
                    results: {
                        skipped: true   
                    }
                };
            }
        }
        
        const sourceIdentity = 'CORE::Labels';

        const oldLabelsModel = services.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        
        let count = oldLabelsModel.find({ count: true });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return { errors: ['No Label values']};
        }
        
        count = count.results[0].tableCount;
        let pages = Math.ceil(count / 200);
        let temp;
        let labels = [];
        for(let i = 0; i <= pages; i++) {
            temp = oldLabelsModel.find({
                paginate: {
                    limit: 200,
                    offset: i * 200
                }
            });
            
            if (!lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                let newRows = [];
                newRows.push({
                    key: row.OBJTYPE,
                    lang: 'enus',
                    value: row.KEY,
                    text: row.US_LABEL
                });
                newRows.push({
                    key: row.OBJTYPE,
                    lang: 'ptbr',
                    value: row.KEY,
                    text: row.BR_LABEL
                });
                
                return lodash.concat(prev, newRows);
            }, []);
            
            labels = lodash.concat(labels, temp);
        }
        
        
        let response = newLabelModel.batchCreate(labels);

        return response;
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};