$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'component');
const newComponentModel = $.timp.core.server.models.tables.component.componentModel;
const newInternationalizationModel = $.timp.core.server.models.tables.component.internationalizationModel;
const newPrivilegeModel = $.timp.core.server.models.tables.component.privilegeModel;

const sourceSchema = $.schema.slice(1,-1);
        
const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importComponentData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;

        const sourceIdentity = 'CORE::Components';
        const targetIdentity = newComponentModel.getIdentity();
        const targetSchema = newComponentModel.getSchema();
        
        const appsModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::Apps');
        
        let apps = appsModel.find({
            select: [{
                field: 'COMPONENT_ID'
            }, {
                field: 'US_LABEL'
            }, {
                field: 'BR_LABEL'
            }]
        }).results;
        
        let appsMap = lodash.reduce(apps, function(prev, row) {
            prev[row.COMPONENT_ID] = {
                id: row.COMPONENT_ID,
                usLabel: row.US_LABEL,
                brLabel: row.BR_LABEL
            };
            
            return prev;
        }, {});
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'name': {
                source: 'NAME'
            },
            'labelEnus': {
                source: 'ID',
                preProcessFn: function(param) {
                    return appsMap[param] ? appsMap[param].usLabel : null;
                }
            },
            'labelPtbr': {
                source: 'ID',
                preProcessFn: function(param) {
                    return appsMap[param] ? appsMap[param].brLabel : null;
                }
            },
            'version': {
                source: 'VERSION'
            }
        };
        let filters = [{
           field: 'NAME',
           operator: '$notIn',
           value: {
               collection: 'CORE::COMPONENT',
               query: {
                   select: [{
                       field: 'name'
                   }]
               }
           }
        }, {
           field: 'NAME',
           operator: '$notIn',
           value: ['TIS', 'UES', 'MKT', 'SCNC']
        }];
        
        let oldComponentsModel = $.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        if(oldComponentsModel && !$.lodash.isEmpty(oldComponentsModel.errors)){
            return true;
        }
    
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, { filters: filters, targetModel: newComponentModel });
        
        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importMessageCodeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::MessageCodeText';
        const targetIdentity = newInternationalizationModel.getIdentity();
        const targetSchema = newInternationalizationModel.getSchema();

        const messageCodeModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::MessageCode');
        
        let count = messageCodeModel.find({ count: true });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return {
                errors: ['No MessageCode values']
            };
        }
        
        count = count.results[0].tableCount;
        let pages = Math.ceil(count / 200);
        let temp;
        let messageCodeMap = {};
        for(let i = 0; i <= pages; i++) {
            temp = messageCodeModel.find({
                select: [{
                    field: 'CODE'
                }, {
                    field: 'ID_COMPONENT'
                }],
                orderBy: [{ field: 'ID' }],
                paginate: {
                    limit: 200,
                    offset: i * 200
                }
            });
            
            if (!lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                prev[row.CODE] = row.ID_COMPONENT;
                
                return prev;
            }, {});
            
            lodash.assign(messageCodeMap, temp);
        }
        
        const mapping = {
            'code': {
                source: 'CODE'
            },
            'lang': {
                source: 'LANG'
            },
            'text': {
                source: 'TEXT'
            },
            'componentId': {
                source: 'CODE',
                preProcessFn: function(param) {
                    return messageCodeMap[param];
                }
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newInternationalizationModel
            });

        return response;
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importPrivilegeData = function(importDataOnlyIfEmpty) {
  try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;

        const sourceIdentity = 'CORE::Privileges';
        const targetIdentity = newPrivilegeModel.getIdentity();
        const targetSchema = newPrivilegeModel.getSchema();
        
        const appsModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::Apps');
        const privilegesModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::Privileges');

        let components = appsModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'COMPONENT_ID'
            }]
        }).results;
    
        let appsMap = lodash.reduce(components, function(prev, row) {
            prev[row.ID] = row.COMPONENT_ID;
            
            return prev;
        }, {});
        
        let count = privilegesModel.find({ count: true });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return {
                errors: ['No MessageCode values']
            };
        }
        
        count = count.results[0].tableCount;
        let pages = Math.ceil(count / 200);
        let temp;
        let privilegesMap = {};
        for(let i = 0; i <= pages; i++) {
            temp = privilegesModel.find({
                select: [{
                    field: 'ID'
                }, {
                    field: 'DESCRIPTION'
                }, {
                    field: 'DESCRIPTION_ENUS'
                }, {
                    field: 'DESCRIPTION_PTBR'
                }],
                orderBy: [{ field: 'ID' }],
                paginate: {
                    limit: 200,
                    offset: i * 200
                }
            });
            
            if (!lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                prev[row.ID] = {
                    desc: !lodash.isNil(row.DESCRIPTION) ? row.DESCRIPTION.toString() : '',
                    enus: !lodash.isNil(row.DESCRIPTION_ENUS) ? row.DESCRIPTION_ENUS.toString() : '',
                    ptbr: !lodash.isNil(row.DESCRIPTION_PTBR) ? row.DESCRIPTION_PTBR.toString() : ''
                };
                
                return prev;
            }, {});
            
            lodash.assign(privilegesMap, temp);
        }
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'componentId': {
                source: 'ID_APP',
                preProcessFn: function(param) {
                    return appsMap[param];
                }
            },
            'name': {
                source: 'NAME'
            },
            'descriptionEnus': {
                source: 'ID',
                preProcessFn: function(param) {
                    return privilegesMap[param].enus || privilegesMap[param].desc;
                }
            },
            'descriptionPtbr': {
                source: 'ID',
                preProcessFn: function(param) {
                    return privilegesMap[param].ptbr || privilegesMap[param].desc;
                }
            },
            'role': {
                source: 'ROLE'
            }
        };
        
        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newPrivilegeModel
            });
        
        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};