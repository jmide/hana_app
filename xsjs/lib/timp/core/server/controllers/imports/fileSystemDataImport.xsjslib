$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.orm', 'BaseModel');
const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server.models.tables', 'fileSystem');
const newObjectTypeModel = $.timp.core.server.models.tables.fileSystem.objectTypeModel;
const newFolderModel = $.timp.core.server.models.tables.fileSystem.folderModel;
const newFolderShareModel = $.timp.core.server.models.tables.fileSystem.folderShareModel;
const newFileModel = $.timp.core.server.models.tables.fileSystem.fileModel;
const newFileShareModel = $.timp.core.server.models.tables.fileSystem.fileShareModel;
const newFileFavoriteModel = $.timp.core.server.models.tables.fileSystem.fileFavoriteModel;

const sourceSchema = $.schema.slice(1,-1);

const increaseSequence = function(targetSchema, targetIdentity, field, response) {
    if(!lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = services.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    'function': {
                        name: 'IFNULL',
                        params: [{
                            field: 'id'
                        }, 0]
                    }
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
};

this.importObjectTypeData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;

        const sourceIdentity = 'CORE::ObjectType';
        const targetSchema = newObjectTypeModel.getSchema();
        const targetIdentity = newObjectTypeModel.getIdentity();
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'componentId': {
                source: 'ID_COMPONENT'
            },
            'name': {
                source: 'NAME'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newObjectTypeModel
            });
        
        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importFolderData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::Folder';
        const targetSchema = newFolderModel.getSchema();
        const targetIdentity = newFolderModel.getIdentity();
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.ID_USER'
            },
            'modificationUser': {
                source: 'MODIFICATION.ID_USER'
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'parentId': {
                source: 'ID_PARENT'
            },
            'objectTypeId': {
                source: 'ID_OBJECT_TYPE'
            },
            'name': {
                source: 'NAME'
            },
            'status': {
                source: 'STATUS'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newFolderModel
            });
    
        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importFolderShareData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::FolderShare';
        const targetSchema = newFolderShareModel.getSchema();
        const targetIdentity = newFolderShareModel.getIdentity();
        
        const mapping = {
            'folderId': {
                source: 'ID_FOLDER'
            },
            'userId': {
                source: 'ID_USER'
            },
            'groupId': {
                source: 'ID_GROUP'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newFolderShareModel
            });
    
        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importFilesData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::File';
        const targetSchema = newFileModel.getSchema();
        const targetIdentity = newFileModel.getIdentity();
        
        const newFileModelFields = newFileModel.getDefinition().fields;
        const oldObjectTypeModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::ObjectType');
        const oldFileModel = services.createBaseRuntimeModel(sourceSchema, 'CORE::File');
        
        let count = oldObjectTypeModel.find({
            count: true,
            aliases: [{
                name: 'OT',
                collection: oldObjectTypeModel.getIdentity(),
                isPrimary: true
            }, {
                name: 'F',
                collection: oldFileModel.getIdentity()
            }],
            join: [{
                type: 'inner',
                alias: 'F',
                on: [{
                    alias: 'F',
                    field: 'ID_OBJECT_TYPE',
                    operator: '$eq',
                    value: {
                        alias: 'OT',
                        field: 'ID'
                    }
                }]
            }]
        });
        
        if(!lodash.isEmpty(count.errors)) {
            return count;
        }
        if(lodash.isEmpty(count.results)) {
            return { errors: ['No ObjectType values'] };
        }
        
        count = count.results[0].tableCount;
        
        let pages = Math.ceil(count / 200);
        let temp;
        let objectTypeMap = {};
        for(let i = 0; i <= pages; i++) {
            temp = oldObjectTypeModel.find({
                aliases: [{
                    name: 'OT',
                    isPrimary: true,
                    collection: oldObjectTypeModel.getIdentity()
                }, {
                    name: 'F',
                    collection: oldFileModel.getIdentity()
                }, {
                  name: 'PF',
                  collection: oldFileModel.getIdentity(),
                  query: {
                        select: [{
                            field: 'ID'
                        }, {
                            field: 'ID_OBJECT_TYPE'
                        }, {
                            field: 'ID_OBJECT'
                        }],
                        paginate: {
                            limit: 200,
                            offset: i * 200
                        },
                        orderBy: [{
                            field: 'ID'
                        }]
                  }
                }],
                join: [{
                    type: 'inner',
                    alias: 'PF',
                    map: 'files',
                    on: [{
                        alias: 'PF',
                        field: 'ID_OBJECT_TYPE',
                        operator: '$eq',
                        value: {
                            alias: 'OT',
                            field: 'ID'
                        }
                    }]
                }],
                orderBy: [{
                    alias: 'OT',
                    field: 'ID'
                }]
            });
            
            if (!lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = lodash.reduce(temp.results, function(prev, row) {
                lodash.forEach(row.files, function(file) {
                    prev[file.ID] = {
                        name: row.NAME,
                        objectId: file.ID_OBJECT
                    };
                });
                
                return prev;
            }, {});
            
            lodash.assign(objectTypeMap, temp);
        }
        
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'creationUser': {
                source: 'CREATION.ID_USER'
            },
            'modificationUser': {
                source: 'MODIFICATION.ID_USER'
            },
            'creationDate': {
                source: 'CREATION.DATE'
            },
            'modificationDate': {
                source: 'MODIFICATION.DATE'
            },
            'folderId': {
                source: 'ID_FOLDER'
            },
            'status': {
                source: 'STATUS'
            }
        };
        
        let fieldKeys = lodash.keys(newFileModelFields);
        
        lodash.map(fieldKeys, function(key) {
            let columnName = newFileModelFields[key].columnName;
            if (lodash.keys(mapping).indexOf(columnName) === -1) {
                mapping[columnName] = {
                  source: 'ID',
                  preProcessFn: function(param) {
                    let nameArray = objectTypeMap[param].name.split('::');
                    let name = nameArray[0].toUpperCase() + nameArray[1].toUpperCase();
                    let fk = columnName.split('_')[0] + columnName.split('_')[1];
                
                    return fk === name ? objectTypeMap[param].objectId : null;
                  }
                };
            }
        });

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newFileModel
            });
        
        return increaseSequence(targetSchema, targetIdentity, 'ID', response);
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importFileShareData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::FileShare';
        const targetSchema = newFileShareModel.getSchema();
        const targetIdentity = newFileShareModel.getIdentity();
        
        const mapping = {
            'fileId': {
                source: 'ID_FILE'
            },
            'userId': {
                source: 'ID_USER'
            },
            'groupId': {
                source: 'ID_GROUP'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newFileShareModel
            });
    
        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};

this.importFilesFavData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'CORE::FileFavs';
        const targetSchema = newFileFavoriteModel.getSchema();
        const targetIdentity = newFileFavoriteModel.getIdentity();
        
        const mapping = {
            'fileId': {
                source: 'ID_FILE'
            },
            'userId': {
                source: 'ID_USER'
            }
        };

        let response = services
            .importData(sourceSchema, sourceIdentity, targetSchema, targetIdentity, mapping, {
                importDataOnlyIfEmpty: importDataOnlyIfEmpty,
                targetModel: newFileFavoriteModel
            });
    
        return response;
    } catch(e) {
		return {
            errors: $.parseError(e)
        };
    }
};