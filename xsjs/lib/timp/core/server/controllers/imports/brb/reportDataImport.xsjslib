$.import('timp.brb.server.models.tables.report', 'report');
const reportModel = $.timp.brb.server.models.tables.report.report.reportModel;
const commentModel = $.timp.brb.server.models.tables.report.report.commentModel;
const reportTaxModel = $.timp.brb.server.models.tables.report.report.reportTaxModel;

$.import('timp.brb.server.models.tables.variant', 'variant');
const variantModel = $.timp.brb.server.models.tables.variant.variant.variantModel;
const variantAnalyzedModel = $.timp.brb.server.models.tables.variant.variant.variantAnalyzedModel;

$.import('timp.core.server.models.tables', 'fileSystem');
const fileModel = $.timp.core.server.models.tables.fileSystem.fileModel;

$.import('timp.core.server.libraries.internal', 'util');
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config.application.schema.slice(1, -1);

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

function increaseSequence(targetSchema, targetIdentity, field, response) {
	if (!$.lodash.isEmpty(response.errors)) {
		return response;
	}

	let model = $.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
	let newSequence = model.find({
		ignoreCache: true,
		select: [{
			calculation: {
				operator: '+',
				params: [{
						'function': {
							name: 'IFNULL',
							params: [{
									aggregation: {
										type: 'MAX',
										param: {
											field: field
										}
									}
                                },
        							0]
						}
		        },
					1]
			},
			as: 'maxId'
        }]
	});

	if (!$.lodash.isEmpty(newSequence.errors)) {
		return newSequence;
	}

	newSequence = newSequence.results[0].maxId;
	return services.alterSequence(targetSchema, targetIdentity + '::' + field,
		newSequence);
}

this.importReportData = function() {
	let retVal = {
		errors: [],
		results: {}
	};
	let coreFileInstances = [];
	let reportTaxInstances = [];
	const structureModel = $.createBaseRuntimeModel(schema, 'ATR::Structure', false);
	const oldReportModel = $.createBaseRuntimeModel(schema, 'BRB::Report', false);
	let response = structureModel.find({
		aliases: [{
			collection: structureModel.getIdentity(),
			isPrimary: true,
			name: 'Structure'
		    }, {
			collection: oldReportModel.getIdentity(),
			name: 'Report'
		    }],
		select: [{
			field: 'ID',
			alias: 'Structure'
		    }, {
			field: 'HASH',
			alias: 'Structure'
		    }],
		join: [{
			type: 'inner',
			alias: 'Report',
			on: [{
				alias: 'Report',
				field: 'structureID',
				operator: '$eq',
				value: {
					alias: 'Structure',
					field: 'ID'
				}
		        }]
		    }]
	});
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	let structureIdHashMap = $.lodash.reduce(response.results, function(map, result) {
		map[result.ID] = result.HASH;
		return map;
	}, {});
	const regex = new RegExp('^/timp/core/server/endpoint.xsjs/images/get/(\\d+)/$');
	let importMap = {
		'id': {
			source: 'ID',
			ignoreSequence: true
		},
		'json': {
			source: 'JSON'
		},
		'name': {
			source: 'reportName',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'description': {
			source: 'reportDescription',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'structureHash': {
			source: 'structureID',
			preProcessFn: function(value) {
				return $.lodash.isNil(structureIdHashMap[value]) ? null : structureIdHashMap[value];
			}
		},
		'structureGroupId': {
			source: 'structureGroupID'
		},
		'printTitle': {
			source: 'printTitle',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'printDescription': {
			source: 'printDescription',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'imageId': {
			source: 'imageURL',
			preProcessFn: function(value) {
				let id = $.lodash.isNil(value) ? null : value.match(regex);
				if (!$.lodash.isNil(id)) {
					id = Number(id.pop());
				}
				return $.lodash.isNumber(id) ? id : null;
			}
		}
	};
	let options;
	let columnMapForStatus = {
		'Deleted': {
			column: 'isDeleted',
			value: 1
		},
		'Trash': {
			column: 'wasRemoved',
			value: 1
		},
		'Standard': {
			column: 'isPreDefined',
			value: 1
		},
		'Active': {
			column: 'isPublic',
			value: 0
		},
		'Public': {
			column: 'isPublic',
			value: 1
		}
	};
	let excludedIds = [];
	let filters = [];
	$.lodash.forEach(columnMapForStatus, function(metadata, status) {
		filters = [{
			field: metadata.column,
			operator: '$eq',
			value: metadata.value
	        }];
		if (!$.lodash.isEmpty(excludedIds)) {
			filters.push({
				field: 'ID',
				operator: '$notIn',
				value: excludedIds
			});
		}
		options = {
			filters: filters,
			sourceModel: oldReportModel,
			targetModel: reportModel
		};
		response = services.importData(schema, 'BRB::Report', schema, 'BRB::REPORT', importMap, options);
		if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, response.errors);
		} else {
			retVal.results[status] = response.results;
			response = oldReportModel.find({
				select: [{
					field: 'ID'
		        }, {
					field: 'creation.User'
		        }, {
					field: 'creation.Date'
		        }, {
					field: 'modification.User'
		        }, {
					field: 'modification.Date'
		        }, {
					field: 'idTax'
		        }],
				where: filters
			});
			if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
				retVal.errors = $.lodash.concat(retVal.errors, response.errors);
			} else if ($.lodash.isArray(response.results) && !$.lodash.isEmpty(response.results)) {
				$.lodash.forEach(response.results, function(instance) {
					excludedIds.push(instance.ID);
					coreFileInstances.push({
						folderId: -1,
						brbReportId: instance.ID,
						status: status,
						creationUser: instance['creation.User'],
						creationDate: instance['creation.Date'],
						modificationUser: instance['modification.User'],
						modificationDate: instance['modification.Date']
					});
					if ($.lodash.isPlainObject(instance.idTax) && !$.lodash.isNil(instance.idTax) && $.lodash.isArray(instance.idTax.idTax) && !$.lodash
						.isEmpty(instance.idTax.idTax)) {
						$.lodash.forEach(instance.idTax.idTax, function(tax) {
							reportTaxInstances.push({
								reportId: instance.ID,
								taxId: tax
							});
						});
					}
				});
			}
		}
	});
	if (!$.lodash.isEmpty(coreFileInstances)) {
		response = fileModel.batchCreate(coreFileInstances, {
		    ignoreSQLStatementDefaults: ['creationUser', 'modificationUser']
		});
		if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, response.errors);
		}
	}
	if (!$.lodash.isEmpty(reportTaxInstances)) {
		response = reportTaxModel.batchCreate(reportTaxInstances);
		if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, response.errors);
		}
	}
	response = increaseSequence(reportModel.getSchema(), reportModel.getIdentity(), 'ID', retVal);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.results.sequence = response.results;
	return retVal;
};

this.importCommentData = function() {
	let retVal = {
		errors: [],
		results: {}
	};
	const oldCommentModel = $.createBaseRuntimeModel(schema, 'BRB::Comments', false);
	let importMap = {
		'id': {
			source: 'ID',
			ignoreSequence: true
		},
		'comment': {
			source: 'COMMENT'
		},
		'reportId': {
			source: 'ID_REPORT'
		},
		'lineId': {
			source: 'ID_LINE'
		},
		'columnId': {
			source: 'ID_COLUMN'
		},
		'isPublic': {
			source: 'ISPUBLIC',
			preProcessFn: function(value) {
				return value === 1;
			}
		},
		'isDeleted': {
			source: 'ISDELETED',
			preProcessFn: function(value) {
				return value === 1;
			}
		},
		'creationUser': {
			source: 'CREATION_USER'
		},
		'creationDate': {
			source: 'CREATION_DATE'
		}
	};
	let options = {
		sourceModel: oldCommentModel,
		targetModel: commentModel
	};
	let response = services.importData(schema, 'BRB::Comments', schema, 'BRB::REPORT_COMMENT', importMap, options);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.reportComment = response.results;
	response = increaseSequence(commentModel.getSchema(), commentModel.getIdentity(), 'ID', retVal);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.results.sequence = response.results;
	return retVal;
};

this.syncBrbTables = function() {
	let retVal = {};
	retVal.r1 = reportModel.sync();
	retVal.r2 = commentModel.sync();
	retVal.r3 = reportTaxModel.sync();
	retVal.r4 = variantModel.sync();
	retVal.r5 = variantAnalyzedModel.sync();
	return retVal;
	// return commentModel.getDefinition();
};