$.import('timp.brb.server.models.tables.report', 'report');
const reportModel = $.timp.brb.server.models.tables.report.report.reportModel;
const commentModel = $.timp.brb.server.models.tables.report.report.commentModel;
const reportTaxModel = $.timp.brb.server.models.tables.report.report.reportTaxModel;

$.import('timp.brb.server.models.tables.variant', 'variant');
const variantModel = $.timp.brb.server.models.tables.variant.variant.variantModel;
const variantAnalyzedModel = $.timp.brb.server.models.tables.variant.variant.variantAnalyzedModel;

$.import('timp.core.server.models.tables', 'fileSystem');
const fileModel = $.timp.core.server.models.tables.fileSystem.fileModel;

$.import('timp.core.server.libraries.internal', 'util');
$.import('timp.core.server', 'config');
const schema = $.timp.core.server.config.application.schema.slice(1, -1);

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

function increaseSequence(targetSchema, targetIdentity, field, response) {
	if (!$.lodash.isEmpty(response.errors)) {
		return response;
	}

	let model = $.createBaseRuntimeModel(targetSchema, targetIdentity, false, true);
	let newSequence = model.find({
		ignoreCache: true,
		select: [{
			calculation: {
				operator: '+',
				params: [{
						'function': {
							name: 'IFNULL',
							params: [{
									aggregation: {
										type: 'MAX',
										param: {
											field: field
										}
									}
                                },
        							0]
						}
		        },
					1]
			},
			as: 'maxId'
        }]
	});

	if (!$.lodash.isEmpty(newSequence.errors)) {
		return newSequence;
	}

	newSequence = newSequence.results[0].maxId;
	return services.alterSequence(targetSchema, targetIdentity + '::' + field,
		newSequence);
}

this.importVariantData = function() {
	let retVal = {
		errors: [],
		results: {}
	};
	let coreFileInstances = [];
	const oldVariantModel = $.createBaseRuntimeModel(schema, 'BRB::Variants', false);
	let importMap = {
		'id': {
			source: 'ID',
			ignoreSequence: true
		},
		'json': {
			source: 'JSON'
		},
		'name': {
			source: 'VARIANT_NAME',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'description': {
			source: 'VARIANT_DESCRIPTION',
			preProcessFn: function(value) {
				return (value === 'undefined' || $.lodash.isNil(value)) ? null : value;
			}
		},
		'reportId': {
			source: 'ID_REPORT'
		}
	};
	let options;
	let columnMapForStatus = {
		'Deleted': {
			column: 'ISDELETED',
			value: 1
		},
		'Active': {
			column: 'isPublic',
			value: 0
		},
		'Public': {
			column: 'isPublic',
			value: 1
		}
	};
	let excludedIds = [];
	let filters = [];
	let response;
	$.lodash.forEach(columnMapForStatus, function(metadata, status) {
		filters = [{
			field: metadata.column,
			operator: '$eq',
			value: metadata.value
	        }];
		if (!$.lodash.isEmpty(excludedIds)) {
			filters.push({
				field: 'ID',
				operator: '$notIn',
				value: excludedIds
			});
		}
		options = {
			filters: filters,
			sourceModel: oldVariantModel,
			targetModel: variantModel
		};
		response = services.importData(schema, 'BRB::Variants', schema, 'BRB::VARIANT', importMap, options);
		if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, response.errors);
		} else {
			retVal.results[status] = response.results;
			response = oldVariantModel.find({
				select: [{
					field: 'ID'
		        }, {
					field: 'CREATION_USER'
		        }, {
					field: 'CREATION_DATE'
		        }, {
					field: 'MODIFICATION_USER'
		        }, {
					field: 'MODIFICATION_DATE'
		        }],
				where: filters
			});
			if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
				retVal.errors = $.lodash.concat(retVal.errors, response.errors);
			} else if ($.lodash.isArray(response.results) && !$.lodash.isEmpty(response.results)) {
				$.lodash.forEach(response.results, function(instance) {
					excludedIds.push(instance.ID);
					coreFileInstances.push({
						folderId: -1,
						brbVariantId: instance.ID,
						status: status,
						creationUser: instance.CREATION_USER,
						creationDate: instance.CREATION_DATE,
						modificationUser: instance.MODIFICATION_USER,
						modificationDate: instance.MODIFICATION_DATE
					});
				});
			}
		}
	});
	if (!$.lodash.isEmpty(coreFileInstances)) {
		response = fileModel.batchCreate(coreFileInstances, {
		    ignoreSQLStatementDefaults: ['creationUser', 'modificationUser']
		});
		if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, response.errors);
		}
	}
	response = increaseSequence(variantModel.getSchema(), variantModel.getIdentity(), 'ID', retVal);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.results.sequence = response.results;
	return retVal;
};

this.importVariantAnalyzedData = function() {
	let retVal = {
		errors: [],
		results: {}
	};
	const oldVariantAnalyzedModel = $.createBaseRuntimeModel(schema, 'BRB::StatusNFCorrection', false);
	let importMap = {
		'id': {
			source: 'ID',
			ignoreSequence: true
		},
		'variantId': {
			source: 'ID_VARIANT'
		},
		'lineId': {
			source: 'ID_LINE'
		},
		'status': {
			source: 'STATUS'
		}
	};
	let options = {
		sourceModel: oldVariantAnalyzedModel,
		targetModel: variantAnalyzedModel
	};
	let response = services.importData(schema, 'BRB::StatusNFCorrection', schema, 'BRB::VARIANT_ANALYZED', importMap, options);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.variantAnalyzed = response.results;
	response = increaseSequence(variantAnalyzedModel.getSchema(), variantAnalyzedModel.getIdentity(), 'ID', retVal);
	if ($.lodash.isArray(response.errors) && !$.lodash.isEmpty(response.errors)) {
		retVal.errors = $.lodash.concat(retVal.errors, response.errors);
	}
	retVal.results.sequence = response.results;
	return retVal;
};