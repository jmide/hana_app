$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.core.server.models.tables', 'configuration');
const newConfigurationModel = $.timp.core.server.models.tables.configuration.configurationModel;
const newLicenseModel = $.timp.core.server.models.tables.configuration.licenseModel;

$.import('timp.core.server.controllers.public', 'configuration');
const configurationCtrl = $.timp.core.server.controllers.public.configuration;

const sourceSchema = $.schema.slice(1, -1);

this.importConfigurationData = function (importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        const sourceIdentity = 'CORE::Config';
        const oldConfigModel = $.createBaseRuntimeModel(sourceSchema, sourceIdentity);
        const atrSettingsModel = $.createBaseRuntimeModel(sourceSchema, 'ATR::Settings');
        let errors = [];
        let insertResponse = { skiped: true };
        const oldComponentsModel = $.createBaseRuntimeModel(sourceSchema, 'CORE::Components');
        importDataOnlyIfEmpty = importDataOnlyIfEmpty && oldComponentsModel && $.lodash.isEmpty(oldComponentsModel.errors);
        let components = oldComponentsModel.find({
            select: [{
                field: 'ID'
            }, {
                field: 'NAME'
            }]
        }).results;
        let idNameMap = lodash.reduce(components, function (prev, value) {
            prev[value.NAME] = value.ID;
            return prev;
        }, {});
        let eccYear = {
            key: 'ECC::YEAR',
            componentId: idNameMap.CORE
        };
        let insertedRows = [{
            key: 'ECC::LANG',
            componentId: idNameMap.CORE,
            value: 'P'
        }, {
            key: 'TIMP::TDFIntegration',
            componentId: idNameMap.CORE,
            value: 'true'
        }, {
            key: 'LINES_15_COLUMNS',
            componentId: idNameMap.BRB || idNameMap.CORE,
            value: '30000'
        }, {
            key: 'TABLE::SEPARATOR',
            componentId: idNameMap.BRE || idNameMap.CORE,
            value: ';'
        }];
        let insert = true;
        if (importDataOnlyIfEmpty) {
            let where = $.lodash.map(insertedRows, function (item) {
                return {
                    field: 'key',
                    operator: '$eq',
                    value: item.key
                };
            });
            let count = newConfigurationModel.find({
                count: true,
                where: [where]
            });
            insert = lodash.isEmpty(count.errors) && !lodash.isEmpty(count.results);
            count = count.results[0].tableCount;
            insert = insert && (count === 0);
        }
        if (insert) {
            let parametersMap = {
                'ECC::LANGUAGE': 'ECC::LANG',
                'ECC::CLIENT': 'ECC::CLIENT',
                'sessionTime': 'TIMP::SessionTime',
                'tdfSchemaName': 'TDF::SCHEMA',
                'instalationType': 'TIMP::TDFIntegration',
                'LINES_15_COLUMNS': 'LINES_15_COLUMNS',
                'TABLE::SEPARATOR': 'TABLE::SEPARATOR'
            };
            let oldConfigurations = oldConfigModel.find({
                distinct: true,
                where: [{
                    field: 'PARAMETER',
                    operator: '$in',
                    value: ['ECC::CLIENT', 'sessionTime', 'tdfSchemaName', 'ECC::HOST', 'ECC::PORT']
                }]
            });
            if (!$.lodash.isNil(oldConfigurations) && !$.lodash.isEmpty(oldConfigurations.errors)) {
                errors = $.lodash.concat(errors, oldConfigurations.errors);
                oldConfigurations = [];
            } else if ($.lodash.isNil(oldConfigurations)) {
                oldConfigurations = [];
            } else {
                oldConfigurations = oldConfigurations.results;
            }
            let addedToInsert = [];
            $.lodash.forEach(oldConfigurations, function (row) {
                if (addedToInsert.indexOf(row.PARAMETER) === -1) {
                    insertedRows.push({
                        key: parametersMap[row.PARAMETER] || row.PARAMETER,
                        componentId: idNameMap.CORE,
                        value: row.VALUE
                    });
                    addedToInsert.push(row.PARAMETER);
                }
            });
            let countECCYear = newConfigurationModel.find({
                count: true,
                where: [{
                    field: 'key',
                    operator: '$eq',
                    value: eccYear.key
                }]
            });
            if (lodash.isEmpty(countECCYear.errors) && !lodash.isEmpty(countECCYear.results)) {
                countECCYear = countECCYear.results[0].tableCount;
                if (countECCYear === 0) {
                    let atrYear = atrSettingsModel.find({
                        select: [{
                            aggregation: {
                                type: 'MIN',
                                param: {
                                    field: 'YEAR'
                                }
                            },
                            as: 'year'
                        }]
                    });
                    if (!$.lodash.isNil(atrYear) && !$.lodash.isEmpty(atrYear.errors)) {
                        errors = $.lodash.concat(errors, atrYear.errors);
                        atrYear = null;
                    } else if (!$.lodash.isNil(atrYear)) {
                        atrYear = atrYear.results[0].year;
                    }
                    if (!$.lodash.isNil(atrYear)) {
                        eccYear.value = atrYear;
                        insertedRows.push(eccYear);
                        configurationCtrl.createFullDates(atrYear);
                    }
                }
            }
            insertResponse = newConfigurationModel.batchCreate(insertedRows);
            errors = $.lodash.concat(errors, insertResponse.errors);
            delete insertResponse.errors;
        }
        return {
            errors: errors,
            response: insertResponse
        };
    } catch (e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importLicenseData = function (importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        const sourceIdentity = 'CORE::Licenses';
        const targetSchema = newLicenseModel.getSchema();
        const targetIdentity = newLicenseModel.getIdentity();
        const mapping = {
            'id': {
                source: 'ID',
                ignoreSequence: true
            },
            'license': {
                source: 'LICENSE'
            },
            'availableUsers': {
                source: 'USERS'
            },
            'expiration': {
                source: 'EXPIRATION'
            },
            'isActive': {
                useTranslate: true,
                source: 'EXPIRATION',
                preProcessFn: function (param) {
                    let now = new Date().getTime();
                    let expiration = new Date(param).getTime();
                    return now < expiration;
                }
            }
        };
        let response = services.importData(
            sourceSchema, 
            sourceIdentity, 
            targetSchema, 
            targetIdentity, 
            mapping, 
            { importDataOnlyIfEmpty: importDataOnlyIfEmpty, targetModel: newLicenseModel }
        );
        return response;
    } catch (e) {
        return {
            errors: $.parseError(e)
        };
    }
};