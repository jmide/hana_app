$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.bfb.server.models.models.tables.layout', 'layout');
const newLayoutModel = $.timp.bfb.server.models.models.tables.layout.layout.layoutModel;

const schema = $.schema.slice(1,-1);

function increaseSequence(targetSchema, targetIdentity, field, response) {
    if(!$.lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = $.createBaseRuntimeModel(targetSchema, targetIdentity);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    field: 'ID'
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!$.lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!$.lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
}

this.importLayoutData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'BFB::Layout';
        const targetIdentity = newLayoutModel.getIdentity();
        
        const layoutStructureModel = $.createBaseRuntimeModel(schema, 'BFB::LayoutXStructure');
        const structureModel = $.createBaseRuntimeModel(schema, 'ATR::Structure');
        
        let count = layoutStructureModel.find({ count: true });
        
        if(!$.lodash.isEmpty(count.errors)) {
            return count;
        }
        if($.lodash.isEmpty(count.results)) {
            return { errors: ['No Layout values'] };
        }
        
        count = count.results[0].tableCount;
        let size = 5;
        let pages = Math.ceil(count / size);
        let temp;
        let layoutStructuresMap = {};
        
        for(let i = 0; i <= pages; i++) {
            temp = layoutStructureModel.find({
                aliases: [{
                    name: 'LS',
                    collection: layoutStructureModel.getIdentity(),
                    isPrimary: true
                }, {
                    name: 'S',
                    collection: structureModel.getIdentity()
                }, {
                    name: 'SLS',
                    collection: layoutStructureModel.getIdentity(),
                    query: {
                        select: [{
                            field: 'ID'
                        }],
                        paginate: {
                            limit: size,
                            offset: i * size
                        }
                    }
                }],
                join: [{
                    type: 'inner',
                    alias: 'SLS',
                    on: [{
                        alias: 'SLS',
                        field: 'ID',
                        operator: '$eq',
                        value: {
                            alias: 'LS',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'S',
                    type: 'inner',
                    map: 'structure',
                    on: [{
                        alias: 'S',
                        field: 'ID',
                        operator: '$eq',
                        value: {
                            alias: 'LS',
                            field: 'ID_STRUCTURE'
                        }
                    }]
                }]
            });
            
            if (!$.lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = $.lodash.reduce(temp.results, function(prev, row) {
                prev[row.ID_LAYOUT] = {
                    structureGroup: row.ID_STRUCTURE_GROUP,
                    hash: !$.lodash.isNil(row.structure) && !$.lodash.isNil(row.structure[0]) ? row.structure[0].HASH : -1
                };
                
                return prev;
            }, {});
            
            $.lodash.assign(layoutStructuresMap, temp);
        }
        
        let mapping = {
            'ID': {
                source: 'ID',
                ignoreSequence: true
            },
            'NAME': {
                source: 'NAME'
            },
            'DESCRIPTION': {
                source: 'DESCRIPTION'
            },
            'EFFECTIVE_DATE_FROM': {
                source: 'EFFECTIVE_DATE_FROM'
            },
            'EFFECTIVE_DATE_TO': {
                source: 'EFFECTIVE_DATE_TO'
            },
            'FORM_TYPE_ID': {
                source: 'ID_FORM_TYPE'
            },
            'JSON': {
                source: 'JSON',
                preProcessFn: function(param) {
                    return param || {};
                }
            },
            'STRUCTURE_HASH': {
                source: 'ID',
                preProcessFn: function(param) {
                    return layoutStructuresMap[param].hash;
                }
            },
            'STRUCTURE_GROUP_ID'  : {
                source: 'ID',
                preProcessFn: function(param) {
                    return layoutStructuresMap[param].structureGroup;
                }
            }
        };
        
        let response = services
            .importData(schema, sourceIdentity, schema, targetIdentity, mapping, { importDataOnlyIfEmpty: importDataOnlyIfEmpty });
        
        return increaseSequence(schema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};