$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.bfb.server.models.models.tables.setting', 'setting');
const newSettingModel = $.timp.bfb.server.models.models.tables.setting.setting.settingModel;
$.import('timp.bfb.server.models.models.tables.setting', 'settingEEFI');
const newSettingEEFIModel = $.timp.bfb.server.models.models.tables.setting.settingEEFI.settingEEFIModel;

const schema = $.schema.slice(1,-1);

function increaseSequence(targetSchema, targetIdentity, field, response) {
    if(!$.lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = $.createBaseRuntimeModel(targetSchema, targetIdentity);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    field: 'ID'
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!$.lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!$.lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
}

this.importSettingsData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'BFB::Setting';
        const targetIdentity = newSettingModel.getIdentity();
        
        let mapping = {
            'ID': {
                source: 'ID',
                ignoreSequence: true
            },
            'NAME': {
                source: 'NAME'
            },
            'DESCRIPTION': {
                source: 'DESCRIPTION'
            },
            'EFFECTIVE_DATE_FROM': {
                source: 'VALID_FROM'
            },
            'EFFECTIVE_DATE_TO': {
                source: 'VALID_TO'
            },
            'LAYOUT_ID': {
                source: 'ID_LAYOUT'
            }
        };
        
        let response = services
            .importData(schema, sourceIdentity, schema, targetIdentity, mapping, { importDataOnlyIfEmpty: importDataOnlyIfEmpty });
        
        return increaseSequence(schema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};

this.importSettingsEEFIData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        const sourceIdentity = 'BFB::SettingXEEFI';
        const targetIdentity = newSettingEEFIModel.getIdentity();
        
        let mapping = {
            'SETTING_ID': {
                source: 'ID_SETTING'
            },
            'COMPANY_ID': {
                source: 'ID_COMPANY'
            },
            'STATE_ID': {
                source: 'UF'
            },
            'BRANCH_ID': {
                source: 'ID_BRANCH'
            },
            'TAX_ID': {
                source: 'ID_TAX'
            }
        };
        
        let response = services
            .importData(schema, sourceIdentity, schema, targetIdentity, mapping, { importDataOnlyIfEmpty: importDataOnlyIfEmpty });
        
        return response;
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};