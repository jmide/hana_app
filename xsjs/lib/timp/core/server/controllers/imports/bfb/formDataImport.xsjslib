$.import('timp.core.server.libraries.internal', 'util');
const services = $.timp.core.server.libraries.internal.util.services;

$.import('timp.bfb.server.models.models.tables.form', 'form');
const newFormModel = $.timp.bfb.server.models.models.tables.form.form.formModel;

const schema = $.schema.slice(1,-1);

function increaseSequence(targetSchema, targetIdentity, field, response) {
    if(!$.lodash.isEmpty(response.errors)) {
        return response;
    }
    
    let model = $.createBaseRuntimeModel(targetSchema, targetIdentity);
    let newSequence = model.find({
        select: [{
            aggregation: {
                type: 'MAX',
                param: {
                    field: 'ID'
                }
            },
            as: 'maxId'
        }]
    });
    
    if (!$.lodash.isEmpty(newSequence.errors)) {
        return newSequence;
    }
        
    newSequence = newSequence.results[0].maxId + 1;

    let alterSequenceResponse = services.alterSequence(targetSchema, targetIdentity + '::' + field, newSequence);
    
    if (!$.lodash.isEmpty(alterSequenceResponse.errors)) {
        return alterSequenceResponse;
    }
    
    return response;
}

this.importFormData = function(importDataOnlyIfEmpty) {
    try {
        importDataOnlyIfEmpty = $.lodash.isNil(importDataOnlyIfEmpty) || importDataOnlyIfEmpty;
        
        if (importDataOnlyIfEmpty) {
            let newLabelcount = newFormModel.find({ count: true });   
            
            if(!$.lodash.isEmpty(newLabelcount.errors)) {
                return newLabelcount;
            }
            if($.lodash.isEmpty(newLabelcount.results)) {
                return { errors: ['No Target Label values']};
            }
            
            if(newLabelcount.results[0].tableCount > 0) {
                return {
                    results: {
                        skipped: true   
                    }
                };
            }
        }
        
        const sourceIdentity = 'BFB::Form';
        const targetIdentity = newFormModel.getIdentity();
            
        const oldFormModel = $.createBaseRuntimeModel(schema, sourceIdentity);
        const superiodModel = $.createBaseRuntimeModel(schema, 'TFP::FiscalSubPeriod');
        
        let count = oldFormModel.find({ count: true });
        
        if(!$.lodash.isEmpty(count.errors)) {
            return count;
        }
        if($.lodash.isEmpty(count.results)) {
            return { errors: ['No Forms values'] };
        }
        
        count = count.results[0].tableCount;
        let size = 5;
        let pages = Math.ceil(count / size);
        let temp;
        let bfbForms = [];
        
        for(let i = 0; i <= pages; i++) {
            temp = oldFormModel.find({
                aliases: [{
                    name: 'F',
                    collection: oldFormModel.getIdentity(),
                    isPrimary: true
                }, {
                    name: 'SP',
                    collection: superiodModel.getIdentity()
                }, {
                    name: 'SF',
                    collection: oldFormModel.getIdentity(),
                    query: {
                        select: [{
                            field: 'ID'
                        }],
                        paginate: {
                            limit: size,
                            offset: i * size
                        }
                    }
                }],
                join: [{
                    type: 'inner',
                    alias: 'SF',
                    on: [{
                        alias: 'SF',
                        field: 'ID',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'ID'
                        }
                    }]
                }, {
                    alias: 'SP',
                    type: 'left',
                    map: 'subperiod',
                    on: [{
                        alias: 'SP',
                        field: 'ID_COMPANY',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'ID_COMPANY'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_BRANCH',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'ID_BRANCH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'ID_TAX',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'ID_TAX'
                        }
                    }, {
                        alias: 'SP',
                        field: 'MONTH',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'MONTH'
                        }
                    }, {
                        alias: 'SP',
                        field: 'YEAR',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'YEAR'
                        }
                    }, {
                        alias: 'SP',
                        field: 'SUB_PERIOD',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'SUBPERIOD'
                        }
                    }, {
                        alias: 'SP',
                        field: 'UF',
                        operator: '$eq',
                        value: {
                            alias: 'F',
                            field: 'UF'
                        }
                    }]
                }]
            });
            
            if (!$.lodash.isEmpty(temp.erros)) {
                return temp;
            }
            
            temp = $.lodash.reduce(temp.results, function(prev, row) {
                let results = [];
                if ($.lodash.isEmpty(row.subperiod)) {
                    results.push({
                        id: row.ID,
                        settingId: row.ID_SETTING,
                        name: row.NAME,
                        description: row.DESCRIPTION,
                        form: row.FORM,
                        json: row.JSON,
                        centralization: row.CENTRALIZATION === '1',
                        appliedCentralization: row.APPLIED_CENTRALIZATION === '1'
                    });
                } else {
                    $.lodash.forEach(row.subperiod, function(subperiod) {
                        results.push({
                            id: row.ID,
                            settingId: row.ID_SETTING,
                            name: row.NAME,
                            description: row.DESCRIPTION,
                            form: row.FORM,
                            json: row.JSON,
                            centralization: row.CENTRALIZATION === '1',
                            appliedCentralization: row.APPLIED_CENTRALIZATION === '1',
                            subperiodId: subperiod ? subperiod.ID : null
                        });
                    });    
                }
                
                return $.lodash.concat(prev, results);
            }, []);
            
            bfbForms = $.lodash.concat(bfbForms, temp);
        }

        let response = newFormModel.importDataOnlyIfEmptyhCreate(bfbForms, { ignoreSequenceFields: ['id'] });
        
        return increaseSequence(schema, targetIdentity, 'ID', response);
    } catch(e) {
        return {
            errors: $.parseError(e)
        };
    }
};