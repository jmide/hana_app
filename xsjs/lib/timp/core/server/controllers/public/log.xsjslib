$.import('timp.core.server.models.tables', 'log');
const logModel = $.timp.core.server.models.tables.log.logModel;
const logDetails = $.timp.core.server.models.tables.log.logDetails;
const logTCMDetails = $.timp.core.server.models.tables.log.logTCMDetails;
$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;
$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;

$.import('timp.core.server.controllers.refactor', 'log');
const logController = $.timp.core.server.controllers.refactor.log;

this.createEvent = logController.createEvent;
this.logInstaller = logController.logInstaller;

this.listLogEvents = function(eventType, page, filters, isExport) {
	var response = {
		data: []
	};
	
	try {
		let parameters = {
			aliases: [{
				collection: logModel.getIdentity(),
				name: 'Log',
				isPrimary: true
		    }, {
				collection: componentModel.getIdentity(),
				name: 'Component'
		    }, {
				collection: userModel.getIdentity(),
				name: 'User'
		    }],
			select: [{
				field: 'id',
				alias: 'Log'
		    }, {
				field: 'name',
				as: 'componentName',
				alias: 'Component'
		    }, {
				field: 'hanaUser',
				alias: 'User'
		    }, {
				field: 'date',
				alias: 'Log'
		    }, {
				field: 'internationalizationCode',
				alias: 'Log',
				as: 'messageCode'
		    }],
			join: [{
				alias: 'Component',
				map: 'ComponentInformation',
				type: 'inner',
				on: [{
					alias: 'Component',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'Log',
						field: 'componentId'
					}
		        }]
		    }, {
				alias: 'User',
				map: 'UserInformation',
				type: 'inner',
				on: [{
					alias: 'User',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'Log',
						field: 'userId'
					}
		        }]
		    }],
			where: [{
				field: 'type',
				alias: 'Log',
				operator: '$eq',
				value: eventType
            }],
			paginate: {
				limit: 15,
				offset: (page - 1) * 15
			},
			orderBy: [{
			    field: 'id',
			    alias: 'Log',
			    type: 'desc'
			}]
		};
		if (filters && $.lodash.has(filters, 'codeDescription')) {
		    let messageCodesText = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::MessageCodeText');
		    let lang = $.request.cookies.get("Content-Language");
		    let CodesRegex = new RegExp('^[(a-zA-Z)]+[0-9]+');
		    parameters.aliases.push({
				collection: messageCodesText.getIdentity(),
				name: 'messageCodeText'
		    });
			parameters.where.push([{
				field: 'TEXT',
				alias: 'messageCodeText',
				operator: '$contains',
				value: filters.codeDescription
			}, {
			    field: 'CODE',
				alias: 'messageCodeText',
				operator: '$like',
				value:  filters.codeDescription && CodesRegex.test(filters.codeDescription) && filters.codeDescription.match(CodesRegex)[0] || filters.codeDescription
			}], {
			    field: 'LANG',
				alias: 'messageCodeText',
				operator: '$eq',
				value: lang
			});
			parameters.join.push({
				alias: 'messageCodeText',
				map: 'messageCodeTextInformation',
				type: 'left',
				on: [{
					alias: 'messageCodeText',
					field: 'CODE',
					operator: '$eq',
					value: {
						alias: 'Log',
						field: 'internationalizationCode'
					}
		        }]
		    });
		}
		if (filters && $.lodash.has(filters, 'hanaUser')) {
			parameters.where.push({
				field: 'id',
				alias: 'User',
				operator: '$in',
				value: filters.hanaUser
			});
		}
		if (filters && $.lodash.has(filters, 'component')) {
			parameters.where.push({
				field: 'name',
				alias: 'Component',
				operator: '$in',
				value: filters.component
			});
		}
		if (filters && $.lodash.has(filters, 'messageCode')) {
			parameters.where.push({
				field: 'internationalizationCode',
				alias: 'Log',
				operator: '$in',
				value: filters.messageCode
			});
		}
		if (filters && $.lodash.has(filters, 'range')) {
			if (!$.lodash.isNil(filters.range.startDate)) {
			    let startDate = new $.moment(filters.range.startDate, 'MM/DD/YYYY');
			    if (!$.lodash.isEqual(startDate.format("DD/MM/YYYY"), 'Invalid date')) {
    				parameters.where.push({
    					field: 'date',
    					alias: 'Log',
    					operator: '$gte',
    					value: startDate.format("YYYY-MM-DD[T]00:00:00.000[Z]")
    				});
			    }
			    parameters.orderBy = [{
                    field: 'date',
                    alias: 'Log',
                    type: 'desc'
			    }];
			}
			if (!$.lodash.isNil(filters.range.endDate)) {
			    let endDate = new $.moment(filters.range.endDate, 'MM/DD/YYYY');
			    if (!$.lodash.isEqual(endDate.format("DD/MM/YYYY"), 'Invalid date')) {
    				parameters.where.push({
    					field: 'date',
    					alias: 'Log',
    					operator: '$lte',
    					value: endDate.format("YYYY-MM-DD[T]23:59:59.999[Z]")
    				});
			    }
			}
		}
		if (filters && $.lodash.has(filters, 'startId')) {
		    parameters.where.push({
				field: 'id',
				alias: 'Log',
				operator: '$gt',
				value: filters.startId
			});
		}
		var logData = {};
		
		//ADT: ONLY FOR EXPORT this has return inside
		if (isExport) {
			delete parameters.paginate;
			//adding the json field and listing
			parameters.select.push({
				field: 'trace',
				alias: 'Log',
				as: 'json'
		    });
			logData = logModel.find(parameters);
			
			//validate errors
            if(!$.lodash.isEmpty(logData.errors)){
                response.data = [];
                return response;
            }
			$.lodash.map(logData.results, function(value) {
				if ($.lodash.isArray(value.UserInformation)) {
					value.hanaUser = value.UserInformation[0].hanaUser;
					delete value.UserInformation;
				}
				if ($.lodash.isArray(value.ComponentInformation)) {
					value.componentName = value.ComponentInformation[0].componentName;
					delete value.ComponentInformation;
				}
				
				return value;
			});
			response.data = logData.results;
			return response;
		}
		logData = logModel.find(parameters);
		if ($.lodash.isEmpty(logData.errors)) {
			$.lodash.map(logData.results, function(value) {
				if ($.lodash.isArray(value.UserInformation)) {
					value.hanaUser = value.UserInformation[0].hanaUser;
					delete value.UserInformation;
				}
				if ($.lodash.isArray(value.ComponentInformation)) {
					value.componentName = value.ComponentInformation[0].componentName;
					delete value.ComponentInformation;
				}
				return value;
			});
			response.data = logData.results;
		} else {
			response.data = [];
		}
		parameters.count = true;
		delete parameters.paginate;
		response.pageCount = logModel.find(parameters);
		response.pageCount = Math.ceil(response.pageCount.results[0].tableCount / 15);
		response.page = page;
	} catch (e) {
		response.errors = [$.parseError(e)];
	}
	return response;
};

this.listLogAdvancedFilters = function(eventType) {
	var response = {
		codes: [],
		users: [],
		components: []
	};
	try {
		var components = logModel.find({
			distinct: true,
			aliases: [{
				collection: logModel.getIdentity(),
				name: 'Log'
            }, {
				collection: componentModel.getIdentity(),
				name: 'Component',
				isPrimary: true
            }],
			select: [{
				field: 'name',
				alias: 'Component'
            }],
			join: [{
				alias: 'Log',
				type: 'inner',
				on: [{
					alias: 'Log',
					operator: '$eq',
					field: 'componentId',
					value: {
						field: 'id',
						alias: 'Component'
					}
                }]
            }],
			where: [{
				field: 'type',
				alias: 'Log',
				operator: '$eq',
				value: eventType
            }],
			orderBy: [{
				field: 'name',
				alias: 'Component',
				type: 'asc'
            }]
		});
		if (!$.lodash.isEmpty(components.results)) {
			components = $.lodash.map(components.results, function(value) {
				return value.name;
			});
			response.components = components;
		}
		var codes = logModel.find({
			distinct: true,
			select: [{
				field: "internationalizationCode"
            }],
			where: [{
				field: 'internationalizationCode',
				operator: '$neq',
				value: null
            }, {
				field: 'type',
				operator: '$eq',
				value: eventType
            }],
			orderBy: [{
				field: 'internationalizationCode',
				type: 'asc'
            }]
		});
		if (!$.lodash.isEmpty(codes.results)) {
			codes = $.lodash.map(codes.results, function(value) {
				return value.internationalizationCode;
			});
			response.codes = codes;
		}
		var users = logModel.find({
			distinct: true,
			aliases: [{
				collection: logModel.getIdentity(),
				name: 'Log'
            }, {
				collection: userModel.getIdentity(),
				name: 'User',
				isPrimary: true
            }],
			select: [{
				field: 'id',
				alias: 'User'
            }, {
				field: 'name',
				alias: 'User'
            }, {
				field: 'lastName',
				alias: 'User'
            }],
			join: [{
				alias: 'Log',
				type: 'inner',
				on: [{
					alias: 'Log',
					field: 'userId',
					operator: '$eq',
					value: {
						field: 'id',
						alias: 'User'
					}
                }]
            }],
			where: [{
				field: 'type',
				alias: 'Log',
				operator: '$eq',
				value: eventType
            }],
			orderBy: [{
				field: 'name',
				alias: 'User',
				type: 'asc'
            }]
		});
		if (!$.lodash.isEmpty(users.results)) {
			users = $.lodash.map(users.results, function(value) {
				value.fullName = value.name;
				if (!$.lodash.isEqual(value.name, value.lastName)) {
					value.fullName = value.name + " " + value.lastName;
				}
				return {
					id: value.id,
					name: value.fullName
				};
			});
			response.users = users;
		}
	} catch (e) {
		response.errors = [$.parseError(e)];
	}
	return response;
};

this.getLogEventDetails = function(id) {
	try {
		var event = logModel.find({
			aliases: [{
				collection: logModel.getIdentity(),
				name: 'Log',
				isPrimary: true
		    }, {
				collection: componentModel.getIdentity(),
				name: 'Component'
		    }, {
				collection: userModel.getIdentity(),
				name: 'User'
		    }],
			select: [{
				field: 'id',
				alias: 'Log'
		    }, {
				field: 'name',
				as: 'componentName',
				alias: 'Component'
		    }, {
				field: 'hanaUser',
				alias: 'User'
		    }, {
				field: 'date',
				alias: 'Log'
		    }, {
				field: 'internationalizationCode',
				alias: 'Log',
				as: 'messageCode'
		    }, {
				field: 'trace',
				alias: 'Log',
				as: 'json'
		    }, {
		        field: 'objectId',
		        alias: 'Log'
		    }],
			join: [{
				alias: 'Component',
				map: 'ComponentInformation',
				type: 'inner',
				on: [{
					alias: 'Component',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'Log',
						field: 'componentId'
					}
		        }]
		    }, {
				alias: 'User',
				map: 'UserInformation',
				type: 'inner',
				on: [{
					alias: 'User',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'Log',
						field: 'userId'
					}
		        }]
		    }],
			where: [{
				field: 'id',
				alias: 'Log',
				operator: '$eq',
				value: id
            }]
		});
		if ($.lodash.isEmpty(event.errors)) {
			$.lodash.map(event.results, function(value) {
				if ($.lodash.isArray(value.UserInformation)) {
					value.hanaUser = value.UserInformation[0].hanaUser;
					delete value.UserInformation;
				}
				if ($.lodash.isArray(value.ComponentInformation)) {
					value.componentName = value.ComponentInformation[0].componentName;
					delete value.ComponentInformation;
				}
				if ($.lodash.has(value, 'json')) {
					value.json = JSON.parse(value.json);
					value.json = JSON.stringify(value.json.data);
				}
				value.logDetails = (logDetails.find({
				    where: [{
        				field: 'idHeader',
        				operator: '$eq',
        				value: value.id
                    }]
				})).results
				value.tcmDetails = (logTCMDetails.find({
				    where: [{
        				field: 'idHeader',
        				operator: '$eq',
        				value: value.id
                    }]
				})).results
				
				return value;
			});
			return event.results;
		}
	} catch (e) {
		return [];
	}
};

this.getLogEventTypes = function(id) {
	try {
		if ($.lodash.isArray(id)) {
			let events = logModel.find({
				select: [{
					field: 'id'
                }, {
					field: 'type'
                }],
				where: [{
					field: 'id',
					operator: '$in',
					value: id
                }]
			});
			let response = {};
			if ($.lodash.isEmpty(events.errors)) {
				$.lodash.map(events.results, function(value) {
					response[value.id] = value.type;
				});
				return response;
			} else {
				return {};
			}
		} else {
			return {};
		}
	} catch (e) {
		return {};
	}
};