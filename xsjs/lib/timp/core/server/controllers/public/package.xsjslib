$.import('timp.core.server.controllers.refactor', 'package');
const packageCtrl = $.timp.core.server.controllers.refactor.package;

this.savePackage = packageCtrl.savePackage;
this.listPackages = packageCtrl.listPackages;
this.getPackagesRequiredInformation = packageCtrl.getRequiredInformation; 
this.deletePackage = packageCtrl.deletePackage;
this.getPackageInformation = packageCtrl.getPackageInformation;
this.packagesAdvancedFilters = packageCtrl.listAdvancedFilters;