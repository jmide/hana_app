$.import('timp.core.server.controllers.refactor', 'user');
const userCtrl = $.timp.core.server.controllers.refactor.user;
$.import('timp.core.server.models.tables', 'user');
const userModel = $.timp.core.server.models.tables.user.userModel;
$.import('timp.core.server.controllers.refactor', 'configuration');
const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
$.import('timp.core.server.models.views', 'userPrivilege');
const orgPrivilegesViewModel = $.timp.core.server.models.views.userPrivilege.orgPrivilegesView;

this.saveUser = userCtrl.saveUser;
this.listUsers = userCtrl.listUsers;
this.getUserInformation = userCtrl.getUserInformation;
this.getUserPrivileges = userCtrl.getUserPrivileges;
this.getUserOrgPrivileges = userCtrl.getUserOrgPrivileges;
this.getUsersRequiredInformation = userCtrl.getRequiredInformation;
this.changeUserStatus = userCtrl.changeUserStatus;
this.userAdvancedFilters = userCtrl.listAdvancedFilters;
this.getOrgPrivileges = userCtrl.getOrgPrivileges;

//Executed by the JOB - user_job
this.automaticCreateUser = userCtrl.automaticCreateUser;

this.getDefaultFieldsData = function(elements) {
	try {
		let options = {
			select: [{
				"field": "name"
        	}, {
				"field": "lastName"
            }]
		};
		if ($.lodash.isArray(elements) && !$.lodash.isEmpty(elements)) {
			let users = [];
			var usersData = {};

			$.lodash.forEach(elements, function(elem) {
				if (elem.creationUser) {
					users.push(elem.creationUser);
				}
				if (elem.modificationUser) {
					users.push(elem.modificationUser);
				}
			});

			if (!$.lodash.isEmpty(users)) {
				if ($.lodash.isIntArray(users)) {
					options.select.push({
						"field": "id"
					});
					options.where = [{
						"field": "id",
						"operator": "$in",
						"value": $.lodash.uniq(users)
    	            }];

					userModel.find(options).results.map(function(elem) {
						usersData[elem.id] = elem;
					});

				} else if ($.lodash.isStringArray(users)) {
					options.select.push({
						"field": "hanaUser"
					});
					options.where = [{
						"field": "hanaUser",
						"operator": "$in",
						"value": $.lodash.uniq(users)
    	            }];

					userModel.find(options).results.map(function(elem) {
						usersData[elem.hanaUser] = elem;
					});
				}
			}

			$.lodash.forEach(elements, function(elem) {
				elem.creationUser = usersData[elem.creationUser] ? usersData[elem.creationUser] : {};
				elem.modificationUser = usersData[elem.modificationUser] ? usersData[elem.modificationUser] : {};
			});
		}
	} catch (e) {
		return $.parseError(e);
	}
	return elements;
};

this.getUsersByEEFI = function(eefs) {
	var response = [];
	try {
		if ($.lodash.isArray(eefs) && !$.lodash.isEmpty(eefs)) {
			let typeOfSearch = configurationCtrl.getSystemConfiguration({
				componentName: "CORE",
				keys: ["TIMP::UsersManagement"]
			});
			if ($.lodash.isArray(typeOfSearch) && !$.lodash.isEmpty(typeOfSearch) && $.lodash.isEqual(typeOfSearch[0].value, "basic")) {
				let whereOptions = [];

				$.lodash.forEach(eefs, function(elem) {
					whereOptions.push([{
						field: "companyId",
						operator: "$eq",
						value: elem.id_company
					}, {
						field: "branchId",
						operator: "$eq",
						value: elem.id_branch
					}, {
						field: "stateId",
						operator: "$eq",
						value: elem.id_state
                   }]);
				});
				response = orgPrivilegesViewModel.find({
					distinct: true,
					select: [{
						field: "userId"
				}, {
						field: "hanaUser"
				}],
					where: whereOptions
				}).results;
			} else {
				var analyticalPrivileges = [];
				let users = userModel.find({
					select: [{
						field: "hanaUser"
					}]
				}).results.map(function(elem) {
					return elem.hanaUser;
				});
				
                //Doing a more dinamic search no matter the package the APs are. 
				let getPackageIdQuery = 'SELECT DISTINCT PACKAGE_ID FROM "_SYS_REPO"."ACTIVE_OBJECT" ' +
                                        'WHERE OBJECT_NAME = \'TIMP_EMPRESA_' + eefs[0].id_company + "_UF_" + eefs[0].id_state + "_FILIAL_" + eefs[0].id_branch + '\'';
                let getPackageIdExecution =  $.execute(getPackageIdQuery).results;
                var packageId = !$.lodash.isEmpty(getPackageIdExecution) ? getPackageIdExecution[0].PACKAGE_ID : "timp.atr.modeling.client.pb.grc";
                
                
                //Getting Users 
                var likePriv = false;
				$.lodash.forEach(eefs, function(elem) { 
				    if(elem.id_company && elem.id_state && elem.id_branch){
				        analyticalPrivileges.push(packageId + "/TIMP_EMPRESA_" + elem.id_company + "_UF_" + elem.id_state + "_FILIAL_" + elem.id_branch);
				    } else if(elem.id_company && elem.id_state){
				        analyticalPrivileges.push(packageId + "/TIMP_EMPRESA_" + elem.id_company + "_UF_" + elem.id_state + "%");
    				    likePriv = true;
				    } else if(elem.id_company){
				        analyticalPrivileges.push(packageId + "/TIMP_EMPRESA_" + elem.id_company + "%");
				        likePriv = true;
				    }  
					
				});

				let hanaUsersQuery = 'SELECT DISTINCT ID AS "userId", HANA_USER AS "hanaUser" FROM "PUBLIC"."EFFECTIVE_PRIVILEGES" AS PRIVILEGES ' +
					'INNER JOIN ' + $.schema + '."CORE::USER" AS USER ' +
					'ON PRIVILEGES.USER_NAME = USER.HANA_USER ' +
					'WHERE USER_NAME IN (\'' + users.join("','") + '\') ';
				if(likePriv){
				    hanaUsersQuery = hanaUsersQuery + 'AND OBJECT_NAME LIKE (\'' + analyticalPrivileges.join("','") + '\')';
				} else {
				    hanaUsersQuery = hanaUsersQuery + 'AND OBJECT_NAME IN (\'' + analyticalPrivileges.join("','") + '\')';
				}
				response = $.execute(hanaUsersQuery).results;
			}
		} else {
			$.messageCodes.push({
				"code": "CORE000025", 
				"type": 'E'
			});
		}
	} catch (e) {
	    $.messageCodes.push({
			"code": "CORE000021",
			"type": 'E',
			"errorInfo": $.parseError(e)
		});
	}
	return response;
};