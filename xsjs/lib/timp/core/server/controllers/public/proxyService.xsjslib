$.import('timp.core.server.controllers.refactor', 'proxyService');
const proxyServiceCtrl = $.timp.core.server.controllers.refactor.proxyService;

$.import('timp.core.server.libraries.internal', 'proxyServer');
const proxyServer = $.timp.core.server.libraries.internal.proxyServer;
$.import('timp.core.server.libraries.internal.util','X2J');
const x2j = $.timp.core.server.libraries.internal.util.X2J;
const _this = this;
$.import('timp.core.server.controllers.refactor', 'configuration');
const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
this.getSystemConfiguration = configurationCtrl.getSystemConfiguration;


_this.saveProxyAdapter = proxyServiceCtrl.saveProxyAdapter;
_this.getProxyAdapter = proxyServiceCtrl.getProxyAdapter;
_this.updateProxyService = proxyServiceCtrl.updateProxyService;
_this.getProxyServices = proxyServiceCtrl.getProxyServices;
_this.servicesAdvancedFilters = proxyServiceCtrl.listAdvancedFilters;
_this.changeAdapterStatus = proxyServiceCtrl.changeAdapterStatus;
_this.proxyServiceAdvancedFilters = proxyServiceCtrl.listAdvancedFilters;
_this.getServiceInformation = proxyServiceCtrl.getServiceInformation;
_this.setDefaultAdapter = proxyServiceCtrl.setDefaultAdapter;
_this.getAdaptersByType = proxyServiceCtrl.getAdaptersByType;
_this.getPPID = function(adapterId){
    var ppid = 0;
    try{
        var host = "";
    	var port = "";
    	var serviceResponse = _this.getServiceInformation("TMFELSGetPpidSyncIn");
    	var serviceUrl = "";
    	if(serviceResponse && serviceResponse[0]){
    	    var serviceDestination = $.lodash.filter(serviceResponse[0].destinations,function(dest){
    	        return dest.isDefault;
    	    })[0];
    	    if(!serviceDestination){
    	        serviceDestination = serviceResponse[0].destinations[0];
    	    }
    	    if(serviceDestination.adapterId){
    	        adapterId = serviceDestination.adapterId;
        	    var adapterResponse = _this.getProxyAdapter({
        			filter: {
        				id: adapterId
        			}
        		});
        		if (adapterResponse) {
        			adapterId = adapterResponse.adapters[0].id;
        			host = adapterResponse.adapters[0].hostName;
        			port = adapterResponse.adapters[0].port;
        		}
    	    }
    	    serviceUrl = serviceDestination.destination;
    	    serviceUrl = serviceUrl.split('bndg_url')[1];
    	}
    	var location = (host + ":" + port + serviceUrl);
        var json = {'tmf:Ppid': '?'};
        var xml = x2j.converterJSON({json:json});
        
        //is https configured?
		let httpsEnabled = false;
        if (serviceResponse && serviceResponse[0] && serviceResponse[0].destinations && serviceResponse[0].destinations[0]){
            httpsEnabled = serviceResponse[0].destinations[0].isHttpsEnabled;
        }
        var externalCallResponse = _this.executeTDFExternalService("POST",adapterId,location,xml,null,httpsEnabled,'TMFELSGetPpidSyncIn');
        if(externalCallResponse && externalCallResponse.xml){
    	    var x = x2j.converterXML({xml:externalCallResponse.xml});
    	    if($.lodash.isPlainObject(x) && x.Envelope && x.Envelope.Body && x.Envelope.Body.Ppid){
    	        ppid = x.Envelope.Body.Ppid;
    	    }
    	}
    }catch(e){
        $.trace.error(e);
    }
    return ppid;
};
_this.setMessageSyncIn = function(adapterId,ppid,componentName,systemVersion,message){
    var response = {};
    try{
        var host = "";
    	var port = "";
    	var serviceResponse = _this.getServiceInformation("TMFELSSetMessageSyncIn");
    	var serviceUrl = "";
    	if(serviceResponse && serviceResponse[0]){
    	    var serviceDestination = $.lodash.filter(serviceResponse[0].destinations,function(dest){
    	        return dest.isDefault;
    	    })[0];
    	    if(!serviceDestination){
    	        serviceDestination = serviceResponse[0].destinations[0];
    	    }
    	    if(serviceDestination.adapterId){
    	        adapterId = serviceDestination.adapterId;
        	    var adapterResponse = _this.getProxyAdapter({
        			filter: {
        				id: adapterId
        			}
        		});
        		if (adapterResponse) {
        			adapterId = adapterResponse.adapters[0].id;
        			host = adapterResponse.adapters[0].hostName;
        			port = adapterResponse.adapters[0].port;
        		}
    	    }
    	    serviceUrl = serviceDestination.destination;
    	    serviceUrl = serviceUrl.split('bndg_url')[1];
    	}
    	var location = (host + ":" + port + serviceUrl);
    	var today = new Date();
    	var json = {
            "tmf:LogMessage":{
                ppid:ppid,
                timestamp:today.getTime(),
                progmodule:componentName,
                progversion:systemVersion,
                username:$.session.getUsername(),
                status:"I",
                text:message
            }
        };
        var xml = x2j.converterJSON({json:json});
        //is https configured?
		let httpsEnabled = false;
        if (serviceResponse && serviceResponse[0] && serviceResponse[0].destinations && serviceResponse[0].destinations[0]){
            httpsEnabled = serviceResponse[0].destinations[0].isHttpsEnabled;
        }
        response = _this.executeTDFExternalService("POST",adapterId,location,xml,null,httpsEnabled,'TMFELSSetMessageSyncIn');
    }catch(e){
        $.trace.error(e);
    }
    return response;
};
_this.executeTDFExternalService = function(method, adapterId, destination, xml, secureAdapterPath, httpsIntegration, serviceName) {
	var response = {};
	if ($.lodash.isInteger(adapterId) && $.lodash.isString(xml) && $.lodash.isString(method)) {
		var adapter = proxyServiceCtrl.getAdapterById(adapterId);
		if (!$.lodash.isNil(adapter)) {
			let systemData = $.execute('SELECT SYSTEM_ID, HOST FROM "M_DATABASE"').results[0];
			let passphrase = systemData ? systemData.HOST + "-" + systemData.SYSTEM_ID : "Deleted59Hashed53Cords";
			let password = $.decrypt(adapter.password, passphrase);
			let body = xml;
			if(xml.indexOf('Envelope') === -1){
			    let header = 'xmlns:tmf="http://sap.com/xi/TMFLOCBR"';
			    let hasTDF = this.getSystemConfiguration({
                    componentName: "CORE",
                    keys: ["TIMP::TDFIntegration"]
                })[0].value;
			    if ([false, 'false'].indexOf(hasTDF) !== -1) {
			        header = 'xmlns:urn="urn:sap-com:document:sap:rfc:functions"';
			    }
			 //   let xmlurl = destination.indexOf('tmfpcoreportrun') === -1 ? 'http://schemas.xmlsoap.org/soap/envelope/' : 'http://www.w3.org/2003/05/soap-envelope';
			 //   const envelope = '<soapenv:Envelope xmlns:soapenv="' + xmlurl + '" ' + header + '><soapenv:Header/><soapenv:Body>#{PARAM_BODY}</soapenv:Body></soapenv:Envelope>';
			    const envelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' + header + '><soapenv:Header/><soapenv:Body>#{PARAM_BODY}</soapenv:Body></soapenv:Envelope>';
			    body = envelope.replace('#{PARAM_BODY}', xml);
			}
			let serviceOptions = {
				contentType: 'text/xml',
				headers: {
					'Content-Type': 'text/xml; charset=utf-8',
					Authorization: 'Basic ' + $.util.codec.encodeBase64(adapter.userName + ':' + password)
				},
				body: body
			};
			if(secureAdapterPath){
			    return serviceOptions;
			}
			
			if(httpsIntegration){
			    destination = destination.replace("http", "https");
			    destination = destination.replace("8000", "443");
			}
			
			let serviceResponse = proxyServer.externalClientRequest(method, destination, serviceOptions,httpsIntegration, serviceName);
			response.status = serviceResponse.status;
			response.xml = serviceResponse.body && serviceResponse.body.asString ? serviceResponse.body.asString() : '';
			response.isSuccessful = ($.lodash.isEqual(serviceResponse.status, $.net.http.OK));
			response.errors = serviceResponse.errors;
		} else {
			throw "Error while retrieving service data"
		}
	} else {
		throw "Invalid parameters"
	}
	return response;
};

_this.executeTOMExternalService = function(method, adapterId, serviceDestinationId, fileId) {
	var response = {};
	if ($.lodash.isInteger(adapterId) && $.lodash.isString(fileId) && $.lodash.isString(method) && $.lodash.isInteger(serviceDestinationId)) {
		var adapter = proxyServiceCtrl.getAdapterById(adapterId);   
		var serviceDestination = proxyServiceCtrl.getServiceDestination(serviceDestinationId);
		if (!$.lodash.isNil(adapter) && !$.lodash.isNil(serviceDestination)) {
		    serviceDestination.destination = serviceDestination.destination.replace("<ID_FILE>","'" + fileId + "'");
            serviceDestination.destination = serviceDestination.destination.replace("&lt;ID_FILE&gt;","'" + fileId + "'");
		    let destination = adapter.hostName + ":" + adapter.port + serviceDestination.destination;
		    if(destination.indexOf('?sap-client') === -1){
		         destination += "?sap-client=" + adapter.client;
		    }
			let systemData = $.execute('SELECT SYSTEM_ID, HOST FROM "M_DATABASE"').results[0];
			let passphrase = systemData ? systemData.HOST + "-" + systemData.SYSTEM_ID : "Deleted59Hashed53Cords";
			let password = $.decrypt(adapter.password, passphrase);
			let serviceOptions = {
				contentType: 'text/plain',
				headers: {
					'Content-Type': 'text/plain; charset=utf-8',
					Authorization: 'Basic ' + $.util.codec.encodeBase64(adapter.userName + ':' + password)
				}
			};
			let serviceResponse = proxyServer.externalClientRequest(method, destination, serviceOptions);
			if (serviceResponse.body && $.lodash.isFunction(serviceResponse.body.asString)) {
				response = {
					status: serviceResponse.status,
					xml: serviceResponse.body.asString()
				};
				response.isSuccessful = ($.lodash.isEqual(serviceResponse.status, $.net.http.OK)) ? true : false;
			}
		} else {
			throw "Error while retrieving service data"
		}
	} else {
		throw "Invalid parameters"
	}
	return response;
};