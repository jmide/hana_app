$.import('timp.core.server.controllers.refactor', 'group');
const groupCtrl = $.timp.core.server.controllers.refactor.group;

this.saveGroup = groupCtrl.saveGroup;
this.listGroups = groupCtrl.listGroups;
this.getGroupInformation = groupCtrl.getGroupInformation;
this.getGroupsRequiredInformation = groupCtrl.getRequiredInformation;
this.deleteGroup = groupCtrl.deleteGroup;
this.groupsAdvancedFilters = groupCtrl.listAdvancedFilters;
this.changeGroupStatus = groupCtrl.changeGroupStatus;
this.getGroupsByUser = groupCtrl.getGroupsByUser;

//Executed by the JOB - user_group_job
this.automaticCreateGroup = groupCtrl.automaticCreateGroup;
this.checkHanaGroupStatus = groupCtrl.checkHanaGroupStatus;
