$.import('timp.core.server.controllers.refactor', 'configuration');
const configurationCtrl = $.timp.core.server.controllers.refactor.configuration;

$.import('timp.core.server.models.tables', 'configuration');
const fullDatesModel = $.timp.core.server.models.tables.configuration.fullDatesModel;

this.saveSystemConfiguration = configurationCtrl.saveSystemConfiguration;
this.getSystemConfiguration = configurationCtrl.getSystemConfiguration;
this.userCanChooseHighPerformanceBRB = configurationCtrl.userCanChooseHighPerformanceBRB;

this.createFullDates = function(year) {
	if ($.lodash.isInteger(year) || $.lodash.isInteger(Number(year))) {
		var startDate = year + '0101';
		var limitDate = Number(new $.moment().format("YYYY"));
		limitDate = (limitDate + 1) + '0101';
		var dateMoment = new $.moment(startDate, "YYYYMMDD");
		var limitMoment = new $.moment(limitDate, "YYYYMMDD");
		var dates = [];
		while (dateMoment.format("YYYYMMDD") !== limitMoment.format("YYYYMMDD")) {
			dates.push({
				fullDate: dateMoment.format("YYYYMMDD"),
				year: dateMoment.format("YYYY")
			});
			dateMoment.add(1, 'days');
		}
		if (!$.lodash.isEmpty(dates)) {
			fullDatesModel.delete({
				where: [{
					literal: {
						type: 'integer',
						value: 1
					},
					operator: '$eq',
					value: 1
		        }]
			});
			var response = fullDatesModel.batchCreate(dates);
			return response;
		} else {
			return {
				"errors": [{
				    error: 'No dates where created'
				}],
				"results": {
					"created": false,
					"instances": []
				}
			};
		}
		return response;
	} else {
		throw "Invalid Parameter";
	}
};