$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'attachments');
var attachment = $.timp.core.server.models.attachments.table;

$.import('timp.core.server.models', 'objects');
var objects = $.timp.core.server.models.objects.objects;
var objectTypesModel = $.timp.core.server.models.objects.objectTypes;

$.import('timp.core.server.models', 'components');
var components = $.timp.core.server.models.components.components;

$.import('timp.core.server.models', 'fileSystem');
var fileModel = $.timp.core.server.models.fileSystem.file;
var fileShareModel = $.timp.core.server.models.fileSystem.fileShare;

$.import('timp.core.server.controllers', 'fileCRUDF');
var fileCRUDF = $.timp.core.server.controllers.fileCRUDF;

$.import('timp.core.server.controllers', 'fileShare');
var fileShare = $.timp.core.server.controllers.fileShare;

//start of decoding base64
this.b64ToUint6 = function(nChr) {
  return nChr > 64 && nChr < 91 ?
      nChr - 65
    : nChr > 96 && nChr < 123 ?
      nChr - 71
    : nChr > 47 && nChr < 58 ?
      nChr + 4
    : nChr === 43 ?
      62
    : nChr === 47 ?
      63
    :
      0;
};

function uint6ToB64 (nUint6) {

  return nUint6 < 26 ?
      nUint6 + 65
    : nUint6 < 52 ?
      nUint6 + 71
    : nUint6 < 62 ?
      nUint6 - 4
    : nUint6 === 62 ?
      43
    : nUint6 === 63 ?
      47
    :
      65;

}

//base64 to byte arrays
this.base64DecToArr = function(sBase64, nBlocksSize) {
  var sB64Enc = sBase64.replace(/[^A-Za-z0-9\+\/]/g, ""), 
    nInLen = sB64Enc.length,
    nOutLen = nBlocksSize ? Math.ceil((nInLen * 3 + 1 >> 2) / nBlocksSize) * nBlocksSize : nInLen * 3 + 1 >> 2, 
    taBytes = new Uint8Array(nOutLen);

  for (var nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
    nMod4 = nInIdx & 3;
    nUint24 |= this.b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
    if (nMod4 === 3 || nInLen - nInIdx === 1) {
      for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
        taBytes[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
      }
      nUint24 = 0;
    }
  }
  return taBytes;
};

function base64EncArr (aBytes) {

  var nMod3 = 2, sB64Enc = "";

  for (var nLen = aBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
    nMod3 = nIdx % 3;
    if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
    nUint24 |= aBytes[nIdx] << (16 >>> nMod3 & 24);
    if (nMod3 === 2 || aBytes.length - nIdx === 1) {
      sB64Enc += String.fromCharCode(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
      nUint24 = 0;
    }
  }

  return sB64Enc.substr(0, sB64Enc.length - 2 + nMod3) + (nMod3 === 2 ? '' : nMod3 === 1 ? '=' : '==');

}
//end of decoding base64

//check id?
this.PUT = function (object) {
    let errorGenerator = new $.ErrorGenerator('PUT');
    try {
        object = object || util.getParam('object');
        if (object) {
            var idComponent = components.READ({ where: [{ field: "name", oper: "LIKE", value: object.component }] })[0];
            if(!idComponent){
                throw errorGenerator.paramsError(['componentId']);
            }
            var dataString = object.file;
            var matches = dataString.match(/^data:([A-Za-z-+\/\.]+);base64,(.+)$/);
    
            if (matches.length !== 3) {
                throw errorGenerator.paramsError(['input']);
            }
    		
    		var fileBuffer = this.base64DecToArr(matches[2]).buffer;
    		//object.body.asArrayBuffer();
    
            var response = attachment.CREATE({
    			file: fileBuffer,
    			name: object.name,
    			description: object.description,
    			type: object.type,
    			idComponent: idComponent.id
    		});
    		
    		var idObjectType = objectTypesModel.READ({
            fields: ['id'],
                where: [{field: 'name', oper: '=', value: 'CORE::Attachments'}]
    		})[0].id;
    		
    		var fileData = {
                status: 4,
                idFolder: -1,
                idObjectType: idObjectType,
                idObject: response.id
    		};
    		
    		var newFile = fileModel.CREATE(fileData);
    		
    		response.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + response.id + "/";
            response.status = 'Public';
            response.idFile = newFile.id;
    		return (JSON.stringify(response));
    	} else {
            throw errorGenerator.generateError('00', '06');
    	}   
    } catch(e) {
        throw errorGenerator.generateError('00', '69', null, e);
    }
};

this.UPLOAD = function (id) {
    var response;
    let errorGenerator = new $.ErrorGenerator('UPLOAD');
    try {
        if ($.request.entities.hasOwnProperty(0)) {
            var data = $.request.entities;
            var file = data[0].body.asArrayBuffer();
    		var name = data[1].body.asString();
    		var description = data[2].body.asString();
    		var type = data[3].body.asString();
    		var component = data[4].body.asString();
            //return {component: component, url: "/timp/core/server/endpoint.xsjs/attachments/get/7/", name: name, id: 63};
    		var idComponent = components.READ({ where: [{ field: "name", oper: "LIKE", value: component }] })[0];
            if(!idComponent) {
                throw errorGenerator.paramsError(['componentId']);
            }
            if (util.isValid(id) && !isNaN(id)) {
                if(util.isString(id)){ id = Number(id); }
                response = attachment.UPDATE({
                    id: id,
                    file: file,
                    name: name,
                    description: description,
                    type: type,
                    idComponent: idComponent.id
                });
            } else {
                response = attachment.CREATE({
                    file: file,
                    name: name,
                    description: description,
                    type: type,
                    idComponent: idComponent.id
                });
    
                var idObjectType = objectTypesModel.READ({
                    fields: ['id'],
                    where: [{field: 'name', oper: '=', value: 'CORE::Attachments'}]
                })[0].id;
    
                var fileData = {
                    status: 4,
                    idFolder: -1,
                    idObjectType: idObjectType,
                    idObject: response.id
                };
    
                var newFile = fileModel.CREATE(fileData);
            }
            response.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + response.id + "/";
            response.status = 'Public';
            response.idFile = newFile.id;
    		return (JSON.stringify(response));
    	} else {
            var object = id ? id : util.getParam('object');
            if (util.isValid(object.id) && !isNaN(object.id)) {
                var idFile = object.id;
                if(util.isString(object.id)){ idFile = Number(object.id); }
                var _data = {
                    id: idFile,
                    name: object.name,
                    description: object.description
                };
                return attachment.UPDATE(_data);
            }
            throw errorGenerator.generateError('00', '06');
    	}
    } catch(e) {
        throw errorGenerator.generateError('00', '69', null, e);
    }
};

this.GET = function (object) {
    let getBytes = object.getBytes || false;
    let id = typeof(object) === "object" ? object.id : object;
    if(!isNaN(id)) {
        var lines = attachment.READ({
            where: [{field: "id", oper: "=", value: Number(id)}]
        })[0];
        
        if(lines) {
            if (getBytes) {
                return lines;
            }
            $.response.contentType = lines.type;
            if ("_setBody" in $.response && typeof $.response._setBody === "function") {
                $.response._setBody(lines.file);
            } else {
                return (lines.file);
            }
        } else {
            return ("Received an nonexistent ID");
        }
    }
    return ("Did not receive a valid ID");
};

this.GET64 = function (id){
    if(!isNaN(id)) {
        var lines = attachment.READ({
            where: [{field: "id", oper: "=", value: Number(id)}]
        })[0];
        var arr = [];
        if(lines) {
            if(lines.type === 'text/plain' && typeof lines.file === 'string'){
                var file = this.encodeString(lines.file);
                arr  = new Uint8Array(file);
                return base64EncArr(arr);
            }else{
                arr = new Uint8Array(lines.file);
                return base64EncArr(arr);
            }
            
        } else {
            return ("Received an nonexistent ID");
        }
    }
    return ("Did not receive a valid ID");
};

this.encodeString = function(stringFile){
       if (typeof stringFile === 'string' && stringFile.constructor()) {
        let stringFileEncoded = '\ufeff' + stringFile;
        let buffer = new ArrayBuffer(stringFileEncoded);
        return buffer;
    }
    return stringFile;
};

this.DELETE = function (id) {
    let errorGenerator = new $.ErrorGenerator('DELETE');
    try {
        id = id ? id : util.getParam('object');
        id = typeof(id) === "object" ? id.id : id;
        if(!isNaN(id)) {
            var lines = attachment.DELETE(Number(id));
            if(lines) {
                return (lines);
            } else {
                throw errorGenerator.generateError('00', '06');
            }
        } else {
            throw errorGenerator.generateError('00', '06');
        }   
    } catch (e) {
        throw errorGenerator.generateError('00', '71');
    }
};

this.LIST = function (object) {
    var where = [];
    object = object ? object : util.getParam('object');
    if(object){
        var idComponent = components.READ({ where: [{ field: "name", oper: "LIKE", value: object.component }] })[0];
        if(idComponent){
            where.push({ field: "idComponent", oper: "=", value: idComponent.id });
        } else {
            return [];
        }
        if(object.id){
            where.push({field: 'id',oper:'=',value:object.id});
        }
    }
    
    var publicAttachment = [];
    var privateAttachment = [];
    var iSharedAttachment = [];
    var shareWithMeAttachment = [];
    
    //<<< Get the public attachment >>>
    var publicFilter = {
        objectType: 'CORE::Attachments',
        status: ['Public']
    };
    var filesPublic = fileCRUDF.listFiles(publicFilter);
    
    if (filesPublic.length > 0){
        var onPublic = [{left:'id', right:'idObject'}];
        onPublic.push(filesPublic.map(function(file){
            return {field: "id", oper: "=", value: file.id};
        }));
        
        var linesPublic = attachment.READ({
            fields: ["id","name", "description", "type", "idComponent", "creationDate", "creationUser", "modificationDate", "modificationUser"],
            where: where,
            join:[{
                table: fileModel,
                alias: 'file',
                fields:['id'],
                on: onPublic
            }]
        });
        publicAttachment = linesPublic.map(function(file){
            file.idFile = file.file[0].id;
            delete file.file;
            file.status = 'Public';
            file.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + file.id + "/";
            file.isOwner = file.creationUser === $.getUserID();
            return file;
        });
    }
    
    //<<< Get the private(active) attachment >>>
    var privateFilter = {
        objectType: 'CORE::Attachments',
        status: ['Active'],
        attachment: true
    };
    var filesPrivate = fileCRUDF.listFiles(privateFilter);
    
    if (filesPrivate.length > 0){
        var onPrivate = [{left:'id', right:'idObject'}];
        onPrivate.push(filesPrivate.map(function(file){
            return {field: "id", oper: "=", value: file.id};
        }));
        
        var linesPrivate = attachment.READ({
            fields: ["id","name", "description", "type", "idComponent", "creationDate", "creationUser", "modificationDate", "modificationUser"],
            where: where,
            join:[{
                table: fileModel,
                alias: 'file',
                fields:['id'],
                on: onPrivate
            }]
        });
        
        privateAttachment = linesPrivate.map(function(file){
            file.idFile = file.file[0].id;
            delete file.file;
            file.status = 'Private';
            file.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + file.id + "/";
            file.isOwner = file.creationUser === $.getUserID();
            return file;
        });
    }
    
    //<<< Gets the attachment that I shared >>>
    if (filesPrivate.length > 0){
        var onShared = [{left:'id', right:'idObject'}];
        onShared.push(filesPrivate.map(function(file){
            return {field: "id", oper: "=", value: file.id};
        }));
        
        var linesShared = attachment.READ({
            fields: ["id","name", "description", "type", "idComponent", "creationDate", "creationUser", "modificationDate", "modificationUser"],
            where: where,
            join:[{
                table: fileModel,
                alias: 'file',
                fields:['id'],
                on: onShared
            },{
                table: fileShareModel,
                alias: 'fileShare',
                fields:['id'],
                on: [
                    {leftTable: fileModel, left: 'id', right: 'idFile'}
                ]
            }]
        });
        
        iSharedAttachment = linesShared.map(function(file){
            file.idFile = file.file[0].id;
            delete file.file;
            delete file.fileShare;
            file.status = 'Shared';
            file.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + file.id + "/";
            file.isOwner = file.creationUser === $.getUserID();
            return file;
        });
    }
    
    //<<< Gets shared with me attached. >>>
    var sharedFilter = {
        objectType: 'CORE::Attachments'
    };
    var filesShared = fileShare.listFileShare(sharedFilter);
    
    if (filesShared.length > 0) {
        var onShared = [{left:'id', right:'idObject'}];
        onShared.push(filesShared.map(function(shared) {
            return {field: "id", oper: "=", value: shared.file[0].id};
        }));
        
        var linesShared = attachment.READ({
            fields: ["id","name", "description", "type", "idComponent", "creationDate", "creationUser", "modificationDate", "modificationUser"],
            where: where,
            join:[{
                table: fileModel,
                alias: 'file',
                fields:['id'],
                on: onShared
            }]
        });
        
        shareWithMeAttachment = linesShared.map(function(file){
            file.idFile = file.file[0].id;
            delete file.file;
            file.status = 'Shared';
            file.url = "/timp/core/server/endpoint.xsjs/attachments/get/" + file.id + "/";
            file.isOwner = file.creationUser === $.getUserID();
            return file;
        });
    }
    
    //<<< Delete private attachments which I shared. >>>
    var privateAttachmentNotShared = privateAttachment.filter(function(filePrivate){
        // var found = false;
        for (var i = 0; i < iSharedAttachment.length; i++){
            if (filePrivate.id === iSharedAttachment[i].id){
                // found = true;
                // break;
                return false;
            }
        }
        // return (found)?false:true;
    });
    
    return publicAttachment.concat(privateAttachmentNotShared.concat(iSharedAttachment.concat(shareWithMeAttachment)));
};

this.getURL = function(id){
    try{
        id = typeof(id) === 'number' ? id : id.id;
        return "/timp/core/server/endpoint.xsjs/attachments/get/" + id + "/";
    }catch(e){
        return e;
    }
};

this.list = function (){
    var response = objects.listObject(attachment, "CORE::Attachments");
    return (response);
};

this.getHelpFile = function(object) {
    $.import('timp.core.server.controllers.refactor','componentManual');
    const componentManualController = $.timp.core.server.controllers.refactor.componentManual;
    let params = {
        componentName: object.component,
        language: $.request.cookies.get('Content-Language')
    };
    let file = componentManualController.getComponentManual(params);
    file.url = this.getURL(file.id);
    return file;
};