$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'users');
var users = $.timp.core.server.models.users.table;

this.usersTable = users;

$.import('timp.core.server.models', 'users');
var groups = $.timp.core.server.models.users;

this.groupTable = groups.group;
this.groupUserTable = groups.groupUser;
this.listGroups = function(options) {
	try {
		var hanaUser = $.session.getUsername();
		var readOptionsUser = {
			where: [{
				"field": "hana_user",
				"value": hanaUser.toUpperCase(),
				"oper": "="
            }]
		};
		var optionsUser = users.READ(readOptionsUser);

		var readOptionsAllGroups = {
			join: [{
				alias: 'group_leader',
				fields: ['id', 'name', 'hana_user', 'middle_name', 'last_name'],
				table: users,
				on: [{
					left: 'id_leader',
					right: 'id'
                }],
				outer: 'left'
            },{
				alias: 'UXG',
				fields: ['id_user'],
				table: this.groupUserTable,
				on: [{
					left: 'id',
					right: 'id'
                }],
				outer: 'right'
            }],
			order_by: options && options.order ? options.order : ["id"]
		};
		var allGroups = this.groupTable.READ(readOptionsAllGroups);

		for (var l = 0; l < allGroups.length; l++) {
			if (optionsUser[0].admin == 1) {
				allGroups[l].isAssigned = 1;
				allGroups[l].canEdit = 1;
			} else {
				if (allGroups[l].hasOwnProperty('creationUser') && allGroups[l].creationUser == hanaUser) {
					allGroups[l].canEdit = 1;
				} else if (allGroups[l].hasOwnProperty('group_leader') && allGroups[l].group_leader[0].hana_user == hanaUser) {
					allGroups[l].canEdit = 1;
				}
			}
			if (allGroups[l].hasOwnProperty('group_leader')) {
				allGroups[l].group_leader = allGroups[l].group_leader[0];
			}
		}

		return allGroups;
	} catch (e) {
		$.messageCodes.push({
			"code": "",
			"type": 'E',
			"errorInfo": util.parseError(e)
		});
		return null;
	}

};
//Read the id from the license login 
// function readLicenseTable(){
//     var hanaUser = $.session.getUsername();
//     try{
//     var conn = $.db.getConnection();
//     var pstmt = conn.prepareStatement('SELECT \"ID\"  FROM \"TIMP\".\"CORE::Licenses\" WHERE \"USERNAME\" = ?');
// 	pstmt.setString(1,hanaUser);
//     var rs = pstmt.executeQuery();
//     if(rs.next()){
//         return rs.getInteger(1);
//     }else{
//         return -1;
//     }
    
//     }catch(e){
//         return -1;
//     }
// }


this.listHanaUsers = function () {
    var userList = users.listHanaUsers();
    for (var i = 0; i < userList.length; i++) {
        userList[i] = userList[i][0];
    }
    return (userList);
};
this.listAllUsers = function(){
    var usersData = users.READ({order_by: ["name"],fields:["id","hana_user","name","last_name"]});
    return usersData.map(function(user){
        return {
            id: user.id,
            hanaName: user.hana_user,
            name: (user.name || '') + " "+(user.last_name || '')
        };
    });
};

this.getLoggedUser = function () {
    try{
        var hanaUser = $.session.getUsername();
        var options = {
            where : [{field: 'hana_user', oper: '=', value: hanaUser.toUpperCase()}]
        };
        
        var result = users.READ(options);
        // return result[0];
        var response = result[0];
        return (response);
    } catch (e) {
        throw 'Impossible to execute this.getLoggedUser: ' + util.parseError(e);
    }
};

this.getUser = function () {
    return ({ userName:$.session.getUsername() });
};

this.getUserName = function () {
    var hanaUser = $.session.getUsername();
    var user = users.READ({
        where: [{ field: 'hana_user', value: hanaUser, oper: '=' }]
    })[0];
    
    var response = {
        name: user.name,
        lastName: user.last_name,
        hanaUser: hanaUser
    };
    
    return (response);
};

this.getTimpUser = function(){
    var options = {
        where: [{ field: 'hana_user', value: $.session.getUsername(), oper: '=' }]
    };
    var returnObj = users.READ(options);
    return returnObj[0];
};

this.getUserGroups = function(){
    return 'mkt_api';
    //return users_group;
};

/*
    Replace the creation and modification Users' id to the User's full name.
*/
this.getUserFullName = function(object){
    try {
        if (object) {
                var creationUser = users.READ({
                    fields: ['id', 'name', 'last_name'],
                    where: [{ field: 'id', oper:'=', value: object.creationUser }]
                })[0];
                if (creationUser){
                    object.creationUser = creationUser.name + ' ' + creationUser.last_name;    
                }//return Creation User
                    
                var modificationUser = users.READ({
                    fields: ['id', 'name', 'last_name'],
                    where: [{ field: 'id', oper:'=', value: object.modificationUser }]
                })[0];
                if (modificationUser){
                    object.modificationUser = modificationUser.name + ' ' + modificationUser.last_name;    
                }//return Modification User
                return object; 
        } else {
            throw 'Impossible to execute this.getUserFullName: Object is null' ;
        }
    } catch (e) {
        throw 'Impossible to execute this.getUserFullName: ' + util.parseError(e);
    }
};

this.getMapUsersGroups = function(){
    try{
        var response = this.groupUserTable.READ({
            order_by: ['id_group']
        });
        return response;
    }catch (e){
        $.messageCodes.push({
			"code": "",
			"type": 'E',
			"errorInfo": util.parseError(e)
		});
		return null;
    }
    
};
