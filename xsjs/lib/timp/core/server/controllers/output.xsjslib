$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'output');
var outputTable = $.timp.core.server.models.output.output;
var outputTypeTable = $.timp.core.server.models.output.outputType;
var outputValueTable = $.timp.core.server.models.output.outputValue;

this.list = function(object){
    var result = [];
    var response = outputTable.list(object);
    // throw response
    if(response){
        for(var i = 0; i < response.length; i++){
            var resObj = {};
            resObj.id = response[i].id;
            resObj.idComponent = response[i].idComponent;
            resObj.objectType = response[i].objectType;
            resObj.idObject = response[i].idObject;
            
            resObj.outputType = {};
            if(response[i].outputType){
                for(var j = 0; j < response[i].outputType.length; j++){
                    resObj.outputType.id = response[i].outputType[j].id;
                    resObj.outputType.iconFont = response[i].outputType[j].iconFont;
                    resObj.outputType.icon = response[i].outputType[j].icon;
                }
            }
            if(response[i].outputTypeText){
                for(var k = 0; k < response[i].outputTypeText.length; k++){
                    resObj.outputType.name = response[i].outputTypeText[k].name;
                    resObj.outputType.description = response[i].outputTypeText[k].description;
                }
            }
            resObj.name = response[i].name;
            resObj.description = response[i].description;
           
            if(response[i].values){
                resObj.values = [];
                for(var l = 0; l < response[i].values.length; l++){
                    var valuesArray = {
                        id: response[i].values[l].id,
                        value: response[i].values[l].value,
                        dataType: response[i].values[l].valueDataType,
                        year: response[i].values[l].year,
                        month: response[i].values[l].month,
                        subperiod: response[i].values[l].subperiod,
                        idCompany: response[i].values[l].idCompany,
                        uf: response[i].values[l].uf,
                        idBranch: response[i].values[l].idBranch,
                        idTax: response[i].values[l].idTax
                        // rest of the JSON (from JSON column)
                    }
                    resObj.values.push(valuesArray);
                }
            }
            result.push(resObj);
       }
    }
    return result;
};

this.create = function(){
    let errorGenerator = new $.ErrorGenerator('create');
    try  {
        var result = {};
        var object = $.request.parameters.get('object');
        var response = outputTable.create(object);
        
        if(response){
            result.id = response.id;
        	result.idComponent = response.idComponent;
        	result.objectType = response.objectType;
        	result.idObject = response.idObject;
        // 	result.outputType.: {
        // 		result.outputType.id:response.
        // 		result.outputType.name:response.
        // 		result.outputType.description:response.
        // 		result.outputType.iconFont:response.
        // 		result.outputType.icon:response.
        //     }
            result.name = response.name;
            result.description = response.description;
            
            if(response.idOutputType) {
                var outputTypeList = outputTypeTable.list(object);
                throw outputTypeList;   
            }
        }
    
        
        return result;
    } catch (e) {
        throw errorGenerator.generateError('00', '97', null, e);
    }
};

this.update = function(){
    let errorGenerator = new $.ErrorGenerator('update');
    try {
        var object = $.request.parameters.get('object');
        var response = outputTable.update(object);
        
        return response;   
    } catch(e) {
        throw errorGenerator.generateError('00', '98', null, e);
    }
};

this.createValue = function(){
    let errorGenerator = new $.ErrorGenerator('createValue');
    try {
        var object = $.request.parameters.get('object');
        var response = outputValueTable.create(object);
        
        return response;
    } catch(e) {
        throw errorGenerator.generateError('00', '98', null, e);
    }
};

this.updateValue = function(){
    let errorGenerator = new $.ErrorGenerator('updateValue');
    try {
        var object = $.request.parameters.get('object');
        var response = outputValueTable.update(object);
        
        return response;
    } catch(e) {
        throw errorGenerator.generateError('00', '98', null, e);
    }
};

this.listOutputType = function(){
    var result = [];
    var resObj = {};
    var object = $.request.parameters.get('object');
    var response = outputTypeTable.list(object);
    
    if(response){
        // Avoid DoS object attack
        if ( response.length > 1000 ) {
            throw 'Request response overflow';
        }
        for(var i = 0; i < response.length; i++){
            resObj.id = response[i].id;
            resObj.icon = response[i].icon;
            resObj.iconFont = response[i].iconFont;
            if(response[i].outputTypeText.length){
                resObj.name = response[i].outputTypeText[0].name;
                resObj.description = response[i].outputTypeText[0].description;
            }
            result.push(resObj);
        }
    }
    return result;
}

this.updateOutputValue = function(object){
    let errorGenerator = new $.ErrorGenerator('updateOutputValue');
    try {
        var option = {
                join: [{
                    table: outputTable,
                    alias: "outputTable",
                    on: [{
                        left: 'idOutput',
                        right: 'id'
                    }]
                }],
                where: [
                {
                    field: 'idComponent',
                    oper: '=',
                    value: '22',
                    table: outputTable
                },{
                    field: 'idCompany',
                    oper: '=',
                    value: object.idCompany
                },{
                    field: 'idBranch',
                    oper: '=',
                    value: object.idBranch
                },{
                    field: 'ufOrig',
                    oper: '=',
                    value: object.ufOrig
                },{
                    field: 'idTax',
                    oper: '=',
                    value: object.idTax
                },{
                    field: 'year',
                    oper: '=',
                    value: object.year
                },{
                    field: 'month',
                    oper: '=',
                    value: object.month
                },{
                    field: 'subPeriod',
                    oper: '=',
                    value: object.subPeriod
                }]
                // simulate: true
        }
     
        var optionForUpdate = outputValueTable.READ(option)[0];
        var newValue = Number(optionForUpdate.value) + Number(object.ajusteValue);
        
        var response = outputValueTable.UPDATE({id: optionForUpdate.id, value: newValue});
        return response;   
    } catch(e) {
        throw errorGenerator.generateError('00', '98', null, e);
    }
};


