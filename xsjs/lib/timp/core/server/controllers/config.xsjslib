$.import('timp.core.server.models', 'config');
var configModel = $.timp.core.server.models.config.model;

this.getParameter = function (component, parameter) {
    if(component && parameter){
        var lines = configModel.READ({
            where: [
                {field: 'component', oper: '=', value: component},
                {field: 'parameter', oper: '=', value: parameter}
            ]
        });
        if( lines.length > 0){
            return lines[0].value;
        }
        else{
            return null;
        }
    }
    else{
        return null;
    }
};

this.getParameterID =  function(component, parameter){
    try {
        var value = configModel.READ({
            fields: ["id"],
            where: [{field: 'component', oper: '=', value: component },
                    {field: 'parameter', oper: '=', value: parameter }]
        })[0].value;
    } catch (e) {
        return null;
    }
    return value;
};

this.setParameter = function (component, parameter, value) {
    var response;
    if(component && parameter){
        //read if the parameter exists
        var parID = this.getParameterID(component,parameter);
        if(parID){ 
            //MODIFY
            response = configModel.UPDATE({
                id          : parID,
    			component   : component,
    			parameter   : parameter,
    			value       : value
    		});   
        }
        else{ 
            //CREATE
            response = configModel.CREATE({
    			component   : component,
    			parameter   : parameter,
    			value       : value
    		});  
        }
        return response;
    }
    else{
        return null;
    }
};
    
this.getHomeURL = function(){
    try {
        var url = configModel.READ({
            fields: ['value'],
            where: [
                {field: "parameter", oper: "=", value: "ALLTAX::HOME"}
            ]
        })[0].value; 
    } catch (e) {
        url = "/timp/tkb/";
    }
    
    if (typeof url !== 'string') {
        url = JSON.stringify(url);
    }
    
    $.response.contentType = 'text/plain';
    $.response.setBody(url);
    
    return url;
};

this.hasTDFIntegration = function(){
    $.import('timp.core.server.controllers.refactor','configuration');
    const configurationController = $.timp.core.server.controllers.refactor.configuration;
    let systemConfigurationResponse = configurationController.getSystemConfiguration({
        keys:['TIMP::TDFIntegration','InstalationType'],
        componentName: "CORE"
    });
    var hasTdfIntegration = $.lodash.filter(systemConfigurationResponse,function(e){
        return e.value === "true";
    }).length > 0;
    return hasTdfIntegration;
};

this.getTdfSchema = function() {
    var response;
    try {
        response = configModel.READ({
            fields: ['value'],
            where: [{field: "parameter", oper: "=", value: "tdfSchemaName"}]
        })[0].value;
    } catch (e) {
        response = '';
    }
    
    return response;
};

this.getSessionTime = function() {
    var response = {};
	try {
		var time = configModel.READ({
            fields: ['value'],
            where: [
                {field: 'component', oper: '=', value: 'CORE'},
                {field: 'parameter', oper: '=', value: 'sessionTime'}
            ]
        });
        response = time.length > 0 ? time[0].value : 600000; // tiempo guardado o 10 Minutos por default
	} catch (e) {
		response = 600000; // 10 Minutos por default
	}
	return response;
};