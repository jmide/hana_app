$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'fileSystem');
var fileSystem = $.timp.core.server.models.fileSystem;

$.import('timp.core.server.models.objects', 'objectTypes');
var objectTypeModel = $.timp.core.server.models.objects.objectTypes;

$.import('timp.core.server.models', 'users');
var users = $.timp.core.server.models.users.table;


this.getSessionConfiguration = function () {
	$.import('timp.core.server.controllers.refactor', 'configuration');
	var configurationCtrl = $.timp.core.server.controllers.refactor.configuration;
	return configurationCtrl.getSessionConfiguration();
};

// Constantes
// Los datos están en segundos
var sessionConfig = this.getSessionConfiguration();
var LOCK_TIME = sessionConfig.fileLockTime || 300;
var VERIFY_TIME = sessionConfig.verifyFileLockTime || 30;

/*
	Verifica si el archivo está bloqueado
	si lo está retorna false de lo contrario
	retorna el id del lock que se creó
	object = {
		objectType: String,
		idObject: Integer || Array
	}
*/
this.getLock = function(object) {
	object = object || util.getParam('object');
	if (typeof object === 'string') {
		object = JSON.parse(object);
	}
	try {
		if (!object.hasOwnProperty('objectType')) {
			throw 'Object Type is required';
		}
		if (!object.hasOwnProperty('idObject')) {
			throw 'The ID Object is required';
		}
		var idObjectType = fileSystem.getIdObjectType(object.objectType);
		var whereOptions = [{
			field: 'idObjectType',
			oper: '=',
			value: idObjectType
		}];
		if (object.idObject instanceof Array) {
			var temp = [];
			for (var i = 0; i < object.idObject.length; i++) {
				temp.push({
					field: 'idObject',
					oper: '=',
					value: object.idObject[i]
				});
			}
			whereOptions.push(temp);
		} else {
			whereOptions.push({
				field: 'idObject',
				oper: '=',
				value: object.idObject
			});
		}
		if (object.onExecute) {
			whereOptions.push({
				field: 'onExecute',
				oper: '!=',
				value: 1
			});
		}
		var exists = fileSystem.readFileLock({
			where: whereOptions
		});
		var tmp, user;
		if (exists.length > 0) {
			var remove;
			if (object.idObject instanceof Array) {
				var isOk = true;
				tmp = [];
				for (i = 0; i < exists.length; i++) {
					var x = exists[i];
					tmp.push(x.id);
					remove = this.tryRemoveLock(x);
					if (remove === false) {
						if ($.getUserID() !== x.creationUser) {
							isOk = false;
							x.userRequest = $.getUserID();
							x.dateRequest = new Date(Date.now());
							user = users.getUser(x.creationUser);
							this.cleanLock(x);
							fileSystem.saveFileLock(x);
						}
					}
				}
				if (!isOk) {
					return exists.map(function(item) {
						return item.idObject;
					});
				} else {
					return {
						id: tmp,
						time: VERIFY_TIME
					};
				}
			} else {
				for (var e = 0; e < exists.length; e++) {
					if (!this.tryRemoveLock(exists[e])) {
						exists = exists[e];
						remove = false;
						break;
					} else {
						remove = true;
					}
				}
				if (remove === false) {
					if ($.getUserID() === exists.creationUser) {
						return {
							id: exists.id,
							time: VERIFY_TIME,
							onExecute: exists.onExecute
						};
					}
					exists.userRequest = $.getUserID();
					exists.dateRequest = new Date(Date.now());
					user = users.getUser(exists.creationUser);
					this.cleanLock(exists);
					fileSystem.saveFileLock(exists);
					return {
						user: user.name + ' ' + user.last_name,
						onExecute: exists.onExecute
					};
				}
			}
		}
		var createOptions, lock;
		if (object.idObject instanceof Array) {
			tmp = [];
			for (i = 0; i < object.idObject.length; i++) {
				createOptions = {
					idObjectType: idObjectType,
					idObject: object.idObject[i],
					lockDate: new Date(Date.now()),
					lockVerification: new Date(Date.now()),
					onExecute: object.onExecute ? 1 : 0,
					lockConditional: false
				};
				lock = fileSystem.saveFileLock(createOptions);
				tmp.push(lock.id);
			}
			return {
				id: tmp,
				time: VERIFY_TIME
			};
		}
		createOptions = {
			idObjectType: idObjectType,
			idObject: object.idObject,
			lockDate: new Date(Date.now()),
			lockVerification: new Date(Date.now()),
			onExecute: object.onExecute ? 1 : 0,
			lockConditional: false
		};
		lock = fileSystem.saveFileLock(createOptions);
		if (lock) {
			return {
				id: lock.id,
				time: VERIFY_TIME,
				onExecute: lock.onExecute
			};
		}
		throw lock;
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE009039',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		return false;
	}
};

/*
	Verifica si el archivo está bloqueado
	si no lo está retorna true si no
	verifica si puede ser liberado y si se puede 
	retorna true de lo contrario false.
	object = {
		id: Integer,
		lockDate: Date,
		lockVerification: Date,
		lockConditional: Boolean || Integer
	}
*/
this.tryRemoveLock = function(object) {
	try {
		var removeLock = function() {
			return fileSystem.deleteFileLock(object.id);
		};
		var remove;
		var now = new Date(Date.now());
		var lockVerification = new Date(object.lockVerification);
		// 30 segundos es el intervalo en el que se llamará la función verify
		// la cual verifica si el usuario está aún en la edición y si hay algún
		// otro usuario que haya solicitado editar dicho registro
		// Se agregan 5 segundos por cualquier eventualidad
		if ((now.getTime() - lockVerification.getTime()) / 1000 > VERIFY_TIME + 5) {
			remove = removeLock();
			if (remove) {
				return true;
			}
			throw remove;
		}
		if (object.lockConditional === false || object.lockConditional === 2) {
			return false;
		}
		var lockDate = new Date(object.lockDate);
		// Tiempo Máximo para editar: 5 Minutos
		// Si el usuario cumple 5 minutos a partir de cuando obtuvo el lock
		// se agrega lockConditional en true para solicitar al usuario si continuará
		// editando, si la respuesta es negativa se elimina el registro del lock
		// si no hay respuesta debido a que posiblemente el usuario cerró el navegador
		// o cambió de url, se agregan 30 segundos para verificar si no hubo respuesta
		// y si es mayor que este tiempo se procede a liberar el lock para el usuario
		// que lo solicita
		if ((now.getTime() - lockDate.getTime()) / 1000 > LOCK_TIME + 30) {
			remove = removeLock();
			if (remove) {
				return true;
			}
			throw remove;
		}
		return false;
	} catch (e) {
		$.trace.error(e);
		$.messageCodes.push({
			code: 'CORE009039',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	remueve el lock
	object = {
		id: Number || Array
	}
*/
this.removeLock = function(object) {
	try {
		object = object || util.getParam('object');
		if (typeof object === 'string') {
			object = JSON.parse(object);
		}
		if (object.id instanceof Array) {
			return object.id.reduce(function(resp, id) {
				return resp && fileSystem.deleteFileLock(id);
			}, true);
		}
		return fileSystem.deleteFileLock(object.id);
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE009044',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
};

/*
	Verifica si alguien a intentado acceder el archivo
	y si es así retorna el nombre del usuario que lo 
	intentó, la fecha y la fecha en que se bloqueó.
	Si no simplemente retorna true
	Pero si el usuario cumplió con el límite de tiempo
	de edición lo condiciona y retorna false
	object = {
		id: Integer || Array
	}
*/
this.verify = function(object) {
	try {
		var _self = this;
		object = object || $.request.parameters.get('object');
		if (typeof object === 'string') {
			object = JSON.parse(object);
		}
		var where = [];
		if (object.id instanceof Array) {
			var tmp = [];
			object.id.forEach(function(id) {
				tmp.push({
					field: 'id',
					oper: '=',
					value: id
				});
			});
			where.push(tmp);
		} else {
			where.push({
				field: 'id',
				oper: '=',
				value: object.id
			});
		}
		var locks = fileSystem.readFileLock({
			where: where
		});
		if (locks.length === 0) {
			throw 'Lock not found';
		}
		return locks.map(function(lock) {
			var now = new Date(Date.now());
			var creation = new Date(lock.lockDate);
			lock.lockVerification = now;
			_self.cleanLock(lock);
			fileSystem.saveFileLock(lock);
			var user = null;
			if (lock.userRequest !== null) {
				user = users.getUser(lock.userRequest);
				user = user.name + ' ' + user.last_name;
				lock.userRequest = null;
				lock.dateRequest = null;
				_self.cleanLock(lock);
				fileSystem.saveFileLock(lock);
			}
			if ((now.getTime() - creation.getTime()) / 1000 >= LOCK_TIME) {
				lock.lockConditional = true;
				_self.cleanLock(lock);
				fileSystem.saveFileLock(lock);
				return {
					id: lock.idObject,
					response: false,
					user: user
				};
			}
			return {
				id: lock.idObject,
				response: true,
				onExecute: lock.onExecute,
				user: user
			};
		});
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE009040',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		return null;
	}
};

/*
	Recibe la respuesta del usuario de si quiere
	continuar manteniendo el lock del archivo,
	si la respuesta es positiva, extiende el lock
	por el tiempo definido, de lo contrario se elimina
	el registro de bloqueo, liberando el archivo
	object = {
		id: Integer || Array,
		extend: Boolean
	}
*/
this.extendTime = function(object) {
	try {
		object = object || util.getParam('object');
		if (typeof object === 'string') {
			object = JSON.parse(object);
		}
		var _self = this;
		var where = [];
		if (object.id instanceof Array) {
			var tmp = [];
			object.id.forEach(function(id) {
				tmp.push({
					field: 'id',
					oper: '=',
					value: id
				});
			});
			where.push(tmp);
		} else {
			where.push({
				field: 'id',
				oper: '=',
				value: object.id
			});
		}
		var locks = fileSystem.readFileLock({
			where: where
		});
		if (locks.length) {
			return locks.map(function(lock) {
				if (object.extend === true) {
					var lockDate = new Date(Date.now());
					lock.lockDate = lockDate;
					lock.lockConditional = false;
					lock.lockVerification = lockDate;
					_self.cleanLock(lock);
					return fileSystem.saveFileLock(lock);
				}
				var remove = fileSystem.deleteFileLock(lock.id);
				if (remove === true) {
					return false;
				}
				throw remove;
			});
		}
		throw 'Lock not found';
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE009042',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		return null;
	}
};

/*
	Verifica si el lock continúa activo
	object = {
		id: Integer || Array
	}
*/
this.isLocked = function(object) {
	try {
		object = object || util.getParam('object');
		if (typeof object === 'string') {
			object = JSON.parse(object);
		}
		var where = [];
		if (object.id instanceof Array) {
			var tmp = [];
			object.id.forEach(function(id) {
				tmp.push({
					field: 'id',
					oper: '=',
					value: id
				});
			});
			where.push(tmp);
		} else {
			where.push({
				field: 'id',
				oper: '=',
				value: object.id
			});
		}
		var lock = fileSystem.readFileLock({
			where: where
		});
		return object.id instanceof Array ? object.id.length === lock.length : (lock.length > 0);
	} catch (e) {
		$.messageCodes.push({
			code: 'CORE009039',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		return null;
	}
};

this.cleanLock = function(lock) {
	delete lock.creationDate;
	delete lock.modificationDate;
	delete lock.creationUser;
	delete lock.modificationUser;
};

/**
 * Function only to check if client has active connection with server in case of 
 * server temporary downtime.
 */
this.verifyConnection = function() {
	$.response.status = 200;
	return true;
};