
//ALEX EDIT: no importing core.api inside core.*
//$.import('timp.core.server.api','api');
//var core_api = $.timp.core.server.api.api;
//var util = core_api.util;
//var tablesLabels = core_api.labels.table;

$.import('timp.core.server','util');
var util = $.timp.core.server.util;
$.import('timp.core.server.models', 'labels');
var tablesLabels = $.timp.core.server.models.labels.table;

var error = {};

// this.listLabels = function () {
// 	var objType = util.getParam('objType');
// 	var bond = util.getParam('bond');
// 	var response; 
	
// 	if(typeof objType != "undefined") {
//         //response = tablesLabels.getLabelsByObjType(objType);
//         response = tablesLabels.getLabelsByObjType(objType, bond);
//         if(response.length > 0) {
//            return
//         } else {
//             error.errorMsg = 'Couldn’t find labels with objType <' + objType + '>';
//            return
//         }
// 	} else {
//         error.errorMsg = 'No parameters used';
//        return
// 	}
// }

this.listLabels = function (object) {
    var objType = util.getParam('objType') || object.objType;
    
    if(typeof objType == 'string') {
        try {
            objType = JSON.parse(objType)
        } catch (e) {}
    }
    
    var bond = util.getParam('bond');
    var response;
    
    var labels = [];
    var label;
    if(typeof objType != "undefined") {
        response = tablesLabels.getLabelsByObjType(objType, bond);

        if(response.length > 0) {
            for (var y = 0; y < response.length; y++) {
                label = {};
                label.id = response[y].id;
                label.objType = response[y].objType;
                label.key = response[y].key;
                label.label = response[y].label;
                label.bond = response[y].bond;
                labels.push(label);
            }
            
            var objTypesOrderedByKey = [
                "DIA_ORDINAL",
                "DAY_TYPE",
                "DIA_SEMANA",
                "DAY_MONTH",
                "MES"
            ];
            
            if(objTypesOrderedByKey.indexOf(objType) !== -1){
                labels.sort(function(a, b){
                    return a.key - b.key;
                });
                // throw response;
            }
            
            return labels;
        } else {
            error.errorMsg = 'Couldn’t find labels with objType <' + objType + '>';
            return error;
        }
    } else {
        error.errorMsg = 'No parameters used';
        return error;
    }
}

this.listLabelsFromMultipleTypes = function (object) {
    // var object = util.getParam('object');
    
    // if(typeof object == 'string') {
    //     try {
    //         object = JSON.parse(object)
    //     } catch (e) {}
    // }
    
    var objTypes = object.objTypes;
    if(typeof objTypes != 'object') {
        error.errorMsg = 'Coundn’t find a parameter named objType that is an Array';
        return error;
    }
    
    var returnObj = {};
    
    for(var i = 0; i < objTypes.length; i++){
        returnObj[objTypes[i]] = tablesLabels.getLabelsByObjType(objTypes[i]);
    }
    
    return returnObj
}

this.listUfs = function (object) {

    var objType = 'UF';
    var removedUfs;
	var response;
    // var object = util.getParam('object');
    if(object){
        // object = JSON.parse(object);
        if(object.removedUfs){
            removedUfs = object.removedUfs;
        } else {
            removedUfs = [];
        }
    } else {
        removedUfs = [];
    }
        
    response = tablesLabels.getLabelsByObjType(objType);
    
    for (var i = 0; i < removedUfs.length; i++) {
        for (var j = 0; j < response.length; j++) {
            if(response[j].key.trim() == removedUfs[i].trim()) {
                //Remove the element from response array
                response.splice(j,1);
            }
        }
    }
    
    if(response.length > 0) {
        return response;
    } else {
        error.errorMsg = 'Couldn’t find labels with objType <' + objType + '>';
        return error;
    }
}