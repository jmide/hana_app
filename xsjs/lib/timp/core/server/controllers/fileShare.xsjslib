//IMPORTS
$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.controllers', 'fileShare');
var fileShare = $.timp.core.server.controllers.fileShare;

$.import('timp.core.server.models', 'fileSystem');
var folderModel = $.timp.core.server.models.fileSystem.folder;
var folderShareModel = $.timp.core.server.models.fileSystem.folderShare;
var fileModel = $.timp.core.server.models.fileSystem.file;
var fileShareModel = $.timp.core.server.models.fileSystem.fileShare;
var fileFavsModel = $.timp.core.server.models.fileSystem.fileFavs;

$.import('timp.core.server.models', 'objects');
var objectTypeModel = $.timp.core.server.models.objects.objectTypes;

$.import('timp.core.server.controllers', 'users');
var user = $.timp.core.server.controllers.users;

/* SHARE
   This function that creates a share of a file or a folder 
   options = {
   		fileId : 9   //optional but not null
		folderId : 8   //optional but not null
		userId :10	//optional but not null
		groupId : 3  //optional but not null
   }

   response.success = true |  false
*/
this.share = function(options) {
    options = options || JSON.parse($.request.parameters.get("object"));
    try {
        var shared = false;
        if (!options.hasOwnProperty("groupId")) {
            options.groupId = -1;
        }
        if (!options.hasOwnProperty("userId")) {
            options.userId = -1;
        }
        if(!options.hasOwnProperty("folderId")){
            if (!options.hasOwnProperty("component") || !options.hasOwnProperty("subComponent")) {
                return false;
            }
            if (!options.hasOwnProperty("status")) {
            return false;
        }
        }
       
        var objectTypeName = options.component + "::" + options.subComponent;
        var objectType = objectTypeModel.READ({
            where: [{
                field: "name",
                oper: "=",
                value: objectTypeName
            }]
        })[0];
        

        if (options.status === "Shared") {
            if(options.hasOwnProperty("fileId")){
                if (!shared) {
                    if (options.hasOwnProperty("userId") && options.userId.length) {
                        // Avoid DoS object attack
                        if ( options.userId.length > 1000 ) {
                            throw 'Request options.userId overflow';
                        }
                        //for atravez de  todos los ids 
                        for (var i = 0; i < options.userId.length; i++) {
                            var share = {};
                            share.idUser = options.userId[i];
                            share.objectType = objectType.id
                            if (options.hasOwnProperty('fileId') && options.fileId !== null) {
                                share.idFile = options.fileId;
                                
                                //check if file is already share to the user
                                var sharedFile = fileShareModel.READ({
                                    where: [{
                                        field: "idFile",
                                        oper: "=",
                                        value: options.fileId
                                    },{
                                        field: "idUser",
                                        oper: "=",
                                        value: options.userId[i]
                                    },{
                                        field: "idObjectType",
                                        oper: "=",
                                        value: objectType.id
                                    }]
                                });
                                if(sharedFile.length == 0){
                                    fileShareModel.CREATE(share);
                                }
                            }
                        }
                        shared = true;
                    }
                }
    
                if (!shared) {
                    if (options.hasOwnProperty("groupId") && options.groupId.length && !shared) {
                        // Avoid DoS object attack
                        if ( options.groupId.length > 1000 ) {
                            throw 'Request options.groupId overflow';
                        }
                        
                        for (var i = 0; i < options.groupId.length; i++) {
                            var share = {};
                            share.idGroup = options.groupId[i];
                            share.objectType = objectType.id
                            if (options.hasOwnProperty('fileId') && options.fileId !== null) {
                                share.idFile = options.fileId;
                                fileShareModel.CREATE(share);
                            }
                        }
                        shared = true;
                    }
                }
            }else if(options.hasOwnProperty("folderId")){
                if (!shared) {
                    var share = {};
                    if (options.hasOwnProperty("userId") && options.userId !== null) {
                        // share.idUser = options.userId;
                        var idUsers = options.userId;
                        
                        for(var j = 0; j<idUsers.length;j++){
                            var folderShare = folderShareModel.READ({
                                where:[{
                                    field: "idUser",
                                    oper: "=",
                                    value: idUsers[j]
                                },{
                                    field: "idFolder",
                                    oper: "=",
                                    value: options.folderId
                                }]
                            });
                            if(folderShare.length == 0){
                                share.idFolder = options.folderId;
                                share.idUser= idUsers[j];
                                folderShareModel.CREATE(share);
                            }
                        }
                    }
    
                    if (options.hasOwnProperty("groupId") && options.groupId !== null) {
                        // share.idGroup = options.groupId;
                        var idGroups = options.groupId;
                         for(var j = 0; j<idGroups.length;j++){
                             var folderShare = folderShareModel.READ({
                                where:[{
                                    field: "idGroup",
                                    oper: "=",
                                    value: idGroups[j]
                                },{
                                    field: "idFolder",
                                    oper: "=",
                                    value: options.folderId
                                }]
                            });
                            if(folderShare.length == 0){ 
                                share.idFolder = options.folderId;
                                share.idUser= idGroups[j];
                                folderShareModel.CREATE(share);
                            }
                        }
                    }
    
                    // if (options.hasOwnProperty("fileId") && options.fileId !== null) {
                    //     share.idFile = options.fileId;
                    //     //var response = fileShareModel.CREATE(share);
                    //     fileShareModel.CREATE(share);
                    //     //log("shared a file");
                    // }
                    
                        // share.idFolder = options.folderId;
                        // // share.objectType = objectType.id;
                        // folderShareModel.CREATE(share);
                        // //log("shared a folder");
                }
            }
        } else {
            if(options.status == "Active"){
                var favorites = fileFavsModel.READ({
                    where:[{
                        field: "idObjectType",
                        oper: "=",
                        value: objectType.id
                    },{
                        field: "idFile",
                        oper: "=",
                        value: options.fileId
                    },{
                        field: "idUser",
                        oper:"!=",
                        value: user.getTimpUser().id
                    }]
                })
                // Avoid DoS object attack
                if ( favorites.length > 5000 ) {
                    throw 'Request favorites overflow';
                }
                for(var _i = 0; _i < favorites.length; _i++){
                    fileFavsModel.DELETE(favorites[_i].id);
                }
                
            }
            var sharedFile = fileShareModel.READ({
                where: [{
                    field: "idFile",
                    oper: "=",
                    value: options.fileId
                }]
            });
            if (sharedFile.length > 0) {
                // Avoid DoS object attack
                if ( sharedFile.length > 5000 ) {
                    throw 'Request sharedFile overflow';
                }
                for (var i = 0; i < sharedFile.length; i++) {
                    var fileDelete = fileShareModel.DELETE(sharedFile[i].id);
                }
            }

            var updateFile = fileModel.UPDATEWHERE({
                status: options.status
            }, [{
                field: 'id',
                oper: '=',
                value: options.fileId
            }])
            return updateFile

        }
        return true;
    } catch (e) {
        return (util.parseError(e));
    }
};
this.unShare = function(options) {
    var share = {};
    var where = [];
    try {
        if (options.hasOwnProperty("userId") && options.userId !== null) {
            share.idUser = options.userId;
            where.push({
                field: "idUser",
                oper: "=",
                value: options.userId
            });
        }
        if (options.hasOwnProperty("groupId") && options.groupId !== null) {
            share.groupId = options.groupId;
            where.push({
                field: "idGroup",
                oper: "=",
                value: options.groupId
            });
        }

        if (options.hasOwnProperty("fileId") && options.fileId !== null) {
            share.idFile = options.fileId;
            where.push({
                field: "idFile",
                oper: "=",
                value: options.fileId
            });
            var file = fileShareModel.READ({
                where: where
            });
            //fileShareModel.DELETE(file[0].id);
            //log("shared a file");
        }
        if (options.hasOwnProperty("folderId") && options.folderId !== null) {
            share.idFolder = options.folderId;
            where.push({
                field: "idFolder",
                oper: "=",
                value: options.folderId
            });
            var folder = folderShareModel.READ({
                where: where
            });
            //folderShareModel.DELETE(folder[0].id);
            //log("shared a folder");
        }
    } catch (e) {
        return (util.parseError(e));
    }
};
this.listFolderShare = function() {

    try {
        var response = folderShareModel.READ();
        if (response.length > 0) {
            return response;
        } else {
            return response;
        }
    } catch (e) {
        return (util.parseError(e));
    }
};
this.listFileShare = function(options) {

    try {
        var objectType = objectTypeModel.READ({
            where: [{
                field: "name",
                oper: "=",
                value: options.objectType
            }]
        });

        var response = fileShareModel.READ({
            where: [{
                field: "idUser",
                oper: "=",
                value: user.getTimpUser().id
            }, {
                field: "idObjectType",
                oper: "=",
                value: objectType[0].id
            }],
            join: [{
                table: fileModel,
                alias: "file",
                on: [{
                    left: "idFile",
                    right: "id"
                }]
            }]
        });
        if (response.length > 0) {
            return response;
        } else {
            return response;
        }
    } catch (e) {
        return (util.parseError(e));
    }
};

this.listShareFilesCreationUser = function(options){
    var objectType = objectTypeModel.READ({
        where: [{
            field: "name",
            oper: "=",
            value: options.objectType
        }]
    });
    
    var response = fileShareModel.READ({
        distinct :true,
        fields : ['idFile'],
        where:[{
            field: "idObjectType",
            oper: "=",
            value: objectType[0].id
        },{
            field: "creationUser",
            oper: "=",
            value: user.getTimpUser().id
        }]
    });
    
    return response;
}