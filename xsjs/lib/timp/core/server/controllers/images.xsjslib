$.import('timp.core.server','util');
var util = $.timp.core.server.util;

$.import('timp.core.server.models', 'images');
var images_table = $.timp.core.server.models.images.images_table;

//ALEX EDIT - quitando import de api de core en cosas de core :P
//$.import('timp.core.server.api', 'api');
//var objects = $.timp.core.server.api.api.objects;
$.import('timp.core.server.models', 'objects');
var objects = $.timp.core.server.models.objects.objects;


this.PUT = function (id) {
    let errorGenerator = new $.ErrorGenerator('PUT');
    try {
        if ($.request.entities.hasOwnProperty(0)) {
    		var object = $.request.entities[0];
    		
    		var image_buffer = object.body.asArrayBuffer();
    
            if (util.isValid(id) && !isNaN(id)) {
                if(util.isString(id)) id = Number(id);
                var response = images_table.UPDATE({
                    id: id,
        			image: image_buffer
        		});
            } else {
                var response = images_table.CREATE({
        			image: image_buffer
        		});   
            }
    		
    		return (JSON.stringify(response));
    	} else {
    	    throw errorGenerator.generateError('00', '87');
    	}   
    } catch(e) {
        throw errorGenerator.generateError('00', '87', null, e);
    }
};

//start of decoding base64

this.b64ToUint6 = function(nChr) {

  return nChr > 64 && nChr < 91 ?
      nChr - 65
    : nChr > 96 && nChr < 123 ?
      nChr - 71
    : nChr > 47 && nChr < 58 ?
      nChr + 4
    : nChr === 43 ?
      62
    : nChr === 47 ?
      63
    :
      0;

};

//base64 to byte arrays

this.base64DecToArr = function(sBase64, nBlocksSize) {

  var
    sB64Enc = sBase64.replace(/[^A-Za-z0-9\+\/]/g, ""), nInLen = sB64Enc.length,
    nOutLen = nBlocksSize ? Math.ceil((nInLen * 3 + 1 >> 2) / nBlocksSize) * nBlocksSize : nInLen * 3 + 1 >> 2, taBytes = new Uint8Array(nOutLen);

  for (var nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
    nMod4 = nInIdx & 3;
    nUint24 |= this.b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
    if (nMod4 === 3 || nInLen - nInIdx === 1) {
      for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
        taBytes[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
      }
      nUint24 = 0;

    }
  }

  return taBytes;
};

//end of decoding base64

function UTF8ArrToStr (aBytes) {

  var sView = "";

  for (var nPart, nLen = aBytes.length, nIdx = 0; nIdx < nLen; nIdx++) {
    nPart = aBytes[nIdx];
    sView += String.fromCharCode(
      nPart > 251 && nPart < 254 && nIdx + 5 < nLen ? /* six bytes */
        /* (nPart - 252 << 30) may be not so safe in ECMAScript! So...: */
        (nPart - 252) * 1073741824 + (aBytes[++nIdx] - 128 << 24) + (aBytes[++nIdx] - 128 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128
      : nPart > 247 && nPart < 252 && nIdx + 4 < nLen ? /* five bytes */
        (nPart - 248 << 24) + (aBytes[++nIdx] - 128 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128
      : nPart > 239 && nPart < 248 && nIdx + 3 < nLen ? /* four bytes */
        (nPart - 240 << 18) + (aBytes[++nIdx] - 128 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128
      : nPart > 223 && nPart < 240 && nIdx + 2 < nLen ? /* three bytes */
        (nPart - 224 << 12) + (aBytes[++nIdx] - 128 << 6) + aBytes[++nIdx] - 128
      : nPart > 191 && nPart < 224 && nIdx + 1 < nLen ? /* two bytes */
        (nPart - 192 << 6) + aBytes[++nIdx] - 128
      : /* nPart < 127 ? */ /* one byte */
        nPart
    );
  }

  return sView;

}

function uint6ToB64 (nUint6) {

  return nUint6 < 26 ?
      nUint6 + 65
    : nUint6 < 52 ?
      nUint6 + 71
    : nUint6 < 62 ?
      nUint6 - 4
    : nUint6 === 62 ?
      43
    : nUint6 === 63 ?
      47
    :
      65;

}

function base64EncArr (aBytes) {

  var nMod3 = 2, sB64Enc = "";

  for (var nLen = aBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
    nMod3 = nIdx % 3;
    if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
    nUint24 |= aBytes[nIdx] << (16 >>> nMod3 & 24);
    if (nMod3 === 2 || aBytes.length - nIdx === 1) {
      sB64Enc += String.fromCharCode(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
      nUint24 = 0;
    }
  }

  return sB64Enc.substr(0, sB64Enc.length - 2 + nMod3) + (nMod3 === 2 ? '' : nMod3 === 1 ? '=' : '==');

}


//check id?
this.PUT64 = function (object) {
    let errorGenerator = new $.ErrorGenerator('PUT64');
    try {
        if (object) {
            var dataString = object.file;
            
    		var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    
            if (matches.length !== 3) {
                return new Error('Invalid input string');
            }
    		
    		var image_buffer = this.base64DecToArr(matches[2]).buffer;
    		//object.body.asArrayBuffer();
    
                var response = images_table.CREATE({
        			image: image_buffer
        		});   
    		
    		return (JSON.stringify(response));
    	} else {
    	    throw errorGenerator.generateError('00', '87');
    	}   
    } catch(e) {
        throw errorGenerator.generateError('00', '87', null, e);
    }
};

this.GET = function (id) {
    let errorGenerator = new $.ErrorGenerator('GET');
    try {
        if(!isNaN(id)) {
            var lines = images_table.READ({
        	    where: [{field: 'id', oper: '=', value: Number(id)}]
        	});
            if(lines) {
                $.response.contentType = 'document';
                if ('_setBody' in $.response && typeof $.response._setBody === 'function') {
                    $.response._setBody(lines[0].image);
                } else {
                    return (lines[0].image);
                }
            } else {
                throw errorGenerator.generateError('00', '06');
            }
        }
        throw errorGenerator.generateError('00', '06');
    } catch(e) {
        throw errorGenerator.generateError('00', '99', null, e);
    }
};

this.GET64 = function (id) {
    let errorGenerator = new $.ErrorGenerator('GET64');
    try {
        if(!isNaN(id)) {
            var lines = images_table.READ({
        	    where: [{field: 'id', oper: '=', value: Number(id)}]
        	});
            if(lines) {
                var arr = new Uint8Array(lines[0].image);
                return base64EncArr(arr);
            } else {
                throw errorGenerator.generateError('00', '06');
            }
        }
        throw errorGenerator.generateError('00', '06');
    } catch(e) {
        throw errorGenerator.generateError('00', '99', null, e);
    }
};

this.DELETE = function (id) {
    let errorGenerator = new $.ErrorGenerator('DELETE');
    try {
        if(!isNaN(id)) {
            var lines = images_table.DELETE(id);
            
            if(lines) {
                return (lines);
            } else {
                throw errorGenerator.generateError('00', '06');
            }
        } else {
            throw errorGenerator.generateError('00', '06');
        }   
    } catch (e) {
        throw errorGenerator.generateError('00', '89', null, e);
    }
};

this.LIST = function () {
    let errorGenerator = new $.ErrorGenerator('LIST');
    try {
        var lines = images_table.READ({
            fields: ['id','creationDate','creationUser','modificationDate','modificationUser']
        });
        
        for (var i = 0; i < lines.length; i++) {
            lines[i].url = '/timp/core/server/endpoint.xsjs/images/get/'+lines[i].id+'/';
        }
        
        return (JSON.stringify(lines));   
    } catch(e) {
        throw errorGenerator.generateError('00', '99', null, e);
    }
};

this.list = function (){
    let errorGenerator = new $.ErrorGenerator('list');
    try {
        var response = objects.listObjects(images_table, "CORE::Images");
        return (response);
    } catch(e) {
        throw errorGenerator.generateError('00', '99', null, e);
    }
};

this.create = function (){
    
}

this.update = function (){
    
}

this.read = function (){
    
}