$.import('timp.core.server', 'util');
var util = $.timp.core.server.util;

$.import('timp.core.server.orm', 'sql');
var sql = $.timp.core.server.orm.sql;

$.import('timp.core.server.controllers', 'users');
var user = $.timp.core.server.controllers.users;

$.import('timp.core.server.models', 'fileSystem');
var folderModel = $.timp.core.server.models.fileSystem.folder;
var folderShareModel = $.timp.core.server.models.fileSystem.folderShare;
var fileModel = $.timp.core.server.models.fileSystem.file;
var fileShareModel = $.timp.core.server.models.fileSystem.fileShare;
var fileFavsModel = $.timp.core.server.models.fileSystem.fileFavs;

$.import('timp.core.server.models', 'objects');
var objectTypesModel = $.timp.core.server.models.objects.objectTypes;

$.import('timp.core.server.models', 'schema');
var schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;


//CONSTANTS 
this.statusTypes = {
	STANDARD : 0,
	ACTIVE : 1,
	TRASH : 2,
	DELETED : 3
};

this.folderCats = {
	STANDARD : 0,
	PUBLIC : 1,
	FAVORITE : 2,
	MYFOLDERS: 3,
	SHARED : 4
};

function getOptions() {
 
    var options = util.getParam('object');
    
    while(typeof options == 'string') {
        options = JSON.parse(options);
    }
    
    if (typeof options == 'undefined') options = {};
    return options;
}

this.getFolderItems = function(){
    try{
        var options = getOptions();
    	var response;
    	if (options.folderCategory=="this.folderCats.PUBLIC"){ 
    	    response = this.getPublicFolder(options);
    	    }else if (options.folderCategory=="this.folderCats.FAVORITE"){
    	    	response = this.getFavoriteFolder(options);
    	    }else if (options.folderCategory=="this.folderCats.STANDARD"){
    	        response = this.getStandardFolder(options);
    	    }else if (options.folderCategory=="this.folderCats.SHARED"){
    	        response = this.getSharedFolder(options);
    	    }else if (options.folderCategory=="this.folderCats.MYFOLDERS"){
    	        response = this.getMyFolders(options);
    	}else{
    	   response = this.getFolder(options); 
    	}
    	return response;
    }catch(e){
        throw 'error: '+ e;
    }
}

this.getUserGroups = function(id){
    
    try{
        $.import('timp.mkt.server.api','api');
        var mkt_api = timp.mkt.server.api.api;
        var users_groupsModel = mkt_api.tables.users_groups;
        
        var where = [];
        where.push({"field": "id_user", "value": id, "oper": "="});
        var options = {
            where:where
        };
        
        var result = users_groupsModel.READ(options);
        
        var response = [];
        for(var i = 0; i< result.length; i++){
            response.push(result[i].id_group);
        }
        
        return response;
        
    }catch(e){
        throw e;
    }
}

this.getFolder = function(options){
    try{
        
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        
        //GET ROOT FOLDER
        var subFolders = [];
        var where = [];
        var valueWhere = -1;
        
        var joinFolders = [{
                fields: [],
                alias: 'folderShare',
                table: this.folderShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFolder'
                    },
                    {
                        field: "idGroup", 
                        value: valueWhere, 
                        oper: "="
                    }
                ]
            }
        ];
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            where.push({"field": "idParent", "value": options.folderId, "oper": "="});
        }
        
        var optionsFolder = {
            fields:['id','name','description','status','idParent'],
            join:joinFolders,
            where:where
        };
        
        var resultFolder = folderModel.READ(optionsFolder);
        
        var folders = {};
        
        for(var i = 0; i < resultFolder.length; i++){
            folders[resultFolder[i].id] = resultFolder[i];
        }
        for(var reg in folders){
            for(var prop in folders[reg]){
                if(prop == 'idParent'){
                    folders[reg].id_parent = folders[reg].idParent;
                    delete folders[reg].idParent;
                }
            }
        }
        
        for(var folder in folders){
            if(folders[folder].idParent == null){
                subFolders.push(folders[folder]);
            }
        }
        
        response.folders = subFolders;
        
        //GET PUBLIC FILES
        
        var joinFiles = [
            {
                fields: [],
                alias: 'fileShare',
                table: this.fileShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFile'
                    }
                ]
            }
        ];
        
        if(userGroupsIDs.length > 0){
            joinFiles[0].on.push([[[{field: 'idUser', value: userID, oper: '='},{field: 'idGroup', value: userGroupsIDs, oper: '='}]]]);
        }else{
            joinFiles[0].on.push({field: 'idUser', value: userID, oper: '='});
        }
        
        joinFiles.push(   
            {
                fields: [],
                alias: 'folder',
                table: this.folderModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idFolder',
                        right: 'id'
                    }
                ]
            }
        );
        
        joinFiles.push(
            {
                fields: ['name'],
                alias: 'objectTypes',
                table: this.objectTypesModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idObjectType',
                        right: 'id'
                    }
                ]
            }
        );
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
           joinFiles[2].on.push({field:'name', value:options.objectType, oper: '='});
        }
        
        var whereFiles = [];
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            whereFiles.push({field:'status', value:options.status, oper: '='});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFiles.push({"field": "idFolder", "value": options.folderId, "oper": "="});
        }
        
        var optionsFiles = {
            fields:['id','status','idFolder','idObjectType'],
            join:joinFiles,
            where:whereFiles
        };
        
        var resultFile = fileModel.READ(optionsFiles);
        
        for(var reg2 in resultFile){
            for(var prop2 in resultFile[reg2]){
                if(prop2 == 'idFolder'){
                    resultFile[reg2].id_folder = resultFile[reg2].idFolder;
                }
                if(prop2 == 'idObjectType'){
                    resultFile[reg2].id_objectType = resultFile[reg2].idObjectType;
                }
                if(prop2 == 'objectTypes'){
                    var optionsOT = {
                        where:[{field:'id', value: resultFile[reg2].idObjectType, oper:'='}]
                    }
                    
                    resultFile[reg2].objectType = {
                        id:resultFile[reg2].idObjectType,
                        name: objectTypesModel.READ(optionsOT)[0].name
                    }
                }
            }
            delete resultFile[reg2].idFolder;
            delete resultFile[reg2].idObjectType;
            delete resultFile[reg2].objectTypes;
        }
        
        response.files = resultFile;
        return response;
        
    }catch(e){
        throw 'error: '+ util.parseError(e);
    }
}

this.getPublicFolder = function(options){
    try{
        
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        
        //GET ROOT FOLDER
        var publicFolders = [];
        var where = [];
        var valueWhere = -1;
        
        var joinFolders = [{
                fields: [],
                alias: 'folderShare',
                table: this.folderShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFolder'
                    },
                    {
                        field: "idGroup", 
                        value: valueWhere, 
                        oper: "="
                    }
                ]
            }
        ];
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            where.push({"field": "idParent", "value": options.folderId, "oper": "="});
        }
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFolders = [];
            for(var c = 0; c< options.status.length;c++){
                statusFolders.push(eval(options.status[c]));
            }
            where.push({"field": "status", "value": statusFolders, "oper": "="});
        }
        
        var optionsFolder = {
            fields:['id','name','description','status','idParent'],
            join:joinFolders,
            where:where
        };
        
        var resultFolder = folderModel.READ(optionsFolder);
        
        var folders = {};
        
        for(var i = 0; i < resultFolder.length; i++){
            folders[resultFolder[i].id] = resultFolder[i];
        }
        for(var reg in folders){
            for(var prop in folders[reg]){
                if(prop == 'idParent'){
                    folders[reg].id_parent = folders[reg].idParent;
                    delete folders[reg].idParent;
                }
            }
        }
        
        var idPublics = [];
        for(var folder in folders){
            if(folders[folder].idParent == null){
                publicFolders.push(folders[folder]);
                idPublics.push(folders[folder].id);
            }
        }
        
        response.folders = publicFolders;
        
        //GET PUBLIC FILES
        
        var joinFiles = [
            {
                fields: [],
                alias: 'fileShare',
                table: this.fileShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFile'
                    },
                    {
                        field: 'idGroup',
                        value: -1,
                        oper: '='
                    }
                ]
            }
        ];
        
        joinFiles.push(   
            {
                fields: [],
                alias: 'folder',
                table: this.folderModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idFolder',
                        right: 'id'
                    }
                ]
            }
        );
        
        joinFiles.push(
            {
                fields: ['name'],
                alias: 'objectTypes',
                table: this.objectTypesModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idObjectType',
                        right: 'id'
                    }
                ]
            }
        );
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
           joinFiles[2].on.push({field:'name', value:options.objectType, oper: '='});
        }
        
        var whereFiles = [];
        
        for(var b = 0;b<idPublics.length;b++){
            whereFiles.push({"field": "idFolder", "value": idPublics[b], "oper": "!="});
        }
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFiles = [];
            for(var c = 0; c< options.status.length;c++){
                statusFiles.push(eval(options.status[c]));
            }
            whereFiles.push({"field": "status", "value": statusFiles, "oper": "="});
        }
        
        // if(options.hasOwnProperty('status') && options.status.length > 0){
        //     whereFiles.push({field:'status', value:options.status, oper: '='});
        // }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFiles.push({"field": "idFolder", "value": options.folderId, "oper": "="});
        }
        
        var optionsFiles = {
            fields:['id','status','idFolder','idObjectType'],
            join:joinFiles,
            where:whereFiles
        };
        
        var resultFile = fileModel.READ(optionsFiles);
        
        for(var reg2 in resultFile){
            for(var prop2 in resultFile[reg2]){
                if(prop2 == 'idFolder'){
                    resultFile[reg2].id_folder = resultFile[reg2].idFolder;
                }
                if(prop2 == 'idObjectType'){
                    resultFile[reg2].id_objectType = resultFile[reg2].idObjectType;
                }
                if(prop2 == 'objectTypes'){
                    var optionsOT = {
                        where:[{field:'id', value: resultFile[reg2].idObjectType, oper:'='}]
                    }
                    
                    resultFile[reg2].objectType = {
                        id:resultFile[reg2].idObjectType,
                        name: objectTypesModel.READ(optionsOT)[0].name
                    }
                }
            }
            delete resultFile[reg2].idFolder;
            delete resultFile[reg2].idObjectType;
            delete resultFile[reg2].objectTypes;
        }
        
        response.files = resultFile;
        return response;
        
    }catch(e){
        throw 'error: '+ util.parseError(e);
    }
}

this.getFavoriteFolder = function(options){
    try{
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        
        //GET FILES
        
        var joinFiles = [
            {
                fields: [],
                alias: 'fileShare',
                table: this.fileShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFile'
                    }
                ]
            }
        ];
        
        if(userGroupsIDs.length > 0){
            joinFiles[0].on.push([[[{field: 'idUser', value: userID, oper: '='},{field: 'idGroup', value: userGroupsIDs, oper: '='}]]]);
        }else{
            joinFiles[0].on.push({field: 'idUser', value: userID, oper: '='});
        }
                
        joinFiles.push(   
            {
                fields: [],
                alias: 'fileFavs',
                table: this.fileFavsModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'id',
                        right: 'idFile'
                    },
                    {
                        field: 'idUser',
                        value: userID,
                        oper: '='
                    }
                ]
            }
        );
        
        joinFiles.push(
            {
                fields: ['name'],
                alias: 'objectTypes',
                table: this.objectTypesModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idObjectType',
                        right: 'id'
                    }
                ]
            }
        );
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
            joinFiles[2].on.push({field:'name', value:options.objectType, oper: '='});
        }
        
        var whereFiles = [];
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFiles = [];
            for(var c = 0; c< options.status.length;c++){
                statusFiles.push(eval(options.status[c]));
            }
            whereFiles.push({"field": "status", "value": statusFiles, "oper": "="});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFiles.push({"field": "idFolder", "value": options.folderId, "oper": "="});
        }
        
        var optionsFiles = {
            fields:['id','status','idFolder','idObjectType'],
            join:joinFiles,
            where:whereFiles
        };
        
        var resultFile = fileModel.READ(optionsFiles);
        
        for(var reg2 in resultFile){
            for(var prop2 in resultFile[reg2]){
                if(prop2 == 'idFolder'){
                    resultFile[reg2].id_folder = resultFile[reg2].idFolder;
                }
                if(prop2 == 'idObjectType'){
                    resultFile[reg2].id_objectType = resultFile[reg2].idObjectType;
                }
                if(prop2 == 'objectTypes'){
                    var optionsOT = {
                        where:[{field:'id', value: resultFile[reg2].idObjectType, oper:'='}]
                    }
                    
                    resultFile[reg2].objectType = {
                        id:resultFile[reg2].idObjectType,
                        name: objectTypesModel.READ(optionsOT)[0].name
                    }
                }
            }
        
            delete resultFile[reg2].idFolder;
            delete resultFile[reg2].idObjectType;
            delete resultFile[reg2].objectTypes;
            
        }
        
        response.folders = [];
        response.files = resultFile;
        return response;
        
    }catch(e){
        throw 'error: '+ util.parseError(e);
    }
}

this.getStandardFolder = function(options){
    try{
        
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        
        //GET ROOT FOLDER
        var standardFolders = [];
        var where = [];
        
        where.push({"field": "status", "value": this.statusTypes.STANDARD, "oper": "="});
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            where.push({"field": "idParent", "value": options.folderId, "oper": "="});
        }
        
        var optionsFolder = {
            fields:['id','name','description','status','idParent'],
            where:where
        };
        
        var resultFolder = folderModel.READ(optionsFolder);
        
        var folders = {};
        
        for(var i = 0; i < resultFolder.length; i++){
            folders[resultFolder[i].id] = resultFolder[i];
        }
        
        for(var reg in folders){
            for(var prop in folders[reg]){
                if(prop == 'idParent'){
                    folders[reg].id_parent = folders[reg].idParent;
                    delete folders[reg].idParent;
                }
            }
        }
        
        for(var folder in folders){
            if(folders[folder].idParent == null){
                standardFolders.push(folders[folder]);
            }
        }
        
        response.folders = standardFolders;
        response.files = [];
        return response;
        
    }catch(e){
        throw 'error: '+ util.parseError(e);
    }
}

this.getSharedFolder = function(options){
    
    try{
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        var executeFolder = {};
        var queryFolder = "";
        
        //get all the folders shared with me  but counting the total shares
        //Foi necessário usar o sql.SELECT devido ao ORM ainda não suportar count em campos de tabelas de JOIN
        queryFolder = 'SELECT f.ID, f.NAME, f.DESCRIPTION, f.STATUS, f.ID_PARENT , count(fs.ID_USER) as totalShares ';
        queryFolder += 'FROM '+ schema.default +'."CORE::Folder" as f ';
        queryFolder += 'JOIN(';
        
        var selectJoin = 'SELECT uf.ID FROM '+ schema.default +'."CORE::Folder" as uf JOIN '+ schema.default +'."CORE::FolderShare" as ufs ON uf.ID = ufs.ID_FOLDER ';
        selectJoin += 'WHERE ufs.ID_USER = ' + userID + ' ';
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFolder = [];
            selectJoin += 'AND uf.STATUS IN (';
            var first = true;
            for(var c = 0; c< options.status.length;c++){
                if(first ==  false){
                    selectJoin += ', ';   
                }
                selectJoin += eval(options.status[c]);
                first = false;
            }
            selectJoin += ') ';
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            selectJoin += 'AND uf.ID_PARENT = ' + options.folderId;
        }
        
        queryFolder = queryFolder + selectJoin;
        queryFolder += ') AS s ';
        queryFolder += 'ON f.ID = s.ID JOIN '+ schema.default +'."CORE::FolderShare" as fs ON s.ID = fs.ID_FOLDER ';
        queryFolder += 'GROUP BY f.ID, f.NAME, f.DESCRIPTION, f.STATUS, f.ID_PARENT';
        
        executeFolder.query = queryFolder;
        var resultFolders = sql.SELECT(executeFolder);
        
        var folders = [];
        var idFolders = [];
        for(var a = 0; a < resultFolders.length; a++){
            var obj = {
                id:resultFolders[a][0],
                name:resultFolders[a][1],
                description:resultFolders[a][2],
                status:resultFolders[a][3],
                id_parent:resultFolders[a][4],
                totalShares:parseInt(resultFolders[a][5])
            };
            folders.push(obj);
            idFolders.push(resultFolders[a][0]);
        }
        
        //get all folder shared with my groups
        
        var joinGroupFolder = [
            {
                fields:[],
                alias: 'folderShare',
                table: this.folderShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFolder'
                    },
                    {
                        field: 'idGroup', 
                        value: userGroupsIDs, 
                        oper: '='
                    }
                ]            
            }
        ];
        
        var whereFolderGroup = [];
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFolderGroup = [];
            for(var c = 0; c< options.status.length;c++){
                statusFolderGroup.push(eval(options.status[c]));
            }
            whereFolderGroup.push({"field": "status", "value": statusFolderGroup, "oper": "="});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFolderGroup.push({"field": "idParent", "value": options.folderId, "oper": "="});
        }
        
        var read_optionsFolder = {
            fields:['id','name','description','status','idParent'],
            join:joinGroupFolder,
            where:whereFolderGroup
        };
        
        var resultGroupFolder = folderModel.READ(read_optionsFolder);
        
        for(var n in resultGroupFolder){
            resultGroupFolder[n].totalShares = 2;
            folders.push(resultGroupFolder[n]);
        }
        
        for(var reg in folders){
            for(var prop in folders[reg]){
                if(prop == 'idParent'){
                    folders[reg].id_parent = folders[reg].idParent;
                    delete folders[reg].idParent;
                }
            }
        }
        
        var sharedFolders = [];
        for(var m in folders){
            if(folders[m].id_parent == null && folders[m].totalShares > 1){
                sharedFolders.push(folders[m]);
            }
        }
        
        var idAllFolders = [];
        for(var o in  sharedFolders){
            idAllFolders.push(sharedFolders[o].id);
        }
        
        response.folders = sharedFolders;
        
        //get all the files shared with me  but counting the total sharesai 
        var executeFile = {};
        var queryFile = 'SELECT f.ID, f.STATUS, f.ID_FOLDER, f.ID_OBJECT_TYPE, count(fs.ID_USER) as totalShares ';
        queryFile += 'FROM '+ schema.default +'."CORE::File" as f ';
        queryFile += 'JOIN(';
        
        var selectJoinFile = 'SELECT uf.ID, ot.NAME FROM '+ schema.default +'."CORE::File" as uf JOIN '+ schema.default +'."CORE::FileShare" as ufs ON uf.ID = ufs.ID_FILE ';
        selectJoinFile += 'JOIN '+ schema.default +'."CORE::ObjectType" as ot ON uf.ID_OBJECT_TYPE = ot.ID ';
        selectJoinFile += 'WHERE ufs.ID_USER = ' + userID + ' ';
        selectJoinFile += 'AND uf.ID_FOLDER NOT IN (';
        
        var first2 = true;
        for(var d = 0; d< idAllFolders.length;d++){
            if(first2 ==  false){
                selectJoinFile += ', ';   
            }
            selectJoinFile += idAllFolders[d];
            first2 = false;
        }
        
        selectJoinFile += ') ';
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            selectJoinFile  += 'AND uf.STATUS IN (';
            var first3 = true;
            for(var e = 0; e< options.status.length;e++){
                if(first3 ==  false){
                    selectJoinFile += ', ';   
                }
                selectJoinFile += eval(options.status[e]);
                first3 = false;
            }
            selectJoinFile += ') ';
        }
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
            
            selectJoinFile  += 'AND ot.NAME IN (';
            var first4 = true;
            for(var f = 0; f< options.objectType.length;f++){
                if(first4 ==  false){
                    selectJoinFile += ', ';   
                }
                selectJoinFile += "'" + options.objectType[f] + "'";
                first4 = false;
            }
            selectJoinFile += ') ';
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            selectJoinFile += 'AND uf.ID_FOLDER = ' + options.folderId;
        }
        
        queryFile += selectJoinFile;
        
        queryFile += ') AS s ';
        queryFile += 'ON f.ID = s.ID JOIN '+ schema.default +'."CORE::FileShare" as fs ON s.ID = fs.ID_FILE ';
        queryFile += 'GROUP BY f.ID, f.STATUS, f.ID_FOLDER, f.ID_OBJECT_TYPE';
        
        executeFile.query = (queryFile);
        
        var resultFiles = sql.SELECT(executeFile);
        var files = [];
        for(var a = 0; a < resultFiles.length; a++){
            var optionsOT = {
                where:[{field:'id', value: resultFiles[a][3], oper:'='}]
            };
            var obj = {
                id:resultFiles[a][0],
                status:resultFiles[a][1],
                id_folder:resultFiles[a][2],
                id_objectType:resultFiles[a][3],
                objectType:{
                    id:resultFolders[a][3],
                    name:objectTypesModel.READ(optionsOT)[0].name
                },
                totalShares:parseInt(resultFiles[a][4])
            };
            files.push(obj);
        }
        
        //get all the files shared with my groups
        
        var joinGroupFile = [
            {
                fields:[],
                alias: 'fileShare',
                table: this.fileShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFile'
                    },
                    {
                        field: 'idGroup', 
                        value: userGroupsIDs, 
                        oper: '='
                    }
                ]            
            },
            {
                fields: [],
                alias: 'objectTypes',
                table: this.objectTypesModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idObjectType',
                        right: 'id'
                    }
                ]
            }
        ];
        
        var whereFileGroup = [];
        
        for(var q = 0;q<idAllFolders.length;q++){
            whereFileGroup.push({"field": "idFolder", "value": idAllFolders[q], "oper": "!="});    
        }
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFileGroup = [];
            for(var c = 0; c< options.status.length;c++){
                statusFileGroup.push(eval(options.status[c]));
            }
            whereFileGroup.push({"field": "status", "value": statusFileGroup, "oper": "="});
        }
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
            joinGroupFile[1].on.push({field:'name', value:options.objectType, oper: '='});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFileGroup.push({"field": "idFolder", "value": options.folderId, "oper": "="});
        }
        
        var read_optionsFile = {
            fields:['id','status','idFolder','idObjectType'],
            join:joinGroupFile,
            where:whereFileGroup
        };
        
        var resultGroupFile = fileModel.READ(read_optionsFile);
        
        for(var n in resultGroupFile){
            resultGroupFile[n].totalShares = 2;
            files.push(resultGroupFile[n]);
        }
        
        for(var reg in files){
            var optionsOT = {
                where:[{field:'id',value:files[reg].idObjectType,oper:'='}]
            };
            for(var prop in files[reg]){
                if(prop == 'idFolder'){
                    files[reg].id_folder = files[reg].idFolder;
                }
                if(prop == 'idObjectType'){
                    files[reg].id_objectType = files[reg].idObjectType;
                }
            }
            if(!files[reg].hasOwnProperty('objectType')){
                files[reg].objectType = {
                    id:files[reg].idObjectType,
                    name:objectTypesModel.READ(optionsOT)[0].name
                }
            }
            if(!files[reg].hasOwnProperty('totalShares')){
                files[reg].totalShares = 2;
            }
            delete files[reg].idFolder;
            delete files[reg].idObjectType;
        }
        
        var sharedFiles = [];
        for(var r in files){
            if(files[r].totalShares > 1){
                sharedFiles.push(files[r]);
            }
        }
        
        response.files = sharedFiles;
            
        return response;
        
    }catch(e){
        throw util.parseError(e);
    }
    
}

this.getMyFolders = function(options){
    try{
        var response = {};
        var userID = user.getTimpUser().id;
        var userGroupsIDs = this.getUserGroups(userID);
        var executeFolder = {};
        var queryFolder = "";
        
        //get all the folders shared with me  but counting the total shares
        //Foi necessário usar o sql.SELECT devido ao ORM ainda não suportar count em campos de tabelas de JOIN
        queryFolder = 'SELECT f.ID, f.NAME, f.DESCRIPTION, f.STATUS, f.ID_PARENT , count(fs.ID_USER) as totalShares ';
        queryFolder += 'FROM '+ schema.default +'."CORE::Folder" as f ';
        queryFolder += 'JOIN(';
        
        var selectJoin = 'SELECT uf.ID FROM '+ schema.default +'."CORE::Folder" as uf JOIN '+ schema.default +'."CORE::FolderShare" as ufs ON uf.ID = ufs.ID_FOLDER ';
        selectJoin += 'WHERE ufs.ID_USER = ' + userID + ' ';
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFolder = [];
            selectJoin += 'AND uf.STATUS IN (';
            var first = true;
            for(var c = 0; c< options.status.length;c++){
                if(first ==  false){
                    selectJoin += ', ';   
                }
                selectJoin += eval(options.status[c]);
                first = false;
            }
            selectJoin += ') ';
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            selectJoin += 'AND uf.ID_PARENT = ' + options.folderId;
        }
        
        queryFolder = queryFolder + selectJoin;
        queryFolder += ') AS s ';
        queryFolder += 'ON f.ID = s.ID JOIN '+ schema.default +'."CORE::FolderShare" as fs ON s.ID = fs.ID_FOLDER ';
        queryFolder += 'GROUP BY f.ID, f.NAME, f.DESCRIPTION, f.STATUS, f.ID_PARENT';
        
        executeFolder.query = queryFolder;
        var resultFolders = sql.SELECT(executeFolder);
        
        var folders = [];
        var idFolders = [];
        for(var a = 0; a < resultFolders.length; a++){
            var obj = {
                id:resultFolders[a][0],
                name:resultFolders[a][1],
                description:resultFolders[a][2],
                status:resultFolders[a][3],
                id_parent:resultFolders[a][4],
                totalShares:parseInt(resultFolders[a][5])
            };
            folders.push(obj);
            idFolders.push(resultFolders[a][0]);
        }
        
        //get all folder shared with my groups
        
        var joinGroupFolder = [
            {
                fields:[],
                alias: 'folderShare',
                table: this.folderShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFolder'
                    },
                    {
                        field: 'idGroup', 
                        value: userGroupsIDs, 
                        oper: '='
                    }
                ]            
            }
        ];
        
        var whereFolderGroup = [];
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFolderGroup = [];
            for(var c = 0; c< options.status.length;c++){
                statusFolderGroup.push(eval(options.status[c]));
            }
            whereFolderGroup.push({"field": "status", "value": statusFolderGroup, "oper": "="});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFolderGroup.push({"field": "idParent", "value": options.folderId, "oper": "="});
        }
        
        var read_optionsFolder = {
            fields:['id','name','description','status','idParent'],
            join:joinGroupFolder,
            where:whereFolderGroup
        };
        
        var resultGroupFolder = folderModel.READ(read_optionsFolder);
        
        for(var n in resultGroupFolder){
            resultGroupFolder[n].totalShares = 2;
            folders.push(resultGroupFolder[n]);
        }
        
        for(var reg in folders){
            for(var prop in folders[reg]){
                if(prop == 'idParent'){
                    folders[reg].id_parent = folders[reg].idParent;
                    delete folders[reg].idParent;
                }
            }
        }
        
        var sharedFolders = [];
        for(var m in folders){
            if(folders[m].id_parent == null && folders[m].totalShares === 1){
                sharedFolders.push(folders[m]);
            }
        }
        
        var idAllFolders = [];
        for(var o in  sharedFolders){
            idAllFolders.push(sharedFolders[o].id);
        }
        
        response.folders = sharedFolders;
        
        //get all the files shared with me  but counting the total sharesai 
        var executeFile = {};
        var queryFile = 'SELECT f.ID, f.STATUS, f.ID_FOLDER, f.ID_OBJECT_TYPE, count(fs.ID_USER) as totalShares ';
        queryFile += 'FROM '+ schema.default +'."CORE::File" as f ';
        queryFile += 'JOIN(';
        
        var selectJoinFile = 'SELECT uf.ID, ot.NAME FROM '+ schema.default +'."CORE::File" as uf JOIN '+ schema.default +'."CORE::FileShare" as ufs ON uf.ID = ufs.ID_FILE ';
        selectJoinFile += 'JOIN '+ schema.default +'."CORE::ObjectType" as ot ON uf.ID_OBJECT_TYPE = ot.ID ';
        selectJoinFile += 'WHERE ufs.ID_USER = ' + userID + ' ';
        selectJoinFile += 'AND uf.ID_FOLDER NOT IN (';
        
        var first2 = true;
        for(var d = 0; d< idAllFolders.length;d++){
            if(first2 ==  false){
                selectJoinFile += ', ';   
            }
            selectJoinFile += idAllFolders[d];
            first2 = false;
        }
        
        selectJoinFile += ') ';
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            selectJoinFile  += 'AND uf.STATUS IN (';
            var first3 = true;
            for(var e = 0; e< options.status.length;e++){
                if(first3 ==  false){
                    selectJoinFile += ', ';   
                }
                selectJoinFile += eval(options.status[e]);
                first3 = false;
            }
            selectJoinFile += ') ';
        }
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
            
            selectJoinFile  += 'AND ot.NAME IN (';
            var first4 = true;
            for(var f = 0; f< options.objectType.length;f++){
                if(first4 ==  false){
                    selectJoinFile += ', ';   
                }
                selectJoinFile += "'" + options.objectType[f] + "'";
                first4 = false;
            }
            selectJoinFile += ') ';
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            selectJoinFile += 'AND uf.ID_FOLDER = ' + options.folderId;
        }
        
        queryFile += selectJoinFile;
        
        queryFile += ') AS s ';
        queryFile += 'ON f.ID = s.ID JOIN '+ schema.default +'."CORE::FileShare" as fs ON s.ID = fs.ID_FILE ';
        queryFile += 'GROUP BY f.ID, f.STATUS, f.ID_FOLDER, f.ID_OBJECT_TYPE';
        
        executeFile.query = (queryFile);
        
        var resultFiles = sql.SELECT(executeFile);
        var files = [];
        for(var a = 0; a < resultFiles.length; a++){
            var optionsOT = {
                where:[{field:'id', value: resultFiles[a][3], oper:'='}]
            };
            var obj = {
                id:resultFiles[a][0],
                status:resultFiles[a][1],
                id_folder:resultFiles[a][2],
                id_objectType:resultFiles[a][3],
                objectType:{
                    id:resultFolders[a][3],
                    name:objectTypesModel.READ(optionsOT)[0].name
                },
                totalShares:parseInt(resultFiles[a][4])
            };
            files.push(obj);
        }
        
        //get all the files shared with my groups
        
        var joinGroupFile = [
            {
                fields:[],
                alias: 'fileShare',
                table: this.fileShareModel,
                on: [
                    {
                        left: 'id',
                        right: 'idFile'
                    },
                    {
                        field: 'idGroup', 
                        value: userGroupsIDs, 
                        oper: '='
                    }
                ]            
            },
            {
                fields: [],
                alias: 'objectTypes',
                table: this.objectTypesModel,
                on: [
                    {
                        left_table: this.fileModel,
                        left: 'idObjectType',
                        right: 'id'
                    }
                ]
            }
        ];
        
        var whereFileGroup = [];
        
        for(var q = 0;q<idAllFolders.length;q++){
            whereFileGroup.push({"field": "idFolder", "value": idAllFolders[q], "oper": "!="});    
        }
        
        if(options.hasOwnProperty('status') && options.status.length > 0){
            var statusFileGroup = [];
            for(var c = 0; c< options.status.length;c++){
                statusFileGroup.push(eval(options.status[c]));
            }
            whereFileGroup.push({"field": "status", "value": statusFileGroup, "oper": "="});
        }
        
        if(options.hasOwnProperty('objectType') && options.objectType.length > 0){
            joinGroupFile[1].on.push({field:'name', value:options.objectType, oper: '='});
        }
        
        if(options.hasOwnProperty('folderId') && options.folderId != null){
            whereFileGroup.push({"field": "idFolder", "value": options.folderId, "oper": "="});
        }
        
        var read_optionsFile = {
            fields:['id','status','idFolder','idObjectType'],
            join:joinGroupFile,
            where:whereFileGroup
        };
        
        var resultGroupFile = fileModel.READ(read_optionsFile);
        
        for(var n in resultGroupFile){
            resultGroupFile[n].totalShares = 2;
            files.push(resultGroupFile[n]);
        }
        
        for(var reg in files){
            var optionsOT = {
                where:[{field:'id',value:files[reg].idObjectType,oper:'='}]
            };
            for(var prop in files[reg]){
                if(prop == 'idFolder'){
                    files[reg].id_folder = files[reg].idFolder;
                }
                if(prop == 'idObjectType'){
                    files[reg].id_objectType = files[reg].idObjectType;
                }
            }
            if(!files[reg].hasOwnProperty('objectType')){
                files[reg].objectType = {
                    id:files[reg].idObjectType,
                    name:objectTypesModel.READ(optionsOT)[0].name
                }
            }
            if(!files[reg].hasOwnProperty('totalShares')){
                files[reg].totalShares = 2;
            }
            delete files[reg].idFolder;
            delete files[reg].idObjectType;
        }
        
        var sharedFiles = [];
        for(var r in files){
            if(files[r].totalShares === 1){
                sharedFiles.push(files[r]);
            }
        }
        
        response.files = sharedFiles;
            
        return response;
        
    }catch(e){
        throw util.parseError(e);
    }
};
