this.api = {
	orm: {
		tableLib: function () {
			//ORM V1
			$.import('timp.core.server.orm', 'table');
			let table_lib = $.timp.core.server.orm.table;
			return table_lib;
		}
	},
	router: {
		v2: function () {
			$.import('timp.core.server.router', 'router');
			const Router = $.timp.core.server.router.router.Router;
			return Router;
		}
	}
};