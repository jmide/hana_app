$.import('timp.core.server.libraries.internal', 'Performance');

$.import('timp.core.server.libraries.internal', 'util');
this.util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server.libraries.external', 'lodash');
this.lodash = $.timp.core.server.libraries.external.lodash;

$.import('timp.core.server.libraries.external', 'xlsx');
this.xlsx = $.timp.core.server.libraries.external.xlsx;

$.import('timp.core.server', 'util');
this.legacyUtil = $.timp.core.server.util;

$.import('timp.core.server.models', 'schema');
this.schema = $.timp.core.server.models.schema;

$.import('timp.core.server', 'config');
this.currentVersion = $.timp.core.server.config.application.version;
this.systemVersion = $.timp.core.server.config.application.version;

$.import("timp.core.server", "base64");
this.base64 = $.timp.core.server.base64;

$.import('timp.core.server.orm', 'sql');
this.sql = $.timp.core.server.orm.sql;

//teste Performance
$.import('timp.core.server.orm', 'OrmHighPerformance');
this.OrmHighPerformance = $.timp.core.server.orm.OrmHighPerformance;

$.import('timp.core.server.orm', 'table');
this.table_lib = $.timp.core.server.orm.table;

$.import('timp.core.server.orm', 'view');
this.view_lib = $.timp.core.server.orm.view;

$.import('timp.core.server.models', 'actionsParameters');
this.actionsParameters = $.timp.core.server.models.actionsParameters;

$.import('timp.core.server.models', 'actions');
this.actions = $.timp.core.server.models.actions;

$.import('timp.core.server.models', 'components');
this.components = $.timp.core.server.models.components.components;
this.modelComponents = $.timp.core.server.models.components;
this.apps = $.timp.core.server.models.components.apps;
this.privileges = $.timp.core.server.models.components.privileges;

$.import('timp.core.server.controllers', 'privileges');
this.privilegesController = $.timp.core.server.controllers.privileges;

$.import('timp.core.server.models', 'users');
this.users = $.timp.core.server.models.users.users;
this.usersModel = $.timp.core.server.models.users;

$.import('timp.core.server.models', 'cvUserPrivileges');
this.cvUserPrivilegesModel = $.timp.core.server.models.cvUserPrivileges.table;

$.import("timp.core.server.controllers", "users");
this.usersController = $.timp.core.server.controllers.users;

$.import("timp.core.server.controllers.refactor", "user");
this.usersController2 = $.timp.core.server.controllers.refactor.user;

$.import('timp.core.server.models', 'labels');
this.labels = $.timp.core.server.models.labels;

$.import('timp.core.server.controllers', 'labels');
this.labelsController = $.timp.core.server.controllers.labels;

//resources controller
$.import('timp.core.server.orm', 'resources');
this.resources = $.timp.core.server.orm.resources;

//Config
$.import('timp.core.server.models', 'config');
this.config = $.timp.core.server.models.config;

$.import('timp.core.server.controllers', 'config');
this.configController = $.timp.core.server.controllers.config;

$.import('timp.core.server.controllers', 'images');
this.imagesController = $.timp.core.server.controllers.images;
//Images
$.import('timp.core.server.models', 'images');
this.images = $.timp.core.server.models.images;

// Attachments
$.import('timp.core.server.models', 'attachments');
this.attachments = $.timp.core.server.models.attachments;

$.import('timp.core.server.controllers','attachments');
this.attachmentsController = $.timp.core.server.controllers.attachments;

$.import('timp.core.server.models', 'additionalFunction');
this.additionalFunction = $.timp.core.server.models.additionalFunction;

//Actions
$.import('timp.core.server.models', 'actions');
this.actions = $.timp.core.server.models.actions;

//Request
$.import('timp.core.server.models', 'requests');
this.requests = $.timp.core.server.models.requests;

//Periodicity
$.import('timp.core.server.models', 'periodicity');
this.periodicity = $.timp.core.server.models.periodicity;

//LogTracer
$.import('timp.core.server.models.tables', 'LogTracer');
this.LogTracerModel = $.timp.core.server.models.tables.LogTracer;

try {
	$.import('timp.core.server.models', 'objects');
	$.import('timp.core.server.controllers', 'objects');
	this.objects = $.timp.core.server.models.objects.objects;
	this.objectTypes = $.timp.core.server.models.objects.objectTypes;
	// this.folders = $.timp.core.server.models.objects.folders;
	this.objectsUserData = $.timp.core.server.models.objects.objectsUserData;
	this.objectsShare = $.timp.core.server.models.objects.objectShare;
	this.objectCtrl = $.timp.core.server.controllers.objects;
	this.objectTypeBaseModel = $.timp.core.server.models.objects.objectTypeBaseModel;
} catch (e) {

}

try {
	$.import('timp.core.server.models', 'fileSystem');
	this.fileSystem = $.timp.core.server.models.fileSystem;
	this.folder = $.timp.core.server.models.fileSystem.folder;
	this.folderShare = $.timp.core.server.models.fileSystem.folderShare;
	this.file = $.timp.core.server.models.fileSystem.file;
	this.fileShare = $.timp.core.server.models.fileSystem.fileShare;
	this.fileFavs = $.timp.core.server.models.fileSystem.fileFavs;
	this.fileLock = $.timp.core.server.models.fileSystem.fileLock;
	this.fileBaseModel = $.timp.core.server.models.fileSystem.fileBaseModel;
	this.folderBaseModel = $.timp.core.server.models.fileSystem.folderBaseModel;
	this.fileFavsBaseModel = $.timp.core.server.models.fileSystem.fileFavsBaseModel;
	this.fileShareBaseModel = $.timp.core.server.models.fileSystem.fileShareBaseModel;
} catch (e) {

}

try {
	$.import('timp.core.server.controllers', 'fileCRUDF');
	this.fileCRUDFController = $.timp.core.server.controllers.fileCRUDF;
	$.import('timp.core.server.controllers', 'fileCRUDFNew');
	this.fileCRUDFNew = $.timp.core.server.controllers.fileCRUDFNew;
	$.import('timp.core.server.controllers', 'fileShare');
	this.fileShareController = $.timp.core.server.controllers.fileShare;
	$.import('timp.core.server.controllers', 'fileExplorer');
	this.fileExplorerController = $.timp.core.server.controllers.fileExplorer;
} catch (e) {

}

$.import('timp.core.server.models', 'messageCodes');
this.messageCodes = $.timp.core.server.models.messageCodes.messageCodes;
this.messageCodeTexts = $.timp.core.server.models.messageCodes.messageCodeTexts;

$.import('timp.core.server.controllers', 'messageCodes');
this.messageCodesController = $.timp.core.server.controllers.messageCodes;

$.import('timp.core.server.models', 'orgStructure');
this.company = $.timp.core.server.models.orgStructure.company;
this.branch = $.timp.core.server.models.orgStructure.branch;
// throw this.branch

$.import('timp.core.server.installer', 'installer');
this.Installer = $.timp.core.server.installer.installer.Installer;

$.import('timp.core.server.router', 'router');
this.Router = $.timp.core.server.router.router.Router;

$.import('timp.core.server', 'complexUtil');
this.complexUtil = $.timp.core.server.complexUtil;

$.import('timp.core.server.install', 'install');
this.install = $.timp.core.server.install.install;

// $.import('timp.core.formLayout.server.install','install');
// this.formLayoutInstall = $.timp.core.formLayout.server.install.install;

try {
	$.import('timp.core.server.objects', 'meta_object');
	this.meta_object = $.timp.core.server.objects.meta_object;

	$.import('timp.core.server.objects', 'base_object');
	this.base_object = $.timp.core.server.objects.base_object;

	$.import('timp.core.server.logic', 'meta_action');
	this.meta_action = $.timp.core.server.logic.meta_action;

	$.import('timp.core.server.objects', 'classes');
	this.classes = $.timp.core.server.objects.classes;
} catch (e) {

}

try {
	$.import('timp.core.server.actions', 'actions');
	this.componentActions = $.timp.core.server.actions.actions;
} catch (e) {
	this.componentActions = e;
}

$.import('timp.core.server.models', 'dataTypeConversion');
this.dataTypeConversion = $.timp.core.server.models.dataTypeConversion;

try {
	$.import('timp.core.server.models', 'output');
	this.outputModel = $.timp.core.server.models.output;
	this.output = $.timp.core.server.models.output.output;
	this.outputType = $.timp.core.server.models.output.outputType;
	this.outputTypeText = $.timp.core.server.models.output.outputTypeText;
	this.outputValue = $.timp.core.server.models.output.outputValue;
} catch (e) {

}

$.import('timp.core.server.controllers', 'output');
this.outputCtrl = $.timp.core.server.controllers.output;

//-- APIs Output Controllers
$.import('timp.core.server.controllers', 'output');
try {
	this.outputList = $.timp.core.server.controllers.output.list;
} catch (e) {
	this.outputList = e;
}
try {
	this.outputCreate = $.timp.core.server.controllers.output.create;
} catch (e) {
	this.outputCreate = e;
}
try {
	this.outputUpdate = $.timp.core.server.controllers.output.update;
} catch (e) {
	this.outputUpdate = e;
}
try {
	this.createValue = $.timp.core.server.controllers.output.createValue;
} catch (e) {
	this.createValue = e;
}
try {
	this.updateValue = $.timp.core.server.controllers.output.updateValue;
} catch (e) {
	this.updateValue = e;
}
try {
	this.listOutputType = $.timp.core.server.controllers.output.listOutputType;
} catch (e) {
	this.listOutputType = e;
}

//formula parser
$.import('timp.core.server.controllers', 'formulaParser');
this.formulaParser = $.timp.core.server.controllers.formulaParser;

$.import('timp.core.server.controllers', 'formula');
this.formulaController = $.timp.core.server.controllers.formula;

$.import('timp.core.server.models.tables', 'componentManual');
this.componentManualModel = $.timp.core.server.models.tables.componentManual;
$.import('timp.core.server.models.tables', 'logDetails');
this.logDetails = $.timp.core.server.models.tables.logDetails;
$.import('timp.core.server.controllers.refactor', 'componentManual');
this.componentManualController = $.timp.core.server.controllers.refactor.componentManual;
$.import('timp.core.server.controllers.refactor', 'log');
this.supervisorCoreController = $.timp.core.server.controllers.refactor.log;

$.import('timp.core.server.orm', 'services');
$.import('timp.core.server.orm', 'tableServices');
$.import('timp.core.server.controllers.public', 'configuration');
$.import('timp.core.server.controllers.refactor', 'seederParser');
$.import('timp.core.server.controllers.public', 'component');
$.import('timp.core.server.controllers.public', 'license');
$.import('timp.core.server.controllers.public', 'proxyService');
$.import('timp.core.server.controllers.public', 'user');
$.import('timp.core.server.controllers.public', 'package');
$.import('timp.core.server.controllers.public', 'group');
$.import('timp.core.server.controllers.public', 'label');
$.import('timp.core.server.controllers.public', 'log');
$.import('timp.core.server.controllers.public', 'fileSystem');
const _this = this;
let imports = [
    $.timp.core.server.controllers.public.configuration,
    $.timp.core.server.orm.services,
    $.timp.core.server.orm.tableServices,
    $.timp.core.server.controllers.public.component,
    $.timp.core.server.controllers.public.license,
    $.timp.core.server.controllers.refactor.seederParser,
    $.timp.core.server.controllers.public.proxyService,
    $.timp.core.server.controllers.public.package,
    $.timp.core.server.controllers.public.user,
    $.timp.core.server.controllers.public.group,
    $.timp.core.server.controllers.public.label,
    $.timp.core.server.controllers.public.log,
    $.timp.core.server.controllers.public.fileSystem
];

$.lodash.forEach(imports, function(_import) {
	for (let method in _import) {
		if (_import.hasOwnProperty(method) && $.lodash.isFunction(_import[method])) {
			_this[method] = _import[method];
		}
	}
});

$.import('timp.core.server.models.views', 'UserTaxes');

this.view = {
	userTaxes: $.timp.core.server.models.views.UserTaxes
};