$.import('timp.core.server.models.tables', 'configuration');
const configurationModel = $.timp.core.server.models.tables.configuration.configurationModel;
$.import('timp.core.server.controllers.refactor', 'component');
const componentCtrl = $.timp.core.server.controllers.refactor.component;

const coreComponentId = componentCtrl.getCoreComponentId() || 1;

this.seeder = {
	model: configurationModel,
	'base': [
	// ECC  configurations
	// {
	// 	'key': 'ECC::HOST',
	// 	'componentId': coreComponentId,
	// 	'value': 'as1-100-25'
	// }, {
	// 	'key': 'ECC::PORT',
	// 	'componentId': coreComponentId,
	// 	'value': 8000
	// }, {
	// 	'key': 'ECC::CLIENT',
	// 	'componentId': coreComponentId,
	// 	'value': 300
	// }, {
	// 	'key': 'ECC::LANG',
	// 	'componentId': coreComponentId,
	// 	'value': 'P'
	// }, {
	// 	'key': 'ECC::YEAR',
	// 	'componentId': coreComponentId,
	// 	'value': 2010
	// }, {
	// 	'key': 'ECC::YEAR_2_DIGIT',
	// 	'componentId': coreComponentId,
	// 	'value': 10
	// }, {
	// 	'key': 'ECC:CLIENT_TEST',
	// 	'componentId': coreComponentId,
	// 	'value': 120
	// }, {
	// 	'key': 'ECC::SCHEMA',
	// 	'componentId': coreComponentId,
	// 	'value': 'SAP_ECC_TIMP'
	// }, {
	// 	'key': 'ECC::FULLDATE',
	// 	'componentId': coreComponentId,
	// 	'value': 20100101
	// },

	// TDF Configurations
	// {
	// 	'key': 'TIMP::TDFIntegration',
	// 	'componentId': coreComponentId,
	// 	'value': true
	// }, {
	// 	'key': 'TDF::SCHEMA',
	// 	'componentId': coreComponentId,
	// 	'value': 'SAPABAP2'
	// }, {
	// 	'key': 'tdfSchemaName',
	// 	'componentId': coreComponentId,
	// 	'value': 'SAPABAP2'
	// },

	// Trace  Configurations
	// {
	// 	'key': 'TIMP::TracerActivity',
	// 	'componentId': coreComponentId,
	// 	'value': false
	// },

	// BRB Executor Configurations
	// {
	// 	'key': 'TIMP::HighPerformanceBRBEngine',
	// 	'componentId': coreComponentId,
	// 	'value': 'choose'
	// }, {
	// 	'key': 'TIMP::HighPerformanceBRBEngineRules',
	// 	'componentId': coreComponentId,
	// 	'value': 'choose'
	// }, {
	// 	'key': 'TIMP::HighPerformanceBRBEngineBCB',
	// 	'componentId': coreComponentId,
	// 	'value': 'choose'
	// },

	// Session Configurations
	// {
	// 	'key': 'TIMP::LogonType', // used in ADM
	// 	'componentId': coreComponentId,
	// 	'value': 'Basic'
	// }, {
	// 	'key': 'TIMP::SessionTime', // used in ADM
	// 	'componentId': coreComponentId,
	// 	'value': 900
	// }, 
	{
		'key': 'TIMP::FileLockTime', // used in ADM
		'componentId': coreComponentId,
		'value': 300
	}, {
		'key': 'TIMP::VerifyFileLockTime', // used in ADM
		'componentId': coreComponentId,
		'value': 30
	}

	// User privileges Configurations
	// {
	// 	'key': 'TIMP::UsersManagement',
	// 	'componentId': coreComponentId,
	// 	'value': 'basic'
	// }

	// Deletion management Configurations
	// {
	// 	'key': 'TIMP::DeleteDateLog',
	// 	'componentId': coreComponentId,
	// 	'value': 20190412
	// }, {
	// 	'key': 'TIMP::DeleteDateAncilliaryObligations',
	// 	'componentId': coreComponentId,
	// 	'value': 20190508
	// }, {
	// 	'key': 'TIMP::DeleteUserData',
	// 	'componentId': coreComponentId,
	// 	'value': false
	// }, {
	// 	'key': 'TIMP::DeleteTimeLog',
	// 	'componentId': coreComponentId,
	// 	'value': ''
	// }, {
	// 	'key': 'TIMP::DeleteTimeAncilliaryObligations',
	// 	'componentId': coreComponentId,
	// 	'value': ''
	// }, {
	// 	'key': 'TIMP::DeleteDateLogExecuted',
	// 	'componentId': coreComponentId,
	// 	'value': ''
	// }, {
	// 	'key': 'TIMP::DeleteDateAncilliaryObligationsExecuted',
	// 	'componentId': coreComponentId,
	// 	'value': ''
	// },
	]
};