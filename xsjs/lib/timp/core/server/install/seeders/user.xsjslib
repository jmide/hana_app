$.import('timp.core.server.models', 'schema');
const schema = $.timp.core.server.models.schema.default.slice(1, -1);

$.import('timp.core.server.models.tables', 'user');

this.seeder = {
    model: $.timp.core.server.models.tables.user.userModel,
	'base': [{
	    name: schema,
        lastName: schema,
        hanaUser: schema
	}]
};