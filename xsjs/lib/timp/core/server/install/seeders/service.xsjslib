$.import('timp.core.server.models.tables', 'proxyService');

this.seeder = {
	model: $.timp.core.server.models.tables.proxyService.serviceModel,
	'base': [{
		name: "TMFNotaFiscalCorrectionsSyncIn",
		descriptionEnus: "Nota Fiscal Correction Service",
		descriptionPtbr: "Serviço de Correção de Nota Fiscal",
		type: 'TDF'
    }, {
		name: "TMFFiscalPeriodGetStatusSyncIn",
		descriptionEnus: "Fiscal Period Status Verification Service",
		descriptionPtbr: "Serviço de Verificação de Status de Período Fiscal",
		type: 'TDF'
    }, {
		name: "TMFFIPostingPostSyncIn",
		descriptionEnus: "Post of FI documents to SAP ERP Service",
		descriptionPtbr: "Serviço de publicação de documentos FI no SAP ERP",
		type: 'TDF'
    }, {
		name: "TMFFiscalPeriodChangeStatusSyncIn",
		descriptionEnus: "Fiscal Period Status Update Service",
		descriptionPtbr: "Serviço de Atualização de Status de Período Fiscal",
		type: 'TDF'
    }, {
		name: "TMFFIReversalPostSyncIn",
		descriptionEnus: "Reverse posting of FI documents in SAP ERP Service",
		descriptionPtbr: "Serviço de Reversão de publicação de documentos FI no SAP ERP",
		type: 'TDF'
    }, {
		name: "TMFCIAPCreditFactorModifySyncIn",
		descriptionEnus: "Credit Factor Maintenance Service",
		descriptionPtbr: "Serviço de Manutenção de Fator de Creditamento",
		type: 'TDF'
    }, {
		name: "TMFCIAPAccountingSyncIn",
		descriptionEnus: "Run Accounting for all CIAP records stored in the ERP system",
		descriptionPtbr: "Serviço para executar processos de contabilização para registro do CIAP",
		type: 'TDF'
    }, {
		name: "TMFEFDReportRun",
		descriptionEnus: "SPED EFD Report Execution Service",
		descriptionPtbr: "Serviço de Execução de Relatórios SPED EFD",
		type: 'TDF'
    }, {
		name: "TMFPCOReportRun",
		descriptionEnus: "SPED PCO Report Execution Service",
		descriptionPtbr: "Serviço de Execução de Relatórios SPED PCO",
		type: 'TDF'
    }, {
		name: "TMFECDReportRun",
		descriptionEnus: "SPED ECD Report Execution Service",
		descriptionPtbr: "Serviço de Execução de Relatórios SPED ECD",
		type: 'TDF'
    }, {
		name: "TMFECFreportExecutionSyncIn",
		descriptionEnus: "SPED ECF Report Execution Service",
		descriptionPtbr: "Serviço de Execução de Relatórios SPED ECF",
		type: 'TDF'
    }, {
		name: "TMFTOMExportFile",
		descriptionEnus: "SPED Files Export Service",
		descriptionPtbr: "Serviço de Exportação de Arquivos do SPED",
		type: 'TDF'
    }, {
        name: "TMFELSSetMessageSyncIn",
        descriptionEnus: "Service to Log External TDF Transactions",
        descriptionPtbr: "Serviço para cadastrar transações externas do TDF",
        type: "TDF"
    }, {
        name: "TMFELSGetPpidSyncIn",
        descriptionEnus: "Service to upload data from external source into SAP TDF",
        descriptionPtbr: "Serviço de carregamento de dados em massa de sistemas externos no SAP TDF",
        type: "TDF"
    }, {
        name: "TMFNFCreationSyncIn",
        descriptionEnus: "TDF Invoice Creation Service",
        descriptionPtbr: "Serviço de Criação de Nota Fiscal do TDF",
        type: "TDF"
    }, {
    	name: "TMFNFCorrectionLetterCreationSyncIn",
    	descriptionEnus: "Create correction letters in the ERP system",
    	descriptionPtbr: "Criar correção nas cartas do sistema ERP",
    	type: "TDF"
    }, {
    	name: "timp_modifica_dados_contabeis",
    	descriptionEnus: "SAP ECC Releases Financial correction",
    	descriptionPtbr: "Correção de Lançamentos Contábeis do SAP ECC",
    	type: "ECC"
    }]
};