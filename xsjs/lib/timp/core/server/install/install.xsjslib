const _this = this;
// $.import('timp.core.server.libraries.internal', 'util');
// const util = $.timp.core.server.libraries.internal.util;

$.import('timp.core.server.installer', 'installer');
const Installer = $.timp.core.server.installer.installer.Installer;

$.import('timp.core.server.install.seeders', 'dataTypeConversion');
const dataTypeConversionSeeder = $.timp.core.server.install.seeders.dataTypeConversion.seeder;

$.import('timp.core.server.install.seeders', 'user');		
const userSeeder = $.timp.core.server.install.seeders.user.seeder;
$.import('timp.core.server.install.seeders', 'service');
const serviceSeeder = $.timp.core.server.install.seeders.service.seeder;
$.import('timp.core.server.install.seeders', 'configuration');
const configurationSeeder = $.timp.core.server.install.seeders.configuration.seeder;

$.import('timp.core.server.models.tables', 'dataTypeConversion');
const dataTypeConversionModel = $.timp.core.server.models.tables.dataTypeConversion.dataTypeConversionModel;

$.import('timp.core.server.models.tables', 'component');
const componentModel = $.timp.core.server.models.tables.component.componentModel;
const i18nModel = $.timp.core.server.models.tables.component.internationalizationModel;

$.import('timp.core.server.install.models', 'modelIndex');
const modelIndex = $.timp.core.server.install.models.modelIndex.models;

$.import('timp.core.server.controllers.imports', 'index');
const importDataCtrl = $.timp.core.server.controllers.imports.index;

$.import('timp.core.server.controllers.refactor', 'seederParser');
const getParsedSeeder = $.timp.core.server.controllers.refactor.seederParser.getParsedSeeder;

$.import('timp.core.server.controllers.refactor', 'license');
const licenseController = $.timp.core.server.controllers.refactor.license;

$.import('timp.core.server.controllers.refactor', 'configuration');
const configurationController = $.timp.core.server.controllers.refactor.configuration;

$.import('timp.core.server.install.i18n', 'messageCodes');
const i18n = $.timp.core.server.install.i18n.messageCodes;

$.import('timp.core.server.models.tables', 'configuration');
const configurationModel = $.timp.core.server.models.tables.configuration.configurationModel;

// $.import('timp.core.server.orm', 'BaseModel');
// const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
const currentVersion = $.timp.core.server.config.application.version;

_this.metadata = {
	name: 'CORE',
	labelEnus: 'TIMP Main Component',
	labelPtbr: 'Componente Central do TIMP'
};

this.install = function(page) {
	let seeder = getParsedSeeder([
		userSeeder,
		serviceSeeder
		// configurationSeeder
	]);
	let response;
	const beforeInstall = function() {
		// Installer uses DataTypeConversionModel, so we need to solve this dependency in the beforeInstall function.
		let beforeInstallResponse = dataTypeConversionModel.exists();
		if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
			throw beforeInstallResponse.errors;
		} else if (!beforeInstallResponse.results.exists) {
			beforeInstallResponse = dataTypeConversionModel.sync();
			if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
				throw beforeInstallResponse.errors;
			}
		}
		beforeInstallResponse = dataTypeConversionModel.find({
			count: true,
			select: [{
				field: 'id'
			}]
		});
		if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
			throw beforeInstallResponse.errors;
		}
		let count = beforeInstallResponse.results[0].tableCount;
		count = $.lodash.isInteger(count) ? count : 0;
		if (count <= 0) {
			beforeInstallResponse = dataTypeConversionModel.batchCreate(dataTypeConversionSeeder.base);
			if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
				throw beforeInstallResponse.errors;
			}
		}
		beforeInstallResponse = componentModel.exists();
		if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
			throw beforeInstallResponse.errors;
		} else if (!beforeInstallResponse.results.exists) {
			beforeInstallResponse = componentModel.sync();
			if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
				throw beforeInstallResponse.errors;
			}
			let componentMetadata = $.lodash.assign($.lodash.cloneDeep(_this.metadata), {
				version: currentVersion
			});
			beforeInstallResponse = componentModel.create(componentMetadata);
			if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
				throw beforeInstallResponse.errors;
			}
		}
		beforeInstallResponse = importDataCtrl.importComponentData(false);
		if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
			throw beforeInstallResponse.errors;
		}
		if ($.lodash.isBoolean(i18nModel.exists().results.exists) && i18nModel.exists().results.exists) {
			beforeInstallResponse = i18nModel.delete({
				where: [{
					literal: {
						type: 'integer',
						value: 1
					},
					operator: '$eq',
					value: 1
				}]
			});
			if ($.lodash.isArray(beforeInstallResponse.errors) && !$.lodash.isEmpty(beforeInstallResponse.errors)) {
				throw beforeInstallResponse.errors;
			}
		}
	};
	const afterInstall = function() {
		let retVal = {
			errors: []
		};
		let skipDependencies;
		const sanitizeCleanInstallErrors = function(response) {
			if (response.errors.length === 1 && $.lodash.isPlainObject(response.errors[0]) && $.lodash.isString(response.errors[0].name) &&
				response.errors[0].name === 'Invalid Source') {
				response.errors = [];
			}
			return response;
		};
		let afterInstallResponse = importDataCtrl.importUserData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importGroupData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importGroupUserData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importConfigurationData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importLicenseData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importTaxData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importTaxTypeData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importTaxMapData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importAdapterData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importServiceData(true);
		afterInstallResponse = sanitizeCleanInstallErrors(afterInstallResponse);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.importLogData(true);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		skipDependencies = afterInstallResponse.results.skipped;
		skipDependencies = $.lodash.isBoolean(skipDependencies) ? skipDependencies : false;
		afterInstallResponse = importDataCtrl.importLogErrorData(skipDependencies);
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		afterInstallResponse = importDataCtrl.performCoreLogImports();
		if ($.lodash.isArray(afterInstallResponse.errors) && !$.lodash.isEmpty(afterInstallResponse.errors)) {
			retVal.errors = $.lodash.concat(retVal.errors, afterInstallResponse.errors);
		}
		// LICENSE
		licenseController.activateTaxesForLicenseType();
		// System Configuration: Create keys on the CORE::CONFIGURATION table. 
		if (configurationSeeder && configurationSeeder.base && configurationSeeder.base.length) {
			let systemConfigurationResponse = configurationController.createSystemConfigurationKeys(configurationSeeder);
			if ($.lodash.isArray(systemConfigurationResponse.errors) && !$.lodash.isEmpty(systemConfigurationResponse.errors)) {
				retVal.errors = $.lodash.concat(retVal.errors, systemConfigurationResponse.errors);
			}
		}
		if (!$.lodash.isEmpty(retVal.errors)) {
			throw retVal.errors;
		}
		if (configurationModel.getDefinition()) {
			let executorsConfigurations = [
				'TIMP::HighPerformanceBRBEngine',
				'TIMP::HighPerformanceBRBEngineRules',
				'TIMP::HighPerformanceBRBEngineBCB'
			];
			let response = configurationModel.find({
				where: [{
					field: 'key',
					operator: '$in',
					value: [
						'TIMP::HighPerformanceBRBEngine',
						'TIMP::HighPerformanceBRBEngineRules',
						'TIMP::HighPerformanceBRBEngineBCB'
					]
				}]
			});
			if (response && response.results) {
				let configurationsToCreate = [];
				let configurationsInTable = response.results.map(function(config) {
					return config.key;
				});
				executorsConfigurations.forEach(function(config) {
					if (configurationsInTable.indexOf(config) === -1) {
						configurationsToCreate.push({
							key: config,
							value: 'choose',
							componentId: 1
						});
					}
				});
				if (configurationsToCreate && configurationsToCreate.length) {
					configurationModel.batchUpsert(configurationsToCreate);
				}
			}
		}
	};
	response = new Installer({
		beforeInstall: beforeInstall,
		models: modelIndex,
		component: _this.metadata,
		seeder: seeder,
		i18n: i18n.getMessageCodes(),
		afterInstall: afterInstall,
		page: page
	}, {
		returnSeederInstances: true
	}).install();
	return response;
};