this.value = {
	"be": {
		"generic": {
			"success": {
				"create": {
					"000": {
						"enus": "Created.",
						"ptbr": "Criado."
					}
				},
				"read": {
					"000": {
						"enus": "Read.",
						"ptbr": "A informação foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Updated.",
						"ptbr": "Atualizado."
					}
				},
				"delete": {
					"000": {
						"enus": "Deleted.",
						"ptbr": "Deletado."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create.",
						"ptbr": "Erro na criação."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read.",
						"ptbr": "Erro tentando ler as informações."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update.",
						"ptbr": "Erro tentando atualizar."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete.",
						"ptbr": "Erro tentando deletar."
					}
				},
				"internalError": {
					"000": {
						"enus": "Internal server error.",
						"ptbr": "Internal server error."
					}
				},
				"paramsError": {
					"000": {
						"enus": "Invalid parameters.",
						"ptbr": "Parâmetros Inválidos."
					}
				},
				"routeNotFound": {
					"000": {
						"enus": "Route not found.",
						"ptbr": "Route not found."
					}
				},
				"authenticationError": {
					"000": {
						"enus": "Not autheticated.",
						"ptbr": "Not autheticated."
					}
				},
				"insufficientPrivileges": {
					"000": {
						"enus": "Insufficient privileges.",
						"ptbr": "Insufficient privileges."
					}
				}
			}
		},
		"user": {
			"success": {
				"create": {
					"000": {
						"enus": "User created successfully.",
						"ptbr": "Usuário criado com sucesso."
					},
					"001": {
						"enus": "Users created successfully.",
						"ptbr": "Usuários criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "User information was consulted.",
						"ptbr": "A informação do Usuário  foi consultada."
					},
					"001": {
						"enus": "Users information was consulted.",
						"ptbr": "A informação dos Usuários  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "User updated successfully.",
						"ptbr": "Usuário atualizado com sucesso."
					},
					"001": {
						"enus": "Users updated successfully.",
						"ptbr": "Usuários atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "User deleted successfully.",
						"ptbr": "Usuário deletado com sucesso."
					},
					"001": {
						"enus": "Users deleted successfully.",
						"ptbr": "Usuários deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the User.",
						"ptbr": "Erro na criação do Usuário."
					},
					"001": {
						"enus": "Error trying to create the Users.",
						"ptbr": "Erro na criação dos Usuários."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the User.",
						"ptbr": "Erro tentando ler as informações do Usuário."
					},
					"001": {
						"enus": "Error trying to read the Users.",
						"ptbr": "Erro tentando ler as informações dos Usuários."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the User.",
						"ptbr": "Erro tentando atualizar o Usuário."
					},
					"001": {
						"enus": "Error trying to update the Users.",
						"ptbr": "Erro tentando atualizar os Usuários."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the User.",
						"ptbr": "Erro tentando deletar o Usuário."
					},
					"001": {
						"enus": "Error trying to delete the Users.",
						"ptbr": "Erro tentando deletar os Usuários."
					}
				}
			}
		},
		"group": {
			"success": {
				"create": {
					"000": {
						"enus": "Group created successfully.",
						"ptbr": "Grupo criado com sucesso."
					},
					"001": {
						"enus": "Groups created successfully.",
						"ptbr": "Grupos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Group information was consulted.",
						"ptbr": "A informação do Grupo  foi consultada."
					},
					"001": {
						"enus": "Groups information was consulted.",
						"ptbr": "A informação dos Grupos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Group updated successfully.",
						"ptbr": "Grupo atualizado com sucesso."
					},
					"001": {
						"enus": "Groups updated successfully.",
						"ptbr": "Grupos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Group deleted successfully.",
						"ptbr": "Grupo deletado com sucesso."
					},
					"001": {
						"enus": "Groups deleted successfully.",
						"ptbr": "Grupos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Group.",
						"ptbr": "Erro na criação do Grupo."
					},
					"001": {
						"enus": "Error trying to create the Groups.",
						"ptbr": "Erro na criação dos Grupos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Group.",
						"ptbr": "Erro tentando ler as informações do Grupo."
					},
					"001": {
						"enus": "Error trying to read the Groups.",
						"ptbr": "Erro tentando ler as informações dos Grupos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Group.",
						"ptbr": "Erro tentando atualizar o Grupo."
					},
					"001": {
						"enus": "Error trying to update the Groups.",
						"ptbr": "Erro tentando atualizar os Grupos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Group.",
						"ptbr": "Erro tentando deletar o Grupo."
					},
					"001": {
						"enus": "Error trying to delete the Groups.",
						"ptbr": "Erro tentando deletar os Grupos."
					}
				}
			}
		},
		"package": {
			"success": {
				"create": {
					"000": {
						"enus": "Package created successfully.",
						"ptbr": "Pacote criado com sucesso."
					},
					"001": {
						"enus": "Packages created successfully.",
						"ptbr": "Pacotes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Package information was consulted.",
						"ptbr": "A informação do Pacote  foi consultada."
					},
					"001": {
						"enus": "Packages information was consulted.",
						"ptbr": "A informação dos Pacotes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Package updated successfully.",
						"ptbr": "Pacote atualizado com sucesso."
					},
					"001": {
						"enus": "Packages updated successfully.",
						"ptbr": "Pacotes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Package deleted successfully.",
						"ptbr": "Pacote deletado com sucesso."
					},
					"001": {
						"enus": "Packages deleted successfully.",
						"ptbr": "Pacotes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Package.",
						"ptbr": "Erro na criação do Pacote."
					},
					"001": {
						"enus": "Error trying to create the Packages.",
						"ptbr": "Erro na criação dos Pacotes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Package.",
						"ptbr": "Erro tentando ler as informações do Pacote."
					},
					"001": {
						"enus": "Error trying to read the Packages.",
						"ptbr": "Erro tentando ler as informações dos Pacotes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Package.",
						"ptbr": "Erro tentando atualizar o Pacote."
					},
					"001": {
						"enus": "Error trying to update the Packages.",
						"ptbr": "Erro tentando atualizar os Pacotes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Package.",
						"ptbr": "Erro tentando deletar o Pacote."
					},
					"001": {
						"enus": "Error trying to delete the Packages.",
						"ptbr": "Erro tentando deletar os Pacotes."
					}
				}
			}
		},
		"layout": {
			"success": {
				"create": {
					"000": {
						"enus": "Layout created successfully.",
						"ptbr": "Leiaute criado com sucesso."
					},
					"001": {
						"enus": "Layouts created successfully.",
						"ptbr": "Leiautes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Layout information was consulted.",
						"ptbr": "A informação do Leiaute  foi consultada."
					},
					"001": {
						"enus": "Layouts information was consulted.",
						"ptbr": "A informação dos Leiautes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Layout updated successfully.",
						"ptbr": "Leiaute atualizado com sucesso."
					},
					"001": {
						"enus": "Layouts updated successfully.",
						"ptbr": "Leiautes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Layout deleted successfully.",
						"ptbr": "Leiaute deletado com sucesso."
					},
					"001": {
						"enus": "Layouts deleted successfully.",
						"ptbr": "Leiautes deletados com sucesso."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Layout successfully executed.",
						"ptbr": "Leiaute executado com sucesso."
					},
					"001": {
						"enus": "Layout successfully exported.",
						"ptbr": "Leiaute exportado com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Layout.",
						"ptbr": "Erro na criação do Leiaute."
					},
					"001": {
						"enus": "Error trying to create the Layouts.",
						"ptbr": "Erro na criação dos Leiautes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Layout.",
						"ptbr": "Erro tentando ler as informações do Leiaute."
					},
					"001": {
						"enus": "Error trying to read the Layouts.",
						"ptbr": "Erro tentando ler as informações dos Leiautes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Layout.",
						"ptbr": "Erro tentando atualizar o Leiaute."
					},
					"001": {
						"enus": "Error trying to update the Layouts.",
						"ptbr": "Erro tentando atualizar os Leiautes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Layout.",
						"ptbr": "Erro tentando deletar o Leiaute."
					},
					"001": {
						"enus": "Error trying to delete the Layouts.",
						"ptbr": "Erro tentando deletar os Leiautes."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Error trying to execute the Layout.",
						"ptbr": "Erro tentando executar o Leiaute."
					},
					"001": {
						"enus": "Error trying to export the Layout.",
						"ptbr": "Erro tentnado exportar o Leiaute."
					}
				}
			}
		},
		"report": {
			"success": {
				"create": {
					"000": {
						"enus": "Report created successfully.",
						"ptbr": "Relatório criado com sucesso."
					},
					"001": {
						"enus": "Reports created successfully.",
						"ptbr": "Relatórios criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Report information was consulted.",
						"ptbr": "A informação do Relatório  foi consultada."
					},
					"001": {
						"enus": "Reports information was consulted.",
						"ptbr": "A informação dos Relatórios  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Report updated successfully.",
						"ptbr": "Relatório atualizado com sucesso."
					},
					"001": {
						"enus": "Reports updated successfully.",
						"ptbr": "Relatórios atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Report deleted successfully.",
						"ptbr": "Relatório deletado com sucesso."
					},
					"001": {
						"enus": "Reports deleted successfully.",
						"ptbr": "Relatórios deletados com sucesso."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Report successfully executed.",
						"ptbr": "Relatório executado com sucesso."
					},
					"001": {
						"enus": "Report successfully exported.",
						"ptbr": "Relatório exportado com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Report.",
						"ptbr": "Erro na criação do Relatório."
					},
					"001": {
						"enus": "Error trying to create the Reports.",
						"ptbr": "Erro na criação dos Relatórios."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Report.",
						"ptbr": "Erro tentando ler as informações do Relatório."
					},
					"001": {
						"enus": "Error trying to read the Reports.",
						"ptbr": "Erro tentando ler as informações dos Relatórios."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Report.",
						"ptbr": "Erro tentando atualizar o Relatório."
					},
					"001": {
						"enus": "Error trying to update the Reports.",
						"ptbr": "Erro tentando atualizar os Relatórios."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Report.",
						"ptbr": "Erro tentando deletar o Relatório."
					},
					"001": {
						"enus": "Error trying to delete the Reports.",
						"ptbr": "Erro tentando deletar os Relatórios."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Error trying to execute the Report.",
						"ptbr": "Erro tentando executar o Relatório."
					},
					"001": {
						"enus": "Error trying to export the Report.",
						"ptbr": "Erro tentnado exportar o Relatório."
					}
				}
			}
		},
		"structure": {
			"success": {
				"create": {
					"000": {
						"enus": "Structure created successfully.",
						"ptbr": "Estrutura criado com sucesso."
					},
					"001": {
						"enus": "Structures created successfully.",
						"ptbr": "Estruturas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Structure information was consulted.",
						"ptbr": "A informação do Estrutura  foi consultada."
					},
					"001": {
						"enus": "Structures information was consulted.",
						"ptbr": "A informação dos Estruturas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Structure updated successfully.",
						"ptbr": "Estrutura atualizado com sucesso."
					},
					"001": {
						"enus": "Structures updated successfully.",
						"ptbr": "Estruturas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Structure deleted successfully.",
						"ptbr": "Estrutura deletado com sucesso."
					},
					"001": {
						"enus": "Structures deleted successfully.",
						"ptbr": "Estruturas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Structure.",
						"ptbr": "Erro na criação do Estrutura."
					},
					"001": {
						"enus": "Error trying to create the Structures.",
						"ptbr": "Erro na criação dos Estruturas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Structure.",
						"ptbr": "Erro tentando ler as informações do Estrutura."
					},
					"001": {
						"enus": "Error trying to read the Structures.",
						"ptbr": "Erro tentando ler as informações dos Estruturas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Structure.",
						"ptbr": "Erro tentando atualizar o Estrutura."
					},
					"001": {
						"enus": "Error trying to update the Structures.",
						"ptbr": "Erro tentando atualizar os Estruturas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Structure.",
						"ptbr": "Erro tentando deletar o Estrutura."
					},
					"001": {
						"enus": "Error trying to delete the Structures.",
						"ptbr": "Erro tentando deletar os Estruturas."
					}
				}
			}
		},
		"installer": {
			"success": {},
			"error": {}
		},
		"process": {
			"success": {
				"create": {
					"000": {
						"enus": "Process created successfully.",
						"ptbr": "Processo criado com sucesso."
					},
					"001": {
						"enus": "Processes created successfully.",
						"ptbr": "Processos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Process information was consulted.",
						"ptbr": "A informação do Processo  foi consultada."
					},
					"001": {
						"enus": "Processes information was consulted.",
						"ptbr": "A informação dos Processos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Process updated successfully.",
						"ptbr": "Processo atualizado com sucesso."
					},
					"001": {
						"enus": "Processes updated successfully.",
						"ptbr": "Processos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Process deleted successfully.",
						"ptbr": "Processo deletado com sucesso."
					},
					"001": {
						"enus": "Processes deleted successfully.",
						"ptbr": "Processos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Process.",
						"ptbr": "Erro na criação do Processo."
					},
					"001": {
						"enus": "Error trying to create the Processes.",
						"ptbr": "Erro na criação dos Processos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Process.",
						"ptbr": "Erro tentando ler as informações do Processo."
					},
					"001": {
						"enus": "Error trying to read the Processes.",
						"ptbr": "Erro tentando ler as informações dos Processos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Process.",
						"ptbr": "Erro tentando atualizar o Processo."
					},
					"001": {
						"enus": "Error trying to update the Processes.",
						"ptbr": "Erro tentando atualizar os Processos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Process.",
						"ptbr": "Erro tentando deletar o Processo."
					},
					"001": {
						"enus": "Error trying to delete the Processes.",
						"ptbr": "Erro tentando deletar os Processos."
					}
				}
			}
		},
		"task": {
			"success": {
				"create": {
					"000": {
						"enus": "Task created successfully.",
						"ptbr": "Tarefa criado com sucesso."
					},
					"001": {
						"enus": "Tasks created successfully.",
						"ptbr": "Tarefas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Task information was consulted.",
						"ptbr": "A informação do Tarefa  foi consultada."
					},
					"001": {
						"enus": "Tasks information was consulted.",
						"ptbr": "A informação dos Tarefas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Task updated successfully.",
						"ptbr": "Tarefa atualizado com sucesso."
					},
					"001": {
						"enus": "Tasks updated successfully.",
						"ptbr": "Tarefas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Task deleted successfully.",
						"ptbr": "Tarefa deletado com sucesso."
					},
					"001": {
						"enus": "Tasks deleted successfully.",
						"ptbr": "Tarefas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Task.",
						"ptbr": "Erro na criação do Tarefa."
					},
					"001": {
						"enus": "Error trying to create the Tasks.",
						"ptbr": "Erro na criação dos Tarefas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Task.",
						"ptbr": "Erro tentando ler as informações do Tarefa."
					},
					"001": {
						"enus": "Error trying to read the Tasks.",
						"ptbr": "Erro tentando ler as informações dos Tarefas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Task.",
						"ptbr": "Erro tentando atualizar o Tarefa."
					},
					"001": {
						"enus": "Error trying to update the Tasks.",
						"ptbr": "Erro tentando atualizar os Tarefas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Task.",
						"ptbr": "Erro tentando deletar o Tarefa."
					},
					"001": {
						"enus": "Error trying to delete the Tasks.",
						"ptbr": "Erro tentando deletar os Tarefas."
					}
				}
			}
		},
		"action": {
			"success": {
				"create": {
					"000": {
						"enus": "Action created successfully.",
						"ptbr": "Ação criado com sucesso."
					},
					"001": {
						"enus": "Actions created successfully.",
						"ptbr": "Açãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Action information was consulted.",
						"ptbr": "A informação do Ação  foi consultada."
					},
					"001": {
						"enus": "Actions information was consulted.",
						"ptbr": "A informação dos Açãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Action updated successfully.",
						"ptbr": "Ação atualizado com sucesso."
					},
					"001": {
						"enus": "Actions updated successfully.",
						"ptbr": "Açãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Action deleted successfully.",
						"ptbr": "Ação deletado com sucesso."
					},
					"001": {
						"enus": "Actions deleted successfully.",
						"ptbr": "Açãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Action.",
						"ptbr": "Erro na criação do Ação."
					},
					"001": {
						"enus": "Error trying to create the Actions.",
						"ptbr": "Erro na criação dos Açãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Action.",
						"ptbr": "Erro tentando ler as informações do Ação."
					},
					"001": {
						"enus": "Error trying to read the Actions.",
						"ptbr": "Erro tentando ler as informações dos Açãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Action.",
						"ptbr": "Erro tentando atualizar o Ação."
					},
					"001": {
						"enus": "Error trying to update the Actions.",
						"ptbr": "Erro tentando atualizar os Açãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Action.",
						"ptbr": "Erro tentando deletar o Ação."
					},
					"001": {
						"enus": "Error trying to delete the Actions.",
						"ptbr": "Erro tentando deletar os Açãos."
					}
				}
			}
		},
		"rule": {
			"success": {
				"create": {
					"000": {
						"enus": "Rule created successfully.",
						"ptbr": "Regra criado com sucesso."
					},
					"001": {
						"enus": "Rules created successfully.",
						"ptbr": "Regras criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Rule information was consulted.",
						"ptbr": "A informação do Regra  foi consultada."
					},
					"001": {
						"enus": "Rules information was consulted.",
						"ptbr": "A informação dos Regras  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Rule updated successfully.",
						"ptbr": "Regra atualizado com sucesso."
					},
					"001": {
						"enus": "Rules updated successfully.",
						"ptbr": "Regras atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Rule deleted successfully.",
						"ptbr": "Regra deletado com sucesso."
					},
					"001": {
						"enus": "Rules deleted successfully.",
						"ptbr": "Regras deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Rule.",
						"ptbr": "Erro na criação do Regra."
					},
					"001": {
						"enus": "Error trying to create the Rules.",
						"ptbr": "Erro na criação dos Regras."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Rule.",
						"ptbr": "Erro tentando ler as informações do Regra."
					},
					"001": {
						"enus": "Error trying to read the Rules.",
						"ptbr": "Erro tentando ler as informações dos Regras."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Rule.",
						"ptbr": "Erro tentando atualizar o Regra."
					},
					"001": {
						"enus": "Error trying to update the Rules.",
						"ptbr": "Erro tentando atualizar os Regras."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Rule.",
						"ptbr": "Erro tentando deletar o Regra."
					},
					"001": {
						"enus": "Error trying to delete the Rules.",
						"ptbr": "Erro tentando deletar os Regras."
					}
				}
			}
		},
		"regulation": {
			"success": {
				"create": {
					"000": {
						"enus": "Regulation created successfully.",
						"ptbr": "Regulação criado com sucesso."
					},
					"001": {
						"enus": "Regulations created successfully.",
						"ptbr": "Regulaçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Regulation information was consulted.",
						"ptbr": "A informação do Regulação  foi consultada."
					},
					"001": {
						"enus": "Regulations information was consulted.",
						"ptbr": "A informação dos Regulaçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Regulation updated successfully.",
						"ptbr": "Regulação atualizado com sucesso."
					},
					"001": {
						"enus": "Regulations updated successfully.",
						"ptbr": "Regulaçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Regulation deleted successfully.",
						"ptbr": "Regulação deletado com sucesso."
					},
					"001": {
						"enus": "Regulations deleted successfully.",
						"ptbr": "Regulaçãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Regulation.",
						"ptbr": "Erro na criação do Regulação."
					},
					"001": {
						"enus": "Error trying to create the Regulations.",
						"ptbr": "Erro na criação dos Regulaçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Regulation.",
						"ptbr": "Erro tentando ler as informações do Regulação."
					},
					"001": {
						"enus": "Error trying to read the Regulations.",
						"ptbr": "Erro tentando ler as informações dos Regulaçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Regulation.",
						"ptbr": "Erro tentando atualizar o Regulação."
					},
					"001": {
						"enus": "Error trying to update the Regulations.",
						"ptbr": "Erro tentando atualizar os Regulaçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Regulation.",
						"ptbr": "Erro tentando deletar o Regulação."
					},
					"001": {
						"enus": "Error trying to delete the Regulations.",
						"ptbr": "Erro tentando deletar os Regulaçãos."
					}
				}
			}
		},
		"setting": {
			"success": {
				"create": {
					"000": {
						"enus": "Setting created successfully.",
						"ptbr": "Configuração criado com sucesso."
					},
					"001": {
						"enus": "Settings created successfully.",
						"ptbr": "Configuraçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Setting information was consulted.",
						"ptbr": "A informação do Configuração  foi consultada."
					},
					"001": {
						"enus": "Settings information was consulted.",
						"ptbr": "A informação dos Configuraçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Setting updated successfully.",
						"ptbr": "Configuração atualizado com sucesso."
					},
					"001": {
						"enus": "Settings updated successfully.",
						"ptbr": "Configuraçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Setting deleted successfully.",
						"ptbr": "Configuração deletado com sucesso."
					},
					"001": {
						"enus": "Settings deleted successfully.",
						"ptbr": "Configuraçãos deletados com sucesso."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Setting successfully executed.",
						"ptbr": "Configuração executado com sucesso."
					},
					"001": {
						"enus": "Setting successfully exported.",
						"ptbr": "Configuração exportado com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Setting.",
						"ptbr": "Erro na criação do Configuração."
					},
					"001": {
						"enus": "Error trying to create the Settings.",
						"ptbr": "Erro na criação dos Configuraçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Setting.",
						"ptbr": "Erro tentando ler as informações do Configuração."
					},
					"001": {
						"enus": "Error trying to read the Settings.",
						"ptbr": "Erro tentando ler as informações dos Configuraçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Setting.",
						"ptbr": "Erro tentando atualizar o Configuração."
					},
					"001": {
						"enus": "Error trying to update the Settings.",
						"ptbr": "Erro tentando atualizar os Configuraçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Setting.",
						"ptbr": "Erro tentando deletar o Configuração."
					},
					"001": {
						"enus": "Error trying to delete the Settings.",
						"ptbr": "Erro tentando deletar os Configuraçãos."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Error trying to execute the Setting.",
						"ptbr": "Erro tentando executar o Configuração."
					},
					"001": {
						"enus": "Error trying to export the Setting.",
						"ptbr": "Erro tentnado exportar o Configuração."
					}
				}
			}
		},
		"form": {
			"success": {
				"create": {
					"000": {
						"enus": "Form created successfully.",
						"ptbr": "Formulário criado com sucesso."
					},
					"001": {
						"enus": "Forms created successfully.",
						"ptbr": "Formulários criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Form information was consulted.",
						"ptbr": "A informação do Formulário  foi consultada."
					},
					"001": {
						"enus": "Forms information was consulted.",
						"ptbr": "A informação dos Formulários  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Form updated successfully.",
						"ptbr": "Formulário atualizado com sucesso."
					},
					"001": {
						"enus": "Forms updated successfully.",
						"ptbr": "Formulários atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Form deleted successfully.",
						"ptbr": "Formulário deletado com sucesso."
					},
					"001": {
						"enus": "Forms deleted successfully.",
						"ptbr": "Formulários deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Form.",
						"ptbr": "Erro na criação do Formulário."
					},
					"001": {
						"enus": "Error trying to create the Forms.",
						"ptbr": "Erro na criação dos Formulários."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Form.",
						"ptbr": "Erro tentando ler as informações do Formulário."
					},
					"001": {
						"enus": "Error trying to read the Forms.",
						"ptbr": "Erro tentando ler as informações dos Formulários."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Form.",
						"ptbr": "Erro tentando atualizar o Formulário."
					},
					"001": {
						"enus": "Error trying to update the Forms.",
						"ptbr": "Erro tentando atualizar os Formulários."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Form.",
						"ptbr": "Erro tentando deletar o Formulário."
					},
					"001": {
						"enus": "Error trying to delete the Forms.",
						"ptbr": "Erro tentando deletar os Formulários."
					}
				}
			}
		},
		"digitalFile": {
			"success": {
				"create": {
					"000": {
						"enus": "Digital File created successfully.",
						"ptbr": "Arquivo Digital criado com sucesso."
					},
					"001": {
						"enus": "Digital Files created successfully.",
						"ptbr": "Arquivos Digitales criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Digital File information was consulted.",
						"ptbr": "A informação do Arquivo Digital  foi consultada."
					},
					"001": {
						"enus": "Digital Files information was consulted.",
						"ptbr": "A informação dos Arquivos Digitales  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Digital File updated successfully.",
						"ptbr": "Arquivo Digital atualizado com sucesso."
					},
					"001": {
						"enus": "Digital Files updated successfully.",
						"ptbr": "Arquivos Digitales atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Digital File deleted successfully.",
						"ptbr": "Arquivo Digital deletado com sucesso."
					},
					"001": {
						"enus": "Digital Files deleted successfully.",
						"ptbr": "Arquivos Digitales deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Digital File.",
						"ptbr": "Erro na criação do Arquivo Digital."
					},
					"001": {
						"enus": "Error trying to create the Digital Files.",
						"ptbr": "Erro na criação dos Arquivos Digitales."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Digital File.",
						"ptbr": "Erro tentando ler as informações do Arquivo Digital."
					},
					"001": {
						"enus": "Error trying to read the Digital Files.",
						"ptbr": "Erro tentando ler as informações dos Arquivos Digitales."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Digital File.",
						"ptbr": "Erro tentando atualizar o Arquivo Digital."
					},
					"001": {
						"enus": "Error trying to update the Digital Files.",
						"ptbr": "Erro tentando atualizar os Arquivos Digitales."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Digital File.",
						"ptbr": "Erro tentando deletar o Arquivo Digital."
					},
					"001": {
						"enus": "Error trying to delete the Digital Files.",
						"ptbr": "Erro tentando deletar os Arquivos Digitales."
					}
				}
			}
		},
		"spedEfd": {
			"success": {
				"create": {
					"000": {
						"enus": "Sped EFD created successfully.",
						"ptbr": "SPED EFD criado com sucesso."
					},
					"001": {
						"enus": "Sped EFDs created successfully.",
						"ptbr": "Sped EFDs criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Sped EFD information was consulted.",
						"ptbr": "A informação do SPED EFD  foi consultada."
					},
					"001": {
						"enus": "Sped EFDs information was consulted.",
						"ptbr": "A informação dos Sped EFDs  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Sped EFD updated successfully.",
						"ptbr": "SPED EFD atualizado com sucesso."
					},
					"001": {
						"enus": "Sped EFDs updated successfully.",
						"ptbr": "Sped EFDs atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Sped EFD deleted successfully.",
						"ptbr": "SPED EFD deletado com sucesso."
					},
					"001": {
						"enus": "Sped EFDs deleted successfully.",
						"ptbr": "Sped EFDs deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Sped EFD.",
						"ptbr": "Erro na criação do SPED EFD."
					},
					"001": {
						"enus": "Error trying to create the Sped EFDs.",
						"ptbr": "Erro na criação dos Sped EFDs."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Sped EFD.",
						"ptbr": "Erro tentando ler as informações do SPED EFD."
					},
					"001": {
						"enus": "Error trying to read the Sped EFDs.",
						"ptbr": "Erro tentando ler as informações dos Sped EFDs."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Sped EFD.",
						"ptbr": "Erro tentando atualizar o SPED EFD."
					},
					"001": {
						"enus": "Error trying to update the Sped EFDs.",
						"ptbr": "Erro tentando atualizar os Sped EFDs."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Sped EFD.",
						"ptbr": "Erro tentando deletar o SPED EFD."
					},
					"001": {
						"enus": "Error trying to delete the Sped EFDs.",
						"ptbr": "Erro tentando deletar os Sped EFDs."
					}
				}
			}
		},
		"spedEcd": {
			"success": {
				"create": {
					"000": {
						"enus": "Sped ECD created successfully.",
						"ptbr": "SPED ECD criado com sucesso."
					},
					"001": {
						"enus": "Sped ECDs created successfully.",
						"ptbr": "Sped ECDs criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Sped ECD information was consulted.",
						"ptbr": "A informação do SPED ECD  foi consultada."
					},
					"001": {
						"enus": "Sped ECDs information was consulted.",
						"ptbr": "A informação dos Sped ECDs  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Sped ECD updated successfully.",
						"ptbr": "SPED ECD atualizado com sucesso."
					},
					"001": {
						"enus": "Sped ECDs updated successfully.",
						"ptbr": "Sped ECDs atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Sped ECD deleted successfully.",
						"ptbr": "SPED ECD deletado com sucesso."
					},
					"001": {
						"enus": "Sped ECDs deleted successfully.",
						"ptbr": "Sped ECDs deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Sped ECD.",
						"ptbr": "Erro na criação do SPED ECD."
					},
					"001": {
						"enus": "Error trying to create the Sped ECDs.",
						"ptbr": "Erro na criação dos Sped ECDs."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Sped ECD.",
						"ptbr": "Erro tentando ler as informações do SPED ECD."
					},
					"001": {
						"enus": "Error trying to read the Sped ECDs.",
						"ptbr": "Erro tentando ler as informações dos Sped ECDs."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Sped ECD.",
						"ptbr": "Erro tentando atualizar o SPED ECD."
					},
					"001": {
						"enus": "Error trying to update the Sped ECDs.",
						"ptbr": "Erro tentando atualizar os Sped ECDs."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Sped ECD.",
						"ptbr": "Erro tentando deletar o SPED ECD."
					},
					"001": {
						"enus": "Error trying to delete the Sped ECDs.",
						"ptbr": "Erro tentando deletar os Sped ECDs."
					}
				}
			}
		},
		"spedEcf": {
			"success": {
				"create": {
					"000": {
						"enus": "Sped ECF created successfully.",
						"ptbr": "SPED ECF criado com sucesso."
					},
					"001": {
						"enus": "Sped ECFs created successfully.",
						"ptbr": "Sped ECFs criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Sped ECF information was consulted.",
						"ptbr": "A informação do SPED ECF  foi consultada."
					},
					"001": {
						"enus": "Sped ECFs information was consulted.",
						"ptbr": "A informação dos Sped ECFs  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Sped ECF updated successfully.",
						"ptbr": "SPED ECF atualizado com sucesso."
					},
					"001": {
						"enus": "Sped ECFs updated successfully.",
						"ptbr": "Sped ECFs atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Sped ECF deleted successfully.",
						"ptbr": "SPED ECF deletado com sucesso."
					},
					"001": {
						"enus": "Sped ECFs deleted successfully.",
						"ptbr": "Sped ECFs deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Sped ECF.",
						"ptbr": "Erro na criação do SPED ECF."
					},
					"001": {
						"enus": "Error trying to create the Sped ECFs.",
						"ptbr": "Erro na criação dos Sped ECFs."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Sped ECF.",
						"ptbr": "Erro tentando ler as informações do SPED ECF."
					},
					"001": {
						"enus": "Error trying to read the Sped ECFs.",
						"ptbr": "Erro tentando ler as informações dos Sped ECFs."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Sped ECF.",
						"ptbr": "Erro tentando atualizar o SPED ECF."
					},
					"001": {
						"enus": "Error trying to update the Sped ECFs.",
						"ptbr": "Erro tentando atualizar os Sped ECFs."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Sped ECF.",
						"ptbr": "Erro tentando deletar o SPED ECF."
					},
					"001": {
						"enus": "Error trying to delete the Sped ECFs.",
						"ptbr": "Erro tentando deletar os Sped ECFs."
					}
				}
			}
		},
		"an3": {
			"success": {
				"create": {
					"000": {
						"enus": "AN3 created successfully.",
						"ptbr": "AN3 criado com sucesso."
					},
					"001": {
						"enus": "AN3s created successfully.",
						"ptbr": "AN3s criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "AN3 information was consulted.",
						"ptbr": "A informação do AN3  foi consultada."
					},
					"001": {
						"enus": "AN3s information was consulted.",
						"ptbr": "A informação dos AN3s  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "AN3 updated successfully.",
						"ptbr": "AN3 atualizado com sucesso."
					},
					"001": {
						"enus": "AN3s updated successfully.",
						"ptbr": "AN3s atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "AN3 deleted successfully.",
						"ptbr": "AN3 deletado com sucesso."
					},
					"001": {
						"enus": "AN3s deleted successfully.",
						"ptbr": "AN3s deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the AN3.",
						"ptbr": "Erro na criação do AN3."
					},
					"001": {
						"enus": "Error trying to create the AN3s.",
						"ptbr": "Erro na criação dos AN3s."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the AN3.",
						"ptbr": "Erro tentando ler as informações do AN3."
					},
					"001": {
						"enus": "Error trying to read the AN3s.",
						"ptbr": "Erro tentando ler as informações dos AN3s."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the AN3.",
						"ptbr": "Erro tentando atualizar o AN3."
					},
					"001": {
						"enus": "Error trying to update the AN3s.",
						"ptbr": "Erro tentando atualizar os AN3s."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the AN3.",
						"ptbr": "Erro tentando deletar o AN3."
					},
					"001": {
						"enus": "Error trying to delete the AN3s.",
						"ptbr": "Erro tentando deletar os AN3s."
					}
				}
			}
		},
		"an4": {
			"success": {
				"create": {
					"000": {
						"enus": "AN4 created successfully.",
						"ptbr": "AN4 criado com sucesso."
					},
					"001": {
						"enus": "AN4s created successfully.",
						"ptbr": "AN4s criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "AN4 information was consulted.",
						"ptbr": "A informação do AN4  foi consultada."
					},
					"001": {
						"enus": "AN4s information was consulted.",
						"ptbr": "A informação dos AN4s  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "AN4 updated successfully.",
						"ptbr": "AN4 atualizado com sucesso."
					},
					"001": {
						"enus": "AN4s updated successfully.",
						"ptbr": "AN4s atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "AN4 deleted successfully.",
						"ptbr": "AN4 deletado com sucesso."
					},
					"001": {
						"enus": "AN4s deleted successfully.",
						"ptbr": "AN4s deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the AN4.",
						"ptbr": "Erro na criação do AN4."
					},
					"001": {
						"enus": "Error trying to create the AN4s.",
						"ptbr": "Erro na criação dos AN4s."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the AN4.",
						"ptbr": "Erro tentando ler as informações do AN4."
					},
					"001": {
						"enus": "Error trying to read the AN4s.",
						"ptbr": "Erro tentando ler as informações dos AN4s."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the AN4.",
						"ptbr": "Erro tentando atualizar o AN4."
					},
					"001": {
						"enus": "Error trying to update the AN4s.",
						"ptbr": "Erro tentando atualizar os AN4s."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the AN4.",
						"ptbr": "Erro tentando deletar o AN4."
					},
					"001": {
						"enus": "Error trying to delete the AN4s.",
						"ptbr": "Erro tentando deletar os AN4s."
					}
				}
			}
		},
		"panel": {
			"success": {
				"create": {
					"000": {
						"enus": "Panel created successfully.",
						"ptbr": "Painel criado com sucesso."
					},
					"001": {
						"enus": "Panels created successfully.",
						"ptbr": "Paineles criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Panel information was consulted.",
						"ptbr": "A informação do Painel  foi consultada."
					},
					"001": {
						"enus": "Panels information was consulted.",
						"ptbr": "A informação dos Paineles  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Panel updated successfully.",
						"ptbr": "Painel atualizado com sucesso."
					},
					"001": {
						"enus": "Panels updated successfully.",
						"ptbr": "Paineles atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Panel deleted successfully.",
						"ptbr": "Painel deletado com sucesso."
					},
					"001": {
						"enus": "Panels deleted successfully.",
						"ptbr": "Paineles deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Panel.",
						"ptbr": "Erro na criação do Painel."
					},
					"001": {
						"enus": "Error trying to create the Panels.",
						"ptbr": "Erro na criação dos Paineles."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Panel.",
						"ptbr": "Erro tentando ler as informações do Painel."
					},
					"001": {
						"enus": "Error trying to read the Panels.",
						"ptbr": "Erro tentando ler as informações dos Paineles."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Panel.",
						"ptbr": "Erro tentando atualizar o Painel."
					},
					"001": {
						"enus": "Error trying to update the Panels.",
						"ptbr": "Erro tentando atualizar os Paineles."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Panel.",
						"ptbr": "Erro tentando deletar o Painel."
					},
					"001": {
						"enus": "Error trying to delete the Panels.",
						"ptbr": "Erro tentando deletar os Paineles."
					}
				}
			}
		},
		"registry": {
			"success": {
				"create": {
					"000": {
						"enus": "Registry created successfully.",
						"ptbr": "Cadastro criado com sucesso."
					},
					"001": {
						"enus": "Registries created successfully.",
						"ptbr": "Cadastros criados com sucesso."
					},
					"002": {
						"enus": "An adjustment code register was created.",
						"ptbr": "Foi criado um código de ajuste."
					},
					"003": {
						"enus": "An adjustment code association register was created.",
						"ptbr": "Foi criada uma associação de código de ajuste."
					},
					"004": {
						"enus": "A revenue code register was created.",
						"ptbr": "Foi criado um código da receita."
					},
					"005": {
						"enus": "An Accounting Code register was created.",
						"ptbr": "Foi criado um registro de Códigos de Contabilização."
					},
					"006": {
						"enus": "An Accounting Code register was created.",
						"ptbr": "Foi criado um registro de Códigos de Contabilização."
					},
					"010": {
						"enus": "A Legal Representative register was created.",
						"ptbr": "Foi criado um registro de Representantes Legais."
					},
					"011": {
						"enus": "An Opening Balances register was created.",
						"ptbr": "Foi criado um registro de Saldos Iniciais."
					},
					"012": {
						"enus": "A Country/Coin register was created.",
						"ptbr": "Foi criado um registro de País/Moeda."
					},
					"013": {
						"enus": "A NBS register was created.",
						"ptbr": "Foi criado um registro de NBS."
					},
					"014": {
						"enus": "A Framework register was created.",
						"ptbr": "Foi criado um registro de Enquadramento."
					},
					"015": {
						"enus": "A RAS Register was created.",
						"ptbr": "Foi criado um Registro RAS."
					},
					"016": {
						"enus": "A RP Register was created.",
						"ptbr": "Foi criado um Registro RP."
					},
					"017": {
						"enus": "A RVS Register was created.",
						"ptbr": "Foi criado um Registro RVS."
					},
					"018": {
						"enus": "A RF Register was created.",
						"ptbr": "Foi criado um Registro RF."
					},
					"019": {
						"enus": "A Fiscal Documents - ICMS register was created.",
						"ptbr": "Foi criado um registro de Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An Adjustment Code for IPI Calculation register was created.",
						"ptbr": "Foi criado um registro de Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "A Code Contributions Adjustments for Credits register was created.",
						"ptbr": "Foi criado um registro de Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "A Codes of Adjustment Calculation of ICMS and ICMS-ST register was created.",
						"ptbr": "Foi criado um registro de Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An Additional Information – Declaratory Value register was created.",
						"ptbr": "Foi criado um registro de Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "A Settings and Information Value from Fiscal Documents register was created.",
						"ptbr": "Foi criado um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "A Codes of ICMS Obligations Payable register was created.",
						"ptbr": "Foi criado um registro de Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "A Fiscal Document Remarks - Registry 0460 register was created.",
						"ptbr": "Foi criado um registro de Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An Apuration Book X SPED-Fiscal register was created.",
						"ptbr": "Foi criado um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "A Tax Credit Utilization Type - ICMS register was created.",
						"ptbr": "Foi criado um registro de Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "A Code of the Tax Situation related to Pis/Pasep and Cofins register was created.",
						"ptbr": "Foi criado um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "A Code of Social Contribution register was created.",
						"ptbr": "Foi criado um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "A Credit Type Code register was created.",
						"ptbr": "Foi criado um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "A Rule Code for Tasks register was created.",
						"ptbr": "Foi criado um registro do Código de Regra."
					},
					"033": {
						"enus": "A Task Relevance Determined by Rule register was created.",
						"ptbr": "Foi criado um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "A Code for Reopening Motives register was created.",
						"ptbr": "Foi criado um registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "A competence limit was created.",
						"ptbr": "Foi criado um limite de competência."
					},
					"036": {
						"enus": "A register for Conversion Rates was created.",
						"ptbr": "Foi criado um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "A register for a Payment Guide was created.",
						"ptbr": "Foi criado um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "A register for a ICMS-ST Transport was created.",
						"ptbr": "Foi criado um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "A register for Scenario Configuration was created.",
						"ptbr": "Foi criado um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "A register for Scenario Hierarchy was created.",
						"ptbr": "Foi criado um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "A register for Scenario Configuration was created.",
						"ptbr": "Foi criado um registro de Configuração de Cenário."
					}
				},
				"read": {
					"000": {
						"enus": "Registry information was consulted.",
						"ptbr": "A informação do Cadastro  foi consultada."
					},
					"001": {
						"enus": "Registries information was consulted.",
						"ptbr": "A informação dos Cadastros  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Registry updated successfully.",
						"ptbr": "Cadastro atualizado com sucesso."
					},
					"001": {
						"enus": "Registries updated successfully.",
						"ptbr": "Cadastros atualizados com sucesso."
					},
					"002": {
						"enus": "An adjustment code register was updated.",
						"ptbr": "Foi atualizado um código de ajuste."
					},
					"003": {
						"enus": "An adjustment code association register was updated.",
						"ptbr": "Foi atualizada uma associação de código de ajuste."
					},
					"004": {
						"enus": "A revenue code register was updated.",
						"ptbr": "Foi atualizado um código da receita."
					},
					"005": {
						"enus": "An Accounting Code register was updated.",
						"ptbr": "Foi atualizado um registro de Códigos de Contabilização."
					},
					"006": {
						"enus": "An Accounting Code register was updated.",
						"ptbr": "Foi atualizado um registro de Códigos de Contabilização."
					},
					"007": {
						"enus": "A Company register was updated.",
						"ptbr": "Foi atualizado um registro de Empresa."
					},
					"008": {
						"enus": "A Branch register was updated.",
						"ptbr": "Foi atualizado um registro de Filial."
					},
					"009": {
						"enus": "A CFOP register was updated.",
						"ptbr": "Foi atualizado um registro de CFOP."
					},
					"010": {
						"enus": "A Legal Representative register was updated.",
						"ptbr": "Foi atualizado um registro de Representantes Legais."
					},
					"011": {
						"enus": "An Opening Balances register was updated.",
						"ptbr": "Foi atualizado um registro de Saldos Iniciais."
					},
					"012": {
						"enus": "A Country/Coin register was updated.",
						"ptbr": "Foi atualizado um registro de País/Moeda."
					},
					"013": {
						"enus": "A NBS register was updated.",
						"ptbr": "Foi atualizado um registro de NBS."
					},
					"014": {
						"enus": "A Framework register was updated.",
						"ptbr": "Foi atualizado um registro de Enquadramento."
					},
					"015": {
						"enus": "A RAS Register was updated.",
						"ptbr": "Foi atualizado um Registro RAS."
					},
					"016": {
						"enus": "A RP Register was updated.",
						"ptbr": "Foi atualizado um Registro RP."
					},
					"017": {
						"enus": "A RVS Register was updated.",
						"ptbr": "Foi atualizado um Registro RVS."
					},
					"018": {
						"enus": "A RF Register was updated.",
						"ptbr": "Foi atualizado um Registro RF."
					},
					"019": {
						"enus": "A Fiscal Documents - ICMS register was updated.",
						"ptbr": "Foi atualizado um registro de Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An Adjustment Code for IPI Calculation register was updated.",
						"ptbr": "Foi atualizado um registro de Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "A Code Contributions Adjustments for Credits register was updated.",
						"ptbr": "Foi atualizado um registro de Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "A Codes of Adjustment Calculation of ICMS and ICMS-ST register was updated.",
						"ptbr": "Foi atualizado um registro de Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An Additional Information – Declaratory Value register was updated.",
						"ptbr": "Foi atualizado um registro de Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "A Settings and Information Value from Fiscal Documents register was updated.",
						"ptbr": "Foi atualizado um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "A Codes of ICMS Obligations Payable register was updated.",
						"ptbr": "Foi atualizado um registro de Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "A Fiscal Document Remarks - Registry 0460 register was updated.",
						"ptbr": "Foi atualizado um registro de Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An Apuration Book X SPED-Fiscal register was updated.",
						"ptbr": "Foi atualizado um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "A Tax Credit Utilization Type - ICMS register was updated.",
						"ptbr": "Foi atualizado um registro de Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "A Code of the Tax Situation related to Pis/Pasep and Cofins register was updated.",
						"ptbr": "Foi atualizado um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "A Code of Social Contribution register was updated.",
						"ptbr": "Foi atualizado um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "A Credit Type Code register was updated.",
						"ptbr": "Foi atualizado um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "A Rule Code for Tasks register was updated.",
						"ptbr": "Foi atualizado um registro do Código de Regra."
					},
					"033": {
						"enus": "A Task Relevance Determined by Rule register was updated.",
						"ptbr": "Foi atualizado um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "A Code for Reopening Motives register was updated.",
						"ptbr": "Foi atualizado um registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "A competence limit was updated.",
						"ptbr": "Foi atualizado um limite de competência."
					},
					"036": {
						"enus": "A register for Conversion Rates was updated.",
						"ptbr": "Foi atualizado um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "A register for a Payment Guide was updated.",
						"ptbr": "Foi atualizado um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "A register for a ICMS-ST Transport was updated.",
						"ptbr": "Foi atualizado um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "A register for Scenario Configuration was updated.",
						"ptbr": "Foi atualizado um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "A register for Scenario Hierarchy was updated.",
						"ptbr": "Foi atualizado um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "A register for Scenario Configuration was updated.",
						"ptbr": "Foi atualizado um registro de Configuração de Cenário."
					}
				},
				"delete": {
					"000": {
						"enus": "Registry deleted successfully.",
						"ptbr": "Cadastro deletado com sucesso."
					},
					"001": {
						"enus": "Registries deleted successfully.",
						"ptbr": "Cadastros deletados com sucesso."
					},
					"002": {
						"enus": "An adjustment code register was deleted.",
						"ptbr": "Foi excluído um código de ajuste."
					},
					"003": {
						"enus": "An adjustment code association register was deleted.",
						"ptbr": "Foi excluída uma associação de código de ajuste."
					},
					"004": {
						"enus": "A revenue code register was deleted.",
						"ptbr": "Foi excluído um código da receita."
					},
					"005": {
						"enus": "An Accounting Code register was deleted.",
						"ptbr": "Foi excluído um registro de Códigos de Contabilização."
					},
					"006": {
						"enus": "An Accounting Code register was deleted.",
						"ptbr": "Foi excluído um registro de Códigos de Contabilização."
					},
					"010": {
						"enus": "A Legal Representative register was deleted.",
						"ptbr": "Foi excluído um registro de Representantes Legais."
					},
					"011": {
						"enus": "An Opening Balances register was deleted.",
						"ptbr": "Foi excluído um registro de Saldos Iniciais."
					},
					"012": {
						"enus": "A Country/Coin register was deleted.",
						"ptbr": "Foi excluído um registro de País/Moeda."
					},
					"013": {
						"enus": "A NBS register was deleted.",
						"ptbr": "Foi excluído um registro de NBS."
					},
					"014": {
						"enus": "A Framework register was deleted.",
						"ptbr": "Foi excluído um registro de Enquadramento."
					},
					"015": {
						"enus": "A RAS Register was deleted.",
						"ptbr": "Foi excluído um Registro RAS."
					},
					"016": {
						"enus": "A RP Register was deleted.",
						"ptbr": "Foi excluído um Registro RP."
					},
					"017": {
						"enus": "A RVS Register was deleted.",
						"ptbr": "Foi excluído um Registro RVS."
					},
					"018": {
						"enus": "A RF Register was deleted.",
						"ptbr": "Foi excluído um Registro RF."
					},
					"019": {
						"enus": "A Fiscal Documents - ICMS register was deleted.",
						"ptbr": "Foi excluído um registro de Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An Adjustment Code for IPI Calculation register was deleted.",
						"ptbr": "Foi excluído um registro de Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "A Code Contributions Adjustments for Credits register was deleted.",
						"ptbr": "Foi excluído um registro de Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "A Codes of Adjustment Calculation of ICMS and ICMS-ST register was deleted.",
						"ptbr": "Foi excluído um registro de Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An Additional Information – Declaratory Value register was deleted.",
						"ptbr": "Foi excluído um registro de Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "A Settings and Information Value from Fiscal Documents register was deleted.",
						"ptbr": "Foi excluído um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "A Codes of ICMS Obligations Payable register was deleted.",
						"ptbr": "Foi excluído um registro de Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "A Fiscal Document Remarks - Registry 0460 register was deleted.",
						"ptbr": "Foi excluído um registro de Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An Apuration Book X SPED-Fiscal register was deleted.",
						"ptbr": "Foi excluído um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "A Tax Credit Utilization Type - ICMS register was deleted.",
						"ptbr": "Foi excluído um registro de Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "A Code of the Tax Situation related to Pis/Pasep and Cofins register was deleted.",
						"ptbr": "Foi excluído um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "A Code of Social Contribution register was deleted.",
						"ptbr": "Foi excluído um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "A Credit Type Code register was deleted.",
						"ptbr": "Foi excluído um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "A Rule Code for Tasks register was deleted.",
						"ptbr": "Foi excluído um registro do Código de Regra."
					},
					"033": {
						"enus": "A Task Relevance Determined by Rule register was deleted.",
						"ptbr": "Foi excluído um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "A Code for Reopening Motives register was deleted.",
						"ptbr": "Foi excluído um registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "A competence limit was deleted.",
						"ptbr": "Foi excluído um limite de competência."
					},
					"036": {
						"enus": "A register for Conversion Rates was deleted.",
						"ptbr": "Foi excluído um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "A register for a Payment Guide was deleted.",
						"ptbr": "Foi excluído um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "A register for a ICMS-ST Transport was deleted.",
						"ptbr": "Foi excluído um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "A register for Scenario Configuration was deleted.",
						"ptbr": "Foi excluído um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "A register for Scenario Hierarchy was deleted.",
						"ptbr": "Foi excluído um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "A register for Scenario Configuration was deleted.",
						"ptbr": "Foi excluído um registro de Configuração de Cenário."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Registry.",
						"ptbr": "Erro na criação do Cadastro."
					},
					"001": {
						"enus": "Error trying to create the Registries.",
						"ptbr": "Erro na criação dos Cadastros."
					},
					"002": {
						"enus": "An error occurred while trying to create adjustment code.",
						"ptbr": "Ocorreu um erro ao tentar criar código de ajuste."
					},
					"003": {
						"enus": "An error occurred while trying to create a register for Adjustment Code Association.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Associação de Códigos de Ajuste."
					},
					"004": {
						"enus": "An error occurred while trying to create a register for Revenue Code.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Código da Receita."
					},
					"005": {
						"enus": "An error occurred while trying to create a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Códigos de Contabilização."
					},
					"006": {
						"enus": "An error occurred while trying to create a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Códigos de Contabilização."
					},
					"010": {
						"enus": "An error occurred while trying to create a register for Legal Representatives.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Representantes Legais."
					},
					"011": {
						"enus": "An error occurred while trying to create a register for Opening Balances.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Saldos Iniciais."
					},
					"012": {
						"enus": "An error occurred while trying to create a register for Country/Coin.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de País/Moeda."
					},
					"013": {
						"enus": "An error occurred while trying to create a register for NBS.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de NBS."
					},
					"014": {
						"enus": "An error occurred while trying to create a register for Framework.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Enquadramento."
					},
					"015": {
						"enus": "An error occurred while trying to create a RAS Register.",
						"ptbr": "Ocorreu um erro ao tentar criar um Registro RAS."
					},
					"016": {
						"enus": "An error occurred while trying to create a RP Register.",
						"ptbr": "Ocorreu um erro ao tentar criar um Registro RP."
					},
					"017": {
						"enus": "An error occurred while trying to create a RVS Register.",
						"ptbr": "Ocorreu um erro ao tentar criar um Registro RVS."
					},
					"018": {
						"enus": "An error occurred while trying to create a RF Register.",
						"ptbr": "Ocorreu um erro ao tentar criar um Registro RF."
					},
					"019": {
						"enus": "An error occurred while trying to create a register for Fiscal Documents - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An error occurred while trying to create a register for Adjustment Codes for IPI Calculation.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "An error occurred while trying to create a register for Code Contributions Adjustments or Credits.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "An error occurred while trying to create a register for Codes of Adjustment Calculation of ICMS and ICMS - ST.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An error occurred while trying to create a register for Additional Information– Declaratory Values.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "An error occurred while trying to create a register for Settings and Info Values from Fiscal Documents.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "An error occurred while trying to create a register for Codes of ICMS Obligations Payable.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "An error occurred while trying to create a register for Fiscal Document Remarks - Registry 0460.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An error occurred while trying to create a register for From - To Apuration Book X SPED - Fiscal.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "An error occurred while trying to create a register for 5.5 Tax Credit Utilization Type - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "An error occurred while trying to create a Code of the Tax Situation related to Pis/Pasep and Cofins register.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "An error occurred while trying to create a Code of Social Contribution register.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "An error occurred while trying to create a Credit Type Code register.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "An error occurred while trying to create a Rule Code for Tasks register.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro do Código de Regra."
					},
					"033": {
						"enus": "An error occurred while trying to create a Task Relevance Determined by Rule register.",
						"ptbr": "Ocorreu um erro ao tentar criar um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "An error occurred while trying to create a Code for Reopening Motives register.",
						"ptbr": "Ocorreu um erro ao tentar criar registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "An error occurred while trying to create a competence limit.",
						"ptbr": "Erro ao criar um limite de competência."
					},
					"036": {
						"enus": "An error occurred while trying to create a register for Conversion Rates.",
						"ptbr": "Erro ao criar um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "An error occurred while trying to create a register for a Payment Guide.",
						"ptbr": "Erro ao criar um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "An error occurred while trying to create a register for a ICMS-ST Transport.",
						"ptbr": "Erro ao criar um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "An error occurred while trying to create a register for a Scenario Configuration.",
						"ptbr": "Erro ao criar um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "An error occurred while trying to create a register for a Scenario Hierarchy.",
						"ptbr": "Erro ao criar um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "An error occurred while trying to create a register for a Scenario Configuration.",
						"ptbr": "Erro ao criar um registro de Configuração de Cenário."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Registry.",
						"ptbr": "Erro tentando ler as informações do Cadastro."
					},
					"001": {
						"enus": "Error trying to read the Registries.",
						"ptbr": "Erro tentando ler as informações dos Cadastros."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Registry.",
						"ptbr": "Erro tentando atualizar o Cadastro."
					},
					"001": {
						"enus": "Error trying to update the Registries.",
						"ptbr": "Erro tentando atualizar os Cadastros."
					},
					"002": {
						"enus": "An error occurred while trying to update a register for Adjustment Codes.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Códigos de Ajustes."
					},
					"003": {
						"enus": "An error occurred while trying to update a register for Adjustment Code Association.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Associação de Códigos de Ajuste."
					},
					"004": {
						"enus": "An error occurred while trying to update a register for Revenue Code.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Código da Receita."
					},
					"005": {
						"enus": "An error occurred while trying to update a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Códigos de Contabilização."
					},
					"006": {
						"enus": "An error occurred while trying to update a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Códigos de Contabilização."
					},
					"007": {
						"enus": "An error occurred while trying to update a register for Companies.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Empresas."
					},
					"008": {
						"enus": "An error occurred while trying to update a register for Branches.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Filiais."
					},
					"009": {
						"enus": "An error occurred while trying to update a register for CFOP.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  CFOP."
					},
					"010": {
						"enus": "An error occurred while trying to update a register for Legal Representatives.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Representantes Legais."
					},
					"011": {
						"enus": "An error occurred while trying to update a register for Opening Balances.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Saldos Iniciais."
					},
					"012": {
						"enus": "An error occurred while trying to update a register for Country/Coin.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  País/Moeda."
					},
					"013": {
						"enus": "An error occurred while trying to update a register for NBS.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  NBS."
					},
					"014": {
						"enus": "An error occurred while trying to update a register for Framework.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Enquadramento."
					},
					"015": {
						"enus": "An error occurred while trying to update a RAS Register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um Registro RAS."
					},
					"016": {
						"enus": "An error occurred while trying to update a RP Register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um Registro RP."
					},
					"017": {
						"enus": "An error occurred while trying to update a RVS Register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um Registro RVS."
					},
					"018": {
						"enus": "An error occurred while trying to update a RF Register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um Registro RF."
					},
					"019": {
						"enus": "An error occurred while trying to update a register for Fiscal Documents - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An error occurred while trying to update a register for Adjustment Codes for IPI Calculation.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "An error occurred while trying to update a register for Code Contributions Adjustments or Credits.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "An error occurred while trying to update a register for Codes of Adjustment Calculation of ICMS and ICMS - ST.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An error occurred while trying to update a register for Additional Information– Declaratory Values.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "An error occurred while trying to update a register for Settings and Info Values from Fiscal Documents.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "An error occurred while trying to update a register for Codes of ICMS Obligations Payable.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "An error occurred while trying to update a register for Fiscal Document Remarks - Registry 0460.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An error occurred while trying to update a register for From - To Apuration Book X SPED - Fiscal.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "An error occurred while trying to update a register for Tax Credit Utilization Type - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de  Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "An error occurred while trying to update a Code of the Tax Situation related to Pis/Pasep and Cofins register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "An error occurred while trying to update a Code of Social Contribution register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "An error occurred while trying to update a Credit Type Code register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "An error occurred while trying to update a Rule Code for Tasks register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro do Código de Regra."
					},
					"033": {
						"enus": "An error occurred while trying to update a Task Relevance Determined by Rule register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "An error occurred while trying to update a Code for Reopening Motives register.",
						"ptbr": "Ocorreu um erro ao tentar atualizar registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "An error occurred while trying to update a competence limit.",
						"ptbr": "Erro ao atualizar um limite de competência."
					},
					"036": {
						"enus": "An error occurred while trying to update a register for Conversion Rates.",
						"ptbr": "Erro ao atualizar um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "An error occurred while trying to update a register for a Payment Guide.",
						"ptbr": "Erro ao atualizar um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "An error occurred while trying to update a register for a ICMS-ST Transport.",
						"ptbr": "Erro ao atualizar um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "An error occurred while trying to update a register for a Scenario Configuration.",
						"ptbr": "Erro ao atualizar um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "An error occurred while trying to update a register for a Scenario Hierarchy.",
						"ptbr": "Erro ao atualizar um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "An error occurred while trying to update a register for a Scenario Configuration.",
						"ptbr": "Erro ao atualizar um registro de Configuração de Cenário."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Registry.",
						"ptbr": "Erro tentando deletar o Cadastro."
					},
					"001": {
						"enus": "Error trying to delete the Registries.",
						"ptbr": "Erro tentando deletar os Cadastros."
					},
					"002": {
						"enus": "An error occurred while trying to delete a register for Adjustment Codes.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Códigos de Ajustes."
					},
					"003": {
						"enus": "An error occurred while trying to delete a register for Adjustment Code Association.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Associação de Códigos de Ajuste."
					},
					"004": {
						"enus": "An error occurred while trying to delete a register for Revenue Code.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Código da Receita."
					},
					"005": {
						"enus": "An error occurred while trying to delete a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Códigos de Contabilização."
					},
					"006": {
						"enus": "An error occurred while trying to delete a register for Accounting Codes.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Códigos de Contabilização."
					},
					"010": {
						"enus": "An error occurred while trying to delete a register for Legal Representatives.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Representantes Legais."
					},
					"011": {
						"enus": "An error occurred while trying to delete a register for Opening Balances.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Saldos Iniciais."
					},
					"012": {
						"enus": "An error occurred while trying to delete a register for Country/Coin.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de País/Moeda."
					},
					"013": {
						"enus": "n error occurred while trying to delete a register for NBS.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de NBS."
					},
					"014": {
						"enus": "An error occurred while trying to delete a register for Framework.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Enquadramento."
					},
					"015": {
						"enus": "An error occurred while trying to delete a RAS Register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um Registro RAS."
					},
					"016": {
						"enus": "An error occurred while trying to delete a RP Register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um Registro RP."
					},
					"017": {
						"enus": "An error occurred while trying to delete a RVS Register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um Registro RVS."
					},
					"018": {
						"enus": "An error occurred while trying to delete a RF Register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um Registro RF."
					},
					"019": {
						"enus": "An error occurred while trying to delete a register for Fiscal Documents - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Documentos Fiscais - ICMS."
					},
					"020": {
						"enus": "An error occurred while trying to delete a register for Adjustment Codes for IPI Calculation.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Código de Ajuste da Apuração do IPI."
					},
					"021": {
						"enus": "An error occurred while trying to delete a register for Code Contributions Adjustments or Credits.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Código de Ajustes de Contribuições ou Créditos."
					},
					"022": {
						"enus": "An error occurred while trying to delete a register for Codes of Adjustment Calculation of ICMS and ICMS - ST.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Código de Ajuste da Apuração do ICMS e ICMS ST."
					},
					"023": {
						"enus": "An error occurred while trying to delete a register for Additional Information– Declaratory Values.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Informações Adicionais - Valores Declaratórios."
					},
					"024": {
						"enus": "An error occurred while trying to delete a register for Settings and Info Values from Fiscal Documents.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Ajustes e Informação de Valores Provenientes de Documento Fiscal."
					},
					"025": {
						"enus": "An error occurred while trying to delete a register for Codes of ICMS Obligations Payable.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Códigos das Obrigações de ICMS a Recolher."
					},
					"026": {
						"enus": "An error occurred while trying to delete a register for Fiscal Document Remarks - Registry 0460.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Observações do Documento Fiscal - Registro 0460."
					},
					"027": {
						"enus": "An error occurred while trying to delete a register for From - To Apuration Book X SPED - Fiscal.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de DE-PARA Livro Apuração X SPED-Fiscal."
					},
					"028": {
						"enus": "An error occurred while trying to delete a register for Tax Credit Utilization Type - ICMS.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Tipos de Utilização dos Créditos Fiscais - ICMS."
					},
					"029": {
						"enus": "An error occurred while trying to delete a Code of the Tax Situation related to Pis/Pasep and Cofins register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro do Código da Situação Tributária referente ao Pis/Pasep e Cofins."
					},
					"030": {
						"enus": "An error occurred while trying to delete a Code of Social Contribution register .",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro do Código de Contribuição Social Apurada."
					},
					"031": {
						"enus": "An error occurred while trying to delete a Credit Type Code register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro do Código de Tipo de Crédito."
					},
					"032": {
						"enus": "An error occurred while trying to delete a Rule Code for Tasks register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro do Código de Regra."
					},
					"033": {
						"enus": "An error occurred while trying to delete a Task Relevance Determined by Rule register.",
						"ptbr": "Ocorreu um erro ao tentar excluir um registro de Determinação de Relevância por Regra."
					},
					"034": {
						"enus": "An error occurred while trying to delete a Code for Reopening Motives register.",
						"ptbr": "Ocorreu um erro ao tentar excluir registro do Código de Motivos de Reabertura."
					},
					"035": {
						"enus": "An error occurred while trying to delete a competence limit.",
						"ptbr": "Erro ao excluir um limite de competência."
					},
					"036": {
						"enus": "An error occurred while trying to delete a register for Conversion Rates.",
						"ptbr": "Erro ao excluir um registro de Taxas de Conversão."
					},
					"037": {
						"enus": "An error occurred while trying to delete a register for a Payment Guide.",
						"ptbr": "Erro ao excluir um registro de Determinação Automática de Guias."
					},
					"038": {
						"enus": "An error occurred while trying to delete a register for a ICMS-ST Transport.",
						"ptbr": "Erro ao excluir um registro de ICMS-ST Transporte."
					},
					"039": {
						"enus": "An error occurred while trying to delete a register for a Scenario Configuration.",
						"ptbr": "Erro ao excluir um registro de Configuração de Cenário."
					},
					"040": {
						"enus": "An error occurred while trying to delete a register for a Scenario Hierarchy.",
						"ptbr": "Erro ao excluir um registro de Hierarquia de Cenário."
					},
					"041": {
						"enus": "An error occurred while trying to delete a register for a Scenario Configuration.",
						"ptbr": "Erro ao excluir um registro de Configuração de Cenário."
					}
				}
			}
		},
		"adjustment": {
			"success": {
				"create": {
					"000": {
						"enus": "Adjustment created successfully.",
						"ptbr": "Ajuste criado com sucesso."
					},
					"001": {
						"enus": "Adjustments created successfully.",
						"ptbr": "Ajustes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Adjustment information was consulted.",
						"ptbr": "A informação do Ajuste  foi consultada."
					},
					"001": {
						"enus": "Adjustments information was consulted.",
						"ptbr": "A informação dos Ajustes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Adjustment updated successfully.",
						"ptbr": "Ajuste atualizado com sucesso."
					},
					"001": {
						"enus": "Adjustments updated successfully.",
						"ptbr": "Ajustes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Adjustment deleted successfully.",
						"ptbr": "Ajuste deletado com sucesso."
					},
					"001": {
						"enus": "Adjustments deleted successfully.",
						"ptbr": "Ajustes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Adjustment.",
						"ptbr": "Erro na criação do Ajuste."
					},
					"001": {
						"enus": "Error trying to create the Adjustments.",
						"ptbr": "Erro na criação dos Ajustes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Adjustment.",
						"ptbr": "Erro tentando ler as informações do Ajuste."
					},
					"001": {
						"enus": "Error trying to read the Adjustments.",
						"ptbr": "Erro tentando ler as informações dos Ajustes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Adjustment.",
						"ptbr": "Erro tentando atualizar o Ajuste."
					},
					"001": {
						"enus": "Error trying to update the Adjustments.",
						"ptbr": "Erro tentando atualizar os Ajustes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Adjustment.",
						"ptbr": "Erro tentando deletar o Ajuste."
					},
					"001": {
						"enus": "Error trying to delete the Adjustments.",
						"ptbr": "Erro tentando deletar os Ajustes."
					}
				}
			}
		},
		"ruleAssigment": {
			"success": {
				"create": {
					"000": {
						"enus": "Rule Assigment created successfully.",
						"ptbr": "Atribuição de Regra criado com sucesso."
					},
					"001": {
						"enus": "Rule Assigments created successfully.",
						"ptbr": "Atribuição de Regras criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Rule Assigment information was consulted.",
						"ptbr": "A informação do Atribuição de Regra  foi consultada."
					},
					"001": {
						"enus": "Rule Assigments information was consulted.",
						"ptbr": "A informação dos Atribuição de Regras  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Rule Assigment updated successfully.",
						"ptbr": "Atribuição de Regra atualizado com sucesso."
					},
					"001": {
						"enus": "Rule Assigments updated successfully.",
						"ptbr": "Atribuição de Regras atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Rule Assigment deleted successfully.",
						"ptbr": "Atribuição de Regra deletado com sucesso."
					},
					"001": {
						"enus": "Rule Assigments deleted successfully.",
						"ptbr": "Atribuição de Regras deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Rule Assigment.",
						"ptbr": "Erro na criação do Atribuição de Regra."
					},
					"001": {
						"enus": "Error trying to create the Rule Assigments.",
						"ptbr": "Erro na criação dos Atribuição de Regras."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Rule Assigment.",
						"ptbr": "Erro tentando ler as informações do Atribuição de Regra."
					},
					"001": {
						"enus": "Error trying to read the Rule Assigments.",
						"ptbr": "Erro tentando ler as informações dos Atribuição de Regras."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Rule Assigment.",
						"ptbr": "Erro tentando atualizar o Atribuição de Regra."
					},
					"001": {
						"enus": "Error trying to update the Rule Assigments.",
						"ptbr": "Erro tentando atualizar os Atribuição de Regras."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Rule Assigment.",
						"ptbr": "Erro tentando deletar o Atribuição de Regra."
					},
					"001": {
						"enus": "Error trying to delete the Rule Assigments.",
						"ptbr": "Erro tentando deletar os Atribuição de Regras."
					}
				}
			}
		},
		"adjustmentSchedule": {
			"success": {
				"create": {
					"000": {
						"enus": "Adjustment Schedule created successfully.",
						"ptbr": "Cronograma de Ajuste criado com sucesso."
					},
					"001": {
						"enus": "Adjustment Schedules created successfully.",
						"ptbr": "Cronograma de Ajustes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Adjustment Schedule information was consulted.",
						"ptbr": "A informação do Cronograma de Ajuste  foi consultada."
					},
					"001": {
						"enus": "Adjustment Schedules information was consulted.",
						"ptbr": "A informação dos Cronograma de Ajustes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Adjustment Schedule updated successfully.",
						"ptbr": "Cronograma de Ajuste atualizado com sucesso."
					},
					"001": {
						"enus": "Adjustment Schedules updated successfully.",
						"ptbr": "Cronograma de Ajustes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Adjustment Schedule deleted successfully.",
						"ptbr": "Cronograma de Ajuste deletado com sucesso."
					},
					"001": {
						"enus": "Adjustment Schedules deleted successfully.",
						"ptbr": "Cronograma de Ajustes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Adjustment Schedule.",
						"ptbr": "Erro na criação do Cronograma de Ajuste."
					},
					"001": {
						"enus": "Error trying to create the Adjustment Schedules.",
						"ptbr": "Erro na criação dos Cronograma de Ajustes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Adjustment Schedule.",
						"ptbr": "Erro tentando ler as informações do Cronograma de Ajuste."
					},
					"001": {
						"enus": "Error trying to read the Adjustment Schedules.",
						"ptbr": "Erro tentando ler as informações dos Cronograma de Ajustes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Adjustment Schedule.",
						"ptbr": "Erro tentando atualizar o Cronograma de Ajuste."
					},
					"001": {
						"enus": "Error trying to update the Adjustment Schedules.",
						"ptbr": "Erro tentando atualizar os Cronograma de Ajustes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Adjustment Schedule.",
						"ptbr": "Erro tentando deletar o Cronograma de Ajuste."
					},
					"001": {
						"enus": "Error trying to delete the Adjustment Schedules.",
						"ptbr": "Erro tentando deletar os Cronograma de Ajustes."
					}
				}
			}
		},
		"payment": {
			"success": {
				"create": {
					"000": {
						"enus": "Payment created successfully.",
						"ptbr": "Pagamento criado com sucesso."
					},
					"001": {
						"enus": "Payments created successfully.",
						"ptbr": "Pagamentos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Payment information was consulted.",
						"ptbr": "A informação do Pagamento  foi consultada."
					},
					"001": {
						"enus": "Payments information was consulted.",
						"ptbr": "A informação dos Pagamentos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Payment updated successfully.",
						"ptbr": "Pagamento atualizado com sucesso."
					},
					"001": {
						"enus": "Payments updated successfully.",
						"ptbr": "Pagamentos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Payment deleted successfully.",
						"ptbr": "Pagamento deletado com sucesso."
					},
					"001": {
						"enus": "Payments deleted successfully.",
						"ptbr": "Pagamentos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Payment.",
						"ptbr": "Erro na criação do Pagamento."
					},
					"001": {
						"enus": "Error trying to create the Payments.",
						"ptbr": "Erro na criação dos Pagamentos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Payment.",
						"ptbr": "Erro tentando ler as informações do Pagamento."
					},
					"001": {
						"enus": "Error trying to read the Payments.",
						"ptbr": "Erro tentando ler as informações dos Pagamentos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Payment.",
						"ptbr": "Erro tentando atualizar o Pagamento."
					},
					"001": {
						"enus": "Error trying to update the Payments.",
						"ptbr": "Erro tentando atualizar os Pagamentos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Payment.",
						"ptbr": "Erro tentando deletar o Pagamento."
					},
					"001": {
						"enus": "Error trying to delete the Payments.",
						"ptbr": "Erro tentando deletar os Pagamentos."
					}
				}
			}
		},
		"fiscalPeriod": {
			"success": {
				"create": {
					"000": {
						"enus": "Fiscal Period created successfully.",
						"ptbr": "Período Fiscal criado com sucesso."
					},
					"001": {
						"enus": "Fiscal Periods created successfully.",
						"ptbr": "Períodos Fiscales criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Fiscal Period information was consulted.",
						"ptbr": "A informação do Período Fiscal  foi consultada."
					},
					"001": {
						"enus": "Fiscal Periods information was consulted.",
						"ptbr": "A informação dos Períodos Fiscales  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Fiscal Period updated successfully.",
						"ptbr": "Período Fiscal atualizado com sucesso."
					},
					"001": {
						"enus": "Fiscal Periods updated successfully.",
						"ptbr": "Períodos Fiscales atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Fiscal Period deleted successfully.",
						"ptbr": "Período Fiscal deletado com sucesso."
					},
					"001": {
						"enus": "Fiscal Periods deleted successfully.",
						"ptbr": "Períodos Fiscales deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Fiscal Period.",
						"ptbr": "Erro na criação do Período Fiscal."
					},
					"001": {
						"enus": "Error trying to create the Fiscal Periods.",
						"ptbr": "Erro na criação dos Períodos Fiscales."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Fiscal Period.",
						"ptbr": "Erro tentando ler as informações do Período Fiscal."
					},
					"001": {
						"enus": "Error trying to read the Fiscal Periods.",
						"ptbr": "Erro tentando ler as informações dos Períodos Fiscales."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Fiscal Period.",
						"ptbr": "Erro tentando atualizar o Período Fiscal."
					},
					"001": {
						"enus": "Error trying to update the Fiscal Periods.",
						"ptbr": "Erro tentando atualizar os Períodos Fiscales."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Fiscal Period.",
						"ptbr": "Erro tentando deletar o Período Fiscal."
					},
					"001": {
						"enus": "Error trying to delete the Fiscal Periods.",
						"ptbr": "Erro tentando deletar os Períodos Fiscales."
					}
				}
			}
		},
		"fiscalSubPeriod": {
			"success": {
				"create": {
					"000": {
						"enus": "Fiscal SubPeriod created successfully.",
						"ptbr": "Subperíodo Fiscal criado com sucesso."
					},
					"001": {
						"enus": "Fiscal SubPeriods created successfully.",
						"ptbr": "Subperíodos Fiscales criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Fiscal SubPeriod information was consulted.",
						"ptbr": "A informação do Subperíodo Fiscal  foi consultada."
					},
					"001": {
						"enus": "Fiscal SubPeriods information was consulted.",
						"ptbr": "A informação dos Subperíodos Fiscales  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Fiscal SubPeriod updated successfully.",
						"ptbr": "Subperíodo Fiscal atualizado com sucesso."
					},
					"001": {
						"enus": "Fiscal SubPeriods updated successfully.",
						"ptbr": "Subperíodos Fiscales atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Fiscal SubPeriod deleted successfully.",
						"ptbr": "Subperíodo Fiscal deletado com sucesso."
					},
					"001": {
						"enus": "Fiscal SubPeriods deleted successfully.",
						"ptbr": "Subperíodos Fiscales deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Fiscal SubPeriod.",
						"ptbr": "Erro na criação do Subperíodo Fiscal."
					},
					"001": {
						"enus": "Error trying to create the Fiscal SubPeriods.",
						"ptbr": "Erro na criação dos Subperíodos Fiscales."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Fiscal SubPeriod.",
						"ptbr": "Erro tentando ler as informações do Subperíodo Fiscal."
					},
					"001": {
						"enus": "Error trying to read the Fiscal SubPeriods.",
						"ptbr": "Erro tentando ler as informações dos Subperíodos Fiscales."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Fiscal SubPeriod.",
						"ptbr": "Erro tentando atualizar o Subperíodo Fiscal."
					},
					"001": {
						"enus": "Error trying to update the Fiscal SubPeriods.",
						"ptbr": "Erro tentando atualizar os Subperíodos Fiscales."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Fiscal SubPeriod.",
						"ptbr": "Erro tentando deletar o Subperíodo Fiscal."
					},
					"001": {
						"enus": "Error trying to delete the Fiscal SubPeriods.",
						"ptbr": "Erro tentando deletar os Subperíodos Fiscales."
					}
				}
			}
		},
		"structureGroup": {
			"success": {
				"create": {
					"000": {
						"enus": "Structure Group created successfully.",
						"ptbr": "Grupo de Estrutura criado com sucesso."
					},
					"001": {
						"enus": "Structure Groups created successfully.",
						"ptbr": "Grupos de Estruturas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Structure Group information was consulted.",
						"ptbr": "A informação do Grupo de Estrutura  foi consultada."
					},
					"001": {
						"enus": "Structure Groups information was consulted.",
						"ptbr": "A informação dos Grupos de Estruturas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Structure Group updated successfully.",
						"ptbr": "Grupo de Estrutura atualizado com sucesso."
					},
					"001": {
						"enus": "Structure Groups updated successfully.",
						"ptbr": "Grupos de Estruturas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Structure Group deleted successfully.",
						"ptbr": "Grupo de Estrutura deletado com sucesso."
					},
					"001": {
						"enus": "Structure Groups deleted successfully.",
						"ptbr": "Grupos de Estruturas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Structure Group.",
						"ptbr": "Erro na criação do Grupo de Estrutura."
					},
					"001": {
						"enus": "Error trying to create the Structure Groups.",
						"ptbr": "Erro na criação dos Grupos de Estruturas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Structure Group.",
						"ptbr": "Erro tentando ler as informações do Grupo de Estrutura."
					},
					"001": {
						"enus": "Error trying to read the Structure Groups.",
						"ptbr": "Erro tentando ler as informações dos Grupos de Estruturas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Structure Group.",
						"ptbr": "Erro tentando atualizar o Grupo de Estrutura."
					},
					"001": {
						"enus": "Error trying to update the Structure Groups.",
						"ptbr": "Erro tentando atualizar os Grupos de Estruturas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Structure Group.",
						"ptbr": "Erro tentando deletar o Grupo de Estrutura."
					},
					"001": {
						"enus": "Error trying to delete the Structure Groups.",
						"ptbr": "Erro tentando deletar os Grupos de Estruturas."
					}
				}
			}
		},
		"structureMap": {
			"success": {
				"create": {
					"000": {
						"enus": "Structure Map created successfully.",
						"ptbr": "Mapeamento de Estrutura criado com sucesso."
					},
					"001": {
						"enus": "Structure Maps created successfully.",
						"ptbr": "Mapeamentos de Estruturas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Structure Map information was consulted.",
						"ptbr": "A informação do Mapeamento de Estrutura  foi consultada."
					},
					"001": {
						"enus": "Structure Maps information was consulted.",
						"ptbr": "A informação dos Mapeamentos de Estruturas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Structure Map updated successfully.",
						"ptbr": "Mapeamento de Estrutura atualizado com sucesso."
					},
					"001": {
						"enus": "Structure Maps updated successfully.",
						"ptbr": "Mapeamentos de Estruturas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Structure Map deleted successfully.",
						"ptbr": "Mapeamento de Estrutura deletado com sucesso."
					},
					"001": {
						"enus": "Structure Maps deleted successfully.",
						"ptbr": "Mapeamentos de Estruturas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Structure Map.",
						"ptbr": "Erro na criação do Mapeamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to create the Structure Maps.",
						"ptbr": "Erro na criação dos Mapeamentos de Estruturas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Structure Map.",
						"ptbr": "Erro tentando ler as informações do Mapeamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to read the Structure Maps.",
						"ptbr": "Erro tentando ler as informações dos Mapeamentos de Estruturas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Structure Map.",
						"ptbr": "Erro tentando atualizar o Mapeamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to update the Structure Maps.",
						"ptbr": "Erro tentando atualizar os Mapeamentos de Estruturas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Structure Map.",
						"ptbr": "Erro tentando deletar o Mapeamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to delete the Structure Maps.",
						"ptbr": "Erro tentando deletar os Mapeamentos de Estruturas."
					}
				}
			}
		},
		"structureRelation": {
			"success": {
				"create": {
					"000": {
						"enus": "Structure Relation created successfully.",
						"ptbr": "Relacionamento de Estrutura criado com sucesso."
					},
					"001": {
						"enus": "Structure Relations created successfully.",
						"ptbr": "Relacionamentos de Estruturas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Structure Relation information was consulted.",
						"ptbr": "A informação do Relacionamento de Estrutura  foi consultada."
					},
					"001": {
						"enus": "Structure Relations information was consulted.",
						"ptbr": "A informação dos Relacionamentos de Estruturas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Structure Relation updated successfully.",
						"ptbr": "Relacionamento de Estrutura atualizado com sucesso."
					},
					"001": {
						"enus": "Structure Relations updated successfully.",
						"ptbr": "Relacionamentos de Estruturas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Structure Relation deleted successfully.",
						"ptbr": "Relacionamento de Estrutura deletado com sucesso."
					},
					"001": {
						"enus": "Structure Relations deleted successfully.",
						"ptbr": "Relacionamentos de Estruturas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Structure Relation.",
						"ptbr": "Erro na criação do Relacionamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to create the Structure Relations.",
						"ptbr": "Erro na criação dos Relacionamentos de Estruturas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Structure Relation.",
						"ptbr": "Erro tentando ler as informações do Relacionamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to read the Structure Relations.",
						"ptbr": "Erro tentando ler as informações dos Relacionamentos de Estruturas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Structure Relation.",
						"ptbr": "Erro tentando atualizar o Relacionamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to update the Structure Relations.",
						"ptbr": "Erro tentando atualizar os Relacionamentos de Estruturas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Structure Relation.",
						"ptbr": "Erro tentando deletar o Relacionamento de Estrutura."
					},
					"001": {
						"enus": "Error trying to delete the Structure Relations.",
						"ptbr": "Erro tentando deletar os Relacionamentos de Estruturas."
					}
				}
			}
		},
		"taxGroup": {
			"success": {
				"create": {
					"000": {
						"enus": "Tax Group created successfully.",
						"ptbr": "Grupo de Tributo criado com sucesso."
					},
					"001": {
						"enus": "Tax Groups created successfully.",
						"ptbr": "Grupos de Tributos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Tax Group information was consulted.",
						"ptbr": "A informação do Grupo de Tributo  foi consultada."
					},
					"001": {
						"enus": "Tax Groups information was consulted.",
						"ptbr": "A informação dos Grupos de Tributos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Tax Group updated successfully.",
						"ptbr": "Grupo de Tributo atualizado com sucesso."
					},
					"001": {
						"enus": "Tax Groups updated successfully.",
						"ptbr": "Grupos de Tributos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Tax Group deleted successfully.",
						"ptbr": "Grupo de Tributo deletado com sucesso."
					},
					"001": {
						"enus": "Tax Groups deleted successfully.",
						"ptbr": "Grupos de Tributos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Tax Group.",
						"ptbr": "Erro na criação do Grupo de Tributo."
					},
					"001": {
						"enus": "Error trying to create the Tax Groups.",
						"ptbr": "Erro na criação dos Grupos de Tributos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Tax Group.",
						"ptbr": "Erro tentando ler as informações do Grupo de Tributo."
					},
					"001": {
						"enus": "Error trying to read the Tax Groups.",
						"ptbr": "Erro tentando ler as informações dos Grupos de Tributos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Tax Group.",
						"ptbr": "Erro tentando atualizar o Grupo de Tributo."
					},
					"001": {
						"enus": "Error trying to update the Tax Groups.",
						"ptbr": "Erro tentando atualizar os Grupos de Tributos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Tax Group.",
						"ptbr": "Erro tentando deletar o Grupo de Tributo."
					},
					"001": {
						"enus": "Error trying to delete the Tax Groups.",
						"ptbr": "Erro tentando deletar os Grupos de Tributos."
					}
				}
			}
		},
		"documentApproval": {
			"success": {
				"create": {
					"000": {
						"enus": "Document Approval created successfully.",
						"ptbr": "Aprovação de Documento criado com sucesso."
					},
					"001": {
						"enus": "Document Approvals created successfully.",
						"ptbr": "Aprovaçãos de Documentos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Document Approval information was consulted.",
						"ptbr": "A informação do Aprovação de Documento  foi consultada."
					},
					"001": {
						"enus": "Document Approvals information was consulted.",
						"ptbr": "A informação dos Aprovaçãos de Documentos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Document Approval updated successfully.",
						"ptbr": "Aprovação de Documento atualizado com sucesso."
					},
					"001": {
						"enus": "Document Approvals updated successfully.",
						"ptbr": "Aprovaçãos de Documentos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Document Approval deleted successfully.",
						"ptbr": "Aprovação de Documento deletado com sucesso."
					},
					"001": {
						"enus": "Document Approvals deleted successfully.",
						"ptbr": "Aprovaçãos de Documentos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Document Approval.",
						"ptbr": "Erro na criação do Aprovação de Documento."
					},
					"001": {
						"enus": "Error trying to create the Document Approvals.",
						"ptbr": "Erro na criação dos Aprovaçãos de Documentos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Document Approval.",
						"ptbr": "Erro tentando ler as informações do Aprovação de Documento."
					},
					"001": {
						"enus": "Error trying to read the Document Approvals.",
						"ptbr": "Erro tentando ler as informações dos Aprovaçãos de Documentos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Document Approval.",
						"ptbr": "Erro tentando atualizar o Aprovação de Documento."
					},
					"001": {
						"enus": "Error trying to update the Document Approvals.",
						"ptbr": "Erro tentando atualizar os Aprovaçãos de Documentos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Document Approval.",
						"ptbr": "Erro tentando deletar o Aprovação de Documento."
					},
					"001": {
						"enus": "Error trying to delete the Document Approvals.",
						"ptbr": "Erro tentando deletar os Aprovaçãos de Documentos."
					}
				}
			}
		},
		"documentConsulting": {
			"success": {
				"create": {
					"000": {
						"enus": "Document Consulting created successfully.",
						"ptbr": "Consulta de Documento criado com sucesso."
					},
					"001": {
						"enus": "Document Consultings created successfully.",
						"ptbr": "Consultas de Documentos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Document Consulting information was consulted.",
						"ptbr": "A informação do Consulta de Documento  foi consultada."
					},
					"001": {
						"enus": "Document Consultings information was consulted.",
						"ptbr": "A informação dos Consultas de Documentos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Document Consulting updated successfully.",
						"ptbr": "Consulta de Documento atualizado com sucesso."
					},
					"001": {
						"enus": "Document Consultings updated successfully.",
						"ptbr": "Consultas de Documentos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Document Consulting deleted successfully.",
						"ptbr": "Consulta de Documento deletado com sucesso."
					},
					"001": {
						"enus": "Document Consultings deleted successfully.",
						"ptbr": "Consultas de Documentos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Document Consulting.",
						"ptbr": "Erro na criação do Consulta de Documento."
					},
					"001": {
						"enus": "Error trying to create the Document Consultings.",
						"ptbr": "Erro na criação dos Consultas de Documentos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Document Consulting.",
						"ptbr": "Erro tentando ler as informações do Consulta de Documento."
					},
					"001": {
						"enus": "Error trying to read the Document Consultings.",
						"ptbr": "Erro tentando ler as informações dos Consultas de Documentos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Document Consulting.",
						"ptbr": "Erro tentando atualizar o Consulta de Documento."
					},
					"001": {
						"enus": "Error trying to update the Document Consultings.",
						"ptbr": "Erro tentando atualizar os Consultas de Documentos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Document Consulting.",
						"ptbr": "Erro tentando deletar o Consulta de Documento."
					},
					"001": {
						"enus": "Error trying to delete the Document Consultings.",
						"ptbr": "Erro tentando deletar os Consultas de Documentos."
					}
				}
			}
		},
		"book": {
			"success": {
				"create": {
					"000": {
						"enus": "Book created successfully.",
						"ptbr": "Livro criado com sucesso."
					},
					"001": {
						"enus": "Books created successfully.",
						"ptbr": "Livros criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Book information was consulted.",
						"ptbr": "A informação do Livro  foi consultada."
					},
					"001": {
						"enus": "Books information was consulted.",
						"ptbr": "A informação dos Livros  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Book updated successfully.",
						"ptbr": "Livro atualizado com sucesso."
					},
					"001": {
						"enus": "Books updated successfully.",
						"ptbr": "Livros atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Book deleted successfully.",
						"ptbr": "Livro deletado com sucesso."
					},
					"001": {
						"enus": "Books deleted successfully.",
						"ptbr": "Livros deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Book.",
						"ptbr": "Erro na criação do Livro."
					},
					"001": {
						"enus": "Error trying to create the Books.",
						"ptbr": "Erro na criação dos Livros."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Book.",
						"ptbr": "Erro tentando ler as informações do Livro."
					},
					"001": {
						"enus": "Error trying to read the Books.",
						"ptbr": "Erro tentando ler as informações dos Livros."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Book.",
						"ptbr": "Erro tentando atualizar o Livro."
					},
					"001": {
						"enus": "Error trying to update the Books.",
						"ptbr": "Erro tentando atualizar os Livros."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Book.",
						"ptbr": "Erro tentando deletar o Livro."
					},
					"001": {
						"enus": "Error trying to delete the Books.",
						"ptbr": "Erro tentando deletar os Livros."
					}
				}
			}
		},
		"output": {
			"success": {
				"create": {
					"000": {
						"enus": "Output created successfully.",
						"ptbr": "Saída criado com sucesso."
					},
					"001": {
						"enus": "Outputs created successfully.",
						"ptbr": "Saídas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Output information was consulted.",
						"ptbr": "A informação do Saída  foi consultada."
					},
					"001": {
						"enus": "Outputs information was consulted.",
						"ptbr": "A informação dos Saídas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Output updated successfully.",
						"ptbr": "Saída atualizado com sucesso."
					},
					"001": {
						"enus": "Outputs updated successfully.",
						"ptbr": "Saídas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Output deleted successfully.",
						"ptbr": "Saída deletado com sucesso."
					},
					"001": {
						"enus": "Outputs deleted successfully.",
						"ptbr": "Saídas deletados com sucesso."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Output successfully executed.",
						"ptbr": "Saída executado com sucesso."
					},
					"001": {
						"enus": "Output successfully exported.",
						"ptbr": "Saída exportado com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Output.",
						"ptbr": "Erro na criação do Saída."
					},
					"001": {
						"enus": "Error trying to create the Outputs.",
						"ptbr": "Erro na criação dos Saídas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Output.",
						"ptbr": "Erro tentando ler as informações do Saída."
					},
					"001": {
						"enus": "Error trying to read the Outputs.",
						"ptbr": "Erro tentando ler as informações dos Saídas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Output.",
						"ptbr": "Erro tentando atualizar o Saída."
					},
					"001": {
						"enus": "Error trying to update the Outputs.",
						"ptbr": "Erro tentando atualizar os Saídas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Output.",
						"ptbr": "Erro tentando deletar o Saída."
					},
					"001": {
						"enus": "Error trying to delete the Outputs.",
						"ptbr": "Erro tentando deletar os Saídas."
					}
				},
				"customEvent": {
					"000": {
						"enus": "Error trying to execute the Output.",
						"ptbr": "Erro tentando executar o Saída."
					},
					"001": {
						"enus": "Error trying to export the Output.",
						"ptbr": "Erro tentnado exportar o Saída."
					}
				}
			}
		},
		"attachment": {
			"success": {
				"create": {
					"000": {
						"enus": "Attachment created successfully.",
						"ptbr": "Anexo criado com sucesso."
					},
					"001": {
						"enus": "Attachments created successfully.",
						"ptbr": "Anexos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Attachment information was consulted.",
						"ptbr": "A informação do Anexo  foi consultada."
					},
					"001": {
						"enus": "Attachments information was consulted.",
						"ptbr": "A informação dos Anexos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Attachment updated successfully.",
						"ptbr": "Anexo atualizado com sucesso."
					},
					"001": {
						"enus": "Attachments updated successfully.",
						"ptbr": "Anexos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Attachment deleted successfully.",
						"ptbr": "Anexo deletado com sucesso."
					},
					"001": {
						"enus": "Attachments deleted successfully.",
						"ptbr": "Anexos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Attachment.",
						"ptbr": "Erro na criação do Anexo."
					},
					"001": {
						"enus": "Error trying to create the Attachments.",
						"ptbr": "Erro na criação dos Anexos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Attachment.",
						"ptbr": "Erro tentando ler as informações do Anexo."
					},
					"001": {
						"enus": "Error trying to read the Attachments.",
						"ptbr": "Erro tentando ler as informações dos Anexos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Attachment.",
						"ptbr": "Erro tentando atualizar o Anexo."
					},
					"001": {
						"enus": "Error trying to update the Attachments.",
						"ptbr": "Erro tentando atualizar os Anexos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Attachment.",
						"ptbr": "Erro tentando deletar o Anexo."
					},
					"001": {
						"enus": "Error trying to delete the Attachments.",
						"ptbr": "Erro tentando deletar os Anexos."
					}
				}
			}
		},
		"image": {
			"success": {
				"create": {
					"000": {
						"enus": "Image created successfully.",
						"ptbr": "Imagem criado com sucesso."
					},
					"001": {
						"enus": "Images created successfully.",
						"ptbr": "Imagemes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Image information was consulted.",
						"ptbr": "A informação do Imagem  foi consultada."
					},
					"001": {
						"enus": "Images information was consulted.",
						"ptbr": "A informação dos Imagemes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Task updated successfully.",
						"ptbr": "Tarefa atualizado com sucesso."
					},
					"001": {
						"enus": "Images updated successfully.",
						"ptbr": "Imagemes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Image deleted successfully.",
						"ptbr": "Imagem deletado com sucesso."
					},
					"001": {
						"enus": "Images deleted successfully.",
						"ptbr": "Imagemes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Image.",
						"ptbr": "Erro na criação do Imagem."
					},
					"001": {
						"enus": "Error trying to create the Images.",
						"ptbr": "Erro na criação dos Imagemes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Image.",
						"ptbr": "Erro tentando ler as informações do Imagem."
					},
					"001": {
						"enus": "Error trying to read the Images.",
						"ptbr": "Erro tentando ler as informações dos Imagemes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Image.",
						"ptbr": "Erro tentando atualizar o Imagem."
					},
					"001": {
						"enus": "Error trying to update the Images.",
						"ptbr": "Erro tentando atualizar os Imagemes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Image.",
						"ptbr": "Erro tentando deletar o Imagem."
					},
					"001": {
						"enus": "Error trying to delete the Images.",
						"ptbr": "Erro tentando deletar os Imagemes."
					}
				}
			}
		},
		"file": {
			"success": {
				"create": {
					"000": {
						"enus": "File created successfully.",
						"ptbr": "Arquivo criado com sucesso."
					},
					"001": {
						"enus": "Files created successfully.",
						"ptbr": "Arquivos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "File information was consulted.",
						"ptbr": "A informação do Arquivo  foi consultada."
					},
					"001": {
						"enus": "Files information was consulted.",
						"ptbr": "A informação dos Arquivos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "File updated successfully.",
						"ptbr": "Arquivo atualizado com sucesso."
					},
					"001": {
						"enus": "Files updated successfully.",
						"ptbr": "Arquivos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "File deleted successfully.",
						"ptbr": "Arquivo deletado com sucesso."
					},
					"001": {
						"enus": "Files deleted successfully.",
						"ptbr": "Arquivos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the File.",
						"ptbr": "Erro na criação do Arquivo."
					},
					"001": {
						"enus": "Error trying to create the Files.",
						"ptbr": "Erro na criação dos Arquivos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the File.",
						"ptbr": "Erro tentando ler as informações do Arquivo."
					},
					"001": {
						"enus": "Error trying to read the Files.",
						"ptbr": "Erro tentando ler as informações dos Arquivos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the File.",
						"ptbr": "Erro tentando atualizar o Arquivo."
					},
					"001": {
						"enus": "Error trying to update the Files.",
						"ptbr": "Erro tentando atualizar os Arquivos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the File.",
						"ptbr": "Erro tentando deletar o Arquivo."
					},
					"001": {
						"enus": "Error trying to delete the Files.",
						"ptbr": "Erro tentando deletar os Arquivos."
					}
				}
			}
		},
		"hierarchy": {
			"success": {
				"create": {
					"000": {
						"enus": "Hierarchy created successfully.",
						"ptbr": "Hierarquia criado com sucesso."
					},
					"001": {
						"enus": "Hierarchies created successfully.",
						"ptbr": "Hierarquias criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Hierarchy information was consulted.",
						"ptbr": "A informação do Hierarquia  foi consultada."
					},
					"001": {
						"enus": "Hierarchies information was consulted.",
						"ptbr": "A informação dos Hierarquias  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Hierarchy updated successfully.",
						"ptbr": "Hierarquia atualizado com sucesso."
					},
					"001": {
						"enus": "Hierarchies updated successfully.",
						"ptbr": "Hierarquias atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Hierarchy deleted successfully.",
						"ptbr": "Hierarquia deletado com sucesso."
					},
					"001": {
						"enus": "Hierarchies deleted successfully.",
						"ptbr": "Hierarquias deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Hierarchy.",
						"ptbr": "Erro na criação do Hierarquia."
					},
					"001": {
						"enus": "Error trying to create the Hierarchies.",
						"ptbr": "Erro na criação dos Hierarquias."
					},
					"002": {
						"enus": "Name of the hierarchy configuration is being used.",
						"ptbr": "Já existe uma hierarquia com o mesmo nome."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Hierarchy.",
						"ptbr": "Erro tentando ler as informações do Hierarquia."
					},
					"001": {
						"enus": "Error trying to read the Hierarchies.",
						"ptbr": "Erro tentando ler as informações dos Hierarquias."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Hierarchy.",
						"ptbr": "Erro tentando atualizar o Hierarquia."
					},
					"001": {
						"enus": "Error trying to update the Hierarchies.",
						"ptbr": "Erro tentando atualizar os Hierarquias."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Hierarchy.",
						"ptbr": "Erro tentando deletar o Hierarquia."
					},
					"001": {
						"enus": "Error trying to delete the Hierarchies.",
						"ptbr": "Erro tentando deletar os Hierarquias."
					}
				}
			}
		},
		"processActivator": {
			"success": {
				"create": {
					"000": {
						"enus": "Process Activator created successfully.",
						"ptbr": "Ativador de Processo criado com sucesso."
					},
					"001": {
						"enus": "Process Activators created successfully.",
						"ptbr": "Ativadore de Processos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Process Activator information was consulted.",
						"ptbr": "A informação do Ativador de Processo  foi consultada."
					},
					"001": {
						"enus": "Process Activators information was consulted.",
						"ptbr": "A informação dos Ativadore de Processos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Process Activator updated successfully.",
						"ptbr": "Ativador de Processo atualizado com sucesso."
					},
					"001": {
						"enus": "Process Activators updated successfully.",
						"ptbr": "Ativadore de Processos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Process Activator deleted successfully.",
						"ptbr": "Ativador de Processo deletado com sucesso."
					},
					"001": {
						"enus": "Process Activators deleted successfully.",
						"ptbr": "Ativadore de Processos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Process Activator.",
						"ptbr": "Erro na criação do Ativador de Processo."
					},
					"001": {
						"enus": "Error trying to create the Process Activators.",
						"ptbr": "Erro na criação dos Ativadore de Processos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Process Activator.",
						"ptbr": "Erro tentando ler as informações do Ativador de Processo."
					},
					"001": {
						"enus": "Error trying to read the Process Activators.",
						"ptbr": "Erro tentando ler as informações dos Ativadore de Processos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Process Activator.",
						"ptbr": "Erro tentando atualizar o Ativador de Processo."
					},
					"001": {
						"enus": "Error trying to update the Process Activators.",
						"ptbr": "Erro tentando atualizar os Ativadore de Processos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Process Activator.",
						"ptbr": "Erro tentando deletar o Ativador de Processo."
					},
					"001": {
						"enus": "Error trying to delete the Process Activators.",
						"ptbr": "Erro tentando deletar os Ativadore de Processos."
					}
				}
			}
		},
		"request": {
			"success": {
				"create": {
					"000": {
						"enus": "Request created successfully.",
						"ptbr": "Solicitação criado com sucesso."
					},
					"001": {
						"enus": "Requests created successfully.",
						"ptbr": "Solicitaçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Request information was consulted.",
						"ptbr": "A informação do Solicitação  foi consultada."
					},
					"001": {
						"enus": "Requests information was consulted.",
						"ptbr": "A informação dos Solicitaçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Request updated successfully.",
						"ptbr": "Solicitação atualizado com sucesso."
					},
					"001": {
						"enus": "Requests updated successfully.",
						"ptbr": "Solicitaçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Request deleted successfully.",
						"ptbr": "Solicitação deletado com sucesso."
					},
					"001": {
						"enus": "Requests deleted successfully.",
						"ptbr": "Solicitaçãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Request.",
						"ptbr": "Erro na criação do Solicitação."
					},
					"001": {
						"enus": "Error trying to create the Requests.",
						"ptbr": "Erro na criação dos Solicitaçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Request.",
						"ptbr": "Erro tentando ler as informações do Solicitação."
					},
					"001": {
						"enus": "Error trying to read the Requests.",
						"ptbr": "Erro tentando ler as informações dos Solicitaçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Request.",
						"ptbr": "Erro tentando atualizar o Solicitação."
					},
					"001": {
						"enus": "Error trying to update the Requests.",
						"ptbr": "Erro tentando atualizar os Solicitaçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Request.",
						"ptbr": "Erro tentando deletar o Solicitação."
					},
					"001": {
						"enus": "Error trying to delete the Requests.",
						"ptbr": "Erro tentando deletar os Solicitaçãos."
					}
				}
			}
		},
		"table": {
			"success": {
				"create": {
					"000": {
						"enus": "Table created successfully.",
						"ptbr": "Tabela criado com sucesso."
					},
					"001": {
						"enus": "Tables created successfully.",
						"ptbr": "Tabelas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Table information was consulted.",
						"ptbr": "A informação do Tabela  foi consultada."
					},
					"001": {
						"enus": "Tables information was consulted.",
						"ptbr": "A informação dos Tabelas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Table updated successfully.",
						"ptbr": "Tabela atualizado com sucesso."
					},
					"001": {
						"enus": "Tables updated successfully.",
						"ptbr": "Tabelas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Table deleted successfully.",
						"ptbr": "Tabela deletado com sucesso."
					},
					"001": {
						"enus": "Tables deleted successfully.",
						"ptbr": "Tabelas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Table.",
						"ptbr": "Erro na criação do Tabela."
					},
					"001": {
						"enus": "Error trying to create the Tables.",
						"ptbr": "Erro na criação dos Tabelas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Table.",
						"ptbr": "Erro tentando ler as informações do Tabela."
					},
					"001": {
						"enus": "Error trying to read the Tables.",
						"ptbr": "Erro tentando ler as informações dos Tabelas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Table.",
						"ptbr": "Erro tentando atualizar o Tabela."
					},
					"001": {
						"enus": "Error trying to update the Tables.",
						"ptbr": "Erro tentando atualizar os Tabelas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Table.",
						"ptbr": "Erro tentando deletar o Tabela."
					},
					"001": {
						"enus": "Error trying to delete the Tables.",
						"ptbr": "Erro tentando deletar os Tabelas."
					}
				}
			}
		},
		"invoice": {
			"success": {
				"create": {
					"000": {
						"enus": "Invoice created successfully.",
						"ptbr": "Conta criado com sucesso."
					},
					"001": {
						"enus": "Invoices created successfully.",
						"ptbr": "Contas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Invoice information was consulted.",
						"ptbr": "A informação do Conta  foi consultada."
					},
					"001": {
						"enus": "Invoices information was consulted.",
						"ptbr": "A informação dos Contas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Invoice updated successfully.",
						"ptbr": "Conta atualizado com sucesso."
					},
					"001": {
						"enus": "Invoices updated successfully.",
						"ptbr": "Contas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Invoice deleted successfully.",
						"ptbr": "Conta deletado com sucesso."
					},
					"001": {
						"enus": "Invoices deleted successfully.",
						"ptbr": "Contas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Invoice.",
						"ptbr": "Erro na criação do Conta."
					},
					"001": {
						"enus": "Error trying to create the Invoices.",
						"ptbr": "Erro na criação dos Contas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Invoice.",
						"ptbr": "Erro tentando ler as informações do Conta."
					},
					"001": {
						"enus": "Error trying to read the Invoices.",
						"ptbr": "Erro tentando ler as informações dos Contas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Invoice.",
						"ptbr": "Erro tentando atualizar o Conta."
					},
					"001": {
						"enus": "Error trying to update the Invoices.",
						"ptbr": "Erro tentando atualizar os Contas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Invoice.",
						"ptbr": "Erro tentando deletar o Conta."
					},
					"001": {
						"enus": "Error trying to delete the Invoices.",
						"ptbr": "Erro tentando deletar os Contas."
					}
				}
			}
		},
		"component": {
			"success": {
				"create": {
					"000": {
						"enus": "Component created successfully.",
						"ptbr": "Componente criado com sucesso."
					},
					"001": {
						"enus": "Components created successfully.",
						"ptbr": "Componentes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Component information was consulted.",
						"ptbr": "A informação do Componente  foi consultada."
					},
					"001": {
						"enus": "Components information was consulted.",
						"ptbr": "A informação dos Componentes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Component updated successfully.",
						"ptbr": "Componente atualizado com sucesso."
					},
					"001": {
						"enus": "Components updated successfully.",
						"ptbr": "Componentes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Component deleted successfully.",
						"ptbr": "Componente deletado com sucesso."
					},
					"001": {
						"enus": "Components deleted successfully.",
						"ptbr": "Componentes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Component.",
						"ptbr": "Erro na criação do Componente."
					},
					"001": {
						"enus": "Error trying to create the Components.",
						"ptbr": "Erro na criação dos Componentes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Component.",
						"ptbr": "Erro tentando ler as informações do Componente."
					},
					"001": {
						"enus": "Error trying to read the Components.",
						"ptbr": "Erro tentando ler as informações dos Componentes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Component.",
						"ptbr": "Erro tentando atualizar o Componente."
					},
					"001": {
						"enus": "Error trying to update the Components.",
						"ptbr": "Erro tentando atualizar os Componentes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Component.",
						"ptbr": "Erro tentando deletar o Componente."
					},
					"001": {
						"enus": "Error trying to delete the Components.",
						"ptbr": "Erro tentando deletar os Componentes."
					}
				}
			}
		},
		"role": {
			"success": {
				"create": {
					"000": {
						"enus": "Role created successfully.",
						"ptbr": "Role criado com sucesso."
					},
					"001": {
						"enus": "Roles created successfully.",
						"ptbr": "Roles criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Role information was consulted.",
						"ptbr": "A informação do Role  foi consultada."
					},
					"001": {
						"enus": "Roles information was consulted.",
						"ptbr": "A informação dos Roles  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Role updated successfully.",
						"ptbr": "Role atualizado com sucesso."
					},
					"001": {
						"enus": "Roles updated successfully.",
						"ptbr": "Roles atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Role deleted successfully.",
						"ptbr": "Role deletado com sucesso."
					},
					"001": {
						"enus": "Roles deleted successfully.",
						"ptbr": "Roles deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Role.",
						"ptbr": "Erro na criação do Role."
					},
					"001": {
						"enus": "Error trying to create the Roles.",
						"ptbr": "Erro na criação dos Roles."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Role.",
						"ptbr": "Erro tentando ler as informações do Role."
					},
					"001": {
						"enus": "Error trying to read the Roles.",
						"ptbr": "Erro tentando ler as informações dos Roles."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Role.",
						"ptbr": "Erro tentando atualizar o Role."
					},
					"001": {
						"enus": "Error trying to update the Roles.",
						"ptbr": "Erro tentando atualizar os Roles."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Role.",
						"ptbr": "Erro tentando deletar o Role."
					},
					"001": {
						"enus": "Error trying to delete the Roles.",
						"ptbr": "Erro tentando deletar os Roles."
					}
				}
			}
		},
		"privilege": {
			"success": {
				"create": {
					"000": {
						"enus": "Privilege created successfully.",
						"ptbr": "Privilégio criado com sucesso."
					},
					"001": {
						"enus": "Privileges created successfully.",
						"ptbr": "Privilégios criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Privilege information was consulted.",
						"ptbr": "A informação do Privilégio  foi consultada."
					},
					"001": {
						"enus": "Privileges information was consulted.",
						"ptbr": "A informação dos Privilégios  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Privilege updated successfully.",
						"ptbr": "Privilégio atualizado com sucesso."
					},
					"001": {
						"enus": "Privileges updated successfully.",
						"ptbr": "Privilégios atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Privilege deleted successfully.",
						"ptbr": "Privilégio deletado com sucesso."
					},
					"001": {
						"enus": "Privileges deleted successfully.",
						"ptbr": "Privilégios deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Privilege.",
						"ptbr": "Erro na criação do Privilégio."
					},
					"001": {
						"enus": "Error trying to create the Privileges.",
						"ptbr": "Erro na criação dos Privilégios."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Privilege.",
						"ptbr": "Erro tentando ler as informações do Privilégio."
					},
					"001": {
						"enus": "Error trying to read the Privileges.",
						"ptbr": "Erro tentando ler as informações dos Privilégios."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Privilege.",
						"ptbr": "Erro tentando atualizar o Privilégio."
					},
					"001": {
						"enus": "Error trying to update the Privileges.",
						"ptbr": "Erro tentando atualizar os Privilégios."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Privilege.",
						"ptbr": "Erro tentando deletar o Privilégio."
					},
					"001": {
						"enus": "Error trying to delete the Privileges.",
						"ptbr": "Erro tentando deletar os Privilégios."
					}
				}
			}
		},
		"folder": {
			"success": {
				"create": {
					"000": {
						"enus": "Folder created successfully.",
						"ptbr": "Pasta criado com sucesso."
					},
					"001": {
						"enus": "Folders created successfully.",
						"ptbr": "Pastas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Folder information was consulted.",
						"ptbr": "A informação do Pasta  foi consultada."
					},
					"001": {
						"enus": "Folders information was consulted.",
						"ptbr": "A informação dos Pastas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Folder updated successfully.",
						"ptbr": "Pasta atualizado com sucesso."
					},
					"001": {
						"enus": "Folders updated successfully.",
						"ptbr": "Pastas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Folder deleted successfully.",
						"ptbr": "Pasta deletado com sucesso."
					},
					"001": {
						"enus": "Folders deleted successfully.",
						"ptbr": "Pastas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Folder.",
						"ptbr": "Erro na criação do Pasta."
					},
					"001": {
						"enus": "Error trying to create the Folders.",
						"ptbr": "Erro na criação dos Pastas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Folder.",
						"ptbr": "Erro tentando ler as informações do Pasta."
					},
					"001": {
						"enus": "Error trying to read the Folders.",
						"ptbr": "Erro tentando ler as informações dos Pastas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Folder.",
						"ptbr": "Erro tentando atualizar o Pasta."
					},
					"001": {
						"enus": "Error trying to update the Folders.",
						"ptbr": "Erro tentando atualizar os Pastas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Folder.",
						"ptbr": "Erro tentando deletar o Pasta."
					},
					"001": {
						"enus": "Error trying to delete the Folders.",
						"ptbr": "Erro tentando deletar os Pastas."
					}
				}
			}
		},
		"company": {
			"success": {
				"create": {
					"000": {
						"enus": "Company created successfully.",
						"ptbr": "Empresa criado com sucesso."
					},
					"001": {
						"enus": "Companies created successfully.",
						"ptbr": "Empresas criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Company information was consulted.",
						"ptbr": "A informação do Empresa  foi consultada."
					},
					"001": {
						"enus": "Companies information was consulted.",
						"ptbr": "A informação dos Empresas  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Company updated successfully.",
						"ptbr": "Empresa atualizado com sucesso."
					},
					"001": {
						"enus": "Companies updated successfully.",
						"ptbr": "Empresas atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Company deleted successfully.",
						"ptbr": "Empresa deletado com sucesso."
					},
					"001": {
						"enus": "Companies deleted successfully.",
						"ptbr": "Empresas deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Company.",
						"ptbr": "Erro na criação do Empresa."
					},
					"001": {
						"enus": "Error trying to create the Companies.",
						"ptbr": "Erro na criação dos Empresas."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Company.",
						"ptbr": "Erro tentando ler as informações do Empresa."
					},
					"001": {
						"enus": "Error trying to read the Companies.",
						"ptbr": "Erro tentando ler as informações dos Empresas."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Company.",
						"ptbr": "Erro tentando atualizar o Empresa."
					},
					"001": {
						"enus": "Error trying to update the Companies.",
						"ptbr": "Erro tentando atualizar os Empresas."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Company.",
						"ptbr": "Erro tentando deletar o Empresa."
					},
					"001": {
						"enus": "Error trying to delete the Companies.",
						"ptbr": "Erro tentando deletar os Empresas."
					}
				}
			}
		},
		"record": {
			"success": {
				"create": {
					"000": {
						"enus": "Record created successfully.",
						"ptbr": "Registro criado com sucesso."
					},
					"001": {
						"enus": "Records created successfully.",
						"ptbr": "Registros criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Record information was consulted.",
						"ptbr": "A informação do Registro  foi consultada."
					},
					"001": {
						"enus": "Records information was consulted.",
						"ptbr": "A informação dos Registros  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Record updated successfully.",
						"ptbr": "Registro atualizado com sucesso."
					},
					"001": {
						"enus": "Records updated successfully.",
						"ptbr": "Registros atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Record deleted successfully.",
						"ptbr": "Registro deletado com sucesso."
					},
					"001": {
						"enus": "Records deleted successfully.",
						"ptbr": "Registros deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Record.",
						"ptbr": "Erro na criação do Registro."
					},
					"001": {
						"enus": "Error trying to create the Records.",
						"ptbr": "Erro na criação dos Registros."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Record.",
						"ptbr": "Erro tentando ler as informações do Registro."
					},
					"001": {
						"enus": "Error trying to read the Records.",
						"ptbr": "Erro tentando ler as informações dos Registros."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Record.",
						"ptbr": "Erro tentando atualizar o Registro."
					},
					"001": {
						"enus": "Error trying to update the Records.",
						"ptbr": "Erro tentando atualizar os Registros."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Record.",
						"ptbr": "Erro tentando deletar o Registro."
					},
					"001": {
						"enus": "Error trying to delete the Records.",
						"ptbr": "Erro tentando deletar os Registros."
					}
				}
			}
		},
		"execution": {
			"success": {
				"create": {
					"000": {
						"enus": "Execution created successfully.",
						"ptbr": "Execução criado com sucesso."
					},
					"001": {
						"enus": "Executions created successfully.",
						"ptbr": "Execuçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Execution information was consulted.",
						"ptbr": "A informação do Execução  foi consultada."
					},
					"001": {
						"enus": "Executions information was consulted.",
						"ptbr": "A informação dos Execuçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Execution updated successfully.",
						"ptbr": "Execução atualizado com sucesso."
					},
					"001": {
						"enus": "Executions updated successfully.",
						"ptbr": "Execuçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Execution deleted successfully.",
						"ptbr": "Execução deletado com sucesso."
					},
					"001": {
						"enus": "Executions deleted successfully.",
						"ptbr": "Execuçãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Execution.",
						"ptbr": "Erro na criação do Execução."
					},
					"001": {
						"enus": "Error trying to create the Executions.",
						"ptbr": "Erro na criação dos Execuçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Execution.",
						"ptbr": "Erro tentando ler as informações do Execução."
					},
					"001": {
						"enus": "Error trying to read the Executions.",
						"ptbr": "Erro tentando ler as informações dos Execuçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Execution.",
						"ptbr": "Erro tentando atualizar o Execução."
					},
					"001": {
						"enus": "Error trying to update the Executions.",
						"ptbr": "Erro tentando atualizar os Execuçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Execution.",
						"ptbr": "Erro tentando deletar o Execução."
					},
					"001": {
						"enus": "Error trying to delete the Executions.",
						"ptbr": "Erro tentando deletar os Execuçãos."
					}
				}
			}
		},
		"visualization": {
			"success": {
				"create": {
					"000": {
						"enus": "Visualization created successfully.",
						"ptbr": "Visualização criado com sucesso."
					},
					"001": {
						"enus": "Visualizations created successfully.",
						"ptbr": "Visualizaçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Visualization information was consulted.",
						"ptbr": "A informação do Visualização  foi consultada."
					},
					"001": {
						"enus": "Visualizations information was consulted.",
						"ptbr": "A informação dos Visualizaçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Visualization updated successfully.",
						"ptbr": "Visualização atualizado com sucesso."
					},
					"001": {
						"enus": "Visualizations updated successfully.",
						"ptbr": "Visualizaçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Visualization deleted successfully.",
						"ptbr": "Visualização deletado com sucesso."
					},
					"001": {
						"enus": "Visualizations deleted successfully.",
						"ptbr": "Visualizaçãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Visualization.",
						"ptbr": "Erro na criação do Visualização."
					},
					"001": {
						"enus": "Error trying to create the Visualizations.",
						"ptbr": "Erro na criação dos Visualizaçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Visualization.",
						"ptbr": "Erro tentando ler as informações do Visualização."
					},
					"001": {
						"enus": "Error trying to read the Visualizations.",
						"ptbr": "Erro tentando ler as informações dos Visualizaçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Visualization.",
						"ptbr": "Erro tentando atualizar o Visualização."
					},
					"001": {
						"enus": "Error trying to update the Visualizations.",
						"ptbr": "Erro tentando atualizar os Visualizaçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Visualization.",
						"ptbr": "Erro tentando deletar o Visualização."
					},
					"001": {
						"enus": "Error trying to delete the Visualizations.",
						"ptbr": "Erro tentando deletar os Visualizaçãos."
					}
				}
			}
		},
		"system": {
			"success": {
				"update": {
					"000": {
						"enus": "System updated successfully.",
						"ptbr": "Sistema atualizado com sucesso."
					}
				}
			},
			"error": {
				"update": {
					"000": {
						"enus": "Error trying to update the System.",
						"ptbr": "Erro tentando atualizar o Sistema."
					}
				}
			}
		},
		"variant": {
			"success": {
				"create": {
					"000": {
						"enus": "Variant created successfully.",
						"ptbr": "Variante criado com sucesso."
					},
					"001": {
						"enus": "Variants created successfully.",
						"ptbr": "Variantes criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Variant information was consulted.",
						"ptbr": "A informação do Variante  foi consultada."
					},
					"001": {
						"enus": "Variants information was consulted.",
						"ptbr": "A informação dos Variantes  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Variant updated successfully.",
						"ptbr": "Variante atualizado com sucesso."
					},
					"001": {
						"enus": "Variants updated successfully.",
						"ptbr": "Variantes atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Variant deleted successfully.",
						"ptbr": "Variante deletado com sucesso."
					},
					"001": {
						"enus": "Variants deleted successfully.",
						"ptbr": "Variantes deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Variant.",
						"ptbr": "Erro na criação do Variante."
					},
					"001": {
						"enus": "Error trying to create the Variants.",
						"ptbr": "Erro na criação dos Variantes."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Variant.",
						"ptbr": "Erro tentando ler as informações do Variante."
					},
					"001": {
						"enus": "Error trying to read the Variants.",
						"ptbr": "Erro tentando ler as informações dos Variantes."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Variant.",
						"ptbr": "Erro tentando atualizar o Variante."
					},
					"001": {
						"enus": "Error trying to update the Variants.",
						"ptbr": "Erro tentando atualizar os Variantes."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Variant.",
						"ptbr": "Erro tentando deletar o Variante."
					},
					"001": {
						"enus": "Error trying to delete the Variants.",
						"ptbr": "Erro tentando deletar os Variantes."
					}
				}
			}
		},
		"service": {
			"success": {
				"create": {
					"000": {
						"enus": "Service created successfully.",
						"ptbr": "Serviço criado com sucesso."
					},
					"001": {
						"enus": "Services created successfully.",
						"ptbr": "Serviços criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Service information was consulted.",
						"ptbr": "A informação do Serviço  foi consultada."
					},
					"001": {
						"enus": "Services information was consulted.",
						"ptbr": "A informação dos Serviços  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Service updated successfully.",
						"ptbr": "Serviço atualizado com sucesso."
					},
					"001": {
						"enus": "Services updated successfully.",
						"ptbr": "Serviços atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Service deleted successfully.",
						"ptbr": "Serviço deletado com sucesso."
					},
					"001": {
						"enus": "Services deleted successfully.",
						"ptbr": "Serviços deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Service.",
						"ptbr": "Erro na criação do Serviço."
					},
					"001": {
						"enus": "Error trying to create the Services.",
						"ptbr": "Erro na criação dos Serviços."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Service.",
						"ptbr": "Erro tentando ler as informações do Serviço."
					},
					"001": {
						"enus": "Error trying to read the Services.",
						"ptbr": "Erro tentando ler as informações dos Serviços."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Service.",
						"ptbr": "Erro tentando atualizar o Serviço."
					},
					"001": {
						"enus": "Error trying to update the Services.",
						"ptbr": "Erro tentando atualizar os Serviços."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Service.",
						"ptbr": "Erro tentando deletar o Serviço."
					},
					"001": {
						"enus": "Error trying to delete the Services.",
						"ptbr": "Erro tentando deletar os Serviços."
					}
				}
			}
		},
		"adapter": {
			"success": {
				"create": {
					"000": {
						"enus": "Adapter created successfully.",
						"ptbr": "Adaptador criado com sucesso."
					},
					"001": {
						"enus": "Adapters created successfully.",
						"ptbr": "Adaptadores criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Adapter information was consulted.",
						"ptbr": "A informação do Adaptador  foi consultada."
					},
					"001": {
						"enus": "Adapters information was consulted.",
						"ptbr": "A informação dos Adaptadores  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Adapter updated successfully.",
						"ptbr": "Adaptador atualizado com sucesso."
					},
					"001": {
						"enus": "Adapters updated successfully.",
						"ptbr": "Adaptadores atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Adapter deleted successfully.",
						"ptbr": "Adaptador deletado com sucesso."
					},
					"001": {
						"enus": "Adapters deleted successfully.",
						"ptbr": "Adaptadores deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Adapter.",
						"ptbr": "Erro na criação do Adaptador."
					},
					"001": {
						"enus": "Error trying to create the Adapters.",
						"ptbr": "Erro na criação dos Adaptadores."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Adapter.",
						"ptbr": "Erro tentando ler as informações do Adaptador."
					},
					"001": {
						"enus": "Error trying to read the Adapters.",
						"ptbr": "Erro tentando ler as informações dos Adaptadores."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Adapter.",
						"ptbr": "Erro tentando atualizar o Adaptador."
					},
					"001": {
						"enus": "Error trying to update the Adapters.",
						"ptbr": "Erro tentando atualizar os Adaptadores."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Adapter.",
						"ptbr": "Erro tentando deletar o Adaptador."
					},
					"001": {
						"enus": "Error trying to delete the Adapters.",
						"ptbr": "Erro tentando deletar os Adaptadores."
					}
				}
			}
		},
		"summary": {
			"success": {
				"create": {
					"000": {
						"enus": "Summary created successfully.",
						"ptbr": "Resumo criado com sucesso."
					},
					"001": {
						"enus": "Summaries created successfully.",
						"ptbr": "Resumos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Summary information was consulted.",
						"ptbr": "A informação do Resumo  foi consultada."
					},
					"001": {
						"enus": "Summaries information was consulted.",
						"ptbr": "A informação dos Resumos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Summary updated successfully.",
						"ptbr": "Resumo atualizado com sucesso."
					},
					"001": {
						"enus": "Summaries updated successfully.",
						"ptbr": "Resumos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Summary deleted successfully.",
						"ptbr": "Resumo deletado com sucesso."
					},
					"001": {
						"enus": "Summaries deleted successfully.",
						"ptbr": "Resumos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Summary.",
						"ptbr": "Erro na criação do Resumo."
					},
					"001": {
						"enus": "Error trying to create the Summaries.",
						"ptbr": "Erro na criação dos Resumos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Summary.",
						"ptbr": "Erro tentando ler as informações do Resumo."
					},
					"001": {
						"enus": "Error trying to read the Summaries.",
						"ptbr": "Erro tentando ler as informações dos Resumos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Summary.",
						"ptbr": "Erro tentando atualizar o Resumo."
					},
					"001": {
						"enus": "Error trying to update the Summaries.",
						"ptbr": "Erro tentando atualizar os Resumos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Summary.",
						"ptbr": "Erro tentando deletar o Resumo."
					},
					"001": {
						"enus": "Error trying to delete the Summaries.",
						"ptbr": "Erro tentando deletar os Resumos."
					}
				}
			}
		},
		"correction": {
			"success": {
				"create": {
					"000": {
						"enus": "Correction created successfully.",
						"ptbr": "Correção criado com sucesso."
					},
					"001": {
						"enus": "Corrections created successfully.",
						"ptbr": "Correçãos criados com sucesso."
					}
				},
				"read": {
					"000": {
						"enus": "Correction information was consulted.",
						"ptbr": "A informação do Correção  foi consultada."
					},
					"001": {
						"enus": "Corrections information was consulted.",
						"ptbr": "A informação dos Correçãos  foi consultada."
					}
				},
				"update": {
					"000": {
						"enus": "Correction updated successfully.",
						"ptbr": "Correção atualizado com sucesso."
					},
					"001": {
						"enus": "Corrections updated successfully.",
						"ptbr": "Correçãos atualizados com sucesso."
					}
				},
				"delete": {
					"000": {
						"enus": "Correction deleted successfully.",
						"ptbr": "Correção deletado com sucesso."
					},
					"001": {
						"enus": "Corrections deleted successfully.",
						"ptbr": "Correçãos deletados com sucesso."
					}
				}
			},
			"error": {
				"create": {
					"000": {
						"enus": "Error trying to create the Correction.",
						"ptbr": "Erro na criação do Correção."
					},
					"001": {
						"enus": "Error trying to create the Corrections.",
						"ptbr": "Erro na criação dos Correçãos."
					}
				},
				"read": {
					"000": {
						"enus": "Error trying to read the Correction.",
						"ptbr": "Erro tentando ler as informações do Correção."
					},
					"001": {
						"enus": "Error trying to read the Corrections.",
						"ptbr": "Erro tentando ler as informações dos Correçãos."
					}
				},
				"update": {
					"000": {
						"enus": "Error trying to update the Correction.",
						"ptbr": "Erro tentando atualizar o Correção."
					},
					"001": {
						"enus": "Error trying to update the Corrections.",
						"ptbr": "Erro tentando atualizar os Correçãos."
					}
				},
				"delete": {
					"000": {
						"enus": "Error trying to delete the Correction.",
						"ptbr": "Erro tentando deletar o Correção."
					},
					"001": {
						"enus": "Error trying to delete the Corrections.",
						"ptbr": "Erro tentando deletar os Correçãos."
					}
				}
			}
		}
	}
};