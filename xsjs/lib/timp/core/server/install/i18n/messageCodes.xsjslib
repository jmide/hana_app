$.import('timp.core.server.install.i18n', 'i18n');
const i18n = $.timp.core.server.install.i18n.i18n.value;

this.categoriesMap = {
	"create": "0",
	"read": "1",
	"update": "2",
	"delete": "3",
	"customEvent": "4",
	"internalError": "5",
	"paramsError": "6",
	"routeNotFound": "7",
	"authenticationError": "8",
	"insufficientPrivileges": "9"
};

this.objectsMap = {
	"generic": "00",
	"user": "01",
	"group": "02",
	"package": "03",
	"layout": "04",
	"report": "05",
	"structure": "06",
	"installer": "07",
	"process": "08",
	"task": "09",
	"action": "10",
	"rule": "11",
	"regulation": "12",
	"setting": "13",
	"form": "14",
	"digitalFile": "15",
	"spedEfd": "16",
	"spedEcd": "17",
	"spedEcf": "18",
	"an3": "19",
	"an4": "20",
	"panel": "21",
	"registry": "22",
	"adjustment": "23",
	"ruleAssigment": "24",
	"adjustmentSchedule": "25",
	"payment": "26",
	"fiscalPeriod": "27",
	"fiscalSubPeriod": "28",
	"structureGroup": "29",
	"structureMap": "30",
	"structureRelation": "31",
	"taxGroup": "32",
	"documentApproval": "33",
	"documentConsulting": "34",
	"book": "35",
	"output": "36",
	"attachment": "37",
	"image": "38",
	"file": "39",
	"hierarchy": "40",
	"processActivator": "41",
	"request": "42",
	"table": "43",
	"invoice": "44",
	"component": "45",
	"role": "46",
	"privilege": "47",
	"folder": "48",
	"company": "49",
	"record": "50",
	"execution": "51",
	"visualization": "52",
	"system": "53",
	"variant": "54",
	"service": "55",
	"adapter": "56",
	"summary": "57",
	"correction": "58"
};

const lodash = $.lodash;
const _this = this; 
this.setMessageByType = function(objects, result, objectKey, type) {
	lodash.forEach(objects, function(category, categoryKey) {
		lodash.forEach(category, function(subcategory, subcategoryKey) {
			let code = 'CORE0' + _this.objectsMap[objectKey] + type + _this.categoriesMap[categoryKey] + subcategoryKey;
			result.push({
				code: code,
				lang: 'enus',
				text: subcategory.enus
			});
			result.push({
				code: code,
				lang: 'ptbr',
				text: subcategory.ptbr
			});
		});
	});
};

this.generateCodes = function(messagesObject) {
	if (lodash.isNil(messagesObject)) {
		return [];
	}
	
	let objectsKeys = lodash.keys(messagesObject);

	return lodash.reduce(objectsKeys, function(prev, objectKey) {
		let successObjects = messagesObject[objectKey]['success'];
		let errorObjects = messagesObject[objectKey]['error'];

		_this.setMessageByType(successObjects, prev, objectKey, 0);
		_this.setMessageByType(errorObjects, prev, objectKey, 1);

		return prev;
	}, []);
}

this.getMessageCodes =  function() {
    let messages = _this.generateCodes(i18n['be']);
    messages = lodash.concat(messages, _this.generateCodes(i18n['ui']));
    return {
        identity: 'CORE::INTERNATIONALIZATION',
        'new': messages
    };
};