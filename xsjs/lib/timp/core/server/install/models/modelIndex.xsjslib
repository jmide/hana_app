$.import('timp.core.server.models', 'actions');
$.import('timp.core.server.models', 'actionsParameters');
$.import('timp.core.server.models', 'labels');
$.import('timp.core.server.models', 'images');
$.import('timp.core.server.models', 'attachments');
$.import('timp.core.server.models', 'requests');
$.import('timp.core.server.models', 'objects');
$.import('timp.core.server.models', 'fileSystem');
$.import('timp.core.server.models', 'messageCodes');
$.import('timp.core.server.models', 'periodicity');
$.import('timp.core.server.models', 'output');
// $.import('timp.core.server.models', 'users');
// $.import('timp.core.server.models', 'config');

//New Models
$.import('timp.core.server.models.tables', 'user');
$.import('timp.core.server.models.tables', 'group');
$.import('timp.core.server.models.tables', 'package');
$.import('timp.core.server.models.tables', 'proxyService');
$.import('timp.core.server.models.tables', 'component');
$.import('timp.core.server.models.tables', 'configuration');
$.import('timp.core.server.models.tables', 'dataTypeConversion');
$.import('timp.core.server.models.tables', 'log');
$.import('timp.core.server.models.tables', 'logDetails');
$.import('timp.core.server.models.tables', 'tax');
// $.import('timp.core.server.models.tables', 'label');
// $.import('timp.core.server.models.tables', 'output');
// $.import('timp.core.server.models.tables', 'fileSystem');
// $.import('timp.core.server.models.tables', 'attachments');
$.import('timp.core.server.models.tables', 'componentManual');
$.import('timp.core.server.models', 'additionalFunction');
$.import('timp.core.server.models.tables', 'LogTracer');
$.import('timp.core.server.models.tables', 'UserTaxes');
$.import('timp.core.server.models.tables', 'GroupTaxes');



this.models = [
    $.timp.core.server.models.actions.table,
    $.timp.core.server.models.actionsParameters.table,
    $.timp.core.server.models.additionalFunction.table,
    $.timp.core.server.models.images.table,
    $.timp.core.server.models.attachments.table,
    $.timp.core.server.models.requests.table,
    $.timp.core.server.models.objects.objects.table,
    $.timp.core.server.models.objects.objectTypes.table,
    $.timp.core.server.models.objects.objectsUserData.table,
    $.timp.core.server.models.objects.objectShare.table,
    $.timp.core.server.models.fileSystem.folder.table,
    $.timp.core.server.models.fileSystem.folderShare.table,
    $.timp.core.server.models.fileSystem.file.table,
    $.timp.core.server.models.fileSystem.fileShare.table,
    $.timp.core.server.models.fileSystem.fileFavs.table,
    $.timp.core.server.models.fileSystem.fileLock.table,
    $.timp.core.server.models.labels.table,
    $.timp.core.server.models.messageCodes.messageCodes.table,
    $.timp.core.server.models.messageCodes.messageCodeTexts.table,
    $.timp.core.server.models.periodicity.table,
    $.timp.core.server.models.output.output.table,
    $.timp.core.server.models.output.outputType.table,
    $.timp.core.server.models.output.outputTypeText.table,
    $.timp.core.server.models.output.outputValue.table,
    $.timp.core.server.models.tables.user.userModel,
    $.timp.core.server.models.tables.user.userPrivilegeModel,
    $.timp.core.server.models.tables.user.userOrgPrivilegeModel,
    $.timp.core.server.models.tables.user.userPackageModel,
    $.timp.core.server.models.tables.group.groupModel,
    $.timp.core.server.models.tables.group.groupUserModel,
    $.timp.core.server.models.tables.group.groupPrivilegeModel,
    $.timp.core.server.models.tables.group.groupPackageModel,
    $.timp.core.server.models.tables.group.groupOrgPrivilegeModel,
    $.timp.core.server.models.tables.package.packageModel,
    $.timp.core.server.models.tables.package.packagePrivilegeModel,
    $.timp.core.server.models.tables.proxyService.adapterModel,
    $.timp.core.server.models.tables.proxyService.serviceModel,
    $.timp.core.server.models.tables.proxyService.serviceDestinationModel,
    $.timp.core.server.models.tables.component.componentModel,
    $.timp.core.server.models.tables.component.internationalizationModel,
    $.timp.core.server.models.tables.component.privilegeModel,
    $.timp.core.server.models.tables.configuration.configurationModel,
    $.timp.core.server.models.tables.configuration.licenseModel,
    $.timp.core.server.models.tables.configuration.taxLicenseModel,
    $.timp.core.server.models.tables.configuration.fullDatesModel,
    $.timp.core.server.models.tables.configuration.alertDeleteLogModel,
    $.timp.core.server.models.tables.configuration.alertDeleteFilesModel,
    $.timp.core.server.models.tables.dataTypeConversion.dataTypeConversionModel,
    $.timp.core.server.models.tables.log.logModel,
    $.timp.core.server.models.tables.log.logDetails,
    $.timp.core.server.models.tables.log.logTCMDetails,
    

    $.timp.core.server.models.tables.logDetails.logATRModel,
    $.timp.core.server.models.tables.logDetails.logBCBModel,
    $.timp.core.server.models.tables.logDetails.logBFBModel,
    $.timp.core.server.models.tables.logDetails.logBRBModel,
    $.timp.core.server.models.tables.logDetails.logBREModel,
    $.timp.core.server.models.tables.logDetails.logBSCModel,
    $.timp.core.server.models.tables.logDetails.logDFGModel,
    $.timp.core.server.models.tables.logDetails.logMDRModel,
    $.timp.core.server.models.tables.logDetails.logTAAModel,
    $.timp.core.server.models.tables.logDetails.logTBDModel,
    $.timp.core.server.models.tables.logDetails.logTCCModel,
    $.timp.core.server.models.tables.logDetails.logTDKModel,
    $.timp.core.server.models.tables.logDetails.logTFBModel,
    $.timp.core.server.models.tables.logDetails.logTFPModel,
    $.timp.core.server.models.tables.logDetails.logTPCModel,
    
    $.timp.core.server.models.tables.LogTracer.LogTracerTable,

    $.timp.core.server.models.tables.tax.taxModel,
    $.timp.core.server.models.tables.tax.taxTypeModel,
    $.timp.core.server.models.tables.tax.taxMapModel,
    $.timp.core.server.models.tables.componentManual.table,
    $.timp.core.server.models.tables.UserTaxes.getTableModel(),
    $.timp.core.server.models.tables.GroupTaxes.getTableModel()
    // $.timp.core.server.models.tables.label.labelModel,
    // $.timp.core.server.models.tables.fileSystem.objectTypeModel,
    // $.timp.core.server.models.tables.fileSystem.folderModel,
    // $.timp.core.server.models.tables.fileSystem.folderShareModel,
    // $.timp.core.server.models.tables.fileSystem.fileModel,
    // $.timp.core.server.models.tables.fileSystem.fileShareModel,
    // $.timp.core.server.models.tables.fileSystem.fileFavoriteModel,
    // $.timp.core.server.models.tables.attachment.imageModel,
    // $.timp.core.server.models.tables.attachment.attachmentModel,
    // $.timp.core.server.models.tables.output.outputModel
];