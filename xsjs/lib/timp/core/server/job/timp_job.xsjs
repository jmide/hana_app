$.trace.error('JOB RUN OK');
$.response = new $.web.WebRequest($.net.http.POST, '/timp');
$.response.setBody = function () {};
$.request = new $.web.WebRequest($.net.http.POST, '/timp');

function runJob () {
   try {
        $.trace.error(['Job RUN bpma_api']);
        $.import('timp.bpma.server.api','api');
        var bpma_api = $.timp.bpma.server.api.api;
        bpma_api.job();
        
    } catch (e) {
        $.trace.error(['BPMA Job Failed', $.parseError(e)]);
    } 
    try {
        $.trace.error(['Job RUN dfg_api']);
        $.import("timp.dfg.server.api","api");
        var dfg_api = $.timp.dfg.server.api.api;
        dfg_api.job();
    } catch(e){
        $.trace.error(["DFG Job Failed",$.parseError(e)]);
    }
    
    try{
        $.trace.error(['BSC Job - Execution started']);
        $.import("timp.bsc.server.api","api");
        var bscApi = $.timp.bsc.server.api.api;
        bscApi.correctionController.approveByJob();
        
    }catch(e){
        $.trace.error(["BSC Job - Execution Failed",$.parseError(e)]);
    }
    try{
        $.import("timp.tfp.server.api","api");
        var tfpApi = $.timp.tfp.server.api.api;
        tfpApi.periodConfigController.openAutomaticPeriodConfigurations();
        tfpApi.subPeriodConfigController.openAutomaticSubPeriodConfigurations();
    }catch(e){
        $.trace.error(["TFP Job - Execution Failed",$.parseError(e)]);
    }
    try{
        $.trace.error(['LOG Job - Execution started']);
        $.import("timp.log.server.api","api");
        var logApi = $.timp.log.server.api.api.logRefactorController;
        logApi.deleteLogTime(); 
    }catch(e){
        $.trace.error(["LOG Job - Execution Failed",$.parseError(e)]);
    }
}
