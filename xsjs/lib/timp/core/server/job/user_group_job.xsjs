$.trace.error('JOB RUN OK');
$.response = new $.web.WebRequest($.net.http.POST, '/timp');
$.response.setBody = function () {};
$.request = new $.web.WebRequest($.net.http.POST, '/timp');

$.import('timp.core.server.api','api');
var coreApi = $.timp.core.server.api.api;

function runJob () {
    $.trace.error(['Create Groups Job - Execution Started']);
    try {
        coreApi.automaticCreateGroup();
    } catch (e) {
        $.trace.error(["Create Groups Job - Execution Failed", $.parseError(e)]);
    } 
    $.trace.error(["Verifying HANA Groups Status - Execution Started"]);
    try {
        coreApi.checkHanaGroupStatus(); 
    } catch (e) {
        $.trace.error(['Groups Status Job- Execution Failed', $.parseError(e)]);
    } 
}