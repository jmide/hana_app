$.trace.error('JOB RUN OK');
$.response = new $.web.WebRequest($.net.http.POST, '/timp');
$.response.setBody = function () {};
$.request = new $.web.WebRequest($.net.http.POST, '/timp');
$.import('timp.core.server.api','api');
var coreApi = $.timp.core.server.api.api;

function runJob () {
    try {
        $.trace.error(["User's Job - Execution started"]);
        coreApi.automaticCreateUser();
        $.trace.error(['Job Executed Successfully - Users updated']);
    } catch (e) {
        $.trace.error(["User's Job - Execution Failed: ", $.parseError(e)]);
    }
}