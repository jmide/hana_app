$.trace.error('JOB RUN OK');
$.response = new $.web.WebRequest($.net.http.POST, '/timp');
$.response.setBody = function () {};
$.request = new $.web.WebRequest($.net.http.POST, '/timp');

function runJob () {
	//MDR JOB
	try {
		$.trace.error(['MDR Job - Execution started - Job RUN mdrApi']);
		$.import('timp.mdr.server.api', 'api');
    	const jobs = $.timp.mdr.server.api.api.jobs;
    	jobs.sendMessagesParameter.runJob('DAILY', 'AFTERNOON');
	} catch (e) {
		$.trace.error(['MDR Daily Afternoon Job Failed', $.parseError(e)]);
	}
}