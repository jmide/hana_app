const _RECORDS_BY_PAGE = 15;

const _generateFindFiltersOptionsSchema = {
	properties: {
		alias: {
			type: 'string'
		},
		id: {
			type: 'integer',
			minimum: 1
		},
		search: {
			type: 'array'
		},
		paginate: {
			type: 'integer'
		},
		orderBy: {
			type: 'array'
		},
		isBaseModel: {
			type: 'boolean',
			required: true
		}
	}
};

const _generateFindFiltersFilterBySchema = {
    properties: {
        creationUser: {
            type: 'integer',
            minimum: 1
        },
        creationDate: {
            type: 'string',
            allowEmpty: false
        },
        modificationUser: {
            type: 'integer',
            minimum: 1
        },
        modificationDate: {
            type: 'string',
            allowEmpty: false
        }
    }  
};

function _parseFilterBy(options, filterBy) {
    $.lodash.forEach(filterBy, function(value, key) {
        if ($.lodash.isEqual(key, 'creationUser')) {
            options.creationUser = filterBy.creationUser;
        }
        if ($.lodash.isEqual(key, 'modificationUser')) {
            options.modificationUser = filterBy.modificationUser;
        }
        if ($.lodash.isEqual(key, 'creationDate') && !$.lodash.isEqual(new Date(filterBy.creationDate), 'Invalid Date')) {
            options.creationDate = filterBy.creationDate;
        }
        if ($.lodash.isEqual(key, 'modificationDate') && !$.lodash.isEqual(new Date(filterBy.modificationDate), 'Invalid Date')) {
            options.modificationDate = filterBy.modificationDate;
        }
    });
    return options;
}

this.generateFindFilters = function(findParameters, options, filterBy) {
	if ($.validator.validate(options, _generateFindFiltersOptionsSchema, {additionalProperties: true}).valid) {
	    if (filterBy) {
	        options = _parseFilterBy(options, filterBy);
	    }
		let whereClause = [];
		if (options.creationUser) {
			whereClause.push({
				field: (options.isBaseModel) ? 'CREATION_USER' : 'CREATION.ID_USER',
				operator: '$eq',
				value: options.creation[0]
			});
		}
		if (options.creationDate) {
			whereClause.push({
				field: (options.isBaseModel) ? 'CREATION_DATE' : 'CREATION.DATE',
				operator: '$eq',
				value: $.moment(options.creation[1]).format('YYYY-MM-DD')
			});
		}
		if (options.modificationUser) {
			whereClause.push({
				field: (options.isBaseModel) ? 'MODIFICATION_USER' : 'MODIFICATION.ID_USER',
				operator: '$eq',
				value: options.modification[0]
			});
		}
		if (options.modificationDate) {
			whereClause.push({
				field: (options.isBaseModel) ? 'MODIFICATION_DATE' : 'MODIFICATION.DATE',
				operator: '$eq',
				value: $.moment(options.modification[1]).format('YYYY-MM-DD')
			});
		}
		if (options.search && $.lodash.isString(options.search[0]) && !$.lodash.isNil(options.search[1])) {
			whereClause.push({
				field: options.search[0],
				operator: '$like',
				value: {
					function: {
						name: 'UPPER',
						params: ['%' + options.search[1] + '%']
					}
				}
			});
		}
		if (options.id) {
			whereClause.push({
				field: 'ID',
				operator: '$eq',
				value: options.id
			});
		}
		if (options.alias) {
			$.lodash.forEach(whereClause, function(value) {
				value.alias = options.alias;
			});
		}
		if (options.paginate) {
			findParameters.paginate = {
				limit: _RECORDS_BY_PAGE,
				offset: _RECORDS_BY_PAGE * (options.paginate - 1)
			};
		}
		if (options.orderBy) {
			if ($.lodash.isStringArray(options.orderBy)) {
				findParameters.orderBy = [];
				$.lodash.forEach(options.orderBy, function(value) {
					findParameters.orderBy.push({
						field: value,
						type: 'asc'
					});
				});
			}
		}
		findParameters.where = whereClause;
		return findParameters;
	} else {
		//throw bad request
	}
};