$.import('timp.core.server.libraries.external', 'cryptoJS');
const cryptoJS = $.timp.core.server.libraries.external.cryptoJS.CryptoJS;
$.import('timp.core.server.libraries.external', 'pbkdf2');
const passwordKeyDerivation = $.timp.core.server.libraries.external.pbkdf2; //Password Key Based Derivation Function - CryptoJS Extension

/**
 * Calculate a 32 bit FNV-1a hash
 * Found here: {@link https://gist.github.com/vaiorabbit/5657561}
 * More Information {@link http://isthe.com/chongo/tech/comp/fnv/}
 *
 * @param {string} str the input value
 * @param {boolean} [asString=false] set to true to return the hash value as
 *     8-digit hex string instead of an integer
 * @param {integer} [seed] optionally pass the hash of the previous chunk
 * @returns {integer|string}
 */
function generateHash(str, asString, seed) {
	/*jshint bitwise:false */
	var i, l,
		hval = (seed === undefined) ? 0x811c9dc5 : seed;

	for (i = 0, l = str.length; i < l; i++) {
		hval ^= str.charCodeAt(i);
		hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
	}
	if (asString) {
		// Convert to 8 digit hex string
		return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
	}
	return hval >>> 0;
}

/*
    Function for string encryption. Function recieves a string that is going to be encrypted, a passphrase that is required
    to generate a unique key (You need to know or store the passphrase in order to decrypt the string!), and optionally 
    the number of iterations you want to perform in order to make your password more secure (The higher the iterations the
    slower will it take to encrypt!).Function will return a cyphertext storing the salt that was used to generate the encrypted
    string, the initialization vector that AES receives (iv) and the encrypted string.
    
    @param {string} stringToEncrypt - String that you want to encrypt (base64 file or password you wish to encrypt/decrypt)
    @param {string} passphrase - Mandatory parameter that is required to lock and unlock the encrypted string
    @param {number} [iterations] - The number of iterations you wish to perform for key generation. The higher the slower and more secure the final string
    @returns {string|boolean} Returns a the cyphertext which is composed of 32 bit salt + 32 bit IV + Encrypted String or false if invalid param, error, or encryption
*/
this.encrypt = function(stringToEncrypt, passphrase, iterations) {
	try {
		if ($.lodash.isString(stringToEncrypt) && $.lodash.isString(passphrase) && !$.lodash.isEmpty(stringToEncrypt)) {
			var salt = cryptoJS.lib.WordArray.random(128 / 8);
			var iv = cryptoJS.lib.WordArray.random(128 / 8);
			var key = passwordKeyDerivation.CryptoJS.PBKDF2(generateHash(passphrase, true), salt, {
				keySize: 512 / 32,
				iterations: $.lodash.isInteger(iterations) ? iterations : 100
			});
			var encrypted = cryptoJS.AES.encrypt(stringToEncrypt, key, {
				iv: iv
			});
			var cyphertext = salt.toString() + iv.toString() + encrypted.toString();
			return cyphertext;
		} else {
			return false;
		}
	} catch (e) {
		return $.parseError(e);
	}
};
$.encrypt = this.encrypt;

/*
    Function for string decryption. Function recieves a string that is going to be decrypted, the passphrase that was used to encrypt
    the cyphertext, and optionally the same number of iterations that was used to encrypt. Function will return the decrypted cyphertext
    or false if invalid, error, or invalid decryption (invalid passphrase or cyphertext)
    
    @param {string} cyphertext - Encrypted string 
    @param {string} passphrase - Mandatory parameter that is required to unlock the encrypted string
    @param {number} [iterations] - The number of iterations you wish to perform for key generation. The higher the slower and more secure the final string
    @param {Object} [predefined] - Object with predefined key and iv
    @param {string} predefined.key - Instead of calculating key, use this key instead
    @param {string} predefined.iv - Instead of calculating the initialization vector, use this vector instead
    @returns {string|boolean} Returns decrypted string or false if invalid, error, or invalid decryption (invalid passphrase or cyphertext)
*/
this.decrypt = function(cyphertext, passphrase, iterations, predefined) {
	try {
		var decrypted;
		if ($.lodash.isString(cyphertext) && !$.lodash.isNil(predefined) && !$.lodash.isNil(predefined.key) && !$.lodash.isNil(predefined.iv)) {
			decrypted = cryptoJS.AES.decrypt(cyphertext, predefined.key, {
				iv: predefined.iv 
			});
			decrypted = decrypted.toString(cryptoJS.enc.Utf8);
			return !$.lodash.isEmpty(decrypted) ? decrypted : false;
		}
		if ($.lodash.isString(cyphertext) && $.lodash.isString(passphrase) && !$.lodash.isEmpty(cyphertext)) {
			var salt = cryptoJS.enc.Hex.parse(cyphertext.substr(0, 32));
			var iv = cryptoJS.enc.Hex.parse(cyphertext.substr(32, 32));
			var encrypted = cyphertext.substring(64);
			var key = passwordKeyDerivation.CryptoJS.PBKDF2(generateHash(passphrase, true), salt, {
				keySize: 512 / 32,
				iterations: $.lodash.isInteger(iterations) ? iterations : 100
			});
			decrypted = cryptoJS.AES.decrypt(encrypted, key, {
				iv: iv
			});
			decrypted = decrypted.toString(cryptoJS.enc.Utf8);
			return !$.lodash.isEmpty(decrypted) ? decrypted : cyphertext;
		}
		return false;
	} catch (e) {
		return $.parseError(e);
	}
};
$.decrypt = this.decrypt;