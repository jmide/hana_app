/* 
    Parses errors returned by the XS Server into readable format
    
    @param {Object} error - Error object provided by a try catch
    @returns {Object} returns error detailing 
*/
this.parseError = function(error) {
	var response = {};
	if ($.lodash.isObjectLike(error)) {
		if ($.lodash.has(error, "fileName")) {
			response.fileName = error.fileName;
		}
		if ($.lodash.has(error, "lineNumber")) {
			response.lineNumber = error.lineNumber;
		}
		if ($.lodash.has(error, "columnNumber")) {
			response.columnNumber = error.columnNumber;
		}
		response.message = error.toString();
		return response;
	}
	return error.toString();
};
$.parseError = this.parseError;

/*
    Function will return an array with all model instances (BaseModel and Legacy CORE Models). REQUIRES SYSTEM PRIVILEGES
    
    @param {string} componentName Component name in correct alias form
    @returns {Array|boolean} Returns an object with all baseModel instances found for provided component within the results property. If not successful, will return false or error array
*/
this.getComponentModelInstances = function(componentName) {
	$.import('timp.core.server.orm', 'table');
	var ormTable = $.timp.core.server.orm.table.Table;
	$.import('timp.core.server.orm', 'BaseModel');
	const BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

	try {
		var activeObjectModel = $.createBaseRuntimeModel("_SYS_REPO", "ACTIVE_OBJECT");
		var results = activeObjectModel.find({
			select: [{
				field: 'OBJECT_NAME'
		}, {
				field: 'PACKAGE_ID'
		}],
			where: [{
				field: 'PACKAGE_ID',
				operator: '$like',
				value: 'timp.%'
		}, {
				field: 'PACKAGE_ID',
				operator: '$like',
				value: '%.models%'
		}, {
				field: 'PACKAGE_ID',
				operator: '$like',
				value: 'timp.' + $.lodash.toLower(componentName) + '.%'
		}, {
				field: 'OBJECT_SUFFIX',
				operator: '$eq',
				value: 'xsjslib'
		}]
		}).results.map(function(row) {
			return {
				route: row.PACKAGE_ID,
				file: row.OBJECT_NAME
			};
		});
		var modelInstances = [];
		var modelMap = {};
		for (var key in results) {
			if (results.hasOwnProperty(key)) {
				$.import(results[key].route, results[key].file);
				let componentModels = this._getXSNestedInstance(results[key].route, results[key].file);
				if ($.lodash.isObjectLike(componentModels)) {
					Object.keys(componentModels).forEach(function(property) {
						if ((componentModels[property] instanceof BaseModel && $.lodash.isEqual(componentModels[property].getType(), "table")) ||
							componentModels[property] instanceof ormTable) {
							modelInstances.push(componentModels[property]);
						}
					});
				}
			}
		}
		modelInstances = $.lodash.filter(modelInstances, function(table) {
			if (!$.lodash.isNil(table.name) && !$.lodash.isEqual(table.name.indexOf($.lodash.toUpper(componentName)), -1)) {
				modelMap[table.name] = true;
				return true;
			} else if ($.lodash.isFunction(table.getDefinition) && $.lodash.isEqual(table.getDefinition().component, $.lodash.toUpper(componentName))) {
				modelMap[table.getIdentity()] = true;
				return true;
			}
		});
		modelInstances = $.lodash.filter(modelInstances, function(table) {
			if (modelMap[table.name]) {
				delete modelMap[table.name];
				return true;
			} else if ($.lodash.isFunction(table.getIdentity) && modelMap[table.getIdentity()]) {
				delete modelMap[table.getIdentity()];
				return true;
			}
		});
		return {
			errors: [],
			results: modelInstances
		};
	} catch (e) {
		return {
			errors: $.parseError(e),
			results: []
		};
	}
	return false;
};

/* 
    Function to calculate average execution time for a passed function in milliseconds/seconds
    for passed number of iterations
    
    @param {Function} functionToMeasure - Function that you want to test
    @param {integer} iterations - number of times you want to execute the function
    @param {boolean} calculateInSeconds - Pass if you want the return value as seconds
    @returns {Object} Returns an object containing the function retVal, average execution time per cycle and total time
*/
this.functionBenchmark = function(functionToMeasure, parameters, iterations, calculateInSeconds) {
	if (!$.lodash.isInteger(iterations)) {
		iterations = 1;
	}
	if ($.lodash.isFunction(functionToMeasure) && ($.lodash.isArray(parameters))) {
		var benchmarkStartTime = new Date().getTime();
		var totalTime = 0;
		var functionResponse;
		for (var i = 0; i < iterations; i++) {
			let subTaskStartTime = new Date().getTime();
			functionResponse = functionToMeasure.apply(null, parameters);
			let subTaskEndTime = new Date().getTime();
			totalTime += (subTaskEndTime - subTaskStartTime);
		}
		var benchmarkEndTime = new Date().getTime();
		var retVal = {
			result: functionResponse,
			averageTimePerCycle: totalTime / iterations,
			totalExecutionTime: benchmarkEndTime - benchmarkStartTime
		};
		if ($.lodash.isBoolean(calculateInSeconds) && calculateInSeconds) {
			retVal.averageTimePerCycle = retVal.averageTimePerCycle / 1000;
			retVal.totalExecutionTime = retVal.totalExecutionTime / 1000;
		}
		return retVal;
	}
	return {
		totalTime: 0,
		averageTimePerCycle: 0
	};
};
$.functionBenchark = this.functionBenchmark;

this.parseXml = function(xml) {
	var parser = new $.util.SAXParser();
	var rootElement;
	var elementStack = [];

	parser.startElementHandler = function(name, attrs) {
		var data = attrs; // use attrs object with all properties as template
		data.name = name; // add the name to the object

		if (!rootElement) { // the first element we see is the root element we want to send as response
			rootElement = data;
		} else {
			var currentElement = elementStack[elementStack.length - 1];
			if (!currentElement.children) { // first time we see a child we have to create the children array
				currentElement.children = [data];
			} else {
				currentElement.children.push(data);
			}
		}
		elementStack.push(data);
	};

	parser.endElementHandler = function() {
		elementStack.pop();
	};
	parser.characterDataHandler = function(s) {
		var currentElement = elementStack[elementStack.length - 1];
		if (!currentElement.characterData) { // the first time we see char data we store it as string
			currentElement.characterData = s;
		} else if (!Array.isArray(currentElement.characterData)) { // if we already have a string we convert it to an array and append the new data
			currentElement.characterData = [currentElement.characterData, s];
		} else { // just append new data to the existing array
			currentElement.characterData.push(s);
		}
	};
	parser.parse(xml);
	return {
	    rootElement: rootElement,
	    elementStack: elementStack
	};
};

this.generateError = function(functionName, extraData, stackedError) {
	let stack = [];
	extraData = extraData || { object: '00', category: '5000' };

	let error = $.lodash.assign({
		functionName: functionName
	}, extraData);

	if ($.lodash.isArray(stackedError)) {
		stack = stackedError;
	} else if (!$.lodash.isNil(stackedError) && $.lodash.has(stackedError, 'stack') && !$.lodash.isNil(stackedError.stack)) {
		stack = stackedError.stack;
	} else if (!$.lodash.isNil(stackedError)){
		stack.unshift(stackedError);
	}

	stack.unshift($.lodash.cloneDeep(error));

	error.stack = stack;

	return error;
};
$.generateError = this.generateError;