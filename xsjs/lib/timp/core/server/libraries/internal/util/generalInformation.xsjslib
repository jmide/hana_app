this.getUserModelInformation = function(modelToJoin, isBaseModel, schema, isCreation) {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;

	var creation = (isBaseModel) ? 'creationUser' : 'CREATION.ID_USER';
	var modification = (isBaseModel) ? 'modificationUser' : 'MODIFICATION.ID_USER';
	var result = userModel.find({
		aliases: [{
			collection: userModel.getIdentity(),
			name: 'User',
			isPrimary: true 
		}, {
			collection: modelToJoin,
			ignoreCache: true,
			name: 'ModelToJoin',
			schema: schema
		}],
		select: [{
			field: 'id',
			alias: 'User' 
		}, {
			field: 'name',
			alias: 'User'
		}, {
			field: 'lastName',
			alias: 'User'
		}],
		join: [{
			alias: 'ModelToJoin',
			type: 'inner',
			on: [{
				alias: 'ModelToJoin',
				operator: '$eq',
				field: (isCreation) ? creation : modification,
				value: {
					alias: 'User',
					field: 'id'
				}
			}]
		}]
	}).results.map(function(value) {
		return {
			key: value.id,
			name: value.name + ' ' + value.lastName
		};
	});
	return result;
};

this.getTaxInformation = function() {
	$.import('timp.core.server.models.tables', 'tax');
	const taxModel = $.timp.core.server.models.tables.tax.taxModel;

	return taxModel.find({}).results.map(function(value) {
		return {
			key: value.code,
			name: value.description
		};
	});
};

this.getAllCompanies = function() {
	const companyView = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.emp_fil/EMPRESA', true);
	return companyView.find({
		distinct: true,
		select: [{
			field: 'COD_EMP',
			as: 'company'
		}],
		orderBy: [{
			field: 'COD_EMP',
			type: 'asc'
		}]
	}).results.map(function(value) {
		return value.company;
	});
};

this.getAllBranches = function() {
	const branchView = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.emp_fil/FILIAL', true);
	return branchView.find({
		select: [{
			field: 'COD_EMP',
			as: 'company'
		}, {
			field: 'UF_FILIAL',
			as: 'state'
		}, {
			field: 'COD_FILIAL',
			as: 'branch'
		}],
		orderBy: [{
			field: 'COD_EMP',
			type: 'asc'
		}, {
			field: 'UF_FILIAL',
			type: 'asc'
		}, {
			field: 'COD_FILIAL',
			type: 'asc'
		}]
	});
};

let _userId_ = {};
this.getUserId = function() {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;

    if(_userId_[$.session.getUsername()] != undefined){
        return _userId_[$.session.getUsername()];
    }
	let userId = userModel.find({
		select: [{
			field: 'id'
	    }],
		where: [{
			field: 'hanaUser',
			operator: '$eq',
			value: $.session.getUsername()
	    }]
	});
	if (!$.lodash.isEmpty(userId.errors) || $.lodash.isEmpty(userId.results)) {
	    userId = userModel.find({
    		select: [{
    			field: 'id'
    	    }],
    		where: [{
    			field: 'hanaUser',
    			operator: '$eq',
    			value: $.schema.slice(1, -1)
    	    }]
    	});
	    if (!$.lodash.isEmpty(userId.errors) || $.lodash.isEmpty(userId.results)) {
	        return -1;
	    
	    }
	}
	userId = userId.results[0].id;
	_userId_[$.session.getUsername()] = userId;
	return userId;
};
$.getUserId = this.getUserId;
$.getUserID = this.getUserId;

this.getUserInformation = function(hanaUser) {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;

	if ($.lodash.isString(hanaUser)) {
		return userModel.find({
			where: [{
				field: 'hanaUser',
				operator: '$eq',
				value: hanaUser
        }]
		}).results[0];
	}
	return {};
};

this.getSystemUserMetadata = function() {
	$.import('timp.core.server.models.tables', 'user');
	const userModel = $.timp.core.server.models.tables.user.userModel;

	return userModel.find({
		where: [{
			field: 'hanaUser',
			operator: '$eq',
			value: $.schema.slice(1, -1)
        }]
	}).results[0];
};
$.getSystemUserMetadata = this.getSystemUserMetadata;

this.getSessionLanguage = function() {
	var language = $.request.cookies.get('Content-Language');
	if ($.lodash.isEqual(language, 'enus')) {
		return language;
	} else {
		return 'ptrbr';
	}
};
$.getSessionLanguage = this.getSessionLanguage;

/*
    Function that returns component information based on component id or name
    
    @param {(string|number)} identifier - Component ID or Name
    @returns {Object} Returns component information if it matches any id or name provided
*/
this.getComponentInformation = function(identifier) {
	$.import('timp.core.server.models.tables', 'component');
	const componentModel = $.timp.core.server.models.tables.component.componentModel;

    if(componentModel && componentModel.exists().results.exists){
    	if ($.lodash.isInteger(identifier) || $.lodash.isString(identifier)) {
    		return componentModel.find({
    			where: [{
    				field: ($.lodash.isInteger(identifier)) ? 'id' : 'name',
    				operator: '$eq',
    				value: identifier
                }]
    		});
    	} else if ($.lodash.isIntArray(identifier) || $.lodash.isStringArray(identifier)) {
    		return componentModel.find({
    			where: [{
    				field: ($.lodash.isInteger(identifier)) ? 'id' : 'name',
    				operator: '$in',
    				value: identifier
                }]
    		});
    	} else {
    		//throw bad request
    	}
    }
};

this.userHasGrcPrivileges = function(hanaUser){
    if(!$.lodash.isNil(hanaUser) && $.lodash.isString(hanaUser)){
        let query = 'SELECT USER_NAME FROM "PUBLIC"."EFFECTIVE_ROLES" WHERE USER_NAME = \'' + hanaUser + '\' AND ROLE_NAME = \'timp.core.server.role::timpgrcac\'';
        let hasGrcRole = $.execute(query).results[0];
        if(!$.lodash.isNil(hasGrcRole)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;    
    }
};
$.userHasGrcPrivileges = this.userHasGrcPrivileges;