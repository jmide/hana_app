$.import('timp.core.server.libraries.external', 'lodash');
let _ = $.timp.core.server.libraries.external.lodash;

function URLRouter(options) {
    this.performance = {
        instance: options.endpointTiming || new Date().getTime(),
    };
    if(options.coreTiming ){
        this.performance.coreTiming = options.coreTiming
    }
    this.init = function (opts) {
        this.routes = opts.routes;
        this.default = opts.default;
        return this;
    };
    this.selfDocument = function (lb) {
        lb = lb || '<br>';
        var content = '<pre>======================= SELF DOCUMENTATION =======================';
        for (var i = 0; i < this.routes.length; i++) {
            var route = this.routes[i];
            content += lb + route.url.replace(/^\^/, '/').replace(/\$$/, '');
            for (var y = 0; route.hasOwnProperty('test') && y < route.test.length; y++) {
                // if ('data' in route.test[y]) {
                if (Array.isArray(route.test[y]) && route.test[y].length === 2) {
                    content += lb + '\treceives: ' + lb + '\t\t' + str(route.test[y][0], undefined, 4);
                    content += lb + '\t returns: ' + lb + '\t\t' + str(route.test[y][1], undefined, 4);
                } else {
                    content += lb + '\treceives: ' + lb + '\t\t' + str(route.test[y], undefined, 4);
                }
                // }
            }
        }
        return content + '</pre>';
    };
    this.init(options);
    this.performance.instance = (new Date().getTime() - this.performance.instance) / 1000;
    return this;
}

let checkUserPrivileges = function (privileges, component) {
    $.import('timp.core.server.models.tables', 'component');
    const componentModel = $.timp.core.server.models.tables.component.componentModel;
    const privilegeModel = $.timp.core.server.models.tables.component.privilegeModel;
    $.import('timp.core.server.models.views', 'userPrivilege');
    const userPrivilegeView = $.timp.core.server.models.views.userPrivilege.userPrivilegeView;
    var userPrivileges = [];
    if ($.userHasGrcPrivileges($.session.getUsername()) && componentModel.exists().results.exists && privilegeModel.exists().results.exists) {
        userPrivileges = $.execute(
            'SELECT DISTINCT PRIVILEGE.NAME AS "privilegeName" ' +
            'FROM ' + $.schema + '."CORE::PRIVILEGE" AS PRIVILEGE ' +
            'INNER JOIN "PUBLIC"."EFFECTIVE_ROLES" AS ROLES ' +
            'ON PRIVILEGE.ROLE = ROLES.ROLE_NAME ' +
            'INNER JOIN ' + $.schema + '."CORE::COMPONENT" AS COMPONENT ' +
            'ON PRIVILEGE.COMPONENT_ID = COMPONENT.ID ' +
            'WHERE USER_NAME = \'' + $.session.getUsername() + '\' AND PRIVILEGE.NAME IN (\'' + privileges.join('\',\'') + '\')' +
            ' AND COMPONENT.NAME = \'' + component + '\'');
    } else if (!_.isEmpty(privileges) && userPrivilegeView.exists().results.exists) {
        userPrivileges = userPrivilegeView.find({
            select: [{
                field: 'privilegeName',
                as: 'name'
            }],
            where: [{
                field: 'privilegeName',
                operator: '$in',
                value: privileges
            }, {
                field: 'hanaUser',
                operator: '$eq',
                value: $.session.getUsername()
            }, {
                field: 'componentName',
                operator: '$eq',
                value: component
            }]
        });
    }
    return {
        errors: userPrivileges.errors,
        results: {
            valid: userPrivileges.results.length === privileges.length
        }
    };
};

URLRouter.prototype.errorHandler = function (parsedResponse) {
    if ($.response.status === 200) {
        for (var i = 0; i < $.messageCodes.length; i++) {
            var message = $.messageCodes[i];
            if (message.type === 'E') {
                $.response.status = 500;
            }
        }
    }
    //Include only error messageCodes
    $.messageCodes = $.messageCodes.filter(function (row) {
        return row.type === 'E' ? true : $.successMessageCodesActive;
    });
    try {
        while (typeof parsedResponse === 'string') {
            parsedResponse = JSON.parse(parsedResponse);
        }
    } catch (e) {
        $.logError('', e);
    }
    parsedResponse = {
        results: parsedResponse,
        messageCodes: $.messageCodes,
        status: $.response.status
    };

    $.res(parsedResponse);
};

URLRouter.prototype.parseURL = function (ctx, component) {
    const _this_ = this;
    _this_.performance.parseURLtime = new Date().getTime();
    //Default response type
    $.response.contentType = 'application/json';
    var url = $.request.queryPath;
    // if (url == 'tests/') {
    //     var tests = [];
    //     for (var i = 0; i < this.routes.length; i++) {
    //         var route = this.routes[i];
    //         if (route.hasOwnProperty('test')) {
    //             for (var y = 0; y < route.test.length; y++) {
    //                 tests.push({
    //                     url: route.test[y].url || route.url.replace(/^\^/,'/').replace(/\$$/,''),
    //                     data: route.test[y].data,
    //                     validate: route.test[y].validate
    //                 })
    //             }
    //         }
    //     }
    //     $.response.setBody(JSON.stringify(tests));
    //     return true;
    // }
    var response;
    for (var i = 0; i < this.routes.length; i++) {
        var route = this.routes[i];
        var result = url.match(route.url);

        if (result) {
            if(!(route.view) && route.pathView) {
		        route.ctx = new (route.ctx);
                route.view = route.ctx[route.pathView];
            }
            $.requestInitialTime = new Date().getTime();
            if (!_.isNil($.request.parameters.get('timeForLog'))) {
                $.timeForLog = $.request.parameters.get('timeForLog');
            }
            if (typeof route.ctx !== 'undefined') {
                ctx = route.ctx;
            }
            var parameters = result.slice(1);
            if (!route.hasOwnProperty('view') || typeof route.view !== 'function') {
                throw 'Misconfigured route: ' + i + '\nThe assigned view is not a function.';
            }
            $.response.status = 200;
            ctx.__URLROUTER = true; //Let the controller know it's being called from the UI
            var s = getParam('object');
            if (typeof s !== 'undefined') {
                parameters = parameters.concat([s]);
            }
            var error = false;
            let ignoreErrorHandler = false;
            parameters.forEach(function (item) {
                if (typeof item !== 'object') {
                    return;
                }
                Object.keys(item).forEach(function (key) {
                    if (typeof item[key] === 'string' && item[key].toLowerCase().match(/eval\s*\((.)*\)/g)) {
                        error = true;
                    }
                    if ( !(item.comp === 'TIS' && key === 'xml') ) {
                        item[key] = typeof item[key] === 'string' ? item[key].escapeHtml() : item[key];
                    } 
                    if (typeof item[key] === 'object') {
                        var tmp = JSON.stringify(item[key]);
                        if (tmp.toLowerCase().match(/eval\s*\((.)*\)/g)) {
                            error = true;
                        }
                        if ( !(item.comp === 'TIS' && key === 'xml') ) {
                            item[key] = JSON.parse(tmp.escapeHtml());
                        }
                    }
                });
            });
            if (error) {
                $.response.status = 404;
                response = {
                    messageCodes: [{
                        type: 'E',
                        code: 'CORE000013',
                        errorInfo: $.parseError('Expression "eval" found')
                    }]
                };
                // $.res(response)
                // return;
                ignoreErrorHandler = true;
                _this_.performance.isValid = false;
            } else {
                let userPrivilegeResponse;
                let valid;
                let privileges = (Array.isArray(route.privileges) && !_.isNil(route.privileges)) ? route.privileges : [];
                if (!_.isEmpty(privileges)) {
                    if (!_.isString(component)) {
                        component = '';
                    }
                    component = _.toUpper(component);
                    userPrivilegeResponse = checkUserPrivileges(route.privileges, component);
                    valid = _.isBoolean(userPrivilegeResponse.results.valid) ? userPrivilegeResponse.results.valid : false;
                }
                if (_.isEmpty(privileges) || valid) {
                    _this_.performance.endpointTime = new Date().getTime();
                    response = route.view.apply(ctx, parameters);
                     _this_.performance.endpointTime = (new Date().getTime() - _this_.performance.endpointTime) / 1000;
                } else {
                    _this_.performance.isValid = false;
                    $.response.status = $.net.http.FORBIDDEN;
                    response = {
                        messageCodes: $.messageCodes,
                        results: {}
                    };
                    ignoreErrorHandler = true;
                }
            }
            
            _this_.performance.parseURLtime = (new Date().getTime() - _this_.performance.parseURLtime) / 1000;
            _this_.performance.parseURLtime = _this_.performance.parseURLtime - _this_.performance.endpointTime;
            
            if (response && $.request.cookies.get('performance') == 'true') {
                response.__performance__ = {
                    app: $.getPerformance(),
                    router: _this_.performance
                };
            }
            
            if (typeof this.errorHandler === 'function' && $.messageCodes.length && !ignoreErrorHandler) {
                this.errorHandler(response);
            } else {
                $.res(response);
            }
            return response;
        }
    }
    response = this.default.apply(ctx || this) || '';
    response = '<pre>Invalid URL</pre>';
    $.response.status = 404;
    //response += this.selfDocument(); //Removed valid endpoints list for certification security vulnerability purposes
    $.response.contentType = 'text/html';
    $.res(response);
    return response;
};
// this.URLRouter = Declare({ = {;
//  options: {
//      'routes': [{url: isRegex, view: isFunction}],
//      'default': isFunction
//  }
// }, URLRouter);
this.Router = URLRouter;


let str = function(value) {
	if (typeof value == 'string') {
		return value;
	}
	var seen = [];

	return JSON.stringify(value, function(key, val) {
		if (val !== null && typeof val == 'object') {
			if (seen.indexOf(val) !== -1) {
				try {
					JSON.stringify(val);
					return val;
				} catch (e) {
					return 'cyclic value!';
				}
			}
			seen.push(val);
		}
		return val;
	}) || typeof value;
};


let isValid = function (value) {
	return (typeof value !== 'undefined' && value !== null);
};

let getParam = function (name) {
	var resp = $.request.parameters.get(String(name));
	if (!isValid(resp)) {
		resp = $.request.parameters.get('object');
		if (isValid(resp)) {
			try {
				while (_.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp.hasOwnProperty(name) ? resp[name] : undefined;
			} catch (e) {
				return resp;
			}
		}
	} else {
		if (isValid(resp)) {
			try {
				while (_.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp;
			} catch (e) {
				return resp;
			}
		}
	}
	return resp;
};
