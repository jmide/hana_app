var util = this;
const _this = this;

$.messageCodes = [];
$.successMessageCodesActive = false;

$.logSuccess = function (code, message) {
	$.messageCodes.push({
		code,
		message: util.parseError(message || 'Success'),
		type: 'S'
	});
};

$.logError = function(code, error, type) {
	$.messageCodes.push({
		code: code,
		type: type ? type : 'E',
		error: util.parseError(error ? error : code)
	});
};

// X-Frame-Options SAMEORIGIN
$.response.cookies.set('X-Frame-Options', 'DENY');
$.response.headers.set('X-Frame-Options', 'DENY');

/*
Shorten way of using $.response.setBody
Does not give an error when receives an object, but converts it using JSON.stringify
Usage:
    $.res(<whatever>)
*/
$.res = function(value, indent) {
	if (value && typeof value === 'object' && value.constructor.name === 'ArrayBuffer') {
		$.response.setBody(value);
	} else if (indent && value && typeof value !== 'string') {
		value = JSON.stringify(value, undefined, indent);
		$.response.setBody(value);
	} else {
		value = util.str(value);
		$.response.setBody(value);
	}
	return value;
};

//Set default language
if ($.request.parameters.get('lang')) {
	$.response.cookies.set('Content-Language', $.request.parameters.get('lang'));
} else {
	if (!$.request.cookies.get('Content-Language')) {
		$.response.cookies.set('Content-Language', 'ptrbr');
	}
}


this.str = function(value) {
	if (typeof value == 'string') {
		return value;
	}
	var seen = [];

	return JSON.stringify(value, function(key, val) {
		if (val !== null && typeof val == 'object') {
			if (seen.indexOf(val) !== -1) {
				try {
					JSON.stringify(val);
					return val;
				} catch (e) {
					return 'cyclic value!';
				}
			}
			seen.push(val);
		}
		return val;
	}) || typeof value;
};


function each(arr, fn, ctx) {
	var res = [];
	var p = 0;
	var response;
	if (typeof ctx === 'undefined') {
		ctx = this;
	}
	if (typeof arr === 'undefined') {
		return res;
	} else if (typeof arr !== 'object') {
		response = fn.call(ctx, arr);
		if (typeof response !== 'undefined') {
			res.push(response);
		}
	} else if (arr instanceof[].constructor) {
		var len = arr.length;
		for (p = 0; p < len; p++) {
			if (typeof p !== 'undefined' && typeof arr[p] !== 'undefined') {
				response = fn.call(ctx, arr[p], p, arr);
				if (typeof response !== 'undefined') {
					res.push(response);
				}
			}
		}
	} else {
		for (p in arr) {
			if (arr.hasOwnProperty(p)) {
				if (typeof p !== 'undefined' && typeof arr[p] !== 'undefined') {
					response = fn.call(ctx, arr[p], p, arr);
					if (typeof response !== 'undefined') {
						res.push(response);
					}
				}
			}
		}
	}
	return res;
}
this.each = each;
this.map = each;


function mapObject(arr, fn, ctx) {
	var res = {};
	var fnRes = [];
	var p = 0;
	var isValidAnswer = function(fnRe) {
		return ((typeof fnRe === 'object') && (typeof fnRe.length === 'number') && (fnRe.length === 2));
	};
	if (typeof ctx === 'undefined') {
		ctx = this;
	}
	if (typeof arr === 'undefined') {
		return res;
	} else if (typeof arr !== 'object') {
		fnRes = fn.call(ctx, arr);
		if (isValidAnswer(fnRes)) {
			res[fnRes[0]] = fnRes[1];
		}
	} else if (typeof arr.length === 'number') {
		var len = arr.length;
		for (p = 0; p < len; p++) {
			if (p !== 'undefined' && arr[p] !== 'undefined') {
				fnRes = fn.call(ctx, arr[p], p, arr);
			}
			if (isValidAnswer(fnRes)) {
				res[fnRes[0]] = fnRes[1];
			}
		}
	} else {
		for (p in arr) {
			if (arr.hasOwnProperty(p)) {
				if (p !== 'undefined' && arr[p] !== 'undefined') {
					fnRes = fn.call(ctx, arr[p], p, arr);
				}
				if (isValidAnswer(fnRes)) {
					res[fnRes[0]] = fnRes[1];
				}
			}
		}
	}
	return res;
}
this.mapObject = mapObject;


function flipDictionary(oldDic) {
	var newObj = {};
	each(oldDic, function(v, k) {
		if (typeof v === 'string' && typeof k === 'string') {
			newObj[v] = k;
		}
	});
	return newObj;
}
this.flipDictionary = flipDictionary;


function FunctionWrapper(validate, callback) {
	var _callback = callback;
	callback = function() {
		validate.apply(this, arguments);
		_callback.apply(this, arguments);
	};
	return callback;
}
this.FunctionWrapper = FunctionWrapper;


/*
Set of functions to parse datatypes:
*/
function isValid(value) {
	return (typeof value !== 'undefined' && value !== null);
}
this.isValid = isValid;


function isNone(value) {
	return !isValid(value);
}
this.isNone = isNone;


function isNumber(value) {
	return (typeof value === 'number' && !isNaN(value));
}
this.isNumber = isNumber;


function isString(value) {
	return (typeof value === 'string');
}
this.isString = isString;


function isBoolean(value) {
	return (typeof value === 'boolean');
}
this.isBoolean = isBoolean;


function isObject(value) {
	return (isValid(value) && value instanceof Object);
}
this.isObject = isObject;


function isArray(value) {
	return (isValid(value) && value instanceof Array);
}
this.isArray = isArray;


function isDate(value) {
	return (isValid(value) && value instanceof Date);
}
this.isDate = isDate;


function isRegex(value) {
	return (isValid(value) && value instanceof RegExp || isString(value));
}
this.isRegex = isRegex;


function isFunction(value) {
	return (typeof value === 'function');
}
this.isFunction = isFunction;


function isDBType(value) {
	return (isValid(value) && flipDictionary($.db.types).hasOwnProperty(value));
}
this.isDBType = isDBType;


function ParseAnything(format, object, name) {
	if (!isValid(object)) {
		throw name + ' is undefined';
	}

	var fn;
	var property;
	if (isFunction(format)) {
		fn = format;
		var isValid2 = fn(object);
		if (!isValid2) {
			throw name + ' ' + fn.name + '(' + JSON.stringify(object) + ') returned false';
		}
	} else if (isArray(format) && format.length === 1) {
		fn = format[0];
		if (isArray(object)) {
			for (var i = 0; i < object.length; i++) {
				property = name + '[' + i + ']';
				ParseAnything(fn, object[i], property);
			}
		} else {
			throw property + ' isArray(' + JSON.stringify(object) + ') returned false';
		}
	} else if (isObject(format)) {
		for (var x in format) {
			property = name + '.' + x;
			if (!object.hasOwnProperty(x)) {
				throw name + ' has not attribute ' + JSON.stringify(x);
			}

			if (format.hasOwnProperty(x)) {
				ParseAnything(format[x], object[x], property);
			}
		}
	}
	return true;
}
this.ParseAnything = ParseAnything;


function SelfDocument(format, name) {
	return name + ' = ' + SelfDocument._iter(format);
}
this.SelfDocument = SelfDocument;
SelfDocument.translations = {
	'isValid': '(not undefined)',
	'isNumber': '(number)',
	'isString': '(string)',
	'isObject': '(object)',
	'isArray': '(array)',
	'isDate': '(date)',
	'isRegex': '(regex)',
	'isFunction': '(function)'
};
SelfDocument._iter = function(format) {
	var translation;
	var sub;
	if (isFunction(format)) {
		if (SelfDocument.translations.hasOwnProperty(format.name)) {
			translation = SelfDocument.translations[format.name];
		} else {
			translation = '(' + format.name + ')';
		}
		return translation;
	} else if (isArray(format) && format.length === 1) {
		sub = SelfDocument._iter(format[0]);
		translation = '[' + sub + ', ..., ' + sub + ']';
		return translation;
	} else if (isObject(format)) {
		sub = [];
		for (var x in format) {
			if (format.hasOwnProperty(x)) {
				var _sub = '\t' + x + ' : ' + SelfDocument._iter(format[x]).replace(/\n/g, '\n\t');
				sub.push(_sub);
			}
		}
		translation = '{\n' + sub.join(',\n') + '\n}';
		return translation;
	}
	return '';
};


/*
    JSON.stringify that won't break for circular references
*/
function defuse(e, depth) {
	if (typeof depth == 'undefined') {
		depth = 100;
	}
	if (depth == 0) {
		return 'util.defuse: too deep';
	}
	depth -= 1;
	var bones = {};
	if (typeof e === 'undefined') {
		return ({}).u;
	}
	try {
		if (typeof e === 'object' && typeof e.toArray === 'function') {
			e = e.toArray();
		}
	} catch (e1) {
		//return undefined;
	}
	if (e instanceof[].constructor) {
		bones = [];
	}
	for (var x in e) {
		if (e.hasOwnProperty(x)) {
			if (typeof e[x] === 'function') {
				bones[x] = 'function';
			} else if (typeof e[x] === 'object') {
				try {
					bones[x] = defuse(e[x], depth);
				} catch (_e) {
					bones[x] = '__impossible__';
				}
			} else {
				bones[x] = e[x];
			}
		} else {
			try {
				if (bones[x] instanceof Date) {
					bones[x] = bones[x].toString();
				} else if (typeof e[x] === 'object') {
					bones[x] = defuse(e[x], depth);
				} else {
					bones[x] = e[x];
				}
			} catch (error1) {
				bones[x] = '__hidden__';
			}
		}
	}
	return bones;
}
this.defuse = defuse;


function parseError(e) {
	var seen = [];
	if (typeof e == 'object' && e !== null && typeof e.hasOwnProperty == 'function' && e.hasOwnProperty('fileName') && e.hasOwnProperty(
		'lineNumber') && e.hasOwnProperty('columnNumber')) {
		return JSON.stringify({
			'fileName': e.fileName,
			'lineNumber': e.lineNumber,
			'columnNumber': e.columnNumber,
			'Error Msg': e.toString()
		});
	}
	if (typeof e === 'object') {
		return JSON.stringify(e, function(key, val) {
			if (val !== null && typeof val == 'object') {
				if (seen.indexOf(val) !== -1) {
					try {
						JSON.stringify(val);
						return val;
					} catch (e1) {
						return 'cyclic value!';
					}
				}
				seen.push(val);
			}
			return val;
		}, 4);
	} else {
		return e;
	}
}
this.parseError = parseError;


function Declare(args, fn) {
	var parse = [];
	var _args = [];
	for (var x in args) {
		if (args.hasOwnProperty(x)) {
			_args.push(x);
			parse.push([x, args[x]]);
		}
	}
	var _doc = 'Usage: ' + fn.name + '(' + _args.join(', ') + ')\nWhere: \n';
	for (var i = 0; i < parse.length; i++) {
		var name = parse[i][0];
		var rule = parse[i][1];
		var partial = SelfDocument(rule, name);
		_doc += '\t' + partial.replace(/\n/g, '\n\t') + '\n';
	}

	fn._validate = function(arg) {
		try {
			for (var j = 0; j < parse.length; j++) {
				var name2 = parse[j][0];
				var rule2 = parse[j][1];
				ParseAnything(rule2, arg[j], name2);
			}
		} catch (e) {
			throw 'Wrong parameteres for function ' + fn.name + '\n\t' + parseError(e) + '\n' + _doc + '\nReceived: ' + parseError(args);
		}
	};

	fn._doc = _doc;

	return fn;
}
this.Declare = Declare;


function JSTypeConverter() {
	function convertNumber(n) {
		if (n === null || typeof n === 'undefined' || n === '') {
			return null;
		}
		var value = parseFloat(n);
		if (isNaN(value)) {
			value = undefined;
		}
		return value;
	}

	function convertDate(n) {
		if (typeof n !== 'string' || !n) {
			return false;
		} else {
			//return new Date(n).toISOString().split('T')[0];
			//this.debug('WHY? Date', n, typeof n);
			return new Date().toString();
		}
	}

	function forceStringify(n) {
		return typeof n === 'string' && n || JSON.stringify(n);
	}
	this.types = {
		1: function(n) {
			var v = convertNumber(n);
			if (v < 256) {
				return v;
			}
			return false;
		},
		2: convertNumber,
		3: convertNumber,
		4: convertNumber,
		5: convertNumber,
		6: convertNumber,
		7: convertNumber,
		8: String,
		9: String,
		10: String,
		11: String,
		12: String,
		13: String,
		14: convertDate,
		15: convertDate,
		16: convertDate,
		25: forceStringify,
		26: String,
		27: String,
		47: convertNumber,
		51: String,
		52: String,
		55: String,
		62: convertDate
	};
	this.isValid = function(value, type) {
		var val = this.parseValue(value, type);
		if (val === 0) {
			return true;
		} else if (val === '') {
			return true;
		} else if (val === null) {
			return true;
		}
		return !!val;
	};
	this.parseValue = function(value, type) {
		if (typeof this.types[type] !== 'function') {
			return value;
		}
		var val = this.types[type](value);
		return val;
	};
}
this.JSTypeConverter = JSTypeConverter;


function debug() {
	var error = {};
	return error;
}
this.debug = debug;

$.import('timp.core.server.libraries.internal.util','router');
this.URLRouter = $.timp.core.server.libraries.internal.util.router.Router;


//Uses the URLRouter to add some functionality to the alltax.alltax.base.controller.BasicController
/*
Parameters:
    controller: <alltax.alltax.base.controller.BasicController>
Usage example:
    $.import('alltax.alltax','util');
    var util = $.alltax.alltax.util;
    try {
        $.import('alltax.alltax.base','controller');
        var controller = $.alltax.alltax.base.controller;
        $.import('bre.server.model','rule');
        var model = $.bre.server.model.rule.model;
        $.import('bre.server.model','datastructure');
        var datastructure = $.bre.server.model.datastructure.model;
        
        var controller = new controller.BasicController({
            model: model
        });
        
        //controller.parseRequest();
        util.RestWrapper(controller);
    } catch (e) {
        $.response.setBody(util.parseError(e));    
    }
*/
function RestWrapper(controller) {
	var router = new URLRouter({
		routes: [
			{
				url: '^create/$',
				view: controller.CREATE
			},
			{
				url: '^read/(\\d+)/$',
				view: controller.READ
			},
			{
				url: '^read/$',
				view: controller.READ
			},
			{
				url: '^update/(\\d+)/$',
				view: controller.UPDATE
			},
			{
				url: '^delete/(\\d+)/$',
				view: controller.DELETE
			},
			{
				url: '^/?$',
				view: controller.parseRequest
			}
        ],
		'default': function() {
			$.response.status = 404; //404
			$.response.contentType = 'text/plain';
			var resp = 'Unknown URL\n';
			var urls = ['create/', 'read/:id/', 'read/', 'update/:id/', 'delete/:id/'];
			for (var i = 0; i < urls.length; i++) {
				resp += urls[i] + '\n';
			}
			$.response.setBody(resp);
		}
	});

	var response = router.parseURL(controller);

	if (typeof response !== 'undefined') {
		if (typeof response === 'object') {
			var strRes = JSON.stringify(response);
			$.response.status = 200;
			$.response.contentType = 'application/json';
			if ($.request.parameters.get('callback')) {
				$.response.setBody($.request.parameters.get('callback') + '(' + strRes + ')');
			} else {
				$.response.setBody(strRes);
			}
		}
	}
	return undefined;
}
this.RestWrapper = RestWrapper;


//Just call this function and the setBody will handle the callback itself
/*
Usage example:
    $.import('alltax.alltax','util');
    var util = $.alltax.alltax.util;
    util.CallbackDecorator();

    $.response.status = 200;
    $.response.contentType = 'text/javascript';
    $.response.setBody(JSON.stringify({foo:12}));
Case mything.xsjs:
    {'foo':12}
Case mything.xsjs?callback=foo:
    foo({'foo':12})
*/
function CallbackDecorator() { //DEPRECATED
	//     $.response.callback = {
	//         setBody: $.response.setBody
	//     }
	//  $.response.setBody = function (body) {
	//      var callback = $.request.parameters.get('callback');
	//      try {
	//             if (!!callback) {
	//                 // $.response.contentType = 'application/javascript';
	//                 $.response.callback.setBody(callback+'('+body+')');
	//             } else {
	//                 // $.response.contentType = 'text/json';
	//                 $.response.callback.setBody(body);
	//             }
	//      } catch (e) {
	//          $.response.status = 500;
	//          $.response.callback.setBody('ERROR: $.response.setBody requires a string');
	//      }
	//      return body;
	//  }
}
this.CallbackDecorator = CallbackDecorator;


//This function has the objective of sorting objects by desired property and order type. It returns an array.
//Obj is object you want to sort.
//propertyToSort is the property you want to sort, if not mentioned it will only convert the Object to an Array.
//orderType is the order you want to sort. By default is ascending order, but if its 'desc' it will be descending order.
//var obj = {'28':{'fieldId':28,'position':0},'30':{'fieldId':30,'position':1},'31':{'fieldId':31,'position':3},'174':{'fieldId':174,'position':2}};
//var result = sortObjectsToArray(obj, 'position', 'desc');
//It will become: [{'fieldId':31,'position':3},{'fieldId':174,'position':2},{'fieldId':30,'position':1},{'fieldId':28,'position':0}]
function sortObjectsToArray(obj, propertyToSort, orderType) {
	var arr = [];
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop)) {
			arr.push(obj[prop]);
		}
	}
	arr.sort(function(a, b) {
		if (typeof orderType !== 'undefined' && orderType.toUpperCase() === 'DESC') {
			return b[propertyToSort] - a[propertyToSort];
		} else {
			return a[propertyToSort] - b[propertyToSort];
		}
	});
	return arr;
}
this.sortObjectsToArray = sortObjectsToArray;


/* 
Workaround for the problems TimpData caused by wrapping every POST in a 'object' argument
Usage:
    util.getParam('my_parameter');
*/
this.getParam = function getParam(name) {
	var resp = $.request.parameters.get(String(name));
	if (!util.isValid(resp)) {
		resp = $.request.parameters.get('object');
		if (util.isValid(resp)) {
			try {
				while (util.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp.hasOwnProperty(name) ? resp[name] : undefined;
			} catch (e) {
				return resp;
			}
		}
	} else {
		if (util.isValid(resp)) {
			try {
				while (util.isString(resp)) {
					resp = JSON.parse(resp);
				}
				return resp;
			} catch (e) {
				return resp;
			}
		}
	}
	return resp;
};


//Overwrite the $.import to solve the infinite loop condition
// if (!$.hasOwnProperty('_importList')) {
//     $._import = $.import;
//     $._importList = {};
//     $._importDebug = ['importing files'];
//     $.import = function (pack, file) {
//         var key = pack + '.' + file;
//         $._importDebug.push(key);
//         try {
//             if (!$._importList.hasOwnProperty(key)) {
//                 $._importList[key] = $._import(pack, file);
//                 if (!$._importList.hasOwnProperty(key)) {
//                     $._importList[key] = {};
//                 }
//             }
//         } catch (e) {
//             $._importDebug.push('failed', util.parseError(e));
//             throw $._importDebug.join('\n');
//         }
//         return $._importList[key];
//     }
// }

this.encrypt = function(plainText) {
	$.import('timp.core.server', 'cryptoAES');
	var CryptoJS = $.timp.core.server.cryptoAES.CryptoJS;
	var key = CryptoJS.enc.Base64.parse("253D3FB468A0E24677C28A624BE0F939");
	var iv = CryptoJS.enc.Base64.parse("                ");
	var encrypted = CryptoJS.AES.encrypt(plainText, key, {
		iv: iv
	});
	return encrypted;
};
this.decrypt = function(encrypted) {
	$.import('timp.core.server', 'cryptoAES');
	var CryptoJS = $.timp.core.server.cryptoAES.CryptoJS;
	var key = CryptoJS.enc.Base64.parse("253D3FB468A0E24677C28A624BE0F939");
	var iv = CryptoJS.enc.Base64.parse("                ");
	var decrypted = CryptoJS.AES.decrypt(encrypted, key, {
		iv: iv
	});
	return decrypted.toString(CryptoJS.enc.Utf8);
};



//Forcing the string convertion of an Object to be it's JSON representation
Object.prototype.toString = function() {
	var seen = [];

	return JSON.stringify(this, function(key, val) {
		if (val !== null && typeof val == 'object') {
			if (seen.indexOf(val) !== -1) {
				try {
					JSON.stringify(val);
					return val;
				} catch (e) {
					return 'cyclic value!';
				}
			}
			seen.push(val);
		}
		return val;
	});
};


//Escape HTML characters
String.prototype.escapeHtml = function() {
	return String(this).replace(/<.*>/g, function(s) {
		return s.replace(/(<|>)/g, function(ele) {
			return {
				"<": "&lt;",
				">": "&gt;"
			}[ele];
		});
	});
};


//Forcing the toJSON to keep the Timezone information
Date.prototype.toJSON = Date.prototype.toString;

/**
 * @deprecated
 * @use toSAPDate (Not prototype)
 */
Date.prototype.toSAPDate = function() {
	var year = String(this.getFullYear());
	var month = String(this.getMonth() + 1);
	month = month.length === 1 ? '0' + month : month;
	var date = this.getDate().toString();
	date = date.length === 1 ? '0' + date : date;
	return year + month + date;
};

/**
 * @param {Date} date date to parse
 * @returns {String} Date in SAP/ABAP format
 */
this.toSAPDate = function(date){
    const isoDate = date.toISOString();
    const onlyDate = isoDate.split('T')[0];
    return onlyDate.replace(/-/g, '');
}

/**
 * @deprecated
 * @use toTIMPDate (Not prototype)
 */
Date.toTimpDate = function(_date) {
	if (!_date) {
		return '#';
	}

	if (typeof _date === 'object') {
		_date = _date.toString();
	}

	_date = new Date(_date);
	if (isNaN(_date)) {
		return '#';
	}

	var parts = _date.toString().split(' ');
	var months = {
		'Jan': '01',
		'Feb': '02',
		'Mar': '03',
		'Apr': '04',
		'May': '05',
		'Jun': '06',
		'Jul': '07',
		'Aug': '08',
		'Sep': '09',
		'Oct': '10',
		'Nov': '11',
		'Dec': '12'
	};
	return [parts[2], months[parts[1]], parts[3]].join('/');
};

/**
 * @param {Date} date date to parse
 * @returns {String} Date in timp format
 */
this.toTIMPDate = function(date){
    if (!date) {
		return '#';
	}
	const isoDate = date.toISOString();
    const onlyDate = isoDate.split('T')[0];
    return onlyDate.split('-').reverse().join('/');
};