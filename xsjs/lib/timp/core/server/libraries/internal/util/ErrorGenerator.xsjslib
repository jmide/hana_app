function ErrorGenerator(functionName) {
	this.init(functionName);
}

ErrorGenerator.prototype = {
	init: function(functionName) {
		this.functionName = functionName;
		ErrorGenerator.categories = {
			create: '0000',
			read: '1000',
			update: '2000',
			delete: '3000',
			custom: '4000',
			internal: '5000',
			params: '6000',
			routeNotFound: '7000',
			authentication: '8000',
			insufficientPrivileges: '9000'
		};
	},
	saveError: function(stack, object) {
		return this.generateError(object, ErrorGenerator.categories.create, null, stack);
	},
	readError: function(stack, object) {
		return this.generateError('00', '99', null, stack);
	},
	updateError: function(stack, object, data) {
		return this.generateError(object, ErrorGenerator.categories.update, data, stack);
	},
	deleteError: function(stack, object, data) {
		return this.generateError(object, ErrorGenerator.categories.delete, data, stack);
	},
	internalError: function(stack) {
		return this.generateError('00', '05', null, stack);
	},
	paramsError: function(stack) {
		if(!$.lodash.isNil(stack) && !$.lodash.isStringArray(stack)) {
			stack = $.lodash.map(stack, function(field) {
				return {
					attribute: field,
					message: 'Invalid field.'
				};
			});
		}

		return this.generateError('00', '06', null, stack);
	},
	authenticationError: function(stack) {
		return this.generateError('00', '08', null, stack);
	},
	insufficientPrivilegesError: function(stack) {
		return this.generateError('00', '09', null, stack);
	},
	generateError: function(object, category, customData, stackedError) {
		let stack = [];
	    
		let error = {
			category: category,
			functionName: this.functionName
		};
		
		if (!$.lodash.isNil(object)) {
            error.object = object;		    
		}
        
		if (!$.lodash.isNil(customData)) {
		    if (!$.lodash.isNil(customData.objectId)) {
                error.objectId = customData.objectId;
                delete error.objectId;
            }
		    error.data = customData;
		}

		if ($.lodash.isArray(stackedError)) {
			stack = stackedError;
		} else if (!$.lodash.isNil(stackedError) && $.lodash.has(stackedError, 'stack') && !$.lodash.isNil(stackedError.stack)) {
			stack = stackedError.stack;
		} else if (!$.lodash.isNil(stackedError)){
			stack.unshift(stackedError);
		}

		stack.unshift($.lodash.cloneDeep(error));

		error.stack = stack;

		return error;
	}
};

this.ErrorGenerator = ErrorGenerator;