/* 
    Add any desired content/types that you wish to send
*/
var _contentTypes = [
    'text/css',
    'text/csv',
    'text/html',
    'image/jpeg',
    'application/javascript',
    'application/json',
    'application/xml',
    'text/plain',
    'text/xml',
    'text/json',
    'application/pdf'
];


/**
    Add any desired requestHeaders that you wish to send
*/
var _requestHeaders = {
	'Accept': 'string',
	'Accept-Charset': 'string',
	'Accept-Encoding': 'string',
	'Accept-Language': 'string',
	'Access-Control-Request-Method': 'string',
	'Access-Control-Request-Headers': 'string',
	'Authorization': 'string',
	'Cache-Control': 'string',
	'Connection': 'string',
	'Cookie': 'object', //Key Value Object
	'Content-Length': 'integer',
	'Conent-MD5': 'string',
	'Content-Type': 'string',
	'Date': 'string',
	'Host': 'string',
	'Origin': 'string',
	'Proxy-Authorization': 'string',
	'X-Requested-With': 'string',
	'Proxy-Connection': 'string',
	'X-Csrf-Token': 'string',
	'X-CSRFToken': 'string',
	'X-XSRF-TOKEN': 'string'
};


const xshttpdestMap = {
    'TMFFiscalPeriodGetStatusSyncIn':'TMFFiscalPeriodGetStatusSyncIn',
    'TMFFiscalPeriodChangeStatusSyncIn':'TMFFiscalPeriodChangeStatusSyncIn',
    'TMFNotaFiscalCorrectionsSyncIn':'TMFNotaFiscalCorrectionsSyncIn',
    'TMFFIPostingPostSyncIn':'TMFFIPostingPostSyncIn',
    'TMFNFCorrectionLetterCreationSyncIn':'TMFNFCorrectionLetterCreationSyncIn',
    'TMFFIReversalPostSyncIn':'TMFFIReversalPostSyncIn',
    'TMFNFCreationSyncIn':'TMFNFCreationSyncIn',
    'TMFCIAPCreditFactorModifySyncIn':'TMFCIAPCreditFactorModifySyncIn',
    'TMFCIAPAccountingSyncIn':'TMFCIAPAccountingSyncIn',
    'TMFELSSetMessageSyncIn':'TMFELSSetMessageSyncIn',
    'TMFELSGetPpidSyncIn':'TMFELSGetPpidSyncIn',
    'TMFEFDReportRun':'TMFEFDReportRun',
    'TMFPCOReportRun':'TMFPCOReportRun',
    'TMFECDReportRun':'TMFECDReportRun',
    'TMFECFreportExecutionSyncIn':'TMFECFreportExecutionSyncIn',
    'TMFTOMExportFile':'TMFTOMExportFile'
};


/**
 * @description Returns XS Request Method
 * @param {string} method - Contains desired request method Ex. 'POST'
 * @returns Returns XS request method object
*/
function _getRequestMethod(method) {
	const requestMethod = {
		'POST': $.net.http.POST,
		'GET': $.net.http.GET,
		'PUT': $.net.http.PUT,
		'PATCH': $.net.http.PATCH,
		'DEL': $.net.http.DEL,
		'OPTIONS': $.net.http.OPTIONS,
		'TRACE': $.net.http.TRACE,
		'CONNECT': $.net.http.CONNECT
	};
	if ($.lodash.isString(method) && $.lodash.has(requestMethod, method)) {
		return requestMethod[method];
	} else {
		return $.net.http.GET;
	}
}


/**
 * @description Validates request parameters sent to externalClientRequestFunction. Request Parameters must be in an object 
 * type form that can contain the request content type and the headers you want to append to the request. In the controller
 * variables are contained all mapped content types in the variable _contentType as well as all request headers and their
 * value type.
 * @example Request Parameter Example: 
 *	{
 *		contentType: 'application/xml',
 *		headers: {
 *			'Accept': 'application/xml',
 *			'Accept-Charset': 'utf-8',
 *			'Accept-Language': 'en-US',
 * 			'Authorization': 'Basic VVNFUl9USU1QOkhEQnRkZjAwJA==
 *			'Cache-Control': 'no-cache',
 *			'Cookie': {
 *				'$Version': 1,
 *				'Content-Language': 'enus'
 *			},
 *			'Content-Length': 350
 *		},
 *		body: '<>'
 *	}
 * @param {Object} requestParameter - Request Parameter object
 * @returns {boolean} Returns true/false if parameter is valid
*/
function _validateRequestParameters(requestParameters) {
	var jsonSchema = {
		properties: {
			contentType: {
				type: 'string',
				'enum': _contentTypes
			},
			headers: {
				type: 'object',
				properties: {}
			},
			body: {
			    type: 'any'
			}
		}
	};
	Object.keys(_requestHeaders).forEach(function(header) {
		jsonSchema.properties.headers.properties[header] = {
			type: _requestHeaders[header]
		};
	});
	return $.validator.validate(requestParameters, jsonSchema).valid;
}


/* */
function _createWebRequest(requestMethod, destination, requestParameters) {
	var request = new $.web.WebRequest(requestMethod, '');
	if ($.lodash.has(requestParameters, 'contentType')) {
		request.contentType = requestParameters.contentType;
	}
	if ($.lodash.has(requestParameters, 'headers')) {
		Object.keys(requestParameters.headers).forEach(function(header) {
			if ($.lodash.isEqual(header, 'Cookie')) {
				Object.keys(request.Parameters.headers.Cookie).forEach(function(cookie) {
					request.cookies.set(cookie, request.Parameters.headers.Cookie[cookie]);
				});
			} else {
				request.headers.set(header, requestParameters.headers[header]);
			}
		});
	}
	if ($.lodash.has(requestParameters, 'body')) {
	    request.setBody(requestParameters.body);
	}
	return request;
}


/**
 * 	@description Function that performs a external system call to the destination passed with the parameters described and method.
 *  @param {string} method - String that indicates what method will be used Ex. 'POST', 'GET'
 *  @param {string} destination - Url that you want to call
 *  @param {Object} parameters - Request parameters that include content type, headers, cookies and body
 *  @param {string} parameters.contentType - Request Content type. Mapped content types can be found at the private variable _contentType
 *  @param {Object} parameters.headers - Request headers. Mapped headers can be found at private variable _requestHeaders
 *  @param {string} parameters.body - Request body
 *  @returns {Object} Returns client response
*/
this.externalClientRequest = function(method, destination, parameters,httpsIntegration, serviceName) {
	var response = {
		errors: [],
		results: []
	};
 	if (httpsIntegration) {
 		return this.externalSecureClientRequest(method, destination, parameters, serviceName);
    }
	var client = new $.net.http.Client();
	var requestMethod = _getRequestMethod(method);
	var areValidParameters = _validateRequestParameters(parameters);
	if (areValidParameters) {
		try {
			let request = _createWebRequest(requestMethod, destination, parameters);
			client.request(request, destination);
			let clientResponse = client.getResponse();
			client.close();
			return clientResponse;
		} catch (e) {
			response.errors.push($.parseError(e));
			return response;
		}
	} else {
		return false;
	}
};


/**
 * 	@description Function that performs a external system call to the destination passed with the parameters described and method.
 *  @param {string} method - String that indicates what method will be used Ex. 'POST', 'GET'
 *  @param {Object} parameters - Request parameters that include content type, headers, cookies and body
 *  @param {string} parameters.contentType - Request Content type. Mapped content types can be found at the private variable _contentType
 *  @param {Object} parameters.headers - Request headers. Mapped headers can be found at private variable _requestHeaders
 *  @param {string} parameters.body - Request body
 *  @returns {Object} Returns client response
*/
this.externalSecureClientRequest = function(method, destination, parameters, serviceName) {
	var response = {
		errors: [],
		results: []
	};//
	var destination = $.net.http.readDestination('timp.core.server.libraries.internal',xshttpdestMap[serviceName]);
	var client = new $.net.http.Client();
	var requestMethod = _getRequestMethod(method);
	var areValidParameters = _validateRequestParameters(parameters);
	if (areValidParameters) {
		try {
			let request = _createWebRequest(requestMethod, destination, parameters);
			client.request(request, destination);
			let clientResponse = client.getResponse();
			client.close();
			return clientResponse;
		} catch (e) {
			response.errors.push($.parseError(e));
			return response;
		}
	} else {
		return false;
	}
};