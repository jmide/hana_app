var performance = {};

$.getPerformance = function(){
    $.import('timp.core.server.libraries.external','lodash');
    let _ = $.timp.core.server.libraries.external.lodash;
    performance.total = _.reduce(performance, (acc, value) =>{
        return acc + value;
    }, 0);
    return performance;
}

$.setPerformance = function(name){
    if(performance[name] && performance[name].start != undefined ){
        let total = ( Date.now() - performance[name].start ) / 1000;
        performance[name] = total;
    }else {
        performance[name] = 'tracker duplicado set: ' + name ; 
    }
}

$.initPerformance = function(name){
    if(performance[name] == undefined){
        performance[name] = {};
        performance[name].start = Date.now();
    }else{
        performance[name] = 'tracker duplicado init: ' + name ;
    }
}