const _this = this;

$.import('timp.core.server.libraries.external', 'lodash');
this.lodash = $.timp.core.server.libraries.external.lodash;
$.lodash = this.lodash;

$.import('timp.core.server.orm', 'BaseModel');
this.BaseModel = $.timp.core.server.orm.BaseModel.BaseModel;

$.import('timp.core.server', 'config');
this.schema = $.timp.core.server.config.application.schema;
$.version = $.timp.core.server.config.application.version;
$.schema = this.schema;
$.viewSchema = '_SYS_BIC';

$.import('timp.core.server.libraries.external', 'revalidator');
this.validator = $.timp.core.server.libraries.external.revalidator.jsonValidator;
$.validator = this.validator;

$.import('timp.core.server.libraries.external', 'moment');
this.moment = $.timp.core.server.libraries.external.moment.moment;
$.moment = this.moment;

$.import('timp.core.server.orm', 'services');
this.services = $.timp.core.server.orm.services;

$.import('timp.core.server.orm', 'tableServices');
const tableServices = $.timp.core.server.orm.tableServices;

this.lodash.forEach(this.lodash.keys(tableServices), function(value) {
	if (tableServices.hasOwnProperty(value) && _this.lodash.isFunction(tableServices[value])) {
		_this.services[value] = tableServices[value];
	}
});

$.import('timp.core.server.libraries.internal.util', 'ErrorGenerator');
$.ErrorGenerator = $.timp.core.server.libraries.internal.util.ErrorGenerator.ErrorGenerator;
$.import('timp.core.server.libraries.internal.util', 'xsTools');
$.import('timp.core.server.libraries.internal.util', 'cryptography');
$.import('timp.core.server.libraries.internal.util', 'generalInformation');
$.import('timp.core.server.libraries.internal.util', 'orm');
$.import('timp.core.server.libraries.internal.util', 'X2J');
$.import('timp.core.server.libraries.internal.util', 'legacyUtil');

let utilFiles = [
    $.timp.core.server.libraries.internal.util.xsTools,
    $.timp.core.server.libraries.internal.util.cryptography,
    $.timp.core.server.libraries.internal.util.generalInformation,
    $.timp.core.server.libraries.internal.util.orm,
    $.timp.core.server.libraries.internal.util.X2J,
    $.timp.core.server.libraries.internal.util.legacyUtil
];

_this.lodash.forEach(utilFiles, function(_import) {
	_this.lodash.forEach(_this.lodash.keys(_import), function(value) {
		if (_import.hasOwnProperty(value) && _this.lodash.isFunction(_import[value])) {
			_this[value] = _import[value];
		}
	});
});

/**
 * Returns a string with the first letter capitalized
 * @param {String} string - The string used to capitalize
 * @return {String} A string with the first letter capitalize
 */
this.capitalizeFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

$.mapTracker = function(json){
    var tracker = [];
    var jsonArray = json.details || json;
	if (jsonArray && Array.isArray(jsonArray) && jsonArray.length > 0) {
		jsonArray.forEach((e) => {
		    e.newValue = e.newValue || e.name || '';
		    e.oldValue = e.oldValue || '';
		    
			tracker.push({
				field: e.message.ptrbr || '',
				newValue: e.newValue.ptrbr || e.newValue || '',
				oldValue: e.oldValue.ptrbr || e.oldValue || ''
			});
		});
	}
	
	return tracker;
}


$.getApi = function(component, api){
    api = api || 'api';
	$.import(`timp.${component}.server.api`, api);
 	return $.timp[component].server.api[api];
};