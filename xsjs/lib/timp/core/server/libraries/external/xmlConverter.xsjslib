// --------------------------------------------------------------------------
// xml2json.xsjslib
//
// Simple XML to JSON converter
//
// Important Note:
//
// This XML to JSON converter is a very simple converter which is designed
// to parse XML documents from secure sources such as PPMS and CTS+. This
// is not a general purpose XML parser and it is not suited for parsing XML
// documents from untrusted sources.
//
// For the simple Xpath implementation do not use paths (or parts thereof) 
// that haven’t been validated (risk of denial of service attacks).
//
// THIS PARSER MUST NOT BE USED FROM ANY APPLICATION OTHER THAN THE
// SAP HANA APPLICATION LIFECYCLE MANAGER:
// --------------------------------------------------------------------------

var START_TAG = 'o';
var END_TAG = 'c';
var EMPTY_ELEMENT_TAG = 'a';

function xmlElement(parent) {
	//   this.parent = parent;

	this.isArray = function(name) {
		return Object.prototype.toString.call(this[name]) === '[object Array]';
	}

	function getRootNode(json) {
		while (json.parent != null) {
			json = json.parent;
		}
		return json;
	}

	this.evaluate = function(query) {

		function getWildCardNodes(node, query) {

			var resultSet = [];
			for (var key in node) {
				if (key in xml2json.forbiddenElementNames) {
					continue;
				}
				if (key === query) {
					var elems = node[key];
					elems.forEach(function(elem) {
						resultSet.push(elem);
					});
				} else {
					var elems = node[key];
					elems.forEach(function(elem) {
						resultSet = resultSet.concat(getWildCardNodes(elem, query));
					});
				}
			}
			return resultSet;
		}

		var textQuery = '[text()]';
		var isTextQuery = false;
		var startNode = this;
		var wildcardQuery = false;

		if (query.startsWith(".//")) {
			// wildcard query from here
			//
			query = query.substring(3);
			wildcardQuery = true;
		}
		if (query.startsWith("./")) {
			// select nodes from the current node (default)
			//
			query = query.substring(2);
		} else if (query.startsWith('//')) {
			query = query.substring(2);
			startNode = getRootNode(startNode);
			wildcardQuery = true;
		} else if (query.startsWith('/')) {
			query = query.substring(1);
			startNode = getRootNode(startNode);
		}

		if (query.endsWith(textQuery)) {
			isTextQuery = true;
			query = query.substring(0, query.length - textQuery.length);
		}

		var previousCandidates = [];
		var candidates = [startNode];

		if (wildcardQuery) {
			if (query.search("/") !== -1) {
				throw "Wildcard queries may only contain a single path element";
			}
			candidates = getWildCardNodes(startNode, query);
		} else {

			var elems = query.split('/');
			elems.forEach(function(elem) {
				previousCandidates = candidates;
				candidates = [];
				previousCandidates.forEach(function(cand) {
					if (cand[elem]) {
						var cds = cand[elem];
						if (cand.isArray(elem)) {
							cds.forEach(function(c) {
								candidates.push(c);
							});
						} else {
							candidates.push(cds);
						}
					}
				});
			});
		}
		if (isTextQuery) {
			var nodes = candidates;
			candidates = [];
			nodes.forEach(function(n) {
				if (n.$) {
					candidates.push(n.$);
				} else {
					candidates.push("");
				}
			});
		}
		return candidates;
	};

	this.jsonStringify = function(obj) {
		if (!obj) {
			obj = this;
		}
		return JSON.stringify(obj, function(key, val) {
			if (key === 'parent') {
				return undefined;
			} else {
				return val;
			}
		});
	}
};

function unescape(text) {
	var t = text;
	if (t === undefined) {
		return t;
	}
	t = t.replace(/&lt;/g, "<");
	t = t.replace(/&gt;/g, ">");
	t = t.replace(/&amp;/g, "&");
	return t;
}

var xml2json = {

	namePattern: /^[A-Za-z_][A-Za-z0-9_-]+/,
	elementNamePattern: /^[A-Za-z_][A-Za-z0-9_-]*/,
	elementNamePatternWithNameSpace: /^([A-Za-z_][A-Za-z0-9_-]*):([A-Za-z_][A-Za-z0-9_-]+)/,
	whiteSpace: /^[\s\n\r]*/,
	whiteSpaceFollowedByElementPattern: /^[\s\n\r]*</,
	textPattern: /^[^<]+/,
	prologPattern: /^<\?xml[^\?>]*\?>/,
	valueMatch: /^"([^""]*)"/,

	forbiddenElementNames: {
		"isArray": true,
		"evaluate": true,
		"jsonStringify": true,
		"attrs": true,
		"parent": true,
		"$": true
	},

	/**
	 * Parse the XML document
	 *
	 * @param options optional parameter with possible property "ONLY_ARRAY"
	 * so all elements will be stored in arrays
	 */
	parse: function(xmlString, options) {

		this.options = options !== undefined ? options : {};
		this.jsonDocument = new xmlElement(null);
		this.currentJsonDocument = this.jsonDocument;
		this.pos = 0;
		this.xmlString = xmlString;
		try {
			this.prolog();
			this.parseElement();
		} catch (e) {
			throw "Error at position: " + this.pos + ': ' + e;
		}
		return this.jsonDocument;
	},

	parseElement: function(ptag) {

		var openTag;
		if (ptag) {
			openTag = ptag;
		} else {
			openTag = this.readTag();
		}
		if (openTag.type !== START_TAG && openTag.type !== EMPTY_ELEMENT_TAG) {
			throw "open tag expected";
		}
		if (this.forbiddenElementNames.hasOwnProperty(openTag.name)) {
			throw 'Reserved keyword "' + openTag.name + '" in xml';
		}

		var oldJsonDocument = this.currentJsonDocument;
		var newElement = new xmlElement(oldJsonDocument);

		if (!this.currentJsonDocument[openTag.name]) {
			if (this.options.ONLY_ARRAY) {
				this.currentJsonDocument[openTag.name] = [newElement];
				this.currentJsonDocument = newElement;
			} else {
				this.currentJsonDocument = this.currentJsonDocument[openTag.name] = newElement;
			}
		} else {
			// if (this.currentJsonDocument.isArray(openTag)) { // Original Code
			if (this.currentJsonDocument[openTag.name] instanceof Array) {
				this.currentJsonDocument[openTag.name].push(newElement);
				this.currentJsonDocument = newElement;
			} else {
				var elemArray = [this.currentJsonDocument[openTag.name], newElement];
				this.currentJsonDocument[openTag.name] = elemArray;
				this.currentJsonDocument = newElement;
			}
		}

		this.currentJsonDocument.attrs = openTag.attrs;

		if (openTag.type === START_TAG) {
			while (true) {
				if (this.whiteSpaceFollowedByElement()) {
					var tag = this.readTag();
					if (tag.type === START_TAG || tag.type === EMPTY_ELEMENT_TAG) {
						this.parseElement(tag);
					} else if (tag.type === END_TAG && tag.name === openTag.name) {
						// done here
						break;
					} else {
						throw 'Excpected closing tag "' + openTag.name + '" but received "' + tag.name + '"';
					}

				} else {
					var tx = this.text();
					// this.currentJsonDocument.$ = unescape(tx); // Original Code
					oldJsonDocument[openTag.name] = unescape(tx);
				}
			}
		}

		this.currentJsonDocument = oldJsonDocument;

	},

	readTag: function() {
		var type;
		this.skipWhiteSpace();
		this.checkEnd();
		if (this.xmlString[this.pos] !== '<') {
			throw "open element expected";
		}
		this.pos++;
		this.checkEnd();
		if (this.xmlString[this.pos] === '/') {
			this.pos++;
			type = END_TAG;
		} else {
			type = START_TAG;
		}
		this.checkEnd();
		var name, ns;

		match = this.match(this.elementNamePatternWithNameSpace);
		if (match) {
			ns = match[1];
			name = match[2];
			this.pos += match[0].length;
		} else {
			var match = this.match(this.elementNamePattern);
			if (!match) {
				throw "cannot parse start tag " + this.xmlString.substring(this.pos);
			}
			name = match[0];
			this.pos += name.length;
		}
		var attrs = this.readAttributes();

		this.checkEnd();
		if (this.xmlString[this.pos] === '/') {
			type = EMPTY_ELEMENT_TAG;
			this.pos++;
		}
		this.checkEnd();
		if (this.xmlString[this.pos] !== '>') {
			throw "open element expected";
		}
		this.pos++;
		let obj = {
			ns: ns,
			name: name,
			type: type
		};
		if(this.options.withAttrs){
		    obj.attrs = attrs;
		}
		return obj;
	},

	readAttributes: function() {
		var attrs = {};
		while (true) {
			var name, ns;
			var match;
			this.skipWhiteSpace();
			match = this.match(this.elementNamePatternWithNameSpace);
			if (match) {
				ns = match[1];
				name = match[2];
				this.pos += match[0].length;
			} else {
				match = this.match(this.elementNamePattern);
				if (!match) {
					break; // no attributes
				}
				name = match[0];
				this.pos += name.length;
			}
			if (this.xmlString[this.pos] !== '=') {
				throw "= expected";
			}
			this.pos++;
			match = this.match(this.valueMatch);
			if (!match) {
				throw "attribute value expected"
			}
			attrs[name] = unescape(match[1]);
			this.pos += match[0].length;
		}
		return attrs;
	},

	skipWhiteSpace: function() {
		var m = this.match(this.whiteSpace);
		if (m) {
			this.pos += m[0].length;
		}
	},

	whiteSpaceFollowedByElement: function() {
		var match = this.match(this.whiteSpaceFollowedByElementPattern);
		if (match) {
			this.pos += match[0].length - 1;
			return true;
		} else {
			return false;
		}
	},

	match: function(pattern) {
		var m = this.xmlString.substring(this.pos).match(pattern);
		//    print('pattern: ' + pattern + ' string: |' + this.xmlString.substring(this.pos) + '| match: ' + (m !== null) + ' pos ' + this.pos);
		return m;
	},

	text: function() {
		this.checkEnd();
		var match = this.match(this.textPattern);
		if (match) {
			this.pos += match[0].length;
			return match[0];
		} else {
			throw "Expected text";
		}
	},

	prolog: function() {
		// skip
		var m = this.match(this.prologPattern);
		if (m) {
			this.pos += m[0].length;
		}

	},
	checkEnd: function() {
		if (this.pos === this.xmlString.length) {
			throw "unexpected end of document";
		}
	}

};
this.xml2json = xml2json;

/**
 * Validate an xml file ("DTD for the poor")
 *
 * @param template template for the XML file
 * @param xmlObj result returned from the parse method
 */
function validateXml(template, xmlObj, msgStack) {

	if (msgStack === undefined) {
		msgStack = [];
	}
	template.forEach(function(ins) {
		msgStack.push(ins[0]);
		var res = xmlObj.evaluate(ins[0]);
		var cardinality = ins[1];
		if (!((cardinality === "1" && res.length === 1) || (cardinality === "0..1" && res.length <= 1) || (cardinality === "1..n" && res.length >=
			1) || (cardinality === "0..n" && res.length >= 0))) {
			throw "Validation error at " + msgStack.join("/") + ": Expected " + cardinality + " children but got " + res.length;
		}
		if (ins.length === 3) {
			for (var i = 0; i < res.length; i++) {
				msgStack.pop();
				msgStack.push(ins[0] + "[" + i + "]");
				var childXmlObj = res[i];
				validateXml(ins[2], childXmlObj, msgStack);
			}
		}
		msgStack.pop();
	});
}