/* Revalidator documentation at: https://github.com/flatiron/revalidator */
$.import('timp.core.server.libraries.external', 'lodash');
const lodash = $.timp.core.server.libraries.external.lodash;
const _this = this;

function mixin(obj) {
	var sources = Array.prototype.slice.call(arguments, 1);
	while (sources.length) {
		var source = sources.shift();
		if (!source) {
			continue;
		}

		if (typeof(source) !== 'object') {
			throw new TypeError('mixin non-object');
		}

		for (var p in source) {
			if (source.hasOwnProperty(p)) {
				obj[p] = source[p];
			}
		}
	}
	return obj;
}

function isDate(value) {
	return Object.prototype.toString.call(value) === '[object Date]';
}

function checkType(val, type, callback) {
	var result = false,
		types = lodash.isArray(type) ? type : [type];

	// No type - no check
	if (type === undefined) return callback(null, type);

	// Go through available types
	// And fine first matching
	for (var i = 0, l = types.length; i < l; i++) {
		type = types[i].toLowerCase().trim();
		if (type === 'string' ? typeof val === 'string' :
			type === 'array' ? lodash.isArray(val) :
			type === 'object' ? val && typeof val === 'object' &&
			!lodash.isArray(val) :
			type === 'number' ? typeof val === 'number' :
			type === 'integer' ? typeof val === 'number' && Math.floor(val) === val :
			type === 'null' ? val === null :
			type === 'boolean' ? typeof val === 'boolean' :
			type === 'date' ? isDate(val) :
			type === 'any' ? typeof val !== 'undefined' : false) {
			return callback(null, type);
		}
	}

	callback(true);
}

this.jsonValidator = {};

this.jsonValidator.validate = function(object, schema, options) {
    object = object ? object : {};
	options = mixin({}, this.validate.defaults, options);
	var errors = [];

	if (schema.type === 'array') {
		this.validateProperty(object, object, '', schema, options, errors);
	} else {
		this.validateObject(object, schema, options, errors);
	}
	return {
		valid: !(errors.length),
		errors: errors
	};
};

this.jsonValidator.validate.messages = {
	required: "Is required",
	allowEmpty: "Must not be empty",
	minLength: "Is too short (minimum is %{expected} characters)",
	maxLength: "Is too long (maximum is %{expected} characters)",
	pattern: "Invalid input",
	minimum: "Must be greater than or equal to %{expected}",
	maximum: "Must be less than or equal to %{expected}",
	exclusiveMinimum: "Must be greater than %{expected}",
	exclusiveMaximum: "Must be less than %{expected}",
	divisibleBy: "Must be divisible by %{expected}",
	minItems: "Must contain more than %{expected} items",
	maxItems: "Must contain less than %{expected} items",
	uniqueItems: "Must hold a unique set of values",
	format: "Is not a valid %{expected}",
	conform: "Must conform to given constraint",
	type: "Must be of %{expected} type",
	additionalProperties: "Must not exist",
	'enum': 'Must be present in given enumerator'
};

this.jsonValidator.validate.defaults = {
	/**
	 * <p>
	 * Enforce 'format' constraints.
	 * </p><p>
	 * <em>Default: <code>true</code></em>
	 * </p>
	 */
	validateFormats: true,
	/**
	 * <p>
	 * When {@link #validateFormats} is <code>true</code>,
	 * treat unrecognized formats as validation errors.
	 * </p><p>
	 * <em>Default: <code>false</code></em>
	 * </p>
	 *
	 * @see validation.formats for default supported formats.
	 */
	validateFormatsStrict: false,
	/**
	 * <p>
	 * When {@link #validateFormats} is <code>true</code>,
	 * also validate formats defined in {@link #validate.formatExtensions}.
	 * </p><p>
	 * <em>Default: <code>true</code></em>
	 * </p>
	 */
	validateFormatExtensions: true,
	/**
	 * <p>
	 * When {@link #additionalProperties} is <code>true</code>,
	 * allow additional unvisited properties on the object.
	 * </p><p>
	 * <em>Default: <code>true</code></em>
	 * </p>
	 */
	additionalProperties: false
};

this.jsonValidator.validate.formatExtensions = {
	'url': /^(https?|ftp|git):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
};

this.jsonValidator.validate.formats = {
	'email': /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
	'ip-address': /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i,
	'ipv6': /^([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4}$/,
	'date-time': /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:.\d{1,3})?Z$/,
	'date': /^\d{4}-\d{2}-\d{2}$/,
	'time': /^\d{2}:\d{2}:\d{2}$/,
	'color': /^#[a-z0-9]{6}|#[a-z0-9]{3}|(?:rgb\(\s*(?:[+-]?\d+%?)\s*,\s*(?:[+-]?\d+%?)\s*,\s*(?:[+-]?\d+%?)\s*\))aqua|black|blue|fuchsia|gray|green|lime|maroon|navy|olive|orange|purple|red|silver|teal|white|yellow$/i,
	//'style':        (not supported)
	//'phone':        (not supported)
	//'uri':          (not supported)
	'host-name': /^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])/,
	'utc-millisec': {
		test: function(value) {
			return typeof(value) === 'number' && value >= 0;
		}
	},
	'regex': {
		test: function(value) {
			try {
				new RegExp(value);
			} catch (e) {
				return false;
			}
			return true;
		}
	}
};

this.jsonValidator.validateObject = function(object, schema, options, errors) {
	var props, allProps = Object.keys(object),
		visitedProps = [];

	// see 5.2
	if (schema.properties) {
		props = schema.properties;
		for (var p in props) {
			if (props.hasOwnProperty(p)) {
				visitedProps.push(p);
				this.validateProperty(object, object[p], p, props[p], options, errors);
			}
		}
	}

	// see 5.3
	if (schema.patternProperties) {
		props = schema.patternProperties;
		for (var p in props) {
			if (props.hasOwnProperty(p)) {
				var re = new RegExp(p);

				// Find all object properties that are matching `re`
				for (var k in object) {
					if (object.hasOwnProperty(k)) {
						if (re.exec(k) !== null) {
							this.validateProperty(object, object[k], k, props[p], options, errors);
							visitedProps.push(k);
						}
					}
				}
			}
		}
	}

	//if additionalProperties is not defined set default value
	if (schema.additionalProperties === undefined) {
		schema.additionalProperties = options.additionalProperties;
	}

	// see 5.4
	if (undefined !== schema.additionalProperties) {
		var i, l;

		var unvisitedProps = allProps.filter(function(k) {
			return -1 === visitedProps.indexOf(k);
		});

		// Prevent additional properties; each unvisited property is therefore an error
		if (schema.additionalProperties === false && unvisitedProps.length > 0) {
			for (i = 0, l = unvisitedProps.length; i < l; i++) {
				this.error("additionalProperties", unvisitedProps[i], object[unvisitedProps[i]], false, errors);
			}
		}
		// additionalProperties is a schema and validate unvisited properties against that schema
		else if (typeof schema.additionalProperties == "object" && unvisitedProps.length > 0) {
			for (i = 0, l = unvisitedProps.length; i < l; i++) {
				this.validateProperty(object, object[unvisitedProps[i]], unvisitedProps[i], schema.unvisitedProperties, options, errors);
			}
		}
	}
};

this.jsonValidator.validateProperty = function(object, value, property, schema, options, errors) {
	var format,
		valid,
		spec,
		type;
	var _self = this;

	function constrain(name, value, assert) {
		if (schema[name] !== undefined && !assert(value, schema[name])) {
			_self.error(name, property, value, schema, errors);
		}
	}

	if (value === undefined) {
		if (schema.required && schema.type !== 'any') {
			return this.error('required', property, undefined, schema, errors);
		} else {
			return;
		}
	}

	if (options.cast) {
		if (('integer' === schema.type || 'number' === schema.type) && value == +value) {
			value = +value;
			object[property] = value;
		}

		if ('boolean' === schema.type) {
			if ('true' === value || '1' === value || 1 === value) {
				value = true;
				object[property] = value;
			}

			if ('false' === value || '0' === value || 0 === value) {
				value = false;
				object[property] = value;
			}
		}
	}

	if (schema.format && options.validateFormats) {
		format = schema.format;

		if (options.validateFormatExtensions) {
			spec = this.validate.formatExtensions[format];
		}
		if (!spec) {
			spec = this.validate.formats[format];
		}
		if (!spec) {
			if (options.validateFormatsStrict) {
				return this.error('format', property, value, schema, errors);
			}
		} else {
			if (!spec.test(value)) {
				return this.error('format', property, value, schema, errors);
			}
		}
	}

	if (schema['enum'] && schema['enum'].indexOf(value) === -1) {
		this.error('enum', property, value, schema, errors);
	}

	// Dependencies (see 5.8)
	if (typeof schema.dependencies === 'string' &&
		object[schema.dependencies] === undefined) {
		this.error('dependencies', property, null, schema, errors);
	}

	if (lodash.isArray(schema.dependencies)) {
		for (var i = 0, l = schema.dependencies.length; i < l; i++) {
			if (object[schema.dependencies[i]] === undefined) {
				this.error('dependencies', property, null, schema, errors);
			}
		}
	}

	if (typeof schema.dependencies === 'object') {
		this.validateObject(object, schema.dependencies, options, errors);
	}

	checkType(value, schema.type, function(err, type) {
		if (err) {
			return _self.error('type', property, typeof value, schema, errors);
		}

		constrain('conform', value, function(a, e) {
			return e(a, object);
		});

		switch (type || (lodash.isArray(value) ? 'array' : typeof value)) {
			case 'string':
				constrain('allowEmpty', value, function(a, e) {
					return e ? e : a !== '';
				});
				constrain('minLength', value.length, function(a, e) {
					return a >= e;
				});
				constrain('maxLength', value.length, function(a, e) {
					return a <= e;
				});
				constrain('pattern', value, function(a, e) {
					e = typeof e === 'string' ?
						e = new RegExp(e) :
						e;
					return e.test(a);
				});
				break;
			case 'integer':
			case 'number':
				constrain('minimum', value, function(a, e) {
					return a >= e;
				});
				constrain('maximum', value, function(a, e) {
					return a <= e;
				});
				constrain('exclusiveMinimum', value, function(a, e) {
					return a > e;
				});
				constrain('exclusiveMaximum', value, function(a, e) {
					return a < e;
				});
				constrain('divisibleBy', value, function(a, e) {
					var multiplier = Math.max((a - Math.floor(a)).toString().length - 2, (e - Math.floor(e)).toString().length - 2);
					multiplier = multiplier > 0 ? Math.pow(10, multiplier) : 1;
					return (a * multiplier) % (e * multiplier) === 0;
				});
				break;
			case 'array':
				constrain('items', value, function(a, e) {
					var nestedErrors;
					for (var i = 0, l = a.length; i < l; i++) {
						nestedErrors = [];
						this.validateProperty(object, a[i], property, e, options, nestedErrors);
						nestedErrors.forEach(function(err) {
							err.property =
								(property ? property + '.' : '') +
								i +
								(err.property ? '.' + err.property.replace(property + '.', '') : '');
						});
						nestedErrors.unshift(0, 0);
						Array.prototype.splice.apply(errors, nestedErrors);
					}
					return true;
				});
				constrain('minItems', value, function(a, e) {
					return a.length >= e;
				});
				constrain('maxItems', value, function(a, e) {
					return a.length <= e;
				});
				constrain('uniqueItems', value, function(a, e) {
					if (!e) return true;

					var h = {};

					for (var i = 0, l = a.length; i < l; i++) {
						var key = JSON.stringify(a[i]);
						if (h[key]) {
							return false;
						}
						h[key] = true;
					}

					return true;
				});
				break;
			case 'object':
				// Recursive validation
				if (schema.properties || schema.patternProperties || schema.additionalProperties) {
					var nestedErrors = [];
					_self.validateObject(value, schema, options, nestedErrors);
					nestedErrors.forEach(function(e) {
						e.property = property + '.' + e.property;
					});
					nestedErrors.unshift(0, 0);
					Array.prototype.splice.apply(errors, nestedErrors);
				}
				break;
		}
	});
};

this.jsonValidator.error = function(attribute, property, actual, schema, errors) {
	var lookup = {
		expected: schema[attribute],
		actual: actual,
		attribute: attribute,
		property: property
	};
	var message = schema.messages && schema.messages[attribute] || schema.message || this.validate.messages[attribute] || "no default message";
	message = message.replace(/%\{([a-z]+)\}/ig, function(_, match) {
		return lookup[match.toLowerCase()] || '';
	});
	errors.push({
		attribute: attribute,
		property: property,
		expected: schema[attribute],
		actual: actual,
		message: message
	});
};