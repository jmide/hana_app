/* Documentation Available at: lodash.com/docs/ */
/*
    Checks if value is classified as an Array object.
*/
this.isArray = function(value) {
	return Array.isArray(value);
};


/*
    Checks if value is classified as a Function object.
*/
this.isFunction = function(value) {
	return value instanceof Function;
};


/**
 * Checks if value is classified as an Array object.
 * @param { * } value - The value to check.
 * @returns { boolean } Returns true if value is an array, else false.
 */
this.keys = function(object) {
	const _this = this;
	if (_this.isNil(object)) {
		return [];
	}
	return Object.keys(object);
};


/*
    Checks if value is object-like. A value is object-like if it's not null and has a typeof result of "object".
*/
this.isObjectLike = function(object) {
	const _this = this;
	return !_this.isNil(object) && typeof object === 'object';
};


/*
    Checks if value is null or undefined.
*/
this.isNil = function(value) {
	return value === undefined || value === null;
};


/*
    Checks if value is classified as a String primitive or object.
*/
this.isString = function(value) {
	return typeof value === 'string' || value instanceof String;
};


/*
    Checks if value is classified as a boolean primitive or object.
*/
this.isBoolean = function(value) {
	return typeof value === 'boolean' || value instanceof Boolean;
};


/*
    Checks if value is an empty object, array or string.
    Objects are considered empty if they have no own enumerable string keyed properties.
    Array or Strings are considered empty if they have a length of 0.
*/
this.isEmpty = function(value) {
	const __this__ = this;
	if (!__this__.isNil(value) && Array.isArray(value) || typeof value === 'string') {
		return value.length === 0;
	} else if (!__this__.isNil(value) && typeof value === 'object') {
		return Object.keys(value).length === 0;
	}
	return true;
};


/*
    Checks if value is a plain object, that is, an object created by the Object constructor or one with a [[Prototype]] of null.
*/
this.isPlainObject = function(value) {
	const __this__ = this;
	if (__this__.isNil(value)) {
		return true;
	}
	if (typeof value === 'object') {
		try {
			JSON.stringify(value);
			return true && !__this__.isArray(value);
		} catch (ex) {
			return false;
		}
	}
	return false;
};


/*
    Checks if key(string) is a direct property of value(object).
*/
this.has = function(value, key) {
	const __this__ = this;
	return (!__this__.isNil(value) && __this__.isPlainObject(value) && value.hasOwnProperty(key));
};


/*
    Converts string, as a whole, to lower case just like String#toLowerCase.
*/
this.toLower = function(value) {
	const __this__ = this;
	if (__this__.isString(value)) {
		return value.toLowerCase();
	}
	return value;
};


/*
    Converts string, as a whole, to upper case just like String#toUpperCase.
*/
this.toUpper = function(value) {
	const __this__ = this;
	if (__this__.isString(value)) {
		return value.toUpperCase();
	}
	return value;
};


/*
    Checks if value is classified as a RegExp object.
*/
this.isRegex = function(value) {
	return value instanceof RegExp;
};


/*
    Checks if value is an integer.
*/
this.isInteger = function isInteger(value) {
	return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
};


/*
    Checks if value is a Number.
*/
this.isNumber = function(value) {
	return typeof value === 'number' && !isNaN(value);
};


/*
    Splits string by separator.
*/
this.split = function(string, separator, limit) {
	const __this__ = this;
	if (__this__.isString(string) && (__this__.isString(separator) || __this__.isRegex(separator))) {
		if (!__this__.isInteger(limit)) {
			limit = 0;
		}
		let chars = string.split(separator);
		if (limit > 0) {
			chars = chars.slice(0, limit);
		}
		return chars;
	}
	return [string];
};


/*
    This method recursively clones value.
*/
this.cloneDeep = function(value) {
	let copy;
	const __this__ = this;
	// Handle the 3 simple types, and null or undefined
	if (__this__.isNil(value) || typeof value !== 'object') {
		return value;
	}

	// Handle Date
	if (value instanceof Date) {
		copy = new Date();
		copy.setTime(value.getTime());
		return copy;
	}

	// Handle Array
	if (value instanceof Array) {
		copy = [];
		for (let i = 0, len = value.length; i < len; i++) {
			copy[i] = __this__.cloneDeep(value[i]);
		}
		return copy;
	}

	// Handle Object
	if (value instanceof Object) {
		copy = {};
		let attr;
		for (attr in value) {
			if (__this__.has(value, attr)) {
				copy[attr] = __this__.cloneDeep(value[attr]);
			}
		}
		return copy;
	}
	return null;
};


/*
    Removes leading and trailing whitespace or specified characters from string.
*/
this.trim = function(string, chars) {
	const __this__ = this;
	if (__this__.isNil(chars) || !__this__.isString(chars)) {
		chars = ' ';
	}
	if (!__this__.isString(string)) {
		return string;
	}
	return string.replace(new RegExp(chars, 'g'), '');
};


/* Escapes string so the RegExp constructor can use it correctly */
const escapeString = function(chars) {
	const escapeChars = [' ', '$', '^', '(', ')', '[', ']', '+', '.'];
	let tempChars = [];
	let tempChar;
	for (let i = 0; i < chars.length; ++i) {
		tempChar = chars[i];
		if (escapeChars.indexOf(chars[i]) > -1) {
			if (chars[i] === ' ') {
				tempChar = '\\s';
			} else {
				tempChar = '\\' + chars[i];
			}
		}
		tempChars.push(tempChar);
	}
	return tempChars.join('');
};


/*
    Removes leading whitespace or specified chars from string.
*/
this.trimStart = function(string, chars) {
	const __this__ = this;
	if (__this__.isNil(chars) || !__this__.isString(chars) || __this__.isEmpty(chars)) {
		chars = ' ';
	}
	if (!__this__.isString(string) || string.length < chars.length) {
		return string;
	}
	return string.replace(new RegExp('^' + escapeString(chars) + '+'), '');
};


/*
    Removes trailing whitespace or specified characters from string.
*/
this.trimEnd = function(string, chars) {
	const __this__ = this;
	if (__this__.isNil(chars) || !__this__.isString(chars) || __this__.isEmpty(chars)) {
		chars = ' ';
	}
	if (!__this__.isString(string) || string.length < chars.length) {
		return string;
	}
	return string.replace(new RegExp(escapeString(chars) + '+$'), '');
};


/*
    Assigns own enumerable string keyed properties of source objects to the destination object.
*/
this.assign = function() {
	const __this__ = this;
	for (var i = 1; i < arguments.length; i++) {
		for (var key in arguments[i]) {
			if (__this__.has(arguments[i], key)) {
				arguments[0][key] = arguments[i][key];
			}
		}
	}
	return arguments[0];
};


/*
    Iterates over elements of collection and invokes iteratee for each element. The iteratee is invoked with two arguments: (value, index|key).
*/
this.forEach = function(collection, iteratee) {
	const __this__ = this;
	if (__this__.isFunction(iteratee) && __this__.isArray(collection)) {
		collection.forEach(iteratee);
	} else if (__this__.isFunction(iteratee) && __this__.isObjectLike(collection)) {
		for (let key in collection) {
			if (collection.hasOwnProperty(key)) {
				iteratee(collection[key], key);
			}
		}
	}
};


this.each = this.forEach;


/*
    This method is like forEach except that it iterates over elements of collection from right to left.
*/
this.forEachRight = function(collection, cb) {
	const __this__ = this;
	if (__this__.isFunction(cb) && __this__.isArray(collection)) {
		__this__.forEach(__this__.cloneDeep(collection).reverse(), cb);
	} else if (__this__.isFunction(cb) && __this__.isPlainObject(collection)) {
		__this__.forEach(Object.keys(collection).reverse(), function(key) {
			if (__this__.has(collection, key)) {
				cb(collection[key], key);
			}
		});
	}
};


this.eachRight = this.forEachRight;


/*
    Reverses array so that the first element becomes the last, the second element becomes the second to last, and so on.
*/
this.reverse = function(array) {
	const __this__ = this;
	if (__this__.isArray(array)) {
		array.reverse();
	}
	return array;
};


/*
    Checks if predicate returns truthy for all elements of collection. Iteration is stopped once predicate returns falsey. The predicate is invoked with two arguments: (value, index|key).
*/
this.every = function(collection, predicate) {
	const __this__ = this;
	if (__this__.isFunction(predicate) && __this__.isArray(collection)) {
		collection.every(predicate);
	} else if (__this__.isFunction(predicate) && __this__.isPlainObject(collection)) {
		for (var key in collection) {
			if (__this__.has(collection, key)) {
				let continueEvery = predicate(collection[key], key);
				if (!(__this__.isBoolean(continueEvery) && continueEvery)) {
					break;
				}
			}
		}
	}
};


/*
   Reduces collection to a value which is the accumulated result of running each element in collection thru iteratee, where each successive invocation is supplied the return value of the previous. If accumulator is not given, the first element of collection is used as the initial value. The iteratee is invoked with three arguments:
   (accumulator, value, index|key).

*/
this.reduce = function(collection, iteratee, accumulator) {
	const __this__ = this;
	if (__this__.isFunction(iteratee) && (__this__.isArray(collection) || __this__.isPlainObject(collection))) {
		if (__this__.isNil(accumulator)) {
			accumulator = collection[Object.keys(collection)[0]];
		}
		__this__.forEach(collection, function(item, idx) {
			accumulator = iteratee(accumulator, item, idx);
		});
	}
	return accumulator;
};


/*
    Converts all elements in array into a string separated by separator.
*/
this.join = function(array, separator) {
	const __this__ = this;
	if (__this__.isArray(array) && !__this__.isEmpty(array)) {
		if (!__this__.isString(separator)) {
			separator = ',';
		}
		return array.join(separator);
	}
	return '';
};


/*
    Iterates over elements of collection, returning an array of all elements predicate returns truthy for. The predicate is invoked with two arguments: (value, index|key).
*/
this.filter = function(collection, predicate) {
	const __this__ = this;
	let retVal = [];
	if (__this__.isFunction(predicate) && (__this__.isPlainObject(collection) || __this__.isArray(collection))) {
		__this__.forEach(collection, function(item, idx) {
			let addToRetVal = predicate(item, idx);
			if (__this__.isBoolean(addToRetVal) && addToRetVal) {
				retVal.push(item);
			}
		});
	}
	return retVal;
};


/*
    Creates an array of values by running each element in collection thru iteratee. 
    The iteratee is invoked with three arguments: (value, index|key).
*/
this.map = function(collection, iteratee) {
	const __this__ = this;
	let retVal = [];
	if (__this__.isFunction(iteratee) && (__this__.isPlainObject(collection) || __this__.isArray(collection))) {
		__this__.forEach(collection, function(item, idx) {
			retVal.push(iteratee(item, idx));
		});
	}
	return retVal;
};


/*
    Performs a deep comparison between two values to determine if they are equivalent.
    Note: This method supports comparing arrays,booleans, dates, objects, numbers, strings, and typed arrays. 
    Objects are compared by their own, not inherited, enumerable properties. Functions are compared by strict equality, i.e. ===.
*/
this.isEqual = function(value, other) {
	const __this__ = this;
	if (value === other) {
		return true;
	} else if (typeof value === typeof other && (__this__.isArray(value) || __this__.isPlainObject(value))) {
		//Loop through properties in object 1
		var p;
		for (p in value) {
			//Check property exists on both objects
			if (__this__.has(value, p) !== __this__.has(value, p)) {
				return false;
			}

			switch (typeof(value[p])) {
				//Deep compare objects
				case 'object':
					if (!__this__.isEqual(value[p], other[p])) {
						return false;
					}
					break;
					//Compare function code
				case 'function':
					if (typeof(other[p]) === 'undefined' || (p !== 'isEqual' && value[p].toString() !== other[p].toString())) {
						return false;
					}
					break;
					//Compare values
				default:
					if (value[p] !== other[p]) {
						return false;
					}
			}
		}

		//Check object 2 for any extra properties
		for (p in other) {
			if (typeof(value[p]) === 'undefined') {
				return false;
			}
		}
		return true;
	}
	return false;
};


/*
    Creates an array of unique values, in order,
*/
this.union = function(value, other) {
	const __this__ = this;
	let retVal = [];
	let addElement;
	if (__this__.isArray(value) && __this__.isArray(other)) {
		if (__this__.isEmpty(value)) {
			retVal = __this__.cloneDeep(other);
		} else if (__this__.isEmpty(other)) {
			retVal = __this__.cloneDeep(value);
		} else {
			// Add the values if they're not in the retVal
			__this__.forEach(value, function(item) {
				addElement = true;
				if (!__this__.isEmpty(retVal)) {
					for (let j = 0; j < retVal.length; ++j) {
						if (__this__.isEqual(item, retVal[j])) {
							addElement = false;
							break;
						}
					}
				}
				if (addElement) {
					retVal.push(item);
				}
			});
			__this__.forEach(other, function(item) {
				if (__this__.isEmpty(retVal)) {
					addElement = true;
				} else {
					addElement = true;
					for (let j = 0; j < retVal.length; ++j) {
						if (__this__.isEqual(item, retVal[j])) {
							addElement = false;
							break;
						}
					}
				}
				if (addElement) {
					retVal.push(item);
				}
			});
		}
	}
	return retVal;
};


/*
    Creates a duplicate-free version of an array
*/
this.uniq = function(array) {
	const __this__ = this;
	let retVal = [];
	if (__this__.isArray(array) && !__this__.isEmpty(array)) {
		let addUniqueValue;
		__this__.forEach(array, function(item, idx) {
			addUniqueValue = true;
			if (idx !== 0) {
				for (let i = 0; i < retVal.length; ++i) {
					if (__this__.isEqual(retVal[i], item)) {
						addUniqueValue = false;
						break;
					}
				}
			}
			if (addUniqueValue) {
				retVal.push(item);
			}
		});
	}
	return retVal;
};


/*
    Creates an array of array values not included in the other given arrays using _this.isEqual for equality comparisons.
*/
this.difference = function(array, values = []) {
	const __this__ = this;
	let retVal = [];
	if (__this__.isArray(array) && __this__.isArray(values) && !__this__.isEmpty(array)) {
		if (__this__.isEmpty(values)) {
			retVal = __this__.cloneDeep(array);
		} else {
			let isNotInValues;
			__this__.forEach(array, function(itemA) {
				isNotInValues = true;
				__this__.every(values, function(itemB) {
					if (__this__.isEqual(itemA, itemB)) {
						isNotInValues = false;
					}
					return isNotInValues;
				});
				if (isNotInValues) {
					retVal.push(itemA);
				}
			});
		}
	}
	return retVal;
};


/*
    Creates an array of unique values that are included in two given arrays using isEqual for equality comparisons. The order and references of result values are determined by the first array.
*/
this.intersection = function(array, values) {
	const __this__ = this;
	let retVal = [];
	if (__this__.isArray(array) && __this__.isArray(values) && !__this__.isEmpty(array)) {
		if (__this__.isEmpty(values)) {
			retVal = __this__.cloneDeep(array);
		} else {
			let isInValues;
			__this__.forEach(array, function(itemA) {
				isInValues = false;
				__this__.every(values, function(itemB) {
					if (__this__.isEqual(itemA, itemB)) {
						isInValues = true;
					}
					return !isInValues;
				});
				if (isInValues) {
					retVal.push(itemA);
				}
			});
		}
	}
	return retVal;
};


/*
    Creates a new array concatenating array with values
*/
this.concat = function(array, values) {
	const __this__ = this;
	if (__this__.isArray(array) && __this__.isArray(values)) {
		return array.concat(values);
	}
	return [];
};


/*
    Gets the index at which the first occurrence of value is found in array using isEqual for equality comparisons. If fromIndex is negative, it's used as the offset from the start of array.
*/
this.indexOf = function(array, value, fromIndex) {
	const __this__ = this;
	let index = -1;
	if (__this__.isArray(array)) {
		if (!__this__.isInteger(fromIndex) || fromIndex < -1) {
			fromIndex = 0;
		}
		for (let i = fromIndex; i < array.length; ++i) {
			if (__this__.isEqual(array[i], value)) {
				index = i;
				break;
			}
		}
	}
	return index;
};


/*
    Iterates over elements of collection, returning the first element predicate returns truthy for. 
*/
this.find = function(collection, predicate) {
	const __this__ = this;
	let retVal = null;
	if (__this__.isFunction(predicate) && (__this__.isArray(collection) || __this__.isPlainObject(collection)) && !__this__.isEmpty(collection)) {
		let objectFound = false,
			predicateResponse;
		__this__.every(collection, function(item, key) {
			predicateResponse = predicate(item, key);
			if (__this__.isBoolean(predicateResponse) && predicateResponse) {
				retVal = item;
				objectFound = true;
			}
			return !objectFound;
		});
	}
	return retVal;
};


/*
    This method is like find except that it returns the index of the first element predicate returns truthy for instead of the element itself.
*/
this.findIndex = function(collection, predicate) {
	const __this__ = this;
	let retVal = -1;
	if (__this__.isFunction(predicate) && __this__.isArray(collection) && !__this__.isEmpty(collection)) {
		let objectFound = false,
			predicateResponse;
		__this__.every(collection, function(item, key) {
			predicateResponse = predicate(item, key);
			if (__this__.isBoolean(predicateResponse) && predicateResponse) {
				retVal = key;
				objectFound = true;
			}
			return !objectFound;
		});
	}
	return retVal;
};


/*
    Converts value to a string. An empty string is returned for null and undefined values. The sign of -0 is preserved.
*/
this.toString = function(value) {
	const _this = this;
	if (_this.isNil(value)) {
		return '';
	}
	return value.toString();
};


/*
    Checks if value is undefined.
*/
this.isUndefined = function(value) {
	return typeof value === 'undefined';
};


this.sortBy = function(origArray, comparator) {
    const _this_ = this;
    if (!(_this_.isArray(comparator) && _this_.isFunction(comparator[0]))) {
        comparator = function(item) {
            return item;
        };
    } else {
        comparator = comparator[0];
    }
	if (origArray.length <= 1) {
		return origArray;
	} else {

		var left = [];
		var right = [];
		var newArray = [];
		var pivot = origArray.pop();
		var length = origArray.length;

		for (var i = 0; i < length; i++) {
			if (comparator(origArray[i]) <= comparator(pivot)) {
				left.push(origArray[i]);
			} else {
				right.push(origArray[i]);
			}
		}

		return newArray.concat(_this_.sortBy(left), pivot, _this_.sortBy(right));
	}
};


/*
    Function will return true if the passed number is of primitive type Integer or castable String
    Ex. [1,2,3,] or ['1', '2', '3']
    
    @param {string|number} number - String o integer value
    @returns {boolean} Returns true if is castable integer
*/
this.isValidInteger = function(number) {
	if (this.isInteger(parseInt(number, 10))) {
		return true;
	}
	return false;
};


//get Pirata; muy similar al de lodash.
this.get = function (obj, path, def) {
	if (Array.isArray(path)) {
      path = path.map((key) => key.replace('.', '#')).join('.');
    }

    if (!(typeof path === 'string' || path instanceof String) ||  obj == undefined) {
      return def;
    }


    const everyFunc = function(step) {
      return !(step && (obj = obj[step]) == undefined);
    };
    let fullPath = path
      .replace(/\[/g, '.')
      .replace(/]/g, '')
      .split('.')
      .filter(Boolean);
    fullPath = fullPath.map((key) => key.replace('#', '.'));
    return Array.isArray(fullPath) && fullPath.length > 0 && fullPath.every(everyFunc) ? obj : def;
};

this.isIntArray = function(array) {
	if (this.isArray(array) && !this.isEmpty(array)) {
		let valid = true;
		for (var num = 0; num < array.length; num++) {
			if (!this.isValidInteger(array[num])) {
				valid = false;
				break;
			}
		}
		return valid;
	} else {
		return false;
	}
};


this.isStringArray = function(array) {
	let valid = true;
	if (this.isArray(array) && !this.isEmpty(array)) {
		for (var str = 0; str < array.length; str++) {
			if (!this.isString(array[str])) {
				valid = false;
				break;
			}
		}
		return valid;
	} else {
		return false;
	}
};

/**
 *  similar function to https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String/padStart
 */
this.padStart = function padStart(string, desiredLength, pad) {
    const STR_PAD_LEFT = 1;
    const STR_PAD_BOTH = 3;
    // default values
    if (typeof (desiredLength) === 'undefined') {
        desiredLength = 0;
    }
    if (typeof (pad) === 'undefined') {
        pad = ' ';
    }
    if (typeof (dir) === 'undefined') {
        var dir = STR_PAD_LEFT;
    }

    if (desiredLength + 1 >= string.length) {
        switch (dir) {
            case STR_PAD_LEFT:
                string = Array(desiredLength + 1 - string.length).join(pad) + string;
                break;
            case STR_PAD_BOTH:
                var padlen = desiredLength - string.length;
                var right = Math.ceil(padlen / 2);
                var left = padlen - right;
                string = Array(left + 1).join(pad) + string + Array(right + 1).join(pad);
                break;
            default:
                string = string + Array(desiredLength + 1 - string.length).join(pad);
                break;
        } // end switch
    }
    return string;
};


/*deburr function*/
function basePropertyOf(object) {
    return (key) => object == null ? undefined : object[key];
}

const deburredLetters = {
  // Latin-1 Supplement block.
  '\xc0': 'A',  '\xc1': 'A', '\xc2': 'A', '\xc3': 'A', '\xc4': 'A', '\xc5': 'A',
  '\xe0': 'a',  '\xe1': 'a', '\xe2': 'a', '\xe3': 'a', '\xe4': 'a', '\xe5': 'a',
  '\xc7': 'C',  '\xe7': 'c',
  '\xd0': 'D',  '\xf0': 'd',
  '\xc8': 'E',  '\xc9': 'E', '\xca': 'E', '\xcb': 'E',
  '\xe8': 'e',  '\xe9': 'e', '\xea': 'e', '\xeb': 'e',
  '\xcc': 'I',  '\xcd': 'I', '\xce': 'I', '\xcf': 'I',
  '\xec': 'i',  '\xed': 'i', '\xee': 'i', '\xef': 'i',
  '\xd1': 'N',  '\xf1': 'n',
  '\xd2': 'O',  '\xd3': 'O', '\xd4': 'O', '\xd5': 'O', '\xd6': 'O', '\xd8': 'O',
  '\xf2': 'o',  '\xf3': 'o', '\xf4': 'o', '\xf5': 'o', '\xf6': 'o', '\xf8': 'o',
  '\xd9': 'U',  '\xda': 'U', '\xdb': 'U', '\xdc': 'U',
  '\xf9': 'u',  '\xfa': 'u', '\xfb': 'u', '\xfc': 'u',
  '\xdd': 'Y',  '\xfd': 'y', '\xff': 'y',
  '\xc6': 'Ae', '\xe6': 'ae',
  '\xde': 'Th', '\xfe': 'th',
  '\xdf': 'ss',
  // Latin Extended-A block.
  '\u0100': 'A',  '\u0102': 'A', '\u0104': 'A',
  '\u0101': 'a',  '\u0103': 'a', '\u0105': 'a',
  '\u0106': 'C',  '\u0108': 'C', '\u010a': 'C', '\u010c': 'C',
  '\u0107': 'c',  '\u0109': 'c', '\u010b': 'c', '\u010d': 'c',
  '\u010e': 'D',  '\u0110': 'D', '\u010f': 'd', '\u0111': 'd',
  '\u0112': 'E',  '\u0114': 'E', '\u0116': 'E', '\u0118': 'E', '\u011a': 'E',
  '\u0113': 'e',  '\u0115': 'e', '\u0117': 'e', '\u0119': 'e', '\u011b': 'e',
  '\u011c': 'G',  '\u011e': 'G', '\u0120': 'G', '\u0122': 'G',
  '\u011d': 'g',  '\u011f': 'g', '\u0121': 'g', '\u0123': 'g',
  '\u0124': 'H',  '\u0126': 'H', '\u0125': 'h', '\u0127': 'h',
  '\u0128': 'I',  '\u012a': 'I', '\u012c': 'I', '\u012e': 'I', '\u0130': 'I',
  '\u0129': 'i',  '\u012b': 'i', '\u012d': 'i', '\u012f': 'i', '\u0131': 'i',
  '\u0134': 'J',  '\u0135': 'j',
  '\u0136': 'K',  '\u0137': 'k', '\u0138': 'k',
  '\u0139': 'L',  '\u013b': 'L', '\u013d': 'L', '\u013f': 'L', '\u0141': 'L',
  '\u013a': 'l',  '\u013c': 'l', '\u013e': 'l', '\u0140': 'l', '\u0142': 'l',
  '\u0143': 'N',  '\u0145': 'N', '\u0147': 'N', '\u014a': 'N',
  '\u0144': 'n',  '\u0146': 'n', '\u0148': 'n', '\u014b': 'n',
  '\u014c': 'O',  '\u014e': 'O', '\u0150': 'O',
  '\u014d': 'o',  '\u014f': 'o', '\u0151': 'o',
  '\u0154': 'R',  '\u0156': 'R', '\u0158': 'R',
  '\u0155': 'r',  '\u0157': 'r', '\u0159': 'r',
  '\u015a': 'S',  '\u015c': 'S', '\u015e': 'S', '\u0160': 'S',
  '\u015b': 's',  '\u015d': 's', '\u015f': 's', '\u0161': 's',
  '\u0162': 'T',  '\u0164': 'T', '\u0166': 'T',
  '\u0163': 't',  '\u0165': 't', '\u0167': 't',
  '\u0168': 'U',  '\u016a': 'U', '\u016c': 'U', '\u016e': 'U', '\u0170': 'U', '\u0172': 'U',
  '\u0169': 'u',  '\u016b': 'u', '\u016d': 'u', '\u016f': 'u', '\u0171': 'u', '\u0173': 'u',
  '\u0174': 'W',  '\u0175': 'w',
  '\u0176': 'Y',  '\u0177': 'y', '\u0178': 'Y',
  '\u0179': 'Z',  '\u017b': 'Z', '\u017d': 'Z',
  '\u017a': 'z',  '\u017c': 'z', '\u017e': 'z',
  '\u0132': 'IJ', '\u0133': 'ij',
  '\u0152': 'Oe', '\u0153': 'oe',
  '\u0149': '\'n', '\u017f': 's'
};

const deburrLetter = basePropertyOf(deburredLetters);

const reLatin = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g;

/** Used to compose unicode character classes. */
const rsComboMarksRange = '\\u0300-\\u036f';
const reComboHalfMarksRange = '\\ufe20-\\ufe2f';
const rsComboSymbolsRange = '\\u20d0-\\u20ff';
const rsComboMarksExtendedRange = '\\u1ab0-\\u1aff';
const rsComboMarksSupplementRange = '\\u1dc0-\\u1dff';
const rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange + rsComboMarksExtendedRange + rsComboMarksSupplementRange;

/** Used to compose unicode capture groups. */
const rsCombo = `[${rsComboRange}]`;

/**
 * Used to match [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks) and
 * [combining diacritical marks for symbols](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks_for_Symbols).
 */
const reComboMark = RegExp(rsCombo, 'g');

/**
 * Deburrs `string` by converting
 * [Latin-1 Supplement](https://en.wikipedia.org/wiki/Latin-1_Supplement_(Unicode_block)#Character_table)
 * and [Latin Extended-A](https://en.wikipedia.org/wiki/Latin_Extended-A)
 * letters to basic Latin letters and removing
 * [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks).
 *
 * @since 3.0.0
 * @category String
 * @param {string} [string=''] The string to deburr.
 * @returns {string} Returns the deburred string.
 * @example
 *
 * deburr('déjà vu')
 * // => 'deja vu'
 */
this.deburr = function deburr(string) {
  return string && string.replace(reLatin, deburrLetter).replace(reComboMark, '');
};

