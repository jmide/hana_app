/*
Usage: 
    BaseAction({
        input: ['BRB::Report', 'ATR::FiscalPeriod'], //Must match the inputs described on the ActionParameters table
        output: ['BRB::Report'], //Must match the inputs described on the ActionParameters table
        action: function (report, period) {return [report];} 
    })
*/
function BaseAction (options) {
    if (!(typeof options == 'object')) {
        throw 'Invalid BaseAction Declaration (options is not a object)'
    }
    if (!(options.hasOwnProperty('input') && options.input.hasOwnProperty('length'))) {
        throw 'Invalid BaseAction Declaration (input is not a array)'
    }
    if (!(options.hasOwnProperty('output') && options.output.hasOwnProperty('length'))) {
        throw 'Invalid BaseAction Declaration (output is not a array)'
    }
    if (!(options.hasOwnProperty('action') && typeof options.action == 'function')) {
        throw 'Invalid BaseAction Declaration (action is not a function)'
    }
    options.context = options.context || {};
    var validate = (function () {
        for (var x = 0; x < this.input.length; x++) {
            try {
                var tag = arguments[x].getTag();
            } catch (e) {
                throw 'Received invalid object (not getTag method)\n' + (arguments[x])
            }
            try {
                if (tag.type !== this.input[x]) {
                    throw 'Expected ' + this.input[x] + ' got ' + tag;
                }
            } catch (e) {
                throw 'Invalid argument for action: ' + e;
            }
        }
    }).bind(options);
    var fn = (function action () {
        this.validate.apply(this, arguments);
        var response = this.action.apply(this, arguments);
        if (typeof response == 'object' && !response.hasOwnProperty('length')) response = [response];
        return response;
    }).bind({
        validate: validate,
        action: options.action,
        context: options.context
    });
    
    for (var prop in options) {
        if (['validate', 'action', 'context'].indexOf(prop) == -1) {
            fn[prop] = options[prop];
        }
    }
    fn.close = fn.close || 0; // Default to User Task
    
    fn.instanceOf = 'BaseAction';
    fn.getFlags = function() {
        return this.flags || {};  
    };
    return fn;
}
this.BaseAction = BaseAction;
/*
BaseAction.getAction({action: 'BRB.createVariant'})
*/
BaseAction.getAction = function (obj_tag) {
    var sliced = obj_tag.action.split('.'),
        component = sliced[0].toLowerCase(),
        action = sliced[1];
    
    try {
        $['import']('timp.' + component + '.server.api', 'api');
        return $['timp'][component]['server']['api']['api']['componentActions'][action];
    } catch (e) {
        var ca = $['timp'][component]['server']['api']['api']['componentActions'];
        if (typeof ca == 'object') {
            if(!ca.hasOwnProperty(action)) {
                throw 'Reference error: Could not find property ' + action + ' on $.timp.' + component + '.server.api.api.componentActions';
            }
        }
        throw 'Impossible to get $.' + ['timp',component,'server','api','api','componentActions',action].join('.') + ':' + util.parseError(ca);
    }
    return false;
}