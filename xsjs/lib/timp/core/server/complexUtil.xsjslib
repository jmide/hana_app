$.import('timp.core.server','util');
this.util = $.timp.core.server.util;

$.import('timp.core.server.models', 'schema');
this.schema = $.timp.core.server.models.schema;

// $.import('timp.schema.server.api','api');
// var schema_api = $.timp.schema.server.api.api;
// var schema = schema_api.schema;

$.import('timp.core.server.orm', 'sql');
this.sql = $.timp.core.server.orm.sql;

/*
Usage:
    $.getSource(package, filename,  extension/suffix);
    //For "/timp_internal/sap_tables/queries/list_sap_tables.sql"
    $.getSource('timp_internal.sap_tables.queries', 'list_sap_tables', 'sql');
*/
$.getSource = function (pkg, file, suffix) {
    var result = this.sql.SELECT({
        // "PACKAGE_ID", "OBJECT_NAME", "OBJECT_SUFFIX", "VERSION_ID", "ACTIVATED_AT", "ACTIVATED_BY", "EDIT", "CDATA", "BDATA", "COMPRESSION_TYPE", "FORMAT_VERSION", "DELIVERY_UNIT", "DU_VERSION", "DU_VENDOR", "DU_VERSION_SP", "DU_VERSION_PATCH", "OBJECT_STATUS", "CHANGE_NUMBER", "RELEASED_AT"
        query: 'SELECT "CDATA" FROM "_SYS_REPO"."ACTIVE_OBJECT"' 
        + " WHERE PACKAGE_ID = ? AND OBJECT_NAME = ? AND OBJECT_SUFFIX = ?",
        values: [
            {type: $.db.types.NVARCHAR, value: pkg},
            {type: $.db.types.NVARCHAR, value: file},
            {type: $.db.types.NVARCHAR, value: suffix}
        ]
    });
    var source;
    if (result.length) {
        source = result[0][0];
    } else {
        source = undefined;
    }
    return source;
};

this.util.URLRouter.prototype.errorHandler = function (parsedResponse) {;
    if($.response.status === 200) {
        for (var i = 0; i < $.messageCodes.length; i++) {
            var message = $.messageCodes[i];
            if (message.type === "E") {
                $.response.status = 500;
            }
        }
    }
    //Include only error messageCodes
    $.messageCodes = $.messageCodes.filter(function(row){
        return row.type == 'E' ? true : $.successMessageCodesActive;
    });
    try {
        while (typeof parsedResponse == 'string') {
            parsedResponse = JSON.parse(parsedResponse);
        }
    } catch (e) {}
    parsedResponse = {
        result: parsedResponse,
        messageCodes: $.messageCodes,
        status: $.response.status
    };
    
    $.res(parsedResponse);
};
