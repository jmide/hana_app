$.import('timp.core.server.router', 'router');
const Router = $.timp.core.server.router.router.Router;
$.import('timp.core.server.controllers.refactor', 'main');
const mainCtrl = $.timp.core.server.controllers.refactor.main;
$.import('timp.core.server.controllers.refactor', 'proxyService');
const proxyServiceCtrl = $.timp.core.server.controllers.refactor.proxyService;
$.import('timp.core.server.controllers.public', 'configuration');
const configuration = $.timp.core.server.controllers.public.configuration;
$.import('timp.core.server.controllers', 'privileges');
const privileges = $.timp.core.server.controllers.privileges;
$.import('timp.core.server.controllers.refactor', 'log');
const logCtrl = $.timp.core.server.controllers.refactor.log;

let routes = [
	{
		url: 'setSessionInformation',
		method: 'GET',
		privileges: [],
		params: {
			lang: 'string'
		},
		category: '99',
		object: '00',
		fn: mainCtrl.setSessionInformation,
		ctx: mainCtrl,
		bypass: true
	},
	{
		url: 'getCoreData',
		method: 'POST',
		privileges: [],
		category: '99',
		object: '00',
		fn: mainCtrl.getCoreData,
		ctx: mainCtrl,
		bypass: true
	},
	{
		url: 'getServiceInformation',
		method: 'GET',
		params: {
			serviceName: 'string'
		},
		privileges: [],
		category: '99',
		object: '00',
		fn: proxyServiceCtrl.getServiceInformation,
		ctx: proxyServiceCtrl
	},
	{
		url: 'getServiceLocation',
		method: 'GET',
		params: {
			adapterId: 'integer',
			serviceDestinationId: 'integer'
		},
		privileges: [],
		category: '99',
		object: '00',
		fn: proxyServiceCtrl.getTDFServiceLocation,
		ctx: proxyServiceCtrl
	},
	{
		url: 'createFullDates',
		method: 'POST',
		params: {
			year: 'integer'
		},
		privileges: [],
		category: '99',
		object: '00',
		fn: configuration.createFullDates,
		ctx: configuration
	},
	{
		url: '^checkParametersByTask/$',
		fn: privileges.checkParametersByTask,
		method: 'GET',
		privileges: [],
		object: '00',
		category: '99',
		ctx: privileges
	},
	{
		url: 'getTableInformation',
		fn: mainCtrl.getTableInformation,
		method: 'GET',
		privileges: [],
		object: '10',
		category: '00',
		ctx: mainCtrl,
		bypass: true
	},
	{
		url: 'getLogTrace',
		fn: logCtrl.getLogTrace,
		params: {
			id: 'integer',
			passphrase: 'string'
		},
		method: 'GET',
		privileges: [],
		object: '10',
		category: '00',
		ctx: logCtrl,
		bypass: true
	}
];
try{
let router = new Router('core', routes);
router.parseURL();
} catch (e) {
    let hola = e
}
