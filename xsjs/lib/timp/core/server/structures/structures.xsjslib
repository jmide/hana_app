this.structures = {
    300: {
      "hanaName": "CV_LOG_DETAILS",
      "version": 4,
      "parent": "LOG",
      "hanaPackage": "timp.core.modeling.log",
      "title": "TIMP Log",
      "titlePT": "TIMP Log",
      "titleEN": "TIMP Log",
      "description": "TIMP log data",
      "descriptionPT": "Dados do log do TIMP",
      "descriptionEN": "TIMP log data",
      "hasTDF": false,
      "inputParameters": [
        
      ],
      "levels": [
        {
          "name": "Header",
          "description": "Header",
          "namePT": "Cabeçalho",
          "descriptionPT": "Dados de cabeçalho",
          "nameEN": "Header",
          "descriptionEN": "Header data",
          "fields": [
            {
              "ID": 1
            },
            {
              "ID": 2
            },
            {
              "ID": 3
            },
            {
              "ID": 4
            },
            {
              "ID": 5
            },
            {
              "ID": 6
            },
            {
              "ID": 7
            },
            {
              "ID": 8
            },
            {
              "ID": 9
            },
            {
              "ID": 10
            },
            {
              "ID": 11
            },
            {
              "ID": 12
            },
            {
              "ID": 13
            },
            {
              "ID": 14
            },
            {
              "ID": 15
            },
            {
              "ID": 1601
            },
            {
              "ID": 1602
            },
            {
              "ID": 1603
            },
            {
              "ID": 1604
            },
            {
              "ID": 1605
            },
            {
              "ID": 1606
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "ATR",
          "description": "ATR Details",
          "namePT": "ATR",
          "descriptionPT": "Detalhe do ATR",
          "nameEN": "ATR",
          "descriptionEN": "ATR Details",
          "fields": [
            {
              "ID": 300
            },
            {
              "ID": 301
            },
            {
              "ID": 302
            },
            {
              "ID": 303
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "BCB",
          "description": "BCB Details",
          "namePT": "BCB",
          "descriptionPT": "Detalhe do BCB",
          "nameEN": "BCB",
          "descriptionEN": "BCB Details",
          "fields": [
            {
              "ID": 400
            },
            {
              "ID": 401
            },
            {
              "ID": 402
            },
            {
              "ID": 403
            },
            {
              "ID": 404
            },
            {
              "ID": 405
            },
            {
              "ID": 406
            },
            {
              "ID": 407
            },
            {
              "ID": 408
            },
            {
              "ID": 409
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "BFB",
          "description": "BFB Details",
          "namePT": "BFB",
          "descriptionPT": "Detalhe do BFB",
          "nameEN": "BFB",
          "descriptionEN": "BFB Details",
          "fields": [
            {
              "ID": 100
            },
            {
              "ID": 101
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "BRB",
          "description": "BRB Details",
          "namePT": "BRB",
          "descriptionPT": "Detalhe do BRB",
          "nameEN": "BRB",
          "descriptionEN": "BRB Details",
          "fields": [
            {
              "ID": 500
            },
            {
              "ID": 501
            },
            {
              "ID": 502
            },
            {
              "ID": 503
            },
            {
              "ID": 504
            },
            {
              "ID": 505
            },
            {
              "ID": 506
            },
            {
              "ID": 507
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "BRE",
          "description": "BRE Details",
          "namePT": "BRE",
          "descriptionPT": "Detalhe do BRE",
          "nameEN": "BRE",
          "descriptionEN": "BRE Details",
          "fields": [
            {
              "ID": 600
            },
            {
              "ID": 601
            },
            {
              "ID": 602
            },
            {
              "ID": 603
            },
            {
              "ID": 604
            },
            {
              "ID": 605
            },
            {
              "ID": 606
            },
            {
              "ID": 607
            },
            {
              "ID": 608
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "BSC",
          "description": "BSC Details",
          "namePT": "BSC",
          "descriptionPT": "Detalhe do BSC",
          "nameEN": "BSC",
          "descriptionEN": "BSC Details",
          "fields": [
            {
              "ID": 700
            },
            {
              "ID": 701
            },
            {
              "ID": 702
            },
            {
              "ID": 703
            },
            {
              "ID": 704
            },
            {
              "ID": 705
            },
            {
              "ID": 706
            },
            {
              "ID": 707
            },
            {
              "ID": 708
            },
            {
              "ID": 709
            },
            {
              "ID": 710
            },
            {
              "ID": 711
            },
            {
              "ID": 712
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "DFG",
          "description": "DFG Details",
          "namePT": "DFG",
          "descriptionPT": "Detalhe do DFG",
          "nameEN": "DFG",
          "descriptionEN": "DFG Details",
          "fields": [
            {
              "ID": 800
            },
            {
              "ID": 801
            },
            {
              "ID": 802
            },
            {
              "ID": 803
            },
            {
              "ID": 804
            },
            {
              "ID": 805
            },
            {
              "ID": 806
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "MDR",
          "description": "MDR Details",
          "namePT": "MDR",
          "descriptionPT": "Detalhe do MDR",
          "nameEN": "MDR",
          "descriptionEN": "MDR Details",
          "fields": [
            {
              "ID": 900
            },
            {
              "ID": 901
            },
            {
              "ID": 902
            },
            {
              "ID": 903
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TAA",
          "description": "TAA Details",
          "namePT": "TAA",
          "descriptionPT": "Detalhe do TAA",
          "nameEN": "TAA",
          "descriptionEN": "TAA Details",
          "fields": [
            {
              "ID": 1000
            },
            {
              "ID": 1001
            },
            {
              "ID": 1002
            },
            {
              "ID": 1003
            },
            {
              "ID": 1004
            },
            {
              "ID": 1005
            },
            {
              "ID": 1006
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TBD",
          "description": "TBD Details",
          "namePT": "TBD",
          "descriptionPT": "Detalhe do TBD",
          "nameEN": "TBD",
          "descriptionEN": "TBD Details",
          "fields": [
            {
              "ID": 1100
            },
            {
              "ID": 1101
            },
            {
              "ID": 1102
            },
            {
              "ID": 1103
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TCC",
          "description": "TCC Details",
          "namePT": "TCC",
          "descriptionPT": "Detalhe do TCC",
          "nameEN": "TCC",
          "descriptionEN": "TCC Details",
          "fields": [
            {
              "ID": 1200
            },
            {
              "ID": 1201
            },
            {
              "ID": 1202
            },
            {
              "ID": 1203
            },
            {
              "ID": 1204
            },
            {
              "ID": 1205
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TDK",
          "description": "TDK Details",
          "namePT": "TDK",
          "descriptionPT": "Detalhe do TDK",
          "nameEN": "TDK",
          "descriptionEN": "TDK Details",
          "fields": [
            {
              "ID": 1300
            },
            {
              "ID": 1301
            },
            {
              "ID": 1302
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TFB",
          "description": "TFB Details",
          "namePT": "TFB",
          "descriptionPT": "Detalhe do TFB",
          "nameEN": "TFB",
          "descriptionEN": "TFB Details",
          "fields": [
            {
              "ID": 200
            },
            {
              "ID": 201
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TFP",
          "description": "TFP Details",
          "namePT": "TFP",
          "descriptionPT": "Detalhe do TFP",
          "nameEN": "TFP",
          "descriptionEN": "TFP Details",
          "fields": [
            {
              "ID": 1400
            },
            {
              "ID": 1401
            },
            {
              "ID": 1402
            },
            {
              "ID": 1403
            },
            {
              "ID": 1404
            },
            {
              "ID": 1405
            },
            {
              "ID": 1406
            },
            {
              "ID": 1407
            },
            {
              "ID": 1408
            },
            {
              "ID": 1409
            },
            {
              "ID": 1410
            },
            {
              "ID": 1411
            },
            {
              "ID": 1412
            },
            {
              "ID": 1413
            },
            {
              "ID": 1414
            },
            {
              "ID": 1415
            },
            {
              "ID": 1416
            },
            {
              "ID": 1417
            },
            {
              "ID": 1418
            },
            {
              "ID": 1419
            },
            {
              "ID": 1420
            },
            {
              "ID": 1421
            },
            {
              "ID": 1422
            },
            {
              "ID": 1423
            },
            {
              "ID": 1424
            },
            {
              "ID": 1425
            },
            {
              "ID": 1426
            },
            {
              "ID": 1427
            },
            {
              "ID": 1428
            },
            {
              "ID": 1429
            },
            {
              "ID": 1430
            },
            {
              "ID": 1431
            },
            {
              "ID": 1432
            },
            {
              "ID": 1433
            },
            {
              "ID": 1434
            },
            {
              "ID": 1435
            },
            {
              "ID": 1436
            }
          ],
          "levels": [
            
          ]
        },
        {
          "name": "TPC",
          "description": "TPC Details",
          "namePT": "TPC",
          "descriptionPT": "Detalhe do TPC",
          "nameEN": "TPC",
          "descriptionEN": "TPC Details",
          "fields": [
            {
              "ID": 1500
            },
            {
              "ID": 1501
            },
            {
              "ID": 1502
            },
            {
              "ID": 1503
            },
            {
              "ID": 1504
            },
            {
              "ID": 1505
            },
            {
              "ID": 1506
            }
          ],
          "levels": [
            
          ]
        }
      ],
      "fields": [
        {
          "ID": 1,
          "hanaName": "ID_DETAILID",
          "isKey": true,
          "label": "ID",
          "labelPT": "ID",
          "labelEN": "ID",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 5000
        },
        {
          "ID": 2,
          "hanaName": "COMPONENT_ID",
          "isKey": false,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Component Id",
          "labelPT": "Id do Componente",
          "labelEN": "Component Id",
          "active": true
        },
        {
          "ID": 3,
          "hanaName": "USER_ID",
          "isKey": false,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "User Id",
          "labelPT": "Id do Usuario",
          "labelEN": "User Id",
          "active": true
        },
        {
          "ID": 4,
          "hanaName": "INTERNATIONALIZATION_CODE",
          "isKey": false,
          "label": "Log Code",
          "labelPT": "Código do log",
          "labelEN": "Log Code",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 32
        },
        {
          "ID": 5,
          "hanaName": "CA_DATE_FORMAT",
          "isKey": false,
          "type": "TIMESTAMP",
          "dimension": 10,
          "isMeasure": false,
          "label": "Date",
          "labelPT": "Data",
          "labelEN": "Date",
          "active": true
        },
        {
          "ID": 6,
          "hanaName": "OBJECT_ID",
          "isKey": false,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Object Id",
          "labelPT": "Id do Objeto",
          "labelEN": "Object Id",
          "active": true
        },
        {
          "ID": 7,
          "hanaName": "TYPE",
          "isKey": false,
          "type": "TINYINT",
          "isMeasure": false,
          "label": "Type",
          "labelPT": "Tipo de log",
          "labelEN": "Log Type",
          "active": true
        },
        {
          "ID": 8,
          "hanaName": "USR_NAME",
          "isKey": false,
          "label": "User Name",
          "labelPT": "Nome do Usuario",
          "labelEN": "User Name",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 50
        },
        {
          "ID": 9,
          "hanaName": "USR_LAST",
          "isKey": false,
          "label": "User Last Name",
          "labelPT": "Sobrenome do Usuario",
          "labelEN": "User Last Name",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 50
        },
        {
          "ID": 10,
          "hanaName": "USR_HANA",
          "isKey": false,
          "label": "HANA USER",
          "labelPT": "Usuario HANA",
          "labelEN": "HANA User",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 50
        },
        {
          "ID": 11,
          "hanaName": "COMP_ENUS",
          "isKey": false,
          "label": "Component Name (English)",
          "labelPT": "Nome Componente (Ingles)",
          "labelEN": "Component Name (English)",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 50
        },
        {
          "ID": 12,
          "hanaName": "COMP_PTBR",
          "isKey": false,
          "label": "Component Name (Portuguese)",
          "labelPT": "Nome Componente (Portuges)",
          "labelEN": "Component Name (Portuguese)",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 50
        },
        {
          "ID": 13,
          "hanaName": "COMP_NAME",
          "isKey": false,
          "label": "Component Technical Name",
          "labelPT": "Nome Técnico do Componente",
          "labelEN": "Component Technical Name",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 128
        },
        {
          "ID": 14,
          "hanaName": "MSG_TEXT",
          "isKey": false,
          "label": "Log Message",
          "labelPT": "Mensagem do Log",
          "labelEN": "Log Message",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true,
          "dimension": 128
        },
        {
          "ID": 15,
          "hanaName": "CODE_DESCRIPTION",
          "isKey": false,
          "label": "Code Description",
          "labelPT": "Descrição do código do Log",
          "labelEN": "Log Code Description",
          "type": "NVARCHAR",
          "isMeasure": false,
          "active": true
        },
        {
          "ID": 100,
          "hanaName": "BFB_MSG",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 101,
          "hanaName": "BFB_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 200,
          "hanaName": "TFB_MSG",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 201,
          "hanaName": "TFB_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 300,
          "hanaName": "ATR_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 301,
          "hanaName": "ATR_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 302,
          "hanaName": "ATR_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 303,
          "hanaName": "ATR_OTHER",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outors Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 400,
          "hanaName": "BCB_FIELD",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Field",
          "labelPT": "Campo",
          "labelEN": "Field"
        },
        {
          "ID": 401,
          "hanaName": "BCB_VALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Value",
          "labelPT": "Valor",
          "labelEN": "Value"
        },
        {
          "ID": 402,
          "hanaName": "BCB_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 403,
          "hanaName": "BCB_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 404,
          "hanaName": "BCB_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 405,
          "hanaName": "BCB_SERVICE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Service",
          "labelPT": "Serviço",
          "labelEN": "Service"
        },
        {
          "ID": 406,
          "hanaName": "BCB_SENTXML",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Sent XML",
          "labelPT": "XML Enviado",
          "labelEN": "Sent XML"
        },
        {
          "ID": 407,
          "hanaName": "BCB_OBTAINEDXML",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Returned XML",
          "labelPT": "XML Retornado",
          "labelEN": "Returned XML"
        },
        {
          "ID": 408,
          "hanaName": "BCB_PARAMETERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Parameters",
          "labelPT": "Parametros",
          "labelEN": "Parameters"
        },
        {
          "ID": 409,
          "hanaName": "BCB_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 500,
          "hanaName": "BRB_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 501,
          "hanaName": "BRB_NAME",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Name",
          "labelPT": "Nome",
          "labelEN": "Name"
        },
        {
          "ID": 502,
          "hanaName": "BRB_OLDNAME",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Old Name",
          "labelPT": "Nome Antigo",
          "labelEN": "Old Name"
        },
        {
          "ID": 503,
          "hanaName": "BRB_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 504,
          "hanaName": "BRB_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 505,
          "hanaName": "BRB_RULEIDS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Rule Ids",
          "labelPT": "Id das Regras",
          "labelEN": "Rule Ids"
        },
        {
          "ID": 506,
          "hanaName": "BRB_VALUES",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Values",
          "labelPT": "Valores",
          "labelEN": "Values"
        },
        {
          "ID": 507,
          "hanaName": "BRB_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 600,
          "hanaName": "BRE_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 601,
          "hanaName": "BRE_CODE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Code",
          "labelPT": "Código",
          "labelEN": "Code"
        },
        {
          "ID": 602,
          "hanaName": "BRE_DESCRIPTION",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Description",
          "labelPT": "Descrição",
          "labelEN": "Description"
        },
        {
          "ID": 603,
          "hanaName": "BRE_BASE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Base",
          "labelPT": "Base",
          "labelEN": "Base"
        },
        {
          "ID": 604,
          "hanaName": "BRE_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 605,
          "hanaName": "BRE_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 606,
          "hanaName": "BRE_ACTION",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Action",
          "labelPT": "Ação",
          "labelEN": "Action"
        },
        {
          "ID": 607,
          "hanaName": "BRE_NAME",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Name",
          "labelPT": "Nome",
          "labelEN": "Name"
        },
        {
          "ID": 608,
          "hanaName": "BRE_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 700,
          "hanaName": "BSC_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 701,
          "hanaName": "BSC_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 702,
          "hanaName": "BSC_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 703,
          "hanaName": "BSC_SERVICE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Service",
          "labelPT": "Serviço",
          "labelEN": "Service"
        },
        {
          "ID": 704,
          "hanaName": "BSC_SENTXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "SENT XML",
          "labelPT": "XML Enviado",
          "labelEN": "SENT XML"
        },
        {
          "ID": 705,
          "hanaName": "BSC_OBTAXML",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Returned XML",
          "labelPT": "XML Retornado",
          "labelEN": "Returned XML"
        },
        {
          "ID": 706,
          "hanaName": "BSC_FIELDNAME",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Field Name",
          "labelPT": "Nome do Campo",
          "labelEN": "Field Name"
        },
        {
          "ID": 707,
          "hanaName": "BSC_REASON",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Reason",
          "labelPT": "Razão",
          "labelEN": "Reason"
        },
        {
          "ID": 708,
          "hanaName": "BSC_JUST",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Justification",
          "labelPT": "Justificativa",
          "labelEN": "Justification"
        },
        {
          "ID": 709,
          "hanaName": "BSC_OBJID",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Object Id",
          "labelPT": "Id do Objeto",
          "labelEN": "Object Id"
        },
        {
          "ID": 710,
          "hanaName": "BSC_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 711,
          "hanaName": "BSC_DOCNUM",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 12,
          "isMeasure": false,
          "label": "Docnum",
          "labelPT": "Docnum",
          "labelEN": "Docnum"
        },
        {
          "ID": 712,
          "hanaName": "BSC_ITEMNUM",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 12,
          "isMeasure": false,
          "label": "Item-Docnum",
          "labelPT": "Item-Docnum",
          "labelEN": "Item-Docnum"
        },
        {
          "ID": 800,
          "hanaName": "DFG_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 801,
          "hanaName": "DFG_BLOCK",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Block",
          "labelPT": "Bloque",
          "labelEN": "Block"
        },
        {
          "ID": 802,
          "hanaName": "DFG_RECORD",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Record",
          "labelPT": "Registro",
          "labelEN": "Record"
        },
        {
          "ID": 803,
          "hanaName": "DFG_FIELD",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Field",
          "labelPT": "Campo",
          "labelEN": "Field"
        },
        {
          "ID": 804,
          "hanaName": "DFG_BLOCKID",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 6,
          "isMeasure": false,
          "label": "Block Id",
          "labelPT": "Id do Bloque",
          "labelEN": "Block Id"
        },
        {
          "ID": 805,
          "hanaName": "DFG_RECORDID",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 6,
          "isMeasure": false,
          "label": "Record Id",
          "labelPT": "Id do Registro",
          "labelEN": "Record Id"
        },
        {
          "ID": 806,
          "hanaName": "DFG_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 900,
          "hanaName": "MDR_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 901,
          "hanaName": "MDR_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 902,
          "hanaName": "MDR_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 903,
          "hanaName": "MDR_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1000,
          "hanaName": "TAA_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1001,
          "hanaName": "TAA_SERVICE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Service",
          "labelPT": "Serviço",
          "labelEN": "Service"
        },
        {
          "ID": 1002,
          "hanaName": "TAA_SENTXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Sent XML",
          "labelPT": "XML Enviado",
          "labelEN": "Sent XML"
        },
        {
          "ID": 1003,
          "hanaName": "TAA_OBTAXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Return XML",
          "labelPT": "XML Retornado",
          "labelEN": "Return XML"
        },
        {
          "ID": 1004,
          "hanaName": "TAA_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 1005,
          "hanaName": "TAA_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 1006,
          "hanaName": "TAA_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1100,
          "hanaName": "TBD_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1101,
          "hanaName": "TBD_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 1102,
          "hanaName": "TBD_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Valor Novo",
          "labelEN": "New Value"
        },
        {
          "ID": 1103,
          "hanaName": "TBD_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1200,
          "hanaName": "TCC_FIELD",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Field",
          "labelPT": "Campo",
          "labelEN": "Field"
        },
        {
          "ID": 1201,
          "hanaName": "TCC_VALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Value",
          "labelPT": "Valor",
          "labelEN": "Value"
        },
        {
          "ID": 1202,
          "hanaName": "TCC_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1203,
          "hanaName": "TCC_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 1204,
          "hanaName": "TCC_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 1205,
          "hanaName": "TCC_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1300,
          "hanaName": "TDK_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1301,
          "hanaName": "TDK_WIDGET",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Widget",
          "labelPT": "Widget",
          "labelEN": "Widget"
        },
        {
          "ID": 1302,
          "hanaName": "TDK_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1400,
          "hanaName": "TFP_OBJID",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Object Id",
          "labelPT": "Id do Objeto",
          "labelEN": "Object Id"
        },
        {
          "ID": 1401,
          "hanaName": "TFP_IDCOMPANY",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Company Id",
          "labelPT": "Id da Empresa",
          "labelEN": "Company Id"
        },
        {
          "ID": 1402,
          "hanaName": "TFP_UF",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 2,
          "isMeasure": false,
          "label": "State Code",
          "labelPT": "Código UF",
          "labelEN": "State Code"
        },
        {
          "ID": 1403,
          "hanaName": "TFP_IDBRANCH",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Branch Id",
          "labelPT": "Id da Filial",
          "labelEN": "Branch Id"
        },
        {
          "ID": 1404,
          "hanaName": "TFP_IDTAX",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Tax Id",
          "labelPT": "Id do Tax",
          "labelEN": "Tax Id"
        },
        {
          "ID": 1405,
          "hanaName": "TFP_YEAR",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Year",
          "labelPT": "Ano",
          "labelEN": "Year"
        },
        {
          "ID": 1406,
          "hanaName": "TFP_MONTH",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 2,
          "isMeasure": false,
          "label": "Month",
          "labelPT": "Mês",
          "labelEN": "Month"
        },
        {
          "ID": 1407,
          "hanaName": "TFP_SUBPERIOD",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Subperiod",
          "labelPT": "Subperiodo",
          "labelEN": "Subperiod"
        },
        {
          "ID": 1408,
          "hanaName": "TFP_STARTDATE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Start Date",
          "labelPT": "Data de Inicio",
          "labelEN": "Start Date"
        },
        {
          "ID": 1409,
          "hanaName": "TFP_ENDDATE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "End Date",
          "labelPT": "End Date",
          "labelEN": "End Date"
        },
        {
          "ID": 1410,
          "hanaName": "TFP_STATUS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Status",
          "labelPT": "Status",
          "labelEN": "Status"
        },
        {
          "ID": 1411,
          "hanaName": "TFP_PERIODICITY",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Periodicity",
          "labelPT": "Periocidade",
          "labelEN": "Periodicity"
        },
        {
          "ID": 1412,
          "hanaName": "TFP_STARTDATETYPE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Type Start Date",
          "labelPT": "Data de Inicio do Tipo",
          "labelEN": "Type Start Date"
        },
        {
          "ID": 1413,
          "hanaName": "TFP_ENDDATETYPE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Type End Date",
          "labelPT": "Data Fim do Tipo",
          "labelEN": "Type End Date"
        },
        {
          "ID": 1414,
          "hanaName": "TFP_TRIBUTO",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Tax",
          "labelPT": "Tributo",
          "labelEN": "Tax"
        },
        {
          "ID": 1415,
          "hanaName": "TFP_OLDSTATUS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Old Status",
          "labelPT": "Status Antigo",
          "labelEN": "Old Status"
        },
        {
          "ID": 1416,
          "hanaName": "TFP_VALIDFROM",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Valid From",
          "labelPT": "Valido Desde",
          "labelEN": "Valid From"
        },
        {
          "ID": 1417,
          "hanaName": "TFP_VALIDTO",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Valid To",
          "labelPT": "Valido Ate",
          "labelEN": "Valid To"
        },
        {
          "ID": 1418,
          "hanaName": "TFP_STATUSTDF",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Status TDF",
          "labelPT": "Status TDF",
          "labelEN": "Status TDF"
        },
        {
          "ID": 1419,
          "hanaName": "TFP_OPENTYPE",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Open Type",
          "labelPT": "Tipo de Abertura",
          "labelEN": "Open Type"
        },
        {
          "ID": 1420,
          "hanaName": "TFP_DAYTYPE",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Day Type",
          "labelPT": "Tipo de Dia",
          "labelEN": "Day Type"
        },
        {
          "ID": 1421,
          "hanaName": "TFP_DAY",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Day",
          "labelPT": "Dia",
          "labelEN": "Day"
        },
        {
          "ID": 1422,
          "hanaName": "TFP_CREATIONDATE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Creation Date",
          "labelPT": "Data de Criação",
          "labelEN": "Creation Date"
        },
        {
          "ID": 1423,
          "hanaName": "TFP_CREATIONUSER",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "Creation User",
          "labelPT": "Usuario de Criação",
          "labelEN": "Creation User"
        },
        {
          "ID": 1424,
          "hanaName": "TFP_MODIFDATE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Modification Date",
          "labelPT": "Data de Modificação",
          "labelEN": "Modification Date"
        },
        {
          "ID": 1425,
          "hanaName": "TFP_MODIFUSER",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "dimension": 32,
          "isMeasure": false,
          "label": "Modification User",
          "labelPT": "Usuario de Modificação",
          "labelEN": "Modification User"
        },
        {
          "ID": 1426,
          "hanaName": "TFP_COMPANY",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Company",
          "labelPT": "Empresa",
          "labelEN": "Company"
        },
        {
          "ID": 1427,
          "hanaName": "TFP_BRANCH",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 4,
          "isMeasure": false,
          "label": "Branch",
          "labelPT": "Filial",
          "labelEN": "Branch"
        },
        {
          "ID": 1428,
          "hanaName": "TFP_TYPE",
          "isKey": false,
          "active": true,
          "type": "INTEGER",
          "dimension": 32,
          "isMeasure": false,
          "label": "Type",
          "labelPT": "Tipo",
          "labelEN": "Type"
        },
        {
          "ID": 1429,
          "hanaName": "TFP_SERVICE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Service",
          "labelPT": "Serviço",
          "labelEN": "Service"
        },
        {
          "ID": 1430,
          "hanaName": "TFP_SENTXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Sent XML",
          "labelPT": "XML Enviado",
          "labelEN": "Sent XML"
        },
        {
          "ID": 1431,
          "hanaName": "TFP_OBTAXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Returned XML",
          "labelPT": "XML Retornado",
          "labelEN": "Returned XML"
        },
        {
          "ID": 1432,
          "hanaName": "TFP_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1433,
          "hanaName": "TFP_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 1434,
          "hanaName": "TFP_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 1435,
          "hanaName": "TFP_IDOBJTYPE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Object Type Id",
          "labelPT": "Id do Tipo de Objeto",
          "labelEN": "Object Type Id"
        },
        {
          "ID": 1436,
          "hanaName": "TFP_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1500,
          "hanaName": "TPC_MESSAGE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 512,
          "isMeasure": false,
          "label": "Message",
          "labelPT": "Mensagem",
          "labelEN": "Message"
        },
        {
          "ID": 1501,
          "hanaName": "TPC_SERVICE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 128,
          "isMeasure": false,
          "label": "Service",
          "labelPT": "Serviço",
          "labelEN": "Service"
        },
        {
          "ID": 1502,
          "hanaName": "TPC_SENTXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Sent XML",
          "labelPT": "XML Enviado",
          "labelEN": "Sent XML"
        },
        {
          "ID": 1503,
          "hanaName": "TPC_OBTAXML",
          "isKey": false,
          "active": true,
          "type": "TEXT",
          "dimension": 10000,
          "isMeasure": false,
          "label": "Returned XML",
          "labelPT": "XML Retornado",
          "labelEN": "Returned XML"
        },
        {
          "ID": 1504,
          "hanaName": "TPC_OLDVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Old Value",
          "labelPT": "Valor Antigo",
          "labelEN": "Old Value"
        },
        {
          "ID": 1505,
          "hanaName": "TPC_NEWVALUE",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "New Value",
          "labelPT": "Novo Valor",
          "labelEN": "New Value"
        },
        {
          "ID": 1506,
          "hanaName": "TPC_OTHERS",
          "isKey": false,
          "active": true,
          "type": "NVARCHAR",
          "dimension": 3000,
          "isMeasure": false,
          "label": "Other Values",
          "labelPT": "Outros Valores",
          "labelEN": "Other Values"
        },
        {
          "ID": 1601,
          "hanaName": "CA_TIME",
          "isKey": false,
          "type": "NVARCHAR",
          "dimension": 8,
          "isMeasure": false,
          "label": "Hora",
          "labelPT": "Time",
          "labelEN": "Hora",
          "active": true
        },
        {
          "ID": 1602,
          "hanaName": "DETAILS_OLD_VALUE",
          "active": true,
          "isKey": false,
          "type": "NVARCHAR",
          "dimension": 70,
          "isMeasure": false,
          "label": "Valor antigo (No novo registro do Log)",
          "labelPT": "Valor antigo (No novo registro do Log)",
          "labelEN": "Old value (In the new register of Log)"
        },
        {
          "ID": 1603,
          "hanaName": "DETAILS_NEW_VALUE",
          "active": true,
          "isKey": false,
          "type": "NVARCHAR",
          "dimension": 70,
          "isMeasure": false,
          "label": "Valor novo (No novo registro do Log)",
          "labelPT": "Valor novo (No novo registro do Log)",
          "labelEN": "New value (In the new register of Log)"
        },
        {
          "ID": 1604,
          "hanaName": "DETAILS_FIELD",
          "active": true,
          "isKey": false,
          "type": "NVARCHAR",
          "dimension": 70,
          "isMeasure": false,
          "label": "Field (No novo registro do Log)",
          "labelPT": "Field (No novo registro do Log)",
          "labelEN": "Field (In the new register of Log)"
        },
        {
          "ID": 1605,
          "hanaName": "DETAILS_ID_HEADER",
          "active": true,
          "isKey": false,
          "type": "INTEGER",
          "isMeasure": false,
          "label": "ID (No novo registro do Log)",
          "labelPT": "ID (No novo registro do Log)",
          "labelEN": "ID (In the new register of Log)"
        },
        {
          "ID": 1606,
          "hanaName": "DETAILS_INTERNATIONALIZATION_CODE",
          "active": true,
          "isKey": false,
          "type": "NVARCHAR",
          "dimension": 32,
          "isMeasure": false,
          "label": "Código do log (No novo registro do Log)",
          "labelPT": "Código do log (No novo registro do Log)",
          "labelEN": "Log code (In the new register of Log)"
        }
      ]
    },
    350: {
      "hanaName": "LOG_STRUCTURE",
      "version": 4,
      "parent": "LOG",
      "hanaPackage": "timp.atr.modeling.log_structures",
      "title": "Log de Modelagem das estruturas",
      "description": "Log de Modelagem das estruturas",
      "descriptionPT": "Log de Modelagem das estruturas",
      "descriptionEN": "Modeling Structure's log",
      "hasTDF": false,
      "isShadow": false,
      "lastID": 32,
      "inputParameters": [
        
      ],
      "levels": [
        {
          "name": "LOG modelagem",
          "description": "Dados de LOG modelagem nas estruturas",
          "namePT": "LOG modelagem",
          "descriptionPT": "Dados de LOG modelagem nas estruturas",
          "nameEN": "Modeling's LOG",
          "descriptionEN": "Modeling's LOG in the structures",
          "fields": [
            {
              "ID": 1
            },
            {
              "ID": 2
            },
            {
              "ID": 3
            },
            {
              "ID": 4
            },
            {
              "ID": 5
            },
            {
              "ID": 6
            },
            {
              "ID": 7
            },
            {
              "ID": 8
            },
            {
              "ID": 9
            },
            {
              "ID": 10
            },
            {
              "ID": 11
            },
            {
              "ID": 12
            },
            {
              "ID": 13
            },
            {
              "ID": 14
            },
            {
              "ID": 15
            },
            {
              "ID": 16
            },
            {
              "ID": 17
            },
            {
              "ID": 18
            },
            {
              "ID": 19
            },
            {
              "ID": 20
            },
            {
              "ID": 21
            },
            {
              "ID": 22
            },
            {
              "ID": 23
            },
            {
              "ID": 24
            },
            {
              "ID": 25
            },
            {
              "ID": 26
            },
            {
              "ID": 27
            },
            {
              "ID": 28
            },
            {
              "ID": 30
            },
            {
              "ID": 31
            }
          ],
          "levels": [
            
          ]
        }
      ],
      "fields": [
        {
          "ID": 1,
          "hanaName": "ID_LOG",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id do log",
          "labelPT": "Id do log",
          "labelEN": "Log id",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "ID",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 2,
          "hanaName": "STRUCTURE_ID_TD1",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id da estrutura",
          "labelPT": "Id da estrutura",
          "labelEN": "Structure id",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "STRUCTURE_ID",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 3,
          "hanaName": "TITLE",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 100,
          "precision": 0,
          "label": "Título",
          "labelPT": "Título",
          "labelEN": "Title",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "TITLE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 4,
          "hanaName": "VERSION",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Versão",
          "labelPT": "Versão",
          "labelEN": "Version",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "VERSION",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 5,
          "hanaName": "COMPONENT_FILE",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 10,
          "precision": 0,
          "label": "Componente do arquivo",
          "labelPT": "Componente do arquivo",
          "labelEN": "Arquivo's component",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "COMPONENT_FILE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 6,
          "hanaName": "HANA_VIEW",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 256,
          "precision": 0,
          "label": "View",
          "labelPT": "View",
          "labelEN": "view",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "HANA_VIEW",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 7,
          "hanaName": "HANANAME_ENV",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 256,
          "precision": 0,
          "label": "View no ambiente atual",
          "labelPT": "View no ambiente atual",
          "labelEN": "Current environment view",
          "tableName": "ATR::Structure",
          "sapName": "hanaName",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 8,
          "hanaName": "IS_NEW",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 1,
          "precision": 0,
          "label": "A estrutura é nova",
          "labelPT": "A estrutura é nova",
          "labelEN": "Is new the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "IS_NEW",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 9,
          "hanaName": "IS_DELETED",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 1,
          "precision": 0,
          "label": "A estrutura foi eliminada",
          "labelPT": "A estrutura foi eliminada",
          "labelEN": "The structure was deleted",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "IS_DELETED",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 10,
          "hanaName": "OBSERVATION_DELETE",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 50,
          "precision": 0,
          "label": "Motivo pelo qual a estrutura foi eliminada",
          "labelPT": "Motivo pelo qual a estrutura foi eliminada",
          "labelEN": "Why the structure was deleted",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "OBSERVATION_DELETE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 11,
          "hanaName": "OLD_HASH",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 30,
          "precision": 0,
          "label": "HASH antigo",
          "labelPT": "HASH antigo",
          "labelEN": "Old HASH",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "OLD_HASH",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 12,
          "hanaName": "NEW_HASH",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 30,
          "precision": 0,
          "label": "Novo HASH",
          "labelPT": "Novo HASH",
          "labelEN": "New HASH",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "NEW_HASH",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 13,
          "hanaName": "DATE_CHANGE",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "TIMESTAMP",
          "dimension": 8,
          "precision": 0,
          "label": "Data de alteração no TD1",
          "labelPT": "Data de alteração no TD1",
          "labelEN": "Modification date in TD1",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "DATE_CHANGE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 14,
          "hanaName": "CHANGE_PROPERTY",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 1,
          "precision": 0,
          "label": "Foi alterada uma propriedade da estrutura",
          "labelPT": "Foi alterada uma propriedade da estrutura",
          "labelEN": "A property in the structure was changed",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "CHANGE_PROPERTY",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 15,
          "hanaName": "PROPERTY",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 25,
          "precision": 0,
          "label": "Propriedade alterada da estrutura",
          "labelPT": "Propriedade alterada da estrutura",
          "labelEN": "Property changed in the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "PROPERTY",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 16,
          "hanaName": "OBSERVATION_PROPERTY",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 25,
          "precision": 0,
          "label": "Motivo pelo qual a propriedade foi alterada na estrutura",
          "labelPT": "Motivo pelo qual a propriedade foi alterada na estrutura",
          "labelEN": "Why the property changed in the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "OBSERVATION_PROPERTY",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 17,
          "hanaName": "ID_USER_TD1",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id do usuario que mudou no TD1",
          "labelPT": "Id do usuario que mudou no TD1",
          "labelEN": "TD1 user's id",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "ID_USER",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 18,
          "hanaName": "USER_NAME_TD1",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 10,
          "precision": 0,
          "label": "Usuario que mudou no TD1",
          "labelPT": "Usuario que mudou no TD1",
          "labelEN": "TD1 user name",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "USER_NAME",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 19,
          "hanaName": "CHANGE_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 1,
          "precision": 0,
          "label": "Foi alterado um campo na estrutura",
          "labelPT": "Foi alterado um campo na estrutura",
          "labelEN": "A field was change in the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "CHANGE_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 20,
          "hanaName": "ID_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id do campo na estrutura",
          "labelPT": "Id do campo na estrutura",
          "labelEN": "Field's id in the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "ID_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 21,
          "hanaName": "HANA_NAME_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 256,
          "precision": 0,
          "label": "HanaName do campo na estrutura",
          "labelPT": "HanaName do campo na estrutura",
          "labelEN": "Field's HanaName in the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "HANA_NAME_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 22,
          "hanaName": "OLD_LABEL_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 500,
          "precision": 0,
          "label": "Label antigo",
          "labelPT": "Label antigo",
          "labelEN": "Old label",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "OLD_LABEL_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 23,
          "hanaName": "NEW_LABEL_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 500,
          "precision": 0,
          "label": "Label novo",
          "labelPT": "Label novo",
          "labelEN": "New label",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "NEW_LABEL_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 24,
          "hanaName": "PROPERTY_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 15,
          "precision": 0,
          "label": "Propriedade alterada no campo",
          "labelPT": "Propriedade alterada no campo",
          "labelEN": "Field's changed property",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "PROPERTY_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 25,
          "hanaName": "OBSERVATION_PROPERTY_FIELD",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "NVARCHAR",
          "dimension": 25,
          "precision": 0,
          "label": "Motivo pelo qual a propriedade do campo foi mudada",
          "labelPT": "Motivo pelo qual a propriedade do campo foi mudada",
          "labelEN": "Why the field's property changed",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "OBSERVATION_PROPERTY_FIELD",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 26,
          "hanaName": "TABLES_IN_VIEW",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "TEXT",
          "dimension": null,
          "precision": 0,
          "label": "Tabelas asociadas a estrutura",
          "labelPT": "Tabelas asociadas a estrutura",
          "labelEN": "Associated tables to the structure",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "TABLES_IN_VIEW",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 27,
          "hanaName": "CREATION_DATE_ENV",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "TIMESTAMP",
          "dimension": 8,
          "precision": 0,
          "label": "Data de replicação da tabela log no ambiente atual",
          "labelPT": "Data de replicação da tabela log no ambiente atual",
          "labelEN": "Log table's replication date no Current environment",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "CREATION.DATE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 28,
          "hanaName": "CREATION_ID_USER_ENV",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id do usuario que fiz a replicação da tabela log no ambiente atual",
          "labelPT": "Id do usuario que fiz a replicação da tabela log no ambiente atual",
          "labelEN": "User id of log table's replication no Current environment",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "CREATION.ID_USER",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 30,
          "hanaName": "MODIFICATION_DATE_ENV",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "TIMESTAMP",
          "dimension": 1,
          "precision": 0,
          "label": "Data de alteração da tabela log no ambiente atual",
          "labelPT": "Data de alteração da tabela log no ambiente atual",
          "labelEN": "Log table's alteration date no Current environment",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "MODIFICATION.DATE",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        },
        {
          "ID": 31,
          "hanaName": "MODIFICATION_ID_USER_ENV",
          "active": true,
          "isKey": false,
          "isMeasure": false,
          "type": "INTEGER",
          "dimension": 0,
          "precision": 0,
          "label": "Id do usuario que fiz a alteração da tabela log no ambiente atual",
          "labelPT": "Id do usuario que fiz a alteração da tabela log no ambiente atual",
          "labelEN": "User id of log table's alteration no Current environment",
          "tableName": "ATR::STRUCTURE_LOG_MODELING",
          "sapName": "MODIFICATION.ID_USER",
          "isShadow": false,
          "shadowTable": null,
          "shadowName": null
        }
      ]
    }
};